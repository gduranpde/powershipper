﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Database
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				Log.Blank();

				Log.Info("Process started at {0}", DateTime.Now);

				ScriptRunner.CreateAll();

				Log.Info("Process completed successfully");
			}
			catch (Exception ex)
			{
				Log.Error(ex);
				Log.Info("Process failed");
			}

		}
	}
}
