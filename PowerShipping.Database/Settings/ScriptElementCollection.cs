﻿using System.Configuration;

namespace PowerShipping.Database
{
	[ConfigurationCollection(typeof(ScriptElement), AddItemName = "Script",
          CollectionType = ConfigurationElementCollectionType.BasicMap)]

	class ScriptElementCollection : ConfigurationElementCollection
	{
		protected override ConfigurationElement CreateNewElement()
		{
			return new ScriptElement();
		}

		protected override object GetElementKey(ConfigurationElement element)
		{
			return ((ScriptElement) element).Path;
		}
	}
}
