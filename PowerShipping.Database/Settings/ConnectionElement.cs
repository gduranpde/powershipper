using System.Configuration;

namespace PowerShipping.Database
{
	class ConnectionElement : ConfigurationElement
	{
		[ConfigurationProperty("Server", DefaultValue = ".", IsRequired = true)]
		public string Server
		{
			get { return (string)this["Server"]; }
			set { this["Server"] = value; }
		}

		[ConfigurationProperty("Database", DefaultValue = "", IsRequired = true)]
		public string Database
		{
			get { return (string)this["Database"]; }
		}

		[ConfigurationProperty("Username", DefaultValue = "sa")]
		public string Username
		{
			get { return (string)this["Username"]; }
		}

		[ConfigurationProperty("Password", DefaultValue = "")]
		public string Password
		{
			get { return (string)this["Password"]; }
		}

		[ConfigurationProperty("IntegratedSecurity", DefaultValue = false)]
		public bool IntergatedSecurity
		{
			get { return (bool)this["IntegratedSecurity"]; }
		}
	}
}