﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace PowerShipping.Database
{
	static class Settings
	{
		public static class Options
		{
			public static bool DropBeforeCreate
			{
				get { return Section.Options.DropBeforeCreate; }
			}
		}

		public static class Connection
		{
			public static string Server
			{
				get { return Section.Connection.Server; }
			}

			public static string Database
			{
				get { return Section.Connection.Database; }
			}

			public static string Username
			{
				get { return Section.Connection.Username; }
			}

			public static string Password
			{
				get { return Section.Connection.Password; }
			}

			public static bool IntegratedSecurity
			{
				get { return Section.Connection.IntergatedSecurity; }
			}

			public static string MasterConnectionString
			{
				get { return ConnectionStringFor("master"); }
			}

			public static string TargetConnectionString
			{
				get { return ConnectionStringFor(Database); }
			}

			private static string ConnectionStringFor(string database)
			{
				var builder = new SqlConnectionStringBuilder();

				builder.DataSource = Server;
				builder.InitialCatalog = database;

				if (IntegratedSecurity)
				{
					builder.IntegratedSecurity = IntegratedSecurity;
				}
				else if (!String.IsNullOrEmpty(Username))
				{
					builder.UserID = Username;

					if (!String.IsNullOrEmpty(Password))
						builder.Password = Password;
				}

				return builder.ConnectionString;
			}
		}

		public static class Scripts
		{
			public static FileInfo[] Files
			{
				get { return Section.Scripts.Cast<ScriptElement>().Select(e => new FileInfo(e.Path)).ToArray(); }
			}
		}

		private static readonly SettingsSection Section =
			(SettingsSection) ConfigurationManager.GetSection("Settings") ?? new SettingsSection();
	}
}
