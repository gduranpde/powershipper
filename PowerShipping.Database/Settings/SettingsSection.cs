using System.Configuration;

namespace PowerShipping.Database
{
	class SettingsSection: ConfigurationSection
	{
		[ConfigurationProperty("Options")]
		public OptionsElement Options
		{
			get { return (OptionsElement)this["Options"]; }
		}

		[ConfigurationProperty("Connection")]
		public ConnectionElement Connection
		{
			get { return (ConnectionElement)this["Connection"]; }
		}

		[ConfigurationProperty("Scripts")]
		public ScriptElementCollection Scripts
		{
			get { return (ScriptElementCollection)this["Scripts"]; }
		}
	}
}