﻿using System.Configuration;

namespace PowerShipping.Database
{
	class ScriptElement : ConfigurationElement
	{
		[ConfigurationProperty("Name")]
		public string Name
		{
			get { return (string)this["Name"]; }
		}

		[ConfigurationProperty("Path", IsKey = true, IsRequired = true)]
		//[RegexStringValidator()]
		public string Path
		{
			get { return (string)this["Path"]; }
		}
	}
}
