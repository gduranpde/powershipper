﻿using System.Configuration;

namespace PowerShipping.Database
{
	class OptionsElement : ConfigurationElement
	{
		[ConfigurationProperty("DropBeforeCreate", DefaultValue = false, IsRequired = true)]
		public bool DropBeforeCreate
		{
			get { return (bool)this["DropBeforeCreate"]; }
		}
	}
}
