ALTER TABLE [dbo].[ConsumerRequestChange] WITH CHECK
	ADD CONSTRAINT [FK_ConsumerRequestChange_ConsumerRequest] FOREIGN KEY([ConsumerRequestId])
	REFERENCES [dbo].[ConsumerRequest] ([Id])
GO
ALTER TABLE [dbo].[ConsumerRequestChange] CHECK CONSTRAINT [FK_ConsumerRequestChange_ConsumerRequest]
GO
