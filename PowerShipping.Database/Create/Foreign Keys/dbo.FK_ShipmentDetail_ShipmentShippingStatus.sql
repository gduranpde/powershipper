ALTER TABLE [dbo].[ShipmentDetail] WITH CHECK
	ADD CONSTRAINT [FK_ShipmentDetail_ShipmentShippingStatus] FOREIGN KEY([ShippingStatus])
	REFERENCES [dbo].[ShipmentShippingStatus] ([Id])
GO
ALTER TABLE [dbo].[ShipmentDetail] CHECK CONSTRAINT [FK_ShipmentDetail_ShipmentShippingStatus]
GO
