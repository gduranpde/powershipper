ALTER TABLE [dbo].[OptOutException]  WITH CHECK ADD  CONSTRAINT [FK_OptOutException_OptOutExceptionStatus] FOREIGN KEY([Status])
REFERENCES [dbo].[OptOutExceptionStatus] ([Id])
GO
ALTER TABLE [dbo].[OptOutException] CHECK CONSTRAINT [FK_OptOutException_OptOutExceptionStatus]
GO
