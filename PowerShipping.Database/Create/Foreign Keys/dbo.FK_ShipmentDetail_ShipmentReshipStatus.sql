ALTER TABLE [dbo].[ShipmentDetail] WITH CHECK
	ADD CONSTRAINT [FK_ShipmentDetail_ShipmentReshipStatus] FOREIGN KEY([ReshipStatus])
	REFERENCES [dbo].[ShipmentReshipStatus] ([Id])
GO
ALTER TABLE [dbo].[ShipmentDetail] CHECK CONSTRAINT [FK_ShipmentDetail_ShipmentReshipStatus]
GO
