ALTER TABLE [dbo].[Shipment] WITH CHECK
	ADD CONSTRAINT [FK_Shipment_KitType] FOREIGN KEY([KitTypeId])
	REFERENCES [dbo].[KitType] ([Id])
GO
ALTER TABLE [dbo].[Shipment] CHECK CONSTRAINT [FK_Shipment_KitType]
GO
