ALTER TABLE [dbo].[ShipmentDetail] WITH CHECK
	ADD CONSTRAINT [FK_ShipmentDetail_Shipment] FOREIGN KEY([ShipmentId])
	REFERENCES [dbo].[Shipment] ([Id])
GO
ALTER TABLE [dbo].[ShipmentDetail] CHECK CONSTRAINT [FK_ShipmentDetail_Shipment]
GO
