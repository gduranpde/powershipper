ALTER TABLE [dbo].[ConsumerRequestException] WITH CHECK
	ADD CONSTRAINT [FK_ConsumerRequestException_ConsumerRequestExceptionStatus] FOREIGN KEY([Status])
	REFERENCES [dbo].[ConsumerRequestExceptionStatus] ([Id])
GO
ALTER TABLE [dbo].[ConsumerRequestException] CHECK CONSTRAINT [FK_ConsumerRequestException_ConsumerRequestExceptionStatus]
GO
