ALTER TABLE [dbo].[ShipmentDetail] WITH CHECK
	ADD CONSTRAINT [FK_ShipmentDetail_ConsumerRequest] FOREIGN KEY([ConsumerRequestId])
	REFERENCES [dbo].[ConsumerRequest] ([Id])
GO
ALTER TABLE [dbo].[ShipmentDetail] CHECK CONSTRAINT [FK_ShipmentDetail_ConsumerRequest]
GO
