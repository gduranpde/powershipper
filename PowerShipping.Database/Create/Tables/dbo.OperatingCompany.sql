﻿USE [PowerShipper]
GO

--DROP TABLE [dbo].[OperatingCompany]
--GO

/****** Object:  Table [dbo].[OperatingCompany]    Script Date: 5/14/2014 4:48:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[OperatingCompany](
	[OperatingCompanyID] [uniqueidentifier] NOT NULL,
	[OperatingCompanyCode] [varchar](15) NOT NULL UNIQUE,
	[OperatingCompanyName] [varchar](100) NULL,
	[ParentCompanyID] [uniqueidentifier] NOT NULL,
	[ServiceState] [int] NOT NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastModifiedBy] [nvarchar](50) NULL,
	[LastModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_OperatingCompany] PRIMARY KEY CLUSTERED 
(
	[OperatingCompanyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[OperatingCompany]  WITH CHECK ADD  CONSTRAINT [FK_OperatingCompany_Company] FOREIGN KEY([ParentCompanyID])
REFERENCES [dbo].[Company] ([Id])
GO

ALTER TABLE [dbo].[OperatingCompany] CHECK CONSTRAINT [FK_OperatingCompany_Company]
GO

ALTER TABLE [dbo].[OperatingCompany]  WITH CHECK ADD  CONSTRAINT [FK_OperatingCompnay_State] FOREIGN KEY([ServiceState])
REFERENCES [dbo].[State] ([Id])
GO

ALTER TABLE [dbo].[OperatingCompany] CHECK CONSTRAINT [FK_OperatingCompnay_State]
GO


