SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FedExShippingDefault](

	[ShipperFedExAccountNumber] [nvarchar](9) NULL,
	[ShipperCompanyName] [nvarchar](100) NULL,
	[ShipperPhysicalAddress] [nvarchar](500) NULL,
	[ShipperContactName] [nvarchar](100) NULL,
	[ShipperContactPhone] [nvarchar](50) NULL,
	[PackagePickupLocation] [nvarchar](500) NULL,
	[MoreThanOnePackageForEachRecipient] [nvarchar](50) NULL,
	[FedExServiceType] [nvarchar](50) NULL,
	[DomesticOrInternational] [nvarchar](50) NULL,
	[AnyPackagesToResidentialAddresses] [nvarchar](50) NULL,
	[SignatureRequired] [nvarchar](50) NULL,
	[BulkLabelSupportSpecialHandling] [nvarchar](50) NULL,
	[PackageType] [nvarchar](50) NULL,
	[DeclaredValueAmount] [nvarchar](50) NULL,
	[PaymentType] [nvarchar](50) NULL,
	[FedExAccountNumberForBilling] [nvarchar](9) NULL,
	[DeliveryServiceForLabels] [nvarchar](50) NULL,
	[AddressToSendLabels] [nvarchar](500) NULL,
	[FedExBulkLabelEmailAddress] [nvarchar](100) NULL,
	[OtherCCEmailAddresses] [nvarchar](1000) NULL,
	[ConsumerCustomerServicePhoneNumber] [nvarchar](50) NULL,
	[PDMShippingExceptionEmailAddress] [nvarchar](50) NULL

) ON [PRIMARY]
GO
