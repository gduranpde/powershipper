SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShipmentDetail](

	[Id] [uniqueidentifier] NOT NULL,
	[ShipmentId] [uniqueidentifier] NOT NULL,
	[ConsumerRequestId] [uniqueidentifier] NOT NULL,
	[ShipmentNumber] [bigint] IDENTITY(1000200000,1) NOT NULL,
	[SortOrder] [int] NOT NULL,
	[HasException] [bit] NOT NULL,
	[IsReship] [bit] NOT NULL,
	[ShipDate] [datetime2](7) NULL,
	[ReshipStatus] [int] NOT NULL,
	[ShippingStatus] [int] NOT NULL,
	[ShippingStatusDate] [datetime2](7) NOT NULL,
	[FedExBatchId] [uniqueidentifier] NULL,
	[FedExTrackingNumber] [nvarchar](50) NULL,
	[FedExStatusDate] [datetime2](7) NULL,
	[FedExStatusCode] [nvarchar](50) NULL,
	[FedExStatusHistory] [ntext] NULL,
	[FedExLastUpdateStatus] [nvarchar](1024) NULL,
	[Notes] [nvarchar](1024) NULL,

	CONSTRAINT [PK_ShipmentDetail] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
