SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shipment](

	[Id] [uniqueidentifier] NOT NULL,
	[KitTypeId] [uniqueidentifier] NOT NULL,
	[PurchaseOrderNumber] [nvarchar](50) NOT NULL,
	[DateLabelsNeeded] [datetime2](7) NOT NULL,
	[DateCustomerWillShip] [datetime2](7) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedTime] [datetime2](7) NOT NULL,

	CONSTRAINT [PK_Shipment] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

) ON [PRIMARY]
GO
