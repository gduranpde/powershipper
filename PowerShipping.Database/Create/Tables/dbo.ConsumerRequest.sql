SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConsumerRequest](

	[Id] [uniqueidentifier] NOT NULL,
	[AccountNumber] [nvarchar](50) NOT NULL,
	[AccountName] [nvarchar](100) NOT NULL,
	[Address1] [nvarchar](200) NOT NULL,
	[Address2] [nvarchar](200) NULL,
	[City] [nvarchar](50) NOT NULL,
	[State] [nvarchar](50) NOT NULL,
	[ZipCode] [nvarchar](5) NOT NULL,
	[Email] [nvarchar](50) NULL,
	[Phone1] [nvarchar](50) NULL,
	[Phone2] [nvarchar](50) NULL,
	[Method] [nvarchar](100) NOT NULL,
	[AnalysisDate] [datetime2](7) NULL,
	[IsOkayToContact] [bit] NULL,
	[OperatingCompany] [nvarchar](30) NULL,
	[WaterHeaterFuel] [nvarchar](30) NULL,
	[IsReship] [bit] NOT NULL,
	[DoNotShip] [bit] NOT NULL,
	[ReceiptDate] [datetime2](7) NULL,
	[AuditFailureDate] [datetime2](7) NULL,
	[BatchId] [uniqueidentifier] NULL,
	[Notes] [nvarchar](1024) NULL,
	[Status] [int] NOT NULL,
	[OutOfState] [bit] NOT NULL,

	CONSTRAINT [PK_ConsumerRequest] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],

	CONSTRAINT [IX_ConsumerRequest_AccountNumber] UNIQUE NONCLUSTERED ( [AccountNumber] ASC )
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ConsumerRequest]  WITH CHECK ADD  CONSTRAINT [FK_ConsumerRequest_ConsumerRequestStatus] FOREIGN KEY([Status])
REFERENCES [dbo].[ConsumerRequestStatus] ([Id])
GO

ALTER TABLE [dbo].[ConsumerRequest] CHECK CONSTRAINT [FK_ConsumerRequest_ConsumerRequestStatus]
GO

ALTER TABLE [dbo].[ConsumerRequest] ADD  CONSTRAINT [DF_ConsumerRequest_OutOfState]  DEFAULT ((0)) FOR [OutOfState]
GOs
