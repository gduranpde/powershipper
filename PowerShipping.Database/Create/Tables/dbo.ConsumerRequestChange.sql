SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConsumerRequestChange](

	[Id] [uniqueidentifier] NOT NULL,
	[ConsumerRequestId] [uniqueidentifier] NOT NULL,
	[ChangedDate] [datetime2](7) NOT NULL,
	[ChangedBy] [nvarchar](100) NOT NULL,
	[AccountNumber] [nvarchar](50) NOT NULL,
	[AccountName] [nvarchar](100) NOT NULL,
	[Address1] [nvarchar](200) NOT NULL,
	[Address2] [nvarchar](200) NULL,
	[City] [nvarchar](50) NOT NULL,
	[State] [nvarchar](50) NOT NULL,
	[ZipCode] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](100) NULL,
	[Phone1] [nvarchar](50) NULL,
	[Phone2] [nvarchar](50) NULL

) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConsumerRequestChange] ADD CONSTRAINT [DF_ConsumerRequestChange_Id] DEFAULT (newid()) FOR [Id]
GO
CREATE NONCLUSTERED INDEX [IX_ConsumerRequestChange] ON [dbo].[ConsumerRequestChange] (	[ConsumerRequestId] ASC, [ChangedDate] ASC )
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
