INSERT	[dbo].[FedExShippingDefault](
		[ShipperFedExAccountNumber],
		[ShipperCompanyName],
		[ShipperPhysicalAddress],
		[ShipperContactName],
		[ShipperContactPhone],
		[PackagePickupLocation],
		[MoreThanOnePackageForEachRecipient],
		[FedExServiceType],
		[DomesticOrInternational],
		[AnyPackagesToResidentialAddresses],
		[SignatureRequired],
		[BulkLabelSupportSpecialHandling],
		[PackageType],
		[DeclaredValueAmount],
		[PaymentType],
		[FedExAccountNumberForBilling],
		[DeliveryServiceForLabels],
		[AddressToSendLabels],
		[FedExBulkLabelEmailAddress],
		[OtherCCEmailAddresses],
		[ConsumerCustomerServicePhoneNumber],
		[PDMShippingExceptionEmailAddress])
VALUES	(
		'ShiNumber',
		'Shipper Company Name',
		'Shipper Physical Address',
		'Shipper Contact Name',
		'Shipper Contact Phone',
		'Package Pickup Location',
		'More Than One Package For Each Recipient',
		'FedEx Service Type',
		'Domestic Or International',
		'Any Packages To Residential Addresses',
		'Signature Required',
		'Bulk Label Support Special Handling',
		'Package Type',
		'Declared Value Amount',
		'Payment Type',
		'BilNumber',
		'Delivery Service For Labels',
		'Address To Send Labels',
		'FedEx Bulk Label Email Address',
		'Other CC Email Addresses',
		'Consumer Customer Service Phone Number',
		'PDM Shipping Exception Email Address')
GO


