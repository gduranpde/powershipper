SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_ShipmentDetail]
AS
SELECT		sd.[Id], sd.[ShipmentId], sd.[ConsumerRequestId],
			s.[KitTypeId], kt.[KitName] [KitTypeName], 
			cr.[AccountNumber], cr.[AccountName],
			cr.[Address1], cr.[Address2], cr.[City], cr.[State], cr.[ZipCode], cr.[Email], cr.[Phone1], cr.[Phone2],
			s.[PurchaseOrderNumber], 
			CONVERT(NVARCHAR(50), sd.[ShipmentNumber]) [ShipmentNumber],
			sd.[SortOrder], sd.[HasException], sd.[IsReship], sd.[ShipDate],
			sd.[ReshipStatus], rs.[Name] [ReshipStatusName],
			sd.[ShippingStatus], ss.[Name] [ShippingStatusName],
			sd.[ShippingStatusDate],
			sd.[FedExBatchId], sd.[FedExTrackingNumber], 
			sd.[FedExStatusDate], sd.[FedExStatusCode], sd.[FedExStatusHistory],
			sd.[FedExLastUpdateStatus],
			sd.[Notes]
FROM		[ShipmentDetail] sd
			INNER JOIN [Shipment] s
				INNER JOIN [KitType] kt
				ON	kt.[Id] = s.[KitTypeId]
			ON	s.[Id] = sd.[ShipmentId]
			INNER JOIN [ConsumerRequest] cr
			ON	cr.[Id] = sd.[ConsumerRequestId]
			INNER JOIN [ShipmentReshipStatus] rs
			ON	rs.[Id] = sd.[ReshipStatus]
			INNER JOIN [ShipmentShippingStatus] ss
			ON	ss.[Id] = sd.[ShippingStatus]
GO
