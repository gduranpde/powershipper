SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_ConsumerRequestChange]
AS
SELECT		trg.[Id], trg.[ConsumerRequestId], 
			trg.[ChangedBy], trg.[ChangedDate],
			
			CONVERT(BIT, CASE WHEN org.[AccountNumber] = trg.[AccountNumber] THEN 0 ELSE 1 END) [AccountNumberChanged],
			CONVERT(BIT, CASE WHEN org.[AccountName] = trg.[AccountName] THEN 0 ELSE 1 END) [AccountNameChanged],
			CONVERT(BIT, CASE WHEN org.[Address1] = trg.[Address1] THEN 0 ELSE 1 END) [Address1Changed],
			CONVERT(BIT, CASE WHEN org.[Address2] = trg.[Address2] THEN 0 ELSE 1 END) [Address2Changed],
			CONVERT(BIT, CASE WHEN org.[City] = trg.[City] THEN 0 ELSE 1 END) [CityChanged],
			CONVERT(BIT, CASE WHEN org.[State] = trg.[State] THEN 0 ELSE 1 END) [StateChanged],
			CONVERT(BIT, CASE WHEN org.[ZipCode] = trg.[ZipCode] THEN 0 ELSE 1 END) [ZipCodeChanged],
			CONVERT(BIT, CASE WHEN org.[Email] = trg.[Email] THEN 0 ELSE 1 END) [EmailChanged],
			CONVERT(BIT, CASE WHEN org.[Phone1] = trg.[Phone1] THEN 0 ELSE 1 END) [Phone1Changed],
			CONVERT(BIT, CASE WHEN org.[Phone2] = trg.[Phone2] THEN 0 ELSE 1 END) [Phone2Changed],
			
			org.[AccountNumber] [OldAccountNumber], trg.[AccountNumber] [NewAccountNumber],
			org.[AccountName] [OldAccountName], trg.[AccountName] [NewAccountName],
			org.[Address1] [OldAddress1], trg.[Address1] [NewAddress1],
			org.[Address2] [OldAddress2], trg.[Address2] [NewAddress2],
			org.[City] [OldCity], trg.[City] [NewCity],
			org.[State] [OldState], trg.[State] [NewState],
			org.[ZipCode] [OldZipCode], trg.[ZipCode] [NewZipCode],
			org.[Email] [OldEmail], trg.[Email] [NewEmail],
			org.[Phone1] [OldPhone1], trg.[Phone1] [NewPhone1],
			org.[Phone2] [OldPhone2], trg.[Phone2] [NewPhone2]
			
FROM		[ConsumerRequestChange] org
			LEFT JOIN [ConsumerRequestChange] trg
			ON	trg.[ConsumerRequestId] = org.[ConsumerRequestId] AND
				trg.[ChangedDate] = (
					SELECT	MIN(x.[ChangedDate])
					FROM	[ConsumerRequestChange] x 
					WHERE	x.[ConsumerRequestId] = org.[ConsumerRequestId] AND
							x.[ChangedDate] > org.[ChangedDate])
				
WHERE		trg.[Id] IS NOT NULL
GO
