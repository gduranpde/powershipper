﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Shipment]
AS
	SELECT	s.[Id], s.[KitTypeId], kt.[KitName] [KitTypeName],
			s.[PurchaseOrderNumber], s.[DateLabelsNeeded], s.[DateCustomerWillShip],
			s.[CreatedBy], s.[CreatedTime]
	FROM	[Shipment] s
			INNER JOIN [KitType] kt
			ON	kt.[Id] = s.[KitTypeId]
GO
