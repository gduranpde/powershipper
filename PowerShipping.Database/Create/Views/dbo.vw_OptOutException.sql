SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_OptOutException]
AS
SELECT	oe.[Id], 
		oe.[OptOutDate], oe.[AccountNumber], oe.[AccountName],
		oe.[Address], oe.[City], oe.[State], oe.[ZipCode], oe.[Email], oe.[Phone1], oe.[Phone2],
		oe.[Notes], oe.[IsEnergyProgram], oe.[IsTotalDesignation],
		oe.[BatchId], ob.[ImportedBy], ob.[ImportedDate],
		oe.[Reason], oe.[Status]
FROM	[OptOutException] oe
		LEFT JOIN [OptOutBatch] ob
		On	ob.[Id] = oe.[BatchId]
GO
