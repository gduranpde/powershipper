SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_OptOut]
AS
SELECT	o.[Id], 
		o.[OptOutDate], CONVERT(NVARCHAR(50), o.[OptOutNumber]) [OptOutNumber],
		o.[AccountNumber], o.[AccountName],
		o.[Address], o.[City], o.[State], o.[ZipCode], o.[Email], o.[Phone1], o.[Phone2],
		o.[IsEnergyProgram], o.[IsTotalDesignation],
		o.[BatchId], ob.[ImportedBy], ob.[ImportedDate],
		o.[Notes]
FROM	[OptOut] o
		LEFT JOIN [OptOutBatch] ob
		On	ob.[Id] = o.[BatchId]
GO
