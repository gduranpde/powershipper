SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_OptOut_GetById]
@Id	UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	[Id], 
			[OptOutDate], [OptOutNumber],
			[AccountNumber], [AccountName],
			[Address], [City], [State], [ZipCode], [Email], [Phone1], [Phone2],
			[IsEnergyProgram], [IsTotalDesignation],
			[BatchId], [ImportedBy], [ImportedDate],
			[Notes]
	FROM	[vw_OptOut]
	WHERE	[Id] = @Id
END
GO
