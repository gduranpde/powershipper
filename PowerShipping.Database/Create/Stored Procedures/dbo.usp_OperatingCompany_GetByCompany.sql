﻿USE [PowerShipper]
GO
/****** Object:  StoredProcedure [dbo].[usp_OperatingCompany_GetByCompany]    Script Date: 05/21/2014 12:13:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [usp_OperatingCompany_GetByCompany] null
ALTER PROCEDURE [dbo].[usp_OperatingCompany_GetByCompany]
@CompanyId	UNIQUEIDENTIFIER = null
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	oc.*, c.CompanyName, ISNULL(s.Name, '') as StateName
	FROM	OperatingCompany AS oc
	INNER JOIN dbo.Company c ON oc.ParentCompanyID = c.Id
	Left JOIN dbo.State s ON oc.ServiceState = s.Id
	WHERE	@CompanyId IS NULL OR c.Id = @CompanyId
	
	ORDER BY CompanyName, StateName
END