SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_KitType_GetById]
@Id	UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	kt.[Id], 
			kt.[Width], kt.[Height], kt.[Length], kt.[Weight], 
			kt.[KitName], kt.[KitContents]
	FROM	[KitType] kt
	WHERE	kt.[Id] = @Id
END
GO