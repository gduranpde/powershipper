﻿USE [PowerShipper]
GO
/****** Object:  StoredProcedure [dbo].[usp_ItemSetup_Delete]******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_ItemSetup_Delete]
@ItemID	UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	DELETE 
	FROM	Item
	WHERE	[ItemID] = @ItemID
END

