﻿USE [PowerShipper]
GO
/****** Object:  StoredProcedure [dbo].[usp_OperatingCompany_Save]    Script Date: 05/22/2014 09:25:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_OperatingCompany_Save]
@OperatingCompanyID			UNIQUEIDENTIFIER,
@OperatingCompanyCode		NVARCHAR(15),
@OperatingCompanyName		NVARCHAR(100),
@ParentCompanyID			UNIQUEIDENTIFIER, 
@ServiceState				INT,
@IsActive					BIT = 1,
@CreatedBy					NVARCHAR(50),
@CreatedDate				DATETIME,
@LastModifiedBy				NVARCHAR(50),
@LastModifiedDate			DATETIME


AS
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT 1 FROM [OperatingCompany] WHERE [OperatingCompanyID] = @OperatingCompanyID)
	BEGIN
				
		UPDATE	[OperatingCompany]
		SET		[OperatingCompanyCode] = @OperatingCompanyCode,
				[OperatingCompanyName] = @OperatingCompanyName,
				[ParentCompanyID] = @ParentCompanyID,
				[ServiceState] = @ServiceState,
				[IsActive] = @IsActive,
				[CreatedBy] = @CreatedBy,
				[CreatedDate] = @CreatedDate,
				[LastModifiedBy] = @LastModifiedBy,
				[LastModifiedDate] = @LastModifiedDate
				
		WHERE	[OperatingCompanyID] = @OperatingCompanyID
		
	END ELSE
	BEGIN

		INSERT	[OperatingCompany](
				[OperatingCompanyID], [OperatingCompanyCode], [OperatingCompanyName], [ParentCompanyID], [ServiceState], [IsActive],
				[CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]
				)
		SELECT	@OperatingCompanyID, @OperatingCompanyCode, @OperatingCompanyName, @ParentCompanyID, @ServiceState ,@IsActive,
				@CreatedBy, @CreatedDate, @LastModifiedBy,  @LastModifiedDate
				
	END
END
