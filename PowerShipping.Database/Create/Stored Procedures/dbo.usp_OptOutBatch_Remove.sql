﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_OptOutBatch_Remove]
@Id	UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @TranStarted BIT
	SELECT	@TranStarted = 0
	
	BEGIN TRY
	
		IF @@TRANCOUNT = 0 
		BEGIN
			BEGIN TRANSACTION
			SET @TranStarted = 1
		END
		
		DELETE 
		FROM	[dbo].[OptOut]
		WHERE	[BatchId] = @Id
		
		DELETE 
		FROM	[dbo].[OptOutException]
		WHERE	[BatchId] = @Id
		
		DELETE 
		FROM	[dbo].[OptOutBatch]
		WHERE	[Id] = @Id
		
				
		IF @TranStarted = 1
			BEGIN
				SET @TranStarted = 0
				COMMIT TRANSACTION
			END
		
		END TRY
	
	BEGIN CATCH
		
		IF @TranStarted = 1
		BEGIN
			SET @TranStarted = 0
    		ROLLBACK TRANSACTION
		END
		
		DECLARE	@ErrorMessage NVARCHAR(4000)
	    DECLARE	@ErrorSeverity INT
		DECLARE	@ErrorState INT

		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE()

		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
    
	END CATCH

	
END
GO


END