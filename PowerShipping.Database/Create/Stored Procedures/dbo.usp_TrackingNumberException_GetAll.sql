SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TrackingNumberException_GetAll]
@ShowDeleted	BIT,
@AccountNumber NVARCHAR(50) = NULL,
@TrackingNumber NVARCHAR(50) = NULL,
@PurchaseOrderNumber NVARCHAR(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	[Id], [PurchaseOrderNumber], [TrackingNumber], [AccountNumber], [AccountName],
			[Address1], [Address2], [City], [State], [ZipCode], [Reference],
			[BatchId], [ImportedBy], [ImportedTime], 
			[Reason], [Status], [StatusName]
	FROM	[vw_TrackingNumberException]
	WHERE	(@ShowDeleted = 1 OR
			@ShowDeleted = 0 AND [Status] <> 1)
			AND
			(@AccountNumber IS NULL OR [AccountNumber] LIKE '%'+ @AccountNumber +'%' )
			AND
			(@TrackingNumber IS NULL OR [TrackingNumber] LIKE '%'+ @TrackingNumber +'%')			
			AND
			(@PurchaseOrderNumber IS NULL OR [PurchaseOrderNumber] LIKE '%'+ @PurchaseOrderNumber +'%')
END
GO
