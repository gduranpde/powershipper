SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ConsumerRequest_Save]
@Id					UNIQUEIDENTIFIER,
@AccountNumber		NVARCHAR(50),
@AccountName		NVARCHAR(100),
@Address1			NVARCHAR(200),
@Address2			NVARCHAR(200),
@City				NVARCHAR(50),
@State				NVARCHAR(50),
@ZipCode			NVARCHAR(5),
@Email				NVARCHAR(50),
@Phone1				NVARCHAR(50),
@Phone2				NVARCHAR(50),
@Method				NVARCHAR(100),
@AnalysisDate		DATETIME2(7),
@IsOkayToContact	BIT,
@OperatingCompany	NVARCHAR(30),
@WaterHeaterFuel	NVARCHAR(30),
@IsReship			BIT,
@DoNotShip			BIT,
@ReceiptDate		DATETIME2(7),
@AuditFailureDate	DATETIME2(7),
@BatchId			UNIQUEIDENTIFIER,
@Notes				NVARCHAR(1024),
@Status				INT,
@ChangedBy			NVARCHAR(50),
@OutOfState			BIT = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @TranStarted BIT
	SELECT	@TranStarted = 0
	
	BEGIN TRY
	
		IF @@TRANCOUNT = 0 
		BEGIN
			BEGIN TRANSACTION
			SET @TranStarted = 1
		END
		
			IF EXISTS(SELECT 1 FROM [ConsumerRequest] WHERE [Id] = @Id)
			BEGIN
			
				DECLARE	@OriginalAccountNumber	NVARCHAR(50)
				DECLARE	@OriginalAccountName	NVARCHAR(100)
				DECLARE	@OriginalAddress1		NVARCHAR(200)
				DECLARE	@OriginalAddress2		NVARCHAR(200)
				DECLARE	@OriginalCity			NVARCHAR(50)
				DECLARE	@OriginalState			NVARCHAR(50)
				DECLARE	@OriginalZipCode		NVARCHAR(5)
				DECLARE	@OriginalEmail			NVARCHAR(50)
				DECLARE	@OriginalPhone1			NVARCHAR(50)
				DECLARE	@OriginalPhone2			NVARCHAR(50)
			
				SELECT	@OriginalAccountNumber = [AccountNumber],
						@OriginalAccountName = [AccountName],
						@OriginalAddress1 = [Address1],
						@OriginalAddress2 = [Address2],
						@OriginalCity = [City],
						@OriginalState = [State],
						@OriginalZipCode = [ZipCode],
						@OriginalEmail = [Email],
						@OriginalPhone1 = [Phone1],
						@OriginalPhone2 = [Phone2]
				FROM	[ConsumerRequest]
				WHERE	[Id] = @Id						
			
				UPDATE	[ConsumerRequest]
				SET		[AccountNumber] = @AccountNumber,
						[AccountName] = @AccountName,
						[Address1] = @Address1,
						[Address2] = @Address2,
						[City] = @City,
						[State] = @State,
						[ZipCode] = @ZipCode,
						[Email] = @Email,
						[Phone1] = @Phone1,
						[Phone2] = @Phone2,
						[Method] = @Method,
						[AnalysisDate] = @AnalysisDate,
						[IsOkayToContact] = @IsOkayToContact,
						[OperatingCompany] = @OperatingCompany,
						[WaterHeaterFuel] = @WaterHeaterFuel,
						[IsReship] = @IsReship,
						[DoNotShip] = @DoNotShip,
						[ReceiptDate] = @ReceiptDate,
						[AuditFailureDate] = @AuditFailureDate,
						[BatchId] = @BatchId,
						[Notes] = @Notes,
						[Status] = @Status,
						[OutOfState] = @OutOfState
				WHERE	[Id] = @Id
				
				IF		@OriginalAccountNumber <> @AccountNumber OR @OriginalAccountName <> @AccountName OR
						@OriginalAddress1 <> @Address1 OR @OriginalAddress2 <> @Address2 OR 
						@OriginalCity <> @City OR @OriginalState <> @State OR
						@OriginalZipCode <> @ZipCode OR @OriginalEmail <> @Email OR 
						@OriginalPhone1 <> @Phone1 OR @OriginalPhone2 <> @Phone2
				BEGIN
			
					INSERT	[ConsumerRequestChange](
							[ConsumerRequestId], [ChangedBy], [ChangedDate], [AccountNumber], [AccountName], 
							[Address1], [Address2], [City], [State], [ZipCode], [Email], [Phone1], [Phone2])
					SELECT	@Id, @ChangedBy, SYSDATETIME(), @AccountNumber, @AccountName,
							@Address1, @Address2, @City, @State, @ZipCode, @Email, @Phone1, @Phone2
				
				END

			END ELSE
			BEGIN
			
				INSERT	[ConsumerRequest](
						[Id], [AccountNumber], [AccountName], 
						[Address1], [Address2], [City], [State], [ZipCode], [Email], [Phone1], [Phone2],
						[Method], [AnalysisDate], [IsOkayToContact], [OperatingCompany], [WaterHeaterFuel],
						[IsReship], [DoNotShip], [ReceiptDate], [AuditFailureDate],
						[BatchId], [Notes], [Status], [OutOfState])
				SELECT	@Id, @AccountNumber, @AccountName,
						@Address1, @Address2, @City, @State, @ZipCode, @Email, @Phone1, @Phone2,
						@Method, @AnalysisDate, @IsOkayToContact, @OperatingCompany, @WaterHeaterFuel,
						@IsReship, @DoNotShip, @ReceiptDate, @AuditFailureDate,
						@BatchId, @Notes, @Status, @OutOfState
				
				INSERT	[ConsumerRequestChange](
						[ConsumerRequestId], [ChangedBy], [ChangedDate], [AccountNumber], [AccountName], 
						[Address1], [Address2], [City], [State], [ZipCode], [Email], [Phone1], [Phone2])
				SELECT	@Id, @ChangedBy, SYSDATETIME(), @AccountNumber, @AccountName,
						@Address1, @Address2, @City, @State, @ZipCode, @Email, @Phone1, @Phone2

			END
			
		IF @TranStarted = 1
			BEGIN
				SET @TranStarted = 0
				COMMIT TRANSACTION
			END
		
		END TRY
	
	BEGIN CATCH
		
		IF @TranStarted = 1
		BEGIN
			SET @TranStarted = 0
    		ROLLBACK TRANSACTION
		END
		
		DECLARE	@ErrorMessage NVARCHAR(4000)
	    DECLARE	@ErrorSeverity INT
		DECLARE	@ErrorState INT

		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE()

		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
    
	END CATCH

END

GO
