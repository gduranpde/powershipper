﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AEG_Delivery_Report]
@PONumber NVARCHAR(50) = NULL,
@StartDate DATETIME2 = NULL,
@EndDate DATETIME2 = NULL,
@KitType UNIQUEIDENTIFIER = null
AS
BEGIN
	SET NOCOUNT ON;
    
	SELECT	cr.*, sd.KitTypeName,sd.PurchaseOrderNumber, 'Delivered' AS TransactionType, x.[CatalogId], x.[Quantity]
	FROM	[vw_ConsumerRequest] cr
	INNER JOIN dbo.vw_ShipmentDetail sd ON sd.ConsumerRequestId = cr.Id
	CROSS JOIN (
     SELECT '1477' [CatalogId], 4 [Quantity]
     UNION ALL
     SELECT '1478' [CatalogId], 4 [Quantity]
     UNION ALL
     SELECT '1479' [CatalogId], 2 [Quantity]
     UNION ALL
     SELECT '1480' [CatalogId], 2 [Quantity]
) x
	WHERE	
			
			(@PONumber IS NULL OR sd.PurchaseOrderNumber LIKE '%'+ @PONumber +'%' )
			AND			
			(@StartDate IS NULL OR cr.ReceiptDate >= @StartDate)
			AND
			(@EndDate IS NULL OR cr.ReceiptDate < @EndDate)
			AND
			(@KitType IS NULL OR sd.KitTypeId = @KitType)
			AND 
			ShippingStatus = 3
END
