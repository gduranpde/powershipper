SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_OptOut_GetMany]
@StartDate DATETIME2 = NULL,
@EndDate DATETIME2 = NULL,
@AccountNumber NVARCHAR(50) = NULL,
@AccountName NVARCHAR(100) = NULL,
@Phone NVARCHAR(50) = NULL,
@Address NVARCHAR(200) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	[Id], 
			[OptOutDate], [OptOutNumber],
			[AccountNumber], [AccountName],
			[Address], [City], [State], [ZipCode], [Email], [Phone1], [Phone2],
			[IsEnergyProgram], [IsTotalDesignation],
			[BatchId], [ImportedBy], [ImportedDate],
			[Notes]
	FROM	[vw_OptOut]
	WHERE	
			(@AccountNumber IS NULL OR [AccountNumber] LIKE '%'+ @AccountNumber +'%' )
			AND
			(@AccountName IS NULL OR [AccountName] LIKE '%'+ @AccountName +'%')
			AND
			(@Phone IS NULL OR [Phone1] LIKE '%'+ @Phone +'%' OR [Phone2] LIKE '%'+ @Phone +'%')
			AND
			(@Address IS NULL OR [Address] LIKE '%'+ @Address +'%')
			AND
			(@StartDate IS NULL OR [OptOutDate] >= @StartDate)
			AND
			(@EndDate IS NULL OR [OptOutDate] <= @EndDate)
END
GO
