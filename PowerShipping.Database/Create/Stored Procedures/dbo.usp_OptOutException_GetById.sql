SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_OptOutException_GetById]
@Id	UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	[Id], 
			[OptOutDate], [AccountNumber], [AccountName],
			[Address], [City], [State], [ZipCode], [Email], [Phone1], [Phone2],
			[Notes], [IsEnergyProgram], [IsTotalDesignation],
			[BatchId], [ImportedBy], [ImportedDate],
			[Reason], [Status]
	FROM	[vw_OptOutException]
	WHERE	[Id] = @Id
END
GO
