﻿USE [PowerShipper]
GO
/****** Object:  StoredProcedure [dbo].[usp_ItemSetup_GetById]    Script Date: 05/21/2014 10:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ItemSetup_GetById]
@ItemID	UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	*
	FROM	Item
	WHERE	[ItemID] = @ItemID
END
