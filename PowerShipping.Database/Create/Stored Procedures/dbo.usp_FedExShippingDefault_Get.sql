SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FedExShippingDefault_Get]
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT	[ShipperFedExAccountNumber],
			[ShipperCompanyName],
			[ShipperPhysicalAddress],
			[ShipperContactName],
			[ShipperContactPhone],
			[PackagePickupLocation],
			[MoreThanOnePackageForEachRecipient],
			[FedExServiceType],
			[DomesticOrInternational],
			[AnyPackagesToResidentialAddresses],
			[SignatureRequired],
			[BulkLabelSupportSpecialHandling],
			[PackageType],
			[DeclaredValueAmount],
			[PaymentType],
			[FedExAccountNumberForBilling],
			[DeliveryServiceForLabels],
			[AddressToSendLabels],
			[FedExBulkLabelEmailAddress],
			[OtherCCEmailAddresses],
			[ConsumerCustomerServicePhoneNumber],
			[PDMShippingExceptionEmailAddress]
	FROM	[FedExShippingDefault]
END
