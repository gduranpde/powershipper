using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace PowerShipping.Database
{
	static class ScriptLoader
	{
		public static ScriptCollection LoadAll(DirectoryInfo folder)
		{
			Guard.NotNull(folder, "folder");
            
			Log.Begin("Loading scripts from [{0}]", folder.Name);
	
			var scripts = from script in folder.GetFiles(ScriptFilePattern)
			              select LoadScript(script);
			
			Log.End();

			return new ScriptCollection(folder.Name, scripts);
		}

		public static ScriptCollection LoadAll(FileInfo[] files)
		{
			Guard.NotNull(files, "files");

			Log.Begin("Loading script collection");

			var scripts = from script in files
						  select LoadScript(script);

			Log.End();

			return new ScriptCollection("Scripts", scripts);
		}
		
		public static Script LoadScript(FileInfo file)
		{
			Guard.NotNull(file, "file");
            
			using (var reader = file.OpenText())
				return LoadScript(file.Name, reader.ReadToEnd());
		}

		public static Script LoadScript(string name, string content)
		{
			Guard.NotNull(name, "name");
			Guard.NotNull(content, "content");
            
			var commands = from command in Regex.Split(content, CommandSplitPattern, CommandSplitOptions)
			               where command.Trim().Length > 0
			               select new ScriptCommand(command);

			return new Script(name, commands);
		}
        
		private const string ScriptFilePattern = "*.sql";

		private const string CommandSplitPattern = @"\s*^\s*GO(\s*|\s*--+.*)$\s*";

		private const RegexOptions CommandSplitOptions =
			RegexOptions.Multiline | RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture;
	}
}