using System.Data.SqlClient;

namespace PowerShipping.Database
{
	class ScriptCommand
	{
		public readonly string CommandText;

		public ScriptCommand(string commandText)
		{
			Guard.NotNull(commandText, "commandText");
			
			CommandText = commandText;
		}

		public void Execute(SqlConnection connection)
		{
			Guard.NotNull(connection, "connection");
			
			new SqlCommand(CommandText, connection).ExecuteNonQuery();
		}
		
		public void Execute(SqlTransaction transaction)
		{
			Guard.NotNull(transaction, "transaction");
			
			new SqlCommand(CommandText, transaction.Connection, transaction).ExecuteNonQuery();
		}

		public static ScriptCommand DropDatabaseCommand(string databaseName)
		{
			Guard.NotNull(databaseName, "databaseName");
			
			return new ScriptCommand("DROP DATABASE [" + databaseName + "]");
		}
		
		public static ScriptCommand CreateDatabaseCommand(string databaseName)
		{
			Guard.NotNull(databaseName, "databaseName");
			
			return new ScriptCommand("CREATE DATABASE [" + databaseName + "]");
		}
	}
}