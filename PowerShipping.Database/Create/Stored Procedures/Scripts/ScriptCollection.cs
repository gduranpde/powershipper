﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace PowerShipping.Database
{
	class ScriptCollection
	{
		public readonly string Name;

		private readonly List<Script> Scripts = new List<Script>();

		public ScriptCollection(string name, IEnumerable<Script> scripts)
		{
			Guard.NotNull(name, "name");
			Guard.NotNull(scripts, "scripts");

			Name = name;
			Scripts.AddRange(scripts);
		}

		public void RunAll(SqlConnection connection)
		{
			Guard.NotNull(connection, "connection");

			Scripts.ForEach(c => c.Run(connection));
		}

		public void RunAll(SqlTransaction transaction)
		{
			Guard.NotNull(transaction, "transaction");

			Scripts.ForEach(c => c.Run(transaction));
		}
	}
}
