using System;
using System.Data.SqlClient;

namespace PowerShipping.Database
{
	static class ScriptRunner
	{
		public static void CreateAll()
		{
			using (var connection = new SqlConnection(Settings.Connection.MasterConnectionString))
			{
				Log.Begin("Connecting to server [{0}]", Settings.Connection.Server);
				connection.Open();
				Log.End();

				if (Settings.Options.DropBeforeCreate)
				{
					Log.Begin("Dropping database [{0}]", Settings.Connection.Database);
					ScriptCommand.DropDatabaseCommand(Settings.Connection.Database).Execute(connection);
					Log.End();
				}

				Log.Begin("Creating database [{0}]", Settings.Connection.Database);
				ScriptCommand.CreateDatabaseCommand(Settings.Connection.Database).Execute(connection);
				Log.End();

				Log.Begin("Connecting to database [{0}]", Settings.Connection.Database);
				connection.ChangeDatabase(Settings.Connection.Database);
				Log.End();

				RunScripts(connection);
			}
		}

		public static void RunScripts(SqlConnection connection)
		{
			Guard.NotNull(connection, "connection");

			using (var transaction = connection.BeginTransaction())
			{
				try
				{
					ScriptLoader
						.LoadAll(Settings.Scripts.Files)
						.RunAll(transaction);

					transaction.Commit();
				}
				catch (Exception ex)
				{
					Log.Error(ex.Message);

					Log.Begin("Rollback in progress");
					transaction.Rollback();
					Log.End();
				}
			}
		}
	}
}