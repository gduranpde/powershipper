using System.Collections.Generic;
using System.Data.SqlClient;

namespace PowerShipping.Database
{
	class Script
	{
		public readonly string Name;

		private readonly List<ScriptCommand> Commands = new List<ScriptCommand>();

		public Script(string name, IEnumerable<ScriptCommand> commands) 
		{
			Guard.NotNull(name, "name");
			Guard.NotNull(commands, "commands");

			Name = name;
			Commands.AddRange(commands);
		}

		public void Run(SqlConnection connection)
		{
			Guard.NotNull(connection, "connection");

			Log.Begin("Running [{0}]", Name);
			Commands.ForEach(c => c.Execute(connection));
			Log.End();
		}

		public void Run(SqlTransaction transaction)
		{
			Guard.NotNull(transaction, "transaction");
			
			Log.Begin("Running [{0}]", Name);
			Commands.ForEach(c => c.Execute(transaction));
			Log.End();
		}
	}
}