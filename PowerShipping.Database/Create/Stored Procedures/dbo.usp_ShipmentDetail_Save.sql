SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ShipmentDetail_Save]
@Id						UNIQUEIDENTIFIER,
@ShipmentId				UNIQUEIDENTIFIER,
@ConsumerRequestId		UNIQUEIDENTIFIER,
@SortOrder				INT,
@HasException			BIT,
@IsReship				BIT,
@ShipDate				DATETIME2(7),
@ReshipStatus			INT,
@ShippingStatus			INT,
@ShippingStatusDate		DATETIME2(7),
@FedExBatchId			UNIQUEIDENTIFIER,
@FedExTrackingNumber	NVARCHAR(50),
@FedExStatusDate		DATETIME2(7),
@FedExStatusCode		NVARCHAR(50),
@FedExStatusHistory		NTEXT,
@FedExLastUpdateStatus	NVARCHAR(1024),
@Notes					NVARCHAR(1024)
AS
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT 1 FROM [ShipmentDetail] WHERE [Id] = @Id)
	BEGIN

		UPDATE	[ShipmentDetail]
		SET		[SortOrder] = @SortOrder,
				[HasException] = @HasException,
				[IsReship] = @IsReship,
				[ShipDate] = @ShipDate,
				[ReshipStatus] = @ReshipStatus,
				[ShippingStatus] = @ShippingStatus,
				[ShippingStatusDate] = @ShippingStatusDate,
				[FedExBatchId] = @FedExBatchId,
				[FedExTrackingNumber] = @FedExTrackingNumber,
				[FedExStatusDate] = @FedExStatusDate,
				[FedExStatusCode] = @FedExStatusCode,
				[FedexStatusHistory] = @FedexStatusHistory,
				[FedExLastUpdateStatus] = @FedExLastUpdateStatus,
				[Notes] = @Notes
		WHERE	[Id] = @Id

	END ELSE
	BEGIN
	
		INSERT	[ShipmentDetail](
				[Id], [ShipmentId], [ConsumerRequestId],
				[SortOrder], [HasException], [IsReship], [ShipDate],
				[ReshipStatus], [ShippingStatus], [ShippingStatusDate],
				[FedExBatchId], [FedExTrackingNumber], [FedExStatusDate], [FedExStatusCode], [FedexStatusHistory], [FedExLastUpdateStatus],
				[Notes])
		SELECT	@Id, @ShipmentId, @ConsumerRequestId,
				@SortOrder, @HasException, @IsReship, @ShipDate,
				@ReshipStatus, @ShippingStatus, @ShippingStatusDate,
				@FedExBatchId, @FedExTrackingNumber, @FedExStatusDate, @FedExStatusCode, @FedexStatusHistory, @FedExLastUpdateStatus,
				@Notes

	END
END
GO