SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

alter procedure [dbo].[usp_Customer_CountRecords]
@ProjectId	UNIQUEIDENTIFIER
as
begin

     select *
	 from [Customer]
	 where ProjectId = @ProjectId
     
end
go