﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TrackingNumberBatch_GetAll]
@StartDate DATETIME2 = NULL
AS
BEGIN
	SET NOCOUNT ON;
  
		SELECT	*
		FROM	dbo.TrackingNumberBatch
		WHERE		
			(@StartDate IS NULL OR (ImportedTime >= @StartDate AND ImportedTime < DATEADD(DAY,1,@StartDate) ))
END
GO