﻿USE [PowerShipper]
GO
/****** Object:  StoredProcedure [dbo].[usp_KitType_GetAllWhereTrue]    Script Date: 05/21/2014 10:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_KitType_GetAllWhereTrue]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	*
	FROM	KitType
	WHERE	[IsActive] = 'True'
END
