﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ConsumerRequestBatch_GetPrevious]
@Today	DATETIME2
AS
BEGIN
	SET NOCOUNT ON;
    
	SELECT	TOP 1 BatchLabel
	FROM	dbo.ConsumerRequestBatch
	WHERE	
		BatchLabel IS NOT NULL
		AND 
		(ImportedDate >= @Today AND ImportedDate < DATEADD(DAY,1,@Today) )
	ORDER BY BatchLabel DESC
END

