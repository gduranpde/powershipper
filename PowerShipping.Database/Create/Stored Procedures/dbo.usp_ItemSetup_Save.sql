﻿USE [PowerShipper]
GO
/****** Object:  StoredProcedure [dbo].[usp_ItemSetup_Save]    Script Date: 05/22/2014 09:25:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ItemSetup_Save]
@ItemID						UNIQUEIDENTIFIER,
@ItemName					NVARCHAR(50),
@ItemDescription			NVARCHAR(500),
@DefaultKwImpact			DECIMAL(10,6),
@DefaultKwhImpact			DECIMAL(10,6),
@IsActive					BIT = 1,
@CreatedBy					NVARCHAR(50),
@CreatedDate				DATETIME,
@LastModifiedBy				NVARCHAR(50),
@LastModifiedDate			DATETIME

AS
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT 1 FROM [Item] WHERE [ItemID] = @ItemID)
	BEGIN
				
		UPDATE	[Item]
		SET		[ItemName] = @ItemName,
				[ItemDescription] = @ItemDescription,
				[DefaultKwImpact] = @DefaultKwImpact,
				[DefaultKwhImpact] = @DefaultKwhImpact,
				[IsActive] = @IsActive,
				[CreatedBy] = @CreatedBy,
				[CreatedDate] = @CreatedDate,
				[LastModifiedBy] = @LastModifiedBy,
				[LastModifiedDate] = @LastModifiedDate
				
		WHERE	[ItemID] = @ItemID
		
	END ELSE
	BEGIN

		INSERT	[Item](
				[ItemID], [ItemName], [ItemDescription], [DefaultKwImpact], [DefaultKwhImpact], [IsActive],
				[CreatedBy], [CreatedDate], [LastModifiedBy], [LastModifiedDate]
				)
		SELECT	@ItemID, @ItemName, @ItemDescription, @DefaultKwImpact, @DefaultKwhImpact ,@IsActive,
				@CreatedBy, @CreatedDate, @LastModifiedBy,  @LastModifiedDate
				
	END
END
