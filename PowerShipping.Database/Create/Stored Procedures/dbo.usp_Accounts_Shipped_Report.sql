﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Accounts_Shipped_Report]
@StartDate DATETIME2 = NULL,
@EndDate DATETIME2 = NULL
AS
BEGIN
	SET NOCOUNT ON;
    
	SELECT	cr.*, sd.KitTypeName, sd.ShipDate
	FROM	[vw_ConsumerRequest] cr
	INNER JOIN dbo.vw_ShipmentDetail sd ON sd.ConsumerRequestId = cr.Id
	
	WHERE			
				
			(@StartDate IS NULL OR cr.ReceiptDate >= @StartDate)
			AND
			(@EndDate IS NULL OR cr.ReceiptDate < @EndDate)			
			AND 
			sd.IsReship = 0
END
GO