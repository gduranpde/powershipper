SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ShipmentDetail_GetFedExLabels]
@ShipmentId	UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

    SELECT		[RecipientId],
				[Contact], [Company],
				[Address1], [Address2], [City], [State], [CountryCode], [Zip], 
				[Phone], [Service], [Residential],
				[Weight1], [Weight2], [Weight3], [Weight4], [Weight5],
				[Reference], [Reference2], [Reference3], [Reference4], [Reference5],
				[InvoiceNumber], [PurchaseOrderNumber],
				[DepartmentNotes]
	FROM		[vw_FedExLabel]
	WHERE		[ShipmentId] = @ShipmentId
	ORDER BY	[SortOrder]
END
GO

