SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FedExShippingDefault_Save]
@ShipperFedExAccountNumber			NVARCHAR(9),
@ShipperCompanyName					NVARCHAR(100),
@ShipperPhysicalAddress				NVARCHAR(500),
@ShipperContactName					NVARCHAR(100),
@ShipperContactPhone				NVARCHAR(50),
@PackagePickupLocation				NVARCHAR(500),
@MoreThanOnePackageForEachRecipient	NVARCHAR(50),
@FedExServiceType					NVARCHAR(50),
@DomesticOrInternational			NVARCHAR(50),
@AnyPackagesToResidentialAddresses	NVARCHAR(50),
@SignatureRequired					NVARCHAR(50),
@BulkLabelSupportSpecialHandling	NVARCHAR(50),
@PackageType						NVARCHAR(50),
@DeclaredValueAmount				NVARCHAR(50),
@PaymentType						NVARCHAR(50),
@FedExAccountNumberForBilling		NVARCHAR(9),
@DeliveryServiceForLabels			NVARCHAR(50),
@AddressToSendLabels				NVARCHAR(500),
@FedExBulkLabelEmailAddress			NVARCHAR(100),
@OtherCCEmailAddresses				NVARCHAR(1000),
@ConsumerCustomerServicePhoneNumber	NVARCHAR(50),
@PDMShippingExceptionEmailAddress	NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE	[FedExShippingDefault]
	SET		[ShipperFedExAccountNumber] = @ShipperFedExAccountNumber,
			[ShipperCompanyName] = @ShipperCompanyName,
			[ShipperPhysicalAddress] = @ShipperPhysicalAddress,
			[ShipperContactName] = @ShipperContactName,
			[ShipperContactPhone] = @ShipperContactPhone,
			[PackagePickupLocation] = @PackagePickupLocation,
			[MoreThanOnePackageForEachRecipient] = @MoreThanOnePackageForEachRecipient,
			[FedExServiceType] = @FedExServiceType,
			[DomesticOrInternational] = @DomesticOrInternational,
			[AnyPackagesToResidentialAddresses] = @AnyPackagesToResidentialAddresses,
			[SignatureRequired] = @SignatureRequired,
			[BulkLabelSupportSpecialHandling] = @BulkLabelSupportSpecialHandling,
			[PackageType] = @PackageType,
			[DeclaredValueAmount] = @DeclaredValueAmount,
			[PaymentType] = @PaymentType,
			[FedExAccountNumberForBilling] = @FedExAccountNumberForBilling,
			[DeliveryServiceForLabels] = @DeliveryServiceForLabels,
			[AddressToSendLabels] = @AddressToSendLabels,
			[FedExBulkLabelEmailAddress] = @FedExBulkLabelEmailAddress,
			[OtherCCEmailAddresses] = @OtherCCEmailAddresses,
			[ConsumerCustomerServicePhoneNumber] = @ConsumerCustomerServicePhoneNumber,
			[PDMShippingExceptionEmailAddress] = @PDMShippingExceptionEmailAddress
END
GO