SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ConsumerRequest_GetById]
@Id	UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

    
	SELECT	[Id], [AccountNumber], [AccountName],
			[Address1], [Address2], [City], [State], [ZipCode], [Email], [Phone1], [Phone2],
			[Method], [AnalysisDate], [IsOkayToContact], [OperatingCompany], [WaterHeaterFuel],
			[IsReship], [DoNotShip], [ReceiptDate], [AuditFailureDate],
			[BatchId], [Notes], [Status], [OutOfState]
	FROM	[ConsumerRequest]
	WHERE	[Id] = @Id

END
GO
