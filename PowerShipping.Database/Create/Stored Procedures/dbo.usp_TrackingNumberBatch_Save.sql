SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TrackingNumberBatch_Save]
@Id						UNIQUEIDENTIFIER,
@FileName				NVARCHAR(100),
@ImportedBy				NVARCHAR(50),
@ImportedTime			DATETIME2(7),
@NumbersInBatch			INT,
@ImportedSuccessfully	INT,
@ImportedWithException	INT
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT 1 FROM [TrackingNumberBatch] WHERE [Id] = @Id)
	BEGIN

		UPDATE	[TrackingNumberBatch]
		SET		[FileName] = @FileName,
				[ImportedBy] = @ImportedBy,
				[ImportedTime] = @ImportedTime,
				[NumbersInBatch] = @NumbersInBatch,
				[ImportedSuccessfully] = @ImportedSuccessfully,
				[ImportedWithException] = @ImportedWithException
		WHERE	[Id] = @Id

	END ELSE
	BEGIN
	
		INSERT	[TrackingNumberBatch](
				[Id], [FileName], [ImportedBy], [ImportedTime],
				[NumbersInBatch], [ImportedSuccessfully], [ImportedWithException])
		SELECT	@Id, @FileName, @ImportedBy, @ImportedTime,
				@NumbersInBatch, @ImportedSuccessfully, @ImportedWithException

	END
END
GO
