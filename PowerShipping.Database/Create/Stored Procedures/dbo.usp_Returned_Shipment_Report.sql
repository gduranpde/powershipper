﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Returned_Shipment_Report]
@StartDate DATETIME2 = NULL,
@EndDate DATETIME2 = NULL
AS
BEGIN
	SET NOCOUNT ON;
    
	SELECT	cr.*, sd.KitTypeName, sd.ShipDate, sd.FedExTrackingNumber, sd.ShipmentNumber, sd.ShippingStatusDate
	FROM	[vw_ConsumerRequest] cr
	INNER JOIN dbo.vw_ShipmentDetail sd ON sd.ConsumerRequestId = cr.Id
	
	WHERE			
				
			(@StartDate IS NULL OR sd.ShippingStatusDate >= @StartDate)
			AND
			(@EndDate IS NULL OR sd.ShippingStatusDate < @EndDate)			
			AND 
			sd.ShippingStatus = 4
END
