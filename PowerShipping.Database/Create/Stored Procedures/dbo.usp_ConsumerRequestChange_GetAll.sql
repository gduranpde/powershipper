﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ConsumerRequestChange_GetAll]
@StartDate DATETIME2 = NULL,
@EndDate DATETIME2 = NULL,
@AccountNumber NVARCHAR(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;
    
	SELECT	*
				
	FROM	vw_ConsumerRequestChange
	WHERE				
			(@StartDate IS NULL OR ChangedDate >= @StartDate)
			AND
			(@EndDate IS NULL OR ChangedDate < @EndDate)
			AND
			(@AccountNumber IS NULL OR [NewAccountNumber] LIKE '%'+ @AccountNumber +'%' )			
END
GO