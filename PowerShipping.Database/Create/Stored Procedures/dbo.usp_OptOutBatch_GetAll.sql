﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_OptOutBatch_GetAll]
@StartDate DATETIME2 = NULL
AS
BEGIN
	SET NOCOUNT ON;
  
		SELECT	*
		FROM	dbo.OptOutBatch
		WHERE		
			(@StartDate IS NULL OR (ImportedDate >= @StartDate AND ImportedDate < DATEADD(DAY,1,@StartDate) ))
END
GO