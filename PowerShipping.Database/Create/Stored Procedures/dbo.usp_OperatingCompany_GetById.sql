﻿USE [PowerShipper]
GO
/****** Object:  StoredProcedure [dbo].[usp_OperatingCompany_GetById]    Script Date: 05/21/2014 10:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_OperatingCompany_GetById]
@OperatingCompanyID	UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	*
	FROM	OperatingCompany
	WHERE	[OperatingCompanyID] = @OperatingCompanyID
END
