SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ConsumerRequestException_GetById]
@Id	UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	[Id], [AccountNumber], [AccountName],
			[Address1], [Address2], [City], [State], [ZipCode], [Email], [Phone1], [Phone2],
			[Method], [AnalysisDate], [IsOkayToContact], [OperatingCompany], [WaterHeaterFuel],
			[BatchId], [Reason], [Status], [Notes], [OutOfState], ImportedDate, BatchLabel, ImportedBy
	FROM	dbo.vw_ConsumerRequestException
	WHERE	[Id] = @Id
END
GO