SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TrackingNumberException_Save]
@Id						UNIQUEIDENTIFIER,
@PurchaseOrderNumber	NVARCHAR(50),
@TrackingNumber			NVARCHAR(50),
@AccountNumber			NVARCHAR(50),
@AccountName			NVARCHAR(100),
@Address1				NVARCHAR(200),
@Address2				NVARCHAR(200),
@City					NVARCHAR(50),
@State					NVARCHAR(50),
@ZipCode				NVARCHAR(5),
@Reference				NVARCHAR(50),
@BatchId				UNIQUEIDENTIFIER,
@Reason					NVARCHAR(4000),
@Status					INT
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT 1 FROM [TrackingNumberException] WHERE [Id] = @Id)
	BEGIN

		UPDATE	[TrackingNumberException]
		SET		[PurchaseOrderNumber] = @PurchaseOrderNumber,
				[TrackingNumber] = @TrackingNumber,
				[AccountNumber] = @AccountNumber,
				[AccountName] = @AccountName,
				[Address1] = @Address1,
				[Address2] = @Address2,
				[City] = @City,
				[State] = @State,
				[ZipCode] = @ZipCode,
				[Reference] = @Reference,
				[BatchId] = @BatchId,
				[Reason] = @Reason,
				[Status] = @Status
		WHERE	[Id] = @Id

	END ELSE
	BEGIN
	
		INSERT	[TrackingNumberException](
				[Id], [PurchaseOrderNumber], 
				[TrackingNumber], [AccountNumber], [AccountName], 
				[Address1], [Address2], [City], [State], [ZipCode], [Reference],
				[BatchId], [Reason], [Status])
		SELECT	@Id, @PurchaseOrderNumber,
				@TrackingNumber, @AccountNumber, @AccountName,
				@Address1, @Address2, @City, @State, @ZipCode, @Reference,
				@BatchId, @Reason, @Status

	END
END
GO
