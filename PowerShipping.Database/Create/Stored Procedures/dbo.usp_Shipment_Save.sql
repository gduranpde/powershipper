SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Shipment_Save]
@Id						UNIQUEIDENTIFIER,
@KitTypeId				UNIQUEIDENTIFIER,
@PurchaseOrderNumber	NVARCHAR(50),
@DateLabelsNeeded		DATETIME2(7),
@DateCustomerWillShip	DATETIME2(7),
@CreatedBy				NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT 1 FROM [Shipment] WHERE [Id] = @Id)
	BEGIN
		UPDATE	[Shipment]
		SET		[KitTypeId] = @KitTypeId,
				[PurchaseOrderNumber] = @PurchaseOrderNumber,
				[DateLabelsNeeded] = @DateLabelsNeeded,
				[DateCustomerWillShip] = @DateCustomerWillShip
		WHERE	[Id] = @Id
	END ELSE
	BEGIN
		INSERT	[Shipment](
				[Id], [KitTypeId], [PurchaseOrderNumber], 
				[DateLabelsNeeded], [DateCustomerWillShip], 
				[CreatedBy], [CreatedTime])
		SELECT	@Id, @KitTypeId, @PurchaseOrderNumber,
				@DateLabelsNeeded, @DateCustomerWillShip,
				@CreatedBy, SYSDATETIME()
	END
END
GO
