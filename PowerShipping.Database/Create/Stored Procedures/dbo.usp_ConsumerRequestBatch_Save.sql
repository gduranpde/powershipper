SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ConsumerRequestBatch_Save]
@Id						UNIQUEIDENTIFIER,
@FileName				NVARCHAR(100),
@ImportedBy				NVARCHAR(50),
@ImportedDate			DATETIME2(7),
@RequestsInBatch		INT,
@ImportedSuccessfully	INT,
@ImportedWithException	INT,
@BatchLabel				NVARCHAR(20) = null
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT 1 FROM [ConsumerRequestBatch] WHERE [Id] = @Id)
	BEGIN

		UPDATE	[ConsumerRequestBatch]
		SET		[FileName] = @FileName,
				[ImportedBy] = @ImportedBy,
				[ImportedDate] = @ImportedDate,
				[RequestsInBatch] = @RequestsInBatch,
				[ImportedSuccessfully] = @ImportedSuccessfully,
				[ImportedWithException] = @ImportedWithException,
				[BatchLabel] = @BatchLabel
		WHERE	[Id] = @Id

	END ELSE
	BEGIN
	
		INSERT	[ConsumerRequestBatch](
				[Id], [FileName], [ImportedBy], [ImportedDate],
				[RequestsInBatch], [ImportedSuccessfully], [ImportedWithException], [BatchLabel])
		SELECT	@Id, @FileName, @ImportedBy, @ImportedDate,
				@RequestsInBatch, @ImportedSuccessfully, @ImportedWithException, @BatchLabel

	END
END
GO
