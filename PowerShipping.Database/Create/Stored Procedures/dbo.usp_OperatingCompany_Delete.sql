﻿USE [PowerShipper]
GO
/****** Object:  StoredProcedure [dbo].[usp_OperatingCompany_Delete]******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].usp_OperatingCompany_Delete
@OperatingCompanyID	UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	DELETE 
	FROM	OperatingCompany
	WHERE	[OperatingCompanyID] = @OperatingCompanyID
END

