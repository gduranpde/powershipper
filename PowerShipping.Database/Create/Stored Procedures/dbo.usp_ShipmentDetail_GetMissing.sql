﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_ShipmentDetail_GetMissing]
@PONumber NVARCHAR(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT		[Id], 
				[ShipmentId],
				[ConsumerRequestId],
				
				[KitTypeId], [KitTypeName], 
				
				[AccountNumber], [AccountName], [Address1], [Address2],
				[City], [State], [ZipCode], [Email], [Phone1], [Phone2], 
			
				[PurchaseOrderNumber], 
				[ShipmentNumber], [SortOrder],
				[HasException], [IsReship], [ShipDate],
				[ReshipStatus], [ReshipStatusName],
				[ShippingStatus], [ShippingStatusName],
				[ShippingStatusDate],
				
				[FedExBatchId], [FedExTrackingNumber], 
				[FedExStatusDate], [FedExStatusCode], [FedExStatusHistory],
				[FedExLastUpdateStatus],
				
				[Notes]
      
	FROM		[vw_ShipmentDetail]
	WHERE		[FedExTrackingNumber] IS NULL AND
				[ShippingStatus] IN (1) -- Pending 
				AND
				(@PONumber IS NULL OR [PurchaseOrderNumber] LIKE '%'+ @PONumber +'%' )
END
GO