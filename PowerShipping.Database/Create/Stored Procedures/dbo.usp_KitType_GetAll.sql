SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_KitType_GetAll]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT		kt.[Id], 
				kt.[Width], kt.[Height], kt.[Length], kt.[Weight], 
				kt.[KitName], kt.[KitContents]
	FROM		[KitType] kt
	ORDER BY	kt.[KitName]
END
GO