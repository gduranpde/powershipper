﻿using System;

namespace PowerShipping.Database
{
	class Guard
	{
		public static void NotNull(object o, string name)
		{
			if (o == null)
				throw new ArgumentNullException(name);
		}
	}
}
