﻿using System;
using System.Diagnostics;

namespace PowerShipping.Database
{
	static class Log
	{
		public static void Blank()
		{
			Trace.WriteLine(String.Empty);
		}

		public static void Error(Exception exception)
		{
			var message = String.Empty;

			while (exception != null)
			{
				message += exception.Message + " ";
				exception = exception.InnerException;
			}

			Error(message);
		}

		public static void Error(string message, params object[] args)
		{
			Trace.WriteLine(String.Format("Error : " + message, args));
		}

		public static void Info(string message, params object[] args)
		{
			Trace.WriteLine(String.Format(message, args));
		}

		public static void Begin(string message, params object[] args)
		{
			Trace.Write(String.Format(message, args).PadRight(100, '.'));
		}

		public static void End()
		{
			Trace.WriteLine("Done");
		}
	}
}
