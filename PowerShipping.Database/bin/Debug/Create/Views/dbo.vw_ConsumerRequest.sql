SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_ConsumerRequest]
AS
SELECT	cr.[Id], cr.[AccountNumber], cr.[AccountName],
		cr.[Address1], cr.[Address2], cr.[City], cr.[State], cr.[ZipCode], cr.[Email], cr.[Phone1], cr.[Phone2],
		cr.[Method], cr.[AnalysisDate], cr.[IsOkayToContact], cr.[OperatingCompany], cr.[WaterHeaterFuel],
		cr.[IsReship], cr.[DoNotShip], cr.[ReceiptDate], cr.[AuditFailureDate],
		cr.[BatchId], cr.[Notes], cr.[Status], crb.[ImportedDate], crb.[ImportedBy], cr.[OutOfState], crb.BatchLabel

FROM	[ConsumerRequest] cr
		LEFT JOIN [ConsumerRequestBatch] crb
		ON	cr.[BatchId] = crb.[Id]
GO
