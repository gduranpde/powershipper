SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_TrackingNumberException]
AS
SELECT	tne.[Id], tne.[PurchaseOrderNumber], tne.[TrackingNumber], tne.[AccountNumber], tne.[AccountName],
		tne.[Address1], tne.[Address2], tne.[City], tne.[State], tne.[ZipCode], tne.[Reference],
		tne.[BatchId], tnb.[ImportedBy], tnb.[ImportedTime], 
		tne.[Reason], tne.[Status], tnes.[Name] [StatusName]
FROM	[TrackingNumberException] tne
		INNER JOIN [TrackingNumberExceptionStatus] tnes
		ON	tnes.[Id] = tne.[Status]
		LEFT JOIN [TrackingNumberBatch] tnb
		ON	tnb.[Id] = tne.[BatchId]
GO
