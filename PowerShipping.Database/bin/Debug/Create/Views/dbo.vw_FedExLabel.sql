SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_FedExLabel]
AS
SELECT	sd.[ShipmentId], sd.[SortOrder],
		cr.[AccountNumber] [RecipientId],
		cr.[AccountName] [Contact],
		'' [Company],
		cr.[Address1] [Address1],
		cr.[Address2] [Address2],
		cr.[City] [City],
		cr.[State] [State],
		'US' [CountryCode],
		cr.[ZipCode] [Zip],
		CASE 
			WHEN ISNULL(cr.[Phone1], '') = ''
			THEN ''
			ELSE 
				CASE
					WHEN LEN(cr.[Phone1]) = 10
					THEN
						SUBSTRING(cr.[Phone1], 1, 3) + '-' +
						SUBSTRING(cr.[Phone1], 4, 3) + '-' +
						SUBSTRING(cr.[Phone1], 7, 4)
					ELSE cr.[Phone1]
				END
		END [Phone],
		'SmartPost' [Service],
		'Y' [Residential],
		CONVERT(NVARCHAR, kt.[Weight]) [Weight1],
		'' [Weight2],
		'' [Weight3],
		'' [Weight4],
		'' [Weight5],
		CONVERT(NVARCHAR(50), sd.[ShipmentNumber]) [Reference],
		CASE 
			WHEN ISNULL(cr.[Phone2], '') = '' 
			THEN ''
			ELSE 'Alt Phone ' + 
				CASE
					WHEN LEN(cr.[Phone2]) = 10
					THEN
						SUBSTRING(cr.[Phone2], 1, 3) + '-' +
						SUBSTRING(cr.[Phone2], 4, 3) + '-' +
						SUBSTRING(cr.[Phone2], 7, 4)
					ELSE cr.[Phone2]
				END
		END [Reference2],
		'' [Reference3],		
		'' [Reference4],
		'' [Reference5],
		'' [InvoiceNumber],
		s.[PurchaseOrderNumber],
		d.[ConsumerCustomerServicePhoneNumber] [DepartmentNotes]
FROM	[ShipmentDetail] sd
		INNER JOIN [Shipment] s
			INNER JOIN [KitType] kt
			ON	kt.[Id] = s.[KitTypeId]
		ON	s.[Id] = sd.[ShipmentId]
		INNER JOIN [ConsumerRequest] cr
		ON	cr.[Id] = sd.[ConsumerRequestId]
		CROSS JOIN [FedExShippingDefault] d
GO


