SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_OptOutException_Save]
@Id					UNIQUEIDENTIFIER,
@OptOutDate			NVARCHAR(50),
@AccountNumber		NVARCHAR(50),
@AccountName		NVARCHAR(100),
@Address			NVARCHAR(200),
@City				NVARCHAR(50),
@State				NVARCHAR(50),
@ZipCode			NVARCHAR(5),
@Email				NVARCHAR(50),
@Phone1				NVARCHAR(50),
@Phone2				NVARCHAR(50),
@Notes				NVARCHAR(1024),
@IsEnergyProgram	NVARCHAR(50),
@IsTotalDesignation	NVARCHAR(50),
@BatchId			UNIQUEIDENTIFIER,
@Reason				NVARCHAR(1024),
@Status				INT
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT 1 FROM [OptOutException] WHERE [Id] = @Id)
	BEGIN

		UPDATE	[OptOutException]
		SET		[OptOutDate] = @OptOutDate,
				[AccountNumber] = @AccountNumber,
				[AccountName] = @AccountName,
				[Address] = @Address,
				[City] = @City,
				[State] = @State,
				[ZipCode] = @ZipCode,
				[Email] = @Email,
				[Phone1] = @Phone1,
				[Phone2] = @Phone2,
				[Notes] = @Notes,
				[IsEnergyProgram] = @IsEnergyProgram,
				[IsTotalDesignation] = @IsTotalDesignation,
				[BatchId] = @BatchId,
				[Reason] = @Reason,
				[Status] = @Status
		WHERE	[Id] = @Id

	END ELSE
	BEGIN
	
		INSERT	[OptOutException](
				[Id], [OptOutDate], [AccountNumber], [AccountName], 
				[Address], [City], [State], [ZipCode], [Email], [Phone1], [Phone2], [Notes],
				[IsEnergyProgram], [IsTotalDesignation],
				[BatchId], [Reason], [Status])
		SELECT	@Id, @OptOutDate, @AccountNumber, @AccountName,
				@Address, @City, @State, @ZipCode, @Email, @Phone1, @Phone2, @Notes,
				@IsEnergyProgram, @IsTotalDesignation,
				@BatchId, @Reason, @Status

	END
END
GO
