SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Shipment_GetById]
@Id	UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	[Id], [KitTypeId], [KitTypeName],
			[PurchaseOrderNumber], [DateLabelsNeeded], [DateCustomerWillShip],
			[CreatedBy], [CreatedTime]
	FROM	[vw_Shipment]
	WHERE	[Id] = @Id
END
GO