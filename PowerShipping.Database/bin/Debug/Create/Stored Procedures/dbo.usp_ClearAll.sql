SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ClearAll]
AS
BEGIN
	SET NOCOUNT ON;
    
    DELETE FROM [TrackingStatusException]
    
    DELETE FROM [TrackingNumberException]
    DELETE FROM [TrackingNumberBatch]
    
	DELETE FROM [ShipmentDetail]
	DELETE FROM [Shipment]

	DELETE FROM [OptOutException]
	DELETE FROM [OptOut]
	DELETE FROM [OptOutBatch]

	DELETE FROM [ConsumerRequestException]
	DELETE FROM [ConsumerRequestChange]
	DELETE FROM [ConsumerRequest]
	DELETE FROM [ConsumerRequestBatch]
END
GO
