SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ConsumerRequest_GetAll]
@ShowDeleted	BIT,
@AccountNumber NVARCHAR(50) = NULL,
@AccountName NVARCHAR(100) = NULL,
@Phone NVARCHAR(50) = NULL,
@Address NVARCHAR(200) = NULL,
@Status INT = NULL,
@BatchId UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SET NOCOUNT ON;
    
	SELECT	cr.[Id], cr.[AccountNumber], cr.[AccountName], 
			cr.[Address1], cr.[Address2],cr.[City], cr.[State], cr.[ZipCode], cr.[Email], cr.[Phone1], cr.[Phone2],
			cr.[Method], cr.[AnalysisDate], cr.[IsOkayToContact], cr.[OperatingCompany], cr.[WaterHeaterFuel],
			cr.[IsReship], cr.[DoNotShip], cr.[ReceiptDate], cr.[AuditFailureDate],
			cr.[BatchId], cr.[Notes], cr.[Status],
			cr.[ImportedDate], cr.[ImportedBy], cr.[OutOfState]
	FROM	[vw_ConsumerRequest] cr
	WHERE	(@ShowDeleted = 1 OR
			@ShowDeleted = 0 AND cr.[Status] <> 7)
			AND
			(@AccountNumber IS NULL OR cr.[AccountNumber] LIKE '%'+ @AccountNumber +'%' )
			AND
			(@AccountName IS NULL OR cr.[AccountName] LIKE '%'+ @AccountName +'%')
			AND
			(@Phone IS NULL OR cr.[Phone1] LIKE '%'+ @Phone +'%' OR cr.[Phone2] LIKE '%'+ @Phone +'%')
			AND
			(@Address IS NULL OR cr.[Address1] LIKE '%'+ @Address +'%' OR cr.[Address2] LIKE '%'+ @Address + '%')
			AND
			(@Status IS NULL OR cr.[Status] = @Status)
			AND
			(@BatchId IS NULL OR cr.[BatchId] = @BatchId)
END
GO
