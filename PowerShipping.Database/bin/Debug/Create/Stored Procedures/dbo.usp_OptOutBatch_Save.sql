SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_OptOutBatch_Save]
@Id						UNIQUEIDENTIFIER,
@FileName				NVARCHAR(100),
@ImportedBy				NVARCHAR(50),
@ImportedDate			DATETIME2(7),
@OptOutsInBatch			INT,
@ImportedSuccessfully	INT,
@ImportedWithException	INT
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT 1 FROM [OptOutBatch] WHERE [Id] = @Id)
	BEGIN

		UPDATE	[OptOutBatch]
		SET		[FileName] = @FileName,
				[ImportedBy] = @ImportedBy,
				[ImportedDate] = @ImportedDate,
				[OptOutsInBatch] = @OptOutsInBatch,
				[ImportedSuccessfully] = @ImportedSuccessfully,
				[ImportedWithException] = @ImportedWithException
		WHERE	[Id] = @Id

	END ELSE
	BEGIN
	
		INSERT	[OptOutBatch](
				[Id], [FileName], [ImportedBy], [ImportedDate],
				[OptOutsInBatch], [ImportedSuccessfully], [ImportedWithException])
		SELECT	@Id, @FileName, @ImportedBy, @ImportedDate,
				@OptOutsInBatch, @ImportedSuccessfully, @ImportedWithException

	END
END
GO
