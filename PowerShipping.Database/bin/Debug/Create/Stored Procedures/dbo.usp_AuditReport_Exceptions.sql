﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AuditReport_Exceptions]
@PONumber NVARCHAR(50) = NULL,
@StartDate DATETIME2 = NULL,
@EndDate DATETIME2 = NULL,
@KitType UNIQUEIDENTIFIER = null
AS
BEGIN
	SET NOCOUNT ON;
    
	SELECT	cr.*, sd.KitTypeName,sd.PurchaseOrderNumber, 'Cancelled' AS TransactionType
	FROM	[vw_ConsumerRequest] cr
	INNER JOIN dbo.vw_ShipmentDetail sd ON sd.ConsumerRequestId = cr.Id
	WHERE	
			
			(@PONumber IS NULL OR sd.PurchaseOrderNumber LIKE '%'+ @PONumber +'%' )
			AND			
			(@StartDate IS NULL OR AuditFailureDate >= @StartDate)
			AND
			(@EndDate IS NULL OR AuditFailureDate < @EndDate)
			AND
			(@KitType IS NULL OR sd.KitTypeId = @KitType)
			AND 
			ShippingStatus = 4
			AND
			cr.[Status] = 5
END
GO