SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_KitType_Save]
@Id						UNIQUEIDENTIFIER,
@Width					FLOAT,
@Height					FLOAT,
@Length					FLOAT,
@Weight					FLOAT,
@KitName				NVARCHAR(50),
@KitContents			NVARCHAR(200)
AS
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT 1 FROM [KitType] WHERE [Id] = @Id)
	BEGIN
		UPDATE	[KitType]
		SET		[Width] = @Width,
				[Height] = @Height,
				[Length] = @Length,
				[Weight] = @Weight,
				[KitName] = @KitName,
				[KitContents] = @KitContents
		WHERE	[Id] = @Id
	END ELSE
	BEGIN
		INSERT	[KitType](
				[Id], [Width], [Height], [Length], [Weight], [KitName], [KitContents])
		SELECT	@Id, @Width, @Height, @Length, @Weight, @KitName, @KitContents
	END
END
GO