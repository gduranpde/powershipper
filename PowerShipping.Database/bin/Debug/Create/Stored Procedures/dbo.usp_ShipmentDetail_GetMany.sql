﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ShipmentDetail_GetMany]
@StatusReship INT = NULL,
@StartDate DATETIME2 = NULL,
@EndDate DATETIME2 = NULL,
@AccountNumber NVARCHAR(50) = NULL,
@AccountName NVARCHAR(100) = NULL,
@Phone NVARCHAR(50) = NULL,
@Address NVARCHAR(200) = NULL,
@ShippingStatus INT = NULL,
@StartShipDate DATETIME2 = NULL,
@EndShipDate DATETIME2 = NULL,
@FedExTrackingNumber NVARCHAR(50) = NULL,
@FedExBatchId UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SET NOCOUNT ON;
    
	SELECT	[Id], 
			[ShipmentId],
			[ConsumerRequestId],
				
			[KitTypeId], [KitTypeName], 
				
			[AccountNumber], [AccountName], [Address1], [Address2],
			[City], [State], [ZipCode], [Email], [Phone1], [Phone2], 
			
			[PurchaseOrderNumber], 
			[ShipmentNumber], [SortOrder],
			[HasException], [IsReship], [ShipDate],
			[ReshipStatus], [ReshipStatusName],
			[ShippingStatus], [ShippingStatusName],
			[ShippingStatusDate],
				
			[FedExBatchId], [FedExTrackingNumber], 
			[FedExStatusDate], [FedExStatusCode], [FedExStatusHistory],
			[FedExLastUpdateStatus],
			
			[Notes]
				
	FROM	[vw_ShipmentDetail]
	WHERE	
			(@ShippingStatus IS NULL OR ShippingStatus = @ShippingStatus)
			AND
			(@StatusReship IS NULL OR ReshipStatus = @StatusReship)
			AND
			(@StartDate IS NULL OR ShippingStatusDate >= @StartDate)
			AND
			(@EndDate IS NULL OR ShippingStatusDate <= @EndDate)
			AND
			(@AccountNumber IS NULL OR [AccountNumber] LIKE '%'+ @AccountNumber +'%' )
			AND
			(@AccountName IS NULL OR [AccountName] LIKE '%'+ @AccountName +'%')
			AND
			(@Phone IS NULL OR [Phone1] LIKE '%'+ @Phone +'%' OR [Phone2] LIKE '%'+ @Phone +'%')
			AND
			(@Address IS NULL OR [Address1] LIKE '%'+ @Address +'%' OR [Address2] LIKE '%'+ @Address +'%')
			AND
			(@StartShipDate IS NULL OR ShipDate >= @StartShipDate)
			AND
			(@EndShipDate IS NULL OR ShipDate < @EndShipDate)
			AND
			(@FedExTrackingNumber IS NULL OR FedExTrackingNumber LIKE '%'+ @FedExTrackingNumber +'%' )
			AND
			(@FedExBatchId IS NULL OR FedExBatchId = @FedExBatchId)
END
GO
