SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_OptOutException_GetMany]
@ShowDeleted	BIT = 0,
@AccountNumber NVARCHAR(50) = NULL,
@AccountName NVARCHAR(100) = NULL,
@Phone NVARCHAR(50) = NULL,
@Address NVARCHAR(200) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	[Id], 
			[OptOutDate], [AccountNumber], [AccountName],
			[Address], [City], [State], [ZipCode], [Email], [Phone1], [Phone2],
			[Notes], [IsEnergyProgram], [IsTotalDesignation],
			[BatchId], [ImportedBy], [ImportedDate],
			[Reason], [Status]
	FROM	[vw_OptOutException]
	WHERE	(@ShowDeleted = 1 OR
			@ShowDeleted = 0 AND [Status] <> 1)
			AND
			(@AccountNumber IS NULL OR [AccountNumber] LIKE '%'+ @AccountNumber +'%' )
			AND
			(@AccountName IS NULL OR [AccountName] LIKE '%'+ @AccountName +'%')
			AND
			(@Phone IS NULL OR [Phone1] LIKE '%'+ @Phone +'%' OR [Phone2] LIKE '%'+ @Phone +'%')
			AND
			(@Address IS NULL OR [Address] LIKE '%'+ @Address +'%')
END
GO
