﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ShipmentDetail_GetByTrackingNumber]
@FedExTrackingNumber	NVARCHAR(50)=NULL,
@AccountName	NVARCHAR(100)=NULL,
@AccountNumber	NVARCHAR(50)=NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT		[Id], 
				[ShipmentId],
				[ConsumerRequestId],
				
				[KitTypeId], [KitTypeName], 
				
				[AccountNumber], [AccountName], [Address1], [Address2],
				[City], [State], [ZipCode], [Email], [Phone1], [Phone2], 
			
				[PurchaseOrderNumber], 
				[ShipmentNumber], [SortOrder],
				[HasException], [IsReship], [ShipDate],
				[ReshipStatus], [ReshipStatusName],
				[ShippingStatus], [ShippingStatusName],
				[ShippingStatusDate],
				
				[FedExBatchId], [FedExTrackingNumber], 
				[FedExStatusDate], [FedExStatusCode], [FedExStatusHistory],
				[FedExLastUpdateStatus],
				
				[Notes]
				
	FROM		[vw_ShipmentDetail]
	WHERE		
				(@FedExTrackingNumber IS NULL OR RIGHT([FedExTrackingNumber],20) LIKE '%'+ @FedExTrackingNumber +'%' )
				AND
				(@AccountName IS NULL OR AccountName LIKE '%'+ @AccountName +'%' )
				AND
				(@AccountNumber IS NULL OR AccountNumber LIKE '%'+ @AccountNumber +'%' )
				AND
				[ShippingStatus] IN (1, 2, 3)
END
GO