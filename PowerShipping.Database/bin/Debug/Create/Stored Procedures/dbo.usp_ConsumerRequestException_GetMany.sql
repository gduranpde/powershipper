SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ConsumerRequestException_GetMany]
@ShowDeleted bit,
@AccountNumber NVARCHAR(50) = NULL,
@AccountName NVARCHAR(100) = NULL,
@Phone NVARCHAR(50) = NULL,
@Address NVARCHAR(200) = NULL,
@BatchId UNIQUEIDENTIFIER = NULL,
@BatchLabel NVARCHAR(20) = NULL,
@StartDate DATETIME2 = NULL
AS
BEGIN
	SET NOCOUNT ON;
  
		SELECT	[Id], [AccountNumber], [AccountName], 
				[Address1], [Address2], [City], [State], [ZipCode], [Email], [Phone1], [Phone2],
				[Method], [AnalysisDate], [IsOkayToContact], [OperatingCompany], [WaterHeaterFuel],
				[BatchId], [Status], [ImportedDate], [ImportedBy], 
				[Reason], [Notes], [OutOfState], [BatchLabel]
		FROM	[vw_ConsumerRequestException]
		WHERE
		(@ShowDeleted = 1 OR (@ShowDeleted = 0 AND [Status] <> 1))
		AND
			(@AccountNumber IS NULL OR [AccountNumber] LIKE '%'+ @AccountNumber +'%' )
			AND
			(@AccountName IS NULL OR [AccountName] LIKE '%'+ @AccountName +'%')
			AND
			(@Phone IS NULL OR [Phone1] LIKE '%'+ @Phone +'%' OR [Phone2] LIKE '%'+ @Phone +'%')
			AND
			(@Address IS NULL OR [Address1] LIKE '%'+ @Address +'%' OR [Address2] LIKE '%'+ @Address +'%')
			AND
			(@BatchId IS NULL OR [BatchId] = @BatchId)
			AND
			(@BatchLabel IS NULL OR [BatchLabel] LIKE '%'+ @BatchLabel +'%')
			AND
			(@StartDate IS NULL OR (ImportedDate >= @StartDate AND ImportedDate < DATEADD(DAY,1,@StartDate) ))
END
GO
