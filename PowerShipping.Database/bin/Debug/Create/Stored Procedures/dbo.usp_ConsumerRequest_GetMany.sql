SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ConsumerRequest_GetMany]
AS
BEGIN
	SET NOCOUNT ON;
    
	SELECT	cr.[Id], cr.[AccountNumber], cr.[AccountName], 
			cr.[Address1], cr.[Address2], cr.[City], cr.[State], cr.[ZipCode], cr.[Email], cr.[Phone1], cr.[Phone2],
			cr.[Method], cr.[AnalysisDate], cr.[IsOkayToContact], cr.[OperatingCompany], cr.[WaterHeaterFuel],
			cr.[IsReship], cr.[DoNotShip], cr.[ReceiptDate], cr.[AuditFailureDate],
			cr.[BatchId], cr.[Notes], cr.[Status],
			cr.[ImportedDate], cr.[ImportedBy], cr.[OutOfState]
	FROM	[vw_ConsumerRequest] cr
	WHERE	cr.[Status] = 0 AND
			cr.[DoNotShip] = 0 AND
			NOT EXISTS(	SELECT	1
						FROM	[OptOut] o
						WHERE	o.[AccountNumber] = cr.[AccountNumber])
END
GO
