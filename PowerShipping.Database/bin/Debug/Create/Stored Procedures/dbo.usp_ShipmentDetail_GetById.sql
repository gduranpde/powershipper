SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ShipmentDetail_GetById]
@Id	UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	[Id], [ShipmentId],
			[ConsumerRequestId],
				
			[KitTypeId], [KitTypeName], 
				
			[AccountNumber], [AccountName], [Address1], [Address2],
			[City], [State], [ZipCode], [Email], [Phone1], [Phone2], 
			
			[PurchaseOrderNumber], 
			[ShipmentNumber], [SortOrder],
			[HasException], [IsReship], [ShipDate],
			[ReshipStatus], [ReshipStatusName],
			[ShippingStatus], [ShippingStatusName],
			[ShippingStatusDate],
			
			[FedExBatchId], [FedExTrackingNumber], 
			[FedExStatusDate], [FedExStatusCode], [FedExStatusHistory],
			[FedExLastUpdateStatus],
			
			[Notes]
			
	FROM	[vw_ShipmentDetail]
	WHERE	[Id] = @Id
END
GO

