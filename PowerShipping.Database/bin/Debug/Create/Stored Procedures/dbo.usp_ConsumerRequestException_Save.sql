SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ConsumerRequestException_Save]
@Id					UNIQUEIDENTIFIER,
@AccountNumber		NVARCHAR(50),
@AccountName		NVARCHAR(100),
@Address1			NVARCHAR(200),
@Address2			NVARCHAR(200),
@City				NVARCHAR(50),
@State				NVARCHAR(50),
@ZipCode			NVARCHAR(5),
@Email				NVARCHAR(50),
@Phone1				NVARCHAR(50),
@Phone2				NVARCHAR(50),
@Method				NVARCHAR(100),
@AnalysisDate		NVARCHAR(50),
@IsOkayToContact	NVARCHAR(50),
@OperatingCompany	NVARCHAR(30),
@WaterHeaterFuel	NVARCHAR(30),
@BatchId			UNIQUEIDENTIFIER,
@Reason				NVARCHAR(1024),
@Status				INT,
@Notes				NVARCHAR(1024),
@OutOfState			BIT = 0
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT 1 FROM [ConsumerRequestException] WHERE [Id] = @Id)
	BEGIN

		UPDATE	[ConsumerRequestException]
		SET		[AccountNumber] = @AccountNumber,
				[AccountName] = @AccountName,
				[Address1] = @Address1,
				[Address2] = @Address2,
				[City] = @City,
				[State] = @State,
				[ZipCode] = @ZipCode,
				[Email] = @Email,
				[Phone1] = @Phone1,
				[Phone2] = @Phone2,
				[Method] = @Method,
				[AnalysisDate] = @AnalysisDate,
				[IsOkayToContact] = @IsOkayToContact,
				[OperatingCompany] = @OperatingCompany,
				[WaterHeaterFuel] = @WaterHeaterFuel,
				[BatchId] = @BatchId,
				[Reason] = @Reason,
				[Status] = @Status,
				[Notes] = @Notes,
				[OutOfState] = @OutOfState
		WHERE	[Id] = @Id

	END ELSE
	BEGIN
	
		INSERT	[ConsumerRequestException](
				[Id], [AccountNumber], [AccountName], 
				[Address1], [Address2], [City], [State], [ZipCode], [Email], [Phone1], [Phone2],
				[Method], [AnalysisDate], [IsOkayToContact], [OperatingCompany], [WaterHeaterFuel],
				[BatchId], [Reason], [Status], [Notes],[OutOfState])
		SELECT	@Id, @AccountNumber, @AccountName,
				@Address1, @Address2, @City, @State, @ZipCode, @Email, @Phone1, @Phone2,
				@Method, @AnalysisDate, @IsOkayToContact, @OperatingCompany, @WaterHeaterFuel,
				@BatchId, @Reason, @Status, @Notes, @OutOfState

	END
END

GO