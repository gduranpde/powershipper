SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TrackingNumberException_GetById]
@Id	UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	[Id], [PurchaseOrderNumber], [TrackingNumber], [AccountNumber], [AccountName],
			[Address1], [Address2], [City], [State], [ZipCode], [Reference],
			[BatchId], [ImportedBy], [ImportedTime],
			[Reason], [Status], [StatusName]
	FROM	[vw_TrackingNumberException]
	WHERE	[Id] = @Id
END
GO
