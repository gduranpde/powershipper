﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Shipment_GetMany]
@StartDate DATETIME2 = NULL,
@EndDate DATETIME2 = NULL,
@PONumber NVARCHAR(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	*
	FROM	[vw_Shipment]
	WHERE	
			(@PONumber IS NULL OR [PurchaseOrderNumber] LIKE '%'+ @PONumber +'%' )
			AND			
			(@StartDate IS NULL OR [CreatedTime] >= @StartDate)
			AND
			(@EndDate IS NULL OR [CreatedTime] < @EndDate)
END
GO
