SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_OptOut_Save]
@Id					UNIQUEIDENTIFIER,
@OptOutDate			DATETIME2(7),
@AccountNumber		NVARCHAR(50),
@AccountName		NVARCHAR(100),
@Address			NVARCHAR(200),
@City				NVARCHAR(50),
@State				NVARCHAR(50),
@ZipCode			NVARCHAR(5),
@Email				NVARCHAR(50),
@Phone1				NVARCHAR(50),
@Phone2				NVARCHAR(50),
@IsEnergyProgram	BIT,
@IsTotalDesignation	BIT,
@BatchId			UNIQUEIDENTIFIER,
@Notes				NVARCHAR(1024)
AS
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT 1 FROM [OptOut] WHERE [Id] = @Id)
	BEGIN
				
		UPDATE	[OptOut]
		SET		[OptOutDate] = @OptOutDate,
				[AccountNumber] = @AccountNumber,
				[AccountName] = @AccountName,
				[Address] = @Address,
				[City] = @City,
				[State] = @State,
				[ZipCode] = @ZipCode,
				[Email] = @Email,
				[Phone1] = @Phone1,
				[Phone2] = @Phone2,
				[IsEnergyProgram] = @IsEnergyProgram,
				[IsTotalDesignation] = @IsTotalDesignation,
				[BatchId] = @BatchId,
				[Notes] = @Notes
		WHERE	[Id] = @Id
		
	END ELSE
	BEGIN

		INSERT	[OptOut](
				[Id], [OptOutDate], [AccountNumber], [AccountName], 
				[Address], [City], [State], [ZipCode], [Email], [Phone1], [Phone2],
				[IsEnergyProgram], [IsTotalDesignation], 
				[BatchId], [Notes])
		SELECT	@Id, @OptOutDate, @AccountNumber, @AccountName,
				@Address, @City, @State, @ZipCode, @Email, @Phone1, @Phone2,
				@IsEnergyProgram, @IsTotalDesignation,
				@BatchId, @Notes
	END
END
GO
