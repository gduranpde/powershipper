INSERT [dbo].[ShipmentShippingStatus] ([Id], [Name]) VALUES (1, N'Pending')
INSERT [dbo].[ShipmentShippingStatus] ([Id], [Name]) VALUES (2, N'In Transit')
INSERT [dbo].[ShipmentShippingStatus] ([Id], [Name]) VALUES (3, N'Delivered')
INSERT [dbo].[ShipmentShippingStatus] ([Id], [Name]) VALUES (4, N'Returned')
GO
