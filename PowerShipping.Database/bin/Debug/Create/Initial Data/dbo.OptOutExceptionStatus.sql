INSERT [dbo].[OptOutExceptionStatus] ([Id], [Name]) VALUES (0, N'Normal')
INSERT [dbo].[OptOutExceptionStatus] ([Id], [Name]) VALUES (1, N'Deleted')
INSERT [dbo].[OptOutExceptionStatus] ([Id], [Name]) VALUES (2, N'Dead')
GO
