INSERT [dbo].[ShipmentReshipStatus] ([Id], [Name]) VALUES (0, N'N/A')
INSERT [dbo].[ShipmentReshipStatus] ([Id], [Name]) VALUES (1, N'First Energy')
INSERT [dbo].[ShipmentReshipStatus] ([Id], [Name]) VALUES (2, N'PDM')
INSERT [dbo].[ShipmentReshipStatus] ([Id], [Name]) VALUES (3, N'Dead')
INSERT [dbo].[ShipmentReshipStatus] ([Id], [Name]) VALUES (4, N'Reshipped')
GO