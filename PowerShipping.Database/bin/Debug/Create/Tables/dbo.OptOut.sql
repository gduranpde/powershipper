SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OptOut](

	[Id] [uniqueidentifier] NOT NULL,
	[OptOutNumber] [bigint] IDENTITY(1000000000,1) NOT NULL,
	[OptOutDate] [datetime2](7) NOT NULL,
	[AccountNumber] [nvarchar](50) NOT NULL,
	[AccountName] [nvarchar](100) NULL,
	[Address] [nvarchar](200) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](5) NULL,
	[Email] [nvarchar](50) NULL,
	[Phone1] [nvarchar](50) NULL,
	[Phone2] [nvarchar](50) NULL,
	[IsEnergyProgram] [bit] NOT NULL,
	[IsTotalDesignation] [bit] NOT NULL,
	[BatchId] [uniqueidentifier] NULL,
	[Notes] [nvarchar](1024) NULL,

	CONSTRAINT [PK_OptOut] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],

	CONSTRAINT [IX_OptOut_AccountNumber] UNIQUE NONCLUSTERED ( [AccountNumber] ASC )
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

) ON [PRIMARY]
GO
