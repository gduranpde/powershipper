SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OptOutException](

	[Id] [uniqueidentifier] NOT NULL,
	[OptOutDate] [nvarchar](50) NULL,
	[AccountNumber] [nvarchar](50) NULL,
	[AccountName] [nvarchar](100) NULL,
	[Address] [nvarchar](200) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](5) NULL,
	[Email] [nvarchar](100) NULL,
	[Phone1] [nvarchar](50) NULL,
	[Phone2] [nvarchar](50) NULL,
	[Notes] [nvarchar](1024) NULL,
	[IsEnergyProgram] [nvarchar](50) NULL,
	[IsTotalDesignation] [nvarchar](50) NULL,
	[BatchId] [uniqueidentifier] NULL,
	[Reason] [nvarchar](1024) NOT NULL,
	[Status] [int] NOT NULL,

	CONSTRAINT [PK_OptOutException] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

) ON [PRIMARY]
GO
