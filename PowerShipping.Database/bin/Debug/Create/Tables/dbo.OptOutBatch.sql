SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OptOutBatch](

	[Id] [uniqueidentifier] NOT NULL,
	[FileName] [nvarchar](100) NOT NULL,
	[ImportedBy] [nvarchar](50) NOT NULL,
	[ImportedDate] [datetime2](7) NOT NULL,
	[OptOutsInBatch] [int] NOT NULL,
	[ImportedSuccessfully] [int] NOT NULL,
	[ImportedWithException] [int] NOT NULL,

	CONSTRAINT [PK_OptOutBatch] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

) ON [PRIMARY]
GO
