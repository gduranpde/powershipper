SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrackingNumberException](

	[Id] [uniqueidentifier] NOT NULL,
	[PurchaseOrderNumber] [nvarchar](50) NULL,
	[TrackingNumber] [nvarchar](50) NULL,
	[AccountNumber] [nvarchar](50) NULL,
	[AccountName] [nvarchar](100) NULL,
	[Address1] [nvarchar](200) NULL,
	[Address2] [nvarchar](200) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](50) NULL,
	[Reference] [nvarchar](100) NULL,
	[BatchId] [uniqueidentifier] NULL,
	[Reason] [nvarchar](4000) NOT NULL,
	[Status] [int] NOT NULL,

	CONSTRAINT [PK_TrackingNumberException] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

) ON [PRIMARY]
GO
