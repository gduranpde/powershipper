SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KitType](

	[Id] [uniqueidentifier] NOT NULL,
	[Width] [float] NOT NULL,
	[Height] [float] NOT NULL,
	[Length] [float] NOT NULL,
	[Weight] [float] NOT NULL,
	[KitName] [nvarchar](50) NOT NULL,
	[KitContents] [nvarchar](200) NOT NULL,
	
	CONSTRAINT [PK_KitType] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

) ON [PRIMARY]
GO
