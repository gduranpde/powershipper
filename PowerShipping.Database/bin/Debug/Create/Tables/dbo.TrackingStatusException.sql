SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrackingStatusException](

	[Id] [uniqueidentifier] NOT NULL,
	[ExceptionDate] [datetime2](7) NOT NULL,
	[TrackingNumber] [nvarchar](50) NOT NULL,
	[IsCleared] [bit] NOT NULL,
	[ClearedBy] [nvarchar](50) NULL,
	[CrearedDate] [datetime2](7) NULL,
	[Notes] [nvarchar](1024) NULL,
	
	CONSTRAINT [PK_ShipmentException] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	
) ON [PRIMARY]
GO
