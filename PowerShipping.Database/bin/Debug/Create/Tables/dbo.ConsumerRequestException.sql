SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConsumerRequestException](

	[Id] [uniqueidentifier] NOT NULL,
	[AccountNumber] [nvarchar](50) NULL,
	[AccountName] [nvarchar](100) NULL,
	[Address1] [nvarchar](200) NULL,
	[Address2] [nvarchar](200) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](5) NULL,
	[Email] [nvarchar](100) NULL,
	[Phone1] [nvarchar](50) NULL,
	[Phone2] [nvarchar](50) NULL,
	[Method] [nvarchar](100) NULL,
	[AnalysisDate] [nvarchar](50) NULL,
	[IsOkayToContact] [nvarchar](50) NULL,
	[OperatingCompany] [nvarchar](30) NULL,
	[WaterHeaterFuel] [nvarchar](30) NULL,
	[BatchId] [uniqueidentifier] NULL,
	[Reason] [nvarchar](1024) NOT NULL,
	[Status] [int] NOT NULL,
	[Notes] [nvarchar](1024) NULL,
	[OutOfState] [bit] NOT NULL,
	CONSTRAINT [PK_CustomerRequestException] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

) ON [PRIMARY]
GO


ALTER TABLE [dbo].[ConsumerRequestException]  WITH CHECK ADD  CONSTRAINT [FK_ConsumerRequestException_ConsumerRequestExceptionStatus] FOREIGN KEY([Status])
REFERENCES [dbo].[ConsumerRequestExceptionStatus] ([Id])
GO

ALTER TABLE [dbo].[ConsumerRequestException] CHECK CONSTRAINT [FK_ConsumerRequestException_ConsumerRequestExceptionStatus]
GO

ALTER TABLE [dbo].[ConsumerRequestException] ADD  CONSTRAINT [DF_ConsumerRequestException_OutOfState]  DEFAULT ((0)) FOR [OutOfState]
GO