ALTER TABLE [dbo].[TrackingNumberException] WITH CHECK
	ADD CONSTRAINT [FK_TrackingNumberException_TrackingNumberExceptionStatus] FOREIGN KEY([Status])
	REFERENCES [dbo].[TrackingNumberExceptionStatus] ([Id])
GO
ALTER TABLE [dbo].[TrackingNumberException] CHECK CONSTRAINT [FK_TrackingNumberException_TrackingNumberExceptionStatus]
GO
