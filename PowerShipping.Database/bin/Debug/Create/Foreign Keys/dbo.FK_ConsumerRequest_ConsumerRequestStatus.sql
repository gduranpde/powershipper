ALTER TABLE [dbo].[ConsumerRequest] WITH CHECK
	ADD CONSTRAINT [FK_ConsumerRequest_ConsumerRequestStatus] FOREIGN KEY([Status])
	REFERENCES [dbo].[ConsumerRequestStatus] ([Id])
GO
ALTER TABLE [dbo].[ConsumerRequest] CHECK CONSTRAINT [FK_ConsumerRequest_ConsumerRequestStatus]
GO
