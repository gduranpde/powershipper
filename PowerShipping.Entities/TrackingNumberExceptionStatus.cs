namespace PowerShipping.Entities
{
	public enum TrackingNumberExceptionStatus
	{
		Normal = 0,
		Deleted = 1
	}
}