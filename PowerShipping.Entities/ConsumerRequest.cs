﻿using System;
using System.Collections.Generic;

namespace PowerShipping.Entities
{
    public class ConsumerRequest
    {
        public Guid Id { get; set; }

        public string AccountNumber { get; set; }

        public string AccountName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string ZipCode { get; set; }

        public string Email { get; set; }

        public string Phone1 { get; set; }

        public string Phone2 { get; set; }

        public string Method { get; set; }

        public DateTime? AnalysisDate { get; set; }

        public bool? IsOkayToContact { get; set; }

        public string OperatingCompany { get; set; }

        public string WaterHeaterFuel { get; set; }

        public bool IsReship { get; set; }

        public bool DoNotShip { get; set; }

        public DateTime? ReceiptDate { get; set; }

        public DateTime? AuditFailureDate { get; set; }

        public Guid? BatchId { get; set; }

        public string BatchLabel { get; set; }

        public string Notes { get; set; }

        public ConsumerRequestStatus Status { get; set; }

        public string ChangedBy { get; set; }

        public string StatusSort
        {
            get { return Status.ToString("F"); }
        }

        public bool OutOfState { get; set; }

        public List<State> States { get; set; }

        public Guid ProjectId { get; set; }

        public int Quantity { get; set; }

        public string PremiseID { get; set; }

        public string CompanyCode { get; set; }

        public string ProjectCode { get; set; }

        public bool OverrideDuplicates { get; set; }

        public string CompanyName { get; set; }

        public string ServiceAddress1 { get; set; }

        public string ServiceAddress2 { get; set; }

        public string ServiceCity { get; set; }

        public string ServiceState { get; set; }

        public string ServiceZip { get; set; }

        public DateTime? CRImportedDate { get; set; }

        //public DateTime? ImportedDate { get; set; }

        public string AccountNumberOld { get; set; }
        //PG31
        public string FacilityType { get; set; }

        public string RateCode { get; set; }

        public string IncomeQualified { get; set; }
        //PG31
        public string HeaterFuel { get; set; }

        public string RequestedKit { get; set; }
    }
}