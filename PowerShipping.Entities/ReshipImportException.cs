﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Entities
{
    public class ReshipImportException
    {
        public Guid ReshipExceptionImportID { get; set; }

        public Guid ReshipImportBatchID { get; set; }

        public string AccountNumber { get; set; }

        public Int64 ShipmentNumber { get; set; }

        public string TrackingNumber { get; set; }

        public string ReasonForException { get; set; }

        public Guid KitTypeID { get; set; }

        public Guid ProjectID { get; set; }

        public Guid ShipmentID { get; set; }

        public int ReshipImportBatchName { get; set; }

        public Guid ShipperID { get; set; }

        public string ImportedBy { get; set; }
        public DateTime ImportedTime { get; set; }

        //private string _ImportedDate;
        //public string ImportedDate
        //{

        //    get { return _ImportedDate; }
        //    set { _ImportedDate = value; }
        //}

        public string ShipperName { get; set; }
     
       
    }
}