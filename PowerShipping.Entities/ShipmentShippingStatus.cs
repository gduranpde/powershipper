﻿namespace PowerShipping.Entities
{
	public enum ShipmentShippingStatus
	{
		Pending = 1,
		InTransit = 2,
		Delivered = 3,
		Returned = 4
	}
}
