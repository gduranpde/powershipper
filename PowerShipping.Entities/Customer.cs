using System;
using System.Text;
using System.Collections.Generic;


namespace PowerShipping.Entities
{
    public class Customer
    {
          
        public Guid Id { get; set; }
        

        public Guid ProjectId { get; set; }
        

        public string AccountNumber { get; set; }
        

        public string CompanyName { get; set; }
        

        public string ContactName { get; set; }
        

        public string Email1 { get; set; }
        

        public string Email2 { get; set; }
        

        public string Phone1 { get; set; }
        

        public string Phone2 { get; set; }
        

        public string MailingAddress1 { get; set; }
        

        public string MailingAddress2 { get; set; }
        

        public string MailingCity { get; set; }
        

        public string MailingState { get; set; }
        

        public string MailingZip { get; set; }
        

        public string ServiceAddress1 { get; set; }
        

        public string ServiceAddress2 { get; set; }
        

        public string ServiceCity { get; set; }
        

        public string ServiceState { get; set; }
        

        public string ServiceZip { get; set; }
        

        public string OperatingCompany { get; set; }
        

        public string WaterHeaterType { get; set; }
        

        public string PremiseID { get; set; }
        

        public bool? OkToContact { get; set; }
        

        public bool? OptOut { get; set; }
        

        public string InvitationCode { get; set; }

        public string FacilityType { get; set; }
        public string RateCode { get; set; }
        public string IncomeQualified { get; set; }

        public string CompanyCode { get; set; }
        public string ProjectCode { get; set; }

    }
}