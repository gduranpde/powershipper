﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Entities
{
    public class Item
    {
        public Guid ItemID { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }

        public decimal DefaultKwImpact { get; set; }
        public decimal DefaultKwhImpact { get; set; }

        public bool IsActive { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }
}
