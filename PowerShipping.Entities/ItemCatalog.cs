﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Entities
{
    public class ItemCatalog
    {
        public Guid ItemCatalogID { get; set; }
        public Guid ItemID { get; set; }
        public Guid ProjectID { get; set; }
        public string CatalogID { get; set; }

    }
}
