﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Entities
{
	public enum ConsumerRequestExceptionStatus
	{
		RequestReceived = 0,
		Deleted = 1,
        Exception = 3,
        OptOut = 4
	}
}
