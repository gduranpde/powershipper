﻿using System;

namespace PowerShipping.Entities
{
	public class TrackingNumberBatch
	{
		public Guid Id { get; set; }

		public string FileName { get; set; }
		public string ImportedBy { get; set; }
		public DateTime ImportedTime { get; set; }

		public int NumbersInBatch { get; set; }
		public int ImportedSuccessfully { get; set; }
		public int ImportedWithException { get; set; }
	}
}
