﻿using System;
namespace PowerShipping.Entities
{
    [Serializable()]
    public class State
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}