using System;

namespace PowerShipping.Entities
{
    public class Reason
    {
          
        public Guid ReasonID { get; set; }
        

        public string ReasonCode { get; set; }
        

        public string ReasonDescription { get; set; }

        public string ReasonFull
        { 
            get { return ReasonCode + " - " + ReasonDescription; }
        }
    }
}