﻿using System;

namespace PowerShipping.Entities
{
	public class TrackingNumberException
	{
		public Guid Id { get; set; }

		public string PurchaseOrderNumber { get; set; }
		public string TrackingNumber { get; set; }
		public string AccountNumber { get; set; }
		public string AccountName { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string ZipCode { get; set; }
		public string Reference { get; set; }

		public Guid? BatchId { get; set; }
		public string ImportedBy { get; set; }
		public DateTime ImportedTime { get; set; }
		
		public string Reason { get; set; }
		public string StatusName { get; set; }
		public TrackingNumberExceptionStatus Status { get; set; }

        public Guid? ProjectId { get; set; }

        public Guid? ShipperId { get; set; }

        public string CompanyCode { get; set; }
        public string ProjectCode { get; set; }
        public string ShipperCode { get; set; }
	}
}
