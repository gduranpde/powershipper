﻿using System;

namespace PowerShipping.Entities
{
    [Serializable]
    public class ShipmentDetail
    {
        public Guid Id { get; set; }

        public Guid ShipmentId { get; set; }

        public string PurchaseOrderNumber { get; set; }

        public Guid KitTypeId { get; set; }

        public string KitTypeName { get; set; }

        public Guid ConsumerRequestId { get; set; }

        public string AccountNumber { get; set; }

        public string ShipmentNumber { get; set; }

        public int SortOrder { get; set; }

        public bool HasException { get; set; }

        public bool IsReship { get; set; }

        public DateTime? ShipDate { get; set; }

        public ShipmentReshipStatus ReshipStatus { get; set; }

        public ShipmentShippingStatus ShippingStatus { get; set; }

        public string ReshipStatusName { get; set; }

        public bool ReturnsAllowed { get; set; }

        public string ShippingStatusName { get; set; }

        public DateTime ShippingStatusDate { get; set; }

        public Guid? FedExBatchId { get; set; }

        public string FedExTrackingNumber { get; set; }

        public string FedExStatusCode { get; set; }

        public DateTime? FedExStatusDate { get; set; }

        public string FedExStatusHistory { get; set; }

        public string FedExLastUpdateStatus { get; set; }

        public string Notes { get; set; }

        public string AccountName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string ZipCode { get; set; }

        public string Email { get; set; }

        public string Phone1 { get; set; }

        public string Phone2 { get; set; }

        public string CompanyName { get; set; }

        public string ServiceAddress1 { get; set; }

        public string ServiceAddress2 { get; set; }

        public string ServiceCity { get; set; }

        public string ServiceState { get; set; }

        public string ServiceZip { get; set; }

        public string ReshipStatusSort
        {
            get { return ReshipStatus.ToString("F"); }
        }

        public string ShippingStatusSort
        {
            get { return ShippingStatus.ToString("F"); }
        }

        public Guid ProjectId { get; set; }

        public Guid ShipperId { get; set; }

        public string CompanyCode { get; set; }

        public string ProjectCode { get; set; }

        public string ShipperCode { get; set; }

        public string PackageTrackingUrl { get; set; }

        public Guid? ReasonId { get; set; }

        public string ReasonCode { get; set; }

        public string ReasonDescription { get; set; }

        public string ShipmentDetailAddress1 { get; set; }

        public string ShipmentDetailAddress2 { get; set; }

        public string ShipmentDetailCity { get; set; }

        public string ShipmentDetailState { get; set; }

        public string ShipmentDetailZip { get; set; }

        public string OperatingCompany { get; set; }

        public DateTime? ReturnDate { get; set; }
    }
}