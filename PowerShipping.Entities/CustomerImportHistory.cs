using System;
using System.Text;
using System.Collections.Generic;


namespace PowerShipping.Entities
{
    public class CustomerImportHistory
    {
          
        public Guid Id { get; set; }
        

        public Guid ProjectId { get; set; }
        

        public string FileName { get; set; }
        

        public DateTime ImportDate { get; set; }
        

        public int CountRecords { get; set; }
        

        public Guid UserId { get; set; }


        public string UploadType { get; set; }


        public int ExceptionCount { get; set; }


        public string UserName { get; set; }

        public string ProjectName { get; set; }

        public string ProjectCode { get; set; }

        
        
    }
}