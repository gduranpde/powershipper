﻿using System;
using System.Collections.Generic;
using System.Web.Security;

namespace PowerShipping.Entities
{
    public class PDMRoles
    {
        public static readonly string ROLE_Administrator = "Administrator";
        public static readonly string ROLE_User = "PDE";
        public static readonly string ROLE_Customer = "Customer";
        public static readonly string ROLE_CallCenter = "CallCenter";

        public static string[] Roles
        {
            get
            {
                return new string[] { ROLE_Administrator, ROLE_User, ROLE_Customer, ROLE_CallCenter };
            }
        }
    }

    public class InternalUser : MembershipUser
    {
        protected string _UserName;

        public override string UserName
        {
            get
            {
                return _UserName;
            }
        }
    }

    public class User : InternalUser
    {
        public static List<User> GetUsersList(MembershipUserCollection coll)
        {
            List<User> ret = new List<User>();

            foreach (MembershipUser u in coll)
            {
                User user = new User(u);

                ret.Add(user);
            }
            return ret;
        }

        public void SetPassword(string pwd)
        {
            string old = this.ResetPassword();
            this.ChangePassword(old, pwd);
        }

        public User()
        {
        }

        public User(MembershipUser user)
        {
            this.UserName = user.UserName;
            this.UserID = (Guid)user.ProviderUserKey;
            this.Email = user.Email;
            this.IsApproved = user.IsApproved;
            this.Comment = user.Comment;
            this.LastLoginDate = user.LastLoginDate;
            this.LastActivityDate = user.LastActivityDate;
        }

        public override string ProviderName
        {
            get
            {
                return Membership.Provider.Name;
            }
        }

        public Guid UserID { get; set; }

        public override object ProviderUserKey
        {
            get
            {
                return (object)this.UserID;
            }
        }

        public string Password
        {
            get;
            set;
        }

        public new string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {
                _UserName = value;
            }
        }

        //public bool IsAdmin
        //{
        //    get
        //    {
        //        if (System.Web.Security.Roles.IsUserInRole(UserName, PDMRoles.ROLE_Administrator))
        //            return true;
        //        return false;
        //    }
        //    set
        //    {
        //        if(value)
        //            Roles = new string[]{ PDMRoles.ROLE_Administrator};
        //        else
        //            Roles = new string[] { PDMRoles.ROLE_User };

        //    }
        //}

        public Company Company { get; set; }

        public List<Project> Projects { get; set; }

        private string _RolesString = null;

        public string RolesString
        {
            get
            {
                return string.Join(",", Roles);
            }
        }

        List<string> RolesCollection = null;

        public string[] Roles
        {
            get
            {
                if (string.IsNullOrEmpty(UserName))
                {
                    return new string[0];
                }
                if (RolesCollection == null)
                {
                    RolesCollection = new List<string>();
                    RolesCollection.AddRange(System.Web.Security.Roles.GetRolesForUser(UserName));
                }

                return RolesCollection.ToArray();
            }
            set
            {
                RolesCollection = new List<string>();
                RolesCollection.AddRange(value);
            }
        }

        public bool HasRole(string role)
        {
            if (RolesCollection == null)
                return false;

            return RolesCollection.Contains(role);
        }

        public void SetRoles(string[] roles)
        {
            foreach (string role in PDMRoles.Roles)
            {
                if (System.Web.Security.Roles.IsUserInRole(UserName, role))
                    System.Web.Security.Roles.RemoveUserFromRole(UserName, role);
            }
            if (roles != null && roles.Length > 0)
                System.Web.Security.Roles.AddUserToRoles(UserName, roles);
        }

        public void SetRoles()
        {
            SetRoles(Roles);
        }
    }
}