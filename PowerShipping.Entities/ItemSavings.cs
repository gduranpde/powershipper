﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Entities
{
    public class ItemSavings
    {
        public Guid ItemSavingsID { get; set; }

        public Guid ItemID { get; set; }
        public Guid ProjectID { get; set; }
        public Guid OperatingCompanyID { get; set; }

        public decimal? KwImpact { get; set; }
        public decimal? KwhImpact { get; set; }

        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        
        public string Project { get; set; }
        public string CatalogID { get; set; }
        public string OperatingCompanyName { get; set; }
    }
}
