﻿namespace PowerShipping.Entities
{
	public class FedExShippingDefault
	{
        public string ShipperFedExAccountNumber { get; set; }
        public string ShipperCompanyName { get; set; }
        public string ShipperPhysicalAddress { get; set; }
        public string ShipperContactName { get; set; }
        public string ShipperContactPhone { get; set; }
        public string PackagePickupLocation { get; set; }
        public string MoreThanOnePackageForEachRecipient { get; set; }
        public string FedExServiceType { get; set; }
        public string DomesticOrInternational { get; set; }
        public string AnyPackagesToResidentialAddresses { get; set; }
        public string SignatureRequired { get; set; }
        public string BulkLabelSupportSpecialHandling { get; set; }
        public string PackageType { get; set; }
        public string DeclaredValueAmount { get; set; }
        public string PaymentType { get; set; }
        public string FedExAccountNumberForBilling { get; set; }
        public string DeliveryServiceForLabels { get; set; }
        public string AddressToSendLabels { get; set; }
        public string FedExBulkLabelEmailAddress { get; set; }
        public string OtherCcEmailAddresses  { get; set; }
        public string ConsumerCustomerServicePhoneNumber { get; set; }
        public string PdmShippingExceptionEmailAddress { get; set; }
	}
}
