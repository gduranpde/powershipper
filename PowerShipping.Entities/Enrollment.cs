using System;
using System.Text;
using System.Collections.Generic;


namespace PowerShipping.Entities
{
    public class Enrollment
    {

        public Int64 EnrollmentID { get; set; }

        public string SubmittedDate { get; set; }

        public string InvitationCode { get; set; }

        public string AccountNumber { get; set; }

        public string AccountNumber12 { get; set; }

        public string ContactFirstName { get; set; }

        public string ContactLastName { get; set; }

        public string ContactEmail { get; set; }

        public string ContactPhone { get; set; }

        public string ContactZipCode { get; set; }

        public string ShipToAddress { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public string ReferralSource { get; set; }

        public string RateCode { get; set; }

        public string IncomeQualified { get; set; }

        public string Quantity { get; set; }
     
        public string HeaterFuel { get; set; }

        public string MoreInfor { get; set; }

        public string MoreInfo { get; set; }

        public string RequestedKit { get; set; }

        public string OperatingCompany { get; set; }

        public string FacilityType { get; set; }

        public string ContactName { get; set; }

        public string Phone2 { get; set; }

        public string Phone1 { get; set; }

        public string OkToContact { get; set; }

        public string OpCo { get; set; }

        public string WaterHeater { get; set; }

        public string PremiseID { get; set; }

        public string CompanyName { get; set; }

        public string ServiceAddress1 { get; set; }

        public string ServiceAddress2 { get; set; }

        public string ServiceCity { get; set; }

        public string ServiceState { get; set; }

        public string ServiceZip { get; set; }

        public string Method { get; set; }

        public string OldAccountNumber { get; set; }

        public string AccountNumberResult { get; set; }
    }
}