﻿using System;

namespace PowerShipping.Entities
{
    [Serializable]
	public class Shipment
	{
		public Guid Id { get; set; }

		public Guid KitTypeId { get; set; }
		public string KitTypeName { get; set; }

		public string PurchaseOrderNumber { get; set; }

		public DateTime DateLabelsNeeded { get; set; }
		public DateTime DateCustomerWillShip { get; set;}

		public string CreatedBy { get; set; }
		public DateTime CreatedTime { get; set;}

        public Guid ProjectId { get; set; }
        public Guid ShipperId { get; set; }

        public string CompanyCode { get; set; }
        public string ProjectCode { get; set; }
        public string ShipperCode { get; set; }

        public int ShipmentDetailsCount { get; set; }

	}
}
