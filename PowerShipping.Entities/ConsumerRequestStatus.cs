﻿namespace PowerShipping.Entities
{
	public enum ConsumerRequestStatus
	{
		RequestReceived = 0,
		PendingShipment = 1,
		InTransit = 2,
		Delivered = 3,
		Returned = 4,
		NonDeliverableOrDead = 5,
		OptOut = 6,
		Deleted = 7,
		NeedResearch = 8,
		Exception = 9
	}
}
