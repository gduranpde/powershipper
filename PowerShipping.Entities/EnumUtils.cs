﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

namespace PowerShipping.Entities
{
    public class EnumUtils
    {
        public static string GetPhoneDigits(string s)
        {
            if (s == null)
                return string.Empty;

            string ret = string.Empty;
            var re = new Regex(@"\d+");
            MatchCollection matches = re.Matches(s);
            foreach (object matche in matches)
            {
                ret = ret + matche;
            }
            return ret;
        }

        public static List<T> EnumToList<T>()
        {
            Type enumType = typeof(T);

            // Can't use type constraints on value types, so have to do check like this
            if (enumType.BaseType != typeof(Enum))
                throw new ArgumentException("T must be of type System.Enum");

            Array enumValArray = Enum.GetValues(enumType);

            var enumValList = new List<T>(enumValArray.Length);

            foreach (int val in enumValArray)
            {
                enumValList.Add((T)Enum.Parse(enumType, val.ToString()));
            }

            return enumValList;
        }

        public static List<ListItem> EnumToListItemsWithNull<T>()
        {
            Type enumType = typeof(T);

            // Can't use type constraints on value types, so have to do check like this
            if (enumType.BaseType != typeof(Enum))
                throw new ArgumentException("T must be of type System.Enum");

            Array enumValArray = Enum.GetValues(enumType);

            var enumValList = new List<ListItem>();

            enumValList.Add(new ListItem("", null));

            foreach (object val in enumValArray)
            {
                enumValList.Add(new ListItem(Enum.GetName(enumType, val), val.ToString()));
            }

            //for (int i = 0; i < enumValArray.Length; i++ )
            //{
            //    enumValList.Add(new ListItem(enumNameArray[i], enumValArray[i].ToString()));
            //}

            return enumValList;
        }

        public static EnumListItem[] EnumToListItemsWithNull(Type tEnum)
        {
            var val = new List<Enum>();
            val.Add(null);
            foreach (Enum en in Enum.GetValues(tEnum))
            {
                val.Add(en);
            }
            return EnumToListItems(val.ToArray());
        }

        //public static EnumListItem[] EnumToListItems(Type tEnum)
        //{
        //    var val = new List<Enum>();
        //    foreach (Enum en in Enum.GetValues(tEnum))
        //    {
        //        val.Add(en);
        //    }
        //    return EnumToListItems(val.ToArray());
        //}

        //public static EnumListItem[] EnumToListItems(params Enum[] values)
        //{
        //    var ret = new List<EnumListItem>();

        //    //Comparison<Enum> comp = Desc.CompareByDesc;
        //    //Array.Sort(values, comp);

        //    foreach (Enum n in values)
        //    {
        //        EnumListItem item = EnumToListItem(n);

        //        if (item != null)
        //            ret.Add(item);
        //    }


        //    return ret.ToArray();
        //}

        //public static EnumListItem[] EnumToListItemsNonSorted(Type tEnum)
        //{
        //    var val = new List<Enum>();
        //    foreach (Enum en in Enum.GetValues(tEnum))
        //    {
        //        val.Add(en);
        //    }
        //    return EnumToListItemsNonSorted(val.ToArray());
        //}

        //public static EnumListItem[] EnumToListItemsNonSorted(params Enum[] values)
        //{
        //    var ret = new List<EnumListItem>();

        //    //Comparison<Enum> comp = Desc.CompareByDesc;
        //    //Array.Sort(values, comp);

        //    foreach (Enum n in values)
        //    {
        //        EnumListItem item = EnumToListItem(n);

        //        if (item != null)
        //            ret.Add(item);
        //    }
        //    return ret.ToArray();
        //}

        //protected static EnumListItem EnumToListItem(object e)
        //{
        //    try
        //    {
        //        string text = GetEnumDesc(e);

        //        return new EnumListItem(text, Convert.ToInt32(e), string.Format("{0}", e));
        //    }
        //    catch
        //    {
        //    }
        //    return null;
        //}

        //public static string GetEnumDesc(object e)
        //{
        //    if (e == null)
        //        return "";

        //    return e.ToString();
        //}


        public static List<EnumListItem> EnumToListWithNull(Type tEnum)
        {
            var val = new List<Enum>();
            val.Add(null);
            foreach (Enum en in Enum.GetValues(tEnum))
            {
                val.Add(en);
            }
            return EnumToListEnums(val.ToArray());
        }

        public static List<EnumListItem> EnumToListEnums(params Enum[] values)
        {
            var ret = new List<EnumListItem>();

            //Comparison<Enum> comp = Desc.CompareByDesc;
            //Array.Sort(values, comp);

            foreach (Enum n in values)
            {
                EnumListItem item = EnumToListItem(n);

                if (item != null)
                    ret.Add(item);
            }


            return ret;
        }



        //public static TAttr GetEnumAttribute<TAttr>(object e) where TAttr : Attribute
        //{

        //    Type tEnum = e.GetType();

        //    MemberInfo[] info = tEnum.GetMember(Enum.GetName(tEnum, e));

        //    if (info != null && info.Length > 0)
        //    {
        //        return (TAttr)Attribute.GetCustomAttribute(info[0], typeof(TAttr));
        //    }

        //    return default(TAttr);
        //}
        //public static string GetEnumDesc(object e)
        //{
        //    if (e == null)
        //        return "";

        //    Desc d = GetEnumAttribute<Desc>(e);
        //    if (d != null)
        //        return d.Text;

        //    return e.ToString();
        //}




        public static EnumListItem[] EnumToListItemsNonSorted(Type tEnum)
        {
            List<Enum> val = new List<Enum>();
            foreach (Enum en in Enum.GetValues(tEnum))
            {
                val.Add(en);
            }
            return EnumToListItemsNonSorted(val.ToArray());
        }

        public static EnumListItem[] EnumToListItemsNonSorted(params Enum[] values)
        {

            var ret = new List<EnumListItem>();

            //Comparison<Enum> comp = Desc.CompareByDesc;
            //Array.Sort(values, comp);

            foreach (Enum n in values)
            {
                EnumListItem item = EnumToListItem(n);

                if (item != null)
                    ret.Add(item);
            }
            return ret.ToArray();
        }

        public static EnumListItem[] EnumToListItems(Type tEnum)
        {
            List<Enum> val = new List<Enum>();
            foreach (Enum en in Enum.GetValues(tEnum))
            {
                val.Add(en);
            }
            return EnumToListItems(val.ToArray());
        }

        public static EnumListItem[] EnumToListItems(params Enum[] values)
        {

            var ret = new List<EnumListItem>();

            Comparison<Enum> comp = Desc.CompareByDesc;
            Array.Sort(values, comp);

            foreach (Enum n in values)
            {
                EnumListItem item = EnumToListItem(n);

                if (item != null)
                    ret.Add(item);
            }


            return ret.ToArray();
        }

        public static EnumListItem[] ListItemsConcat(EnumListItem[] values, params EnumListItem[] addValues)
        {
            List<EnumListItem> ret = new List<EnumListItem>(addValues);
            ret.AddRange(values);

            return ret.ToArray();
        }

        public static TEnum StringToEnum<TEnum>(string s)
        {
            try
            {
                Type t = Nullable.GetUnderlyingType(typeof(TEnum));
                if (t == null)
                    t = typeof(TEnum);


                return (TEnum)Enum.Parse(t, s);
            }
            catch
            {
                return default(TEnum);
            }
        }

        public static string EnumToString(object value)
        {
            if (value == null)
                return null;
            return string.Format("{0}", Convert.ToInt32(value));
        }





        public static TAttr GetEnumAttribute<TAttr>(object e) where TAttr : Attribute
        {

            Type tEnum = e.GetType();

            MemberInfo[] info = tEnum.GetMember(Enum.GetName(tEnum, e));

            if (info != null && info.Length > 0)
            {
                return (TAttr)Attribute.GetCustomAttribute(info[0], typeof(TAttr));
            }

            return default(TAttr);
        }
        public static string GetEnumDesc(object e)
        {
            if (e == null)
                return "";

            Desc d = GetEnumAttribute<Desc>(e);
            if (d != null)
                return d.Text;

            return e.ToString();
        }

        protected static EnumListItem EnumToListItem(object e)
        {
            try
            {
                string text = GetEnumDesc(e);

                return new EnumListItem(text, Convert.ToInt32(e), string.Format("{0}", e));
            }
            catch
            {
            }
            return null;
        }
    }
    
    public class Desc : Attribute
    {
        public int Order = -1;
        public string Text;


        public Desc()
        {
        }

        public Desc(string text)
        {
            Text = text;
        }

        public Desc(string text, int order)
        {
            Text = text;
            Order = order;
        }

        public Desc(int order)
        {
            Order = order;
        }

        public static int CompareByDesc(Enum a, Enum b)
        {
            Desc oa = null;
            if(a != null)
                oa = EnumUtils.GetEnumAttribute<Desc>(a);
            Desc ob = null;
            if (b != null)
                ob = EnumUtils.GetEnumAttribute<Desc>(b);

            if (oa == null && ob == null)
            {
                //return a.CompareTo(b);
                return 0;
            }

            if (oa == null)
                return -1;

            if (ob == null)
                return 1;

            if (oa.Order == ob.Order && ob.Order == -1)
                return string.Compare(oa.Text, ob.Text);

            return oa.Order - ob.Order;
        }
    }

    public class EnumListItem
    {
        public EnumListItem()
        {
        }

        public EnumListItem(string text, int? value)
        {
            Text = text;
            Value = value;
        }

        public EnumListItem(string text, int? value, string enumName)
        {
            Text = text;
            Value = value;
            EnumName = enumName;
        }

        public int? Value { get; set; }

        public string Text { get; set; }

        public string EnumName { get; set; }
    }
}
