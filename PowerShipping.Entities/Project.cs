﻿using System;
using System.Collections.Generic;

namespace PowerShipping.Entities
{
    public class Project
    {
        public Guid Id { get; set; }

        public string ProjectName { get; set; }

        public string ProjectCode { get; set; }

        public Guid CompanyId { get; set; }

        public string CompanyName { get; set; }

        public string CompanyCode { get; set; }

        public bool IsAllowDuplicates { get; set; }

        public bool ReturnsAllowed { get; set; }

        public bool IsActive { get; set; }

        public bool IsAllowDupsInProject { get; set; }

        public List<State> States { get; set; }

        public List<Project> ExceptionProjects { get; set; }

        public string FullName
        {
            get
            {
                if (!string.IsNullOrEmpty(ProjectCode))
                    return CompanyCode + " - " + ProjectCode + " - " + ProjectName;
                return ProjectName;
            }
        }

        public string ProgramID { get; set; }

        public bool DuplicateExceptions { get; set; }

        public List<Project> RemovedExceptionProjects { get; set; }

        public List<Project> AddedExceptionProjects { get; set; }

        public int StateId { get; set; }

        public string StateName { get; set; }
    }
}