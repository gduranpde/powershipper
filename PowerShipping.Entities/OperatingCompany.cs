﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Entities
{
    public class OperatingCompany
    {

        public Guid OperatingCompanyID { get; set; }
        public string OperatingCompanyCode { get; set; }
        public string OperatingCompanyName { get; set; } 
        public Guid ParentCompanyID { get; set; }
        public bool IsActive { get; set; }
        public int ServiceState { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }

        public string CompanyName { get; set; }
        public string StateName { get; set; }
    }
}
