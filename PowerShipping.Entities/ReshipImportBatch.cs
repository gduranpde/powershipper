﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Entities
{
    public class ReshipImportBatch
    {
        public Guid ReshipImportBatchID { get; set; }

        public int ReshipImportBatchName { get; set; }

        public Guid ShipperID { get; set; }

        public int NumberOfRecords { get; set; }

        public int ImportedSuccessfully { get; set; }

        public int NumberWithExceptions { get; set; }

        public string SourceFileName { get; set; }

        public string SourceFilePath { get; set; }

        public string ImportedBy { get; set; }

        public DateTime ImportedTime { get; set; }
     

        public int ImportedWithExceptions { get; set; }
        //public DateTime ImportedDate { get; set; }

        //private string _ImportedDate;
        //public string ImportedDate
        //{

        //    get { return _ImportedDate; }
        //    set { _ImportedDate = value; }
        //}

        public string ShipperName { get; set; }
    }
}