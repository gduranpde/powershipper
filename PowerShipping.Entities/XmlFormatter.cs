﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
namespace PowerShipping.Entities
{
   public static class XmlFormatter
    {

        public static XNamespace XmlNamespace = XNamespace.Get("http://www.w3.org/1999/xhtml");
        public const string programNumber = "programNumber";
        public const string refID = "refID";
        public const string customerFirstname = "customerFirstname";
        public const string customerLastname = "customerLastname";
        public const string customerAccountNumber = "customerAccountNumber";
        public const string primaryContactId = "primaryContactId";
        public const string premiseContactId = "premiseContactId";
        public const string status = "status";
        public const string applicationDate = "applicationDate";

        public const string contactRefID = "contactRefID";
        public const string contactType = "contactType";
        public const string firstName = "firstName";
        public const string lastName = "lastName";
        public const string company = "company";
        public const string address = "address";
        public const string addressCont = "addressCont";
        public const string city = "city";
        public const string state = "state";
        public const string zip = "zip";
        public const string accountNumber = "accountNumber";
        public const string phone = "phone";
        public const string cell = "cell";
        public const string email = "email";

        public const string AnalysisDate = "AnalysisDate";
        public const string ReceiptDate = "ReceiptDate";
        public const string Operating_Company = "Operating_Company";
        public const string Water_Heater_Fuel = "Water_Heater_Fuel";
        public const string Heater_Fuel = "Heater_Fuel";
        public const string OK_to_Contact = "OK_to_Contact";
        public const string Facility_Type = "Facility_Type";
        public const string Exception_Date = "Exception_Date";
        public const string Shipment_Date = "Shipment_Date";
        public const string Rate_Code = "Rate_Code";
        public const string Enrollment_Completion_Date = "Enrollment_Completion_Date";
        public const string Income_Qualified = "Income_Qualified";
        public const string Number_Kits_Shipped = "Number_Kits_Shipped";
        public const string Month_Shipped = "Month_Shipped";
        public const string Kwh_Savings = "Kwh_Savings";
        public const string Kw_savings = "Kw_savings";

        public const string equipmentRefId = "equipmentRefId";
        public const string Description = "Description";
        public const string CatalogID = "CatalogID";
        public const string Date_installed = "Date_installed";
        public const string Quantity = "Quantity";
        public const string Kw_Impact = "Kw_Impact";
        public const string Kwh_Impact = "Kwh_Impact";


    }
}
