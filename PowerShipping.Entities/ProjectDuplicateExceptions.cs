﻿using System;
using System.Collections.Generic;

namespace PowerShipping.Entities
{
	public class ProjectDuplicateExceptions
	{
        public Guid ProjectDuplicateExceptionID { get; set; }

        public Guid MainProjectID { get; set; }

        public Guid ExceptionProjectID { get; set; }
	}
}
