﻿using System;

namespace PowerShipping.Entities
{
	public class OptOutBatch
	{
		public Guid Id { get; set; }
		public string FileName { get; set; }
		public string ImportedBy { get; set; }
		public DateTime ImportedDate { get; set; }

		public int OptOutsInBatch { get; set; }
		public int ImportedSuccessfully { get; set; }
		public int ImportedWithException { get; set; }
        public Guid ProjectId { get; set; }

        public string CompanyCode { get; set; }
        public string ProjectCode { get; set; }
	}
}
