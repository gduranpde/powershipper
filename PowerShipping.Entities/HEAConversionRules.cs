﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Entities
{
    public class HEAConversionRules
    {
        // METHOD
        public string GetMethod(string ACLARA, string CBA) // Column#8, Column#9
        {
            string method = string.Empty;

            if (ACLARA == "Y" && CBA == "Y")
            {
                method = "ACLARA & CBA";
            }
            else if (ACLARA == "Y")
            {
                method = "ACLARA";
            }
            else if (CBA == "Y")
            {
                method = "CBA";
            }
            else
            {
                method = "";
            }

            return method;
        }

        // ANALYSIS DATE
        public string GetAnalysisDate(string LEVEL_1_DATE, string LEVEL_2_DATE, string LEVEL_3_DATE)
        {
            string analysisDate = string.Empty;

            if (LEVEL_1_DATE != "")
            {
                analysisDate = LEVEL_1_DATE;
            }
            else if (LEVEL_2_DATE != "")
            {
                analysisDate = LEVEL_2_DATE;
            }
            else if (LEVEL_3_DATE != "")
            {
                analysisDate = LEVEL_3_DATE;
            }
            else
            {
                analysisDate = "";
            }
            if (analysisDate != "") //Modified 09/26/12 PS
            {
                DateTime dt = Convert.ToDateTime(analysisDate);

                analysisDate = dt.ToString("MM-dd-yyyy");
            }
           

            return analysisDate;
        }

        //ACCOUNT NUMBER
        public string GetAccountNumber(string CONTRACT_ACCT)
        {
            return CONTRACT_ACCT;
        }

        // ADDRESS 1, CITY , STATE, ZIP + SERVICE ADDRESS 1, SERVICE CITY , SERVICE STATE, SERVICE ZIP

        public string[] ParseCityStateZip(string Address)
        { //replacing comma with space
            Address = Address.Replace(",", " ");

            string[] CityStateZip = new string[3];

            string[] Temp = Address.Trim().Split(' ');

            int arrLen = Temp.Length;

            CityStateZip[0] = Temp[arrLen - 1]; //Zip
            CityStateZip[1] = Temp[arrLen - 2]; //State

            string City = string.Empty;

            for (int i = 0; i < arrLen - 2; i++)
            {
                City = City + " " + Temp[i];
            }

            CityStateZip[2] = City; // City

            return CityStateZip;
        }

        public Dictionary<string, string> GetAddress(string CA_ADDR4, string CA_ADDR5, string BP_STD_ADDR4, string BP_STD_ADDR5, string PREM_ADDR4, string PREM_ADDR5)
        {
            //string[] Address = new string[4];

            Dictionary<string, string> Address = new Dictionary<string, string>();

            //-----------------------------------------------------//

            string DESTINATION_ADDRESS = string.Empty, DESTINATION_CITY = string.Empty, DESTINATION_STATE = string.Empty, DESTINATION_ZIP = string.Empty;

            //-- PREFFERED ADDRESS ---//

            string CA_STATE = string.Empty, CA_CITY = string.Empty, CA_ZIP = string.Empty;

            if (CA_ADDR4 != "")
            {
                string[] address = ParseCityStateZip(CA_ADDR5);

                CA_ZIP = address[0];
                CA_STATE = address[1];
                CA_CITY = address[2];
            }

            //-- STANDARED ADDRESS --//

            string BP_STATE = string.Empty, BP_CITY = string.Empty, BP_ZIP = string.Empty;

            if (BP_STD_ADDR4 != "")
            {
                string[] address = ParseCityStateZip(BP_STD_ADDR5);

                BP_ZIP = address[0];
                BP_STATE = address[1];
                BP_CITY = address[2];
            }
            //--SERVICE ADDRESS--//

            string SERVICE_ADDRESS1 = string.Empty;
            string SERVICE_ADDRESS2 = string.Empty;
            string SERVICE_CITY = string.Empty;
            string SERVICE_STATE = string.Empty;
            string SERVICE_ZIP = string.Empty;
            //-- PREMISE ADDRESS --//

            string PREM_STATE = string.Empty, PREM_CITY = string.Empty, PREM_ZIP = string.Empty;

            if (PREM_ADDR4 != "")
            {
                string[] address = ParseCityStateZip(PREM_ADDR5);

                PREM_ZIP = address[0];
                PREM_STATE = address[1];
                PREM_CITY = address[2];
            }

            //-----------------------------------------------------//

            if (CA_ADDR4 != "" && CA_STATE == "PA")
            {
                DESTINATION_ADDRESS = CA_ADDR4;
                DESTINATION_CITY = CA_CITY;
                DESTINATION_STATE = CA_STATE;
                DESTINATION_ZIP = CA_ZIP;
                Service_Adress(PREM_ADDR4, ref SERVICE_ADDRESS1, ref SERVICE_CITY, ref SERVICE_STATE, ref SERVICE_ZIP, PREM_STATE, PREM_CITY, PREM_ZIP);
            }
            else if (BP_STD_ADDR4 != "" && BP_STATE == "PA")
            {
                DESTINATION_ADDRESS = BP_STD_ADDR4;
                DESTINATION_CITY = BP_CITY;
                DESTINATION_STATE = BP_STATE;
                DESTINATION_ZIP = BP_ZIP;
                Service_Adress(PREM_ADDR4, ref SERVICE_ADDRESS1, ref SERVICE_CITY, ref SERVICE_STATE, ref SERVICE_ZIP, PREM_STATE, PREM_CITY, PREM_ZIP);
            }
            else
            {
                DESTINATION_ADDRESS = PREM_ADDR4;
                DESTINATION_CITY = PREM_CITY;
                DESTINATION_STATE = PREM_STATE;
                DESTINATION_ZIP = PREM_ZIP;
            }
            Service_Adress(PREM_ADDR4, ref SERVICE_ADDRESS1, ref SERVICE_CITY, ref SERVICE_STATE, ref SERVICE_ZIP, PREM_STATE, PREM_CITY, PREM_ZIP);//Modified by PS 09/26/12
            Address.Add("ADDRESS", DESTINATION_ADDRESS);
            Address.Add("CITY", DESTINATION_CITY);
            Address.Add("STATE", DESTINATION_STATE);
            Address.Add("ZIP", DESTINATION_ZIP);
            Address.Add("SERVICE_ADDRESS1", SERVICE_ADDRESS1);
            Address.Add("SERVICE_CITY", SERVICE_CITY);
            Address.Add("SERVICE_STATE", SERVICE_STATE);
            Address.Add("SERVICE_ZIP", SERVICE_ZIP);
            return Address;
        }
        //Step #4 Assigning the premise address to the service address
        private static void Service_Adress(string PREM_ADDR4, ref string SERVICE_ADDRESS1, ref string SERVICE_CITY, ref string SERVICE_STATE, ref string SERVICE_ZIP, string PREM_STATE, string PREM_CITY, string PREM_ZIP)
        {
            SERVICE_ADDRESS1 = PREM_ADDR4;
            SERVICE_CITY = PREM_CITY;
            SERVICE_STATE = PREM_STATE;
            SERVICE_ZIP = PREM_ZIP;
        }
        public string OkToContact(string OK_TO_CONTACT)
        {
            string OkStatus = string.Empty;

            if (OK_TO_CONTACT == "Y")
            {
                OkStatus = "YES";
            }
            else if (OK_TO_CONTACT == "N")
            {
                OkStatus = "NO";
            }
            else
            {
                OkStatus = "";
            }

            return OkStatus;
        }
    }
}