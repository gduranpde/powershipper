namespace PowerShipping.Entities
{
	public enum OptOutExceptionStatus
	{
		Normal = 0,
		Deleted = 1,
		Dead = 2
	}
}