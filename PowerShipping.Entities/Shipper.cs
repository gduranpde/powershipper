﻿using System;

namespace PowerShipping.Entities
{
    public class Shipper
    {
        public Guid Id { get; set; }
        public string ShipperCode { get; set; }
        public string ShipperName { get; set; }
        public string PackageTrackingUrl { get; set; }
        public string TransactionFileUrl { get; set; }
        public string BatchTrackingCodeModule { get; set; }

        public string FullName
        {
            get
            {
                if (!string.IsNullOrEmpty(ShipperCode))
                    return ShipperCode + " - " + ShipperName;
                return ShipperName;
            }
        }
    }
}