﻿namespace PowerShipping.Entities
{
    public enum ShipmentReshipStatus
    {
        [Desc("N/A", 1)]
        NA = 0,
        [Desc("First Energy", 0)]
        FirstEnergy = 1,
        [Desc("PDE 1st Call", 2)]
        PDE1stCall = 2,
        [Desc("Dead", 3)]
        Dead = 3,
        [Desc("Reshipped", 7)]
        Reshipped = 4,
        [Desc("PDE 2nd Call", 4)]
        PDM2ndCall = 5,
        [Desc("PDE to Ship", 5)]
        PDEToShip = 6,
        [Desc("Ready for Reship", 6)]
        ReadyForReship = 7,
    }
}