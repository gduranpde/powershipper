﻿using System;
using System.Collections.Generic;

namespace PowerShipping.Entities
{
	public class KitType
	{
		public Guid Id { get; set; }

		public double Width { get; set; }
        public double Height { get; set; }
        public double Length { get; set; }
        public double Weight { get; set; }
        //PG31
        public double Kw_Savings { get; set; }
        public double Kwh_Savings { get; set; }
        //PG31
		public string KitName { get; set; }
		public string KitContents { get; set; }
       
        public string TextKitLabel { get { return string.Format("{0}({1})", KitName, Weight); } }

        public List<KitItem> KitItems { get; set; }
        public List<Item> Items { get; set; }
        public List<KitContent> KitContentsList { get; set; }
        public bool IsActive { get; set; }

        [Obsolete("Used in old version")]
	    public string KitItemsString
	    {
            get
            {
                if (KitItems == null)
                    return "";

                string res = "";
                foreach (KitItem ki in KitItems)
                {
                    res = res + ki.Name + ", ";
                }
                if (res.Length > 0)
                    res = res.Remove(res.Length-2,2);
                return res;
            }
	    }
	}
}
