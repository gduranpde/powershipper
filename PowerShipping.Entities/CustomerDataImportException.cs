﻿using System;

namespace PowerShipping.Entities
{
	public class CustomerDataImportException
	{
		public Guid CustomerDataImportExceptionId { get; set; }

		public Guid CustomerDataImportId { get; set; }

		public string RecordNumber { get; set; }

		public string Reason { get; set; }

		public string RecordData { get; set; }
	}
}
