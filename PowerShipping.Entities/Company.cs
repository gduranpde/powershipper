﻿using System;
using System.Collections.Generic;

namespace PowerShipping.Entities
{
    public class Company
    {
        public Guid Id { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyLogo { get; set; }
        public bool IsActive { get; set; }

        public List<Project> Projects { get; set; }

        public string FullName
        {
            get
            {
                if (!string.IsNullOrEmpty(CompanyCode))
                    return CompanyCode;
                return CompanyName;
            }
        }
    }
}