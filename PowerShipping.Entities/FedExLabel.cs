﻿namespace PowerShipping.Entities
{
	public class FedExLabel
	{
		public string RecipientId { get; set;}
		public string Contact { get; set; }
		public string Company { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string CountryCode { get; set; }
		public string Zip { get; set; }
		public string Phone { get; set; }
		public string Service { get; set; }
		public string Residential { get; set; }
		public string Weight1 { get; set; }
		public string Weight2 { get; set; }
		public string Weight3 { get; set; }
		public string Weight4 { get; set; }
		public string Weight5 { get; set; }
		public string Reference { get; set; }
		public string Reference2 { get; set; }
		public string Reference3 { get; set; }
		public string Reference4 { get; set; }
		public string Reference5 { get; set; }
		public string InvoiceNumber { get; set; }
		public string PurchaseOrderNumber { get; set; }
		public string DepartmentNotes { get; set; }
	}
}
