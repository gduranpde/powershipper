﻿using System;

namespace PowerShipping.Entities
{
	public class OptOutException
	{
		public Guid Id { get; set; }

		public string OptOutDate { get; set; }
		public string AccountNumber { get; set; }
		public string AccountName { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string ZipCode { get; set; }
		public string Email { get; set; }
		public string Phone1 { get; set; }
		public string Phone2 { get; set; }
		public string Notes { get; set; }

		public string IsEnergyProgram { get; set; }
		public string IsTotalDesignation { get; set; }
		
		public Guid? BatchId { get; set; }
		public string Reason { get; set; }
		public OptOutExceptionStatus Status { get; set; }
        public Guid ProjectId { get; set; }

        public string CompanyCode { get; set; }
        public string ProjectCode { get; set; }
	}
}
