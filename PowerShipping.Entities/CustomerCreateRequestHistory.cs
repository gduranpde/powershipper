using System;
using System.Configuration;
using System.IO;
using System.Text;
using System.Collections.Generic;


namespace PowerShipping.Entities
{
    public class CustomerCreateRequestHistory
    {
          
        public Guid Id { get; set; }
        

        public Guid ProjectId { get; set; }
        

        public DateTime UploadTime { get; set; }

        public string Source { get; set; }

        public string FileName { get; set; }
        

        public Guid UserId { get; set; }
        

        public int TotalRecords { get; set; }
        

        public int SuccessfullyUploaded { get; set; }
        

        public int UploadedWithExceptions { get; set; }


        public string UserName { get; set; }
        public string CompanyCode { get; set; }
        public string ProjectCode { get; set; }

        public string FileUploaded
        {
            get
            {
                string file = Id + "_Uploaded.csv";
                return ConfigurationManager.AppSettings["CustomerRequestFolder"] + file;
            }
        }
        public string FileExceptions
        {
            get
            {
                string file = Id + "_Exception.csv";
                return ConfigurationManager.AppSettings["CustomerRequestFolder"] + file;
            }
        }
        public string FileGood
        {
            get
            {
                string file = Id + "_Result.csv";
                return ConfigurationManager.AppSettings["CustomerRequestFolder"] + file;
            }
        }

        public string ProjectName { get; set; }
    }
}