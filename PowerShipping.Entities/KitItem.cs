﻿using System;

namespace PowerShipping.Entities
{
    public class KitItem
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string CatalogID { get; set; }
        public int Quantity { get; set; }
        public decimal Kw_Impact { get; set; }
        public decimal Kwh_Impact { get; set; }
    }
}