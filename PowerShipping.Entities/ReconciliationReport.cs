﻿using System;

namespace PowerShipping.Entities
{
    [Serializable]
    public class ReconciliationReport
	{
        public string ProjectCode { get; set; }
        public string OperatingCompanyCode { get; set; }
        public int NoPart { get; set; }
        public decimal MW { get; set; }
        public decimal MWH { get; set; }

	}
}
