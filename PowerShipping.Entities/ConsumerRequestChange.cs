﻿using System;

namespace PowerShipping.Entities
{
	public class ConsumerRequestChange
	{
		public Guid ConsumerRequestId { get; set; }
		
		public string ChangedBy { get; set; }
		public DateTime ChangedDate { get; set; }
		
		public string OldAccountNumber { get; set; }
		public string NewAccountNumber { get; set; }
		public string OldAccountName { get; set; }
		public string NewAccountName { get; set; }
		public string OldAddress1 { get; set; }
		public string NewAddress1 { get; set; }
		public string OldAddress2 { get; set; }
		public string NewAddress2 { get; set; }
		public string OldCity { get; set; }
		public string NewCity { get; set; }
		public string OldState { get; set; }
		public string NewState { get; set; }
		public string OldZipCode { get; set; }
		public string NewZipCode { get; set; }
		public string OldEmail { get; set; }
		public string NewEmail { get; set; }
		public string OldPhone1 { get; set; }
		public string NewPhone1 { get; set; }
		public string OldPhone2 { get; set; }
		public string NewPhone2 { get; set; }
	}
}
