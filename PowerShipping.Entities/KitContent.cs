﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Entities
{
    public class KitContent
    {
        public Guid KitContentID { get; set; }
        public Guid KitID { get; set; }
        public Guid ItemID { get; set; }
        public int Quantity { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public decimal DefaultKwImpact { get; set; }
        public decimal DefaultKwhImpact { get; set; }
    }
}
