﻿using System;
using System.Web.Security;

namespace PowerShipping.Entities
{
    [Serializable]
	public class Report
	{
		public int ID { get; set; }

		public string StateName 
        {
            get {
                string[] paramsArray = Params.Split(';');
                return paramsArray[0];
            }
        }

        public string StartDate
        {
            get
            {
                string[] paramsArray = Params.Split(';');
                return paramsArray[1];
            }
        }

        public string EndDate
        {
            get
            {
                string[] paramsArray = Params.Split(';');
                return paramsArray[2];
            }
        }

		public int Status { get; set; }

        public string StatusName 
        {
            get { 
                switch (Status)
                {
                    case 0:
                        return "In Progress";
                    case 1:
                        return "Completed";
                    case 2:
                        return "Error";
                }
                return "Undefined";
            }
        }


		public DateTime StartedOn { get; set; }
		public DateTime? CompletedOn { get; set;}

        public string Params { get; set; }
        public string FilePath { get; set; }

        public Guid? UserID { get; set; }

        public string UserName { get; set; }
    
	}
}
