﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Logging
{
    public class CurrentLogger
    {
        public static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }
}
