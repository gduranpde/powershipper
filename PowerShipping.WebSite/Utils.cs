﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

namespace PowerShipping.WebSite
{
    public enum SortOrder
    {
        Ascending,
        Descending
    }
    //PG31
    public static class HelperMethods
    {
        /// <summary>
        /// IsValid
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns> 
        public static bool CheckDbNullOrEmpty(this object obj)
        {
            return obj != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(obj).Trim());
        }

        public static bool IsValid(this object obj)
        {
            return obj != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(obj).Trim());
        }
    }


    public class Sorter<T> : IComparer<T>
    {
        private string _SortString = String.Empty;

        public Sorter()
        {
        }

        public Sorter(string sortstring)
        {
            _SortString = sortstring;
        }

        public string SortString
        {
            get { return _SortString.Trim(); }
            set { _SortString = value; }
        }

        #region IComparer<T> Members

        public int Compare(T x, T y)
        {
            int result = 0;

            if (!string.IsNullOrEmpty(SortString))
            {
                Type t = typeof(T);

                Comparer c = Comparer.DefaultInvariant;
                PropertyInfo pi;

                foreach (string expr in SortString.Split(new[] { ',' }))
                {
                    SortOrder dir = SortOrder.Ascending;
                    string field;

                    if (expr.EndsWith(" DESC"))
                    {
                        field = expr.Replace(" DESC", String.Empty).Trim();
                        dir = SortOrder.Descending;
                    }
                    else
                    {
                        field = expr.Replace(" ASC", String.Empty).Trim();
                    }
                    pi = t.GetProperty(field);
                    if (pi != null)
                    {
                        result = c.Compare(pi.GetValue(x, null), pi.GetValue(y, null));
                        if (dir.Equals(SortOrder.Descending))
                        {
                            result = -result;
                        }
                        if (result != 0)
                        {
                            break;
                        }
                    }
                }
                return result;
            }
            return result;
        }

        #endregion

    }
 }