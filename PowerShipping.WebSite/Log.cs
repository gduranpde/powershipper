﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;

namespace PowerShipping.WebSite
{
    /// <summary>
    ///Writes Log file.
    /// </summary>
    public enum ErrorType
    {
        Information,
        Alert,
        Warning
    }

    public class Log
    {
        public static void LogError(Exception exc)
        {
            WriteLog(ConfigurationManager.AppSettings["LogPath"],
                                           ErrorType.Alert,
                                           MethodBase.GetCurrentMethod().DeclaringType + " " +
                                           MethodBase.GetCurrentMethod().Name,
                                           exc.Message + " " + exc.StackTrace);
        }

        public static void LogInfo(string sourceDescr, string descrText, string userName)
        {
            WriteLog(ConfigurationManager.AppSettings["LogPath"],
                                    ErrorType.Information,
                                    sourceDescr, descrText);
        }


        private static bool LogPath(string path)
        {
            bool b = false;
            if (File.Exists(path))
            {
                b = true;
            }
            else
            {
                b = CreatePath(path);
            }
            //if (Directory.Exists(path))
            //{
            //    b = true;
            //}
            //else
            //{
            //    b = CreatePath(path);
            //}
            return b;
        }

        private static bool CreatePath(string path)
        {
            bool b = false;
            if (path.Length > 3)
            {
                string PATH = "";

                try
                {
                    var r = new Regex(@"(\\)");
                    string[] s = r.Split(path);

                    //foreach (string p in s)
                    for (int i = 0; i < s.Length-1; i++ )
                    {
                        //PATH += p;
                        PATH += s[i];

                        if (!Directory.Exists(PATH))
                        {
                            Directory.CreateDirectory(PATH);
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Create Path " + e.Message + " Path=" + PATH);
                }
                b = Directory.Exists(PATH);
            }
            return b;
        }

        private static void WriteLog(string path, ErrorType LogType, string SourceDesc, string DescText)
        {
            //string filename = DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Year + ".txt";
            //string filename = "PowerShipping.log";
            if (LogPath(path))
            {
                //var fs = new FileStream(path + "\\" + filename, FileMode.OpenOrCreate, FileAccess.Write);
                var fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
                try
                {
                    var m_streamWriter = new StreamWriter(fs);

                    m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
                    m_streamWriter.WriteLine(LogType + " | " + DateTime.Now.ToShortDateString() + " " +
                                             DateTime.Now.ToShortTimeString() + " | " + SourceDesc + " | " + DescText +
                                             " | " + Dns.GetHostByName("LocalHost").HostName);

                    m_streamWriter.Flush();
                }
                catch (Exception e)
                {
                    throw new Exception(" WRITE LOG ERROR: " + e.Message);
                }
                finally
                {
                    fs.Close();
                    fs = null;
                }
            }
        }
    }
}