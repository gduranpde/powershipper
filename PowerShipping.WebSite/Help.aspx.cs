﻿using System;
using System.Web.UI;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.Logging;

namespace PowerShipping.WebSite
{
    public partial class Help : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["type"]))
                    {
                        string helpText = string.Empty;
                        string helpType = Request.QueryString["type"].ToLower();

                        switch (helpType)
                        {
                            case "powershipping.entities.consumerrequest":
                                helpText = Common.GetHelp(typeof(ConsumerRequest), Server.MapPath(""));
                                break;

                            case "powershipping.entities.optout":
                                helpText = Common.GetHelp(typeof(OptOut), Server.MapPath(""));
                                break;

                            case "powershipping.entities.shipmentdetail":
                                helpText = Common.GetHelp(typeof(ShipmentDetail), Server.MapPath(""));
                                break;
                            default:
                                helpText = Common.GetHelp(helpType, Server.MapPath(""));
                                break;
                        }

                        help.Text = helpText;
                        title.Text = Common.GetTitle(helpType);
                    }
                }

                if (!Page.IsPostBack)
                {
                    Shared.Default MasterPage = (Shared.Default)Page.Master;
                    MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Help Page");
                }
            }
            catch (Exception ex)
            {
                CurrentLogger.Log.Error(ex);
            }
        }
    }
}