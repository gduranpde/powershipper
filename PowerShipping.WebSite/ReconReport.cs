﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using PowerShipping.Data;
using PowerShipping.Entities;
using PowerShipping.Logging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace PowerShipping.WebSite
{
    public class ReconReportData
    {
        public string MonthYear = "June 2014";
        public List<string> ProjectList = new List<string> { "P1", "P2", "P3" };
        public List<string> OperatingCompanies = new List<string> { "C1", "C2", "C3", "C4" };
        public int[,] NoPDelivery = new int[3, 4]{
            {2, 2, 3, 0},
            {1, 2, 3, 3},
            {4, 2, 3, 4}
        };
        public decimal[,] MWDelivery = new decimal[3, 4]{
            {2.0m, 2.0m, 3.0m, 0.0m},
            {1.5m, 2.5m, 3.5m, 3.0m},
            {4.3m, 2.3m, 3.5m, 4.5m}
        };
        public decimal[,] MWHDelivery = new decimal[3, 4]{
            {20, 20, 30, 00},
            {10, 20, 30, 30},
            {40, 20, 30, 40}
        };
        public int[,] NoPException = new int[3, 4]{
            {1, 2, 3, 0},
            {15, 2, 3, 3},
            {40, 2, 3, 4}
        };
        public decimal[,] MWException = new decimal[3, 4]{
            {2.0m, 2.0m, 3.0m, 0.0m},
            {1.5m, 2.5m, 3.5m, 3.0m},
            {4.3m, 2.3m, 3.5m, 4.5m}
        };
        public decimal[,] MWHException = new decimal[3, 4]{
            {50, 20, 30, 00},
            {10, 20, 50, 30},
            {80, 20, 30, 40}
        };
        public int[,] NoPExceptionSummary = new int[3, 4]{
            {1, 2, 3, 0},
            {15, 2, 3, 3},
            {40, 2, 3, 4}
        };
        public decimal[,] MWExceptionSummary = new decimal[3, 4]{
            {2.0m, 2.0m, 3.0m, 0.0m},
            {1.5m, 2.5m, 3.5m, 3.0m},
            {4.3m, 2.3m, 3.5m, 4.5m}
        };
        public decimal[,] MWHExceptionSummary = new decimal[3, 4]{
            {50, 20, 30, 00},
            {10, 20, 50, 30},
            {80, 20, 30, 40}
        };
    }

    public class ReconReport
    {
        
        private ReconReportData GetReportData(int stateID, DateTime startDate, DateTime endDate, int lastMonth)
        {

            ReconReportData data = new ReconReportData();
            
            data.MonthYear = startDate.ToString("MMMM")+" " + startDate.Year.ToString() ;

            CurrentLogger.Log.Info("Before Project Get Active By State");
            List<Project> lstProj = DataProvider.Current.Project.GetActiveByState(stateID);
            CurrentLogger.Log.Info("Before Operating Company get active by state");
            List<OperatingCompany> lstOC = DataProvider.Current.OperatingCompany.GetActiveByState(stateID);
            CurrentLogger.Log.Info("Before ReconciliationReport_GetDelivery");
            List<ReconciliationReport> lstDelivery = DataProvider.Current.Report.ReconciliationReport_GetDelivery(stateID, startDate, endDate);
            CurrentLogger.Log.Info("Before ReconciliationReport_GetExceptions");
            List<ReconciliationReport> lstExceptions = DataProvider.Current.Report.ReconciliationReport_GetExceptions(stateID, startDate, endDate);

            List<ReconciliationReport> lstExceptionSummary = null;

            if (lastMonth == 1)
            {
                lstExceptionSummary = DataProvider.Current.Report.ReconciliationReport_GetExceptionSummary(stateID, startDate, endDate);
            }

            data.ProjectList.Clear();

            foreach (Project p in lstProj)
                data.ProjectList.Add(p.ProjectName);

            data.OperatingCompanies.Clear();
            foreach (OperatingCompany p in lstOC)
                data.OperatingCompanies.Add(p.OperatingCompanyCode);

            data.NoPDelivery = new int [lstProj.Count(), lstOC.Count()];
            data.MWDelivery = new decimal[lstProj.Count(), lstOC.Count()];
            data.MWHDelivery = new decimal[lstProj.Count(), lstOC.Count()];

            foreach (ReconciliationReport rec in lstDelivery)
            {
                int iProj = lstProj.FindIndex(x => x.ProjectName == rec.ProjectCode);
                int jOC = lstOC.FindIndex(x => x.OperatingCompanyCode == rec.OperatingCompanyCode);

                if (iProj >= 0 && jOC >= 0)
                {
                    data.NoPDelivery[iProj, jOC] = rec.NoPart;
                    data.MWDelivery[iProj, jOC] = rec.MW;
                    data.MWHDelivery[iProj, jOC] = rec.MWH;
                }
            }

            data.NoPException = new int[lstProj.Count(), lstOC.Count()];
            data.MWException = new decimal[lstProj.Count(), lstOC.Count()];
            data.MWHException = new decimal[lstProj.Count(), lstOC.Count()];

            foreach (ReconciliationReport rec in lstExceptions)
            {
                int iProj = lstProj.FindIndex(x => x.ProjectName == rec.ProjectCode);
                int jOC = lstOC.FindIndex(x => x.OperatingCompanyCode == rec.OperatingCompanyCode);

                if (iProj >= 0 && jOC >= 0)
                {
                    data.NoPException[iProj, jOC] = rec.NoPart;
                    data.MWException[iProj, jOC] = rec.MW;
                    data.MWHException[iProj, jOC] = rec.MWH;
                }
            }

            if (lastMonth == 1)
            {
                data.NoPExceptionSummary = new int[lstProj.Count(), lstOC.Count()];
                data.MWExceptionSummary = new decimal[lstProj.Count(), lstOC.Count()];
                data.MWHExceptionSummary = new decimal[lstProj.Count(), lstOC.Count()];

                foreach (ReconciliationReport rec in lstExceptionSummary)
                {
                    int iProj = lstProj.FindIndex(x => x.ProjectName == rec.ProjectCode);
                    int jOC = lstOC.FindIndex(x => x.OperatingCompanyCode == rec.OperatingCompanyCode);

                    if (iProj >= 0 && jOC >= 0)
                    {
                        data.NoPExceptionSummary[iProj, jOC] = rec.NoPart;
                        data.MWExceptionSummary[iProj, jOC] = rec.MW;
                        data.MWHExceptionSummary[iProj, jOC] = rec.MWH;
                    }
                }
            }

            return data;
        }

        public void GenerateReportForMonth(string filePath, DateTime startDate, DateTime endDate, int StateID, int id, string shortfilePath)
        {
            try
            {
                FileInfo newFile = new FileInfo(filePath);

                if (newFile.Exists)
                {
                    newFile.Delete();  // ensures we create a new workbook
                    newFile = new FileInfo(filePath);
                }
                using (ExcelPackage package = new ExcelPackage(newFile))
                {
                    // add a new worksheet to the empty workbook
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Reconciliation Report");
                    int rowNumber = 1;

                    int MonthNumber;

                    MonthNumber = ((endDate.Year - startDate.Year) * 12) + endDate.Month - startDate.Month + 1;
                    //if (endDate.Year == startDate.Year)
                    //{
                    //    MonthNumber = endDate.Month - startDate.Month + 1;
                    //}
                    //else
                    //{
                    //    MonthNumber = 13 - (startDate.Month - endDate.Month);
                    //}
                    for (int i = 0; i < MonthNumber; i++)
                    {

                        DateTime eachMonthDate = startDate.AddMonths(i);
                        DateTime startDateNew = new DateTime(eachMonthDate.Year, eachMonthDate.Month, 1, 00, 00, 00);
                        DateTime endDateMonth = startDateNew.AddMonths(1).AddDays(-1);
                        DateTime endDateNew = new DateTime(eachMonthDate.Year, eachMonthDate.Month, endDateMonth.Day, 23, 59, 59);

                        int lastMonth = 0;
                        if (i == MonthNumber - 1)
                        {
                            lastMonth = 1;
                        }
                        ReconReportData data = GetReportData(StateID, startDateNew, endDateNew, lastMonth);
                        CurrentLogger.Log.Info("Before GetReportData");
                  
                        CurrentLogger.Log.Info("Before Monthly Delivery");
                        rowNumber = GenerateMonthlyDelivery(worksheet, data, rowNumber);
                        CurrentLogger.Log.Info("Before Monthly Exception");
                        rowNumber = GenerateMonthlyException(worksheet, data, rowNumber);
                        CurrentLogger.Log.Info("Before Monthly NET");
                        rowNumber = GenerateMonthlyNet(worksheet, data, rowNumber);
                        CurrentLogger.Log.Info("Before Monthly Cumulative");
                        rowNumber = GenerateMonthlyCumulative(worksheet, data, rowNumber, i);
                        CurrentLogger.Log.Info("After Monthly Cumulative");
                        if (i < MonthNumber - 1)
                        {
                            rowNumber = rowNumber + 2;
                            worksheet.Row(rowNumber).Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Row(rowNumber).Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            rowNumber = rowNumber + 1;
                        }
                        else if (i == MonthNumber - 1)
                        {
                            rowNumber = ExceptionActivity(worksheet, data, rowNumber);
                        }

                    }
                    CurrentLogger.Log.Info("Before Excel Save");
                    package.Save();
                    CurrentLogger.Log.Info("After Excel Save");
                }
                int status = 1;
                CurrentLogger.Log.Info("Before UpdateReport");
                DataProvider.Current.Report.UpdateReport(id, status, shortfilePath);
                CurrentLogger.Log.Info("After UpdateReport");
            }
            catch (Exception e)
            {
                CurrentLogger.Log.Info("Exception: " + e.ToString());
                FileInfo newFile = new FileInfo(filePath);

                using (ExcelPackage package = new ExcelPackage(newFile))
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Error");
                        worksheet.Cells[1, 1].Value = "ERROR :  "+e ;
                        package.Save();
                    }
                int status = 2;
                DataProvider.Current.Report.UpdateReport(id, status, shortfilePath);
            }
        }

        private int GenerateMonthlyDelivery(ExcelWorksheet worksheet, ReconReportData data, int startTableRow)
        {
            //Add the headers
            
            int startTableIndex = startTableRow;
            int noProj = data.ProjectList.Count();
            int noOpComp = data.OperatingCompanies.Count();

            worksheet.Cells[startTableIndex + 1, 1].Value = data.MonthYear;
            worksheet.Cells[startTableIndex + 2, 1].Value = "Monthly Delivery";
            worksheet.Cells[startTableIndex + 3, 1].Value = "Project";
            worksheet.Cells[startTableIndex + 3, 2].Value = "# of Participants";
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp].Value = "MW Savings";
            worksheet.Cells[startTableIndex + 3, 2 + 2 * noOpComp].Value = "MWH Savings";
            worksheet.Cells[startTableIndex + 5 + noProj, 1].Value = "TOTAL";

            //formatting table
            //column weight
            for (int i = 1; i < 2 + 3 * noOpComp; i++)
            {
                worksheet.Column(i).AutoFit();
            }           

            //bold
            worksheet.Cells[startTableIndex + 1, 1].Style.Font.Bold = true;
            worksheet.Cells[startTableIndex + 3, 1, startTableIndex + 3, 1 + 3 * noOpComp].Style.Font.Bold = true;
            worksheet.Cells[startTableIndex + 5 + noProj, 1,startTableIndex + 5 + noProj, 1 + 3 * noOpComp].Style.Font.Bold = true;
            //alignament Center
            worksheet.Cells[startTableIndex + 3, 2, startTableIndex + 3, 1 + 3 * noOpComp].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[startTableIndex + 4, 2, startTableIndex + 4, 1 + 3 * noOpComp].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            //Merges
            worksheet.Cells[startTableIndex + 3, 2, startTableIndex + 3, 2 + noOpComp - 1].Merge = true;
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp, startTableIndex + 3, 2 + 2 * noOpComp - 1].Merge = true;
            worksheet.Cells[startTableIndex + 3, 2 + 2 * noOpComp, startTableIndex + 3, 2 + 3 * noOpComp - 1].Merge = true;
            //fill color
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp, startTableIndex + 4, 2 + 3 * noOpComp - 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[startTableIndex + 5 + noProj, 2 + noOpComp, startTableIndex + 5 + noProj, 2 + 3 * noOpComp - 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp, startTableIndex + 4, 2 + 2 * noOpComp - 1].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
            worksheet.Cells[startTableIndex + 3, 2 + 2 * noOpComp, startTableIndex + 4, 2 + 3 * noOpComp - 1].Style.Fill.BackgroundColor.SetColor(Color.MediumSeaGreen); ;
            worksheet.Cells[startTableIndex + 5 + noProj, 2 + noOpComp, startTableIndex + 5 + noProj, 2 + 2 * noOpComp - 1].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
            worksheet.Cells[startTableIndex + 5 + noProj, 2 + 2 * noOpComp, startTableIndex + 5 + noProj, 2 + 3 * noOpComp - 1].Style.Fill.BackgroundColor.SetColor(Color.MediumSeaGreen);
            // border
            for (int i = startTableIndex + 3; i < startTableIndex+ 6 + noProj; i++)
            {
                for (int j = 1; j < 2 + 3 * noOpComp; j++)
                {
                    worksheet.Cells[i, j].Style.Border.BorderAround(ExcelBorderStyle.Medium);
                }
            }

            // creating the table subheader (projects , companies)
            int index = 0;
            foreach(string proj in data.ProjectList)
            {
                worksheet.Cells[startTableIndex + 5 + index, 1].Value = proj;
                index++;
            }
            index = 0;
            foreach (string operCompany in data.OperatingCompanies)
            {
                worksheet.Cells[startTableIndex + 4, 2 + index].Value = operCompany;
                worksheet.Cells[startTableIndex + 4, 2 + data.OperatingCompanies.Count + index].Value = operCompany;
                worksheet.Cells[startTableIndex + 4, 2 + 2 * data.OperatingCompanies.Count + index].Value = operCompany;
                index++;
            }

            //introducing data
            for (int i = 0; i < noProj; i++)
            {
                for (int j = 0; j < noOpComp; j++)
                {
                    worksheet.Cells[startTableIndex + 5 + i, 2 + j].Value = data.NoPDelivery[i, j];
                    worksheet.Cells[startTableIndex + 5 + i, 2 + noOpComp + j].Value = data.MWDelivery[i, j];
                    worksheet.Cells[startTableIndex + 5 + i, 2 + 2 * noOpComp + j].Value = data.MWHDelivery[i, j];
                }
            }

            //formulas
            worksheet.Cells[startTableIndex + 5 + noProj, 2, startTableIndex + 5 + noProj, 1 + 3 * noOpComp].Formula = string.Format("SUM({0})", new ExcelAddress(startTableIndex + 5, 2, startTableIndex + 4 + noProj, 2).Address); ;

            return startTableIndex + 6 + noProj;

        }

        private int GenerateMonthlyException(ExcelWorksheet worksheet, ReconReportData data, int startTableRow)
        {
            //Add the headers
            int startTableIndex = startTableRow;
            int noProj = data.ProjectList.Count();
            int noOpComp = data.OperatingCompanies.Count();
            worksheet.Cells[startTableIndex + 2, 1].Value = "Monthly Exception";
            worksheet.Cells[startTableIndex + 3, 1].Value = "Project";
            worksheet.Cells[startTableIndex + 3, 2].Value = "# of Participants";
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp].Value = "MW Savings";
            worksheet.Cells[startTableIndex + 3, 2 + 2 * noOpComp].Value = "MWH Savings";
            worksheet.Cells[startTableIndex + 5 + noProj, 1].Value = "TOTAL";

            //formatting table
            
            //bold
            worksheet.Cells[startTableIndex + 1, 1].Style.Font.Bold = true;
            worksheet.Cells[startTableIndex + 3, 1, startTableIndex + 3, 1 + 3 * noOpComp].Style.Font.Bold = true;
            worksheet.Cells[startTableIndex + 5 + noProj, 1, startTableIndex + 5 + noProj, 1 + 3 * noOpComp].Style.Font.Bold = true;
            //alignament Center
            worksheet.Cells[startTableIndex + 3, 2, startTableIndex + 3, 1 + 3 * noOpComp].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[startTableIndex + 4, 2, startTableIndex + 4, 1 + 3 * noOpComp].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            //Merges
            worksheet.Cells[startTableIndex + 3, 2, startTableIndex + 3, 2 + noOpComp - 1].Merge = true;
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp, startTableIndex + 3, 2 + 2 * noOpComp - 1].Merge = true;
            worksheet.Cells[startTableIndex + 3, 2 + 2 * noOpComp, startTableIndex + 3, 2 + 3 * noOpComp - 1].Merge = true;
            //fill color
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp, startTableIndex + 4, 2 + 3 * noOpComp - 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[startTableIndex + 5 + noProj, 2 + noOpComp, startTableIndex + 5 + noProj, 2 + 3 * noOpComp - 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp, startTableIndex + 4, 2 + 2 * noOpComp - 1].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
            worksheet.Cells[startTableIndex + 3, 2 + 2 * noOpComp, startTableIndex + 4, 2 + 3 * noOpComp - 1].Style.Fill.BackgroundColor.SetColor(Color.MediumSeaGreen); ;
            worksheet.Cells[startTableIndex + 5 + noProj, 2 + noOpComp, startTableIndex + 5 + noProj, 2 + 2 * noOpComp - 1].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
            worksheet.Cells[startTableIndex + 5 + noProj, 2 + 2 * noOpComp, startTableIndex + 5 + noProj, 2 + 3 * noOpComp - 1].Style.Fill.BackgroundColor.SetColor(Color.MediumSeaGreen);
            // border
            for (int i = startTableIndex + 3; i < startTableIndex + 6 + noProj; i++)
            {
                for (int j = 1; j < 2 + 3 * noOpComp; j++)
                {
                    worksheet.Cells[i, j].Style.Border.BorderAround(ExcelBorderStyle.Medium);
                }
            }

            // creating the table subheader (projects , companies)
            int index = 0;
            foreach (string proj in data.ProjectList)
            {
                worksheet.Cells[startTableIndex + 5 + index, 1].Value = proj;
                index++;
            }
            index = 0;
            foreach (string operCompany in data.OperatingCompanies)
            {
                worksheet.Cells[startTableIndex + 4, 2 + index].Value = operCompany;
                worksheet.Cells[startTableIndex + 4, 2 + data.OperatingCompanies.Count + index].Value = operCompany;
                worksheet.Cells[startTableIndex + 4, 2 + 2 * data.OperatingCompanies.Count + index].Value = operCompany;
                index++;
            }

            //introducing data
            for (int i = 0; i < noProj; i++)
            {
                for (int j = 0; j < noOpComp; j++)
                {
                    worksheet.Cells[startTableIndex + 5 + i, 2 + j].Value = data.NoPException[i, j];
                    worksheet.Cells[startTableIndex + 5 + i, 2 + noOpComp + j].Value = data.MWException[i, j];
                    worksheet.Cells[startTableIndex + 5 + i, 2 + 2 * noOpComp + j].Value = data.MWHException[i, j];
                }
            }

            //formulas
            worksheet.Cells[startTableIndex + 5 + noProj, 2, startTableIndex + 5 + noProj, 1 + 3 * noOpComp].Formula = string.Format("SUM({0})", new ExcelAddress(startTableIndex + 5, 2, startTableIndex + 4 + noProj, 2).Address); ;

            return startTableIndex + 6 + noProj;
                        
        }

        private int GenerateMonthlyNet(ExcelWorksheet worksheet, ReconReportData data, int startTableRow)
        {
            //Add the headers
            int startTableIndex = startTableRow;
            int noProj = data.ProjectList.Count();
            int noOpComp = data.OperatingCompanies.Count();
            worksheet.Cells[startTableIndex + 2, 1].Value = "Monthly Net";
            worksheet.Cells[startTableIndex + 3, 1].Value = "Project";
            worksheet.Cells[startTableIndex + 3, 2].Value = "# of Participants";
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp].Value = "MW Savings";
            worksheet.Cells[startTableIndex + 3, 2 + 2 * noOpComp].Value = "MWH Savings";
            worksheet.Cells[startTableIndex + 5 + noProj, 1].Value = "TOTAL";

            //formatting table

            //bold
            worksheet.Cells[startTableIndex + 1, 1].Style.Font.Bold = true;
            worksheet.Cells[startTableIndex + 3, 1, startTableIndex + 3, 1 + 3 * noOpComp].Style.Font.Bold = true;
            worksheet.Cells[startTableIndex + 5 + noProj, 1, startTableIndex + 5 + noProj, 1 + 3 * noOpComp].Style.Font.Bold = true;
            //alignament Center
            worksheet.Cells[startTableIndex + 3, 2, startTableIndex + 3, 1 + 3 * noOpComp].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[startTableIndex + 4, 2, startTableIndex + 4, 1 + 3 * noOpComp].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            //Merges
            worksheet.Cells[startTableIndex + 3, 2, startTableIndex + 3, 2 + noOpComp - 1].Merge = true;
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp, startTableIndex + 3, 2 + 2 * noOpComp - 1].Merge = true;
            worksheet.Cells[startTableIndex + 3, 2 + 2 * noOpComp, startTableIndex + 3, 2 + 3 * noOpComp - 1].Merge = true;
            //fill color
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp, startTableIndex + 4, 2 + 3 * noOpComp - 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[startTableIndex + 5 + noProj, 2 + noOpComp, startTableIndex + 5 + noProj, 2 + 3 * noOpComp - 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp, startTableIndex + 4, 2 + 2 * noOpComp - 1].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
            worksheet.Cells[startTableIndex + 3, 2 + 2 * noOpComp, startTableIndex + 4, 2 + 3 * noOpComp - 1].Style.Fill.BackgroundColor.SetColor(Color.MediumSeaGreen); ;
            worksheet.Cells[startTableIndex + 5 + noProj, 2 + noOpComp, startTableIndex + 5 + noProj, 2 + 2 * noOpComp - 1].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
            worksheet.Cells[startTableIndex + 5 + noProj, 2 + 2 * noOpComp, startTableIndex + 5 + noProj, 2 + 3 * noOpComp - 1].Style.Fill.BackgroundColor.SetColor(Color.MediumSeaGreen);
            // border
            for (int i = startTableIndex + 3; i < startTableIndex + 6 + noProj; i++)
            {
                for (int j = 1; j < 2 + 3 * noOpComp; j++)
                {
                    worksheet.Cells[i, j].Style.Border.BorderAround(ExcelBorderStyle.Medium);
                }
            }

            // creating the table subheader (projects , companies)
            int index = 0;
            foreach (string proj in data.ProjectList)
            {
                worksheet.Cells[startTableIndex + 5 + index, 1].Value = proj;
                index++;
            }
            index = 0;
            foreach (string operCompany in data.OperatingCompanies)
            {
                worksheet.Cells[startTableIndex + 4, 2 + index].Value = operCompany;
                worksheet.Cells[startTableIndex + 4, 2 + data.OperatingCompanies.Count + index].Value = operCompany;
                worksheet.Cells[startTableIndex + 4, 2 + 2 * data.OperatingCompanies.Count + index].Value = operCompany;
                index++;
            }

            //introducing data
            for (int i = 0; i < noProj; i++)
            {
                for (int j = 0; j < noOpComp; j++)
                {
                    worksheet.Cells[startTableIndex + 5 + i, 2 + j].Value = data.NoPDelivery[i, j] - data.NoPException[i, j];
                    worksheet.Cells[startTableIndex + 5 + i, 2 + noOpComp + j].Value = data.MWDelivery[i, j] - data.MWException[i, j];
                    worksheet.Cells[startTableIndex + 5 + i, 2 + 2 * noOpComp + j].Value = data.MWHDelivery[i, j] - data.MWHException[i, j];
                }
            }

            //formulas
            worksheet.Cells[startTableIndex + 5 + noProj, 2, startTableIndex + 5 + noProj, 1 + 3 * noOpComp].Formula = string.Format("SUM({0})", new ExcelAddress(startTableIndex + 5, 2, startTableIndex + 4 + noProj, 2).Address); ;

            return startTableIndex + 6 + noProj;
        }

        private int GenerateMonthlyCumulative(ExcelWorksheet worksheet, ReconReportData data, int startTableRow, int MonthNumber)
        {
            //Add the headers
            int startTableIndex = startTableRow;
            int noProj = data.ProjectList.Count();
            int noOpComp = data.OperatingCompanies.Count();
            worksheet.Cells[startTableIndex + 2, 1].Value = "Cumulative Net";
            worksheet.Cells[startTableIndex + 3, 1].Value = "Project";
            worksheet.Cells[startTableIndex + 3, 2].Value = "# of Participants";
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp].Value = "MW Savings";
            worksheet.Cells[startTableIndex + 3, 2 + 2 * noOpComp].Value = "MWH Savings";
            worksheet.Cells[startTableIndex + 5 + noProj, 1].Value = "TOTAL";

            //formatting table

            //bold
            worksheet.Cells[startTableIndex + 1, 1].Style.Font.Bold = true;
            worksheet.Cells[startTableIndex + 3, 1, startTableIndex + 3, 1 + 3 * noOpComp].Style.Font.Bold = true;
            worksheet.Cells[startTableIndex + 5 + noProj, 1, startTableIndex + 5 + noProj, 1 + 3 * noOpComp].Style.Font.Bold = true;
            //alignament Center
            worksheet.Cells[startTableIndex + 3, 2, startTableIndex + 3, 1 + 3 * noOpComp].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[startTableIndex + 4, 2, startTableIndex + 4, 1 + 3 * noOpComp].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            //Merges
            worksheet.Cells[startTableIndex + 3, 2, startTableIndex + 3, 2 + noOpComp - 1].Merge = true;
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp, startTableIndex + 3, 2 + 2 * noOpComp - 1].Merge = true;
            worksheet.Cells[startTableIndex + 3, 2 + 2 * noOpComp, startTableIndex + 3, 2 + 3 * noOpComp - 1].Merge = true;
            //fill color
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp, startTableIndex + 4, 2 + 3 * noOpComp - 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[startTableIndex + 5 + noProj, 2 + noOpComp, startTableIndex + 5 + noProj, 2 + 3 * noOpComp - 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp, startTableIndex + 4, 2 + 2 * noOpComp - 1].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
            worksheet.Cells[startTableIndex + 3, 2 + 2 * noOpComp, startTableIndex + 4, 2 + 3 * noOpComp - 1].Style.Fill.BackgroundColor.SetColor(Color.MediumTurquoise); ;
            worksheet.Cells[startTableIndex + 5 + noProj, 2 + noOpComp, startTableIndex + 5 + noProj, 2 + 2 * noOpComp - 1].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
            worksheet.Cells[startTableIndex + 5 + noProj, 2 + 2 * noOpComp, startTableIndex + 5 + noProj, 2 + 3 * noOpComp - 1].Style.Fill.BackgroundColor.SetColor(Color.MediumTurquoise);
            // border
            for (int i = startTableIndex + 3; i < startTableIndex + 6 + noProj; i++)
            {
                for (int j = 1; j < 2 + 3 * noOpComp; j++)
                {
                    worksheet.Cells[i, j].Style.Border.BorderAround(ExcelBorderStyle.Medium);
                }
            }

            // creating the table subheader (projects , companies)
            int index = 0;
            foreach (string proj in data.ProjectList)
            {
                worksheet.Cells[startTableIndex + 5 + index, 1].Value = proj;
                index++;
            }
            index = 0;
            foreach (string operCompany in data.OperatingCompanies)
            {
                worksheet.Cells[startTableIndex + 4, 2 + index].Value = operCompany;
                worksheet.Cells[startTableIndex + 4, 2 + data.OperatingCompanies.Count + index].Value = operCompany;
                worksheet.Cells[startTableIndex + 4, 2 + 2 * data.OperatingCompanies.Count + index].Value = operCompany;
                index++;
            }

            //introducing data
            for (int i = 0; i < noProj; i++)
            {
                for (int j = 0; j < noOpComp; j++)
                {
                    string sumPart="";
                    string sumMW = "";
                    string sumMWH = "";
                    for (int k = 0; k < MonthNumber+1; k++)
                    {
                        ExcelAddress sumPartExcel = new ExcelAddress(startTableIndex - 1 + i - noProj - k * ((6 + noProj) * 4 + 3), 2 + j, startTableIndex - 1 + i - noProj - k * ((6 + noProj) * 4 + 3), 2 + j);
                        sumPart = sumPart + (k==0?"":",") + sumPartExcel.Address;
                        ExcelAddress sumMWExcel = new ExcelAddress(startTableIndex - 1 + i - noProj - k * ((6 + noProj) * 4 + 3), 2 + noOpComp + j, startTableIndex - 1 + i - noProj - k * ((6 + noProj) * 4 + 3), 2 + noOpComp + j);
                        sumMW = sumMW + (k == 0 ? "" : ",") + sumMWExcel.Address;
                        ExcelAddress sumMWHExcel = new ExcelAddress(startTableIndex - 1 + i - noProj - k * ((6 + noProj) * 4 + 3), 2 + 2 * noOpComp + j, startTableIndex - 1 + i - noProj - k * ((6 + noProj) * 4 + 3), 2 + 2 * noOpComp + j);
                        sumMWH = sumMWH + (k == 0 ? "" : ",") + sumMWHExcel.Address;
                    }
                    worksheet.Cells[startTableIndex + 5 + i, 2 + j].Formula = string.Format("SUM({0})", sumPart);
                    worksheet.Cells[startTableIndex + 5 + i, 2 + noOpComp + j].Formula = string.Format("SUM({0})", sumMW);
                    worksheet.Cells[startTableIndex + 5 + i, 2 + 2 * noOpComp + j].Formula = string.Format("SUM({0})", sumMWH); 
                }
            }

            //formulas
            worksheet.Cells[startTableIndex + 5 + noProj, 2, startTableIndex + 5 + noProj, 1 + 3 * noOpComp].Formula = string.Format("SUM({0})", new ExcelAddress(startTableIndex + 5, 2, startTableIndex + 4 + noProj, 2).Address); 

            return startTableIndex + 6 + noProj;
        }

        private int ExceptionActivity(ExcelWorksheet worksheet, ReconReportData data, int startTableRow)
        {
            //Add the headers
            int startTableIndex = startTableRow;
            int noProj = data.ProjectList.Count();
            int noOpComp = data.OperatingCompanies.Count();
            worksheet.Cells[startTableIndex + 1, 1].Value = data.MonthYear;
            worksheet.Cells[startTableIndex + 2, 1].Value = "Exception Activity";
            worksheet.Cells[startTableIndex + 3, 1].Value = "Project";
            worksheet.Cells[startTableIndex + 3, 2].Value = "# of Participants";
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp].Value = "MW Savings";
            worksheet.Cells[startTableIndex + 3, 2 + 2 * noOpComp].Value = "MWH Savings";
            worksheet.Cells[startTableIndex + 5 + noProj, 1].Value = "TOTAL";

            //formatting table

            //bold
            worksheet.Cells[startTableIndex + 1, 1].Style.Font.Bold = true;
            worksheet.Cells[startTableIndex + 3, 1, startTableIndex + 3, 1 + 3 * noOpComp].Style.Font.Bold = true;
            worksheet.Cells[startTableIndex + 5 + noProj, 1, startTableIndex + 5 + noProj, 1 + 3 * noOpComp].Style.Font.Bold = true;
            //alignament Center
            worksheet.Cells[startTableIndex + 3, 2, startTableIndex + 3, 1 + 3 * noOpComp].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[startTableIndex + 4, 2, startTableIndex + 4, 1 + 3 * noOpComp].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            //Merges
            worksheet.Cells[startTableIndex + 3, 2, startTableIndex + 3, 2 + noOpComp - 1].Merge = true;
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp, startTableIndex + 3, 2 + 2 * noOpComp - 1].Merge = true;
            worksheet.Cells[startTableIndex + 3, 2 + 2 * noOpComp, startTableIndex + 3, 2 + 3 * noOpComp - 1].Merge = true;
            //fill color
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp, startTableIndex + 4, 2 + 3 * noOpComp - 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[startTableIndex + 5 + noProj, 2 + noOpComp, startTableIndex + 5 + noProj, 2 + 3 * noOpComp - 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp, startTableIndex + 4, 2 + 2 * noOpComp - 1].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
            worksheet.Cells[startTableIndex + 3, 2 + 2 * noOpComp, startTableIndex + 4, 2 + 3 * noOpComp - 1].Style.Fill.BackgroundColor.SetColor(Color.MediumSeaGreen); ;
            worksheet.Cells[startTableIndex + 5 + noProj, 2 + noOpComp, startTableIndex + 5 + noProj, 2 + 2 * noOpComp - 1].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
            worksheet.Cells[startTableIndex + 5 + noProj, 2 + 2 * noOpComp, startTableIndex + 5 + noProj, 2 + 3 * noOpComp - 1].Style.Fill.BackgroundColor.SetColor(Color.MediumSeaGreen);
            // border
            for (int i = startTableIndex + 3; i < startTableIndex + 6 + noProj; i++)
            {
                for (int j = 1; j < 2 + 3 * noOpComp; j++)
                {
                    worksheet.Cells[i, j].Style.Border.BorderAround(ExcelBorderStyle.Medium);
                }
            }

            // creating the table subheader (projects , companies)
            int index = 0;
            foreach (string proj in data.ProjectList)
            {
                worksheet.Cells[startTableIndex + 5 + index, 1].Value = proj;
                index++;
            }
            index = 0;
            foreach (string operCompany in data.OperatingCompanies)
            {
                worksheet.Cells[startTableIndex + 4, 2 + index].Value = operCompany;
                worksheet.Cells[startTableIndex + 4, 2 + data.OperatingCompanies.Count + index].Value = operCompany;
                worksheet.Cells[startTableIndex + 4, 2 + 2 * data.OperatingCompanies.Count + index].Value = operCompany;
                index++;
            }

            //introducing data
            for (int i = 0; i < noProj; i++)
            {
                for (int j = 0; j < noOpComp; j++)
                {
                    worksheet.Cells[startTableIndex + 5 + i, 2 + j].Value = data.NoPExceptionSummary[i, j];
                    worksheet.Cells[startTableIndex + 5 + i, 2 + noOpComp + j].Value = data.MWExceptionSummary[i, j];
                    worksheet.Cells[startTableIndex + 5 + i, 2 + 2 * noOpComp + j].Value = data.MWHExceptionSummary[i, j];
                }
            }

            //formulas
            worksheet.Cells[startTableIndex + 5 + noProj, 2, startTableIndex + 5 + noProj, 1 + 3 * noOpComp].Formula = string.Format("SUM({0})", new ExcelAddress(startTableIndex + 5, 2, startTableIndex + 4 + noProj, 2).Address); ;

            //Column autofit for all columms inclusiv merged one's
            startTableIndex = startTableIndex + 6 + noProj;
            worksheet.Cells[startTableIndex + 3, 2].Value = "# of Participants";
            worksheet.Cells[startTableIndex + 3, 2 + noOpComp].Value = "MW Savings";
            worksheet.Cells[startTableIndex + 3, 2 + 2 * noOpComp].Value = "MWH Savings";
            //bold
            worksheet.Cells[startTableIndex + 3, 1, startTableIndex + 3, 1 + 3 * noOpComp].Style.Font.Bold = true;
            //alignament Center
            worksheet.Cells[startTableIndex + 3, 2, startTableIndex + 3, 1 + 3 * noOpComp].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            //column weight
            for (int i = 1; i < 2 + 3 * noOpComp; i++)
            {
                worksheet.Column(i).AutoFit();
            }
            worksheet.DeleteRow(startTableIndex + 3, 1);


  
            return startTableIndex + 6 + noProj;

        }

        
    }
    }
