﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PowerShipping.Entities;

namespace PowerShipping.WebSite
{
    public class Rules
    {
        public static void RulesForReship(ShipmentDetail sd, ConsumerRequest cr)
        {
            if (sd.ReshipStatus == ShipmentReshipStatus.Reshipped)
            {
                cr.IsReship = true;
                cr.Status = ConsumerRequestStatus.RequestReceived;
            }
            if (sd.ReshipStatus == ShipmentReshipStatus.Dead)
            {
                cr.Status = ConsumerRequestStatus.NonDeliverableOrDead;
                if (cr.ReceiptDate != null)
                {
                    //cr.ReceiptDate = DateTime.Now;
                    cr.AuditFailureDate = DateTime.Now;
                }
            }
        }

        public static void RulesForShipmentStatusToReturn(ShipmentDetail sd, ConsumerRequest cr)
        {
            if (sd.ShippingStatus == ShipmentShippingStatus.Returned)
            {
                sd.ShippingStatusDate = DateTime.Now;
                sd.ReshipStatus = ShipmentReshipStatus.NA;
                
                cr.Status = ConsumerRequestStatus.Returned;
            }
        }
    }
}
