﻿<p class="MsoNormal">
	After FedEx prints a batch of labels, FedEx will send a CSV file to PowerDirect that contains the Tracking # for each shipment.&nbsp; The file sent from FedEx will have the following columns (in order):</p>
<p class="MsoNormal">
	&nbsp;</p>
<p>
	<span style="font-family: Symbol;"><span>&middot;<span style="font-family: &quot;Times New Roman&quot;; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span>CONFIRMATION# (FedEx Tracking #)</p>
<p>
	<span style="font-family: Symbol;"><span>&middot;<span style="font-family: &quot;Times New Roman&quot;; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span>RECIPIENT ID (First Energy Account #)</p>
<p>
	<span style="font-family: Symbol;"><span>&middot;<span style="font-family: &quot;Times New Roman&quot;; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span>REFERENCE (PowerDirect Shipment ID)</p>
<p>
	<span style="font-family: Symbol;"><span>&middot;<span style="font-family: &quot;Times New Roman&quot;; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span>PURCHASE ORDER #&nbsp; (PowerDirect Purchase Order to Fulfillment company)</p>
<p>
	<span style="font-family: Symbol;"><span>&middot;<span style="font-family: &quot;Times New Roman&quot;; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span>CONTACT</p>
<p>
	<span style="font-family: Symbol;"><span>&middot;<span style="font-family: &quot;Times New Roman&quot;; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span>ADDRESS1</p>
<p>
	<span style="font-family: Symbol;"><span>&middot;<span style="font-family: &quot;Times New Roman&quot;; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span>ADDRESS2</p>
<p>
	<span style="font-family: Symbol;"><span>&middot;<span style="font-family: &quot;Times New Roman&quot;; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span>CITY</p>
<p>
	<span style="font-family: Symbol;"><span>&middot;<span style="font-family: &quot;Times New Roman&quot;; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span>ST</p>
<p>
	<span style="font-family: Symbol;"><span>&middot;<span style="font-family: &quot;Times New Roman&quot;; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span>ZIP</p>
<p class="MsoNormal">
	&nbsp;</p>
<p class="MsoNormal">
	A sample is below:</p>
<p class="MsoNormal">
	&nbsp;</p>
<p class="MsoNormal">
	<b>CONFIRMATION#,RECIPIENT ID,REFERENCE,PURCHASE ORDER #,CONTACT,ADDRESS1,ADDRESS2,<wbr>CITY,ST,ZIP</wbr></b></p>
<p>
	<wbr>
	<p class="MsoNormal">
		02927003094305100005,<wbr>110068750279,1000010000,1218-<wbr>FE-9,PINE GROVE MOBILE HM PK,374 N PERRY HWY,,MERCER,PA,16137</wbr></wbr></p>
	<wbr><wbr>
	<p class="MsoNormal">
		02927003094305100012,<wbr>100073321851,1000010001,1218-<wbr>FE-9,R ANDREW MOORE,191 KANE RD,,SHEFFIELD,PA,16347</wbr></wbr></p>
	<wbr><wbr>
	<p class="MsoNormal">
		02927003094305100029,<wbr>100005906712,1000010002,1218-<wbr>FE-9,ROBERT M LESKANIC,9727 RT 6 N,,ALBION,PA,16401</wbr></wbr></p>
	<wbr><wbr>
	<p class="MsoNormal">
		02927003094305100036,<wbr>100019321817,1000010003,1218-<wbr>FE-9,RONALD J HAINES,3738A FILBERT DR,,DANIELSVILLE,PA,18038</wbr></wbr></p>
	<wbr><wbr>
	<p class="MsoNormal">
		02927003094305100043,<wbr>100081148296,1000010004,1218-<wbr>FE-9,ROSMIRA MORALES,2010 PURNBULL AVE,,BRONX,NY,10473</wbr></wbr></p>
	<wbr><wbr>
	<p class="MsoNormal">
		02927003094305100050,<wbr>100080837139,1000010005,1218-<wbr>FE-9,SHARON ANGLE,PO BOX A,,PORTLAND,PA,18351</wbr></wbr></p>
	<wbr><wbr>
	<p class="MsoNormal">
		02927003094305100067,<wbr>110068571899,1000010006,1218-<wbr>FE-9,SHARON MORRIS,1990B WILLIAMSFIELD RD,,JAMESTOWN,PA,16134</wbr></wbr></p>
	<wbr><wbr>
	<p class="MsoNormal">
		02927003094305100074,<wbr>100065120477,1000010007,1218-<wbr>FE-9,TIFFANY GETT,1413E ROUTE 286 HWY E,,INDIANA,PA,15701</wbr></wbr></p>
	<wbr><wbr>
	<p class="MsoNormal">
		02927003094305100081,<wbr>100079532576,1000010008,1218-<wbr>FE-9,TOIVO ALMODOVAR,23 EDDY LN,,EAST STROUDSBURG,PA,18301</wbr></wbr></p>
	<wbr><wbr>
	<p class="MsoNormal">
		02927003094305100098,<wbr>100019672565,1000010009,1218-<wbr>FE-9,ABDUL RASHEED ALI,313 BLUE MOUNTAIN LK,,EAST STROUDSBURG,PA,18301</wbr></wbr></p>
	<wbr><wbr>
	<p class="MsoNormal">
		&nbsp;</p>
	<p class="MsoNormal">
		Post processing and exceptions are below:</p>
	<p class="MsoNormal">
		&nbsp;</p>
	<p>
		<span style="font-family: Symbol;"><span>&middot;<span style="font-family: &quot;Times New Roman&quot;; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span>No Match:&nbsp; if the record identifier (either the Shipment ID or the Account Number) cannot be matched, the record will be flagged for exception</p>
	<p>
		<span style="font-family: Symbol;"><span>&middot;<span style="font-family: &quot;Times New Roman&quot;; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span>No Tracking Number:&nbsp; if a record comes back with no FedEx Tracking Number, the record will be flagged for exception</p>
	<p>
		<span style="font-family: Symbol;"><span>&middot;<span style="font-family: &quot;Times New Roman&quot;; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span>Scan for Missing Records:&nbsp; after all of the records are imported, the program will scan the Shipment Detail table for any records on the PO that do not have accompanying records in the data that is imported.</p>
	<p>
		<span style="font-family: Symbol;"><span>&middot;<span style="font-family: &quot;Times New Roman&quot;; font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span>Append FedEx Import Batch ID:&nbsp; the program should append a value for FedEx Import Batch ID for all records in the batch to provide traceability of the import.</p>
	</wbr></wbr></wbr></wbr></wbr></wbr></wbr></wbr></wbr></wbr></wbr></wbr></wbr></wbr></wbr></wbr></wbr></wbr></wbr></wbr></wbr></p>
