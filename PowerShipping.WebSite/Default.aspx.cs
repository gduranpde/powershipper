﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;

namespace PowerShipping.WebSite
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PowerShipping.WebSite.Shared.Default MasterPage = (PowerShipping.WebSite.Shared.Default)Page.Master;
                MasterPage.MenuVisible = false;
                MasterPage.LeftContentVisible = false;
            }

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Login Page");
            }
         
            Login1.Focus();
        }

        public void OnLoggedIn(object sender, EventArgs e)
        {
            HttpContext.Current.Session.Clear();

            HttpCookie cookie = HttpContext.Current.Response.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie != null && Login1.RememberMeSet)
            {
                cookie.Expires = DateTime.Today.AddDays(1);
            }

            if (Membership.ValidateUser(Login1.UserName, Login1.Password))
            {
                //string[] roles = Roles.GetRolesForUser(Login1.UserName);

                //foreach (string role in roles)
                //{
                //    if (role == PDMRoles.ROLE_CallCenter)
                //    {
                //        Response.Redirect("~/Home/CustomerLookup.aspx");
                //        break;
                //    }
                //}

                Response.Redirect("~/Home/ConsumerRequests.aspx");
            }
        }
        
    }
}
