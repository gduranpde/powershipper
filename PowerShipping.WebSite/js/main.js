﻿function onPanelItemClicking(sender, eventArgs) {
    eventArgs.get_domEvent().stopPropagation();
}
function onPanelItemClicking(sender, eventArgs) {
    eventArgs.get_domEvent().stopPropagation();
}
function IsSelected() {
    var chkText = '';
    var chkTable = document.getElementById('ctl00_ContentPlaceHolder1_ChkBoxListOperatingCompany');
    var chkTr = chkTable.getElementsByTagName('tr');

    var total = 0;

    for (var i = 0; i < chkTr.length; i++) {
        var newItem = 0;
        var chkTd = chkTr[i].getElementsByTagName('td');

        for (var j = 0; j < chkTd.length; j++) {
            var chkInput = chkTd[j].getElementsByTagName('input');
            var chkLabel = chkTd[j].getElementsByTagName('label');

            for (k = 0; k < chkInput.length; k++) {
                var chkOpt = chkInput[k];

                if (chkOpt.checked) {
                    //-------------------------------

                    total = total + 1;

                    //------------------------
                }
            }
        }
    }

    document.getElementById('ctl00_ContentPlaceHolder1_HiddenField1').value = total;
}
function readListControl() {
    document.getElementById('ctl00_ContentPlaceHolder1_LblMessage').innerHTML = "";

    var chkText = '';
    var chkTable = document.getElementById('ctl00_ContentPlaceHolder1_ChkBoxListOperatingCompany');
    var chkTr = chkTable.getElementsByTagName('tr');

    var total = 0;

    for (var i = 0; i < chkTr.length; i++) {
        var newItem = 0;
        var chkTd = chkTr[i].getElementsByTagName('td');

        for (var j = 0; j < chkTd.length; j++) {
            var chkInput = chkTd[j].getElementsByTagName('input');
            var chkLabel = chkTd[j].getElementsByTagName('label');

            for (k = 0; k < chkInput.length; k++) {
                var chkOpt = chkInput[k];

                if (chkOpt.checked) {
                    //-------------------------------

                    var temp = chkLabel[k].innerHTML + ',';

                    var start = temp.indexOf('(');
                    var end = temp.indexOf('-');

                    var temp1 = temp.substring(start + 1, end);

                    chkText = chkText + temp1 + ',';

                    //------------------------
                }
            }
        }
    }

    //------ Sum

    var toSum = chkText.split(",");

    var tot = 0;
    var v;

    for (var i = 0; i < toSum.length; i++) {
        v = parseInt(toSum[i]);

        if (!isNaN(v)) tot += v;
    }

    //------------------

    var totLbl = document.getElementById('ctl00_ContentPlaceHolder1_Label1');

    totLbl.innerHTML = "Total : " + tot;
}
function clearValidatorText() {
    document.getElementById("ctl00_ContentPlaceHolder1_LblMessage").innerHTML = "";
}
//For Dragging popup.
var dragElement = false;
var xDif = 0;
var yDif = 0;
var popupName = "";
function mouse_down(event, elementName) {
    var leftDim = document.getElementById(elementName).offsetLeft;
    var topDim = document.getElementById(elementName).offsetTop;
    dragElement = true;
    popupName = elementName;
    xDif = event.clientX - leftDim;
    yDif = event.clientY - topDim;
}

function dragWin(event) {
    if (dragElement == true) {
        document.getElementById(popupName).style.left = event.clientX - xDif + 'px';
        document.getElementById(popupName).style.top = event.clientY - yDif + 'px';
    }
}

function mouse_up(event, elementName) {
    dragElement = false;
    document.getElementById(elementName).style.left = event.clientX - xDif + 'px';
    document.getElementById(elementName).style.top = event.clientY - yDif + 'px';
    popupName = "";
}
function ImportProcessFailed() {
    var popupDiv = document.getElementById("ImportProcessFailed");
    popupDiv.style.display = (popupDiv.style.display == "block") ? "none" : "block";

    var backDiv = document.getElementById("divBack");
    backDiv.style.display = (backDiv.style.display == "block") ? "none" : "block";
    return false;
}
function AEGXMLReportFailed() {
    var popupDiv = document.getElementById("AEGXMLErrorPopUp");
    popupDiv.style.display = (popupDiv.style.display == "block") ? "none" : "block";

    var backDiv = document.getElementById("divBack");
    backDiv.style.display = (backDiv.style.display == "block") ? "none" : "block";
    return false;
}
function ImportActivityPassPopup() {
    var popupDiv = document.getElementById("ImportActivityPass");
    popupDiv.style.display = (popupDiv.style.display == "block") ? "none" : "block";

    var backDiv = document.getElementById("divBack");
    backDiv.style.display = (backDiv.style.display == "block") ? "none" : "block";
    return false;
}
function closePopup() {
    el = document.getElementById("overlay2");
    el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
    document.getElementById("divBack").style.display = "none";
}
function confirmSubmit() {

    var agree = confirm("Do you  want to delete this report?");
    if (agree)
        return true;
    else
        return false;
}
function ShowModalPopup() {
    $find("ModalBehaviour").show();
}

function HideModalPopup() {
    $find("ModalBehaviour").hide();
}
function ProgressAreaVisibility() {

    document.getElementById("PA").style.visibility = "visible";
    document.getElementById("ctl00_ContentPlaceHolder1_labelErrors").style.visibility = "visible";
    document.getElementById("ctl00_ContentPlaceHolder1_hlExceptions").style.visibility = "visible";

}
function ProgressAreaHide() {

    document.getElementById("PA").style.visibility = "hidden";
    document.getElementById("ctl00_ContentPlaceHolder1_labelErrors").style.visibility = "hidden";
    document.getElementById("ctl00_ContentPlaceHolder1_hlExceptions").style.visibility = "hidden";

}