﻿using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.WebSite.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Admin
{
    public partial class ItemSetup : System.Web.UI.Page
    {

        ItemsSetupPresenter presenter = new ItemsSetupPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Item Setup");
            }
        }

        //get all items
        protected void RadGrid_ItemsSetup_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_ItemsSetup.DataSource = presenter.GetAllItems();
        }

        //add new record
        protected void RadGrid_ItemsSetup_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.PerformInsertCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        if (e.Item is GridEditFormInsertItem)
                        {

                            Item item = new Item();

                            object o = ((ItemSetupCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                            Item itemNew = (Item)o;

                            item.CreatedBy = User.Identity.Name;
                            item.LastModifiedBy = User.Identity.Name;

                            item.CreatedDate = DateTime.Now;
                            item.LastModifiedDate = DateTime.Now;

                            item.ItemID = itemNew.ItemID;
                            item.ItemName = itemNew.ItemName;
                            item.ItemDescription = itemNew.ItemDescription;
                            item.DefaultKwImpact = itemNew.DefaultKwImpact;
                            item.DefaultKwhImpact = itemNew.DefaultKwhImpact;
                            item.IsActive = itemNew.IsActive;

                            presenter.SaveItem(item);
                            RadGrid_ItemsSetup.Rebind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                Label l = new Label();
                l.Text = "Unable to insert item. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_ItemsSetup.Controls.Add(l);
            }
        }

        //edit a record
        protected void RadGrid_ItemsSetup_UpdateCommand(object source, GridCommandEventArgs e)
        {

            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        GridEditFormItem item = (GridEditFormItem)e.Item;
                        Guid id = new Guid(item.GetDataKeyValue("ItemID").ToString());
                        if (id != Guid.Empty)
                        {
                            if (e.Item is GridEditableItem)
                            {
                                object o = ((ItemSetupCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                                Item itemNew = (Item)o;

                                Item it = presenter.GetItemById(itemNew.ItemID);

                                it.ItemName = itemNew.ItemName;
                                it.ItemDescription = itemNew.ItemDescription;
                                it.DefaultKwImpact = itemNew.DefaultKwImpact;
                                it.DefaultKwhImpact = itemNew.DefaultKwhImpact;
                                it.IsActive = itemNew.IsActive;
                                it.LastModifiedBy = User.Identity.Name;
                                it.LastModifiedDate = DateTime.Now;

                                presenter.SaveItem(it);
                                RadGrid_ItemsSetup.Rebind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Label l = new Label();
                l.Text = "Unable to update item. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_ItemsSetup.Controls.Add(l);
            }

        }
    }
}