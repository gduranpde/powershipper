﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Shared/Default.Master"
    CodeBehind="Projects.aspx.cs" Inherits="PowerShipping.WebSite.Admin.Projects"
    Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../Controls/ProjectFilter.ascx" TagName="ProjectFilter"
    TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <uc1:ProjectFilter ID="ucProjectFilter" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="pageDiv">
        <h3>Projects</h3>
        <telerik:RadGrid ID="RadGrid_Projects" runat="server" Width="99%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True"
            Skin="Windows7"
            OnNeedDataSource="RadGrid_Projects_NeedDataSource"
            OnUpdateCommand="RadGrid_Projects_UpdateCommand"
            OnInsertCommand="RadGrid_Projects_InsertCommand"
            OnItemCommand="RadGrid_Projects_ItemCommand"
            AllowFilteringByColumn="false">
            <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                AllowFilteringByColumn="false" AutoGenerateColumns="false" EditMode="EditForms">
                <Columns>                   
                    <telerik:GridEditCommandColumn UniqueName="EditCommandColumn">
                    </telerik:GridEditCommandColumn>
                    <telerik:GridBoundColumn DataField="ProjectName" HeaderText="Project Name" SortExpression="ProjectName"
                        UniqueName="ProjectName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                        <HeaderStyle Width="110px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ProjectCode" HeaderText="Project Code" SortExpression="ProjectCode"
                        UniqueName="ProjectCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                        <HeaderStyle Width="90px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ProgramID" HeaderText="Program ID" SortExpression="ProgramID"
                        UniqueName="ProgramID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                        <HeaderStyle Width="90px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridCheckBoxColumn DataField="IsActive" DataType="System.Boolean" DefaultInsertValue=""
                        HeaderText="Is Active" SortExpression="IsActive" UniqueName="IsActive">
                        <HeaderStyle Width="60px" />
                    </telerik:GridCheckBoxColumn>
                    <telerik:GridCheckBoxColumn DataField="ReturnsAllowed" DataType="System.Boolean"
                        DefaultInsertValue="" HeaderText="Returns Allowed" SortExpression="ReturnsAllowed"
                        UniqueName="ReturnsAllowed">
                        <HeaderStyle Width="60px" />
                    </telerik:GridCheckBoxColumn>
                    <telerik:GridCheckBoxColumn DataField="IsAllowDuplicates" DataType="System.Boolean"
                        DefaultInsertValue="" HeaderText="Is Allow <br> Duplicates" SortExpression="IsAllowDuplicates"
                        UniqueName="IsAllowDuplicates">
                        <HeaderStyle Width="60px" />
                    </telerik:GridCheckBoxColumn>
                    <telerik:GridCheckBoxColumn DataField="IsAllowDupsInProject " DataType="System.Boolean"
                        DefaultInsertValue="" HeaderText="Is Allow Dups <br> In Project" SortExpression="IsAllowDupsInProject"
                        UniqueName="IsAllowDupsInProject">
                        <HeaderStyle Width="100px" />
                    </telerik:GridCheckBoxColumn>
                    <telerik:GridBoundColumn DataField="StateName" HeaderText="State" SortExpression="StateName"
                        UniqueName="StateName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CompanyName" HeaderText="Company Name" SortExpression="CompanyName"
                        UniqueName="CompanyName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridCheckBoxColumn DataField="DuplicateExceptions " DataType="System.Boolean"
                        DefaultInsertValue="" HeaderText="Duplicate Exceptions" SortExpression="DuplicateExceptions"
                        UniqueName="DuplicateExceptions">
                        <HeaderStyle Width="100px" />
                    </telerik:GridCheckBoxColumn>
                </Columns>
                <EditFormSettings UserControlName="../Controls/ProjectCard.ascx" EditFormType="WebUserControl">
                    <EditColumn UniqueName="EditCommandColumn1">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
        </telerik:RadGrid>
    </div>

    <script type="text/javascript">

        function Expand(itemID) {
            var Grid = $find('<%=RadGrid_Projects.ClientID %>');
             var scrollArea = document.getElementById('ctl00_ContentPlaceHolder1_RadGrid_ConsumerRequests')
             var rowElement = document.getElementById(itemID);
             window.scrollTo(0, rowElement.offsetTop + 200);
         }
    </script>

</asp:Content>