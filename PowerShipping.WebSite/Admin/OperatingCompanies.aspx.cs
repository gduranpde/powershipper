﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.WebSite.Controls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Admin
{
    public partial class OperatingCompanies : System.Web.UI.Page
    {
        UserManagementPresenter presenter = new UserManagementPresenter();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Operating Companies");
            }
        }

        //get all operating companies
        protected void RadGrid_OperatingCompanies_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_OperatingCompanies.DataSource = presenter.GetOperatingCompanyByCompany(null); 
        }

        //add new record
        protected void RadGrid_OperatingCompanies_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.PerformInsertCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        if (e.Item is GridEditFormInsertItem)
                        {

                            OperatingCompany opc = new OperatingCompany();

                            object o = ((OperatingCompanyCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                            OperatingCompany ocNew = (OperatingCompany)o;

                            opc.CreatedBy = User.Identity.Name;
                            opc.LastModifiedBy = User.Identity.Name;

                            opc.CreatedDate = DateTime.Now;
                            opc.LastModifiedDate = DateTime.Now;

                            opc.OperatingCompanyID = ocNew.OperatingCompanyID;
                            opc.OperatingCompanyCode = ocNew.OperatingCompanyCode;
                            opc.OperatingCompanyName = ocNew.OperatingCompanyName;
                            opc.ParentCompanyID = ocNew.ParentCompanyID;
                            opc.ServiceState = ocNew.ServiceState;
                            opc.IsActive = ocNew.IsActive;

                            presenter.SaveOperatingCompany(opc);
                            RadGrid_OperatingCompanies.Rebind();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                Label l = new Label();
                l.Text = "Unable to insert operating company. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_OperatingCompanies.Controls.Add(l);
            }
        }


        //edit a record
        protected void RadGrid_OperatingCompanies_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        GridEditFormItem item = (GridEditFormItem)e.Item;
                        Guid id = new Guid(item.GetDataKeyValue("OperatingCompanyID").ToString());
                        if (id != Guid.Empty)
                        {
                            if (e.Item is GridEditableItem)
                            {
                                object o = ((OperatingCompanyCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                                OperatingCompany cNew = (OperatingCompany)o;

                                OperatingCompany c = presenter.GetOperatingCompanyById(cNew.OperatingCompanyID);

                                c.OperatingCompanyCode = cNew.OperatingCompanyCode;
                                c.OperatingCompanyName = cNew.OperatingCompanyName;
                                c.ParentCompanyID = cNew.ParentCompanyID;
                                c.ServiceState = cNew.ServiceState;
                                c.IsActive = cNew.IsActive;
                                c.LastModifiedBy = User.Identity.Name;
                                c.LastModifiedDate = DateTime.Now;

                                presenter.SaveOperatingCompany(c);
                                RadGrid_OperatingCompanies.Rebind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Label l = new Label();
                l.Text = "Unable to update operating company. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_OperatingCompanies.Controls.Add(l);
            }
        }


    }
}