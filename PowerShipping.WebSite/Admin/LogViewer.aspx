﻿<%@ Page Title="Log Viewer" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" CodeBehind="LogViewer.aspx.cs" Inherits="PowerShipping.WebSite.Admin.LogViewer" Theme="Default" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<div class="pageDiv">
    <h3>Log Viewer</h3>
    <br />
    <telerik:RadTabStrip  runat="server" ID="RadTabStrip_Logs" SelectedIndex="0" MultiPageID="RadMultiPage1">
            <Tabs>
                <telerik:RadTab Text="WebSite Log">                    
                </telerik:RadTab>
                 <telerik:RadTab Text="Tracker Log">
                </telerik:RadTab>
            </Tabs>
     </telerik:RadTabStrip>
    <telerik:RadMultiPage  runat="server" ID="RadMultiPage1" SelectedIndex="0" Height="830px"
                Width="90%" CssClass="multiPage">
                <telerik:RadPageView runat="server" ID="RadPageView1" >
                    <telerik:RadTextBox ID="tbSiteLog" runat="server" TextMode="MultiLine" ReadOnly="true" Width="90%" Height="800px" ></telerik:RadTextBox>
                    <br />
                    <asp:Button ID="btnRefreshSiteLog" runat="server" OnClick="btnRefreshSiteLog_Click" Text="Refresh" CssClass="ptButton"/>
                    <asp:Button ID="btnClearSiteLog" runat="server" OnClick="btnClearSiteLog_Click" Text="Clear" CssClass="ptButton"/>
                </telerik:RadPageView>
                <telerik:RadPageView runat="server" ID="RadPageView2">
                   <telerik:RadTextBox ID="tbTrackerLog" runat="server" TextMode="MultiLine" ReadOnly="true" Width="90%" Height="800px"></telerik:RadTextBox>
                    <br />
                   <asp:Button ID="btnRefreshTackerLog" runat="server" OnClick="btnRefreshTackerLog_Click" Text="Refresh" CssClass="ptButton"/>
                    <asp:Button ID="btnClearTackerLog" runat="server" OnClick="btnClearTackerLog_Click" Text="Clear" CssClass="ptButton"/>
                </telerik:RadPageView>
     </telerik:RadMultiPage>
     <br />
     <asp:Label runat="server" ID="labelErrors" ForeColor="Red" />
   </div>
</asp:Content>