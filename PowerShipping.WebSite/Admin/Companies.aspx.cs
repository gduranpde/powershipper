﻿using System;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.WebSite.Controls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Admin
{
    public partial class Companies : System.Web.UI.Page
    {
        UserManagementPresenter presenter = new UserManagementPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Companies");
            }
        }

        protected void RadGrid_Companies_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_Companies.DataSource = presenter.GetCompanyByUser(null);
        }

        protected void RadGrid_Companies_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.PerformInsertCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        if (e.Item is GridEditFormInsertItem)
                        {
                            Company c = new Company();
                            //c.Id = Guid.NewGuid();

                            object o = ((CompanyCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                            Company cNew = (Company)o;

                            c.Id = cNew.Id;
                            c.CompanyName = cNew.CompanyName;
                            c.CompanyCode = cNew.CompanyCode;

                            c.CompanyLogo = cNew.CompanyLogo;
                            c.IsActive = cNew.IsActive;
                            
                            presenter.SaveCompany(c);
                            RadGrid_Companies.Rebind();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                Label l = new Label();
                l.Text = "Unable to insert company. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_Companies.Controls.Add(l);
            }
        }

        protected void RadGrid_Companies_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        GridEditFormItem item = (GridEditFormItem)e.Item;
                        Guid id = new Guid(item.GetDataKeyValue("Id").ToString());
                        if (id != Guid.Empty)
                        {
                            if (e.Item is GridEditableItem)
                            {
                                object o = ((CompanyCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                                Company cNew = (Company)o;

                                Company c = presenter.GetCompanyById(cNew.Id);

                                c.CompanyName = cNew.CompanyName;
                                c.CompanyCode = cNew.CompanyCode;

                                c.CompanyLogo = cNew.CompanyLogo;
                                c.IsActive = cNew.IsActive;

                                presenter.SaveCompany(c);
                                RadGrid_Companies.Rebind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Label l = new Label();
                l.Text = "Unable to update company. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_Companies.Controls.Add(l);
            }
        }
    }
}
