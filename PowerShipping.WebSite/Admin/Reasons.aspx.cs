﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.WebSite.Controls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Admin
{
    public partial class Reasons : System.Web.UI.Page
    {
        private readonly UserManagementPresenter presenter = new UserManagementPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Reasons");
            }
        }

        protected void RadGrid_Reasons_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_Reasons.DataSource = presenter.GetAllReasons();
        }

        protected void RadGrid_Reasons_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.PerformInsertCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        if (e.Item is GridEditFormInsertItem)
                        {
                            var r = new Reason();
                            r.ReasonID = Guid.NewGuid();

                            object o =
                                ((ReasonCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                            var rNew = (Reason)o;

                            r.ReasonCode = rNew.ReasonCode;
                            r.ReasonDescription = rNew.ReasonDescription;

                            presenter.AddReasons(r);
                            RadGrid_Reasons.Rebind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to insert reason. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                RadGrid_Reasons.Controls.Add(l);
            }
        }

        protected void RadGrid_Reasons_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        var item = (GridEditFormItem)e.Item;
                        var id = new Guid(item.GetDataKeyValue("ReasonID").ToString());
                        if (id != Guid.Empty)
                        {
                            if (e.Item is GridEditableItem)
                            {
                                object o =
                                    ((ReasonCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).
                                        FillEntity();
                                var rNew = (Reason)o;

                                Reason r = presenter.GetReasonById(rNew.ReasonID);

                                r.ReasonCode = rNew.ReasonCode;
                                r.ReasonDescription = rNew.ReasonDescription;
                              
                                presenter.SaveReasons(r);
                                RadGrid_Reasons.Rebind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var l = new Label();
                l.Text = "Unable to update reason. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                RadGrid_Reasons.Controls.Add(l);
            }
        }
    }
}
