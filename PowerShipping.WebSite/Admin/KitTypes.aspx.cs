﻿using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.WebSite.Controls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Admin
{
    public partial class KitTypes : Page
    {
        private KitTypesPresenter presenter = new KitTypesPresenter();
        private UserManagementPresenter presenterAdv = new UserManagementPresenter();

        private string _kitTypeId = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Kit Types");

                _kitTypeId = Page.Request.QueryString["KitTypeId"];
            }
        }

        protected void RadGrid_KitType_OnPreRender(object sender, EventArgs e)
        {
           
        }

        protected void RadGrid_KitType_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item is GridDataItem) && !e.Item.IsInEditMode)
            {
                GridDataItem gridItem = (GridDataItem)e.Item;

                if (((KitType)e.Item.DataItem).Id.ToString() == _kitTypeId)
                {
                    e.Item.Edit = true;   
                }
            }
        }

        protected void RadGrid_KitType_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_KitType.DataSource = presenter.GetAll();
        }

        protected void RadGrid_KitType_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.PerformInsertCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        if (e.Item is GridEditFormInsertItem)
                        {
                            var k = new KitType();
                            k.Id = Guid.NewGuid();

                            object o =
                                ((KitTypeCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                            var kNew = (KitType)o;

                            k.KitName = kNew.KitName;
                            k.KitContents = kNew.KitContents;
                            k.Height = kNew.Height;
                            k.Length = kNew.Length;
                            k.Weight = kNew.Weight;
                            //PG31
                            k.Kw_Savings = kNew.Kw_Savings;
                            k.Kwh_Savings = kNew.Kwh_Savings;
                            //PG31
                            k.Width = kNew.Width;

                            k.KitItems = kNew.KitItems;

                            presenter.AddKitType(k);
                            presenterAdv.SaveKitTypeKitItems(k, true);

                            RadGrid_KitType.Rebind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to insert KitType. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                RadGrid_KitType.Controls.Add(l);
            }
        }

        protected void RadGrid_KitType_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        var item = (GridEditFormItem)e.Item;
                        var id = new Guid(item.GetDataKeyValue("Id").ToString());
                        if (id != Guid.Empty)
                        {
                            if (e.Item is GridEditableItem)
                            {
                                object o =
                                    ((KitTypeCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).
                                        FillEntity();
                                var kNew = (KitType)o;

                                KitType k = presenter.GetOne(kNew.Id);

                                k.KitName = kNew.KitName;
                                k.KitContents = kNew.KitContents;
                                k.Height = kNew.Height;
                                k.Length = kNew.Length;
                                k.Weight = kNew.Weight;
                                //PG31
                                k.Kw_Savings = kNew.Kw_Savings;
                                k.Kwh_Savings = kNew.Kwh_Savings;
                                //PG31
                                k.Width = kNew.Width;

                                k.KitItems = kNew.KitItems;

                                presenter.SaveKitType(k);
                                presenterAdv.SaveKitTypeKitItems(k, true);

                                RadGrid_KitType.Rebind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var l = new Label();
                l.Text = "Unable to update KitType. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                RadGrid_KitType.Controls.Add(l);
            }
        }
    }
}