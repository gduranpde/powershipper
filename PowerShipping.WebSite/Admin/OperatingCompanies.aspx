﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Shared/Default.Master"
    CodeBehind="OperatingCompanies.aspx.cs" Inherits="PowerShipping.WebSite.Admin.OperatingCompanies"
    Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="pageDiv">
        <h3>Operating Companies</h3>
        <telerik:RadGrid ID="RadGrid_OperatingCompanies" runat="server" Width="99%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True"
            Skin="Windows7"
            OnNeedDataSource="RadGrid_OperatingCompanies_NeedDataSource"
            OnUpdateCommand="RadGrid_OperatingCompanies_UpdateCommand"
            OnInsertCommand="RadGrid_OperatingCompanies_InsertCommand"
            AllowFilteringByColumn="false">
            <MasterTableView DataKeyNames="OperatingCompanyID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                AllowFilteringByColumn="false" AutoGenerateColumns="false" EditMode="EditForms">
                <Columns>
                    <telerik:GridEditCommandColumn UniqueName="EditCommandColumn">
                        <HeaderStyle Width="30px" />
                    </telerik:GridEditCommandColumn>

                    <telerik:GridBoundColumn DataField="OperatingCompanyCode" HeaderText="Operating Company Code" SortExpression="OperatingCompanyCode"
                        UniqueName="OperatingCompanyCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                        <HeaderStyle Width="100px" />
                    </telerik:GridBoundColumn>

                    <telerik:GridBoundColumn DataField="OperatingCompanyName" HeaderText="Operating Company Name" SortExpression="OperatingCompanyName"
                        UniqueName="OperatingCompanyName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                        <HeaderStyle Width="130px" />
                    </telerik:GridBoundColumn>

                    <telerik:GridBoundColumn DataField="CompanyName" HeaderText="Parent Company" SortExpression="CompanyName"
                        UniqueName="CompanyName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                        <HeaderStyle Width="150px" />
                    </telerik:GridBoundColumn>

                    <telerik:GridBoundColumn DataField="StateName" HeaderText="Service State" SortExpression="StateName"
                        UniqueName="StateName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" >
                        <HeaderStyle Width="70px" />
                    </telerik:GridBoundColumn>

                    <telerik:GridCheckBoxColumn DataField="IsActive" DataType="System.Boolean" DefaultInsertValue=""
                        HeaderText="Is Active" SortExpression="IsActive" UniqueName="IsActive">
                        <HeaderStyle Width="50px" />
                    </telerik:GridCheckBoxColumn>

                </Columns>
                <EditFormSettings UserControlName="../Controls/OperatingCompanyCard.ascx" EditFormType="WebUserControl">
                    <EditColumn UniqueName="EditCommandColumn1">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
        </telerik:RadGrid>
    </div>

</asp:Content>
