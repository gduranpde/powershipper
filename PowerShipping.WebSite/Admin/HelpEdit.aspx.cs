﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using PowerShipping.Business.Presenters;

namespace PowerShipping.WebSite.Admin
{
    public partial class HelpEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DirectoryInfo di = new DirectoryInfo(Server.MapPath("~/Help"));
                DirectoryInfo diPagesAdmin = new DirectoryInfo(Server.MapPath("~/Admin"));
                DirectoryInfo diPagesHome = new DirectoryInfo(Server.MapPath("~/Home"));
                DirectoryInfo diPagesRoot = new DirectoryInfo(Server.MapPath("~"));

                CreateTxtFiles(diPagesRoot, di);
                CreateTxtFiles(diPagesHome, di);
                CreateTxtFiles(diPagesAdmin, di);

                RadComboBox_Files.DataTextField = "Name";
                RadComboBox_Files.DataSource = di.GetFiles();
                RadComboBox_Files.DataBind();
            }

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Help Editor");
            }
        }

        private void CreateTxtFiles(DirectoryInfo diPagesRoot, DirectoryInfo di)
        {
            FileInfo[] listPages = diPagesRoot.GetFiles("*.aspx");
            foreach (FileInfo info in listPages)
            {
                if(!File.Exists(Path.Combine(di.FullName, info.Name.Replace(".aspx", ".txt"))))
                {
                    File.Create(Path.Combine(di.FullName, info.Name.Replace(".aspx", ".txt")));
                }
            }
        }

        protected void RadComboBox_Files_SelectedIndexChanged(object o, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {

        }

        protected void Button_Select_Click(object sender, EventArgs e)
        {
            this.HiddenField_HelpFile.Value = this.RadComboBox_Files.SelectedItem.Text;
            StreamReader sr = new StreamReader(Server.MapPath(string.Format("~/Help/{0}", this.RadComboBox_Files.SelectedItem.Text)));
            string content = sr.ReadToEnd();

            sr.Close();
            this.RadEditor1.Content = content;
        }

        protected void Button_SaveContent_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.HiddenField_HelpFile.Value))
            {
                try
                {
                    string newContent = this.RadEditor1.Content;
                    //if (File.Exists(Server.MapPath(string.Format("~/Help/{0}", this.HiddenField_HelpFile.Value))))
                    //    File.Delete(Server.MapPath(string.Format("~/Help/{0}", this.HiddenField_HelpFile.Value)));

                    StreamWriter sw = new StreamWriter(Server.MapPath(string.Format("~/Help/{0}", this.HiddenField_HelpFile.Value)));
                    sw.Write(newContent);
                    sw.Close();
                }
                catch(Exception ex) { }
            }
            
        } 
    }
}
