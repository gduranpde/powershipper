﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Shared/Default.Master" CodeBehind="KitItems.aspx.cs" Inherits="PowerShipping.WebSite.Admin.KitItems" Theme="Default" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<div class="pageDiv">
    <h3>Kit Items</h3>
    
    <br />   
    <asp:LinkButton ID="lnkBtnBackToKit" runat="server" Text="Back to Kit" OnClick="lnkBtnBackToKit_OnClick"></asp:LinkButton>
    <br />
     <br />
     <telerik:RadGrid ID="RadGrid_Items" runat="server" Width="50%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True" 
              Skin="Windows7" 
                onneeddatasource="RadGrid_Items_NeedDataSource"        
                OnUpdateCommand="RadGrid_Items_UpdateCommand"        
                OnInsertCommand="RadGrid_Items_InsertCommand"                               
                AllowFilteringByColumn="false">
            <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
            AllowFilteringByColumn="false" AutoGenerateColumns="false" EditMode="EditForms">        
            <Columns> 
                <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" >
                    </telerik:GridEditCommandColumn>                 
               
                <telerik:GridBoundColumn DataField="Name" HeaderText="Name" SortExpression="Name"
                        UniqueName="Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" >
                        </telerik:GridBoundColumn>                 
                  <telerik:GridBoundColumn DataField="CatalogID" HeaderText="CatalogID" SortExpression="CatalogID"
                        UniqueName="CatalogID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn> 
                  <telerik:GridBoundColumn DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity"
                        UniqueName="Quantity" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>   
                 <telerik:GridBoundColumn DataField="Kw_Impact" HeaderText="Kw Impact" SortExpression="Kw_Impact"
                        UniqueName="Kw_Impact" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn> 
                <telerik:GridBoundColumn DataField="Kwh_Impact" HeaderText="Kwh Impact" SortExpression="Kwh_Impact"
                        UniqueName="Kwh_Impact" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>                              
            </Columns>              
            <EditFormSettings UserControlName="../Controls/KitItemCard.ascx" EditFormType="WebUserControl">
                <EditColumn UniqueName="EditCommandColumn1">
                </EditColumn>
             </EditFormSettings>
            </MasterTableView>
    </telerik:RadGrid>
    </div>
</asp:Content>

