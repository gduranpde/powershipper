﻿using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.WebSite.Controls;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Admin
{
    public partial class Projects : System.Web.UI.Page
    {
        UserManagementPresenter presenter = new UserManagementPresenter();

        protected void Page_Init(object sender, EventArgs e)
        {
            ucProjectFilter.OnFilter += ucProjectFilter_OnFilter;
            ucProjectFilter.OnClear += ucProjectFilter_OnClear;
            Session["NewLabelRequestIds"] = null;
            Session["NewLabelRequestProjectId"] = null;
        }

        void ucProjectFilter_OnClear()
        {
            RadGrid_Projects.DataSource = presenter.GetProjectByCompany(null);
            RadGrid_Projects.Rebind();
        }

        void ucProjectFilter_OnFilter(List<Project> projects)
        {
            RadGrid_Projects.DataSource = projects;
            RadGrid_Projects.Rebind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Projects");

                Session["addedProjectDuplicateExList"] = new List<Project>();
                Session["removedProjectDuplicateExList"] = new List<Project>();
            }

        }

        protected void RadGrid_Projects_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_Projects.DataSource = presenter.GetProjectByCompany(null);
        }

        protected void RadGrid_Projects_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.PerformInsertCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        if (e.Item is GridEditFormInsertItem)
                        {
                            Project p = new Project();
                            p.Id = Guid.NewGuid();

                            object o = ((ProjectCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                            Project pNew = (Project)o;

                            p.CompanyId = pNew.CompanyId;
                            p.ProjectName = pNew.ProjectName;
                            p.ProjectCode = pNew.ProjectCode;
                            p.IsActive = pNew.IsActive;
                            p.IsAllowDuplicates = pNew.IsAllowDuplicates;
                            p.IsAllowDupsInProject = pNew.IsAllowDupsInProject;
                            p.ReturnsAllowed = pNew.ReturnsAllowed;

                            p.States = pNew.States;
                            p.ProgramID = pNew.ProgramID;
                            p.StateId = pNew.StateId;

                            if ((p.IsAllowDuplicates == false))
                            {
                                p.DuplicateExceptions = pNew.DuplicateExceptions;
                            }

                            presenter.SaveProject(p);
                            presenter.SaveProjectState(p);

                            RadGrid_Projects.Rebind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                Label l = new Label();
                l.Text = "Unable to insert project. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_Projects.Controls.Add(l);
            }
        }

        protected void RadGrid_Projects_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        GridEditFormItem item = (GridEditFormItem)e.Item;
                        Guid id = new Guid(item.GetDataKeyValue("Id").ToString());
                        if (id != Guid.Empty)
                        {
                            if (e.Item is GridEditableItem)
                            {
                                object o = ((ProjectCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                                Project currentProject = (Project)o;

                                Project p = presenter.GetProjectById(currentProject.Id);

                                p.CompanyId = currentProject.CompanyId;
                                p.ProjectName = currentProject.ProjectName;
                                p.ProjectCode = currentProject.ProjectCode;
                                p.IsActive = currentProject.IsActive;
                                p.IsAllowDuplicates = currentProject.IsAllowDuplicates;
                                p.IsAllowDupsInProject = currentProject.IsAllowDupsInProject;
                                p.ReturnsAllowed = currentProject.ReturnsAllowed;
                                p.States = currentProject.States;
                                p.ProgramID = currentProject.ProgramID;
                                if (currentProject.StateId == -1)
                                    throw new Exception("Please select a State");
                                p.StateId = currentProject.StateId;
                                //if exists exProj and isAllowDups == false
                                p.ExceptionProjects = presenter.GetAllSelectedProjects(p.Id);

                                if (p.ExceptionProjects != null)
                                {
                                    if (p.IsAllowDuplicates == false)
                                    {
                                        p.DuplicateExceptions = currentProject.DuplicateExceptions;
                                    }
                                    else
                                    {
                                        foreach (var proj in p.ExceptionProjects)
                                        {
                                            presenter.RemoveProjectFromProjectDuplicateExceptions(p.Id, proj.Id);
                                        }
                                    }
                                }

                                if (currentProject.AddedExceptionProjects != null)
                                {
                                    foreach (Project proj in currentProject.AddedExceptionProjects)
                                    {
                                        presenter.AddProjectToProjectDuplicateExceptions(Guid.NewGuid(), p.Id, proj.Id);
                                    }
                                    Session["addedProjectDuplicateExList"] = new List<Project>();
                                }

                                if (currentProject.RemovedExceptionProjects != null)
                                {
                                    foreach (Project proj in currentProject.RemovedExceptionProjects)
                                    {
                                        presenter.RemoveProjectFromProjectDuplicateExceptions(p.Id, proj.Id);
                                    }
                                    Session["removedProjectDuplicateExList"] = new List<Project>();
                                }

                                presenter.SaveProject(p);
                                presenter.SaveProjectState(p);
                                RadGrid_Projects.Rebind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Label l = new Label();
                l.Text = "Unable to update project. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_Projects.Controls.Add(l);
            }
        }

        protected void RadGrid_Projects_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.EditCommandName)
            {
                RadGrid_Projects.MasterTableView.IsItemInserted = false;
                if (e.Item is GridDataItem)
                {
                    ScrollToItem(e);
                }
            }
            else
                if ((e.CommandSource is Button && (e.CommandSource as Button).Text == "Add"))
                {
                    ScrollToItem(e);
                }

            if ((e.CommandSource is Button && (e.CommandSource as Button).Text == "Remove"))
            {
                ScrollToItem(e);
            }
            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                RadGrid_Projects.MasterTableView.ClearEditItems();
            }
        }

        private void ScrollToItem(GridCommandEventArgs e)
        {
            if (e.Item != null)
            {
                GridDataItem item = (GridDataItem)e.Item;
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scroll", "setTimeout(function(){ Expand('" + item.ClientID + "');}, 100);", true);

            }
        }
    }
}