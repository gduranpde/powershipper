﻿using System;
using PowerShipping.Business.Presenters;

namespace PowerShipping.WebSite.Admin
{
    public partial class FedExShippingInfo : System.Web.UI.Page
    {
        ConsumerRequestsPresenter presenter = new ConsumerRequestsPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ShipmentDetails_Details.InitData();
            }

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Default FedEx Shipping Information");
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                lbMessages.ControlStyle.ForeColor = System.Drawing.Color.Black;
                lbMessages.Text = "";

                presenter.SaveFedExShippingDefault(ShipmentDetails_Details.GetEntity());

                lbMessages.ControlStyle.ForeColor = System.Drawing.Color.Green;
                lbMessages.Text = "Updated";
            }
            catch (Exception ex)
            {
                Log.LogError(ex); 

                lbMessages.ControlStyle.ForeColor = System.Drawing.Color.Red;
                lbMessages.Text = ex.Message;
            }
        }
    }
}
