﻿using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.WebSite.Controls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Admin
{
    public partial class KitSetup : System.Web.UI.Page
    {
        private KitTypesPresenter presenter = new KitTypesPresenter();
        private UserManagementPresenter presenterAdv = new UserManagementPresenter();

        private string _kitTypeId = null;

        protected void Page_Init(object sender, EventArgs e)
        {
            ucKitSetupFilter.OnFilter += ucKitSetupFilter_OnFilter;
            ucKitSetupFilter.OnClear += ucKitSetupFilter_OnClear;
        }

        void ucKitSetupFilter_OnClear()
        {
            RadGrid_KitSetup.DataSource = presenter.GetKitTypeByKitName(null);
            RadGrid_KitSetup.Rebind();
        }

        void ucKitSetupFilter_OnFilter(List<KitType> kitTypes)
        {
            RadGrid_KitSetup.DataSource = kitTypes;
            Session["kitTypes"] = kitTypes;
            RadGrid_KitSetup.Rebind();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //return to selected page index 
                int CurrentPageIndex = 0;
                if (Session["CurrentPageIndex"] != null)
                {
                    int.TryParse(Convert.ToString(Session["CurrentPageIndex"]), out CurrentPageIndex);
                }

                RadGrid_KitSetup.CurrentPageIndex = CurrentPageIndex;

                //maintain page size after navigating
                int CurrentPageSize = 15;
                if (Session["CurrentPageSize"] != null)
                {
                    int.TryParse(Convert.ToString(Session["CurrentPageSize"]), out CurrentPageSize);

                }
                RadGrid_KitSetup.PageSize = CurrentPageSize;

                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Kit Setup");

                _kitTypeId = Page.Request.QueryString["KitTypeId"];
            }
        }

        protected void RadGrid_KitSetup_OnPreRender(object sender, EventArgs e)
        {

        }

        protected void RadGrid_KitSetup_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item is GridDataItem) && !e.Item.IsInEditMode)
            {

                GridDataItem gridItem = (GridDataItem)e.Item;
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scroll", "setTimeout(function(){ Expand('" + gridItem.ClientID + "');}, 100);", true);
                if (((KitType)e.Item.DataItem).Id.ToString() == _kitTypeId)
                {
                    e.Item.Edit = true;
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scroll", "setTimeout(function(){ Expand('" + gridItem.ClientID + "');}, 100);", true);
                    Session.Clear();
                }
            }
        }

        protected void RadGrid_KitSetup_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (Session["kitTypes"] == null)
            {
               // List<KitType> kitTypes = presenter.GetAll();
                //update KitContents field
                RadGrid_KitSetup.DataSource = presenter.GetAllUpdated();//.GetAllUpdated();
            }
            else
            {
                List<KitType> list = (List<KitType>)Session["kitTypes"];
                RadGrid_KitSetup.DataSource = list;
            }
        }


        protected void RadGrid_KitSetup_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {


                if (e.CommandName == RadGrid.PerformInsertCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        if (e.Item is GridEditFormInsertItem)
                        {
                            var k = ((KitSetupCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                            presenter.AddKitType(k);
                            RadGrid_KitSetup.Rebind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to insert KitSetup. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                RadGrid_KitSetup.Controls.Add(l);
            }
        }


        protected void RadGrid_KitSetup_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        var item = (GridEditFormItem)e.Item;
                        var id = new Guid(item.GetDataKeyValue("Id").ToString());
                        if (id != Guid.Empty)
                        {
                            if (e.Item is GridEditableItem)
                            {
                                object o =
                                    ((KitSetupCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).
                                        FillEntity();
                                var kNew = (KitType)o;

                                KitType k = presenter.GetOne(kNew.Id);

                                k.KitName = kNew.KitName;
                                k.KitContents = kNew.KitContents;
                                k.Height = kNew.Height;
                                k.Length = kNew.Length;
                                k.Weight = kNew.Weight;
                                k.Width = kNew.Width;
                                k.Items = kNew.Items;
                                k.IsActive = kNew.IsActive;

                                presenter.SaveKitType(k);
                                //presenterAdv.SaveKitTypeItems(k, true);

                                RadGrid_KitSetup.Rebind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var l = new Label();
                l.Text = "Unable to update KitSetup. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                RadGrid_KitSetup.Controls.Add(l);
            }
        }

        protected void RadGrid_KitSetup_ItemCommand(object source, GridCommandEventArgs e)
        {
            Session["CurrentPageIndex"] = RadGrid_KitSetup.CurrentPageIndex;
            Session["CurrentPageSize"] = RadGrid_KitSetup.PageSize;

            if (e.CommandName == RadGrid.EditCommandName)
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scroll", "setTimeout(function(){ Expand('" + item.ClientID + "');}, 100);", true);
                }
            }
        }


    }
}