﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Shared/Default.Master" CodeBehind="Reasons.aspx.cs" Inherits="PowerShipping.WebSite.Admin.Reasons" Theme="Default" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<div class="pageDiv">
    <h3>Reasons</h3>
    
     <telerik:RadGrid ID="RadGrid_Reasons" runat="server" Width="750px" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True" 
              Skin="Windows7" 
                onneeddatasource="RadGrid_Reasons_NeedDataSource"        
                OnUpdateCommand="RadGrid_Reasons_UpdateCommand"        
                OnInsertCommand="RadGrid_Reasons_InsertCommand"                
                AllowFilteringByColumn="false">
            <MasterTableView DataKeyNames="ReasonID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
            AllowFilteringByColumn="false" AutoGenerateColumns="false" EditMode="EditForms">        
            <Columns> 
                <telerik:GridEditCommandColumn UniqueName="EditCommandColumn">
                <HeaderStyle Width="40px" Font-Italic="true"/>
                <ItemStyle Font-Italic="true"/>                
                    </telerik:GridEditCommandColumn>
               <telerik:GridBoundColumn DataField="ReasonCode" HeaderText="ReasonCode" SortExpression="ReasonCode"
                        UniqueName="ReasonCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" >
                         <HeaderStyle Width="80px" Font-Italic="true"/>
                         <ItemStyle Font-Italic="true"/> 
                        </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ReasonDescription" HeaderText="Description" SortExpression="ReasonDescription"
                        UniqueName="ReasonDescription" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" >
                        <HeaderStyle Width="80px"/>
                        </telerik:GridBoundColumn>                 
            </Columns>              
            <EditFormSettings UserControlName="../Controls/ReasonCard.ascx" EditFormType="WebUserControl">
                <EditColumn UniqueName="EditCommandColumn1">
                </EditColumn>
             </EditFormSettings>
            </MasterTableView>
    </telerik:RadGrid>
    </div>
</asp:Content>
