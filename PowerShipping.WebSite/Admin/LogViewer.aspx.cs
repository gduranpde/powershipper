﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web.UI;
using PowerShipping.Business.Presenters;

namespace PowerShipping.WebSite.Admin
{
    public partial class LogViewer : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadSiteLog();
                LoadTrackerLog();
            }

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Log Viewer");
            }
        }

        protected void btnRefreshSiteLog_Click(object sender, EventArgs e)
        {
            LoadSiteLog();
        }

        protected void btnClearSiteLog_Click(object sender, EventArgs e)
        {
            try
            {
                labelErrors.Text = "";

                StreamWriter fp;
                fp = File.CreateText(ConfigurationManager.AppSettings["LogPath"]);

                fp.WriteLine("");
                fp.Close();

                LoadSiteLog();
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                labelErrors.Text = ex.Message;
            }
        }

        protected void btnRefreshTackerLog_Click(object sender, EventArgs e)
        {
            LoadTrackerLog();
        }

        protected void btnClearTackerLog_Click(object sender, EventArgs e)
        {
            try
            {
                //string newPath = GetNewTrackerLogPath();

                labelErrors.Text = "";

                StreamWriter fp;
                fp = File.CreateText(ConfigurationManager.AppSettings["TrackerLogFile"]);
                //fp = File.CreateText(newPath);

                fp.WriteLine("");
                fp.Close();

                LoadTrackerLog();
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                labelErrors.Text = ex.Message;
            }
        }

        private void LoadSiteLog()
        {
            try
            {
                labelErrors.Text = "";
                tbSiteLog.Text = "";

                List<string> list = ReadFile(ConfigurationManager.AppSettings["LogPath"]);
                foreach (string s in list)
                {
                    tbSiteLog.Text = tbSiteLog.Text + s + "\r\n";
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                labelErrors.Text = ex.Message;
            }
        }

        private void LoadTrackerLog()
        {
            try
            {
                //copy log to site folder then read
                string newPath = GetNewTrackerLogPath();

                File.Copy(ConfigurationManager.AppSettings["TrackerLogFile"], newPath, true);

                labelErrors.Text = "";
                tbTrackerLog.Text = "";

                //List<string> list = ReadFile(ConfigurationManager.AppSettings["TrackerLogFile"]);
                List<string> list = ReadFile(newPath);
                foreach (string s in list)
                {
                    tbTrackerLog.Text = tbTrackerLog.Text + s + "\r\n";
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                labelErrors.Text = ex.Message;
            }
        }

        private string GetNewTrackerLogPath()
        {
            string newPath = "";
            newPath = ConfigurationManager.AppSettings["LogPath"].Substring(0,
                                                                            ConfigurationManager.AppSettings[
                                                                                "LogPath"].LastIndexOf('\\'));
            newPath = Path.Combine(newPath, "PowerShipping.Tracker.log");

            return newPath;
        }

        private List<string> ReadFile(string file)
        {
            var list = new List<string>();

            FileStream fs = File.OpenRead(file);
            var sr = new StreamReader(fs);

            while (sr.Peek() > -1)
            {
                list.Add(sr.ReadLine());
            }
            sr.Close();
            fs.Close();

            return list;
        }
    }
}