﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Shared/Default.Master"  CodeBehind="Shippers.aspx.cs" Inherits="PowerShipping.WebSite.Admin.Shippers" Theme="Default" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<div class="pageDiv">
    <h3>Shippers</h3>
    
     <telerik:RadGrid ID="RadGrid_Shippers" runat="server" Width="100%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True" 
              Skin="Windows7" 
                onneeddatasource="RadGrid_Shippers_NeedDataSource"        
                OnUpdateCommand="RadGrid_Shippers_UpdateCommand"        
                OnInsertCommand="RadGrid_Shippers_InsertCommand"                
                AllowFilteringByColumn="false">
            <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
            AllowFilteringByColumn="false" AutoGenerateColumns="false" EditMode="EditForms">        
            <Columns> 
                <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" >
                <HeaderStyle Width="40px"/>
                    </telerik:GridEditCommandColumn>
               <telerik:GridBoundColumn DataField="ShipperCode" HeaderText="ShipperCode" SortExpression="ShipperCode"
                        UniqueName="ShipperCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" >
                         <HeaderStyle Width="80px"/>
                        </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ShipperName" HeaderText="ShipperName" SortExpression="ShipperName"
                        UniqueName="ShipperName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" >
                        <HeaderStyle Width="80px"/>
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="PackageTrackingUrl" HeaderText="Package Tracking Url" SortExpression="PackageTrackingUrl"
                        UniqueName="PackageTrackingUrl" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        <HeaderStyle Width="300px"/>
                        </telerik:GridBoundColumn> 
                  <telerik:GridBoundColumn DataField="TransactionFileUrl" HeaderText="Transaction File Url" SortExpression="TransactionFileUrl"
                        UniqueName="TransactionFileUrl" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        <HeaderStyle Width="300px"/>
                        </telerik:GridBoundColumn> 
                 <telerik:GridBoundColumn DataField="BatchTrackingCodeModule" HeaderText="Batch Tracking Code Module" SortExpression="BatchTrackingCodeModule"
                        UniqueName="BatchTrackingCodeModule" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        <HeaderStyle Width="170px"/>
                        </telerik:GridBoundColumn>
            </Columns>              
            <EditFormSettings UserControlName="../Controls/ShipperCard.ascx" EditFormType="WebUserControl">
                <EditColumn UniqueName="EditCommandColumn1">
                </EditColumn>
             </EditFormSettings>
            </MasterTableView>
    </telerik:RadGrid>
    </div>
</asp:Content>
