﻿<%@ Page Title="User management" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" CodeBehind="UserManagement.aspx.cs" Inherits="PowerShipping.WebSite.Admin.UserManagement" Theme="Default" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<div class="pageDiv">
    <h3>User Management</h3>
    
    <telerik:RadGrid ID="RadGrid_Users" runat="server" Width="50%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True" 
              Skin="Windows7" 
        onneeddatasource="RadGrid_Users_NeedDataSource"        
        OnUpdateCommand="RadGrid_Users_UpdateCommand"        
        OnInsertCommand="RadGrid_Users_InsertCommand"
        OnDeleteCommand="RadGrid_Users_DeleteCommand"
        AllowFilteringByColumn="True">
            <MasterTableView DataKeyNames="UserId" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
            AllowFilteringByColumn="True" AutoGenerateColumns="false" EditMode="EditForms"
            >        
            <Columns>             
                
                <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" >
                    </telerik:GridEditCommandColumn>
               <%-- <telerik:GridButtonColumn UniqueName="DeleteColumn" Text="Delete" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmText="Are you sure delete this row?"/>
                --%>
                <telerik:GridBoundColumn DataField="UserName" HeaderText="UserName" SortExpression="UserName"
                        UniqueName="UserName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" >
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Password" HeaderText="Password" SortExpression="Password"
                        UniqueName="Password" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" Visible="false">
                        </telerik:GridBoundColumn>
                        
                 <%--<telerik:GridCheckBoxColumn DataField="IsAdmin" DataType="System.Boolean" DefaultInsertValue=""
                    HeaderText="IsAdmin" SortExpression="IsAdmin" UniqueName="IsAdmin" Visible="false">
                 </telerik:GridCheckBoxColumn>--%>
                                 
            </Columns>
               <%--<EditFormSettings ColumnNumber="3" CaptionFormatString="Edit details for User"
                CaptionDataField="UserId">
                <FormTableItemStyle Wrap="False"></FormTableItemStyle>
                <FormCaptionStyle CssClass="EditFormHeader"></FormCaptionStyle>
                <FormMainTableStyle GridLines="None" CellSpacing="0" CellPadding="3" Width="100%" />
                <FormTableStyle CellSpacing="0" CellPadding="2" CssClass="module" GridLines="Both"
                    Height="110px" Width="100%"  />
                <FormTableAlternatingItemStyle Wrap="False"></FormTableAlternatingItemStyle>
                <FormStyle Width="100%" BackColor="#eef2ea"></FormStyle>
                <EditColumn UpdateText="Save" UniqueName="EditCommandColumn1" CancelText="Cancel">
                </EditColumn>                
                <FormTableButtonRowStyle HorizontalAlign="Left"></FormTableButtonRowStyle>
            </EditFormSettings>--%>
            <EditFormSettings UserControlName="../Controls/UserManagementCard.ascx" EditFormType="WebUserControl">
                <EditColumn UniqueName="EditCommandColumn1">
                </EditColumn>
             </EditFormSettings>
            </MasterTableView>
    </telerik:RadGrid>
    </div>
</asp:Content>
