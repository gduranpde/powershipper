﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Shared/Default.Master" CodeBehind="KitContents.aspx.cs" Inherits="PowerShipping.WebSite.Admin.KitContents" %>


<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="pageDiv">
        <h3><%= PageTitle %></h3>

        <br />
        <asp:LinkButton ID="lnkBtnBackToKit" runat="server" Text="Back to Kit" OnClick="lnkBtnBackToKit_OnClick"></asp:LinkButton>
        <br />
        <br />
        <telerik:RadGrid ID="RadGrid_Items" runat="server" Width="99%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True"
            Skin="Windows7"
            OnNeedDataSource="RadGrid_Items_NeedDataSource"
            OnUpdateCommand="RadGrid_Items_UpdateCommand"
            OnInsertCommand="RadGrid_Items_InsertCommand"
            AllowFilteringByColumn="false"
            OnItemCommand="RadGrid_Items_ItemCommand">
            <MasterTableView DataKeyNames="KitContentID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                AllowFilteringByColumn="false" AutoGenerateColumns="false" EditMode="EditForms">                
                <Columns>
                    <telerik:GridEditCommandColumn UniqueName="EditCommandColumn">
                    </telerik:GridEditCommandColumn>

                    <telerik:GridBoundColumn DataField="ItemName" HeaderText="Name" SortExpression="ItemName"
                        UniqueName="ItemName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ItemDescription" HeaderText="Description" SortExpression="ItemDescription"
                        UniqueName="ItemDescription" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" FooterStyle-Wrap="true" ItemStyle-Wrap="true">
                        <HeaderStyle Width="600px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity"
                        UniqueName="Quantity" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DefaultkwImpact" HeaderText="Default Kw Impact" SortExpression="DefaultkwImpact"
                        UniqueName="DefaultkwImpact" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DefaultkwhImpact" HeaderText="Default Kwh Impact" SortExpression="DefaultkwhImpact"
                        UniqueName="DefaultkwhImpact" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings UserControlName="../Controls/KitContentsCard.ascx" EditFormType="WebUserControl">
                    <EditColumn UniqueName="EditCommandColumn1">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <ClientSettings AllowKeyboardNavigation="true">
                <KeyboardNavigationSettings EnableKeyboardShortcuts="true" AllowSubmitOnEnter="true"></KeyboardNavigationSettings>
            </ClientSettings>
        </telerik:RadGrid>
    </div>
</asp:Content>
