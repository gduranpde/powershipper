﻿<%@ Page Title="Kit Types" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" CodeBehind="KitTypes.aspx.cs" Inherits="PowerShipping.WebSite.Admin.KitTypes" Theme="Default" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="pageDiv">
    <h3>Kit Types</h3>
        <telerik:RadGrid ID="RadGrid_KitType" runat="server" Width="800px" GridLines="None"
            AutoGenerateColumns="False" PageSize="15" AllowSorting="True" AllowPaging="True" 
                   Skin="Windows7" 
            oninsertcommand="RadGrid_KitType_InsertCommand" 
            onneeddatasource="RadGrid_KitType_NeedDataSource" 
            onupdatecommand="RadGrid_KitType_UpdateCommand"
            OnPreRender="RadGrid_KitType_OnPreRender"
            OnItemDataBound="RadGrid_KitType_ItemDataBound">
            <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top" EditMode="EditForms">
            <Columns>
            <%--PG31--%>
                <%--<telerik:GridTemplateColumn HeaderStyle-Width="30">
                    <HeaderTemplate>#</HeaderTemplate>
                    <ItemTemplate><%# (Container.ItemIndex + 1).ToString() %></ItemTemplate>            
                    <HeaderStyle Width="30px"></HeaderStyle>
                </telerik:GridTemplateColumn>--%>
                <%--PG31--%>
                  <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" >
                    </telerik:GridEditCommandColumn> 
                <telerik:GridBoundColumn HeaderText="KitName" DataField="KitName"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="KitContents" DataField="KitContents"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Kit Items" DataField="KitItemsString"></telerik:GridBoundColumn>
                <%--PG31--%>
                <telerik:GridBoundColumn HeaderText="Kw_Savings" DataField="Kw_Savings"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Kwh_Savings" DataField="Kwh_Savings"></telerik:GridBoundColumn>
                <%--PG31--%>
                <telerik:GridBoundColumn HeaderText="Width" DataField="Width"></telerik:GridBoundColumn>                
                <telerik:GridBoundColumn HeaderText="Height" DataField="Height"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Length" DataField="Length"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Weight" DataField="Weight"></telerik:GridBoundColumn>
               <%-- <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" EditText="Edit" InsertText="Insert" CancelText="Cancel" >
                    </telerik:GridEditCommandColumn>--%>
                <%--<telerik:GridButtonColumn UniqueName="DeleteColumn" Text="Delete" CommandName="Delete" />--%>                 
               
            </Columns> 
            <EditFormSettings UserControlName="../Controls/KitTypeCard.ascx" EditFormType="WebUserControl">
                <EditColumn UniqueName="EditCommandColumn1">
                </EditColumn>
             </EditFormSettings>
            </MasterTableView>
            </telerik:RadGrid> 
     </div>
</asp:Content>
