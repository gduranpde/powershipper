﻿using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.WebSite.Controls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Admin
{
    public partial class Shippers : Page
    {
        private readonly UserManagementPresenter presenter = new UserManagementPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Shippers");
            }
        }

        protected void RadGrid_Shippers_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_Shippers.DataSource = presenter.GetAllShipers();
        }

        protected void RadGrid_Shippers_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.PerformInsertCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        if (e.Item is GridEditFormInsertItem)
                        {
                            var s = new Shipper();
                            s.Id = Guid.NewGuid();

                            object o =
                                ((ShipperCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                            var sNew = (Shipper)o;

                            s.ShipperCode = sNew.ShipperCode;
                            s.ShipperName = sNew.ShipperName;

                            s.BatchTrackingCodeModule = sNew.BatchTrackingCodeModule;
                            s.PackageTrackingUrl = sNew.PackageTrackingUrl;
                            s.TransactionFileUrl = sNew.TransactionFileUrl;

                            presenter.SaveShipper(s);
                            RadGrid_Shippers.Rebind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to insert shipper. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                RadGrid_Shippers.Controls.Add(l);
            }
        }

        protected void RadGrid_Shippers_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        var item = (GridEditFormItem) e.Item;
                        var id = new Guid(item.GetDataKeyValue("Id").ToString());
                        if (id != Guid.Empty)
                        {
                            if (e.Item is GridEditableItem)
                            {
                                object o =
                                    ((ShipperCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).
                                        FillEntity();
                                var sNew = (Shipper)o;

                                Shipper s = presenter.GetShipperById(sNew.Id);

                                s.ShipperCode = sNew.ShipperCode;
                                s.ShipperName = sNew.ShipperName;

                                s.BatchTrackingCodeModule = sNew.BatchTrackingCodeModule;
                                s.PackageTrackingUrl = sNew.PackageTrackingUrl;
                                s.TransactionFileUrl = sNew.TransactionFileUrl;

                                presenter.SaveShipper(s);
                                RadGrid_Shippers.Rebind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var l = new Label();
                l.Text = "Unable to update shipper. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                RadGrid_Shippers.Controls.Add(l);
            }
        }
    }
}