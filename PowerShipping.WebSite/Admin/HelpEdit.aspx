﻿<%@ Page Title="Help Edit Page" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" CodeBehind="HelpEdit.aspx.cs" Inherits="PowerShipping.WebSite.Admin.HelpEdit" Theme="Default" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">


<div class="jobInfo" runat="server" id="div_jobInfo">
 <div style=" text-align : center;"><b>Help Editor</b></div>
 <table>
    <tr>
        <td class="jobInfo_td">Help File: </td><td class="jobInfo_info">
        <telerik:RadComboBox ID="RadComboBox_Files" runat="server" ></telerik:RadComboBox></td>
    </tr>
     <tr>
        <td colspan="2" align="center">
            <asp:Button runat="server" ID="Button_Select" Text="Select"  Width="120px"
        CssClass="ptButton" onclick="Button_Select_Click"  />
        </td>
    </tr>
 </table>
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:HiddenField  ID="HiddenField_HelpFile" runat="server" />
<div class="pageDiv">
<h3>Help Editor</h3>


    <telerik:RadEditor ID="RadEditor1" Runat="server" Height="424px" Width="1138px">
        <Content>
</Content>
    </telerik:RadEditor>

 <asp:Button runat="server" ID="Button_SaveContent" Text="Save"  Width="120px"
        CssClass="ptButton" onclick="Button_SaveContent_Click"  />

</div>
</asp:Content>
