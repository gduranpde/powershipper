﻿using Dims.DAL;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.WebSite.Controls;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Admin
{
    public partial class KitContents : System.Web.UI.Page
    {

        private readonly UserManagementPresenter presenter = new UserManagementPresenter();
        private readonly KitTypesPresenter kitPresenter = new KitTypesPresenter();

        private Guid? _kitTypeId = null;
        private string pageTitle = "Kit Contents - ";

        protected void Page_Load(object sender, EventArgs e)
        {
            string id = Page.Request.QueryString["KitTypeId"];
            if (string.IsNullOrEmpty(id))
                return;

            try
            {
                _kitTypeId = new Guid(id);
            }
            catch
            {
                _kitTypeId = null;
            }

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Kit Contents");
            }
            pageTitle = pageTitle + Page.Request.QueryString["KitName"];
        }

        protected string PageTitle
        {
            get
            {
                return pageTitle;
            }
        }

        protected void RadGrid_Items_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_Items.DataSource = kitPresenter.GetFromKitContentsByKitType(_kitTypeId);
        }

        protected void RadGrid_Items_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.PerformInsertCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        if (e.Item is GridEditFormInsertItem)
                        {
                            var kit = ((KitContentsCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                            kit.KitID = _kitTypeId.Value;
                            kit.LastModifiedBy = User.Identity.Name;
                            kit.LastModifiedDate = DateTime.Now;

                            kitPresenter.SaveKitContent(kit);
                            kitPresenter.SaveKitTypeItems(kit.ItemID, kit.KitID);

                            RadGrid_Items.Rebind();
                        }
                    }
                }
            }
            catch (DbException sqlEx)
            {
                Log.LogError(sqlEx);
                var l = new Label();
                l.Text = "This item is already present in kit contents";
                l.ControlStyle.ForeColor = Color.Red;
                RadGrid_Items.Controls.Add(l);
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to insert item. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                RadGrid_Items.Controls.Add(l);
            }

        }

        protected void RadGrid_Items_UpdateCommand(object source, GridCommandEventArgs e)
        {

            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        var item = (GridEditFormItem)e.Item;
                        var id = new Guid(item.GetDataKeyValue("KitContentID").ToString());
                        if (id != Guid.Empty)
                        {
                            if (e.Item is GridEditableItem)
                            {
                                var kNew = ((KitContentsCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();

                                KitContent k = kitPresenter.GetKitContentById(kNew.KitContentID);

                                k.Quantity = kNew.Quantity;
                                k.LastModifiedBy = User.Identity.Name;
                                k.LastModifiedDate = DateTime.Now;
                                k.KitID = _kitTypeId.Value;

                                // update the binding in KitTypeItem
                                if (k.ItemID != kNew.ItemID)
                                {
                                    kitPresenter.UpdateKitTypeItem(k.KitID, k.ItemID, kNew.ItemID);
                                }
                                k.ItemID = kNew.ItemID;

                                kitPresenter.SaveKitContent(k);
                                RadGrid_Items.Rebind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var l = new Label();
                l.Text = "Unable to update item. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                RadGrid_Items.Controls.Add(l);
            }

        }

        protected void lnkBtnBackToKit_OnClick(object sender, EventArgs e)
        {
            if (_kitTypeId != null)
                Response.Redirect("KitSetup.aspx?KitTypeId=" + _kitTypeId);
        }

        protected void RadGrid_Items_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.EditCommandName)
            {
                RadGrid_Items.MasterTableView.IsItemInserted = false;

                if (e.Item is GridDataItem)
                {
                    ScrollToItem(e);
                }

            }

            else
                if ((e.CommandSource is Button && (e.CommandSource as Button).Text == "Add"))
                {
                    ScrollToItem(e);
                }

            if ((e.CommandSource is Button && (e.CommandSource as Button).Text == "Remove"))
            {
                ScrollToItem(e);
            }

            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                RadGrid_Items.MasterTableView.ClearEditItems();
            }
        }

        private void ScrollToItem(GridCommandEventArgs e)
        {
            if (e.Item != null)
            {
                GridDataItem item = (GridDataItem)e.Item;
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scroll", "setTimeout(function(){ Expand('" + item.ClientID + "');}, 100);", true);
            }
        }
    }
}