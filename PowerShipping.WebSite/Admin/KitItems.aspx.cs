﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.WebSite.Controls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Admin
{
    public partial class KitItems : Page
    {
        private readonly UserManagementPresenter presenter = new UserManagementPresenter();

        private Guid? _kitTypeId = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            string id = Page.Request.QueryString["KitTypeId"];
            if (string.IsNullOrEmpty(id))
                return;

            try
            {
                _kitTypeId = new Guid(id);
            }
            catch
            {
                _kitTypeId = null;
            }

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Kit Items");
            }
        }

        protected void RadGrid_Items_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_Items.DataSource = presenter.GetKitItemsByKitType(_kitTypeId);
        }

        protected void RadGrid_Items_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.PerformInsertCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        if (e.Item is GridEditFormInsertItem)
                        {
                            var k = new KitItem();
                            k.Id = Guid.NewGuid();

                            object o =
                                ((KitItemCard) e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                            var kNew = (KitItem) o;

                            k.Name = kNew.Name;
                            k.CatalogID = kNew.CatalogID;
                            k.Quantity = kNew.Quantity;
                            k.Kw_Impact = kNew.Kw_Impact;
                            k.Kwh_Impact = kNew.Kwh_Impact;

                            presenter.SaveKitItem(k);
                            if (_kitTypeId != null)
                            {
                                KitType kt = presenter.GetKitTypeById(new Guid(_kitTypeId.ToString()));
                                kt.KitItems = new List<KitItem>();
                                kt.KitItems.Add(k);

                                presenter.SaveKitTypeKitItems(kt, false);
                            }

                            RadGrid_Items.Rebind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to insert item. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                RadGrid_Items.Controls.Add(l);
            }
        }

        protected void RadGrid_Items_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        var item = (GridEditFormItem) e.Item;
                        var id = new Guid(item.GetDataKeyValue("Id").ToString());
                        if (id != Guid.Empty)
                        {
                            if (e.Item is GridEditableItem)
                            {
                                object o =
                                    ((KitItemCard) e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).
                                        FillEntity();
                                var kNew = (KitItem) o;

                                KitItem k = presenter.GetKitItemById(kNew.Id);

                                k.Name = kNew.Name;
                                k.CatalogID = kNew.CatalogID;
                                k.Quantity = kNew.Quantity;
                                k.Kw_Impact = kNew.Kw_Impact;
                                k.Kwh_Impact = kNew.Kwh_Impact;

                                presenter.SaveKitItem(k);
                                RadGrid_Items.Rebind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var l = new Label();
                l.Text = "Unable to update item. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                RadGrid_Items.Controls.Add(l);
            }
        }

        protected void lnkBtnBackToKit_OnClick(object sender, EventArgs e)
        {
            if (_kitTypeId != null)
                Response.Redirect("KitTypes.aspx?KitTypeId=" + _kitTypeId);
        }
    }
}