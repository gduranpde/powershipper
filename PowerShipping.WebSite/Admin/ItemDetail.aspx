﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Shared/Default.Master" CodeBehind="ItemDetail.aspx.cs" Inherits="PowerShipping.WebSite.Admin.ItemDetail" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="pageDiv">
        <h3>Item Setup</h3>
        <telerik:RadGrid ID="RadGrid_ItemDetails" runat="server" Width="99%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True"
            Skin="Windows7"
            OnNeedDataSource="RadGrid_ItemDetails_NeedDataSource"
            OnUpdateCommand="RadGrid_ItemDetails_UpdateCommand"
            OnInsertCommand="RadGrid_ItemDetails_InsertCommand"
            AllowFilteringByColumn="false">
            <MasterTableView DataKeyNames="ItemID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                AllowFilteringByColumn="false" AutoGenerateColumns="false" EditMode="EditForms">
                <ItemStyle Wrap="True" />
                <Columns>
                    <telerik:GridEditCommandColumn UniqueName="EditCommandColumn">
                        <HeaderStyle Width="30px" />
                    </telerik:GridEditCommandColumn>

                    <telerik:GridBoundColumn DataField="ItemName" HeaderText="Item Name " SortExpression="ItemName"
                        UniqueName="ItemName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                        <HeaderStyle Width="30px" />
                    </telerik:GridBoundColumn>

                    <telerik:GridCheckBoxColumn DataField="IsActive" DataType="System.Boolean" DefaultInsertValue=""
                        HeaderText="Is Active" SortExpression="IsActive" UniqueName="IsActive">
                        <HeaderStyle Width="30px" />
                    </telerik:GridCheckBoxColumn>

                    <telerik:GridBoundColumn DataField="ItemDescription" HeaderText="Item Description" SortExpression="ItemDescription"
                        UniqueName="ItemDescription" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" FooterStyle-Wrap="true" ItemStyle-Wrap="true">
                        <HeaderStyle Width="130px" />
                    </telerik:GridBoundColumn>

                </Columns>
                <EditFormSettings UserControlName="../Controls/ItemDetailCard.ascx" EditFormType="WebUserControl">
                    <EditColumn UniqueName="EditCommandColumn1">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
        </telerik:RadGrid>
    </div>

</asp:Content>
