﻿<%@ Page Title="Kit Setup" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" 
    CodeBehind="KitSetup.aspx.cs" Inherits="PowerShipping.WebSite.Admin.KitSetup" Theme="Default" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Src="../Controls/KitSetupFilter.ascx" TagName="KitSetupFilter"
    TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <uc1:KitSetupFilter ID="ucKitSetupFilter" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="pageDiv">
        <h3>Kit Setup</h3>
        <telerik:RadGrid ID="RadGrid_KitSetup" runat="server" Width="99%" GridLines="None"
            AutoGenerateColumns="False" PageSize="15" AllowSorting="True" AllowPaging="True"
            Skin="Windows7"
            OnInsertCommand="RadGrid_KitSetup_InsertCommand"
            OnNeedDataSource="RadGrid_KitSetup_NeedDataSource"
            OnUpdateCommand="RadGrid_KitSetup_UpdateCommand"
            OnPreRender="RadGrid_KitSetup_OnPreRender"
            OnItemCommand="RadGrid_KitSetup_ItemCommand"
            OnItemDataBound="RadGrid_KitSetup_ItemDataBound">
            <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True"
                Width="100%" CommandItemDisplay="Top" EditMode="EditForms">
                <Columns>
                    <telerik:GridEditCommandColumn UniqueName="EditCommandColumn">
                    </telerik:GridEditCommandColumn>
                    <telerik:GridBoundColumn HeaderText="KitName" DataField="KitName"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderText="KitContents" DataField="KitContents">
                        <HeaderStyle Width="500px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderText="Width" DataField="Width"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderText="Height" DataField="Height"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderText="Length" DataField="Length"></telerik:GridBoundColumn>
                    <telerik:GridCheckBoxColumn DataField="IsActive" DataType="System.Boolean" DefaultInsertValue=""
                        HeaderText="Is Active" SortExpression="IsActive" UniqueName="IsActive">
                    </telerik:GridCheckBoxColumn>
                </Columns>
                <EditFormSettings UserControlName="../Controls/KitSetupCard.ascx" 
                    EditFormType="WebUserControl"  >
                    <EditColumn UniqueName="EditCommandColumn1">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
        </telerik:RadGrid>
    </div>

      <script type="text/javascript">

          function Expand(itemID) {
              var Grid = $find('<%=RadGrid_KitSetup.ClientID %>');
            var scrollArea = document.getElementById('ctl00_ContentPlaceHolder1_RadGrid_ConsumerRequests')
            var rowElement = document.getElementById(itemID);
            window.scrollTo(0, rowElement.offsetTop + 200);
        }
    </script>
</asp:Content>
