﻿using System;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.WebSite.Controls;
using Telerik.Web.UI;
using System.Linq;

namespace PowerShipping.WebSite.Admin
{
    public partial class UserManagement : System.Web.UI.Page
    {
        UserManagementPresenter presenter = new UserManagementPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "User Management");
            }
        }

        protected void RadGrid_Users_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_Users.DataSource = presenter.GetMany();
        }

        protected void RadGrid_Users_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.PerformInsertCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        if (e.Item is GridEditFormInsertItem)
                        {
                            User u = new User();

                            object o = ((UserManagementCard) e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                            User uNew = (User)o;

                            u.UserName = uNew.UserName;
                            u.Password = uNew.Password;

                            u.Roles = uNew.Roles;

                            if (uNew.Company == null && (uNew.Roles.Contains(PDMRoles.ROLE_Customer) || uNew.Roles.Contains(PDMRoles.ROLE_CallCenter)))
                            {
                                Label l = new Label();
                                l.Text = "Company must be selected.";
                                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                                RadGrid_Users.Controls.Add(l);
                                return;
                            }

                            presenter.Add(u);
                            u = presenter.GetOne(u.UserName);

                            u.Company = uNew.Company;
                            u.Projects = uNew.Projects;

                            presenter.SaveUserCompany(u);
                            presenter.SaveUserProject(u);

                            RadGrid_Users.Rebind();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                Label l = new Label();
                l.Text = "Unable to insert user. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_Users.Controls.Add(l);
            }
        }

        protected void RadGrid_Users_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        GridEditFormItem item = (GridEditFormItem)e.Item;
                        Guid id = new Guid(item.GetDataKeyValue("UserId").ToString());
                        if (id != Guid.Empty)
                        {
                            if (e.Item is GridEditableItem)
                            {
                                object o =((UserManagementCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                                User uNew = (User)o;

                                User u = presenter.GetById(uNew.UserID);

                                u.UserName = uNew.UserName;
                                u.Password = uNew.Password;

                                u.Roles = uNew.Roles;
                                u.Company = uNew.Company;
                                u.Projects = uNew.Projects;


                                if (uNew.Company == null && (uNew.Roles.Contains(PDMRoles.ROLE_Customer) || uNew.Roles.Contains(PDMRoles.ROLE_CallCenter)))
                                {
                                    Label l = new Label();
                                    l.Text = "Company must be selected.";
                                    l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                                    RadGrid_Users.Controls.Add(l);
                                    return;
                                }

                                presenter.Save(u);

                                presenter.SaveUserCompany(u);
                                presenter.SaveUserProject(u);

                                RadGrid_Users.Rebind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Label l = new Label();
                l.Text = "Unable to update user. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_Users.Controls.Add(l);
            }
        }

        protected void RadGrid_Users_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.DeleteCommandName)
                {
                    //Guid id = new Guid((e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UserId"].ToString());
                   
                    //User u = presenter.GetById(id);

                    //presenter.Remove(u);
                    //RadGrid_Users.Rebind();
                }
            }
            catch (Exception ex)
            {
                Label l = new Label();
                l.Text = "Unable to delete user. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_Users.Controls.Add(l);
            }
        }
    }
}
