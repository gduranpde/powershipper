﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Shared/Default.Master" CodeBehind="Companies.aspx.cs" Inherits="PowerShipping.WebSite.Admin.Companies" Theme="Default" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<div class="pageDiv">
    <h3>Companies</h3>
    
     <telerik:RadGrid ID="RadGrid_Companies" runat="server" Width="50%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True" 
              Skin="Windows7" 
                onneeddatasource="RadGrid_Companies_NeedDataSource"        
                OnUpdateCommand="RadGrid_Companies_UpdateCommand"        
                OnInsertCommand="RadGrid_Companies_InsertCommand"                
                AllowFilteringByColumn="false">
            <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
            AllowFilteringByColumn="false" AutoGenerateColumns="false" EditMode="EditForms">        
            <Columns> 
                <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" >
                    </telerik:GridEditCommandColumn>
               
                <telerik:GridBoundColumn DataField="CompanyName" HeaderText="CompanyName" SortExpression="CompanyName"
                        UniqueName="CompanyName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" >
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="CompanyCode" HeaderText="CompanyCode" SortExpression="CompanyCode"
                        UniqueName="CompanyCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn> 
                 <telerik:GridCheckBoxColumn DataField="IsActive" DataType="System.Boolean" DefaultInsertValue=""
                    HeaderText="IsActive" SortExpression="IsActive" UniqueName="IsActive">
                 </telerik:GridCheckBoxColumn>                                
            </Columns>              
            <EditFormSettings UserControlName="../Controls/CompanyCard.ascx" EditFormType="WebUserControl">
                <EditColumn UniqueName="EditCommandColumn1">
                </EditColumn>
             </EditFormSettings>
            </MasterTableView>
    </telerik:RadGrid>
    </div>
</asp:Content>
