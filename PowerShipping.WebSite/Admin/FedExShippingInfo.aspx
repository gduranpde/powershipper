﻿<%@ Page Title="Default FedEx Shipping Information" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" CodeBehind="FedExShippingInfo.aspx.cs" Inherits="PowerShipping.WebSite.Admin.FedExShippingInfo" Theme="Default" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register src="~/Controls/ShipmentDetailsInfo.ascx" tagname="ShipmentDetails" tagprefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<div class="pageDiv">
    <h3>Default FedEx Shipping Information</h3>
    <uc3:ShipmentDetails ID="ShipmentDetails_Details" runat="server" />
    <br />
    
    <table>
    <tr>
    <td Style="width:200px"></td>
    <td><asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" CssClass="ptButton"/>
    </td>
    </tr>
    <tr>
    <td Style="width:200px"></td>
    <td><asp:Label ID="lbMessages" runat="server" ></asp:Label>
    </td>
    </tr>
    </table>
    
   </div>
</asp:Content>
