﻿using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;
using System.IO;

namespace PowerShipping.WebSite.Shared
{
    public partial class Default : MasterPage
    {
        UserManagementPresenter presenter = new UserManagementPresenter();

        public bool MenuVisible
        {
            get
            {
                return RadMenu1.Visible;
            }

            set
            {
                RadMenu1.Visible = value;
            }
        }

        public bool LeftContentVisible
        {
            get
            {
                return tdLeftContent.Visible;
            }

            set
            {
                tdLeftContent.Visible = value;
            }
        }

        public string Help
        {
            get
            {
                return help.Text;
            }

            set
            {
                help.Text = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Roles.IsUserInRole("PDE");

            //write the number and date of the site version 
            String rootPath = Server.MapPath("~");
            DirectoryInfo outputDir = new DirectoryInfo(rootPath);
            string assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name.ToString();
            string assembleVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            string fileName = String.Format("bin/" + assemblyName + ".dll");
            string filePath = Path.Combine(outputDir.FullName, fileName);
            FileInfo oFileInfo = new FileInfo(filePath);
            DateTime d = oFileInfo.LastWriteTime;
            string modifyDate = d.Date.ToShortDateString();
            DataRealease.Text = "Version " + assembleVersion + "(" + modifyDate + ")";
          
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (presenter.UserCompany != null)
                LogoImage.ImageUrl = "~/Logo/Comapny_" + presenter.UserCompany.Id + ".jpg";
            else
                LogoImage.ImageUrl = "../App_Themes/Default/Images/PDE_191x71.jpg";

            base.OnPreRender(e);
        }

        protected void RadMenu1_ItemDataBound(object sender, RadMenuEventArgs e)
        {
            if (e.Item.NavigateUrl.Contains("#"))
                e.Item.NavigateUrl = "#";
            SiteMapNode node = (SiteMapNode)e.Item.DataItem;
            if ((Roles.IsUserInRole("CallCenter")) && (node.Description == "Phase 1 Reports"))
            {
                e.Item.Remove();
            }
        }

        protected void LoggedOut(object sender, EventArgs e)
        {
            //HttpContext.Current.Session.Clear();
        }
    }
}