﻿using System;
using PowerShipping.Business.Presenters;

namespace PowerShipping.WebSite
{
    public partial class Error : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["Error"]))
                {
                    string errText = string.Empty;
                    Errors errType = (Errors)Convert.ToInt32(Request.QueryString["Error"]);

                    switch (errType)
                    {
                        case Errors.HaventRights:
                            errText = "You don't have rights for this page.";
                            break;
                    }

                    error.Text = errText;
                }
            }

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Error Page");
            }
        }
    }
}
