﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite
{
    public class Helper
    {
        public Helper()
        {

        }
        public static DataTable GetDataSource()
        {
            DataTable dt = new DataTable();
            for (int colCounter = 0; colCounter < 7; colCounter++)
            {
                dt.Columns.Add(new DataColumn(string.Format("Column_{0}", colCounter)));
            }

            for (int rowCouner = 0; rowCouner < 5; rowCouner++)
            {
                DataRow row = dt.NewRow();
                foreach (DataColumn tmpCol in dt.Columns)
                {
                    row[tmpCol] = string.Format("value_{0}_{1}", rowCouner, tmpCol.ColumnName);
                }

                dt.Rows.Add(row);
            }
                return dt;
        }

        public static List<ConsumerRequest> GetAllConsumerRequests()
        {
            List<ConsumerRequest> list = new List<ConsumerRequest>();

            for (int i = 0; i < 20; i++ )
            {
                ConsumerRequest cr = new ConsumerRequest();
                cr.AccountName = "AcName" + i.ToString();
                cr.AccountNumber = "AcNumber" + i.ToString();
                cr.Address1 = "Address1" + i.ToString();
                cr.AnalysisDate = DateTime.Now.AddDays(i);
                cr.AuditFailureDate = DateTime.Now.AddDays(i);
                cr.BatchId = Guid.NewGuid();
                cr.ChangedBy = "ChangedBy" + i.ToString();
                cr.City = "City" + i.ToString();
                cr.DoNotShip = false;
                cr.Email = "Email" + i.ToString();
                cr.Id = Guid.NewGuid();
                cr.IsOkayToContact = true;
                cr.IsReship = true;
                cr.Method = "Method" + i.ToString();
                cr.Notes = "Notes" + i.ToString();
                cr.Phone1 = "Phone1" + i.ToString();
                cr.Phone2 = "Phone2" + i.ToString();
                cr.ReceiptDate = DateTime.Now.AddDays(i);
                cr.State = "State" + i.ToString();
                cr.Status = ConsumerRequestStatus.Delivered;
                cr.ZipCode = "ZipCode" + i.ToString();

                list.Add(cr);
            }
            return list;
        }

        public static List<ConsumerRequestException> GetAllConsumerRequestsExceptions()
        {
            List<ConsumerRequestException> list = new List<ConsumerRequestException>();

            return list;
        }

        public static List<ConsumerRequest> GetAllConsumerRequestsForFedEx()
        {
            List<ConsumerRequest> list = new List<ConsumerRequest>();

            return list;
        }

        public static void ErrorInGrid(string gridName, Page page, Label l)
        {
            //var grid =
            //    (Page.Controls[0].Controls[3].Controls[13].FindControl("RadGrid_Companies") as RadGrid);
            var grid =
                (page.Controls[0].Controls[3].Controls[19].FindControl(gridName) as RadGrid);
            grid.Controls.Add(l);
        }

        public static void ErrorInGrid(Page page, string text)
        {
            Label err = page.Controls[0].Controls[3].Controls[19].FindControl("lbErrPopUp") as Label;
            if (err != null)
                err.Text = text;

            ModalPopupExtender mpe = page.Controls[0].Controls[3].Controls[19].FindControl("mpeError") as ModalPopupExtender;
            if(mpe != null)
                mpe.Show();
        }

        public static void ErrorOnPagePopUp(Page page, string lbId, string mpeId, string error)
        {
            ModalPopupExtender mpe =
                (page.Controls[0].Controls[3].Controls[19].FindControl(mpeId) as ModalPopupExtender);

            Label lb =
               (page.Controls[0].Controls[3].Controls[19].FindControl(lbId) as Label);

            lb.ControlStyle.ForeColor = System.Drawing.Color.Red;
            lb.Text = error;

            mpe.Show();
        }
    }
}
