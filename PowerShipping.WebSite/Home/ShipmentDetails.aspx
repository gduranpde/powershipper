﻿<%@ Page Title="Shipment Details" Language="C#" MasterPageFile="~/Shared/Default.Master"
    AutoEventWireup="true" CodeBehind="ShipmentDetails.aspx.cs" Inherits="PowerShipping.WebSite.Home.ShipmentDetails"
    Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../Controls/ProjectsList.ascx" TagName="ProjectsList" TagPrefix="uc1" %>
<%@ Register Src="../Controls/ShipmentDetailFilter.ascx" TagName="ShipmentDetailFilter"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <uc1:ShipmentDetailFilter ID="ShipmentDetailFilter1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField runat="server" ID="HiddenFiled_CustomQuery" />
    <div class="pageDiv">
        <table style="width:100%;">
            <tr>
                <td style="width:150px;">
                    <h3>
                        Shipment Details</h3>
                </td>
                <td style="width:500px;">
                    <uc1:ProjectsList ID="ProjectsList1" runat="server" IsCompanyUse="true" />					
                </td>
				<td>
					 <asp:Button ID="btnBulkExport" runat="server" OnClick="btnBulkExport_OnClick"
                        Text="Bulk Export" />
				</td>
            </tr>
            <tr>
                <td colspan="3">
                    <telerik:RadGrid ID="RadGrid_ShipmentDetails" runat="server" Width="100%" GridLines="None"
                        AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True"
                        Skin="Windows7" OnNeedDataSource="RadGrid_ShipmentDetails_NeedDataSource" OnUpdateCommand="RadGrid_ShipmentDetails_UpdateCommand"
                        OnDeleteCommand="RadGrid_ShipmentDetails_DeleteCommand" OnItemCreated="RadGrid_ShipmentDetails_ItemCreated"
                        OnInsertCommand="RadGrid_ShipmentDetails_InsertCommand" OnItemCommand="RadGrid_ShipmentDetails_ItemCommand"
                        OnItemDataBound="RadGrid_ShipmentDetails_ItemDataBound">
                        <ExportSettings ExportOnlyData="true" IgnorePaging="true" Excel-Format="ExcelML">
                        </ExportSettings>
                        <ClientSettings>
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                            AutoGenerateColumns="false" PagerStyle-AlwaysVisible="true" UseAllDataFields="true"
                            PagerStyle-Mode="NextPrevNumericAndAdvanced">
                            <CommandItemSettings ShowExportToWordButton="true" ShowExportToExcelButton="true"
                                ShowExportToCsvButton="true" ShowExportToPdfButton="true" />
                            <Columns>
                                <%-- <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" >
                    </telerik:GridEditCommandColumn>--%>
                                <%-- <telerik:GridButtonColumn UniqueName="DeleteColumn" Text="Delete" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmText="Are you sure delete this row?"/>
                --%>
                                <%-- <telerik:GridBoundColumn DataField="ShipmentNumber" HeaderText="Shipment #" SortExpression="ShipmentNumber" ReadOnly="true"
                        UniqueName="ShipmentNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                        </telerik:GridBoundColumn>--%>
                                <telerik:GridTemplateColumn SortExpression="ShipmentNumber" HeaderText="ShipmentNumber"
                                    DataField="ShipmentNumber" DataType="System.String" UniqueName="ShipmentNumber">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="EditButton" runat="server" CommandName="Edit" Text="<%# ((PowerShipping.Entities.ShipmentDetail)Container.DataItem).ShipmentNumber %>"></asp:LinkButton>
                                    </ItemTemplate>
                                    <HeaderStyle Width="100px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="Account #" SortExpression="AccountNumber"
                                    ReadOnly="true" UniqueName="AccountNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false" EditFormColumnIndex="0" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PurchaseOrderNumber" HeaderText="PO #" SortExpression="PurchaseOrderNumber"
                                    UniqueName="PurchaseOrderNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ShipperCode" HeaderText="Shipper" SortExpression="ShipperCode"
                                    UniqueName="ShipperCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <%-- <telerik:GridBoundColumn DataField="FedExTrackingNumber" HeaderText="Tracking Number" SortExpression="FedExTrackingNumber"
                        UniqueName="FedExTrackingNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        >
                        </telerik:GridBoundColumn>   --%>
                                <telerik:GridHyperLinkColumn DataNavigateUrlFields="FedExTrackingNumber" DataTextField="FedExTrackingNumber"
                                    HeaderText="Tracking Number" SortExpression="FedExTrackingNumber" UniqueName="FedExTrackingNumber"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                </telerik:GridHyperLinkColumn>
                                <telerik:GridBoundColumn DataField="FedExStatusCode" HeaderText="Shipper Status Code"
                                    SortExpression="FedExStatusCode" UniqueName="FedExStatusCode" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FedExLastUpdateStatus" HeaderText="Shipper Notes"
                                    SortExpression="FedExLastUpdateStatus" ReadOnly="true" UniqueName="FedExLastUpdateStatus"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="AccountName" HeaderText="Name" SortExpression="AccountName"
                                    UniqueName="AccountName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CompanyName" HeaderText="CompanyName" SortExpression="CompanyName"
                                    UniqueName="CompanyName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ShipmentDetailAddress1" HeaderText="Address1"
                                    SortExpression="ShipmentDetailAddress1" UniqueName="ShipmentDetailAddress1" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ShipmentDetailAddress2" HeaderText="Address2"
                                    SortExpression="ShipmentDetailAddress2" UniqueName="ShipmentDetailAddress2" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ShipmentDetailCity" HeaderText="City" SortExpression="ShipmentDetailCity"
                                    UniqueName="ShipmentDetailCity" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ShipmentDetailState" HeaderText="State" SortExpression="ShipmentDetailState"
                                    UniqueName="ShipmentDetailState" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ShipmentDetailZip" HeaderText="ZipCode" SortExpression="ShipmentDetailZip"
                                    UniqueName="ShipmentDetailZip" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false" MaxLength="5">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email"
                                    UniqueName="Email" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridMaskedColumn DataField="Phone1" HeaderText="Phone 1" SortExpression="Phone1"
                                    Mask="###-###-####" UniqueName="Phone1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false" DataFormatString="{0:(###)###-####}">
                                </telerik:GridMaskedColumn>
                                <telerik:GridMaskedColumn DataField="Phone2" HeaderText="Phone 2" SortExpression="Phone2"
                                    Mask="###-###-####" UniqueName="Phone2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false" DataFormatString="{0:(###)###-####}">
                                </telerik:GridMaskedColumn>
                                <telerik:GridBoundColumn DataField="KitTypeName" HeaderText="Kit Type" SortExpression="KitTypeName"
                                    UniqueName="KitTypeName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false" ReadOnly="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ShippingStatusSort" HeaderText="ShippingStatus"
                                    SortExpression="ShippingStatusSort" ReadOnly="true" UniqueName="ShippingStatusSort"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn UniqueName="ShippingStatus" DataField="ShippingStatus"
                                    HeaderText="ShippingStatus" SortExpression="ShippingStatus">
                                    <ItemTemplate>
                                        <%# Eval("ShippingStatus")%>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList runat="server" ID="ddlShippingStatus" DataValueField="Value" DataTextField="Text">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="ReshipStatusSort" HeaderText="ReshipStatus" SortExpression="ReshipStatusSort"
                                    ReadOnly="true" UniqueName="ReshipStatusSort" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn UniqueName="ReshipStatus" DataField="ReshipStatus" HeaderText="ReshipStatus"
                                    SortExpression="ReshipStatus">
                                    <ItemTemplate>
                                        <%# Eval("ReshipStatus")%>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList runat="server" ID="ddlReshipStatus" DataValueField="Value" DataTextField="Text">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridDateTimeColumn DataField="ShippingStatusDate" HeaderText="ShippingStatusDate"
                                    SortExpression="ShippingStatusDate" UniqueName="ShippingStatusDate" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="true" DataFormatString="{0:MM/dd/yyyy}">
                                </telerik:GridDateTimeColumn>
                                <telerik:GridDateTimeColumn DataField="ShipDate" HeaderText="ShipDate" SortExpression="ShipDate"
                                    UniqueName="ShipDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" DataFormatString="{0:MM/dd/yyyy}">
                                </telerik:GridDateTimeColumn>
                                <telerik:GridDateTimeColumn DataField="ReturnDate" HeaderText="ReturnDate" SortExpression="ReturnDate"
                                    UniqueName="ReturnDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" DataFormatString="{0:MM/dd/yyyy}">
                                </telerik:GridDateTimeColumn>
                                <telerik:GridCheckBoxColumn DataField="IsReship" HeaderText="Reship?" SortExpression="IsReship"
                                    UniqueName="IsReship" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true">
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridBoundColumn DataField="ReasonCode" HeaderText="Return Reason" SortExpression="ReasonCode"
                                    UniqueName="ReasonCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CompanyCode" HeaderText="CompanyCode" SortExpression="CompanyCode"
                                    UniqueName="CompanyCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="OperatingCompany" HeaderText="Op Co" SortExpression="OperatingCompany"
                                    UniqueName="OperatingCompany" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ProjectCode" HeaderText="ProjectCode" SortExpression="ProjectCode"
                                    UniqueName="ProjectCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <EditFormSettings UserControlName="../Controls/ShipmentEditInfo.ascx" EditFormType="WebUserControl">
                                <EditColumn UniqueName="EditCommandColumn1">
                                </EditColumn>
                            </EditFormSettings>
                        </MasterTableView>
                    </telerik:RadGrid>

                    <script type="text/javascript">

        function Expand(itemID) {
            var Grid = $find('<%=RadGrid_ShipmentDetails.ClientID %>');
            var scrollArea = document.getElementById('ctl00_ContentPlaceHolder1_RadGrid_ShipmentDetails')
            var rowElement = document.getElementById(itemID);
            window.scrollTo(0, rowElement.offsetTop + 200);
        }
                    </script>

                </td>
            </tr>
        </table>
    </div>
</asp:Content>