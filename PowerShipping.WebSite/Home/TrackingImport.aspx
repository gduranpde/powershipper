﻿<%@ Page Title="Tracking Import" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" CodeBehind="TrackingImport.aspx.cs" Inherits="PowerShipping.WebSite.Home.TrackingImport" Theme="Default"%>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register src="../Controls/ProjectsList.ascx" tagname="ProjectsList" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


 <telerik:RadProgressManager ID="Radprogressmanager1" runat="server" />
 <div class="pageDiv">

            <table>
             <tr>
                    <td>              
                        <h3>Tracking Import</h3>
                    </td>
                    <td>
                         <uc1:ProjectsList ID="ProjectsList1" runat="server" visible="false"/>
                    </td>
                </tr>
               <%-- <tr>
                    <td colspan="2">
                        <asp:Literal runat="server" ID="help" Text="test"></asp:Literal>            
                    </td>
                </tr>    --%>         
                <tr>
                    <td>
                        <telerik:RadUpload ID="RadUpload1" runat="server" ControlObjectsVisibility="None" />                        
                    </td>
                    <td>                                    
                       <%-- <div>
                            <asp:Label ID="labelNoResults" runat="server" Visible="True">No uploaded files yet</asp:Label>
            
                            <asp:Repeater ID="repeaterResults" runat="server" Visible="False">
                                <HeaderTemplate>
                                    <div class="title">Uploaded files in the target folder:</div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#DataBinder.Eval(Container.DataItem, "FileName")%>
                                    <%#DataBinder.Eval(Container.DataItem, "ContentLength").ToString() + " bytes"%>
                                    <br />
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>--%>
                    </td>
                </tr>
            </table>
            
            <div class="submitArea">
<asp:Button runat="server" ID="SubmitButton" Text="Upload files"
onclick="SubmitButton_Click" />
            </div>

            <telerik:RadProgressArea runat="server" ID="ProgressArea1">
             <Localization Uploaded="File upload progress: " UploadedFiles="Imported records: " CurrentFileName="" 
            TotalFiles="Total records: " EstimatedTime="" TransferSpeed="" />
            </telerik:RadProgressArea>
            <br />
            <asp:Label runat="server" ID="labelErrors" ForeColor="Red" />
            
            <br />
             <br />
            
            <table id="Filter" runat="server" visible="false">
            <tr>
                <td>
                   <h3>Missing Records</h3>
                </td>
            </tr>
                <tr>                          
                    <td>
                        PO #
                        <asp:TextBox ID="tbPONumberFilter" runat="server"></asp:TextBox>
                    </td>                    
                    <td>
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" CssClass="ptButton"/>
                    </td>
                    <td>
                        <asp:Button ID="btnClearFilter" runat="server" OnClick="btnClearFilter_Click" Text="Clear" CssClass="ptButton"/>
                    </td> 
                </tr>
            </table>
            
            <br />
            
            <telerik:RadGrid ID="RadGrid_Shipments" runat="server" Width="100%" GridLines="None" Visible ="false"
                AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True" 
                Skin="Windows7" OnItemCreated="RadGrid_Shipments_ItemCreated" >
                <ExportSettings ExportOnlyData="true" IgnorePaging="true">            
                </ExportSettings>
                <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true">
                        <CommandItemSettings                   
                            ShowExportToWordButton="true"
                            ShowExportToExcelButton="true"
                            ShowExportToCsvButton="true"
                            ShowExportToPdfButton="true"
                            />
                <Columns> 
                 <telerik:GridDateTimeColumn DataField="ShipDate" HeaderText="Ship Date" SortExpression="ShipDate"
                        UniqueName="ShipDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        DataFormatString="{0:MM/dd/yyyy}">
                        </telerik:GridDateTimeColumn>            
                <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="AccountNumber" SortExpression="AccountNumber"
                        UniqueName="AccountNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        EditFormColumnIndex="0">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="AccountName" HeaderText="Name" SortExpression="AccountName"
                        UniqueName="AccountName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>                  
                   <telerik:GridBoundColumn DataField="FedExTrackingNumber" HeaderText="FedExTrackingNumber" SortExpression="FedExTrackingNumber"
                        UniqueName="FedExTrackingNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>              
                   <telerik:GridBoundColumn DataField="PurchaseOrderNumber" HeaderText="PO Number" SortExpression="PurchaseOrderNumber"
                        UniqueName="PurchaseOrderNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="KitTypeName" HeaderText="KitType" SortExpression="KitTypeName"
                        UniqueName="KitTypeName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>    
                    <telerik:GridCheckBoxColumn DataField="IsReship" HeaderText="Reship?" SortExpression="IsReship"
                        UniqueName="IsReship" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true">
                        </telerik:GridCheckBoxColumn>                           
                    <telerik:GridDateTimeColumn DataField="ShippingStatusDate" HeaderText="ShippingStatusDate" SortExpression="ShippingStatusDate"
                        UniqueName="ShippingStatusDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true" 
                        DataFormatString="{0:MM/dd/yyyy}">
                        </telerik:GridDateTimeColumn>
                  <telerik:GridBoundColumn DataField="FedExStatusCode" HeaderText="FedExStatusCode" SortExpression="FedExStatusCode"
                        UniqueName="FedExStatusCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                        
              
            </Columns>               
            </MasterTableView>
    </telerik:RadGrid>

    </div>

</asp:Content>