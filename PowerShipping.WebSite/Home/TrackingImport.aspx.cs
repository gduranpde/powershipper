﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.ImportExport;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class TrackingImport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList1.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Tracking Import");
            }

            //if (!Page.IsPostBack)
            //    this.help.Text = string.Format("<a href='../Help.aspx?type={0}' target='_blank'\">import help</a>", typeof(ShipmentDetail).ToString());//this.help.Text = string.Format("<a href='#1' onclick=\"window.open('../Help.aspx?type={0}' ,'height=800','width=550','resizable=no','scrollbars=yes','toolbar=no','status=no')\">import help</a>", typeof(ShipmentDetail).ToString());
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            foreach (string fileInputID in Request.Files)
            {
                UploadedFile file = UploadedFile.FromHttpPostedFile(Request.Files[fileInputID]);
                if (file.ContentLength > 0)
                {
                    //string path = Path.Combine(ConfigurationManager.AppSettings["UploadFolder"], file.GetName());
                    var path = Server.MapPath("~/Uploads/") + file.GetName();
                    file.SaveAs(path);

                    var importer = new TrackingNumberImporter(path, User.Identity.Name);
                    importer.ImportProgress += ShowImportProgress;
                    TrackingNumberBatch batch = importer.Import();

                    labelErrors.Text = string.Empty;
                    if (!importer.ImportErrors.IsEmpty)
                    {
                        labelErrors.ForeColor = Color.Red;
                        importer.ImportErrors.ForEach(err => labelErrors.Text += err + "<br>");
                    }
                    else
                    {
                        labelErrors.ForeColor = Color.Black;
                        labelErrors.Text = "Total tracking numbers: " + batch.NumbersInBatch + ". <br/>Imported Successfully: " + batch.ImportedSuccessfully +
                                           ". <br/>Imported with exceptions: " + batch.ImportedWithException + ".";

                        FillGrid();
                    }
                }
            }
        }

        private void ShowImportProgress(object sender, ImportExportProgressEventArgs e)
        {
            if (Response.IsClientConnected)
            {
                RadProgressContext progress = RadProgressContext.Current;
                progress.SecondaryTotal = e.Total;
                progress.SecondaryValue = e.Value;
                progress.SecondaryPercent = e.Total == 0
                                                ? 0
                                                : Convert.ToInt32((100 * Convert.ToDouble(e.Value)) /
                                                                  Convert.ToDouble(e.Total));
                progress.CurrentOperationText = e.Message;
            }
        }

        #region Grid

        ShipmentDetailsPresenter presenter = new ShipmentDetailsPresenter();

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            FillGrid();
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            tbPONumberFilter.Text = null;
        }

        protected void RadGrid_Shipments_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                var spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                var addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void FillGrid()
        {
            List<ShipmentDetail> list = GetFilteredList();

            RadGrid_Shipments.Visible = true;
            RadGrid_Shipments.DataSource = list;
            RadGrid_Shipments.DataBind();

            Filter.Visible = true;

            //if (list.Count > 0)
            //{
            //    RadGrid_Shipments.Visible = true;
            //    RadGrid_Shipments.DataSource = list;
            //    RadGrid_Shipments.DataBind();
            //}
            //else
            //    RadGrid_Shipments.Visible = false;
        }

        private List<ShipmentDetail> GetFilteredList()
        {
            string poNum = tbPONumberFilter.Text;
            if (poNum == string.Empty)
                poNum = null;

            return presenter.GetMissingShipmentDetails(poNum);
        }

        #endregion Grid
    }
}