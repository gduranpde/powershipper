﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.WebSite.Controls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class CustomerLookup : Page
    {
	    readonly CustomerPresenter _presenter = new CustomerPresenter();
	    readonly UserManagementPresenter _presenterAdv = new UserManagementPresenter();

        bool _isExport;

        private bool _isFirstLoad;

        protected void Page_Init(object sender, EventArgs e)
        {
            CustomerFilter1.OnFilter += CustomerFilter1_OnFilter;
            CustomerFilter1.OnClear += CustomerFilter1_OnClear;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
			User u = _presenterAdv.GetOne(HttpContext.Current.User.Identity.Name);
			btnBulkExport.Visible = u.Roles.Contains(PDMRoles.ROLE_Administrator) || u.Roles.Contains(PDMRoles.ROLE_User);
			
	        if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Customer");
                _isFirstLoad = true;
            }

            CustomerProjects.OnProjectChanged += ProjectsList1_OnProjectChanged;
        }

        void ProjectsList1_OnProjectChanged(string projectId)
        {
            HiddenFiled_CustomQuery.Value = CustomerFilter1.CurrentQuery;
            RadGrid_Customers_NeedDataSource(null, null);
            RadGrid_Customers.Rebind();
        }

        void CustomerFilter1_OnFilter(string qry)
        {
            HiddenFiled_CustomQuery.Value = qry;
            RadGrid_Customers_NeedDataSource(null, null);
            RadGrid_Customers.Rebind();
            
        }

        void CustomerFilter1_OnClear(string qry)
        {
            HiddenFiled_CustomQuery.Value = qry;
        }


        protected void RadGrid_Customers_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item is GridDataItem) && !e.Item.IsInEditMode)
            {
                GridDataItem gridItem = (GridDataItem)e.Item;
                try
                {
                    Double myDouble = Convert.ToDouble(gridItem["Phone1"].Text);
                    gridItem["Phone1"].Text = myDouble.ToString("###-###-####");

                }
                catch
                {

                }
            }
            if (e.Item is GridDataItem && _isExport)
            {
                TableCell cell = (e.Item as GridDataItem)["AccountNumber"];
                LinkButton lb = cell.FindControl("EditButton") as LinkButton;
                cell.Text = lb.Text;
            }
        }

        protected void RadGrid_Customers_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.EditCommandName)
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scroll", "setTimeout(function(){ Expand('" + item.ClientID + "');}, 100);", true);
                }
            }
            if (e.CommandName == RadGrid.ExportToCsvCommandName || e.CommandName == RadGrid.ExportToExcelCommandName
                || e.CommandName == RadGrid.ExportToWordCommandName || e.CommandName == RadGrid.ExportToPdfCommandName)
            {
                _isExport = true;
            }
        }

        protected void RadGrid_Customers_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                Button spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = Common.IsCanEdit();

                LinkButton addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = Common.IsCanEdit();
            }
        }

        protected void RadGrid_Customers_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            string userId = "";
            string projectId = "";

            //if (CustomerProjects.ProjectId == Guid.Empty)
            //{
            //    return;
            //}

            if (CustomerProjects.ProjectId != Guid.Empty)
            {
                projectId = " AND ProjectId = '" + CustomerProjects.ProjectId + "'";
            }

            if (CustomerProjects.UserId != Guid.Empty)
            {
                userId = " AND UserId = '" + CustomerProjects.UserId + "'";
            }

            if (!_isFirstLoad)
            {

                if (!string.IsNullOrEmpty(HiddenFiled_CustomQuery.Value))
                {
                    string query = HiddenFiled_CustomQuery.Value;

                    query = GetQuery(userId, projectId, query);

                    RadGrid_Customers.DataSource = _presenter.GetFiltered(query);
                }
                //else
                //{
                //    string query = CustomerFilter1.DefaultQuery;

                //    query = GetQuery(userId, projectId, query);

                //    RadGrid_Customers.DataSource = presenter.GetFiltered(query);
                //}
               
            }
        }

        private string GetQuery(string userId, string projectId, string query)
        {
            if (!string.IsNullOrEmpty(userId)) //customer (not admin or user)
            {
                query = query.Replace("[vw_Customer]", " [vw_Customer]  AS CR INNER JOIN dbo.UserProject up ON up.ProjectId = cr.ProjectId");

                if (!string.IsNullOrEmpty(projectId)) // selected project (not All projects)
                {
                    query = query + " AND cr.ProjectId = '" + CustomerProjects.ProjectId + "'";
                }

                query = query + " AND UserId = '" + CustomerProjects.UserId + "'";
            }
            else
            {
                if (!string.IsNullOrEmpty(projectId)) // selected project (not All projects)
                {
                    query = query + " AND ProjectId = '" + CustomerProjects.ProjectId + "'";
                }
                else
                {
                    if (CustomerProjects.CompanyId != Guid.Empty)
                    {
                        List<Project> pList = _presenterAdv.GetProjectByCompany(CustomerProjects.CompanyId);
                        if (pList.Count != 0)
                        {
                            query = query + " AND ProjectId IN (";
                            foreach (Project p in pList)
                            {
                                query = query + "'" + p.Id + "',";
                            }
                            query = query.TrimEnd(',');
                            query = query + ")";
                        }
                    }
                }
            }
            return query;
        }

        protected void RadGrid_Customers_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.PerformInsertCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        if (e.Item is GridEditFormInsertItem)
                        {
                            Customer c = new Customer();

                            c.Id = Guid.NewGuid();

                            object o = ((CustomerCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                            Customer cNew = (Customer)o;

                            c.AccountNumber = cNew.AccountNumber;
                            c.InvitationCode = cNew.InvitationCode;
                           
                            c.CompanyName = cNew.CompanyName;
                            c.ContactName = cNew.ContactName;
                            c.Email1 = cNew.Email1;
                            c.Email2 = cNew.Email2;
                            c.MailingAddress1 = cNew.MailingAddress1;
                            c.MailingAddress2 = cNew.MailingAddress2;
                            c.MailingCity = cNew.MailingCity;
                            c.MailingState = cNew.MailingState;
                            c.MailingZip = cNew.MailingZip;

                            c.OkToContact = cNew.OkToContact;
                            c.OperatingCompany = cNew.OperatingCompany;
                            c.OptOut = cNew.OptOut;
                            c.Phone1 = cNew.Phone1;
                            c.Phone2 = cNew.Phone2;
                            c.PremiseID = cNew.PremiseID;
                            c.ProjectId = cNew.ProjectId;

                            c.ServiceAddress1 = cNew.ServiceAddress1;
                            c.ServiceAddress2 = cNew.ServiceAddress2;
                            c.ServiceCity = cNew.ServiceCity;
                            c.ServiceState = cNew.ServiceState;
                            c.ServiceZip = cNew.ServiceZip;

                            c.WaterHeaterType = cNew.WaterHeaterType;
                         
                            _presenter.Add(c);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                Label l = new Label();
                l.Text = "Unable to Inesert Customers. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_Customers.Controls.Add(l);
            }
        }

        protected void RadGrid_Customers_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        GridEditFormItem item = (GridEditFormItem)e.Item;
                        Guid id = new Guid(item.GetDataKeyValue("Id").ToString());
                        if (id != Guid.Empty)
                        {
                            if (e.Item is GridEditableItem)
                            {
                                object o =
                                    ((CustomerCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).
                                        FillEntity();
                                Customer cNew = (Customer)o;

                                Customer c = _presenter.GetById(cNew.Id);

                                c.AccountNumber = cNew.AccountNumber;
                                c.InvitationCode = cNew.InvitationCode;
                                
                                c.CompanyName = cNew.CompanyName;
                                c.ContactName = cNew.ContactName;
                                c.Email1 = cNew.Email1;
                                c.Email2 = cNew.Email2;
                                c.MailingAddress1 = cNew.MailingAddress1;
                                c.MailingAddress2 = cNew.MailingAddress2;
                                c.MailingCity = cNew.MailingCity;
                                c.MailingState = cNew.MailingState;
                                c.MailingZip = cNew.MailingZip;

                                c.OkToContact = cNew.OkToContact;
                                c.OperatingCompany = cNew.OperatingCompany;
                                c.OptOut = cNew.OptOut;
                                c.Phone1 = cNew.Phone1;
                                c.Phone2 = cNew.Phone2;
                                c.PremiseID = cNew.PremiseID;
                                c.ProjectId = cNew.ProjectId;

                                c.ServiceAddress1 = cNew.ServiceAddress1;
                                c.ServiceAddress2 = cNew.ServiceAddress2;
                                c.ServiceCity = cNew.ServiceCity;
                                c.ServiceState = cNew.ServiceState;
                                c.ServiceZip = cNew.ServiceZip;

                                c.WaterHeaterType = cNew.WaterHeaterType;

                                _presenter.Update(c);
                                RadGrid_Customers.Rebind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                Label l = new Label();
                l.Text = "Unable to update Customers. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_Customers.Controls.Add(l);
            }
        }

        protected void RadGrid_Customers_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.DeleteCommandName)
                {

                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                Label l = new Label();
                l.Text = "Unable to update Customers. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_Customers.Controls.Add(l);
                
            }
        }

        protected void LinkButton_EditButton_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            if (lb != null)
            {
                int itemIndex = Convert.ToInt32(lb.CommandName);
                GridEditableItem editableItem = (GridEditableItem)this.RadGrid_Customers.Items[itemIndex];
                editableItem.Edit = true;
                this.RadGrid_Customers.Rebind();
            }
        }

		protected void btnBulkExport_OnClick(object sender, EventArgs e)
		{
			string userId = "";
			string projectId = "";
			var source = new List<Customer>();

			if (CustomerProjects.ProjectId != Guid.Empty)
			{
				projectId = " AND ProjectId = '" + CustomerProjects.ProjectId + "'";
			}

			if (CustomerProjects.UserId != Guid.Empty)
			{
				userId = " AND UserId = '" + CustomerProjects.UserId + "'";
			}

			if (!string.IsNullOrEmpty(HiddenFiled_CustomQuery.Value))
			{
				string query = HiddenFiled_CustomQuery.Value;
				query = GetQuery(userId, projectId, query);

				source = _presenter.GetFiltered(query);
			}

			if (source.Count > 0)
			{
				ProduceCSV(source);
			}
		}

		private void ProduceCSV(IEnumerable<Customer> source)
		{
			var values = new string[26];

			#region Make the Header
			values[0] = "AccountNumber";
			values[1] = "InvitationCode";
			values[2] = "ContactName";
			values[3] = "CompanyName";
			values[4] = "MailingAddress1";
			values[5] = "MailingAddress2";
			values[6] = "MailingCity";
			values[7] = "MailingState";
			values[8] = "MailingZip";
			values[9] = "Email1";
			values[10] = "Email2";
			values[11] = "Phone1";
			values[12] = "Phone2";
			values[13] = "ServiceAddress1";
			values[14] = "ServiceAddress2";
			values[15] = "ServiceCity";
			values[16] = "ServiceState";
			values[17] = "ServiceZip";
			values[18] = "CompanyCode";
			values[19] = "ProjectCode";
			values[20] = "WaterHeaterType";
			values[21] = "OperatingCompany";
			values[22] = "PremiseID";
			values[23] = "OptOut";
			values[24] = "OkToContact";
			values[25] = "RateCode";
			#endregion

			Response.ClearContent();
			Response.Clear();
			Response.ContentType = "application/CSV";
			Response.AddHeader("Content-Disposition", "attachment;filename=Costumers.csv");
			Response.Write(string.Join(",", values));
			Response.Write(Environment.NewLine);

			if (source != null)
			{
				foreach (var item in source)
				{
					values = new string[41];
					foreach (PropertyInfo prop in item.GetType().GetProperties())
					{
						var propValue = prop.GetValue(item, null);

						#region Make items
						switch (prop.Name)
						{
							case "AccountNumber":
								values[0] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "InvitationCode":
								values[1] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "ContactName":
								values[2] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "CompanyName":
								values[3] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "MailingAddress1":
								values[4] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "MailingAddress2":
								values[5] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "MailingCity":
								values[6] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "MailingState":
								values[7] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "MailingZip":
								values[8] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "Email1":
								values[9] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "Email2":
								values[10] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "Phone1":
								values[11] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "Phone2":
								values[12] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "ServiceAddress1":
								values[13] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "ServiceAddress2":
								values[14] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "ServiceCity":
								values[15] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "ServiceState":
								values[16] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "ServiceZip":
								values[17] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "CompanyCode":
								values[18] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "ProjectCode":
								values[19] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "WaterHeaterType":
								values[20] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "OperatingCompany":
								values[21] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "PremiseID":
								values[22] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "OptOut":
								values[23] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "OkToContact":
								values[24] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "RateCode":
								values[25] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
						}
						#endregion
					}
					Response.Write(string.Join(",", values));
					Response.Write(Environment.NewLine);
				}
			}

			Response.Flush();
			Response.End();
		}
    }
}
