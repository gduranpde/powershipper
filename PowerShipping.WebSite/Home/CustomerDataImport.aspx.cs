﻿using PowerShipping.Business.Presenters;
using PowerShipping.Data;
using PowerShipping.ImportExport;
using PowerShipping.Logging;
using System;
using System.Configuration;
using System.Drawing;
using System.Web.Security;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class CustomerDataImport : System.Web.UI.Page
    {
	    private readonly CustomerDataImportExceptionPresenter _presenter = new CustomerDataImportExceptionPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList1.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Customer Data Import");
            }

            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;
            if (ProjectsList1.ProjectId == Guid.Empty)
                RadUpload1.Enabled = false;
        }

        void ProjectsList1_OnProjectChanged(string projectId)
        {

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                RadUpload1.Enabled = true;

                int recordsNo = DataProvider.Current.Customer.CountRecords(projectId);
                labelCountRecords.Text = "Current Records in Master Database: " + recordsNo;
            }
            else
            {
                RadUpload1.Enabled = false;
                labelCountRecords.Text = string.Empty;
            }
            labelErrors.Text = string.Empty;

	        btnDownloadExp.Visible = false;
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
			btnDownloadExp.Visible = false;

	        int exceptionCount = 0;

			if (ProjectsList1.ProjectId == Guid.Empty)
			{
				labelErrors.ForeColor = Color.Red;
				labelErrors.Text = "Please select project.";
				return;
			}

            foreach (string fileInputId in Request.Files)
            {
                try
                {
                    UploadedFile file = UploadedFile.FromHttpPostedFile(Request.Files[fileInputId]);
                    if (file.ContentLength > 0)
                    {
						if (!string.IsNullOrEmpty(errorLabelAppendReplace.Text))
						{
							errorLabelAppendReplace.Text = "";
						}

	                    if (string.IsNullOrEmpty(RadioButtonList.SelectedValue))
	                    {
		                    errorLabelAppendReplace.Text = "Please select an upload mode: append or replace";
		                    return;
	                    }

	                    var path = Server.MapPath("~/Uploads/") + file.FileName;
	                    file.SaveAs(path);
	                    CurrentLogger.Log.Info("File saved at this path:" + path);

	                    var bulkLoadPath = ConfigurationManager.AppSettings["SqlBulkLoadPath"] + file.FileName;

	                    MembershipUser user = Membership.GetUser(User.Identity.Name);
	                    string userId = user.ProviderUserKey.ToString();

	                    //Replace
	                    int recordsNo;
	                    if (RadioButtonList.SelectedValue == "Replace")
	                    {
		                    var importer = new CustomerImporter(bulkLoadPath, userId, ProjectsList1.ProjectId, "R", exceptionCount);
		                    importer.ImportProgress += ShowImportProgress;

		                    Entities.CustomerImportHistory history = importer.Import(true);

		                    recordsNo = DataProvider.Current.Customer.CountRecords(ProjectsList1.ProjectId.ToString());
		                    labelCountRecords.Text = "Current Records in Master Database: " + recordsNo;
		                    exceptionCount = history.CountRecords - recordsNo;

		                    DataProvider.Current.CustomerImportHistory.UpdateHistory(history.Id.ToString(), exceptionCount, recordsNo);

		                    SetImportInfo(importer, history, recordsNo, exceptionCount);
	                    }
	                    else if (RadioButtonList.SelectedValue == "Append")
	                    {
		                    int initialNoOfRecordsInCustomer = DataProvider.Current.Customer.CountRecords(ProjectsList1.ProjectId.ToString());

							CurrentLogger.Log.Info("In Append ..");
		                    var importer = new CustomerImporter(bulkLoadPath, userId, ProjectsList1.ProjectId, "A", exceptionCount);
		                    importer.ImportProgress += ShowImportProgress;

		                    CurrentLogger.Log.Info("Before - importer.AppendImport();");
		                    Entities.CustomerImportHistory history = importer.Import(false);
		                    CurrentLogger.Log.Info("After - importer.AppendImport();");

		                    recordsNo = DataProvider.Current.Customer.CountRecords(ProjectsList1.ProjectId.ToString());
		                    labelCountRecords.Text = "Current Records in Master Database: " + recordsNo;
		                    int countRecords = recordsNo - initialNoOfRecordsInCustomer;
		                    exceptionCount = history.CountRecords - countRecords;

		                    DataProvider.Current.CustomerImportHistory.UpdateHistory(history.Id.ToString(), exceptionCount, countRecords);

		                    SetImportInfo(importer, history, countRecords, exceptionCount);
	                    }
                    }
                }
                catch (Exception ex)
                {
                    CurrentLogger.Log.Error(ex);
                    Log.LogError(ex);
                }
            }
        }

	    private void SetImportInfo(CustomerImporter importer, Entities.CustomerImportHistory history, int recordsNo, int exceptionCount)
	    {
		    labelErrors.Text = string.Empty;
		    if (!importer.ImportErrors.IsEmpty)
		    {
			    labelErrors.ForeColor = Color.Red;
			    importer.ImportErrors.ForEach(err => labelErrors.Text += err + "<br>");
		    }
		    else
		    {
			    hlExceptions.Visible = false;

			    labelErrors.ForeColor = Color.Black;
			    labelErrors.Text = "Total requests: " + history.CountRecords +
			                       ". <br/>Imported Successfully: " + recordsNo + ". <br/>Exceptions: " +
			                       exceptionCount + ". </br>";
		    }

			if (exceptionCount > 0)
			{
				historyId.Value = history.Id.ToString();
				btnDownloadExp.Visible = true;
			}
	    }

		protected void btnDownloadExp_Click(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(historyId.Value))
			{
				var values = new string[3];

				#region Make the Header

				values[0] = "RecordNumber";
				values[1] = "Reason";
				values[2] = "RecordData";

				#endregion

				Response.ClearContent();
				Response.Clear();
				Response.ContentType = "application/CSV";
				Response.AddHeader("Content-Disposition", "attachment;filename=ConsumerDataImportExceptions.csv");
				Response.Write(string.Join(",", values));
				Response.Write(Environment.NewLine);

				var source = _presenter.GetByCustomerDataImportId(new Guid(historyId.Value));

				foreach (var item in source)
				{
					values = new string[3];
					foreach (var prop in item.GetType().GetProperties())
					{
						var propValue = prop.GetValue(item, null);

						#region Make items

						switch (prop.Name)
						{
							case "RecordNumber":
								values[0] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "Reason":
								values[1] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "RecordData":
								values[2] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
						}

						#endregion
					}
					Response.Write(string.Join(",", values));
					Response.Write(Environment.NewLine);
				}

				Response.Flush();
				Response.End();
			}
		}

	    private void ShowImportProgress(object sender, ImportExportProgressEventArgs e)
        {
            if (Response.IsClientConnected)
            {
                var progress = RadProgressContext.Current;
                progress.SecondaryTotal = e.Total;
                progress.SecondaryValue = e.Value;
                progress.SecondaryPercent = e.Total == 0 ? 0 : Convert.ToInt32((100 * Convert.ToDouble(e.Value)) / Convert.ToDouble(e.Total));
                progress.CurrentOperationText = e.Message;
            }
        }
    }
}
