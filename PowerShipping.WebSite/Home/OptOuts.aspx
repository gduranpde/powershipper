<%@ Page Title="Opt Outs" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" CodeBehind="OptOuts.aspx.cs" Inherits="PowerShipping.WebSite.OptOuts" Theme="Default" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register src="../Controls/ProjectsList.ascx" tagname="ProjectsList" tagprefix="uc1" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="pageDiv">
    
  
  <table>
      <tr>
            <td>              
                 <h3>Opt Outs</h3>
            </td>
            <td colspan=7 align="center">
                 <uc1:ProjectsList ID="ProjectsList1" runat="server" IsCompanyUse="true" />
            </td>
        </tr>
        <tr>
        
            <td>
                Account Number 
                <asp:TextBox ID="tbAccountNumberFilter" runat="server"></asp:TextBox>         
            </td>
            <td>
                Account Name
                <asp:TextBox ID="tbAccountNameFilter" runat="server"></asp:TextBox>
            </td>
            <td>
                Phone Number
                <asp:TextBox ID="tbPhoneFilter" runat="server"></asp:TextBox>
            </td>
            <td>
                Address
                <asp:TextBox ID="tbAddressFilter" runat="server"></asp:TextBox>
            </td>
            <td>
                Start Date
                <telerik:RadDatePicker ID="dpStartDateFilter" runat="server" MinDate="01-01-1990" MaxDate="01-01-2020"></telerik:RadDatePicker>
            </td>
            <td>
                End Date
                <telerik:RadDatePicker ID="dpEndDateFilter" runat="server" MinDate="01-01-1990" MaxDate="01-01-2020"></telerik:RadDatePicker>
            </td>
            <td>
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" CssClass="ptButton"/>
            </td>
            <td>
                <asp:Button ID="btnClearFilter" runat="server" OnClick="btnClearFilter_Click" Text="Clear" CssClass="ptButton"/>
            </td>
        </tr>
    </table>
       
      
  
    <telerik:RadGrid ID="RadGrid_OptOuts" runat="server" Width="100%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True" 
              Skin="Windows7" 
        onneeddatasource="RadGrid_OptOuts_NeedDataSource" 
        oninsertcommand="RadGrid_OptOuts_InsertCommand" 
        onupdatecommand="RadGrid_OptOuts_UpdateCommand"
        OnDeleteCommand="RadGrid_OptOuts_DeleteCommand"
        OnItemDataBound="RadGrid_OptOuts_ItemDataBound"
        OnItemCommand="RadGrid_OptOuts_ItemCommand"
        OnItemCreated="RadGrid_OptOuts_ItemCreated"
        >
            <ExportSettings ExportOnlyData="true" IgnorePaging="true">            
            </ExportSettings>
            <ClientSettings >
                        <Resizing AllowColumnResize="True">
                        </Resizing> 
                        <Selecting AllowRowSelect="true" />                      
            </ClientSettings>
            <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
            AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true">
            <CommandItemSettings
                    ShowExportToWordButton="true"
                    ShowExportToExcelButton="true"
                    ShowExportToCsvButton="true"
                    ShowExportToPdfButton="true"
                    />
            <Columns>             
                <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" >
                <HeaderStyle Width="30px"/>
                    </telerik:GridEditCommandColumn>
               <%-- <telerik:GridButtonColumn UniqueName="DeleteColumn" Text="Delete" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmText="Are you sure delete this row?"/>
                --%>
                
                 <telerik:GridBoundColumn DataField="OptOutNumber" HeaderText="OptOutNumber" SortExpression="OptOutNumber"
                        UniqueName="OptOutNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" ReadOnly="true"
                        EditFormColumnIndex="0">
                        <HeaderStyle Width="100px"/>
                        </telerik:GridBoundColumn>
               
                  <telerik:GridDateTimeColumn DataField="OptOutDate" HeaderText="OptOutDate" SortExpression="OptOutDate"
                        UniqueName="OptOutDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        DataFormatString="{0:MM/dd/yyyy}">
                        <HeaderStyle Width="80px"/>
                        </telerik:GridDateTimeColumn>    
                 <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="AccountNumber" SortExpression="AccountNumber" 
                        UniqueName="AccountNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        EditFormColumnIndex="0"> 
                        <HeaderStyle Width="100px"/>                       
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="AccountName" HeaderText="AccountName" SortExpression="AccountName"
                        UniqueName="AccountName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="Address" HeaderText="Address" SortExpression="Address"
                        UniqueName="Address" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        <HeaderStyle Width="240px"/> 
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="City" HeaderText="City" SortExpression="City"
                        UniqueName="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="State" HeaderText="State" SortExpression="State"
                        UniqueName="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">  
                        <HeaderStyle Width="40px"/>                      
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="ZipCode" HeaderText="ZipCode" SortExpression="ZipCode"
                        UniqueName="ZipCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" MaxLength="5">
                        <HeaderStyle Width="80px"/>
                        </telerik:GridBoundColumn>
                   <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email" Visible="false"
                        UniqueName="Email" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>                                         
                   <%--<telerik:GridBoundColumn DataField="Phone1" HeaderText="Phone1" SortExpression="Phone1"
                        UniqueName="Phone1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        <HeaderStyle Width="30px"/>
                  </telerik:GridBoundColumn>
                   <telerik:GridBoundColumn DataField="Phone2" HeaderText="Phone2" SortExpression="Phone2" Visible="false"
                        UniqueName="Phone2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                  </telerik:GridBoundColumn> --%>
                  
                  <telerik:GridMaskedColumn DataField="Phone1" HeaderText="Phone 1" SortExpression="Phone1" Mask="###-###-####"
                        UniqueName="Phone1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        DataFormatString="{0:(###)###-####}">   
                        <HeaderStyle Width="85px"/>                 
                    </telerik:GridMaskedColumn> 
                  <telerik:GridMaskedColumn DataField="Phone2" HeaderText="Phone 2" SortExpression="Phone2" Mask="###-###-####" Visible="false"
                        UniqueName="Phone2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        DataFormatString="{0:(###)###-####}">                    
                    </telerik:GridMaskedColumn> 
                  
                   <telerik:GridCheckBoxColumn DataField="IsEnergyProgram" HeaderText="Program Opt Out" SortExpression="IsEnergyProgram" ItemStyle-HorizontalAlign="Center"
                        UniqueName="IsEnergyProgram" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true" DefaultInsertValue="true">
                        <HeaderStyle Width="65px"/>
                    </telerik:GridCheckBoxColumn>
                   <telerik:GridCheckBoxColumn DataField="IsTotalDesignation" HeaderText="Total Opt Out" SortExpression="IsTotalDesignation"
                        UniqueName="IsTotalDesignation" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true">
                        <HeaderStyle Width="90px"/>
                   </telerik:GridCheckBoxColumn>
                   
                   <telerik:GridBoundColumn DataField="Notes" HeaderText="Notes" SortExpression="Notes"
                        UniqueName="Notes" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>  
                   
                    <telerik:GridBoundColumn DataField="CompanyCode" HeaderText="CompanyCode" SortExpression="CompanyCode"
                                    UniqueName="CompanyCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">                                
                                </telerik:GridBoundColumn> 
                          <telerik:GridBoundColumn DataField="ProjectCode" HeaderText="ProjectCode" SortExpression="ProjectCode"
                                    UniqueName="ProjectCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">                                
                                </telerik:GridBoundColumn> 
                        
            </Columns>
             <EditFormSettings UserControlName="../Controls/OptOutCard.ascx" EditFormType="WebUserControl">
                <EditColumn UniqueName="EditCommandColumn1">
                </EditColumn>
             </EditFormSettings>
            </MasterTableView>
    </telerik:RadGrid>
    
     <asp:Panel ID="pnlOverlay" style="display:none;" runat="server"></asp:Panel>
                     
     <asp:Panel runat="server" ID="pnlPopup" CssClass="modalPopupBig">
        <table width="100%">
            <tr>
                <td align="center">
                    <h2> ERROR </h2>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lbErrPopUp" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">                   
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnCancelError" runat="server" Text="Ok" />
                </td>
            </tr>
        </table>   
     </asp:Panel>                                                                                   
    
     <ajaxToolkit:ModalPopupExtender runat="server" ID="mpeError" BehaviorID="mpeError"
                 DropShadow="false" BackgroundCssClass="errorBackground" OkControlID="btnCancelError"
                 TargetControlID="pnlOverlay" PopupControlID="pnlPopup" CancelControlID="btnCancelError">
     </ajaxToolkit:ModalPopupExtender>
    
<%--<asp:LinkButton  runat="server" ID="LinkButton_DeleteOptOut" Text="Delete" 
            onclick="LinkButton_DeleteOptOut_Click" CommandArgument="testArg" CommandName="testName" CssClass="hiddenElement"></asp:LinkButton>--%>
            
            
        <script type="text/javascript">

         function Expand(itemID) {
             var Grid = $find('<%=RadGrid_OptOuts.ClientID %>');
             var scrollArea = document.getElementById('ctl00_ContentPlaceHolder1_RadGrid_OptOuts')
            var rowElement = document.getElementById(itemID);            
            window.scrollTo(0, rowElement.offsetTop + 200);
        }
        </script>
</div>
</asp:Content>
