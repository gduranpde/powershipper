﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class AuditReportAccounts : Page
    {
        private readonly AuditReportPresenter presenter = new AuditReportPresenter();
        private bool _isFirstLoad = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList1.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }
            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;

            if(!IsPostBack)
            {
                List<KitType> list = presenter.GetAllKitTypes();
                List<ListItem> items = new List<ListItem>();
                items.Add(new ListItem("All", ""));
                foreach (var kit in list)
                {
                    items.Add(new ListItem(kit.KitName, kit.Id.ToString()));
                }
                this.RadComboBox_KitType.DataSource = items;
                this.RadComboBox_KitType.DataBind(); 
            }
            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Audit Delivery Report");
                _isFirstLoad = true;
            }
        }

        void ProjectsList1_OnProjectChanged(string projectId)
        {
            RadGrid_Accounts_NeedDataSource(null,null);
            RadGrid_Accounts.Rebind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            RadGrid_Accounts_NeedDataSource(null, null);
            RadGrid_Accounts.Rebind();
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            tbPONumberFilter.Text = null;
            dpStartDateFilter.SelectedDate = null;
            dpEndDateFilter.SelectedDate = null;
            RadComboBox_KitType.SelectedValue = null;
        }

        protected void RadGrid_Accounts_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                var spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                var addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGrid_Accounts_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (!_isFirstLoad)
                RadGrid_Accounts.DataSource = GetFilteredListAccounts();
        }

        protected void RadGrid_Accounts_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item is GridDataItem) && !e.Item.IsInEditMode)
            {
                GridDataItem gridItem = (GridDataItem)e.Item;
                try
                {
                    Double myDouble = Convert.ToDouble(gridItem["Phone1"].Text);
                    gridItem["Phone1"].Text = myDouble.ToString("###-###-####");

                    Double myDouble2 = Convert.ToDouble(gridItem["Phone2"].Text);
                    gridItem["Phone2"].Text = myDouble2.ToString("###-###-####");
                }
                catch
                {

                }
            }
        }

        private DataSet GetFilteredListAccounts()
        {
            string poNum = tbPONumberFilter.Text;
            DateTime? startDate = dpStartDateFilter.SelectedDate;
            DateTime? endDate = dpEndDateFilter.SelectedDate;

            if (endDate != null)
                endDate = endDate.Value.AddDays(1);

            Guid? kitType = null;
            string kit = RadComboBox_KitType.SelectedValue;
            if(!string.IsNullOrEmpty(kit))
                kitType = new Guid(RadComboBox_KitType.SelectedValue);

            if (poNum == string.Empty)
                poNum = null;

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList1.ProjectId;
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = ProjectsList1.UserId;
            }

            if (ProjectsList1.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList1.CompanyId;
            }


            return presenter.GetAuditReportAccounts(poNum, startDate, endDate, kitType, userId, projectId, companyId);
        }

		protected void btnBulkExport_OnClick(object sender, EventArgs e)
		{
			DataSet ds = GetFilteredListAccounts();
			DataTable source = ds.Tables[0];

			string[] values = new string[16];

			#region Make the Header
			values[0] = "AccountNumber";
			values[1] = "AccountName";
			values[2] = "ServiceAddress1";
			values[3] = "ServiceAddress2";
			values[4] = "ServiceCity";
			values[5] = "ServiceState";
			values[6] = "ServiceZip";
			values[7] = "Email";
			values[8] = "Phone1";
			values[9] = "Phone2";
			values[10] = "AnalysisDate";
			values[11] = "ReceiptDate";
			values[12] = "KitTypeName";
			values[13] = "OperatingCompany";
			values[14] = "WaterHeaterFuel";
			values[15] = "TransactionType";
			#endregion

			Response.ClearContent();
			Response.Clear();
			Response.ContentType = "application/CSV";
			Response.AddHeader("Content-Disposition", "attachment;filename=AuditDeliveryReport.csv");
			Response.Write(string.Join(",", values));
			Response.Write(Environment.NewLine);

			if (source != null)
			{
				var ordColumns = new int[values.Length];

				foreach (DataColumn col in source.Columns)
				{
					for (var i = 0; i < values.Length; i++)
					{
						if (col.ColumnName == values[i])
						{
							ordColumns[i] = col.Ordinal; break;
						}
					}
				}

				foreach (DataRow dr in source.Rows)
				{
					values = new string[16];

					for (var i = 0; i < ordColumns.Length; i++)
					{
						var val = dr[ordColumns[i]];
						var col = source.Columns[ordColumns[i]];

						if (col.DataType == typeof(DateTime))
						{
							values[i] = val != DBNull.Value ? "\"" + Convert.ToDateTime(val).ToString("MM/dd/yyyy") + "\"" : string.Empty;
						}
						else
						{
							values[i] = val != DBNull.Value ? "\"" + val.ToString() + "\"" : string.Empty;
						}
					}

					Response.Write(string.Join(",", values));
					Response.Write(Environment.NewLine);
				}
			}

			Response.Flush();
			Response.End();
		}
    }
}