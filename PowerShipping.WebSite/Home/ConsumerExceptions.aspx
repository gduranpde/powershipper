﻿<%@ Page Title="Consumer Exceptions" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" CodeBehind="ConsumerExceptions.aspx.cs" Inherits="PowerShipping.WebSite.ConsumerExceptions" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../Controls/ProjectsList.ascx" TagName="ProjectsList" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="pageDiv">


        <table>
            <tr>
                <td>
                    <h3>Consumer Exceptions</h3>
                </td>
                <td colspan="5" align="center">
                    <uc1:ProjectsList ID="ProjectsList1" runat="server" IsCompanyUse="true" />
                </td>
            </tr>
            <tr>

                <td>Account Number 
                <asp:TextBox ID="tbAccountNumberFilter" runat="server"></asp:TextBox>
                </td>
                <td>Account Name
                <asp:TextBox ID="tbAccountNameFilter" runat="server"></asp:TextBox>
                </td>
                <td>Import Date
                <telerik:raddatepicker id="dpStartDateFilter" runat="server" mindate="01-01-1990" maxdate="01-01-2020"></telerik:raddatepicker>
                    <%--Phone Number
                <asp:TextBox ID="tbPhoneFilter" runat="server"></asp:TextBox>--%>
                </td>
                <td>Batch ID
                <asp:TextBox ID="tbBatchID" runat="server"></asp:TextBox>
                    <%-- Address
                <asp:TextBox ID="tbAddressFilter" runat="server"></asp:TextBox>--%>
                </td>
                <td>
                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" CssClass="ptButton" />
                </td>
                <td>
                    <asp:Button ID="btnClearFilter" runat="server" OnClick="btnClearFilter_Click" Text="Clear" CssClass="ptButton" />
                </td>
            </tr>
        </table>


        <telerik:radgrid id="RadGrid_ConsumerRequests" runat="server" width="100%" gridlines="None"
            autogeneratecolumns="false" pagesize="15" allowsorting="True" allowpaging="True"
            skin="Windows7"
            onneeddatasource="RadGrid_ConsumerRequests_NeedDataSource"
            onupdatecommand="RadGrid_ConsumerRequests_UpdateCommand"
            ondeletecommand="RadGrid_ConsumerRequests_DeleteCommand"
            onitemcreated="RadGrid_ConsumerRequests_ItemCreated"
            onitemdatabound="RadGrid_ConsumerRequests_ItemDataBound"
            onitemcommand="RadGrid_ConsumerRequests_ItemCommand">
        <ExportSettings ExportOnlyData="true" IgnorePaging="true">            
            </ExportSettings>
            <ClientSettings>
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
            AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true">
            <CommandItemSettings
                    ShowExportToWordButton="true"
                    ShowExportToExcelButton="true"
                    ShowExportToCsvButton="true"
                    ShowExportToPdfButton="true"
                    />

            <Columns>
	             <telerik:GridButtonColumn ButtonType="PushButton" Text="Override " CommandName="Override" UniqueName="btnOverride">
                </telerik:GridButtonColumn>
                  <telerik:GridTemplateColumn DataField="AccountNumber" HeaderText="AccountNumber" SortExpression="AccountNumber"
                        UniqueName="AccountNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        EditFormColumnIndex="0">
						<ItemTemplate>
							<asp:LinkButton ID="EditButton" runat="server" CommandName="Edit" Text="<%# ((PowerShipping.Entities.ConsumerRequestException)Container.DataItem).AccountNumber %>"></asp:LinkButton>
						</ItemTemplate>
					<HeaderStyle Width="100px" />
                  </telerik:GridTemplateColumn>
                  <telerik:GridBoundColumn DataField="AccountName" HeaderText="AccountName" SortExpression="AccountName"
                        UniqueName="AccountName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="Address1" HeaderText="Address1" SortExpression="Address1"
                        UniqueName="Address1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="Address2" HeaderText="Address2" SortExpression="Address2" Visible="false"
                        UniqueName="Address2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="City" HeaderText="City" SortExpression="City"
                        UniqueName="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="State" HeaderText="State" SortExpression="State"
                        UniqueName="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="ZipCode" HeaderText="ZipCode" SortExpression="ZipCode"
                        UniqueName="ZipCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        <HeaderStyle Width="80px"/>
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email"
                        UniqueName="Email" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                        
                      
                        <telerik:GridMaskedColumn DataField="Phone1" HeaderText="Phone 1" SortExpression="Phone1" Mask="###-###-####"
                        UniqueName="Phone1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        DataFormatString="{0:(###)###-####}">                    
                            </telerik:GridMaskedColumn> 
                        <telerik:GridMaskedColumn DataField="Phone2" HeaderText="Phone 2" SortExpression="Phone2" Mask="###-###-####" Visible="false"
                        UniqueName="Phone2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        DataFormatString="{0:(###)###-####}">                    
                            </telerik:GridMaskedColumn> 
                                            
                         <telerik:GridBoundColumn DataField="AnalysisDate" HeaderText="AnalysisDate" SortExpression="AnalysisDate"
                        UniqueName="AnalysisDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Reason" HeaderText="Reason" SortExpression="Reason" ReadOnly="true"
                        UniqueName="Reason" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                        
                        <telerik:GridBoundColumn DataField="Method" HeaderText="Method" SortExpression="Method" Visible="false"
                        UniqueName="Method" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn> 
                         <telerik:GridBoundColumn DataField="IsOkayToContact" HeaderText="IsOkayToContact" SortExpression="IsOkayToContact" Visible="false"
                        UniqueName="IsOkayToContact" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>  
                         <telerik:GridBoundColumn DataField="OperatingCompany" HeaderText="Operating Company" SortExpression="OperatingCompany" Visible="false"
                        UniqueName="OperatingCompany" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="WaterHeaterFuel" HeaderText="Water Heater Fuel" SortExpression="WaterHeaterFuel" Visible="false"
                        UniqueName="WaterHeaterFuel" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                        
                        <telerik:GridBoundColumn DataField="Notes" HeaderText="Notes" SortExpression="Notes" Visible="false" UniqueName="Notes">
                        </telerik:GridBoundColumn>
                        
                      <telerik:GridTemplateColumn UniqueName="Status" DataField="Status" HeaderText="Status" Visible="false">
                       <ItemTemplate>
                            <%# Eval("Status")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList runat="server" ID="ddlStatus" DataValueField="Value" DataTextField="Text">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>                  
                    <telerik:GridCheckBoxColumn DataField="OutOfState" HeaderText="OutOfState" SortExpression="OutOfState" Visible="false"
                        UniqueName="OutOfState" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true">
                        <HeaderStyle Width="50px"/>
                        </telerik:GridCheckBoxColumn>  
                    
                    <telerik:GridBoundColumn DataField="BatchLabel" HeaderText="Batch ID" SortExpression="BatchLabel" ReadOnly="true"
                        UniqueName="BatchLabel" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>    
                    <telerik:GridDateTimeColumn DataField="ImportedDate" HeaderText="ImportedDate" SortExpression="ImportedDate" ReadOnly="true"
                        UniqueName="ImportedDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        DataFormatString="{0:MM/dd/yyyy}">
                        <HeaderStyle Width="80px"/> 
                    </telerik:GridDateTimeColumn>                           
            </Columns>
                <EditFormSettings UserControlName="../Controls/ConsumerExceptionCard.ascx" EditFormType="WebUserControl">
                <EditColumn UniqueName="EditCommandColumn1">
                </EditColumn>
             </EditFormSettings>
            </MasterTableView>
    </telerik:radgrid>

        <br />
        <table>
            <tr>
                <td>
                   <%--<asp:Button ID="btnValidate" runat="server" OnClick="btnValidate_Click" Text="ReSubmit" CssClass="ptButton" />--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbReuslt" runat="server" Text="" />
                </td>
            </tr>
        </table>

		<div id="BlockOverridesWarning" class="div_BlockOverridesWarningOuter">
            <div style="width: 400px; height: 30px; cursor: move;" onmousedown="mouse_down(event,'BlockOverridesWarning');"
                onmouseup="mouse_up(event,'BlockOverridesWarning');">
                <div style="width: 200px; float: left; padding-top: 10px;">
                    <span style="margin-left: 10px;">Warning Message!</span>
                </div>
                <div style="width: 200px; float: right; padding-top: 10px;">
                    <input id="btnCloseDeviceID" type="button" value="" onclick="return BlockOverridePopup();"
                        style="background-image: url('../../App_Themes/Default/Images/btnPopupClose.png');
                        border: none; width: 48px; height: 21px; position: relative; margin-left: 147px;
                        margin-top: -11px; cursor: pointer;" />
                </div>
            </div>
            <div style="width: 400px; margin-top: 27px;">
                <div style="height: 10px; margin-left: 15px; right: -1px; width: 362px;">
                    <asp:Label ID="LblWarningMessage" runat="server" Text="There are additional exceptions beside duplicate ones. This Consumer Request Exception cannot be processed. Please click OK to continue."
                        ForeColor="Red" Style="margin-left: 10px;"></asp:Label>
                </div>
            </div>
            <br />
            <br />
            <div style="width: 400px; margin-top: 55px;">
                <div style="width: 200px; float: right;">
                    <input id="cancel_BlockOverridesWarning" type="button" value="Ok" onclick="return BlockOverridePopup();"
                        style="margin-left: -29px; margin-top: -22px; cursor: pointer;" />
                </div>
            </div>
        </div>
        <script type="text/javascript">

	        function Expand(itemID) {
		        var Grid = $find('<%=RadGrid_ConsumerRequests.ClientID %>');
		        var scrollArea = document.getElementById('ctl00_ContentPlaceHolder1_RadGrid_ConsumerRequests')
		        var rowElement = document.getElementById(itemID);
		        window.scrollTo(0, rowElement.offsetTop + 200);
	        }

        	function BlockOverridePopup() {
        		var popupDiv = document.getElementById("BlockOverridesWarning");
        		popupDiv.style.display = (popupDiv.style.display == "block") ? "none" : "block";
        		return false;
        	}
        </script>
		 <style type="text/css">
        
        .div_BlockOverridesWarningOuter
        {
            display: none;
            height: 205px;
            width: 402px;
            background-image: url(  '../../App_Themes/Default/Images/div_back_AuditReport12.png' );
            background-repeat: no-repeat;
            z-index: 7001;
            position: absolute;
            left: 485px;
            top: 230px;
            background-position: -2px -2px;
            border-radius: 7px;
        }
    </style>
    </div>
</asp:Content>
