﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class AddressChangeReport : System.Web.UI.Page
    {
        AddressChangeReportPresenter presenter = new AddressChangeReportPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList1.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int) Errors.HaventRights);
            }
            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Address Change Report");
            }
        }

        void ProjectsList1_OnProjectChanged(string projectId)
        {
            RadGrid_AddressChanges.Rebind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            RadGrid_AddressChanges.Rebind();
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            dpStartDateFilter.SelectedDate = null;
            dpEndDateFilter.SelectedDate = null;
            tbAccountNumber.Text = null;
        }

        protected void RadGrid_AddressChanges_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                var spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                var addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGrid_AddressChanges_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_AddressChanges.DataSource = GetFilteredList();
        }

        protected void RadGrid_AddressChanges_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item is GridDataItem) && !e.Item.IsInEditMode)
            {
                GridDataItem gridItem = (GridDataItem)e.Item;
                try
                {
                    Double myDouble = Convert.ToDouble(gridItem["Phone1"].Text);
                    gridItem["Phone1"].Text = myDouble.ToString("###-###-####");

                    Double myDouble2 = Convert.ToDouble(gridItem["Phone2"].Text);
                    gridItem["Phone2"].Text = myDouble2.ToString("###-###-####");
                }
                catch
                {

                }
            }
        }

        private DataSet GetFilteredList()
        {
            DateTime? startDate = dpStartDateFilter.SelectedDate;
            DateTime? endDate = dpEndDateFilter.SelectedDate;

            if (endDate != null)
                endDate = endDate.Value.AddDays(1);

            string accNum = tbAccountNumber.Text;

            if (accNum == string.Empty)
                accNum = null;

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList1.ProjectId;
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = ProjectsList1.UserId;
            }

            if (ProjectsList1.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList1.CompanyId;
            }

            return presenter.GetAll(startDate, endDate, accNum, userId, projectId, companyId);
        }
    }
}
