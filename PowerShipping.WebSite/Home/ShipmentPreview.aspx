﻿<%@ Page Title="Label Request Preview" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" CodeBehind="ShipmentPreview.aspx.cs" Inherits="PowerShipping.WebSite.Home.ShipmentPreview" Theme="Default" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<div class="pageDiv">
    <h3>Label Request Preview</h3>
    
    <asp:HyperLink ID="hlReturn" runat="server" NavigateUrl="~/Home/Shipments.aspx" Text="Return to Request Labels List"></asp:HyperLink>
    <br />
    <br />
    <telerik:RadGrid ID="RadGrid_ShipmentDetails" runat="server" Width="100%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True" 
              Skin="Windows7" 
        onneeddatasource="RadGrid_ShipmentDetails_NeedDataSource" 
       OnItemCreated="RadGrid_ShipmentDetails_ItemCreated"
        OnItemDataBound="RadGrid_ShipmentDetails_ItemDataBound" >
            <ExportSettings ExportOnlyData="true" IgnorePaging="true">            
            </ExportSettings>
             <ClientSettings >
                        <Resizing AllowColumnResize="True">
                        </Resizing>                       
            </ClientSettings>
            <MasterTableView DataKeyNames="RecipientId" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
             AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true">
            <CommandItemSettings
                    ShowExportToWordButton="true"
                    ShowExportToExcelButton="true"
                    ShowExportToCsvButton="true"
                    ShowExportToPdfButton="true"
                    />
            <Columns>             
                 <telerik:GridBoundColumn DataField="RecipientId" HeaderText="RecipientId" SortExpression="RecipientId"
                        UniqueName="RecipientId" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Contact" HeaderText="Contact" SortExpression="Contact"
                        UniqueName="Contact" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Company" HeaderText="Company" SortExpression="Company"
                        UniqueName="Company" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Address1" HeaderText="Address1" SortExpression="Address1"
                        UniqueName="Address1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Address2" HeaderText="Address2" SortExpression="Address2"
                        UniqueName="Address2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="City" HeaderText="City" SortExpression="City"
                        UniqueName="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="State" HeaderText="State" SortExpression="State"
                        UniqueName="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="CountryCode" HeaderText="CountryCode" SortExpression="CountryCode"
                        UniqueName="CountryCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Zip" HeaderText="Zip" SortExpression="Zip"
                        UniqueName="Zip" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Phone" HeaderText="Phone" SortExpression="Phone"
                        UniqueName="Phone" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Service" HeaderText="Service" SortExpression="Service"
                        UniqueName="Service" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Residential" HeaderText="Residential" SortExpression="Residential"
                        UniqueName="Residential" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Weight1" HeaderText="Weight1" SortExpression="Weight1"
                        UniqueName="Weight1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Weight2" HeaderText="Weight2" SortExpression="Weight2"
                        UniqueName="Weight2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Weight3" HeaderText="Weight3" SortExpression="Weight3"
                        UniqueName="Weight3" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Weight4" HeaderText="Weight4" SortExpression="Weight4"
                        UniqueName="Weight4" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Weight5" HeaderText="Weight5" SortExpression="Weight5"
                        UniqueName="Weight5" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Reference" HeaderText="Reference" SortExpression="Reference"
                        UniqueName="Reference" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Reference2" HeaderText="Reference2" SortExpression="Reference2"
                        UniqueName="Reference2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Reference3" HeaderText="Reference3" SortExpression="Reference3"
                        UniqueName="Reference3" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Reference4" HeaderText="Reference4" SortExpression="Reference4"
                        UniqueName="Reference4" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Reference5" HeaderText="Reference5" SortExpression="Reference5"
                        UniqueName="Reference5" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="InvoiceNumber" HeaderText="InvoiceNumber" SortExpression="InvoiceNumber"
                        UniqueName="InvoiceNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="PurchaseOrderNumber" HeaderText="PurchaseOrderNumber" SortExpression="PurchaseOrderNumber"
                        UniqueName="PurchaseOrderNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="DepartmentNotes" HeaderText="DepartmentNotes" SortExpression="DepartmentNotes"
                        UniqueName="DepartmentNotes" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
            </Columns>
            </MasterTableView>
    </telerik:RadGrid>
</div>
</asp:Content>