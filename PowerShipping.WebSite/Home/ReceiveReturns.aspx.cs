﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class ReceiveReturns : Page
    {
        private RecieveReturnsPresenter presenter = new RecieveReturnsPresenter();
        private ConsumerRequestsPresenter presenterCR = new ConsumerRequestsPresenter();

        private bool firstLoad = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList1.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }

            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;

            tbShipmentNumber.Focus();

            if (IsPostBack)
                firstLoad = false;
            else
                firstLoad = true;

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Receive Returns");

                ddlReasons.DataSource = presenter.GetReasonsAll();
                ddlReasons.DataBind();
                TxtReturnDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                //RadDatePicker_ReceiveReturnDate.SelectedDate = DateTime.Now;
            }
        }

        private void ProjectsList1_OnProjectChanged(string projectId)
        {
            RadGrid_Grid.Rebind();
        }

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            try
            {
                lbMessages.Text = "";

                RadGrid_Grid.Rebind();
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                lbMessages.Text = "Error. Reason: " + ex.Message;
                lbMessages.ControlStyle.ForeColor = Color.Red;
            }
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            tbShipmentNumber.Text = null;
            tbTrackingNumber.Text = null;
            tbAccNum.Text = null;
            tbAccName.Text = null;
        }

        #region Grid

        protected void RadGrid_Grid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (firstLoad)
            {
                RadGrid_Grid.DataSource = new List<ShipmentDetail>();
                RadGrid_Grid.Visible = false;
            }
            else
            {
                RadGrid_Grid.DataSource = FillGrid();
                RadGrid_Grid.Visible = true;
            }
        }

        protected void RadGrid_Grid_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                var spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                var addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGrid_Grid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "Return")
            {
                var item = (GridDataItem)e.Item;
                string id = item.GetDataKeyValue("Id").ToString();
                hfId.Value = id;

                if (ddlReasons.Items.Count > 0)
                    ddlReasons.SelectedIndex = 0;
                tbNotes.Text = "";

                ShipmentDetail sd = presenter.GetById(new Guid(id));
                tbPopUpShipNum.Text = sd.ShipmentNumber;
                tbPopUpTrackNum.Text = sd.FedExTrackingNumber;
                tbPopUpName.Text = sd.AccountName;

                tbNotes.Text = sd.Notes;
                if (sd.ReasonId != null)
                    ddlReasons.SelectedValue = sd.ReasonId.ToString();
                bool bReturnsAllowed = true;
                bReturnsAllowed = Convert.ToBoolean(item.GetDataKeyValue("ReturnsAllowed"));
                if (bReturnsAllowed == true)
                {
                    mpe.Show();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "popup();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "BlockReturnsPopup", "BlockReturnsPopup();", true);
                }
            }
        }

        private List<ShipmentDetail> FillGrid()
        {
            string shipmentNum = tbShipmentNumber.Text;
            string trNum = tbTrackingNumber.Text;
            string accNum = tbAccNum.Text;
            string accName = tbAccName.Text;

            if (accNum == string.Empty)
                accNum = null;
            if (accName == string.Empty)
                accName = null;
            if (trNum == string.Empty)
                trNum = null;
            if (shipmentNum == string.Empty)
                shipmentNum = null;

            if (trNum != null)
            {
                if (trNum.Length > 20)
                    trNum = trNum.Substring(trNum.Length - 20);
            }

            if (shipmentNum != null)
            {
                if (shipmentNum.Length > 10)
                    shipmentNum = shipmentNum.Substring(0, 10);
            }

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList1.ProjectId;
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = ProjectsList1.UserId;
            }

            if (ProjectsList1.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList1.CompanyId;
            }

            return presenter.GetByTrackingNumber(shipmentNum, trNum, accNum, accName, userId, projectId, companyId);
        }

        #endregion Grid

        protected void btnRet_Click(object sender, EventArgs e)
        {
            ShipmentDetail sd = presenter.GetById(new Guid(hfId.Value));

            if (sd == null)
            {
                lbMessages.Text = "Shipment not found";
                lbMessages.ControlStyle.ForeColor = System.Drawing.Color.Red;
                return;
            }

            sd.ShippingStatus = ShipmentShippingStatus.Returned;
            sd.ShippingStatusDate = DateTime.Now;
            sd.ReshipStatus = ShipmentReshipStatus.NA;
            //sd.ReturnDate = RadDatePicker_ReceiveReturnDate.SelectedDate;
            sd.ReturnDate = Convert.ToDateTime(TxtReturnDate.Text);

            sd.Notes = tbNotes.Text;
            if (!string.IsNullOrEmpty(ddlReasons.SelectedValue))
                sd.ReasonId = new Guid(ddlReasons.SelectedValue);

            ConsumerRequest cr = presenterCR.GetById(sd.ConsumerRequestId);
            cr.Status = ConsumerRequestStatus.Returned;

            presenter.Save(sd);
            presenterCR.Save(cr);

            lbMessages.Text = "UPDATED";
            lbMessages.ControlStyle.ForeColor = System.Drawing.Color.Green;

            RadGrid_Grid.Rebind();

            mpe.Hide();
        }
    }
}