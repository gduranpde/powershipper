﻿using Ionic.Zip;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Xml.Linq;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class AEGXMLDeliveryReportv3 : AEGReportsV3PageBase
    {
        #region Class Data
        XElement previousNode;
        #endregion

        #region Grid Events
        protected void RadGrid_Accounts_ItemCreated(object sender, GridItemEventArgs e)
        {
            base.RadGrid_ItemCreated(sender, e);
        }
        protected void RadGrid_Accounts_ItemDataBound(object sender, GridItemEventArgs e)
        {
            base.RadGrid_ItemDataBound(sender, e);
        }
        protected void RadGrid_Accounts_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            try
            {
                NeedDataSource(e, RadGrid_Accounts);
            }
            catch (Exception ex)
            {
                Log.LogError(ex);
                AddError(ex.Message);
            }
        }
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }

            if (!IsPostBack)
            {
                base.GetAllKits(RadComboBox_KitType);
                base.SetTitleAndHelpLink("Click here for help", "AEG XML Delivery Report - V3");
            }
            ProjectsList.OnProjectChanged += (projectId) => { ProjectChangedHandler(RadGrid_Accounts); };
            btnSearch.Click += (s, ev) => { base.SearchClickHandler(ProjectsList.ProjectId, RadGrid_Accounts, lblError, false); };
            btnGenerateXml.Click += (s, ev) => { base.GenerateXMLClick(this.Page, ProjectsList.ProjectId, LblWarningMessage); };
            previousNode = null;
            lblError.Text = string.Empty;
            base.errorLabel = lblError;
        }
        #endregion

        #region Event Handlers
        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            tbPONumberFilter.Text = null;
            dpStartDateFilter.SelectedDate = null;
            dpEndDateFilter.SelectedDate = null;
            RadComboBox_KitType.SelectedValue = null;
        }
        protected void BulkExport_Click(object sender, EventArgs e)
        {
            if (ProjectsList.ProjectId == Guid.Empty)
            {
                LblWarningMessage.Text = "Please select a single project.";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AEGXMLReportFailed", "AEGXMLReportFailed();", true);
            }
            else
            {
                ProjectPresenter objProjPresenter = new ProjectPresenter();
                string ProgramID = objProjPresenter.GetProgramID(ProjectsList.ProjectId);
                if (ProgramID == null || ProgramID == string.Empty)
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AEGXMLReportFailed", "AEGXMLReportFailed();", true);
                }
                else
                {
                    ProduceCSV();
                }
            }
        }
        #endregion

        #region Helpers
        protected override void NodeGenerator()
        {
            var recordCount = 0;
            const int maxRecordsInFile = 2000;
            string poNum = tbPONumberFilter.Text;
            DateTime? startDate = dpStartDateFilter.SelectedDate;
            DateTime? endDate = dpEndDateFilter.SelectedDate;

            if (endDate != null)
                endDate = endDate.Value.AddDays(1);

            Guid? kitType = null;
            string kit = RadComboBox_KitType.SelectedValue;
            if (!string.IsNullOrEmpty(kit))
                kitType = new Guid(RadComboBox_KitType.SelectedValue);

            if (poNum == string.Empty)
                poNum = null;

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList.ProjectId;
            }

            if (ProjectsList.UserId != Guid.Empty)
            {
                userId = ProjectsList.UserId;
            }

            if (ProjectsList.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList.CompanyId;
            }

            var dt = presenter.GetAEGXMLDeliveryReportV3(poNum, startDate, endDate, kitType, userId, projectId, companyId).Tables[0];
            List<string> filesGenerated = new List<string>();
            XDocument _document = new XDocument();
            int files = 1;
            if (dt != null)
            {
                //Creating the parent node
                XElement _root = new XElement("systemRecord");
                _document.Add(_root);
                var groupedResult = dt.Select().GroupBy(row => new {ac = row["AccountNumber"].ToString(), bi = row["Id"].ToString()});
                foreach (var item in groupedResult)
                {                   
                    // because of different catalogs id, the result has same rows byut different catalog id
                    // so in the xml there must be only one record  per account number

                    var row = item.ToList().First();

                    if (recordCount < maxRecordsInFile)
                    {
                        CreateImportRecord(row, _root);
                        recordCount++;
                    }
                    else
                    {
                        recordCount = 0;
                        filesGenerated.Add(Server.MapPath("~/Uploads/AEG_DeliveryReport_" + files + ".xml"));
                        _document.Save(Server.MapPath("~/Uploads/AEG_DeliveryReport_" + files + ".xml"), SaveOptions.None);
                        _document = new XDocument();
                        _root = new XElement("systemRecord");
                        _document.Add(_root);
                        files++;
                        CreateImportRecord(row, _root);
                    }

             }

                _document.Save(Server.MapPath("~/Uploads/AEG_DeliveryReport_" + files + ".xml"), SaveOptions.None);
                filesGenerated.Add(Server.MapPath("~/Uploads/AEG_DeliveryReport_" + files + ".xml"));

                AddFilesToZip("AEG_XML_DeliveryReport.zip", filesGenerated);

                string path = Server.MapPath("~/Uploads/AEG_XML_DeliveryReport.zip");
                string filename = "AEG_XML_DeliveryReport.zip";

                Response.ClearContent();
                Response.Clear();
                Response.ContentType = "application/zip";
                Response.AppendHeader("Content-Disposition", "attachment;filename=" + filename + ";");
                Response.TransmitFile(path);
                Response.Flush();
                Response.End();


            }
        }
        protected override DataSet GetFilteredList()
        {
            string poNum = tbPONumberFilter.Text;
            DateTime? startDate = dpStartDateFilter.SelectedDate;
            DateTime? endDate = dpEndDateFilter.SelectedDate;

            if (endDate != null)
                endDate = endDate.Value.AddDays(1);

            Guid? kitType = null;
            string kit = RadComboBox_KitType.SelectedValue;
            if (!string.IsNullOrEmpty(kit))
                kitType = new Guid(RadComboBox_KitType.SelectedValue);

            if (poNum == string.Empty)
                poNum = null;

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList.ProjectId;
            }

            if (ProjectsList.UserId != Guid.Empty)
            {
                userId = ProjectsList.UserId;
            }

            if (ProjectsList.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList.CompanyId;
            }

            return presenter.GetAEGDeliveryReportV3(poNum, startDate, endDate, kitType, userId, projectId, companyId);
        }
        private XElement EquipmentAttributeNode(XElement _root, DataRow dr)
        {
            XElement content = new XElement("importEquipment");
            EquipmentAttributesDeclarations(dr);
            string description = string.Empty;
            string receiptDateEA = string.Empty;
            foreach (DataRow dre in EquipmentAttributes.Rows)
            {
                if (dre["ReceiptDate"] != DBNull.Value && Convert.ToString(dre["ReceiptDate"]) != null || Convert.ToString(dre["ReceiptDate"]) != string.Empty)
                {
                    receiptDateEA = Convert.ToDateTime(dre["ReceiptDate"]).ToString("MM/dd/yyyy");
                }
                CatalogID = Convert.ToString(dre["AccountNumber"]).Trim() + "_" + Convert.ToString(dre["CatalogID"]).Trim();
                CatalogID = CatalogID.Trim();
                //if (dre["Quantity"] == DBNull.Value || dre["Quantity"] == null || Convert.ToInt32(dre["Quantity"]) == 0)
                //{
                //    dre["Quantity"] = 1;
                //}
                //BCA 20141127
                if (dre["Quantity"] == DBNull.Value || dre["Quantity"] == null )
                    if (Convert.ToInt32(dre["Quantity"]) == 0)
                    {
                        dre["Quantity"] = 0;
                    }
                    else
                    {
                        dre["Quantity"] = 1;
                    }
                if (dre["KiQuantity"] != DBNull.Value && dre["KiQuantity"] != null)
                {
                    iQuantity = Convert.ToInt32(dre["Quantity"]) * Convert.ToInt32(dre["KiQuantity"]);
                }
                description = Convert.ToString(dre["KitName"]).Trim();
                XElement temp =
                        new XElement("importEquipmentItem",
                                   CatalogID.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.equipmentRefId, CatalogID) : null,
                                   new XElement("equipmentAttributes",
                                   description.CheckDbNullOrEmpty() ? new XElement("attribute",

                                          (description.CheckDbNullOrEmpty() ? new XElement("attributeName", XmlFormatter.Description) : null),
                                          (description.CheckDbNullOrEmpty() ? new XElement("attributeValue", description) : null)

                                   ) : null,
                                   dre["CatalogID"].CheckDbNullOrEmpty() ? new XElement("attribute",

                                          (dre["CatalogID"].CheckDbNullOrEmpty() ? new XElement("attributeName", XmlFormatter.CatalogID) : null),
                                          (dre["CatalogID"].CheckDbNullOrEmpty() ? new XElement("attributeValue", Convert.ToString(dre["CatalogID"]).Trim()) : null)

                                   ) : null,
                                   dre["ReceiptDate"].CheckDbNullOrEmpty() ? new XElement("attribute",

                                          (receiptDateEA.CheckDbNullOrEmpty() ? new XElement("attributeName", XmlFormatter.Date_installed) : null),
                                          (receiptDateEA.CheckDbNullOrEmpty() ? new XElement("attributeValue", receiptDateEA) : null)

                                   ) : null,
                                    iQuantity.CheckDbNullOrEmpty() ? new XElement("attribute",

                                          (iQuantity.CheckDbNullOrEmpty() ? new XElement("attributeName", XmlFormatter.Quantity) : null),
                                          (iQuantity.CheckDbNullOrEmpty() ? new XElement("attributeValue", iQuantity) : null)
                                   ) : null,
                                   dre["KwImpact"].CheckDbNullOrEmpty() ? new XElement("attribute",
                                        new XElement("attributeName", XmlFormatter.Kw_Impact),
                                        new XElement("attributeValue", Convert.ToDecimal(dre["KwImpact"]))
                                   ) : null,
                                   dre["KwhImpact"].CheckDbNullOrEmpty() ? new XElement("attribute",
                                       new XElement("attributeName", XmlFormatter.Kwh_Impact),
                                       new XElement("attributeValue", Convert.ToDecimal(dre["KwhImpact"]))
                                   ) : null
                               ));

                if (previousNode != null)
                {
                    if (previousNode.Value != temp.Value)
                    {
                        content.Add(temp);
                    }
                    previousNode = temp;
                }
                else
                {
                    content.Add(temp);
                    previousNode = temp;
                }
            }
            return content;
        }
        private void AddFilesToZip(string zipFile, List<string> filesToAdd)
        {
            using (ZipFile zip = new ZipFile())
            {
                zip.AddFiles(filesToAdd, "Files");
                zip.Save(Server.MapPath("~/Uploads/AEG_XML_DeliveryReport.zip"));
            }
        }
        private void CreateImportRecord(DataRow dr, XElement root)
        {
            string FirstName = string.Empty;
            string LastName = string.Empty;
            XElement content4;
            if (Convert.ToString(dr["AccountName"]).Contains(' '))
            {
                String[] Name = ParseName(Convert.ToString(dr["AccountName"]));
                FirstName = Convert.ToString(Name[0]);
                LastName = Convert.ToString(Name[1]);
            }
            else
            {
                FirstName = Convert.ToString(dr["AccountName"]);
            }
            if (dr["AnalysisDate"] != DBNull.Value && Convert.ToString(dr["AnalysisDate"]) != null || Convert.ToString(dr["AnalysisDate"]) != string.Empty)
            {
                AnalysisDate = Convert.ToDateTime(dr["AnalysisDate"]).ToString("MM/dd/yyyy");
            }
            if (dr["ReceiptDate"] != DBNull.Value && Convert.ToString(dr["ReceiptDate"]) != null || Convert.ToString(dr["ReceiptDate"]) != string.Empty)
            {
                ReceiptDate = Convert.ToDateTime(dr["ReceiptDate"]).ToString("MM/dd/yyyy");
            }
            if (dr["ShipDate"] != DBNull.Value && Convert.ToString(dr["ShipDate"]) != null || Convert.ToString(dr["ShipDate"]) != string.Empty)
            {
                ShipDate = Convert.ToDateTime(dr["ShipDate"]).ToString("MM/dd/yyyy");
            }
            if (dr["Quantity"] == DBNull.Value || dr["Quantity"] == null || Convert.ToInt32(dr["Quantity"]) == 0)
            {
                dr["Quantity"] = 1;
            }
            if (dr["KwhImpact"] != DBNull.Value && dr["KwhImpact"] != null)
            {
                Kwh_Savings = Convert.ToInt64(dr["KwhImpact"]) * Convert.ToInt32(dr["Quantity"]);
            }
            if (dr["KwImpact"] != DBNull.Value && dr["KwImpact"] != null)
            {
                Kw_savings = Convert.ToInt64(dr["KwImpact"]) * Convert.ToInt32(dr["Quantity"]);
            }
            string refID_Full = Convert.ToString(dr["Id"]);
            int refID_Length = refID_Full.Length;
            int refID_StartIndex = refID_Length - 4;
            string RefIDPortion = refID_Full.Substring(refID_StartIndex, 4);
            string AccountNumber = Convert.ToString(dr["AccountNumber"]);

            string refIDConcat = "PD_" + RefIDPortion + AccountNumber + "_" + ReceiptDate;
            string refID;
            string contactRefIDPortion = string.Empty;

            string contactRefID = "PD_Pri_Cont_" + Convert.ToString(dr["AccountNumber"]);
            if (contactRefID.Length > 38)
            {
                contactRefIDPortion = contactRefID.Substring(1, 38);
            }
            else
            {
                contactRefIDPortion = contactRefID;
            }
            if (refIDConcat.Length > 50)
            {
                refID = refIDConcat.Substring(1, 50);
            }
            else
            {
                refID = refIDConcat;
            }
            if (dr["ReceiptDate"] != DBNull.Value && dr["ReceiptDate"] != null)
            { MonthShipped = GetFirstDayOfMonth(Convert.ToDateTime(dr["ReceiptDate"])); }

            content4 = new XElement("importRecord",
                   (dr["ProgramID"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.programNumber, Convert.ToString(dr["ProgramID"])) : null),
                   (refID.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.refID, refID) : null),
                   (FirstName.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.customerFirstname, FirstName.Trim()) : null),
                   (LastName.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.customerLastname, LastName) : null),
                   (dr["AccountNumber"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.customerAccountNumber, Convert.ToString(dr["AccountNumber"])) : null),
                   (dr["AccountNumber"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.primaryContactId, "PD_Pri_Cont_" + Convert.ToString(dr["AccountNumber"])) : null),
                   (dr["AccountNumber"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.premiseContactId, "PD_Pri_Cont_" + Convert.ToString(dr["AccountNumber"])) : null),
                   new XElement(XmlFormatter.status, "Delivered"),
                   (AnalysisDate.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.applicationDate, AnalysisDate) : null),
            new XElement("importContacts", ImportContactNode(dr, contactRefIDPortion, FirstName, LastName)),
                 ImportApplicationNode(root, dr),
                 EquipmentAttributeNode(root, dr)
            );
            root.Add(content4);
        }
        private XElement ImportApplicationNode(XElement _root, DataRow dr)
        {
            var xel = new XElement("importApplication",
                            AnalysisDate.CheckDbNullOrEmpty() ? new XElement("importApplicationItem",
                                new XElement("field", XmlFormatter.AnalysisDate),
                                new XElement("value", AnalysisDate)) : null,
                            ReceiptDate.CheckDbNullOrEmpty() ? new XElement("importApplicationItem",
                                new XElement("field", XmlFormatter.ReceiptDate),
                                new XElement("value", ReceiptDate)) : null,
                            CreateXElement("importApplicationItem", XmlFormatter.Operating_Company, "OperatingCompany", dr),
                            CreateXElement("importApplicationItem", XmlFormatter.Water_Heater_Fuel, "WaterHeaterFuel", dr),
                            CreateXElement("importApplicationItem", XmlFormatter.Heater_Fuel, "HeaterFuel", dr),
                            CreateXElement("importApplicationItem", XmlFormatter.OK_to_Contact, "IsOkayToContact", dr),
                            CreateXElement("importApplicationItem", XmlFormatter.Facility_Type, "FacilityType", dr),
                            CreateXElement("importApplicationItem", XmlFormatter.Shipment_Date, "ShipDate", dr),
                            CreateXElement("importApplicationItem", XmlFormatter.Rate_Code, "RateCode", dr),
                            CreateXElement("importApplicationItem", XmlFormatter.Enrollment_Completion_Date, "AnalysisDate", dr),
                            CreateXElement("importApplicationItem", XmlFormatter.Income_Qualified, "IncomeQualified", dr),
                            CreateXElement("importApplicationItem", XmlFormatter.Number_Kits_Shipped, "Quantity", dr),
                            Convert.ToString(MonthShipped).CheckDbNullOrEmpty() ? new XElement("importApplicationItem",
                                               new XElement("field", XmlFormatter.Month_Shipped),
                                               new XElement("value", MonthShipped.ToString("MM/dd/yyyy"))) : null,
                           new XElement("importApplicationItem",
                                               new XElement("field", XmlFormatter.Kwh_Savings),
                                               new XElement("value", Kwh_Savings)),
                           new XElement("importApplicationItem",
                                               new XElement("field", XmlFormatter.Kw_savings),
                                               new XElement("value", Kw_savings)));

            return xel;
        }
        private void ProduceCSV()
        {
            DataTable source = GetFilteredList().Tables[0];

            string[] values = new string[26];
            #region Make the Header
            values[0] = "AccountNumber";
            values[1] = "AccountName";
            values[2] = "ServiceAddress1";
            values[3] = "ServiceAddress2";
            values[4] = "ServiceCity";
            values[5] = "ServiceState";
            values[6] = "ServiceZip";
            values[7] = "Email";
            values[8] = "Phone1";
            values[9] = "Phone2";
            values[10] = "AnalysisDate";
            values[11] = "ReceiptDate";
            values[12] = "KitTypeName";
            values[13] = "OperatingCompany";
            values[14] = "WaterHeaterFuel";
            values[15] = "HeaterFuel";
            values[16] = "IsOkayToContact";
            values[17] = "RateCode";
            values[18] = "TransactionType";
            values[19] = "CatalogID";
            values[20] = "Quantity";
            values[21] = "ProgramID";
            values[22] = "CompanyName";
            values[23] = "KwImpact";
            values[24] = "KwhImpact";
            values[25] = "IncomeQualified";
            #endregion

            Response.ClearContent();
            Response.Clear();
            Response.ContentType = "application/CSV";
            Response.AddHeader("Content-Disposition", "attachment;filename=AEG_DeliveryReport_V3.csv");
            Response.Write(string.Join(",", values));
            Response.Write(Environment.NewLine);

            if (source != null)
            {
                var ordColumns = new int[values.Length];

                foreach (DataColumn col in source.Columns)
                {
                    for (var i = 0; i < values.Length; i++)
                    {
                        if (col.ColumnName == values[i])
                        {
                            ordColumns[i] = col.Ordinal; 
                            break;
                        }
                    }
                }
                
                foreach (DataRow dr in source.Rows)
                {                    
                    values = new string[26];

                    for (var i = 0; i < ordColumns.Length; i++)
                    {
                        var val = dr[ordColumns[i]];
                        var col = source.Columns[ordColumns[i]];

                        if (col.DataType == typeof(DateTime))
                        {
                            values[i] = val != DBNull.Value ? "\"" + Convert.ToDateTime(val).ToString("MM/dd/yyyy") + "\"" : string.Empty;
                        }
                        else
                        {
                            values[i] = val != DBNull.Value ? "\"" + val.ToString() + "\"" : string.Empty;
                        }
                    }

                    Response.Write(string.Join(",", values));
                    Response.Write(Environment.NewLine);
                }
            }

            Response.Flush();
            Response.End();
        }
        #endregion
    }
}