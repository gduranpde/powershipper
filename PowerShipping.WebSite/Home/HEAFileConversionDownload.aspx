﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Shared/Default.Master"
    CodeBehind="HEAFileConversionDownload.aspx.cs" Inherits="PowerShipping.WebSite.Home.HEAFileConversionDownload"
    Theme="Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="pageDiv">
        <div style="border: solid 1px grey; margin: 11px 157px 10px; padding-left: 20px;
            padding-right: 20px; width: 500px;">
            <table>
                <tr style="height: 5px">
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblNoOfRecords" runat="server" Text="# of Records Converted :" Font-Bold="True"
                            Style="margin-left: 8px;"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LblTotalRecords" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
            <table>
                <tr style="height: 10px">
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="BtnDownload" runat="server" Text="DownloadCSV" OnClick="BtnDownload_Click" />
                    </td>
                </tr>
                <tr style="height: 5px">
                </tr>
            </table>
        </div>
    </div>
</asp:Content>