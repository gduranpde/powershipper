﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class OptOutBatches : Page
    {
        private readonly BatchesPresenter presenter = new BatchesPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList1.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }

            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Opt Out Batches");
            }
        }

        void ProjectsList1_OnProjectChanged(string projectId)
        {
            RadGrid_Batches.Rebind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            RadGrid_Batches.Rebind();
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            dpStartDateFilter.SelectedDate = null;
        }

        protected void RadGrid_Batches_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                var spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                var addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGrid_Batches_ItemDataBound(object sender, GridItemEventArgs e)
        {
        }

        protected void RadGrid_Batches_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_Batches.DataSource = GetFilteredList();
        }

        protected void RadGrid_Batches_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.DeleteCommandName)
                {
                    var id =
                        new Guid(
                            (e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"].ToString());
                    presenter.DeleteOptOutBatch(id);
                    RadGrid_Batches.Rebind();
                }
            }
            catch (Exception ex)
            {
                var l = new Label();
                l.Text = "Unable to delete Batch. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                RadGrid_Batches.Controls.Add(l);
            }
        }

        private List<OptOutBatch> GetFilteredList()
        {
            DateTime? startDate = dpStartDateFilter.SelectedDate;

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList1.ProjectId;
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = ProjectsList1.UserId;
            }

            if (ProjectsList1.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList1.CompanyId;
            }

            return presenter.GetOptOutBatches(startDate, userId, projectId, companyId);
        }
    }
}