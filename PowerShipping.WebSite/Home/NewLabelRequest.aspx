﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Shared/Default.Master"  CodeBehind="NewLabelRequest.aspx.cs" Inherits="PowerShipping.WebSite.Home.NewLabelRequest"  Theme="Default" %>


<%@ Register src="~/Controls/ShipmentCommon.ascx" tagname="ShipmentCommon" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="pageDiv">
       <asp:HiddenField  ID="HiddenField_Id" runat="server" />    
<asp:MultiView id="WizardMultiView" ActiveViewIndex="0" runat="Server">
               <asp:View id="Page1" runat="Server">
                
                    <uc2:ShipmentCommon ID="ShipmentCommon_Common" runat="server" />
                
                    <asp:Button id="Page1Back"
                        Text = "Previous"
                        OnClick="BackButton_Command"                       
                        runat= "Server" CssClass="ptButton">
                    </asp:Button> 

                    <asp:Button id="Page1Next"
                        Text = "Next"
                        OnClick="NextButton_Command"                       
                        runat="Server" CssClass="ptButton">
                    </asp:Button> 

                </asp:View>               

                <asp:View id="Page2" runat="Server">
                    <asp:Button id="btnViewFile"
                        Text = "View"
                        OnClick="btnViewFile_OnClick"                       
                        runat="Server" CssClass="ptButton">
                    </asp:Button> 
                    <asp:Button id="PageSave"
                        Text = "Send"
                        OnClick="NextButton_Command"                       
                        runat="Server" CssClass="ptButton">
                    </asp:Button>
                    <asp:Button id="PageRestart"
                        Text = "Undo"
                        OnClick="BackButton_Command"                       
                        runat= "Server" CssClass="ptButton">
                    </asp:Button>  
                </asp:View>  

            </asp:MultiView>

    
    
    <asp:Literal ID="Label_MessageLabel" runat="server"></asp:Literal>
    
    <br />
    <asp:Label ID="lbError" runat="server"></asp:Label>
</div>
    
</asp:Content>