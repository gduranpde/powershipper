﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReshipImportBatches.aspx.cs"
    MasterPageFile="~/Shared/Default.Master" Inherits="PowerShipping.WebSite.Home.ReshipImportBatches"
    Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../Controls/ReshipImportBatchesFilter.ascx" TagName="ReshipImportBatchesFilter"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <uc2:ReshipImportBatchesFilter ID="ReshipImportBatchesFilter1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="pageDiv">
        <table>
            <tr>
                <td>
                    <h3>
                        Reship Import Batches</h3>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <%-- <tr>
                <td>
                    Start Date
                    <telerik:RadDatePicker ID="dpStartDateFilter" runat="server" MinDate="01-01-1990"
                        MaxDate="01-01-2020">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="ptButton" />
                </td>
                <td>
                    <asp:Button ID="btnClearFilter" runat="server" Text="Clear" CssClass="ptButton" />
                </td>
            </tr>--%>
        </table>
        <br />
        <telerik:RadGrid ID="RadGrid_Batches1" runat="server" Width="100%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True"
            Skin="Windows7" OnNeedDataSource="RadGrid_Batches1_NeedDataSource" OnItemCreated="RadGrid_Batches1_ItemCreated"
            OnDeleteCommand="RadGrid_Batches1_DeleteCommand" OnItemCommand="RadGrid_Batches1_ItemCommand1">
            <%--  OnItemDataBound="RadGrid_Batches_ItemDataBound"--%>
            <ExportSettings ExportOnlyData="true" IgnorePaging="true">
            </ExportSettings>
            <ClientSettings>
            </ClientSettings>
            <MasterTableView DataKeyNames="ReshipImportBatchID" AllowMultiColumnSorting="True"
                Width="100%" CommandItemDisplay="Top" AutoGenerateColumns="false" EditMode="EditForms"
                PagerStyle-AlwaysVisible="true">
                <CommandItemSettings ShowExportToWordButton="true" ShowExportToExcelButton="true"
                    ShowExportToCsvButton="true" ShowExportToPdfButton="true" />
                <Columns>
                    <telerik:GridButtonColumn UniqueName="DeleteColumn" Text="Delete" CommandName="Delete"
                        HeaderText="Delete" ConfirmDialogType="RadWindow" ConfirmText="Are you sure delete this batch?" />
                    <telerik:GridButtonColumn UniqueName="ViewFileColumn" Text="View File" CommandName="ViewFile"
                        HeaderText="View File" />
                    <telerik:GridButtonColumn UniqueName="ViewExceptionsColumn" Text="View Exceptions"
                        HeaderText="View Exceptions" CommandName="ViewException" />
                    <telerik:GridBoundColumn DataField="ReshipImportBatchName" HeaderText="BatchName"
                        SortExpression="ReshipImportBatchName" ReadOnly="true" UniqueName="ReshipImportBatchName"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ShipperName" HeaderText="ShipperName" SortExpression="ShipperName"
                        UniqueName="ShipperName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NumberOfRecords" HeaderText="# of Records" SortExpression="NumberOfRecords"
                        UniqueName="NumberOfRecords" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ImportedSuccessfully" HeaderText="# Successful"
                        SortExpression="ImportedSuccessfully" UniqueName="ImportedSuccessfully" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NumberWithExceptions" HeaderText="# with Exceptions"
                        SortExpression="NumberWithExceptions" UniqueName="NumberWithExceptions" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="SourceFileName" HeaderText="File Name" SortExpression="SourceFileName"
                        UniqueName="SourceFileName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ImportedBy" HeaderText="Imported By" SortExpression="ImportedBy"
                        UniqueName="ImportedBy" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ImportedTime" HeaderText="Imported Date" SortExpression="ImportedDate"
                        DataFormatString="{0:MM/dd/yyyy}" UniqueName="ImportedDate" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ImportedTime" HeaderText="Imported Time" SortExpression="ImportedTime"
                        UniqueName="ImportedTime" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        DataFormatString="{0:HH:mm}" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </div>
</asp:Content>