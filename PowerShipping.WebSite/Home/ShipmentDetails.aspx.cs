﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.ImportExport;
using PowerShipping.WebSite.Controls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class ShipmentDetails : Page
    {
        private ShipmentDetailsPresenter presenter = new ShipmentDetailsPresenter();
        private ConsumerRequestsPresenter presenterCR = new ConsumerRequestsPresenter();
        UserManagementPresenter presenterAdv = new UserManagementPresenter();

        bool isExport = false;

        private bool _isFirstLoad = false;


        protected void Page_Load(object sender, EventArgs e)
        {
            ShipmentDetailFilter1.OnFilter += ShipmentDetailFilter1_OnFilter;
            ShipmentDetailFilter1.OnClear += ShipmentDetailFilter1_OnClear;

            if (!string.IsNullOrEmpty(Request.QueryString["FedExBatchId"]))
            {
                ShipmentDetailFilter1.FedExBatchId = new Guid(Request.QueryString["FedExBatchId"]);
            }
            
            if (!string.IsNullOrEmpty(Request.QueryString["AccountNum"]))
            {
                ShipmentDetailFilter1.AccountNumber = Request.QueryString["AccountNum"];
            }
            
            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Shipment Details");

                if (ShipmentDetailFilter1.FedExBatchId == null && string.IsNullOrEmpty(ShipmentDetailFilter1.AccountNumber)) 
                    _isFirstLoad = true;
            }

            if (!ShipmentDetailFilter1.FillView)
            {
                RadGrid_ShipmentDetails.Columns.FindByDataField("CompanyName").Visible = false;
                RadGrid_ShipmentDetails.Columns.FindByDataField("ShipmentDetailAddress1").Visible = false;
                RadGrid_ShipmentDetails.Columns.FindByDataField("ShipmentDetailAddress2").Visible = false;
                RadGrid_ShipmentDetails.Columns.FindByDataField("ShipmentDetailCity").Visible = false;
                RadGrid_ShipmentDetails.Columns.FindByDataField("ShipmentDetailState").Visible = false;
                RadGrid_ShipmentDetails.Columns.FindByDataField("ShipmentDetailZip").Visible = false;
                RadGrid_ShipmentDetails.Columns.FindByDataField("Email").Visible = false;
                RadGrid_ShipmentDetails.Columns.FindByDataField("Phone1").Visible = false;
                RadGrid_ShipmentDetails.Columns.FindByDataField("Phone2").Visible = false;
                RadGrid_ShipmentDetails.Columns.FindByDataField("ShippingStatus").Visible = false;
                RadGrid_ShipmentDetails.Columns.FindByDataField("ReshipStatus").Visible = false;
                RadGrid_ShipmentDetails.Columns.FindByDataField("ReasonCode").Visible = false;
               
                List<double> widths = new List<double>();

                if (ViewState["ColumnWidths"] != null)
                    widths = (List<double>)ViewState["ColumnWidths"];

                if (widths.Count > 0)
                {
                    for (int i = 0; i < RadGrid_ShipmentDetails.Columns.Count; i++)
                    {
                        RadGrid_ShipmentDetails.Columns[i].HeaderStyle.Width = (Unit)widths[i];
                    }
                }
                else
                {
                    widths.Clear();
                    foreach (GridColumn gc in RadGrid_ShipmentDetails.Columns)
                    {
                        widths.Add(gc.HeaderStyle.Width.Value);
                    }
                    ViewState["ColumnWidths"] = widths;
                }
            }
            else
            {
                RadGrid_ShipmentDetails.Columns.FindByDataField("CompanyName").Visible = true;
                RadGrid_ShipmentDetails.Columns.FindByDataField("ShipmentDetailAddress1").Visible = true;
                RadGrid_ShipmentDetails.Columns.FindByDataField("ShipmentDetailAddress2").Visible = true;
                RadGrid_ShipmentDetails.Columns.FindByDataField("ShipmentDetailCity").Visible = true;
                RadGrid_ShipmentDetails.Columns.FindByDataField("ShipmentDetailState").Visible = true;
                RadGrid_ShipmentDetails.Columns.FindByDataField("ShipmentDetailZip").Visible = true;
                RadGrid_ShipmentDetails.Columns.FindByDataField("Email").Visible = true;
                RadGrid_ShipmentDetails.Columns.FindByDataField("Phone1").Visible = true;
                RadGrid_ShipmentDetails.Columns.FindByDataField("Phone2").Visible = true;
                RadGrid_ShipmentDetails.Columns.FindByDataField("ShippingStatus").Visible = true;
                RadGrid_ShipmentDetails.Columns.FindByDataField("ReshipStatus").Visible = true;
                RadGrid_ShipmentDetails.Columns.FindByDataField("ReasonCode").Visible = true;

                foreach (GridColumn gc in RadGrid_ShipmentDetails.Columns)
                {
                    gc.HeaderStyle.Width = 100;
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["FedExBatchId"]) || !string.IsNullOrEmpty(Request.QueryString["AccountNum"]))
            {
                HiddenFiled_CustomQuery.Value = ShipmentDetailFilter1.CurrentQuery;
                RadGrid_ShipmentDetails_NeedDataSource(null, null);
                RadGrid_ShipmentDetails.Rebind();
            }
        }

        void ProjectsList1_OnProjectChanged(string projectId)
        {
            HiddenFiled_CustomQuery.Value = ShipmentDetailFilter1.CurrentQuery;
            RadGrid_ShipmentDetails_NeedDataSource(null, null);
            RadGrid_ShipmentDetails.Rebind();
        }

        private void ShipmentDetailFilter1_OnFilter(string qry)
        {
            HiddenFiled_CustomQuery.Value = qry;
            RadGrid_ShipmentDetails_NeedDataSource(null, null);
            RadGrid_ShipmentDetails.Rebind();
        }

        private void ShipmentDetailFilter1_OnClear(string qry)
        {
            HiddenFiled_CustomQuery.Value = qry;
        }

        protected void RadGrid_ShipmentDetails_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item is GridDataItem) && !e.Item.IsInEditMode)
            {
                GridDataItem gridItem = (GridDataItem)e.Item;
                try
                {
                    HyperLink lb = gridItem["FedExTrackingNumber"].Controls[0] as HyperLink;
                    if(lb != null)
                    {
                        lb.Attributes.Add("target", "_blank");
                        ShipmentDetail shipmentDetail = gridItem.DataItem as ShipmentDetail;
                        if(shipmentDetail != null)
                        {
                            lb.NavigateUrl = shipmentDetail.PackageTrackingUrl + shipmentDetail.FedExTrackingNumber;
                        }
                    }
                }
                catch
                {

                }
            }

            if (e.Item is GridDataItem && isExport)
            {
                TableCell cell = (e.Item as GridDataItem)["FedExTrackingNumber"];
                HyperLink hl = cell.Controls[0] as HyperLink;
                cell.Text = hl.Text;

                TableCell cella = (e.Item as GridDataItem)["ShipmentNumber"];
                LinkButton lba = cella.FindControl("EditButton") as LinkButton;
                cella.Text = lba.Text;
            }
        }

        protected void RadGrid_ShipmentDetails_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.EditCommandName)
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scroll", "setTimeout(function(){ Expand('" + item.ClientID + "');}, 100);", true);
                }
            }
            if (e.CommandName == RadGrid.ExportToCsvCommandName || e.CommandName == RadGrid.ExportToExcelCommandName
               || e.CommandName == RadGrid.ExportToWordCommandName || e.CommandName == RadGrid.ExportToPdfCommandName)
            {
                isExport = true;
            }
        }

        protected void RadGrid_ShipmentDetails_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                LinkButton addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Text = "Add New Shipment Record";

                addButton.Visible = Common.IsCanEdit();

                Button spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = Common.IsCanEdit();

                
            }
        }

        protected void RadGrid_ShipmentDetails_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            //if (!string.IsNullOrEmpty(HiddenFiled_CustomQuery.Value))
            //    RadGrid_ShipmentDetails.DataSource = presenter.GetFiltered(HiddenFiled_CustomQuery.Value);
            //else
            //    RadGrid_ShipmentDetails.DataSource = presenter.GetFiltered(ShipmentDetailFilter1.DefaultQuery);

            string userId = "";
            string projectId = "";

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = " AND ProjectId = '" + ProjectsList1.ProjectId + "'";
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = " AND UserId = '" + ProjectsList1.UserId + "'";
            }

            if (!_isFirstLoad)
            {
                if (!string.IsNullOrEmpty(HiddenFiled_CustomQuery.Value))
                {
                    string query = HiddenFiled_CustomQuery.Value;

                    query = GetQuery(userId, projectId, query);

                    RadGrid_ShipmentDetails.DataSource = presenter.GetFiltered(query);
                }
                //else
                //{
                //    string query = ShipmentDetailFilter1.DefaultQuery;

                //    query = GetQuery(userId, projectId, query);

                //    RadGrid_ShipmentDetails.DataSource = presenter.GetFiltered(query);
                //}
            }
        }

        private string GetQuery(string userId, string projectId, string query)
        {
            if (!string.IsNullOrEmpty(userId)) //customer (not admin or user)
            {
                query = query.Replace("[vw_ShipmentDetail]", " [vw_ShipmentDetail]  AS sd INNER JOIN dbo.UserProject up ON up.ProjectId = sd.ProjectId");

                if (!string.IsNullOrEmpty(projectId)) // selected project (not All projects)
                {
                    //query = query + " AND cr.ProjectId = '" + ProjectsList1.ProjectId + "'";
                    query = query.Replace("where 1 = 1", "where sd.ProjectId ='" + ProjectsList1.ProjectId + "' AND UserId = '" + ProjectsList1.UserId + "'");
                }

                //query = query + " AND UserId = '" + ProjectsList1.UserId + "'";
                query = query.Replace("where 1 = 1", "where UserId = '" + ProjectsList1.UserId + "'");
            }
            else
            {
                if (!string.IsNullOrEmpty(projectId)) // selected project (not All projects)
                {
                    //query = query + " AND ProjectId = '" + ProjectsList1.ProjectId + "'";
                    query = query.Replace("where 1 = 1", "where ProjectId ='" + ProjectsList1.ProjectId + "'");
                }
                else
                {
                    if (ProjectsList1.CompanyId != Guid.Empty)
                    {
                        List<Project> pList = presenterAdv.GetProjectByCompany(ProjectsList1.CompanyId);
                        if (pList.Count != 0)
                        {
                            string s = " WHERE ProjectId IN (";
                            foreach (Project p in pList)
                            {
                                s = s + "'" + p.Id + "',";
                            }
                            s = s.TrimEnd(',');
                            s = s + ")";

                            query = query.Replace("where 1 = 1", s);
                        }
                    }
                }
            }
            return query;
        }

        protected void RadGrid_ShipmentDetails_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        object o =
                            ((ShipmentEditInfo) e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                        var sDetail = (ShipmentDetail) o;

                        ShipmentDetail sd = presenter.GetById(sDetail.Id);

                        ConsumerRequest cr = presenterCR.GetById(sDetail.ConsumerRequestId);

                        sd.FedExTrackingNumber = sDetail.FedExTrackingNumber;

                        if (!sd.ShippingStatus.Equals(sDetail.ShippingStatus)) // updates when status changed
                            sd.ShippingStatusDate = DateTime.Now;
                        else
                            sd.ShippingStatusDate = sDetail.ShippingStatusDate;

                        sd.ShippingStatus = sDetail.ShippingStatus;
                        //PG31
                        if (sd.ShippingStatus == ShipmentShippingStatus.Delivered)
                        {
                            cr.Status = ConsumerRequestStatus.Delivered;
                            if (cr.ReceiptDate == null)
                            {
                                cr.ReceiptDate = DateTime.Now;
                            }

                        }
                        //PG31
                        sd.ReshipStatus = sDetail.ReshipStatus;
                        sd.FedExStatusCode = sDetail.FedExStatusCode;

                        sd.ShipDate = sDetail.ShipDate;
                        sd.IsReship = sDetail.IsReship;
                        sd.Notes = sDetail.Notes;
                        sd.ShipperId = sDetail.ShipperId;

                        sd.ReasonId = sDetail.ReasonId;
                        sd.ReturnDate = sDetail.ReturnDate;

                        sd.ShipmentDetailAddress1 = sDetail.ShipmentDetailAddress1;
                        if (!string.IsNullOrEmpty(sd.ShipmentDetailAddress1))
                            sd.ShipmentDetailAddress1 = sd.ShipmentDetailAddress1.ToUpperInvariant();

                        sd.ShipmentDetailAddress2 = sDetail.ShipmentDetailAddress2;
                        if (!string.IsNullOrEmpty(sd.ShipmentDetailAddress2))
                            sd.ShipmentDetailAddress2 = sd.ShipmentDetailAddress2.ToUpperInvariant();

                        sd.ShipmentDetailCity = sDetail.ShipmentDetailCity;
                        if (!string.IsNullOrEmpty(sd.ShipmentDetailCity))
                            sd.ShipmentDetailCity = sd.ShipmentDetailCity.ToUpperInvariant();

                        sd.ShipmentDetailState = sDetail.ShipmentDetailState;
                        if (!string.IsNullOrEmpty(sd.ShipmentDetailState))
                            sd.ShipmentDetailState = sd.ShipmentDetailState.ToUpperInvariant();

                        sd.ShipmentDetailZip = sDetail.ShipmentDetailZip;

                        cr.AccountName = sDetail.AccountName;
                        if (!string.IsNullOrEmpty(cr.AccountName))
                            cr.AccountName = cr.AccountName.ToUpperInvariant();

                        //cr.Address1 = sDetail.Address1;
                        //if (!string.IsNullOrEmpty(cr.Address1))
                        //    cr.Address1 = cr.Address1.ToUpperInvariant();

                        //cr.Address2 = sDetail.Address2;
                        //if (!string.IsNullOrEmpty(cr.Address2))
                        //    cr.Address2 = cr.Address2.ToUpperInvariant();

                        //cr.City = sDetail.City;
                        //if (!string.IsNullOrEmpty(cr.City))
                        //    cr.City = cr.City.ToUpperInvariant();

                        //cr.State = sDetail.State;
                        //if (!string.IsNullOrEmpty(cr.State))
                        //    cr.State = cr.State.ToUpperInvariant();

                        //cr.ZipCode = sDetail.ZipCode;




                        cr.Email = sd.Email;

                        cr.Phone1 = EnumUtils.GetPhoneDigits(sDetail.Phone1);
                        cr.Phone2 = EnumUtils.GetPhoneDigits(sDetail.Phone2);

                        

                        //for validation
                        cr.States = presenterAdv.GetStatesByProject(cr.ProjectId);

                        cr.CompanyName = sDetail.CompanyName;


                        Rules.RulesForReship(sd, cr);
                        Rules.RulesForShipmentStatusToReturn(sd, cr);


                        ValidationResult validationResult = EntityValidation.Validate(cr);
                        if (validationResult.Success)
                        {
                            cr.ChangedBy = Membership.GetUser().UserName;

                            if (!sd.PurchaseOrderNumber.Equals(sDetail.PurchaseOrderNumber))
                                presenter.UpdatePurchaseOrderNumber(sd.ShipmentId, sDetail.PurchaseOrderNumber);

                            presenter.Save(sd, cr);
                            RadGrid_ShipmentDetails.Rebind();
                        }
                        else
                        {
                            var l = new Label();
                            l.Text = validationResult.Errors.ToString();
                            l.ControlStyle.ForeColor = Color.Red;
                            RadGrid_ShipmentDetails.Controls.Add(l);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to update Shipment. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                RadGrid_ShipmentDetails.Controls.Add(l);
            }
        }

        protected void RadGrid_ShipmentDetails_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                //if (e.CommandName == RadGrid.DeleteCommandName)
                //{
                //    var id =
                //        new Guid(
                //            (e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"].ToString());
                //    presenter.Remove(id);
                //    RadGrid_ShipmentDetails.Rebind();
                //}
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to update Shipment. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                RadGrid_ShipmentDetails.Controls.Add(l);
            }
        }

        protected void RadGrid_ShipmentDetails_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.PerformInsertCommandName)
                {
                    //if (ProjectsList1.ProjectId == Guid.Empty)
                    //{
                    //    var l = new Label();
                    //    l.Text = "Please select project.";
                    //    l.ControlStyle.ForeColor = Color.Red;
                    //    RadGrid_ShipmentDetails.Controls.Add(l);
                    //    return;
                    //}

                    var consumerPresenter = new ConsumerRequestsPresenter();
                    var cr = new ConsumerRequest();
                    object o =
                        ((ShipmentEditInfo) e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                    var sd = (ShipmentDetail) o;


                    if (!string.IsNullOrEmpty(sd.AccountNumber))
                    {
                        cr = consumerPresenter.GetByAccountNumber(sd.AccountNumber, ProjectsList1.ProjectId);
                        if (cr != null)
                        {
                            sd.ConsumerRequestId = cr.Id;
                            sd.IsReship = cr.IsReship;
                            sd.ProjectId = cr.ProjectId;

                            if (!presenter.IsShipmentNumberAlreadyExists(sd.ShipmentNumber))
                            {
                                cr.Status = ConsumerRequestStatus.PendingShipment;

                                presenter.Save(sd);
                                presenterCR.Save(cr);

                                RadGrid_ShipmentDetails.Rebind();
                            }
                            else
                            {
                                var l = new Label();
                                l.Text = "Unable to update Shipment. Reason: " +
                                         "Shipment Number already exists";
                                l.ControlStyle.ForeColor = Color.Red;
                                RadGrid_ShipmentDetails.Controls.Add(l);
                            }
                        }
                        else
                        {
                            if(ProjectsList1.ProjectId == Guid.Empty)
                            {
                                var l = new Label();
                                l.Text = "Unable to update Shipment. Reason: " +
                                         "Please select project.";
                                l.ControlStyle.ForeColor = Color.Red;
                                RadGrid_ShipmentDetails.Controls.Add(l);
                            }
                            else
                            {
                                var l = new Label();
                                l.Text = "Unable to update Shipment. Reason: " +
                                         "Customer Request with this AccountId doesnt exists";
                                l.ControlStyle.ForeColor = Color.Red;
                                RadGrid_ShipmentDetails.Controls.Add(l);
                            }
                        }
                    }
                    else
                    {
                        var l = new Label();
                        l.Text = "Unable to update Shipment. Reason: " + " Account Number shouldn't be empty";
                        l.ControlStyle.ForeColor = Color.Red;
                        RadGrid_ShipmentDetails.Controls.Add(l);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to update Shipment. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                RadGrid_ShipmentDetails.Controls.Add(l);
            }
        }

		protected void btnBulkExport_OnClick(object sender, EventArgs e)
		{
			string userId = "";
			string projectId = "";
			List<ShipmentDetail> source = new List<ShipmentDetail>();

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = " AND ProjectId = '" + ProjectsList1.ProjectId + "'";
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = " AND UserId = '" + ProjectsList1.UserId + "'";
            }

			if (!_isFirstLoad)
			{
				if (!string.IsNullOrEmpty(HiddenFiled_CustomQuery.Value))
				{
					string query = HiddenFiled_CustomQuery.Value;
					query = GetQuery(userId, projectId, query);

					source = presenter.GetFiltered(query);
				}
			}

			if (source.Count > 0)
			{
				ProduceCSV(source);
			}
		}

		private void ProduceCSV(List<ShipmentDetail> source)
		{
			string[] values = new string[41];

			#region Make the Header
			values[0] = "ShipmentNumber";
			values[1] = "AccountNumber";
			values[2] = "PurchaseOrderNumber";
			values[3] = "ShipperCode";
			values[4] = "FedExTrackingNumber";
			values[5] = "FedExStatusCode";
			values[6] = "FedExLastUpdateStatus";
			values[7] = "AccountName";
			values[8] = "CompanyName";
			values[9] = "ShipmentDetailAddress1";
			values[10] = "ShipmentDetailAddress2";
			values[11] = "ShipmentDetailCity";
			values[12] = "ShipmentDetailState";
			values[13] = "ShipmentDetailZip";
			values[14] = "Email";
			values[15] = "Phone1";
			values[16] = "Phone2";
			values[17] = "KitTypeName";
			values[18] = "ShippingStatusSort";
			values[19] = "ShippingStatus";
			values[20] = "ReshipStatusSort";
			values[21] = "ReshipStatus";
			values[22] = "ShippingStatusDate";
			values[23] = "ShipDate";
			values[24] = "ReturnDate";
			values[25] = "IsReship";
			values[26] = "ReasonCode";
			values[27] = "CompanyCode";
			values[28] = "OperatingCompany";
			values[29] = "ProjectCode";
			#endregion

			Response.ClearContent();
			Response.Clear();
			Response.ContentType = "application/CSV";
			Response.AddHeader("Content-Disposition", "attachment;filename=ShipmentDetails.csv");
			Response.Write(string.Join(",", values));
			Response.Write(Environment.NewLine);

			if (source != null)
			{
				foreach (var item in source)
				{
					values = new string[41];
					foreach (PropertyInfo prop in item.GetType().GetProperties())
					{
						var propValue = prop.GetValue(item, null);

						#region Make items
						switch (prop.Name)
						{
							case "ShipmentNumber":
								values[0] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "AccountNumber":
								values[1] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "PurchaseOrderNumber":
								values[2] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "ShipperCode":
								values[3] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "FedExTrackingNumber":
								values[4] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "FedExStatusCode":
								values[5] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "FedExLastUpdateStatus":
								values[6] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "AccountName":
								values[7] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "CompanyName":
								values[8] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "ShipmentDetailAddress1":
								values[9] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "ShipmentDetailAddress2":
								values[10] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "ShipmentDetailCity":
								values[11] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "ShipmentDetailState":
								values[12] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "ShipmentDetailZip":
								values[13] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "Email":
								values[14] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "Phone1":
								values[15] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "Phone2":
								values[16] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "KitTypeName":
								values[17] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "ShippingStatusSort":
								values[18] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "ShippingStatus":
								values[19] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "ReshipStatusSort":
								values[20] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "ReshipStatus":
								values[21] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "ShippingStatusDate":
								values[22] = propValue != null ? "\"" + Convert.ToDateTime(propValue).ToString("MM/dd/yyyy") + "\"" : string.Empty;
								break;
							case "ShipDate":
								values[23] = propValue != null ? "\"" + Convert.ToDateTime(propValue).ToString("MM/dd/yyyy") + "\"" : string.Empty;
								break;
							case "ReturnDate":
								values[24] = propValue != null ? "\"" + Convert.ToDateTime(propValue).ToString("MM/dd/yyyy") + "\"" : string.Empty;
								break;
							case "IsReship":
								values[25] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "ReasonCode":
								values[26] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "CompanyCode":
								values[27] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "OperatingCompany":
								values[28] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "ProjectCode":
								values[29] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
						}
						#endregion
					}
					Response.Write(string.Join(",", values));
					Response.Write(Environment.NewLine);
				}
			}

			Response.Flush();
			Response.End();
		}
    }
}