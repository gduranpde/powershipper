﻿<%@ Page Title="Label Request Send" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" CodeBehind="LabelRequestSend.aspx.cs" Inherits="PowerShipping.WebSite.Home.LabelRequestSend" Theme="Default" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register src="../Controls/ProjectsList.ascx" tagname="ProjectsList" tagprefix="uc1" %>

<%@ Register src="~/Controls/ShipmentDetailsInfo.ascx" tagname="ShipmentDetails" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="pageDiv">
    <h3>Label Request Send</h3>
    
     <uc1:ProjectsList ID="ProjectsList1" runat="server" Visible="false"/>
     
    <asp:Wizard runat="server" id="FedExWizard" DisplaySideBar="false"
        onnextbuttonclick="FedExWizard_NextButtonClick" 
        onfinishbuttonclick="FedExWizard_FinishButtonClick">
    <wizardsteps>
      <asp:wizardstep runat="server" steptype="auto" title="Default information">
        <table>
            <tr>
                <td>
                    <asp:Label ID="lbEmail" runat="server" Text="Enter email for resend: " ForeColor="Red" Width="200px"></asp:Label> 
                </tb>
                <tb>
                   <asp:TextBox ID="tbEmailForSend" runat="server" Width="200px"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmail" runat="server"
                        ErrorMessage="Wrong primary email" Text="*" ControlToValidate="tbEmailForSend"
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                    </asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmail" runat="server" ControlToValidate="tbEmailForSend">*</asp:RequiredFieldValidator> 
                </tb>
            </tr>
            <tr>                
                <td colspan="2">
                 </tb>
            </tr>
             <tr>
                <td colspan="2">
                     <uc3:ShipmentDetails ID="ShipmentDetails_Details" runat="server" Visible="false"/>
                </tb>
            </tr>
        </table>           
      </asp:wizardstep>
       <asp:wizardstep runat="server" steptype="auto" title="Send">
            <asp:Label ID="lbError" runat="server"></asp:Label>
      </asp:wizardstep>
       
      <asp:wizardstep runat="server" steptype="complete" Title="Result">
        
      </asp:wizardstep>
   </wizardsteps>
</asp:Wizard>   
    </div>
    
</asp:Content>
