﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.ImportExport;
using PowerShipping.WebSite.Controls;
using Telerik.Web.UI;
using System.Web.UI;

namespace PowerShipping.WebSite
{
    public partial class OptOutsExceptions : System.Web.UI.Page
    {
        OptOutExceptionPresenter presenter = new OptOutExceptionPresenter();
        OptOutsPresenter presenterOo = new OptOutsPresenter();
        ConsumerRequestsPresenter presenterCr = new ConsumerRequestsPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "OptOut Exceptions");
            }
        }

        void ProjectsList1_OnProjectChanged(string projectId)
        {
            RadGrid_OptOutsEx.Rebind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            lbReuslt.Text = "";
            RadGrid_OptOutsEx.Rebind();
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            tbAccountNumberFilter.Text = null;
            tbAccountNameFilter.Text = null;
            tbAddressFilter.Text = null;
            tbPhoneFilter.Text = null;
        }

        protected void btnValidate_Click(object sender, EventArgs e)
        {
            lbReuslt.Text = "";
            lbReuslt.ForeColor = Color.Black;

            if (ProjectsList1.ProjectId == Guid.Empty)
            {
                lbReuslt.ForeColor = Color.Red;
                lbReuslt.Text = "Please select project.";
                return;
            }

            List<OptOutException> list = GetFilteredList();
            int validated = 0;

            foreach (var exception in list)
            {
                OptOut oo = new OptOut();
                var result = EntityValidation.MapAndValidate(exception, oo, true);

                if (result.Success)
                {
                    exception.Status = OptOutExceptionStatus.Deleted;

                    oo.Id = Guid.NewGuid(); 
                    presenterOo.Save(oo);

                    ConsumerRequest cr = presenterCr.GetByAccountNumber(oo.AccountNumber, ProjectsList1.ProjectId);
                    if (cr != null)
                    {
                        cr.Status = ConsumerRequestStatus.OptOut;
                        presenterCr.Save(cr);
                    }
                    validated++;
                }
                presenter.Save(exception);
            }

            lbReuslt.Text = "Total exceptions process: " + list.Count + ". ReSubmited: " + validated + ".";

            RadGrid_OptOutsEx.Rebind();
        }

        protected void RadGrid_OptOutsEx_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.EditCommandName)
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scroll", "setTimeout(function(){ Expand('" + item.ClientID + "');}, 100);", true);
                }
            }
        }

        protected void RadGrid_OptOutsEx_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                Button spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false; 

                LinkButton addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false; 
            }
        }

        protected void RadGrid_OptOutsEx_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                string reason = item["Reason"].Text;
                reason = reason.Replace("|", "<br/>");
                item["Reason"].Text = reason;
            }

            if ((e.Item is GridDataItem) && !e.Item.IsInEditMode)
            {
                GridDataItem gridItem = (GridDataItem)e.Item;
                try
                {
                    Double myDouble = Convert.ToDouble(gridItem["Phone1"].Text);
                    gridItem["Phone1"].Text = myDouble.ToString("###-###-####");

                    Double myDouble2 = Convert.ToDouble(gridItem["Phone2"].Text);
                    gridItem["Phone2"].Text = myDouble2.ToString("###-###-####");

                }
                catch
                {

                }
            }
            //if ((e.Item is GridEditFormItem) && e.Item.IsInEditMode)
            //{
            //    GridEditFormItem gridEditFormItem = (GridEditFormItem)e.Item;
            //    DropDownList dropDownList = (DropDownList)gridEditFormItem["Status"].FindControl("ddlStatus");
            //    dropDownList.DataSource = Utils.EnumToListItems(typeof(OptOutExceptionStatus));
            //    dropDownList.DataBind();

            //    dropDownList.SelectedValue = Convert.ToInt32(((OptOutException)gridEditFormItem.DataItem).Status).ToString();
            //}
        }

        protected void RadGrid_OptOutsEx_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_OptOutsEx.DataSource = GetFilteredList();
        }
        
        protected void RadGrid_OptOutsEx_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        GridEditFormItem item = (GridEditFormItem)e.Item;
                        Guid id = new Guid(item.GetDataKeyValue("Id").ToString());
                        if (id != Guid.Empty)
                        {
                            if (e.Item is GridEditableItem)
                            {
                                object o = ((OptOutExceptionCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                                OptOutException ooExNew = (OptOutException)o;

                                OptOutException ooEx = presenter.GetById(ooExNew.Id);

                                ooEx.AccountName = ooExNew.AccountName;
                                ooEx.AccountNumber = ooExNew.AccountNumber;
                                ooEx.Address = ooExNew.Address;
                                ooEx.City = ooExNew.City;
                                ooEx.Email = ooExNew.Email;
                                ooEx.IsEnergyProgram = ooExNew.IsEnergyProgram;
                                ooEx.IsTotalDesignation = ooExNew.IsTotalDesignation;
                                ooEx.Notes = ooExNew.Notes;
                                ooEx.OptOutDate = ooExNew.OptOutDate;
                                ooEx.Phone1 = ooExNew.Phone1;
                                ooEx.Phone2 = ooExNew.Phone2;
                                ooEx.State = ooExNew.State;
                                ooEx.ZipCode = ooExNew.ZipCode;

                                //if (ProjectsList1.ProjectId == Guid.Empty)
                                //{
                                //    Label l = new Label();
                                //    l.Text = "Please select project in list.";
                                //    l.ControlStyle.ForeColor = Color.Red;
                                //    RadGrid_OptOutsEx.Controls.Add(l);

                                //    return;
                                //}

                                //ooEx.ProjectId = ProjectsList1.ProjectId;

                                ooEx.ProjectId = ooExNew.ProjectId;

                                EntityValidation.Validate(ooEx, true);//updates reason

                                presenter.Save(ooEx);
                                RadGrid_OptOutsEx.Rebind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                Label l = new Label();
                l.Text = "Unable to update Opt Out Exception. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_OptOutsEx.Controls.Add(l);
            }
        }

        protected void RadGrid_OptOutsEx_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.DeleteCommandName)
                {
                    //Guid id = new Guid((e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"].ToString());
                    //OptOutException ooEx = presenter.GetById(id);
                    //ooEx.Status = OptOutExceptionStatus.Deleted;

                    //presenter.Save(ooEx);
                    //RadGrid_OptOutsEx.Rebind();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                Label l = new Label();
                l.Text = "Unable to update Opt Out Exception. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_OptOutsEx.Controls.Add(l);
            }
        }

        private List<OptOutException> GetFilteredList()
        {
            string accNum = tbAccountNumberFilter.Text;
            string accName = tbAccountNameFilter.Text;
            string phone = tbPhoneFilter.Text;
            string address = tbAddressFilter.Text;

            if (accNum == string.Empty)
                accNum = null;
            if (accName == string.Empty)
                accName = null;
            if (phone == string.Empty)
                phone = null;
            if (address == string.Empty)
                address = null;

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList1.ProjectId;
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = ProjectsList1.UserId;
            }

            if (ProjectsList1.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList1.CompanyId;
            }

            return presenter.GetAll(accNum, accName, phone, address, userId, projectId, companyId);
        }
    }
}
