﻿<%@ Page Title="FedEx Label Request" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" CodeBehind="FedExRequest.aspx.cs" Inherits="PowerShipping.WebSite.FedExRequest" Theme="Default" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register src="../Controls/ProjectsList.ascx" tagname="ProjectsList" tagprefix="uc1" %>

<%@ Register src="~/Controls/ConsumerRequestList.ascx" tagname="ConsumerRequestList" tagprefix="uc1" %>

<%@ Register src="~/Controls/ShipmentCommon.ascx" tagname="ShipmentCommon" tagprefix="uc2" %>

<%@ Register src="~/Controls/ShipmentDetailsInfo.ascx" tagname="ShipmentDetails" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="pageDiv">
    
     <table>
            <tr>
            <td>              
               <%-- <h3>FedEx Label Request</h3>--%>
            </td>           
            <td align="center">
                 <uc1:ProjectsList ID="ProjectsList1" runat="server" Visible="false"/>
            </td>
        </tr>
        </table>
   
<asp:HiddenField  ID="HiddenField_Id" runat="server" />    
<asp:MultiView id="WizardMultiView" ActiveViewIndex="0" runat="Server">
                <asp:View id="Page1" runat="Server">  
                    
                    <table>
                        <%--<tr>
                            <td align="right">
                                <asp:Button id="Page1Next" Text = "Next" OnClick="NextButton_Command"  CssClass="ptButton"                     
                                    runat= "Server">
                                </asp:Button>  
                            </td>
                        </tr>--%>
                        <tr>
                            <td>
                                <uc1:ConsumerRequestList ID="ConsumerRequestList_Requests" runat="server" />
                            </td>
                        </tr>
                    </table> 
                </asp:View>

                <asp:View id="Page2" runat="Server">
                
                    <uc2:ShipmentCommon ID="ShipmentCommon_Common" runat="server" />
                
                    <asp:Button id="Page2Back"
                        Text = "Previous"
                        OnClick="BackButton_Command"                       
                        runat= "Server" CssClass="ptButton">
                    </asp:Button> 

                    <asp:Button id="Page2Next"
                        Text = "Next"
                        OnClick="NextButton_Command"                       
                        runat="Server" CssClass="ptButton">
                    </asp:Button> 

                </asp:View>               

                <asp:View id="Page4" runat="Server">
                    <asp:Button id="btnViewFile"
                        Text = "View"
                        OnClick="btnViewFile_OnClick"                       
                        runat="Server" CssClass="ptButton">
                    </asp:Button> 
                    <asp:Button id="Page4Save"
                        Text = "Send"
                        OnClick="NextButton_Command"                       
                        runat="Server" CssClass="ptButton">
                    </asp:Button>
                    <asp:Button id="Page4Restart"
                        Text = "Undo"
                        OnClick="BackButton_Command"                       
                        runat= "Server" CssClass="ptButton">
                    </asp:Button>  
                </asp:View>  

            </asp:MultiView>

    
    
    <asp:Literal ID="Label_MessageLabel" runat="server"></asp:Literal>
    
    <br />
    <asp:Label ID="lbError" runat="server"></asp:Label>
   
</div>
    
    
    
</asp:Content>