﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReshipImport.aspx.cs" MasterPageFile="~/Shared/Default.Master"
    Inherits="PowerShipping.WebSite.Home.ReshipImport" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadProgressManager ID="Radprogressmanager1" runat="server" />
    <div class="pageDiv">
        <table>
            <tr>
                <td>
                    <h3 style="width: 223px;">
                        Reship Import</h3>
                </td>
                <td>
                    Select Shipper:
                    <telerik:RadComboBox ID="RadComboBox_Shipper" runat="server" DataValueField="Id"
                        DataTextField="ShipperName" Width="80px">
                    </telerik:RadComboBox>
                </td>
                <td>
                    Ship Date:
                    <telerik:RadDatePicker ID="RadDatePicker_ShipperDateEndDate" runat="server" Skin="Telerik"
                        Width="100px">
                        <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                            ViewSelectorText="x">
                        </Calendar>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                        <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                        </DateInput>
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadUpload ID="RadUpload1" runat="server" Width="127px" ControlObjectsVisibility="None" />
                    <%--OnClick="clearValidatorText()"--%>
                </td>
            </tr>
        </table>
        <div class="submitArea">
            <asp:Button runat="server" ID="SubmitButton" Text="Upload files" OnClick="SubmitButton_Click" />
            <br />
            <asp:Label ID="LblMessage" runat="server"></asp:Label>
        </div>
        <telerik:RadProgressArea runat="server" ID="ProgressArea1">
            <Localization Uploaded="File upload progress: " UploadedFiles="Imported records: "
                CurrentFileName="" TotalFiles="Total records: " EstimatedTime="" TransferSpeed="" />
        </telerik:RadProgressArea>
        <asp:Label ID="LblErrors" runat="server"></asp:Label>
    </div>
</asp:Content>