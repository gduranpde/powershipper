﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.ImportExport;
using PowerShipping.WebSite.Controls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite
{
    public partial class OptOuts : Page
    {
        OptOutsPresenter presenter = new OptOutsPresenter();
        ConsumerRequestsPresenter presenterCR = new ConsumerRequestsPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Opt Outs");
            }
        }

        void ProjectsList1_OnProjectChanged(string projectId)
        {
            RadGrid_OptOuts.Rebind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            RadGrid_OptOuts.Rebind();
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            tbAccountNumberFilter.Text = null;
            tbAccountNameFilter.Text = null;
            tbAddressFilter.Text = null;
            tbPhoneFilter.Text = null;
            dpStartDateFilter.SelectedDate = null;
            dpEndDateFilter.SelectedDate = null;
        }

        protected void RadGrid_OptOuts_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.EditCommandName)
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scroll", "setTimeout(function(){ Expand('" + item.ClientID + "');}, 100);", true);
                }
            }
        }

        protected void RadGrid_OptOuts_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                Button spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = Common.IsCanEdit();

                LinkButton addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = Common.IsCanEdit();
            }
        }

        protected void RadGrid_OptOuts_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_OptOuts.DataSource = GetFilteredList();
        }

        protected void RadGrid_OptOuts_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item is GridDataItem) && !e.Item.IsInEditMode)
            {
                GridDataItem gridItem = (GridDataItem)e.Item;
                try
                {
                    Double myDouble = Convert.ToDouble(gridItem["Phone1"].Text);
                    gridItem["Phone1"].Text = myDouble.ToString("###-###-####");
                }
                catch
                {

                }

                string strtxt = gridItem["Address"].Text;
                if (gridItem["Address"].Text.Length > 35)
                {
                    string temp = gridItem["Address"].Text;
                    temp = temp.Remove(35);
                    temp += "...";
                    gridItem["Address"].Text = temp;
                    gridItem["Address"].ToolTip = strtxt;
                }
            }

            if ((e.Item is GridEditFormItem) && e.Item.IsInEditMode)
            {
                
                //GridEditFormItem gridEditFormItem = (GridEditFormItem)e.Item;
                //if (gridEditFormItem.ItemIndex > 0)
                //{
                //    LinkButton updateButton = (LinkButton)gridEditFormItem.FindControl("UpdateButton");
                //    System.Web.UI.HtmlControls.HtmlAnchor deleteButton = new System.Web.UI.HtmlControls.HtmlAnchor();
                //    deleteButton.InnerText = "Delete";
                //    deleteButton.Style.Add("padding-left", "5px");
                //    deleteButton.Attributes.Add("class", updateButton.CssClass);
                //    deleteButton.HRef = "javascript:__doPostBack('ctl00$ContentPlaceHolder1$LinkButton_DeleteOptOut','')";
                //    this.LinkButton_DeleteOptOut.CommandArgument = gridEditFormItem.GetDataKeyValue("Id").ToString();
                //    updateButton.Parent.Controls.Add(deleteButton);
                //}



                //RadDatePicker datePicker = (RadDatePicker)gridEditFormItem["OptOutDate"].FindControl("datePickerEdit");
                //if (gridEditFormItem.DataItem is GridInsertionObject)
                //{
                //    datePicker.SelectedDate = DateTime.Now;
                //}
                //else
                //{
                //    datePicker.SelectedDate = ((OptOut)gridEditFormItem.DataItem).OptOutDate;
                //}

            }
        }


        protected void RadGrid_OptOuts_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                //if (ProjectsList1.ProjectId == Guid.Empty)
                //{
                //    var l = new Label();
                //    l.Text = "Please select project.";
                //    l.ControlStyle.ForeColor = Color.Red;
                //    RadGrid_OptOuts.Controls.Add(l);
                //    return;
                //}

                OptOut oo = new OptOut();

                oo.Id = Guid.NewGuid();

                object o = ((OptOutCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                OptOut ooNew = (OptOut)o;

                oo.AccountName = ooNew.AccountName;
                oo.AccountNumber = ooNew.AccountNumber;
                oo.Address = ooNew.Address;
                oo.City = ooNew.City;
                oo.Email = ooNew.Email;
                oo.IsEnergyProgram = ooNew.IsEnergyProgram;
                oo.IsTotalDesignation = ooNew.IsTotalDesignation;
                oo.Notes = ooNew.Notes;
                oo.OptOutDate = ooNew.OptOutDate;
                //oo.OptOutNumber = ooNew.OptOutNumber;
                oo.Phone1 = ooNew.Phone1;
                oo.Phone2 = ooNew.Phone2;
                oo.State = ooNew.State;
                oo.ZipCode = ooNew.ZipCode;

                //if (ProjectsList1.ProjectId == Guid.Empty)
                //{
                //    Label l = new Label();
                //    l.Text = "Please select project in list.";
                //    l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                //    RadGrid_OptOuts.Controls.Add(l);

                //    return;
                //}

                //oo.ProjectId = ProjectsList1.ProjectId;

                oo.ProjectId = ooNew.ProjectId;

                if(oo.ProjectId == Guid.Empty)
                {
                    //Label l = new Label();
                    //l.Text = "Project must be selected.";
                    //l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                    //RadGrid_OptOuts.Controls.Add(l);
                    ShowErrorPopUp("Project must be selected.");
                    return;
                }

              
                var validationResult = EntityValidation.Validate(oo);
                if (validationResult.Success)
                {
                    presenter.Add(oo);

                    var existingRequest = presenterCR.GetByAccountNumber(oo.AccountNumber, ProjectsList1.ProjectId);
                    if (existingRequest != null)
                    {
                        existingRequest.Status = ConsumerRequestStatus.OptOut;
                        presenterCR.Save(existingRequest);
                    }

                    RadGrid_OptOuts.Rebind();
                }
                else
                {
                    //Label l = new Label();
                    //l.Text = validationResult.Errors.ToString();
                    //l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                    //RadGrid_OptOuts.Controls.Add(l);
                    ShowErrorPopUp(validationResult.Errors.ToString());
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                //Label l = new Label();
                //l.Text = "Unable to update OptOut. Reason: " + ex.Message;
                //l.ControlStyle.ForeColor = Color.Red;
                //RadGrid_OptOuts.Controls.Add(l);

                ShowErrorPopUp("Unable to update OptOut. Reason: " + ex.Message);
            }
        }

        protected void RadGrid_OptOuts_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        GridEditFormItem item = (GridEditFormItem)e.Item;
                        Guid id = new Guid(item.GetDataKeyValue("Id").ToString());
                        if (id != Guid.Empty)
                        {
                            if (e.Item is GridEditableItem)
                            {
                                object o = ((OptOutCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                                OptOut ooNew = (OptOut)o;

                                OptOut oo = presenter.GetById(ooNew.Id);

                                oo.AccountName = ooNew.AccountName;
                                oo.AccountNumber = ooNew.AccountNumber;
                                oo.Address = ooNew.Address;
                                oo.City = ooNew.City;
                                oo.Email = ooNew.Email;
                                oo.IsEnergyProgram = ooNew.IsEnergyProgram;
                                oo.IsTotalDesignation = ooNew.IsTotalDesignation;
                                oo.Notes = ooNew.Notes;
                                oo.OptOutDate = ooNew.OptOutDate;
                                //oo.OptOutNumber = ooNew.OptOutNumber;
                                oo.Phone1 = ooNew.Phone1;
                                oo.Phone2 = ooNew.Phone2;
                                oo.State = ooNew.State;
                                oo.ZipCode = ooNew.ZipCode;

                                //if (ProjectsList1.ProjectId == Guid.Empty)
                                //{
                                //    Label l = new Label();
                                //    l.Text = "Please select project in list.";
                                //    l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                                //    RadGrid_OptOuts.Controls.Add(l);

                                //    return;
                                //}

                                //oo.ProjectId = ProjectsList1.ProjectId;

                                oo.ProjectId = ooNew.ProjectId;

                                
                                var validationResult = EntityValidation.Validate(oo);
                                if (validationResult.Success)
                                {
                                    presenter.Add(oo);
                                    RadGrid_OptOuts.Rebind();
                                }
                                else
                                {
                                    //Label l = new Label();
                                    //l.Text = validationResult.Errors.ToString();
                                    //l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                                    //RadGrid_OptOuts.Controls.Add(l);
                                    ShowErrorPopUp(validationResult.Errors.ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                //Label l = new Label();
                //l.Text = "Unable to update OptOut. Reason: " + ex.Message;
                //l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                //RadGrid_OptOuts.Controls.Add(l);
                ShowErrorPopUp("Unable to update OptOut. Reason: " + ex.Message);
            }
        }

        protected void RadGrid_OptOuts_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.DeleteCommandName)
                {
                    //Guid id = new Guid((e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"].ToString());
                    //ConsumerRequest cr = presenter.GetById(id);
                    //cr.Status = ConsumerRequestStatus.Deleted;

                    //presenter.Save(cr);
                    RadGrid_OptOuts.Rebind();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                Label l = new Label();
                l.Text = "Unable to update OptOut. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_OptOuts.Controls.Add(l);
            }
        }

        private List<OptOut> GetFilteredList()
        {
            string accNum = tbAccountNumberFilter.Text;
            string accName = tbAccountNameFilter.Text;
            string phone = tbPhoneFilter.Text;
            string address = tbAddressFilter.Text;

            DateTime? startDate = dpStartDateFilter.SelectedDate;
            DateTime? endDate = dpEndDateFilter.SelectedDate;

            if (accNum == string.Empty)
                accNum = null;
            if (accName == string.Empty)
                accName = null;
            if (phone == string.Empty)
                phone = null;
            if (address == string.Empty)
                address = null;


            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList1.ProjectId;
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = ProjectsList1.UserId;
            }

            if (ProjectsList1.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList1.CompanyId;
            }

            return presenter.GetAll(startDate, endDate, accNum, accName, phone, address, userId, projectId, companyId);
        }

        //protected void LinkButton_DeleteOptOut_Click(object sender, EventArgs e)
        //{
        //    LinkButton lb = (LinkButton)LinkButton_DeleteOptOut;
        //    foreach (GridItem item in RadGrid_OptOuts.EditItems)
        //        item.Edit = false;

        //    presenter.RemoveOptOut(new Guid(lb.CommandArgument));
        //    RadGrid_OptOuts.Rebind();
        //}

        private void ShowErrorPopUp(string err)
        {
            lbErrPopUp.Text = err;
            mpeError.Show();
        }
    }
}
