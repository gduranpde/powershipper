﻿using System;
using System.Configuration;
using System.Drawing;
using System.Net.Mail;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.ImportExport;
using PowerShipping.WebSite.Controls;

namespace PowerShipping.WebSite.Home
{
    public partial class LabelRequestSend : Page
    {
        private readonly ConsumerRequestsPresenter presenter = new ConsumerRequestsPresenter();
        ShipmentsPresenter presenterShipments = new ShipmentsPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList1.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }

            if (string.IsNullOrEmpty(Request.QueryString["Id"]))
                Response.Redirect("Shipments.aspx");

            if (!IsPostBack)
            {
                ((ShipmentDetailsInfo) FedExWizard.WizardSteps[0].FindControl("ShipmentDetails_Details")).InitData();
            }

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Label Request Send");
            }
        }

        protected void FedExWizard_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {
            ((Label) FedExWizard.WizardSteps[1].FindControl("lbError")).Text = "";

            presenter.SaveFedExShippingDefault(
                ((ShipmentDetailsInfo) FedExWizard.WizardSteps[0].FindControl("ShipmentDetails_Details")).GetEntity());

            Shipment shipm = presenterShipments.GetById(new Guid(Request.QueryString["Id"]));

            ExportResult exportResult = presenter.GenerateExportDate(new Guid(Request.QueryString["Id"]));

            string sendError = string.Empty;
            if (exportResult.ExportErrors == null)
                sendError = SendEmail(exportResult.OutputFilePath, shipm.PurchaseOrderNumber);

            if (exportResult.ExportErrors != null || !string.IsNullOrEmpty(sendError))
            {
                ((Label) FedExWizard.WizardSteps[1].FindControl("lbError")).ControlStyle.ForeColor = Color.Red;
                ((Label) FedExWizard.WizardSteps[1].FindControl("lbError")).Text =
                    exportResult.ExportErrors + sendError;
            }
            else
            {
                ((Label) FedExWizard.WizardSteps[1].FindControl("lbError")).ControlStyle.ForeColor = Color.Green;
                ((Label) FedExWizard.WizardSteps[1].FindControl("lbError")).Text = "Mail sent.";
            }
        }

        protected void FedExWizard_FinishButtonClick(object sender, WizardNavigationEventArgs e)
        {
            Response.Redirect("Shipments.aspx");
        }


        private string SendEmail(string filename, string poNum)
        {
            FedExShippingDefault def =
                ((ShipmentDetailsInfo) FedExWizard.WizardSteps[0].FindControl("ShipmentDetails_Details")).GetEntity();

            var msg = new MailMessage();
            var smtp = new SmtpClient();
            try
            {
                //msg.Subject = ConfigurationManager.AppSettings["EmailSubject"];
                msg.Subject = "PowerShipper Label Request – PO # " + poNum;

                //msg.To.Add(new MailAddress(def.FedExBulkLabelEmailAddress));
                msg.To.Add(new MailAddress(tbEmailForSend.Text.Trim()));
                msg.From = new MailAddress(def.AddressToSendLabels);

                //var sb = new StringBuilder();


                //sb.AppendLine("ShipperFedExAccountNumber: " + def.ShipperFedExAccountNumber + " ");
                //sb.AppendLine("ShipperCompanyName: " + def.ShipperCompanyName + " ");
                //sb.AppendLine("ShipperPhysicalAddress: " + def.ShipperPhysicalAddress + " ");
                //sb.AppendLine("ShipperContactName: " + def.ShipperContactName + " ");
                //sb.AppendLine("ShipperContactPhone: " + def.ShipperContactPhone + " ");
                //sb.AppendLine("PackagePickupLocation: " + def.PackagePickupLocation + " ");
                //sb.AppendLine("MoreThanOnePackageForEachRecipient: " + def.MoreThanOnePackageForEachRecipient + " ");
                //sb.AppendLine("FedExServiceType: " + def.FedExServiceType + " ");
                //sb.AppendLine("DomesticOrInternational: " + def.DomesticOrInternational + " ");
                //sb.AppendLine("AnyPackagesToResidentialAddresses: " + def.AnyPackagesToResidentialAddresses + " ");
                //sb.AppendLine("SignatureRequired: " + def.SignatureRequired + " ");
                //sb.AppendLine("BulkLabelSupportSpecialHandling: " + def.BulkLabelSupportSpecialHandling + " ");
                //sb.AppendLine("PackageType: " + def.PackageType + " ");
                //sb.AppendLine("DeclaredValueAmount: " + def.DeclaredValueAmount + " ");
                //sb.AppendLine("PaymentType: " + def.PaymentType + " ");
                //sb.AppendLine("FedExAccountNumberForBilling: " + def.FedExAccountNumberForBilling + " ");
                //sb.AppendLine("DeliveryServiceForLabels: " + def.DeliveryServiceForLabels + " ");
                //sb.AppendLine("AddressToSendLabels: " + def.AddressToSendLabels + " ");
                //sb.AppendLine("FedExBulkLabelEmailAddress: " + def.FedExBulkLabelEmailAddress + " ");
                //sb.AppendLine("OtherCCEmailAddresses: " + def.OtherCcEmailAddresses + " ");
                //sb.AppendLine("ConsumerCustomerServicePhoneNumber: " + def.ConsumerCustomerServicePhoneNumber + " ");
                //sb.AppendLine("PDMShippingExceptionEmailAddress: " + def.PdmShippingExceptionEmailAddress + " ");

                //msg.Body = sb.ToString();
                msg.Attachments.Add(new Attachment(filename));


                msg.Priority = MailPriority.Normal;
                msg.BodyEncoding = Encoding.UTF8;
                //msg.IsBodyHtml = true;
                

                smtp.Send(msg);

                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                msg.Dispose();
                smtp = null;
            }
        }
    }
}