﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.ImportExport;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class HEAFileConversion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "HEA File Conversion");
                Session["FileName"] = "";
            }
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            try
            {
                UploadedFile file = null;
                bool bFileExtValidation = false;
                bool bIsUploadedValidation = false;

                foreach (string fileInputID in Request.Files)
                {
                    string fileExtension;

                    file = UploadedFile.FromHttpPostedFile(Request.Files[fileInputID]);

                    string GUIDFileName = Convert.ToString(System.Guid.NewGuid() + file.GetExtension());

                    Session["FileName"] = GUIDFileName;

                    if (file.ContentLength > 0)
                    {
                        fileExtension = Path.GetExtension(file.FileName).ToLower();

                        if (fileExtension == ".xls" || fileExtension == ".xlsx")
                        {
                            file.SaveAs(Server.MapPath("~/Uploads/") + GUIDFileName);
                            bFileExtValidation = true;
                        }
                        else
                        {
                            LblMessage.ForeColor = System.Drawing.Color.Red;
                            LblMessage.Text = "Please select files of .xls OR .xlsx format.";
                        }

                        bIsUploadedValidation = true;
                    }

                    if (file.ContentLength > 0 && bFileExtValidation == true)
                    {
                        Response.Redirect("HEAFileConversionUploadPrompt.aspx",false);
                    }
                    else
                    {
                        LblMessage.ForeColor = System.Drawing.Color.Red;
                        if (bIsUploadedValidation == false)
                            LblMessage.Text = "Please select a file.";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                LblMessage.Text = "Unable to Upload File. Reason: " + ex.Message;
                LblMessage.ControlStyle.ForeColor = System.Drawing.Color.Red;
            }
        }
    }
}