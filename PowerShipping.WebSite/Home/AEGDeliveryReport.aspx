﻿<%@ Page Title="AEG Delivery Report" Language="C#" MasterPageFile="~/Shared/Default.Master"
    AutoEventWireup="true" CodeBehind="AEGDeliveryReport.aspx.cs" Inherits="PowerShipping.WebSite.Home.AEGDeliveryReport"
    Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../Controls/ProjectsList.ascx" TagName="ProjectsList" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="pageDiv">
        <table>
            <tr>
                <td>
                    <h3> AEG Delivery Report</h3>
                </td>
                <td colspan="5" align="center">
                    <uc1:ProjectsList ID="ProjectsList1" runat="server" IsCompanyUse="true" />
                </td>
            </tr>
            <tr>
                <td>
                    PO #
                    <asp:TextBox ID="tbPONumberFilter" runat="server"></asp:TextBox>
                </td>
                <td>
                    Start Date
                    <telerik:RadDatePicker ID="dpStartDateFilter" runat="server" MinDate="01-01-1990"
                        MaxDate="01-01-2020">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    End Date
                    <telerik:RadDatePicker ID="dpEndDateFilter" runat="server" MinDate="01-01-1990" MaxDate="01-01-2020">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    Kit Type
                    <telerik:RadComboBox ID="RadComboBox_KitType" runat="server" Width="150px" DataTextField="Text"
                        DataValueField="Value">
                    </telerik:RadComboBox>
                </td>
                <td>
                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search"
                        CssClass="ptButton" />
                </td>
                <td>
                    <asp:Button ID="btnClearFilter" runat="server" OnClick="btnClearFilter_Click" Text="Clear"
                        CssClass="ptButton" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid_Accounts" runat="server" Width="100%" GridLines="None" 
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True"
            Skin="Windows7" OnNeedDataSource="RadGrid_Accounts_NeedDataSource"   
            OnItemCreated="RadGrid_Accounts_ItemCreated" EnableViewState="true"
            OnItemDataBound="RadGrid_Accounts_ItemDataBound">
            <ExportSettings ExportOnlyData="true" IgnorePaging="true" >
            </ExportSettings>
            <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true">
                <CommandItemSettings ShowExportToWordButton="true" ShowExportToExcelButton="true"
                    ShowExportToCsvButton="true" ShowExportToPdfButton="true" />
                <Columns>
                    <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="AccountNumber" SortExpression="AccountNumber"
                        UniqueName="AccountNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" ReadOnly="true" EditFormColumnIndex="0">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AccountName" HeaderText="Name" SortExpression="AccountName"
                        UniqueName="AccountName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ServiceAddress1" HeaderText="ServiceAddress1"
                        SortExpression="ServiceAddress1" UniqueName="ServiceAddress1" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ServiceAddress2" HeaderText="ServiceAddress2"
                        SortExpression="ServiceAddress2" UniqueName="ServiceAddress2" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ServiceCity" HeaderText="ServiceCity" SortExpression="ServiceCity"
                        UniqueName="ServiceCity" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ServiceState" HeaderText="ServiceState" SortExpression="ServiceState"
                        UniqueName="ServiceState" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ServiceZip" HeaderText="ServiceZip" SortExpression="ServiceZip"
                        UniqueName="ServiceZip" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" MaxLength="5">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email"
                        UniqueName="Email" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridMaskedColumn DataField="Phone1" HeaderText="Phone 1" SortExpression="Phone1"
                        Mask="###-###-####" UniqueName="Phone1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" DataFormatString="{0:(###)###-####}">
                    </telerik:GridMaskedColumn>
                    <telerik:GridMaskedColumn DataField="Phone2" HeaderText="Phone 2" SortExpression="Phone2"
                        Mask="###-###-####" UniqueName="Phone2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" DataFormatString="{0:(###)###-####}">
                    </telerik:GridMaskedColumn>
                    <telerik:GridDateTimeColumn DataField="AnalysisDate" HeaderText="AnalysisDate" SortExpression="AnalysisDate"
                        UniqueName="AnalysisDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" DataFormatString="{0:MM/dd/yyyy}">
                    </telerik:GridDateTimeColumn>
                    <telerik:GridDateTimeColumn DataField="ReceiptDate" HeaderText="ReceiptDate" SortExpression="ReceiptDate"
                        UniqueName="ReceiptDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" DataFormatString="{0:MM/dd/yyyy}">
                    </telerik:GridDateTimeColumn>
                    <telerik:GridBoundColumn DataField="KitTypeName" HeaderText="KitType" SortExpression="KitTypeName"
                        UniqueName="KitTypeName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="OperatingCompany" HeaderText="Operating Company"
                        SortExpression="OperatingCompany" UniqueName="OperatingCompany" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="WaterHeaterFuel" HeaderText="Water Heater Fuel"
                        SortExpression="WaterHeaterFuel" UniqueName="WaterHeaterFuel" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TransactionType" HeaderText="Transaction Type"
                        SortExpression="TransactionType" UniqueName="TransactionType" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CatalogID" HeaderText="CatalogID" SortExpression="CatalogID"
                        UniqueName="CatalogID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity"
                        UniqueName="Quantity" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ProgramID" HeaderText="ProgramID" SortExpression="ProgramID"
                        UniqueName="ProgramID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn> 
                     <telerik:GridBoundColumn DataField="HeaterFuel" HeaderText="Heater Fuel"
                        SortExpression="HeaterFuel" UniqueName="HeaterFuel" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="RateCode" HeaderText="Rate Code"
                        SortExpression="RateCode" UniqueName="RateCode" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>                   
                    <telerik:GridBoundColumn DataField="CompanyName" HeaderText="CompanyName" SortExpression="CompanyName"
                        UniqueName="CompanyName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </div>
</asp:Content>