﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReshipImportExceptionsPage.aspx.cs" MasterPageFile="~/Shared/Default.Master"
Inherits="PowerShipping.WebSite.Home.ReshipImportExceptionsPage"  Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../Controls/ReshipImportBatchesFilter.ascx" TagName="ReshipImportBatchesFilter"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <uc2:ReshipImportBatchesFilter ID="ReshipImportBatchesFilter1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="pageDiv">
        <table>
            <tr>
                <td>
                    <h3>
                        Reship Import Batch Exceptions</h3>
                </td>
                <td>
                </td>
                <td>
                </td>
                </tr>     
        </table>
        <br />
        <telerik:RadGrid ID="RadGrid_Batches_Exceptions" runat="server" Width="100%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True"
            Skin="Windows7" onneeddatasource="RadGrid_Batches_Exceptions_NeedDataSource"
             OnItemCreated="RadGrid_Batches_Exceptions_ItemCreated" >
            <%--       
        OnDeleteCommand="RadGrid_Batches_DeleteCommand" 
        
        OnItemDataBound="RadGrid_Batches_ItemDataBound"--%>
            <ExportSettings ExportOnlyData="true" IgnorePaging="true">
            </ExportSettings>
            <ClientSettings>
            </ClientSettings>
            <MasterTableView DataKeyNames="ReshipExceptionImportID" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true">
                <CommandItemSettings ShowExportToWordButton="true" ShowExportToExcelButton="true"
                    ShowExportToCsvButton="true" ShowExportToPdfButton="true" />
                <Columns>
                     <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="Account Number" SortExpression="AccountNumber"
                        ReadOnly="true" UniqueName="AccountNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ShipmentNumber" HeaderText="Shipment Number" SortExpression="ShipmentNumber"
                        ReadOnly="true" UniqueName="ShipmentNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TrackingNumber" HeaderText="Tracking Number" SortExpression="TrackingNumber"
                        ReadOnly="true" UniqueName="TrackingNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ReshipImportBatchName" HeaderText="BatchName" SortExpression="ReshipImportBatchName"
                        ReadOnly="true" UniqueName="ReshipImportBatchName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ShipperName" HeaderText="ShipperName" SortExpression="ShipperName"
                        UniqueName="ShipperName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ImportedBy" HeaderText="Imported By" SortExpression="ImportedBy"
                        UniqueName="ImportedBy" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                     <telerik:GridBoundColumn DataField="ImportedTime" HeaderText="Imported Date" SortExpression="ImportedDate"
                        UniqueName="ImportedDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" DataFormatString="{0:MM/dd/yyyy}"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </div>
</asp:Content>