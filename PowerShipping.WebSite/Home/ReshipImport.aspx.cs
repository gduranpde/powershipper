﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.WebSite.Admin;
using PowerShipping.WebSite.Controls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class ReshipImport : System.Web.UI.Page
    {
        UserManagementPresenter _userManagementPresenter = new UserManagementPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "ReshipImport");

                RadComboBox_Shipper.DataSource = _userManagementPresenter.GetAllShipers();
                RadComboBox_Shipper.SelectedIndex = 2;
                RadComboBox_Shipper.DataBind();
                RadDatePicker_ShipperDateEndDate.SelectedDate = DateTime.Now;
            }
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            foreach (string fileInputID in Request.Files)
            {
                UploadedFile file = UploadedFile.FromHttpPostedFile(Request.Files[fileInputID]);
                if (file.ContentLength > 0)
                {
                    string[] fName = file.GetName().Split('.');
                    string customFileName = fName[0] + DateTime.Now.ToString("yyyy-MM-ddHHmmssfff") + "." + fName[1];

                    var path = Server.MapPath("~/Uploads/") + customFileName;
                    file.SaveAs(path);

                    var importer = new PowerShipping.ImportExport.ReshipImporter(path, User.Identity.Name);
                    importer.ImportProgress += ShowImportProgress;
                    Guid ShipperID = new Guid(RadComboBox_Shipper.SelectedValue.ToString());
                    DateTime ReshipDate = RadDatePicker_ShipperDateEndDate.SelectedDate.Value;

                    PowerShipping.Entities.ReshipImportBatch batch = importer.Import(ReshipDate, ShipperID, path);

                    LblErrors.Text = string.Empty;

                    if (!importer.ImportErrors.IsEmpty)
                    {
                        LblErrors.ForeColor = Color.Red;
                        importer.ImportErrors.ForEach(err => LblErrors.Text += err + "<br>");
                    }
                    else
                    {
                        // hlExceptions.Visible = false;

                        LblErrors.ForeColor = Color.Black;
                        LblErrors.Text = "Total requests: " + batch.NumberOfRecords + ". <br/>Imported Successfully: " + batch.ImportedSuccessfully +
                                           ". <br/>Imported with exceptions: " + batch.NumberWithExceptions; // +". <br/>Batch Label: " + batch.BatchLabel + ".";

                        //if (batch.NumberWithExceptions > 0)
                        //{
                        //    hlExceptions.Text = "View list of exceptions";
                        //    hlExceptions.NavigateUrl = "~/Home/ConsumerExceptions.aspx?BatchId=" + batch.Id;
                        //    hlExceptions.Visible = true;
                        //}
                    }
                }
                else
                {
                    LblMessage.ControlStyle.ForeColor = Color.Red;
                    LblMessage.Text = "No File is selected";
                }
            }
        }

        private void ShowImportProgress(object sender, PowerShipping.ImportExport.ImportExportProgressEventArgs e)
        {
            if (Response.IsClientConnected)
            {
                var progress = RadProgressContext.Current;
                progress.SecondaryTotal = e.Total;
                progress.SecondaryValue = e.Value;
                progress.SecondaryPercent = e.Total == 0 ? 0 : Convert.ToInt32((100 * Convert.ToDouble(e.Value)) / Convert.ToDouble(e.Total));
                progress.CurrentOperationText = e.Message;
            }
        }
    }
}