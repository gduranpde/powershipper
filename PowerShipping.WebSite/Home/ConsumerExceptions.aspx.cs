﻿using System;
using System.Collections.Generic;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.ImportExport;
using PowerShipping.WebSite.Controls;
using Telerik.Web.UI;
using PowerShipping.Logging;
using PowerShipping.Data;

namespace PowerShipping.WebSite
{
    public partial class ConsumerExceptions : Page
    {
        ConsumerExceptionsPresenter presenter = new ConsumerExceptionsPresenter();
        ConsumerRequestsPresenter presenterRequest = new ConsumerRequestsPresenter();
        UserManagementPresenter presenterAdv = new UserManagementPresenter();

        private Guid? _batchId = null;
		bool isExport = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;
            //btnValidate.Enabled = Common.IsCanEdit();

            string batchId = Page.Request.QueryString["BatchId"];
            if (string.IsNullOrEmpty(batchId))
                return;

            try
            {
                _batchId = new Guid(batchId);
            }
            catch
            {
                _batchId = null;
            }

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Consumer Exceptions");
            }
        }

        private void ProjectsList1_OnProjectChanged(string projectId)
        {
            RadGrid_ConsumerRequests.Rebind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            lbReuslt.Text = "";
            RadGrid_ConsumerRequests.Rebind();
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            tbAccountNumberFilter.Text = null;
            tbAccountNameFilter.Text = null;
            //tbAddressFilter.Text = null;
            //tbPhoneFilter.Text = null;
            dpStartDateFilter.SelectedDate = null;
            tbBatchID.Text = null;
        }

        protected void btnValidate_Click(object sender, EventArgs e)
        {
            lbReuslt.Text = "";

            List<ConsumerRequestException> list = GetFilteredList();
            ValidateSelected(list);
        }

        //sc 170912
        private void ValidateSelected(List<ConsumerRequestException> list)
        {
            int validated = 0;

            foreach (var exception in list)
            {
                //for validation
                exception.States = presenterAdv.GetStatesByProject(exception.ProjectId);

                ConsumerRequest request = new ConsumerRequest();
                var result = EntityValidation.MapAndValidate(exception, request, true);

                if (result.Success)
                {
                    exception.Status = ConsumerRequestExceptionStatus.Deleted;

                    request.Id = Guid.NewGuid();
                    request.ChangedBy = Membership.GetUser().UserName;
                    request.CRImportedDate = exception.ImportedDate; // SS Oct15
                    presenterRequest.Save(request);
                    validated++;
                }
                presenter.Save(exception);
            }

            lbReuslt.Text = "Total exceptions process: " + list.Count + ". ReSubmited: " + validated + ".";

            RadGrid_ConsumerRequests.Rebind();
        }

        protected void RadGrid_ConsumerRequests_ItemCommand(object source, GridCommandEventArgs e)
        {
	        switch (e.CommandName)
	        {
		        case RadGrid.EditCommandName:
		        {
			        if (e.Item is GridDataItem)
			        {
				        var item = (GridDataItem) e.Item;
				        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scroll",
					        "setTimeout(function(){ Expand('" + item.ClientID + "');}, 100);", true);
			        }
		        }
			        break;
		        case "Override":
		        {
			        if (e.Item is GridDataItem)
			        {
				        try
				        {
					        var item = (GridDataItem) e.Item;
					        var id = new Guid(item.GetDataKeyValue("Id").ToString());
					        var crEx = presenter.GetById(id);
					        
					        var crExNew = CreateConsumerRequestException(crEx);
							crExNew.CRImportedDate = crEx.ImportedDate;

					        var reason = presenterAdv.GetReasonByID(crEx.Id);

					        //check the CREx reason in order to move the CREx record in CR table
					        if (
						        (string.Equals(reason[0],
							        "Consumer Request with Account Number [" + crEx.AccountNumber + "] already exists")) ||
						        (string.Equals(reason[0],
							        "Consumer Request with Account Number [" + crEx.AccountNumber +
							        "] already exists in the exception project")) ||
						        (string.Equals(reason[0],
							        "Consumer Request with Account Number [" + crEx.AccountNumber +
							        "] already exists in other projects that doesn't allow duplicates")) ||
						        (string.Equals(reason[0],
							        "Consumer Request with Account Number [" + crEx.AccountNumber +
							        "] already exists and the project doesn't allow duplicates")) ||
						        (string.Equals(reason[0], "Project doesn't allow duplicates")) ||
						        (string.Equals(reason[0],
							        "Consumer Request with Account Number [" + crEx.AccountNumber + "] already exists in this Company")) ||
						        (string.Equals(reason[0],
							        "Consumer Request with Account Number [" + crEx.AccountNumber +
							        "] already exists in other projects where duplicates are not allowed")))
					        {
						        var cr = CreateConsumerRequest(crExNew);
						        //insert to CR & delete from CREx
						        DataProvider.Current.ConsumerRequest.InsertConsumerRequest(cr);
						        DataProvider.Current.ConsumerRequestException.DeleteConsumerRequestException(crEx.Id);

						        RadGrid_ConsumerRequests.Rebind();
					        }
					        else
					        {
						        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "BlockOverridePopup", "BlockOverridePopup();",
							        true);
					        }
				        }
				        catch (Exception ex)
				        {
					        CurrentLogger.Log.Error(ex);
					        Log.LogError(ex);
					        Label l = new Label();
					        l.Text = "Unable to update ConsumerRequestException. Reason: " + ex.Message;
					        l.ControlStyle.ForeColor = System.Drawing.Color.Red;
					        RadGrid_ConsumerRequests.Controls.Add(l);
				        }
			        }
		        }
			        break;
		        case RadGrid.ExportToCsvCommandName:
		        case RadGrid.ExportToExcelCommandName:
		        case RadGrid.ExportToWordCommandName:
		        case RadGrid.ExportToPdfCommandName:
		        {
			        isExport = true;
		        }
			        break;
	        }
        }

        protected void RadGrid_ConsumerRequests_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                Button spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                LinkButton addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGrid_ConsumerRequests_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item is GridDataItem) && !e.Item.IsInEditMode)
            {
                GridDataItem item = (GridDataItem)e.Item;
                string reason = item["Reason"].Text;
                reason = reason.Replace("|", "<br/>");
                item["Reason"].Text = reason;

	            try
                {
                    string phone = EnumUtils.GetPhoneDigits(item["Phone1"].Text);
                    if (!string.IsNullOrEmpty(phone))
                    {
                        Double myDouble = Convert.ToDouble(phone);
                        item["Phone1"].Text = myDouble.ToString("###-###-####");
                    }

                    string phone2 = EnumUtils.GetPhoneDigits(item["Phone2"].Text);
                    if (!string.IsNullOrEmpty(phone2))
                    {
                        Double myDouble2 = Convert.ToDouble(phone2);
                        item["Phone2"].Text = myDouble2.ToString("###-###-####");
                    }
                }
                catch
                {
                }
            }
			if (e.Item is GridDataItem && isExport)
			{
				TableCell cell = (e.Item as GridDataItem)["AccountNumber"];
				LinkButton lb = cell.FindControl("EditButton") as LinkButton;
				cell.Text = lb.Text;
			}
        }

        protected void RadGrid_ConsumerRequests_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_ConsumerRequests.DataSource = GetFilteredList();
        }

        protected void RadGrid_ConsumerRequests_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        GridEditFormItem item = (GridEditFormItem)e.Item;
                        Guid id = new Guid(item.GetDataKeyValue("Id").ToString());
                        if (id != Guid.Empty)
                        {
                            if (e.Item is GridEditableItem)
                            {
                                object o = ((ConsumerExceptionCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                                ConsumerRequestException crExNew = (ConsumerRequestException)o;

                                var crEx = CreateConsumerRequestException(crExNew);

	                            List<string> reason = presenterAdv.GetReasonByID(crExNew.Id);

                                //check the CREx reason in order to move the CREx record in CR table
                                if ((string.Equals(reason[0], "Consumer Request with Account Number [" + crEx.AccountNumber + "] already exists"))||
                                    (string.Equals(reason[0], "Consumer Request with Account Number [" + crEx.AccountNumber + "] already exists in the exception project")) ||
                                    (string.Equals(reason[0], "Consumer Request with Account Number [" + crEx.AccountNumber + "] already exists in other projects that doesn't allow duplicates")) ||
                                    (string.Equals(reason[0], "Consumer Request with Account Number [" + crEx.AccountNumber + "] already exists and the project doesn't allow duplicates")) ||
                                    (string.Equals(reason[0], "Project doesn't allow duplicates"))||
                                    (string.Equals(reason[0], "Consumer Request with Account Number [" + crEx.AccountNumber + "] already exists in this Company")) ||
									(string.Equals(reason[0], "Consumer Request with Account Number [" + crEx.AccountNumber + "] already exists in other projects where duplicates are not allowed")))
                                {
                                    var cr = CreateConsumerRequest(crExNew);

	                                //cr.OverrideDuplicates = crExNew.OverrideDuplicates;

                                    //var validationResult = EntityValidation.ValidateConsReqFromConsReqEx(cr);
                                    //if (validationResult.Success) 
                                    //{
                                        //insert to CR & delete from CREx
                                        DataProvider.Current.ConsumerRequest.InsertConsumerRequest(cr);
                                        DataProvider.Current.ConsumerRequestException.DeleteConsumerRequestException(crEx.Id);
                                    //}
                                }
                                else
                                {
                                    if (crEx.Status != ConsumerRequestExceptionStatus.RequestReceived)
                                    {
                                        var validationResult = EntityValidation.Validate(crEx, true, true);
                                        if (validationResult.Success)
                                        {
                                            presenter.Save(crEx);
                                            //sc 170912
                                            List<ConsumerRequestException> list = GetFilteredListOnEdit(crExNew.AccountNumber, crEx.AccountName.ToUpperInvariant());
                                            ValidateSelected(list);
                                            RadGrid_ConsumerRequests.Rebind();
                                        }
                                        else
                                        {
                                            Label l = new Label();
                                            l.Text = validationResult.Errors.ToString();
                                            l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                                            RadGrid_ConsumerRequests.Controls.Add(l);
                                        }
                                    }
                                    else
                                    {
                                        var validationResult = EntityValidation.Validate(crEx, true);
                                        presenter.Save(crEx);
                                        //sc 170912
                                        List<ConsumerRequestException> list = GetFilteredListOnEdit(crExNew.AccountNumber, crEx.AccountName.ToUpperInvariant());
                                        ValidateSelected(list);
                                        //end
                                        RadGrid_ConsumerRequests.Rebind();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CurrentLogger.Log.Error(ex);
                Log.LogError(ex);
                Label l = new Label();
                l.Text = "Unable to update ConsumerRequestException. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_ConsumerRequests.Controls.Add(l);
            }
        }

	    private ConsumerRequestException CreateConsumerRequestException(ConsumerRequestException crExNew)
	    {
		    ConsumerRequestException crEx = presenter.GetById(crExNew.Id);

		    crEx.AccountName = crExNew.AccountName;
		    if (!string.IsNullOrEmpty(crEx.AccountName))
			    crEx.AccountName = crEx.AccountName.ToUpperInvariant();

		    crEx.AccountNumber = crExNew.AccountNumber;
		    crEx.Address1 = crExNew.Address1;
		    if (!string.IsNullOrEmpty(crEx.Address1))
			    crEx.Address1 = crEx.Address1.ToUpperInvariant();
		    crEx.Address2 = crExNew.Address2;
		    if (!string.IsNullOrEmpty(crEx.Address2))
			    crEx.Address2 = crEx.Address2.ToUpperInvariant();
		    crEx.AnalysisDate = crExNew.AnalysisDate;
		    crEx.City = crExNew.City;
		    if (!string.IsNullOrEmpty(crEx.City))
			    crEx.City = crEx.City.ToUpperInvariant();
		    crEx.Email = crExNew.Email;
		    crEx.IsOkayToContact = crExNew.IsOkayToContact;
		    crEx.Method = crExNew.Method;
		    crEx.Notes = crExNew.Notes;
		    crEx.OperatingCompany = crExNew.OperatingCompany;
		    crEx.OutOfState = crExNew.OutOfState;
		    crEx.Phone1 = crExNew.Phone1;
		    crEx.Phone2 = crExNew.Phone2;
		    crEx.State = crExNew.State;
		    if (!string.IsNullOrEmpty(crEx.State))
			    crEx.State = crEx.State.ToUpperInvariant();

		    // NEW
		    crEx.States = presenterAdv.GetStatesByProject(crEx.ProjectId);
		    //----- END NEW ------//

		    crEx.Status = crExNew.Status;
		    crEx.WaterHeaterFuel = crExNew.WaterHeaterFuel;
		    crEx.HeaterFuel = crExNew.HeaterFuel;
		    //PG31
		    crEx.RateCode = crExNew.RateCode;
		    crEx.IncomeQualified = crExNew.IncomeQualified;
		    crEx.FacilityType = crExNew.FacilityType;
		    //PG31
		    crEx.ZipCode = crExNew.ZipCode;
		    crEx.ProjectId = crExNew.ProjectId;
		    crEx.PremiseID = crExNew.PremiseID;
		    crEx.Quantity = crExNew.Quantity;
		    crEx.RequestedKit = crExNew.RequestedKit;
		    crEx.CompanyName = crExNew.CompanyName;

		    crEx.ServiceAddress1 = crExNew.ServiceAddress1;
		    if (!string.IsNullOrEmpty(crEx.ServiceAddress1))
			    crEx.ServiceAddress1 = crEx.ServiceAddress1.ToUpperInvariant();
		    crEx.ServiceAddress2 = crExNew.ServiceAddress2;
		    if (!string.IsNullOrEmpty(crEx.ServiceAddress2))
			    crEx.ServiceAddress2 = crEx.ServiceAddress2.ToUpperInvariant();

		    crEx.ServiceCity = crExNew.ServiceCity;
		    if (!string.IsNullOrEmpty(crEx.ServiceCity))
			    crEx.ServiceCity = crEx.ServiceCity.ToUpperInvariant();
		    crEx.ServiceState = crExNew.ServiceState;
		    if (!string.IsNullOrEmpty(crEx.ServiceState))
			    crEx.ServiceState = crEx.ServiceState.ToUpperInvariant();
		    crEx.ServiceZip = crExNew.ServiceZip;

		    crEx.OverrideDuplicates = crExNew.OverrideDuplicates;
		    return crEx;
	    }

	    private ConsumerRequest CreateConsumerRequest(ConsumerRequestException crExNew)
	    {
		    ConsumerRequest cr = new ConsumerRequest(); //presenterRequest.GetById(crExNew.Id);
		    cr.Id = Guid.NewGuid();
		    cr.AccountName = crExNew.AccountName;
		    if (!string.IsNullOrEmpty(cr.AccountName))
			    cr.AccountName = cr.AccountName.ToUpperInvariant();

		    cr.AccountNumber = crExNew.AccountNumber;
		    cr.Address1 = crExNew.Address1;
		    if (!string.IsNullOrEmpty(cr.Address1))
			    cr.Address1 = cr.Address1.ToUpperInvariant();
		    cr.Address2 = crExNew.Address2;
		    if (!string.IsNullOrEmpty(cr.Address2))
			    cr.Address2 = cr.Address2.ToUpperInvariant();
			if (!string.IsNullOrEmpty(crExNew.AnalysisDate))
				cr.AnalysisDate = DateTime.Parse(crExNew.AnalysisDate);
		    //cr.AnalysisDate = crExNew.AnalysisDate;
		    cr.CRImportedDate = crExNew.CRImportedDate;
		    cr.City = crExNew.City;
		    if (!string.IsNullOrEmpty(cr.City))
			    cr.City = cr.City.ToUpperInvariant();
		    cr.Email = crExNew.Email;
		    //cr.IsOkayToContact = crExNew.IsOkayToContact;
		    cr.Method = crExNew.Method;
		    cr.Notes = crExNew.Notes;
		    cr.OperatingCompany = crExNew.OperatingCompany;
		    cr.OutOfState = crExNew.OutOfState;
		    cr.Phone1 = crExNew.Phone1;
		    cr.Phone2 = crExNew.Phone2;
		    cr.State = crExNew.State;
		    cr.States = presenterAdv.GetStatesByProject(cr.ProjectId);
		    //cr.Status = (ConsumerRequest)crExNew.Status;
		    cr.WaterHeaterFuel = crExNew.WaterHeaterFuel;
		    cr.HeaterFuel = crExNew.HeaterFuel;
		    //PG31
		    cr.RateCode = crExNew.RateCode;
		    cr.IncomeQualified = crExNew.IncomeQualified;
		    cr.FacilityType = crExNew.FacilityType;
		    //PG31
		    cr.ZipCode = crExNew.ZipCode;
		    cr.ProjectId = crExNew.ProjectId;
		    cr.PremiseID = crExNew.PremiseID;
		    cr.Quantity = int.Parse(crExNew.Quantity);
		    cr.RequestedKit = crExNew.RequestedKit;
		    cr.CompanyName = crExNew.CompanyName;
		    cr.BatchId = crExNew.BatchId;
		    cr.BatchLabel = crExNew.BatchLabel;

		    cr.ServiceAddress1 = crExNew.ServiceAddress1;
		    if (!string.IsNullOrEmpty(cr.ServiceAddress1))
			    cr.ServiceAddress1 = cr.ServiceAddress1.ToUpperInvariant();
		    cr.ServiceAddress2 = crExNew.ServiceAddress2;
		    if (!string.IsNullOrEmpty(cr.ServiceAddress2))
			    cr.ServiceAddress2 = cr.ServiceAddress2.ToUpperInvariant();

		    cr.ServiceCity = crExNew.ServiceCity;
		    if (!string.IsNullOrEmpty(cr.ServiceCity))
			    cr.ServiceCity = cr.ServiceCity.ToUpperInvariant();
		    cr.ServiceState = crExNew.ServiceState;
		    if (!string.IsNullOrEmpty(cr.ServiceState))
			    cr.ServiceState = cr.ServiceState.ToUpperInvariant();
		    cr.ServiceZip = crExNew.ServiceZip;
		    return cr;
	    }

	    protected void RadGrid_ConsumerRequests_DeleteCommand(object source, GridCommandEventArgs e)
        {
            //    try
            //    {
            //        if (e.CommandName == RadGrid.DeleteCommandName)
            //        {
            //            Guid id = new Guid((e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"].ToString());
            //            ConsumerRequestException cr = presenter.GetById(id);
            //            cr.Status = ConsumerRequestExceptionStatus.Deleted;

            //            presenter.Save(cr);
            //            RadGrid_ConsumerRequests.Rebind();
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        Label l = new Label();
            //        l.Text = "Unable to update ConsumerRequestException. Reason: " + ex.Message;
            //        l.ControlStyle.ForeColor = System.Drawing.Color.Red;
            //        RadGrid_ConsumerRequests.Controls.Add(l);
            //    }
        }

        private List<ConsumerRequestException> GetFilteredList()
        {
            string accNum = tbAccountNumberFilter.Text;
            string accName = tbAccountNameFilter.Text;
            string batchId = tbBatchID.Text;
            //string phone = tbPhoneFilter.Text;
            //string address = tbAddressFilter.Text;

            if (accNum == string.Empty)
                accNum = null;
            if (accName == string.Empty)
                accName = null;
            if (batchId == string.Empty)
                batchId = null;
            //if (phone == string.Empty)
            //    phone = null;
            //if (address == string.Empty)
            //    address = null;

            DateTime? startDate = dpStartDateFilter.SelectedDate;

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList1.ProjectId;
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = ProjectsList1.UserId;
            }

            if (ProjectsList1.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList1.CompanyId;
            }

            return presenter.GetAll(accNum, accName, batchId, startDate, _batchId, userId, projectId, companyId);
        }

        //sc 170912
        private List<ConsumerRequestException> GetFilteredListOnEdit(string accNum, string accName)
        {
            string batchId = null;

            DateTime? startDate = null;

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList1.ProjectId;
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = ProjectsList1.UserId;
            }

            if (ProjectsList1.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList1.CompanyId;
            }

            return presenter.GetAll(accNum, accName, batchId, startDate, _batchId, userId, projectId, companyId);
        }
    }
}