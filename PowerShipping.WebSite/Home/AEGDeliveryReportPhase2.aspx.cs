﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;
using Telerik.Web.UI.GridExcelBuilder;

namespace PowerShipping.WebSite.Home
{
    public partial class AEGDeliveryReportPhase2 : Page
    {
        private readonly AuditReportPresenter presenterPhase2 = new AuditReportPresenter();
        private bool _isFirstLoadPhase2 = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsListPhase2.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }

            if (!IsPostBack)
            {
                
                RadComboBoxPhase2_KitType.DataBind();
            }

            ProjectsListPhase2.OnProjectChanged += ProjectsListPhase2_OnProjectChanged;

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "AEG Delivery Report - Phase 2");
                _isFirstLoadPhase2 = true;
            }

            RadGridPhase2_Accounts.ExcelMLWorkBookCreated += RadGridPhase2_Accounts_ExcelMLWorkBookCreated;
            RadGridPhase2_Accounts.ExcelMLExportRowCreated += RadGridPhase2_Accounts_ExcelMLExportRowCreated;
            RadGridPhase2_Accounts.ClientSettings.AllowColumnsReorder = true;
        }

        void RadGridPhase2_Accounts_ExcelMLExportRowCreated(object sender, GridExportExcelMLRowCreatedArgs e)
        {
            
        }

        void RadGridPhase2_Accounts_ExcelMLWorkBookCreated(object sender, GridExcelMLWorkBookCreatedEventArgs e)
        {
            var grid = sender as RadGrid;
            if (grid != null)
            {
                grid.MasterTableView.SwapColumns("HeaterFuel", "TransactionType");
            }
        }

        private void ProjectsListPhase2_OnProjectChanged(string projectId)
        {            
            RadGridPhase2_Accounts_NeedDataSource(null, null);
            RadGridPhase2_Accounts.Rebind();
        }

        protected void btnSearchPhase2_Click(object sender, EventArgs e)
        {
            RadGridPhase2_Accounts_NeedDataSource(null, null);
            RadGridPhase2_Accounts.Rebind();
        }

        protected void btnClearFilterPhase2_Click(object sender, EventArgs e)
        {
            tbPONumberFilterPhase2.Text = null;
            dpStartDateFilterPhase2.SelectedDate = null;
            dpEndDateFilterPhase2.SelectedDate = null;
            RadComboBoxPhase2_KitType.SelectedValue = null;
        }

        protected void RadGridPhase2_Accounts_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                var spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                var addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGridPhase2_Accounts_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (!_isFirstLoadPhase2)
                RadGridPhase2_Accounts.DataSource = GetFilteredListAccountsPhase2();
        }

        protected void RadGridPhase2_Accounts_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item is GridDataItem) && !e.Item.IsInEditMode)
            {
                var gridItem = (GridDataItem)e.Item;
                try
                {
                    Double myDouble = Convert.ToDouble(gridItem["Phone1"].Text);
                    gridItem["Phone1"].Text = myDouble.ToString("###-###-####");

                    Double myDouble2 = Convert.ToDouble(gridItem["Phone2"].Text);
                    gridItem["Phone2"].Text = myDouble2.ToString("###-###-####");
                }
                catch
                {
                }
            }
        }

        private DataSet GetFilteredListAccountsPhase2()
        {
            string poNum = tbPONumberFilterPhase2.Text;
            DateTime? startDate = dpStartDateFilterPhase2.SelectedDate;
            DateTime? endDate = dpStartDateFilterPhase2.SelectedDate;

            if (endDate != null)
                endDate = endDate.Value.AddDays(1);

            Guid? kitType = null;
            string kit = RadComboBoxPhase2_KitType.SelectedValue;
            if (!string.IsNullOrEmpty(kit))
                kitType = new Guid(RadComboBoxPhase2_KitType.SelectedValue);

            if (poNum == string.Empty)
                poNum = null;

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsListPhase2.ProjectId != Guid.Empty)
            {
                projectId = ProjectsListPhase2.ProjectId;
            }

            if (ProjectsListPhase2.UserId != Guid.Empty)
            {
                userId = ProjectsListPhase2.UserId;
            }

            if (ProjectsListPhase2.CompanyId != Guid.Empty)
            {
                companyId = ProjectsListPhase2.CompanyId;
            }

            return presenterPhase2.GetAEGReportPhase2(poNum, startDate, endDate, kitType, userId, projectId, companyId);
        }
    }
}