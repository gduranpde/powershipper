﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Shared/Default.Master" CodeBehind="AEGXMLExceptionReport.aspx.cs"
 Inherits="PowerShipping.WebSite.Home.AEGXMLExceptionReport" Theme="Default" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../Controls/ProjectsList.ascx" TagName="ProjectsList" TagPrefix="uc1" %>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="pageDiv" onmousemove="dragWin(event);">
        <table>
            <tr>
                <td>
                    <h3>
                        AEG XML Exception Report</h3>
                </td>
                <td colspan="5" align="center">
                    <uc1:ProjectsList ID="ProjectsList1" runat="server" IsCompanyUse="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="PO #" Width="80px"></asp:Label>   
                    <asp:TextBox ID="tbPONumberFilter" runat="server"></asp:TextBox>
                </td>
                <td>
                   <asp:Label ID="Label3" runat="server" Text="Exception Start Date" Width="120px"></asp:Label>    
                    <telerik:RadDatePicker ID="dpStartDateFilter" runat="server" MinDate="01-01-1990"
                        MaxDate="01-01-2020">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Receipt Start Date" Width="100px"></asp:Label>    
                    <telerik:RadDatePicker ID="dpReceptionStartDate" runat="server" MinDate="01-01-1990"
                        MaxDate="01-01-2020">
                    </telerik:RadDatePicker>
                </td>
              <%--  <td>
                    End Date
                    <telerik:RadDatePicker ID="dpEndDateFilter" runat="server" MinDate="01-01-1990" MaxDate="01-01-2020">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    Kit Type
                    <telerik:RadComboBox ID="RadComboBox_KitType" runat="server" Width="150px" DataTextField="Text"
                        DataValueField="Value">
                    </telerik:RadComboBox>
                </td>--%>
                <td>
                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search"
                        CssClass="ptButton" />
                </td>
                <%--<td>
                    <asp:Button ID="btnClearFilter" runat="server" OnClick="btnClearFilter_Click" Text="Clear"
                        CssClass="ptButton" />
                </td>--%>
                 <%--PG31--%>
                <td>
                    <asp:Button ID="Button1" runat="server" Text="Generate XML" onclick="GenerateXML_Click" CssClass="ptButton"/></td>
                     <%--PG31--%>
            </tr>
            <tr>
             <td>
                    <asp:Label ID="Label2" runat="server" Text="Kit Type" Width="80px"></asp:Label>   
                    <telerik:RadComboBox ID="RadComboBox_KitType" runat="server" Width="150px" DataTextField="Text"
                        DataValueField="Value">
                    </telerik:RadComboBox>
                </td>
                 <td>
                    <asp:Label ID="Label4" runat="server" Text="Exception End Date" Width="120px"></asp:Label>    
                    <telerik:RadDatePicker ID="dpExceptionEndDate" runat="server" MinDate="01-01-1990" MaxDate="01-01-2020">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Receipt End Date" Width="100px"></asp:Label>    
                    <telerik:RadDatePicker ID="dpReceiptEndDate" runat="server" MinDate="01-01-1990" MaxDate="01-01-2020">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    <asp:Button ID="btnClearFilter" runat="server" OnClick="btnClearFilter_Click" Text="Clear"
                        CssClass="ptButton" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid_Exceptions" runat="server" Width="100%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True"
            Skin="Windows7" OnNeedDataSource="RadGrid_Exceptions_NeedDataSource" OnItemCreated="RadGrid_Exceptions_ItemCreated"
            OnItemDataBound="RadGrid_Exceptions_ItemDataBound">
            <ExportSettings ExportOnlyData="true" IgnorePaging="true">
            </ExportSettings>
            <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true">
                <CommandItemSettings ShowExportToWordButton="true" ShowExportToExcelButton="true"
                    ShowExportToCsvButton="true" ShowExportToPdfButton="true" />
                <Columns>
                    <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="AccountNumber" SortExpression="AccountNumber"
                        UniqueName="AccountNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" ReadOnly="true" EditFormColumnIndex="0">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AccountName" HeaderText="Name" SortExpression="AccountName"
                        UniqueName="AccountName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ServiceAddress1" HeaderText="ServiceAddress1"
                        SortExpression="ServiceAddress1" UniqueName="ServiceAddress1" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ServiceAddress2" HeaderText="ServiceAddress2"
                        SortExpression="ServiceAddress2" UniqueName="ServiceAddress2" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ServiceCity" HeaderText="ServiceCity" SortExpression="ServiceCity"
                        UniqueName="ServiceCity" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ServiceState" HeaderText="ServiceState" SortExpression="ServiceState"
                        UniqueName="ServiceState" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ServiceZip" HeaderText="ServiceZip" SortExpression="ServiceZip"
                        UniqueName="ServiceZip" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" MaxLength="5">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email"
                        UniqueName="Email" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridMaskedColumn DataField="Phone1" HeaderText="Phone 1" SortExpression="Phone1"
                        Mask="###-###-####" UniqueName="Phone1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" DataFormatString="{0:(###)###-####}">
                    </telerik:GridMaskedColumn>
                    <telerik:GridMaskedColumn DataField="Phone2" HeaderText="Phone 2" SortExpression="Phone2"
                        Mask="###-###-####" UniqueName="Phone2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" DataFormatString="{0:(###)###-####}">
                    </telerik:GridMaskedColumn>
                    <telerik:GridDateTimeColumn DataField="AnalysisDate" HeaderText="AnalysisDate" SortExpression="AnalysisDate"
                        UniqueName="AnalysisDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" DataFormatString="{0:MM/dd/yyyy}">
                    </telerik:GridDateTimeColumn>
                    <telerik:GridDateTimeColumn DataField="ReceiptDate" HeaderText="ReceiptDate" SortExpression="ReceiptDate"
                        UniqueName="ReceiptDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" DataFormatString="{0:MM/dd/yyyy}">
                    </telerik:GridDateTimeColumn>
                    <telerik:GridDateTimeColumn DataField="AuditFailureDate" HeaderText="Exception Date"
                        SortExpression="AuditFailureDate" ReadOnly="true" UniqueName="AuditFailureDate"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        DataFormatString="{0:MM/dd/yyyy}">
                    </telerik:GridDateTimeColumn>
                    <telerik:GridBoundColumn DataField="KitTypeName" HeaderText="KitType" SortExpression="KitTypeName"
                        UniqueName="KitTypeName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="OperatingCompany" HeaderText="Operating Company"
                        SortExpression="OperatingCompany" UniqueName="OperatingCompany" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="WaterHeaterFuel" HeaderText="Water Heater Fuel"
                        SortExpression="WaterHeaterFuel" UniqueName="WaterHeaterFuel" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="HeaterFuel" HeaderText="Heater Fuel"
                        SortExpression="HeaterFuel" UniqueName="HeaterFuel" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IsOkayToContact" HeaderText="OK to contact"
                        SortExpression="IsOkayToContact" UniqueName="IsOkayToContact" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TransactionType" HeaderText="Transaction Type"
                        SortExpression="TransactionType" UniqueName="TransactionType" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CatalogID" HeaderText="CatalogID" SortExpression="CatalogID"
                        UniqueName="CatalogID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity"
                        UniqueName="Quantity" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ProgramID" HeaderText="ProgramID" SortExpression="ProgramID"
                        UniqueName="ProgramID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CompanyName" HeaderText="CompanyName" SortExpression="CompanyName"
                        UniqueName="CompanyName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
              <%--PG31--%>
    <div id="divBack" style="background-color: black; display: none; height: 100%; opacity: 0.5;
            position: fixed; top: 0; width: 100%; z-index: 7000; margin-left: -240px;">
        </div>
        <%--PG31--%>
        <div id="AEGXMLErrorPopUp" class="div_AEGXMLErrorPopUp">
            <div style="width: 400px; height: 30px; cursor: move;" onmousedown="mouse_down(event,'AEGXMLErrorPopUp');"
                onmouseup="mouse_up(event,'AEGXMLErrorPopUp');">
                <div style="width: 200px; float: left; padding-top: 10px;">
                    <span style="margin-left: 10px;">Error Message!</span>
                </div>
                <div style="width: 200px; float: right; padding-top: 10px;">
                    <input id="btnCloseDeviceID" type="button" value="" onclick="return AEGXMLReportFailed();"
                        style="background-image: url('../../App_Themes/Default/Images/btnPopupClose.png');
                        border: none; width: 48px; height: 21px; position: relative; margin-left: 147px;
                        margin-top: -11px; cursor: pointer;" />
                </div>
            </div>
            <div style="width: 400px; margin-top: 27px;">
                <div style="height: 10px; margin-left: 15px; right: -1px; width: 362px;">
                    <asp:Label ID="LblWarningMessage" runat="server" Text="Error: In order to run the report, the Project must have a valid Program ID. This value can set in the Project Setup screen on the Administrator menu."
                        ForeColor="Red" Style="margin-left: 10px;"></asp:Label>
                </div>
            </div>
            <br />
            <br />
            <div style="width: 400px; margin-top: 55px;">
                <div style="width: 200px; float: right;">
                    <input id="cancel_AEGXMLReportFailed" type="button" value="Ok" onclick="return AEGXMLReportFailed();"
                        style="margin-left: -29px; margin-top: -22px; cursor: pointer;" />
                </div>
            </div>
        </div>
<%--PG31--%>
    </div>
</asp:Content>

