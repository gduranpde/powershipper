<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AEGXMLDeliveryReportPhase2.1(ZIP).aspx.cs"
    MasterPageFile="~/Shared/Default.Master" Inherits="PowerShipping.WebSite.Home.AEGXMLDeliveryReportPhase2Test" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../Controls/ProjectsList.ascx" TagName="ProjectsList" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="pageDiv" onmousemove="dragWin(event);">
        <table>
            <tr style="width:100%">
                <td style="width:45%">
                    <%--<h3>AEG XML Delivery Report - Phase 2.1 (ZIP)</h3>--%>
                </td>
                <td colspan="5" align="center" style="width:55%">
                    <uc1:ProjectsList ID="ProjectsList1" runat="server" IsCompanyUse="true" />
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>PO #
                    <asp:TextBox ID="tbPONumberFilter" runat="server"></asp:TextBox>
                </td>
                <td>Start Date
                    <telerik:raddatepicker id="dpStartDateFilter" runat="server" mindate="01-01-1990"
                        maxdate="01-01-2020">
                    </telerik:raddatepicker>
                </td>
                <td>End Date
                    <telerik:raddatepicker id="dpEndDateFilter" runat="server" mindate="01-01-1990" maxdate="01-01-2020">
                    </telerik:raddatepicker>
                </td>
                <td>Kit Type
                    <telerik:radcombobox id="RadComboBox_KitType" runat="server" width="150px" datatextfield="Text"
                        datavaluefield="Value">
                    </telerik:radcombobox>
                </td>
                <td>
                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search"
                        CssClass="ptButton" />
                </td>
                <td>
                    <asp:Button ID="btnClearFilter" runat="server" OnClick="btnClearFilter_Click" Text="Clear"
                        CssClass="ptButton" />
                </td>
                <%--PG31--%>
                <td>
                    <asp:Button ID="Button1" runat="server" Text="Generate XML" OnClick="GenerateXML_Click" CssClass="ptButton" /></td>
                <%--PG31--%>
				 <td>
                    <asp:Button ID="btnBulkExport" runat="server" Text="Bulk Export" OnClick="BulkExport_Click" CssClass="ptButton" /></td>                
            </tr>
        </table>
        <telerik:radgrid id="RadGrid_Accounts" runat="server" width="100%" gridlines="None"
            autogeneratecolumns="false" pagesize="15" allowsorting="True" allowpaging="True"
            skin="Windows7" onneeddatasource="RadGrid_Accounts_NeedDataSource" onitemcreated="RadGrid_Accounts_ItemCreated"
            onitemdatabound="RadGrid_Accounts_ItemDataBound">
            <ExportSettings ExportOnlyData="true" IgnorePaging="true">
            </ExportSettings>
            <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true">
                <CommandItemSettings ShowExportToWordButton="true" ShowExportToExcelButton="true"
                    ShowExportToCsvButton="true" ShowExportToPdfButton="true" />
                <Columns>
                    <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="AccountNumber" SortExpression="AccountNumber"
                        UniqueName="AccountNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" ReadOnly="true" EditFormColumnIndex="0">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AccountName" HeaderText="Name" SortExpression="AccountName"
                        UniqueName="AccountName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ServiceAddress1" HeaderText="ServiceAddress1"
                        SortExpression="ServiceAddress1" UniqueName="ServiceAddress1" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ServiceAddress2" HeaderText="ServiceAddress2"
                        SortExpression="ServiceAddress2" UniqueName="ServiceAddress2" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ServiceCity" HeaderText="ServiceCity" SortExpression="ServiceCity"
                        UniqueName="ServiceCity" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ServiceState" HeaderText="ServiceState" SortExpression="ServiceState"
                        UniqueName="ServiceState" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ServiceZip" HeaderText="ServiceZip" SortExpression="ServiceZip"
                        UniqueName="ServiceZip" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" MaxLength="5">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email"
                        UniqueName="Email" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridMaskedColumn DataField="Phone1" HeaderText="Phone 1" SortExpression="Phone1"
                        Mask="###-###-####" UniqueName="Phone1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" DataFormatString="{0:(###)###-####}">
                    </telerik:GridMaskedColumn>
                    <telerik:GridMaskedColumn DataField="Phone2" HeaderText="Phone 2" SortExpression="Phone2"
                        Mask="###-###-####" UniqueName="Phone2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" DataFormatString="{0:(###)###-####}">
                    </telerik:GridMaskedColumn>
                    <telerik:GridDateTimeColumn DataField="AnalysisDate" HeaderText="AnalysisDate" SortExpression="AnalysisDate"
                        UniqueName="AnalysisDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" DataFormatString="{0:MM/dd/yyyy}">
                    </telerik:GridDateTimeColumn>
                    <telerik:GridDateTimeColumn DataField="ReceiptDate" HeaderText="ReceiptDate" SortExpression="ReceiptDate"
                        UniqueName="ReceiptDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" DataFormatString="{0:MM/dd/yyyy}">
                    </telerik:GridDateTimeColumn>
                    <telerik:GridBoundColumn DataField="KitTypeName" HeaderText="KitType" SortExpression="KitTypeName"
                        UniqueName="KitTypeName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="OperatingCompany" HeaderText="Operating Company"
                        SortExpression="OperatingCompany" UniqueName="OperatingCompany" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="WaterHeaterFuel" HeaderText="Water Heater Fuel"
                        SortExpression="WaterHeaterFuel" UniqueName="WaterHeaterFuel" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="HeaterFuel" HeaderText="Heater Fuel"
                        SortExpression="HeaterFuel" UniqueName="HeaterFuel" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IsOkayToContact" HeaderText="OK to contact"
                        SortExpression="IsOkayToContact" UniqueName="IsOkayToContact" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="RateCode" HeaderText="Rate Code"
                        SortExpression="RateCode" UniqueName="RateCode" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TransactionType" HeaderText="Transaction Type"
                        SortExpression="TransactionType" UniqueName="TransactionType" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CatalogID" HeaderText="CatalogID" SortExpression="CatalogID"
                        UniqueName="CatalogID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity"
                        UniqueName="Quantity" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ProgramID" HeaderText="ProgramID" SortExpression="ProgramID"
                        UniqueName="ProgramID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CompanyName" HeaderText="CompanyName" SortExpression="CompanyName"
                        UniqueName="CompanyName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Kw_Impact" HeaderText="Kw Impact"
                        SortExpression="Kw_Impact" UniqueName="Kw_Impact" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>                   
                    <telerik:GridBoundColumn DataField="Kwh_Impact" HeaderText="Kwh Impact" SortExpression="Kwh_Impact"
                        UniqueName="Kwh_Impact" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="IncomeQualified" HeaderText="Income Qualified" SortExpression="IncomeQualified"
                        UniqueName="IncomeQualified" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
        </telerik:radgrid>
        <%--PG31--%>
        <div id="divBack" style="background-color: black; display: none; height: 100%; opacity: 0.5; position: fixed; top: 0; width: 100%; z-index: 7000; margin-left: -240px;">
        </div>
        <%--PG31--%>
        <div id="AEGXMLErrorPopUp" class="div_AEGXMLErrorPopUp">
            <div style="width: 400px; height: 30px; cursor: move;" onmousedown="mouse_down(event,'AEGXMLErrorPopUp');"
                onmouseup="mouse_up(event,'AEGXMLErrorPopUp');">
                <div style="width: 200px; float: left; padding-top: 10px;">
                    <span style="margin-left: 10px;">Error Message!</span>
                </div>
                <div style="width: 200px; float: right; padding-top: 10px;">
                    <input id="btnCloseDeviceID" type="button" value="" onclick="return AEGXMLReportFailed();"
                        style="background-image: url('../../App_Themes/Default/Images/btnPopupClose.png'); border: none; width: 48px; height: 21px; position: relative; margin-left: 147px; margin-top: -11px; cursor: pointer;" />
                </div>
            </div>
            <div style="width: 400px; margin-top: 27px;">
                <div style="height: 10px; margin-left: 15px; right: -1px; width: 362px;">
                    <asp:Label ID="LblWarningMessage" runat="server" Text="Error: In order to run the report, the Project must have a valid Program ID. This value can set in the Project Setup screen on the Administrator menu."
                        ForeColor="Red" Style="margin-left: 10px;"></asp:Label>
                </div>
            </div>
            <br />
            <br />
            <div style="width: 400px; margin-top: 55px;">
                <div style="width: 200px; float: right;">
                    <input id="cancel_AEGXMLReportFailed" type="button" value="Ok" onclick="return AEGXMLReportFailed();"
                        style="margin-left: -29px; margin-top: -22px; cursor: pointer;" />
                </div>
            </div>
        </div>
        <%--PG31--%>
    </div>
</asp:Content>
