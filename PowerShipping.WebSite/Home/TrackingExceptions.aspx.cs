﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Data;
using PowerShipping.Entities;
using PowerShipping.ImportExport;
using PowerShipping.WebSite.Controls;
using Telerik.Web.UI;
using System.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class TrackingExceptions : System.Web.UI.Page
    {
        TrackingExceptionsPresenter presenter = new TrackingExceptionsPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList1.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }
            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Tracking Exceptions");
            }
        }

        void ProjectsList1_OnProjectChanged(string projectId)
        {
            RadGrid_TrackingEx.Rebind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            lbReuslt.Text = "";
            RadGrid_TrackingEx.Rebind();
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            tbAccountNumberFilter.Text = null;
            tbTrackingNumberFilter.Text = null;
            tbPurchaseOrderNumberFilter.Text = null;
        }

        protected void btnValidate_Click(object sender, EventArgs e)
        {
            lbReuslt.Text = "";

            List<TrackingNumberException> list = GetFilteredList();
            int validated = 0;

            foreach (var exception in list)
            {
                var validation = EntityValidation.Validate(exception);

                if (validation.Success)
                {
                    ShipmentDetail existingDetail = presenter.GetShipmentByShipmentNumber(exception.Reference);
                    existingDetail.FedExTrackingNumber = exception.TrackingNumber;
                    existingDetail.FedExBatchId = exception.BatchId;
                    presenter.SaveShipmentDetail(existingDetail);

                    exception.Status = TrackingNumberExceptionStatus.Deleted;
                    validated++;
                }
            
                presenter.Save(exception);
            }

            lbReuslt.Text = "Total exceptions process: " + list.Count + ". ReSubmited: " + validated + ".";

            RadGrid_TrackingEx.Rebind();
        }

        protected void RadGrid_TrackingEx_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.EditCommandName)
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scroll", "setTimeout(function(){ Expand('" + item.ClientID + "');}, 100);", true);
                }
            }
        }

        protected void RadGrid_TrackingEx_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                Button spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                LinkButton addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGrid_TrackingEx_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                string reason = item["Reason"].Text;
                reason = reason.Replace("|", "<br/>");
                item["Reason"].Text = reason;
            }
        }

        protected void RadGrid_TrackingEx_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_TrackingEx.DataSource = GetFilteredList();
        }

        protected void RadGrid_TrackingEx_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        GridEditFormItem item = (GridEditFormItem)e.Item;
                        Guid id = new Guid(item.GetDataKeyValue("Id").ToString());
                        if (id != Guid.Empty)
                        {
                            if (e.Item is GridEditableItem)
                            {
                                object o = ((TrackingExceptionCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                                TrackingNumberException trExNew = (TrackingNumberException)o;

                                TrackingNumberException trEx = presenter.GetById(trExNew.Id);

                                trEx.PurchaseOrderNumber = trExNew.PurchaseOrderNumber;
                                trEx.TrackingNumber = trExNew.TrackingNumber;
                                trEx.AccountNumber = trExNew.AccountNumber;

                                trEx.AccountName = trExNew.AccountName;
                                if (!string.IsNullOrEmpty(trEx.AccountName))
                                    trEx.AccountName = trEx.AccountName.ToUpperInvariant();

                                trEx.Address1 = trExNew.Address1;
                                if (!string.IsNullOrEmpty(trEx.Address1))
                                    trEx.Address1 = trEx.Address1.ToUpperInvariant();

                                trEx.Address2 = trExNew.Address2;
                                if (!string.IsNullOrEmpty(trEx.Address2))
                                    trEx.Address2 = trEx.Address2.ToUpperInvariant();

                                trEx.City = trExNew.City;
                                if (!string.IsNullOrEmpty(trEx.City))
                                    trEx.City = trEx.City.ToUpperInvariant();

                                trEx.State = trExNew.State;
                                if (!string.IsNullOrEmpty(trEx.State))
                                    trEx.State = trEx.State.ToUpperInvariant();

                                trEx.ZipCode = trExNew.ZipCode;
                                trEx.Reference = trExNew.Reference;
                               
                                //GridEditFormItem formItem = e.Item as GridEditFormItem;

                                //TrackingNumberException trEx = presenter.GetById(id);

                                //trEx.PurchaseOrderNumber =
                                //    (!string.IsNullOrEmpty(((TextBox)formItem["PurchaseOrderNumber"].Controls[0]).Text))
                                //        ? ((TextBox)formItem["PurchaseOrderNumber"].Controls[0]).Text
                                //        : string.Empty;
                                //trEx.TrackingNumber =
                                //    (!string.IsNullOrEmpty(((TextBox)formItem["TrackingNumber"].Controls[0]).Text))
                                //        ? ((TextBox)formItem["TrackingNumber"].Controls[0]).Text
                                //        : string.Empty;
                                //trEx.AccountName =
                                //    (!string.IsNullOrEmpty(((TextBox)formItem["AccountName"].Controls[0]).Text))
                                //        ? ((TextBox)formItem["AccountName"].Controls[0]).Text.ToUpperInvariant()
                                //        : string.Empty;
                                //trEx.AccountNumber =
                                //    (!string.IsNullOrEmpty(((TextBox)formItem["AccountNumber"].Controls[0]).Text))
                                //        ? ((TextBox)formItem["AccountNumber"].Controls[0]).Text
                                //        : string.Empty;
                                //trEx.Address1 =
                                //   (!string.IsNullOrEmpty(((TextBox)formItem["Address1"].Controls[0]).Text))
                                //       ? ((TextBox)formItem["Address1"].Controls[0]).Text.ToUpperInvariant()
                                //       : string.Empty;
                                //trEx.Address2 =
                                //   (!string.IsNullOrEmpty(((TextBox)formItem["Address2"].Controls[0]).Text))
                                //       ? ((TextBox)formItem["Address2"].Controls[0]).Text.ToUpperInvariant()
                                //       : string.Empty;
                                //trEx.City =
                                //   (!string.IsNullOrEmpty(((TextBox)formItem["City"].Controls[0]).Text))
                                //       ? ((TextBox)formItem["City"].Controls[0]).Text.ToUpperInvariant()
                                //       : string.Empty;
                                //trEx.State =
                                //   (!string.IsNullOrEmpty(((TextBox)formItem["State"].Controls[0]).Text))
                                //       ? ((TextBox)formItem["State"].Controls[0]).Text.ToUpperInvariant()
                                //       : string.Empty;
                                //trEx.ZipCode =
                                //    (!string.IsNullOrEmpty(((TextBox)formItem["ZipCode"].Controls[0]).Text))
                                //        ? ((TextBox)formItem["ZipCode"].Controls[0]).Text
                                //        : string.Empty;
                                //trEx.Reference =
                                //    (!string.IsNullOrEmpty(((TextBox)formItem["Reference"].Controls[0]).Text))
                                //        ? ((TextBox)formItem["Reference"].Controls[0]).Text
                                //        : string.Empty;
                               

                                EntityValidation.Validate(trEx, true);//updates reason

                                presenter.Save(trEx);
                                RadGrid_TrackingEx.Rebind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                Label l = new Label();
                l.Text = "Unable to update Tracking Exception. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_TrackingEx.Controls.Add(l);
            }
        }

        protected void RadGrid_TrackingEx_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.DeleteCommandName)
                {
                    Guid id = new Guid((e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"].ToString());
                    TrackingNumberException trEx = presenter.GetById(id);
                    trEx.Status = TrackingNumberExceptionStatus.Deleted;

                    presenter.Save(trEx);
                    RadGrid_TrackingEx.Rebind();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                Label l = new Label();
                l.Text = "Unable to update Tracking Exception. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_TrackingEx.Controls.Add(l);
            }
        }

        private List<TrackingNumberException> GetFilteredList()
        {
            string accNum = tbAccountNumberFilter.Text;
            string trNum = tbTrackingNumberFilter.Text;
            string poNum = tbPurchaseOrderNumberFilter.Text;
            
            if (accNum == string.Empty)
                accNum = null;
            if (trNum == string.Empty)
                trNum = null;
            if (poNum == string.Empty)
                poNum = null;

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList1.ProjectId;
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = ProjectsList1.UserId;
            }

            if (ProjectsList1.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList1.CompanyId;
            }

            return presenter.GetAll(accNum, trNum, poNum, userId, projectId, companyId);
        }
    }
}
