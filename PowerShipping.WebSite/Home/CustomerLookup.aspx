﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Shared/Default.Master" CodeBehind="CustomerLookup.aspx.cs" Inherits="PowerShipping.WebSite.Home.CustomerLookup" Theme="Default" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>

<%@ Register src="../Controls/CustomerFilter.ascx" tagname="CustomerFilter" tagprefix="uc1" %>
<%--<%@ Register src="../Controls/ProjectsList.ascx" tagname="ProjectsList" tagprefix="uc1" %>--%>
<%@ Register src="../Controls/CustomerImportProjects.ascx" tagname="CustomerImportProjects" tagprefix="uc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

 <uc1:CustomerFilter ID="CustomerFilter1" runat="server" />
 
 </asp:Content>
 
 <asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<asp:HiddenField runat="server" ID="HiddenFiled_CustomQuery" />
<div class="pageDiv">
<table style="width: 100%;">
    <tr>
        <td style="width: 120px;">              
              <h3>Customers</h3>
        </td>
      <%--  <td>
             <uc1:ProjectsList ID="ProjectsList1" runat="server" IsCompanyUse="true" />           
        </td> --%>
        <td style="width: 500px;">
            <uc2:CustomerImportProjects ID="CustomerProjects" runat="server" IsCompanyUse="true" />
        </td>      
		<td style="text-align: left;">
			<asp:Button ID="btnBulkExport" runat="server" OnClick="btnBulkExport_OnClick"
                        Text="Bulk Export" Visible="False" />
		</td>
    </tr>
    <tr>    
       <td colspan=3>
            <telerik:RadGrid ID="RadGrid_Customers" runat="server" Width="100%" GridLines="None"
                    AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True" 
                      Skin="Windows7" 
                OnNeedDataSource="RadGrid_Customers_NeedDataSource"    
                OnUpdateCommand="RadGrid_Customers_UpdateCommand"
                OnDeleteCommand="RadGrid_Customers_DeleteCommand"       
                OnItemDataBound="RadGrid_Customers_ItemDataBound" 
                oninsertcommand="RadGrid_Customers_InsertCommand"
                OnItemCommand="RadGrid_Customers_ItemCommand"   
                OnItemCreated="RadGrid_Customers_ItemCreated"             
               >     
               <ExportSettings ExportOnlyData="true" IgnorePaging="true"  > 
                    </ExportSettings>
                    <ClientSettings  >  
                                <Selecting AllowRowSelect="true" />
                                <Resizing AllowColumnResize="True" /> 
                                                            
                    </ClientSettings>
                    <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                     AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true" PagerStyle-Mode="NextPrevNumericAndAdvanced"
                    >
                    <CommandItemSettings 
                            ShowExportToWordButton="False" 
                            ShowExportToExcelButton="False"
                            ShowExportToCsvButton="False"
                            ShowExportToPdfButton="False"
                            />
                    <Columns> 
                        
                          <telerik:GridTemplateColumn SortExpression="AccountNumber" HeaderText="AccountNumber" DataField="AccountNumber" DataType="System.String" UniqueName="AccountNumber">
                                <ItemTemplate>
                                    <asp:LinkButton ID="EditButton" runat="server" CommandName="Edit" Text="<%# ((PowerShipping.Entities.Customer)Container.DataItem).AccountNumber %>"></asp:LinkButton>
                                   </ItemTemplate>
                                <HeaderStyle Width="100px"/>
                          </telerik:GridTemplateColumn>
                          <telerik:GridBoundColumn DataField="InvitationCode" HeaderText="Invitation Code" SortExpression="InvitationCode"
                                UniqueName="InvitationCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">   
                               <%-- <HeaderStyle Width="160px"/>    --%>                 
                                </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="ContactName" HeaderText="ContactName" SortExpression="ContactName"
                                UniqueName="ContactName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">   
                                <HeaderStyle Width="160px"/>                     
                                </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="CompanyName" HeaderText="Company Name" SortExpression="CompanyName"
                                UniqueName="CompanyName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">   
                                <HeaderStyle Width="160px"/>                     
                                </telerik:GridBoundColumn>
                                
                                
                          <telerik:GridBoundColumn DataField="MailingAddress1" HeaderText="MailingAddress1" SortExpression="MailingAddress1"
                                UniqueName="MailingAddress1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                               <%-- <HeaderStyle Width="140px"/>--%>
                                </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="MailingAddress2" HeaderText="MailingAddress2" SortExpression="MailingAddress2"
                                UniqueName="MailingAddress2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="MailingCity" HeaderText="MailingCity" SortExpression="MailingCity"
                                UniqueName="MailingCity" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                              <%--  <HeaderStyle Width="150px"/>--%>
                                </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="MailingState" HeaderText="MailingState" SortExpression="MailingState"
                                UniqueName="MailingState" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                               <%-- <HeaderStyle Width="40px"/>--%>
                                </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="MailingZip" HeaderText="MailingZip" SortExpression="MailingZip"
                                UniqueName="MailingZip" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" MaxLength="5">
                              <%--  <HeaderStyle Width="80px"/>--%>
                                </telerik:GridBoundColumn>
                           
                          <telerik:GridBoundColumn DataField="Email1" HeaderText="Email1" SortExpression="Email1"
                                UniqueName="Email1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="Email2" HeaderText="Email2" SortExpression="Email2"
                                UniqueName="Email2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                </telerik:GridBoundColumn>     
                                
                          <telerik:GridMaskedColumn DataField="Phone1" HeaderText="Phone 1" SortExpression="Phone1" Mask="###-###-####"
                                UniqueName="Phone1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                DataFormatString="{0:(###)###-####}"> 
                                <HeaderStyle Width="85px"/>                   
                            </telerik:GridMaskedColumn> 
                          <telerik:GridMaskedColumn DataField="Phone2" HeaderText="Phone 2" SortExpression="Phone2" Mask="###-###-####" 
                                UniqueName="Phone2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                DataFormatString="{0:(###)###-####}"> 
                                <HeaderStyle Width="85px"/>                   
                            </telerik:GridMaskedColumn>       
                                
                                
                           <telerik:GridBoundColumn DataField="ServiceAddress1" HeaderText="Service Address 1" SortExpression="ServiceAddress1"
                                UniqueName="ServiceAddress1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                <HeaderStyle Width="140px"/>
                                </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="ServiceAddress2" HeaderText="Service Address 2" SortExpression="ServiceAddress2" 
                                UniqueName="ServiceAddress2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="ServiceCity" HeaderText="Service City" SortExpression="ServiceCity"
                                UniqueName="ServiceCity" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                <HeaderStyle Width="150px"/>
                                </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="ServiceState" HeaderText="ServiceState" SortExpression="ServiceState"
                                UniqueName="ServiceState" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                <HeaderStyle Width="40px"/>
                                </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="ServiceZip" HeaderText="Service Zip" SortExpression="ServiceZip"
                                UniqueName="ServiceZip" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" MaxLength="5">
                                <HeaderStyle Width="80px"/>
                                </telerik:GridBoundColumn>   
                                
                          <telerik:GridBoundColumn DataField="CompanyCode" HeaderText="Comp" SortExpression="CompanyCode"
                                    UniqueName="CompanyCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"> 
                                  <HeaderStyle Width="50px"/>                 
                                </telerik:GridBoundColumn> 
                          <telerik:GridBoundColumn DataField="ProjectCode" HeaderText="Project" SortExpression="ProjectCode"
                                    UniqueName="ProjectCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">    
                                      <HeaderStyle Width="50px"/>                             
                                </telerik:GridBoundColumn> 
                        
                         
                          <telerik:GridBoundColumn DataField="WaterHeaterType" HeaderText="WaterHeaterType" AllowSorting="false">
                                <HeaderStyle Width="40px"/>
                                </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="OperatingCompany" HeaderText="OpCo" AllowSorting="false">
                                       <HeaderStyle Width="45px"/>
                                </telerik:GridBoundColumn>                         
                          <telerik:GridBoundColumn DataField="PremiseID" HeaderText="PremiseID" SortExpression="PremiseID"
                                    UniqueName="PremiseID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                <HeaderStyle Width="80px"/>
                          </telerik:GridBoundColumn> 
                              
                          
                           <telerik:GridCheckBoxColumn DataField="OptOut" HeaderText="Opt Out" SortExpression="OptOut"
                              UniqueName="OptOut" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true">
                              <HeaderStyle Width="60px"/>
                          </telerik:GridCheckBoxColumn>
                          
                          <telerik:GridCheckBoxColumn DataField="OkToContact" HeaderText="Cont..." SortExpression="OkToContact"
                              UniqueName="OkToContact" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true">
                              <HeaderStyle Width="60px"/>
                          </telerik:GridCheckBoxColumn>

						  <telerik:GridBoundColumn DataField="RateCode" HeaderText="Rate Code" AllowSorting="false">
                                <HeaderStyle Width="40px"/>
                          </telerik:GridBoundColumn>
                           
                    </Columns>
                    <EditFormSettings UserControlName="../Controls/CustomerCard.ascx" EditFormType="WebUserControl">
                        <EditColumn UniqueName="EditCommandColumn1">
                        </EditColumn>
                     </EditFormSettings>
                    </MasterTableView>
            </telerik:RadGrid>
            
            <script type="text/javascript">

                function Expand(itemID) {
                    var Grid = $find('<%=RadGrid_Customers.ClientID %>');
                    var scrollArea = document.getElementById('ctl00_ContentPlaceHolder1_RadGrid_Customers')
                    var rowElement = document.getElementById(itemID);
                    window.scrollTo(0, rowElement.offsetTop + 200);
                }
                </script>
            
       </td>
    </tr> 
</table>
</div>   
</asp:Content>
