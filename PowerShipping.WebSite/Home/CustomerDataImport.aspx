﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Shared/Default.Master" CodeBehind="CustomerDataImport.aspx.cs" Inherits="PowerShipping.WebSite.Home.CustomerDataImport" Theme="Default" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<%@ Register src="../Controls/ProjectsList.ascx" tagname="ProjectsList" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


 <telerik:RadProgressManager ID="Radprogressmanager1" runat="server" />
 <div class="pageDiv">  
            
            <table>
                <tr>
                    <td>              
                          <h3>Customer Data Import</h3>
                    </td>
                    <td>
                         <uc1:ProjectsList ID="ProjectsList1" runat="server" />
                    </td>
                </tr>          
                <tr>
                    <td>
                        <telerik:RadUpload ID="RadUpload1" runat="server" ControlObjectsVisibility="None" />                        
                    </td>
                    <td>                                   
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:RadioButtonList runat="server" ID="RadioButtonList" AutoPostBack="false">
                        <asp:ListItem Value="Replace">Replace</asp:ListItem>
                        <asp:ListItem Value="Append">Append</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td>

                    </td>
                </tr>   
                <tr>
                    <td>
                        <asp:Label runat="server" ID="errorLabelAppendReplace" ForeColor="Red" />
                    </td>
                    <td>

                    </td>
                </tr>    
            </table>
            
            <div class="submitArea">
                <asp:Button runat="server" ID="SubmitButton" Text="Upload files"
                onclick="SubmitButton_Click" />
            </div>

            <telerik:RadProgressArea runat="server" ID="ProgressArea1" >
            <Localization Uploaded="File upload progress: " UploadedFiles="Imported records: " CurrentFileName="" 
            TotalFiles="Total records: " EstimatedTime="" TransferSpeed="" />
            </telerik:RadProgressArea>
            <br />
            <asp:HyperLink ID="hlExceptions" runat="server" NavigateUrl="" Visible="false" ></asp:HyperLink>
            <br />
            <asp:Label runat="server" ID="labelErrors" ForeColor="Red" />
            <asp:Label runat="server" ID="labelCountRecords" ForeColor="Black" />
            <br/>
			<div style="padding-top: 20px;">
                <asp:Button runat="server" ID="btnDownloadExp" Text="Download Exceptions"
                onclick="btnDownloadExp_Click" Visible="False" />
				<input type="hidden" id="historyId" value="" runat="server"/>
            </div>
    </div>
</asp:Content>