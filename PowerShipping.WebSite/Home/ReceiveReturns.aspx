﻿<%@ Page Title="Receive Returns" Language="C#" MasterPageFile="~/Shared/Default.Master"
    AutoEventWireup="true" CodeBehind="ReceiveReturns.aspx.cs" Inherits="PowerShipping.WebSite.Home.ReceiveReturns"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../Controls/ProjectsList.ascx" TagName="ProjectsList" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script src="../js/main.js" type="text/javascript"></script>

    <style type="text/css">
        .AuditorPopUp
        {
            background-image: url(  '../../App_Themes/Default/Images/div_back_AuditReport12' );
            background-repeat: no-repeat;
        }
        .div_BlockReturnsWarningOuter
        {
            display: none;
            height: 205px;
            width: 402px;
            background-image: url(  '../../App_Themes/Default/Images/div_back_AuditReport12.png' );
            background-repeat: no-repeat;
            z-index: 7001;
            position: absolute;
            left: 485px;
            top: 230px;
            background-position: -2px -2px;
            border-radius: 7px;
        }
    </style>

    <script type="text/javascript">
        function popup() {
            setTimeout(function() {
                //        document.getElementById('ctl00_ContentPlaceHolder1_ReasonPanel').style.zIndex = '4999';
                //        document.getElementById('ctl00_ContentPlaceHolder1_mpe_backgroundElement').style.zIndex = '4998';
                //        document.getElementById('ctl00_RadMenu1').style.zIndex = '4997';
            }, 100);
        }
        function BlockReturnsPopup() {
            var popupDiv = document.getElementById("BlockReturnsWarning");
            popupDiv.style.display = (popupDiv.style.display == "block") ? "none" : "block";

            var backDiv = document.getElementById("divBack");
            backDiv.style.display = (backDiv.style.display == "block") ? "none" : "block";
            return false;
        }
        function closePopup() {
            el = document.getElementById("overlay2");
            el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
            document.getElementById("divBack").style.display = "none";
        }
        function confirmSubmit() {

            var agree = confirm("Do you  want to delete this report?");
            if (agree)
                return true;
            else
                return false;
        }
    </script>

    <div class="pageDiv" onmousemove="dragWin(event);">
        <table>
            <tr>
                <td>
                    <h3>
                        Receive Returns</h3>
                </td>
                <td align="center">
                    <uc1:ProjectsList ID="ProjectsList1" runat="server" IsCompanyUse="true" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    Shipment #:
                    <asp:TextBox ID="tbShipmentNumber" runat="server" TabIndex="1">
                    </asp:TextBox>
                    Tracking #:
                    <asp:TextBox ID="tbTrackingNumber" runat="server" TabIndex="2">
                    </asp:TextBox>
                    Account Number
                    <asp:TextBox ID="tbAccNum" runat="server">
                    </asp:TextBox>
                    Account Name
                    <asp:TextBox ID="tbAccName" runat="server">
                    </asp:TextBox>
                    <asp:Button ID="btnReturn" runat="server" Text="Search" OnClick="btnSearch_OnClick"
                        CssClass="ptButton" />
                    <asp:Button ID="btnClearFilter" runat="server" OnClick="btnClearFilter_Click" Text="Clear"
                        CssClass="ptButton" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lbMessages" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <telerik:RadGrid ID="RadGrid_Grid" runat="server" Width="100%" GridLines="None" AutoGenerateColumns="false"
                        PageSize="15" AllowSorting="True" AllowPaging="True" Skin="Windows7" OnItemCreated="RadGrid_Grid_ItemCreated"
                        OnItemCommand="RadGrid_Grid_ItemCommand" OnNeedDataSource="RadGrid_Grid_NeedDataSource">
                        <ExportSettings ExportOnlyData="true" IgnorePaging="true">
                        </ExportSettings>
                        <MasterTableView DataKeyNames="Id,ReturnsAllowed" AllowMultiColumnSorting="True"
                            Width="100%" CommandItemDisplay="Top" AutoGenerateColumns="false" EditMode="EditForms"
                            PagerStyle-AlwaysVisible="true">
                            <CommandItemSettings ShowExportToWordButton="true" ShowExportToExcelButton="true"
                                ShowExportToCsvButton="true" ShowExportToPdfButton="true" />
                            <Columns>
                                <telerik:GridButtonColumn ButtonType="PushButton" Text="Return" CommandName="Return"
                                    UniqueName="btnReturn">
                                </telerik:GridButtonColumn>
                                <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="AccountNumber" SortExpression="AccountNumber"
                                    UniqueName="AccountNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false" ReadOnly="true" EditFormColumnIndex="0">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="AccountName" HeaderText="Name" SortExpression="AccountName"
                                    UniqueName="AccountName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                    <HeaderStyle Width="180px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Address1" HeaderText="Address1" SortExpression="Address1"
                                    UniqueName="Address1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                    <HeaderStyle Width="190px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="City" HeaderText="City" SortExpression="City"
                                    UniqueName="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                    <HeaderStyle Width="160px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="State" HeaderText="State" SortExpression="State"
                                    UniqueName="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ZipCode" HeaderText="ZIP" SortExpression="ZipCode"
                                    UniqueName="ZipCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false" MaxLength="10">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FedExTrackingNumber" HeaderText="Tracking Number"
                                    SortExpression="FedExTrackingNumber" UniqueName="FedExTrackingNumber" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="145px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ShipmentNumber" HeaderText="ShipmentNumber" SortExpression="ShipmentNumber"
                                    ReadOnly="true" UniqueName="ShipmentNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                    <HeaderStyle Width="110px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PurchaseOrderNumber" HeaderText="PO Number" SortExpression="PurchaseOrderNumber"
                                    UniqueName="PurchaseOrderNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ShipperCode" HeaderText="Shipper" SortExpression="ShipperCode"
                                    UniqueName="ShipperCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CompanyCode" HeaderText="Comp" SortExpression="CompanyCode"
                                    UniqueName="CompanyCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ProjectCode" HeaderText="Project" SortExpression="ProjectCode"
                                    UniqueName="ProjectCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ReturnsAllowed" HeaderText="ReturnsAllowed" SortExpression="ReturnsAllowed"
                                    UniqueName="ReturnsAllowed" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    Visible="false" ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
        <asp:Panel ID="Panel1" runat="server">
        </asp:Panel>
        <asp:Panel ID="ReasonPanel" runat="server" CssClass="modalPopup" HorizontalAlign="Center">
            <asp:HiddenField ID="hfId" runat="server" />
            <table>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="Label1" runat="server" Text="Return Information" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        Shipment #
                    </td>
                    <td align="left">
                        <asp:Label ID="tbPopUpShipNum" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        Tracking #
                    </td>
                    <td align="left">
                        <asp:Label ID="tbPopUpTrackNum" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        Name
                    </td>
                    <td align="left">
                        <asp:Label ID="tbPopUpName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        Reason for Return
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlReasons" runat="server" DataTextField="ReasonFull" DataValueField="ReasonID"
                            Width="150px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        Return Date
                    </td>
                    <td align="left">
                        <asp:TextBox ID="TxtReturnDate" runat="server" ToolTip="Please click on the calendar Image"
                            Width="100px"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TxtReturnDate"
                            PopupButtonID="ImgBtn_calendar" Format="MM/dd/yyyy">
                        </ajaxToolkit:CalendarExtender>
                        <%--<asp:Image ID="Img_Calendar" runat="server"
                            ImageUrl="~/App_Themes/Default/Images/small_calendar.png" />--%>
                        <asp:ImageButton ID="ImgBtn_calendar" runat="server" ImageUrl="~/App_Themes/Default/Images/small_calendar.png" />
                        <%-- <div id="divCalendar">
                            <telerik:RadDatePicker ID="RadDatePicker_ReceiveReturnDate" runat="server" Skin="Telerik"
                                Width="135px">
                                <calendar skin="Telerik" usecolumnheadersasselectors="False" userowheadersasselectors="False"
                                    viewselectortext="x">
                                </calendar>
                                <datepopupbutton hoverimageurl="" imageurl="" />
                                <dateinput dateformat="MM/dd/yyyy" displaydateformat="MM/dd/yyyy">
                                </dateinput>
                            </telerik:RadDatePicker>
                        </div>--%>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        Notes
                    </td>
                    <td align="left">
                        <asp:TextBox ID="tbNotes" runat="server" TextMode="MultiLine" Rows="3" Width="150px" />
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Button ID="btnRet" runat="server" Text="Return" OnClick="btnRet_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnOk" runat="server" Text="Cancel" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="mpe" runat="server" PopupControlID="ReasonPanel"
            TargetControlID="Panel1" OkControlID="btnOk" BackgroundCssClass="modalBackground">
        </ajaxToolkit:ModalPopupExtender>
        <div id="divBack" style="background-color: black; display: none; height: 100%; opacity: 0.5;
            position: fixed; top: 0; width: 100%; z-index: 7000; margin-left: -210px;">
        </div>
        <div id="BlockReturnsWarning" class="div_BlockReturnsWarningOuter">
            <div style="width: 400px; height: 30px; cursor: move;" onmousedown="mouse_down(event,'BlockReturnsWarning');"
                onmouseup="mouse_up(event,'BlockReturnsWarning');">
                <div style="width: 200px; float: left; padding-top: 10px;">
                    <span style="margin-left: 10px;">Warning Message!</span>
                </div>
                <div style="width: 200px; float: right; padding-top: 10px;">
                    <input id="btnCloseDeviceID" type="button" value="" onclick="return BlockReturnsPopup();"
                        style="background-image: url('../../App_Themes/Default/Images/btnPopupClose.png');
                        border: none; width: 48px; height: 21px; position: relative; margin-left: 147px;
                        margin-top: -11px; cursor: pointer;" />
                </div>
            </div>
            <div style="width: 400px; margin-top: 27px;">
                <div style="height: 10px; margin-left: 15px; right: -1px; width: 362px;">
                    <asp:Label ID="LblWarningMessage" runat="server" Text="This project is no longer accepting Returns. This Return will not be process. Please click OK to continue"
                        ForeColor="Red" Style="margin-left: 10px;"></asp:Label>
                </div>
            </div>
            <br />
            <br />
            <div style="width: 400px; margin-top: 55px;">
                <div style="width: 200px; float: right;">
                    <input id="cancel_BlockReturnsWarning" type="button" value="Ok" onclick="return BlockReturnsPopup();"
                        style="margin-left: -29px; margin-top: -22px; cursor: pointer;" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>