﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Entities;
using PowerShipping.ImportExport;
using Telerik.Web.UI;
using PowerShipping.WebSite.Controls;
using PowerShipping.Business.Presenters;

namespace PowerShipping.WebSite
{
    public partial class FedExRequest : Page
    {
        ConsumerRequestsPresenter presenter = new ConsumerRequestsPresenter();
        ShipmentsPresenter presenterShipments = new ShipmentsPresenter();

        public List<Guid> ListOfSelectedRequests
        {
            get { return (ViewState["ListOfSelectedRequests"] != null) ? (List<Guid>)ViewState["ListOfSelectedRequests"] : (new  List<Guid>()); }
            set { ViewState["ListOfSelectedRequests"] = value; }
        }

        public Shipment ShipmentCommonItem
        {
            get { return (ViewState["ShipmentCommon"] != null) ? (Shipment)ViewState["ShipmentCommon"] : null; }
            set { ViewState["ShipmentCommon"] = value; }
        }

        public Guid ProjectId
        {
            get { return (ViewState["ProjectId"] != null) ? (Guid)ViewState["ProjectId"] : new Guid(); }
            set { ViewState["ProjectId"] = value; }
        }

        public Guid ShipmentId
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_Id.Value))
                           ? new Guid(HiddenField_Id.Value)
                           : Guid.Empty;
            }
            set { HiddenField_Id.Value = value.ToString(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList1.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }

            ConsumerRequestList_Requests.OnNext += ConsumerRequestList_Requests_OnNext;

            if (!Page.IsPostBack)
                InitFirstStep();

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "FedEx Label Request");
            }
        }

        void ConsumerRequestList_Requests_OnNext(Guid projectId)
        {
            ProjectId = projectId;
            NextButton_Command(null, null);
        }

        private void InitFirstStep()
        {
            ((ConsumerRequestList)WizardMultiView.Views[0].FindControl("ConsumerRequestList_Requests")).InitData();
        }

        
        //protected void ToggleRowSelection(object sender, EventArgs e)
        //{
        //    ((sender as CheckBox).Parent.Parent as GridItem).Selected = (sender as CheckBox).Checked;
        //}

        //protected void RadGrid1_ItemCreated(object sender, GridItemEventArgs e)
        //{
        //    if (e.Item is GridDataItem)
        //    {
        //        e.Item.PreRender += new EventHandler(RadGrid1_ItemPreRender);
        //    }
        //}

        //private void RadGrid1_ItemPreRender(object sender, EventArgs e)
        //{
        //    ((sender as GridDataItem)["CheckBoxTemplateColumn"].FindControl("CheckBox1") as CheckBox).Checked = (sender as GridDataItem).Selected;
        //}

        //protected void ToggleSelectedState(object sender, EventArgs e)
        //{
        //    if ((sender as CheckBox).Checked)
        //    {
        //        foreach (GridDataItem dataItem in RadGrid_ConsumerRequests.MasterTableView.Items)
        //        {
        //            (dataItem.FindControl("CheckBox1") as CheckBox).Checked = true;
        //            dataItem.Selected = true;
        //        }
        //    }
        //    else
        //    {
        //        foreach (GridDataItem dataItem in RadGrid_ConsumerRequests.MasterTableView.Items)
        //        {
        //            (dataItem.FindControl("CheckBox1") as CheckBox).Checked = false;
        //            dataItem.Selected = false;
        //        }
        //    }
        //}

        #region MultiView

        protected void NextButton_Command(object sender, EventArgs e)
        {
            if (WizardMultiView.ActiveViewIndex > -1 & WizardMultiView.ActiveViewIndex < 2)
            {
                if (WizardMultiView.ActiveViewIndex == 0)
                {
                    ListOfSelectedRequests = ((ConsumerRequestList)WizardMultiView.Views[0].FindControl("ConsumerRequestList_Requests")).GetListOfSelectedItems();

                    if (ListOfSelectedRequests.Count == 0)
                        return;

                    ((ShipmentCommon)WizardMultiView.Views[1].FindControl("ShipmentCommon_Common")).InitData();

                }
                if (WizardMultiView.ActiveViewIndex == 1)
                {
                    ShipmentCommonItem = ((ShipmentCommon)WizardMultiView.Views[1].FindControl("ShipmentCommon_Common")).GetEntity();

                    try
                    {
                        lbError.Text = string.Empty;

                        //write to db
                        ShipmentId = presenter.SaveExportDate(ListOfSelectedRequests, ShipmentCommonItem, User.Identity.Name, ProjectId);

                        lbError.ControlStyle.ForeColor = System.Drawing.Color.Green;
                        lbError.Text = "FedEx Label Request generated successfully.";
                    }
                    catch (Exception ex)
                    {
                        lbError.ControlStyle.ForeColor = System.Drawing.Color.Red;
                        lbError.Text = ex.Message;
                    }
                }


                WizardMultiView.ActiveViewIndex += 1;
            }
            else if (WizardMultiView.ActiveViewIndex == 2)
            {
                //Response.Redirect("Shipments.aspx");
                Response.Redirect("LabelRequestSend.aspx?Id=" + ShipmentId);
            }
        }

        protected void BackButton_Command(object sender, EventArgs e)
        {
            if (WizardMultiView.ActiveViewIndex > 0 & WizardMultiView.ActiveViewIndex < 2)
            {
               
                WizardMultiView.ActiveViewIndex -= 1;
                lbError.Text = string.Empty;
            }
            else if (WizardMultiView.ActiveViewIndex == 2)
            {
                presenterShipments.Delete(ShipmentId);

                Response.Redirect("Shipments.aspx");
            }
        }

        protected void btnViewFile_OnClick(object sender, EventArgs e)
        {
            //write file to file system
            ExportResult res = presenter.GenerateExportDate(ShipmentId);
            string fileName = res.OutputFilePath.Replace(ConfigurationManager.AppSettings.Get("UploadFolder"), "");
            fileName = fileName.Trim('\\');


            Response.Redirect(@"~/Uploads/" + fileName);
        }

        #endregion

    }
}
