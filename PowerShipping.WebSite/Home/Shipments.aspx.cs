﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.ImportExport;
using PowerShipping.WebSite.Controls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class Shipments : System.Web.UI.Page
    {
        ShipmentsPresenter presenter = new ShipmentsPresenter();
        ConsumerRequestsPresenter presenterCR = new ConsumerRequestsPresenter();
        ShipmentDetailsPresenter presenterSD = new ShipmentDetailsPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList1.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }

            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Create Label Request");
            }
        }

        private void ProjectsList1_OnProjectChanged(string projectId)
        {
            RadGrid_Shipments.Rebind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            RadGrid_Shipments.Rebind();
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            tbPurchaseOrderNumberFilter.Text = null;
            dpStartDateFilter.SelectedDate = null;
            dpEndDateFilter.SelectedDate = null;
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            //Response.Redirect("FedExRequest.aspx");
            Session["LabelRequestReady"] = true;
            Response.Redirect("ConsumerRequests.aspx");
        }

        protected void RadGrid_Shipments_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            List<Shipment> list = GetFilteredList();
            //foreach (Shipment shipment in list)
            //{
            //    shipment.ShipmentDetailsCount = presenterSD.GetByShipmentId(shipment.Id).Count;
            //}
            RadGrid_Shipments.DataSource = list;
        }

        protected void RadGrid_Shipments_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                Button spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                LinkButton addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGrid_Shipments_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item is GridDataItem) && !e.Item.IsInEditMode)
            {
                GridDataItem gridItem = (GridDataItem)e.Item;
                try
                {
                    Double myDouble = Convert.ToDouble(gridItem["Phone1"].Text);
                    gridItem["Phone1"].Text = myDouble.ToString("###-###-####");
                }
                catch
                {
                }
            }
        }

        protected void RadGrid_Shipments_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                Label l = new Label();
                l.Text = "Unable to insert Shipment. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_Shipments.Controls.Add(l);
            }
        }

        protected void RadGrid_Shipments_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        GridEditFormItem item = (GridEditFormItem)e.Item;
                        Guid id = new Guid(item.GetDataKeyValue("Id").ToString());
                        if (id != Guid.Empty)
                        {
                            if (e.Item is GridEditableItem)
                            {
                                GridEditFormItem formItem = e.Item as GridEditFormItem;

                                Hashtable newValues = new Hashtable();
                                e.Item.OwnerTableView.ExtractValuesFromItem(newValues, formItem);

                                With.Transaction(() =>
                                    {
                                        //--- NEW
                                        object o = ((LabelRequestCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                                        Shipment shNew = (Shipment)o;

                                        Shipment sh = presenter.GetById(shNew.Id);

                                        sh.PurchaseOrderNumber = shNew.PurchaseOrderNumber;
                                        sh.ShipperId = shNew.ShipperId;
                                        sh.KitTypeId = shNew.KitTypeId;

                                        presenter.Save(sh);

                                        //----- ShipmentDetail -- //

                                        List<ShipmentDetail> objShipmentDetail = presenterSD.GetShipmentDetailByShipmentID(shNew.Id);

                                        foreach (ShipmentDetail shipmentDetail in objShipmentDetail)
                                        {
                                            shipmentDetail.ShipperId = shNew.ShipperId;
                                            presenterSD.Save(shipmentDetail);
                                        }
                                    });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                Label l = new Label();
                l.Text = "Unable to update OptOut. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_Shipments.Controls.Add(l);
            }
        }

        protected void RadGrid_Shipments_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.DeleteCommandName)
                {
                    //Guid id = new Guid((e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"].ToString());
                    //presenter.Delete(id);
                    //RadGrid_Shipments.Rebind();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                Label l = new Label();
                l.Text = "Unable to delete Shipment. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_Shipments.Controls.Add(l);
            }
        }

        protected void RadGrid_Shipments_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "Send")
            {
                Response.Redirect("LabelRequestSend.aspx?Id=" + (e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"]);
            }
            else if (e.CommandName == "Preview")
            {
                //Response.Redirect("ShipmentPreview.aspx?Id=" + (e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"]);

                ExportResult res = presenterCR.GenerateExportDate(new Guid((e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"].ToString()));
                string fileName = res.OutputFilePath.Replace(ConfigurationManager.AppSettings.Get("UploadFolder"), "");
                fileName = fileName.Trim('\\');

                Response.Redirect(@"~/Uploads/" + fileName);
            }
            else if (e.CommandName == RadGrid.EditCommandName)
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scroll", "setTimeout(function(){ Expand('" + item.ClientID + "');}, 100);", true);
                }
            }
        }

        private List<Shipment> GetFilteredList()
        {
            DateTime? startDate = dpStartDateFilter.SelectedDate;
            DateTime? endDate = dpEndDateFilter.SelectedDate;

            if (endDate != null)
                endDate = endDate.Value.AddDays(1);

            string poNum = tbPurchaseOrderNumberFilter.Text;

            if (poNum == string.Empty)
                poNum = null;

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList1.ProjectId;
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = ProjectsList1.UserId;
            }

            if (ProjectsList1.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList1.CompanyId;
            }

            return presenter.GetAll(startDate, endDate, poNum, userId, projectId, companyId);
        }
    }
}