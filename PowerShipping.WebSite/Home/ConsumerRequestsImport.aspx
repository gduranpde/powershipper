﻿<%@ Page Title="Consumer Requests Import" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" CodeBehind="ConsumerRequestsImport.aspx.cs" Inherits="PowerShipping.WebSite.ConsumerRequestsImport" Theme="Default"%>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<%@ Register src="../Controls/ProjectsList.ascx" tagname="ProjectsList" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


 <telerik:RadProgressManager ID="Radprogressmanager1" runat="server" />
 <div class="pageDiv">  
            
            <table>
                <tr>
                    <td>              
                          <h3>Consumer Requests Import</h3>
                    </td>
                    <td>
                         <uc1:ProjectsList ID="ProjectsList1" runat="server" />
                    </td>
                </tr>
               <%-- <tr>
                    <td colspan="2">
                        <asp:Literal runat="server" ID="help" Text="test"></asp:Literal>            
                    </td>
                </tr>     --%>       
                <tr>
                    <td>
                        <telerik:RadUpload ID="RadUpload1" runat="server" ControlObjectsVisibility="None" OnClientFileSelected="ProgressAreaVisibility"/>                        
                    </td>
                    <td>                                   
                    
                    </td>
                </tr>
            </table>
            
            <div class="submitArea">
                <asp:Button runat="server" ID="SubmitButton" Text="Upload files"
                onclick="SubmitButton_Click" />
            </div>

            <telerik:RadProgressArea runat="server" ID="ProgressArea1" Visible="false">
            <Localization Uploaded="File upload progress: " UploadedFiles="Imported records: " CurrentFileName="" 
            TotalFiles="Total records: " EstimatedTime="" TransferSpeed="" />
            </telerik:RadProgressArea>
            <br />
            <asp:HyperLink ID="hlExceptions" runat="server" NavigateUrl="" Visible="false" ></asp:HyperLink>
            <br />
            <asp:Label runat="server" ID="labelErrors" ForeColor="Red" />

    </div>          

</asp:Content>