﻿<%@ Page Title="Opt Outs Import" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" CodeBehind="OptOutsImport.aspx.cs" Inherits="PowerShipping.WebSite.Home.OptOutsImport" Theme="Default"%>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register src="../Controls/ProjectsList.ascx" tagname="ProjectsList" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


 <telerik:RadProgressManager ID="Radprogressmanager1" runat="server" />
 <div class="pageDiv">

            <table>
            <tr>
                    <td>              
                          <h3>Opt Outs Import</h3>
                    </td>
                    <td>
                         <uc1:ProjectsList ID="ProjectsList1" runat="server" />
                    </td>
                </tr>
                <%--<tr>
                    <td colspan="2">
                        <asp:Literal runat="server" ID="help" Text="test"></asp:Literal>            
                    </td>
                </tr>    --%>         
                <tr>
                    <td>
                        <telerik:RadUpload ID="RadUpload1" runat="server" ControlObjectsVisibility="None" />                        
                    </td>
                    <td>                                    
                       <%-- <div>
                            <asp:Label ID="labelNoResults" runat="server" Visible="True">No uploaded files yet</asp:Label>
            
                            <asp:Repeater ID="repeaterResults" runat="server" Visible="False">
                                <HeaderTemplate>
                                    <div class="title">Uploaded files in the target folder:</div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#DataBinder.Eval(Container.DataItem, "FileName")%>
                                    <%#DataBinder.Eval(Container.DataItem, "ContentLength").ToString() + " bytes"%>
                                    <br />
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>--%>
                    </td>
                </tr>
            </table>
            
            <div class="submitArea">
<asp:Button runat="server" ID="SubmitButton" Text="Upload files"
onclick="SubmitButton_Click" />
            </div>

            <telerik:RadProgressArea runat="server" ID="ProgressArea1">
             <Localization Uploaded="File upload progress: " UploadedFiles="Imported records: " CurrentFileName="" 
            TotalFiles="Total records: " EstimatedTime="" TransferSpeed="" />
            </telerik:RadProgressArea>
             <br />
            <asp:Label runat="server" ID="labelErrors" ForeColor="Red" />

    </div>

</asp:Content>