﻿using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Web.UI;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.ImportExport;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class OptOutsImport : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList1.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }

            //if (!Page.IsPostBack)
            //    this.help.Text = string.Format("<a href='../Help.aspx?type={0}' target='_blank'\">import help</a>", typeof(OptOut).ToString());//this.help.Text = string.Format("<a href='#1' onclick=\"window.open('../Help.aspx?type={0}' ,'height=800','width=550','resizable=no','scrollbars=yes','toolbar=no','status=no')\">import help</a>", typeof(OptOut).ToString());

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Opt Outs Import");
            }

            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;
            if (ProjectsList1.ProjectId == Guid.Empty)
                RadUpload1.Enabled = false;
        }

        void ProjectsList1_OnProjectChanged(string projectId)
        {
            if (ProjectsList1.ProjectId != Guid.Empty)
                RadUpload1.Enabled = true;
            else
                RadUpload1.Enabled = false;
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            //if (RadUpload1.UploadedFiles.Count > 0)
            //{
            //    repeaterResults.DataSource = RadUpload1.UploadedFiles;
            //    repeaterResults.DataBind();
            //    labelNoResults.Visible = false;
            //    repeaterResults.Visible = true;
            //}
            //else
            //{
            //    labelNoResults.Visible = true;
            //    repeaterResults.Visible = false;
            //}

            foreach (string fileInputID in Request.Files)
            {
                UploadedFile file = UploadedFile.FromHttpPostedFile(Request.Files[fileInputID]);
                if (file.ContentLength > 0)
                {
                    var path = Path.Combine(ConfigurationManager.AppSettings["UploadFolder"], file.GetName());
                    file.SaveAs(path);

                    if (ProjectsList1.ProjectId == Guid.Empty)
                    {
                        labelErrors.ForeColor = Color.Red;
                        labelErrors.Text = "Please select project.";
                        return;
                    }

                    var importer = new OptOutImporter(path, User.Identity.Name, ProjectsList1.ProjectId);
                    importer.ImportProgress += ShowImportProgress;
                    OptOutBatch batch = importer.Import();

                    labelErrors.Text = string.Empty;
                    
                    if (!importer.ImportErrors.IsEmpty)
                    {
                        labelErrors.ForeColor = Color.Red;
                        importer.ImportErrors.ForEach(err => labelErrors.Text += err + "<br>");
                    }
                    else
                    {
                        labelErrors.ForeColor = Color.Black;
                        labelErrors.Text = "Total opt outs: " + batch.OptOutsInBatch + ". <br/>Imported Successfully: " + batch.ImportedSuccessfully +
                                           ". <br/>Imported with exceptions: " + batch.ImportedWithException + ".";
                    }
                }
            }
        }

        private void ShowImportProgress(object sender, ImportExportProgressEventArgs e)
        {
            if (Response.IsClientConnected)
            {
                var progress = RadProgressContext.Current;
                progress.SecondaryTotal = e.Total;
                progress.SecondaryValue = e.Value;
                progress.SecondaryPercent = e.Total == 0
                                                ? 0
                                                : Convert.ToInt32((100*Convert.ToDouble(e.Value))/
                                                                  Convert.ToDouble(e.Total));
                progress.CurrentOperationText = e.Message;
            }
        }
    }
}