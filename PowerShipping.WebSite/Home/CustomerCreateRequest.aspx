﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Shared/Default.Master" CodeBehind="CustomerCreateRequest.aspx.cs" Inherits="PowerShipping.WebSite.Home.CustomerCreateRequest" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="../Controls/CustomerCreateRequestFilter.ascx" TagName="CustomerCreateRequestFilter" TagPrefix="uc1" %>
<%@ Register Src="../Controls/ProjectsList.ascx" TagName="ProjectsList" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

    <uc1:CustomerCreateRequestFilter ID="CustomerCreateRequestFilter1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <telerik:radprogressmanager id="Radprogressmanager1" runat="server" />
    <asp:HiddenField runat="server" ID="HiddenFiled_CustomQuery" />
    <div class="pageDiv">

        <table>
            <tr>
                <td colspan="3">
                    <h3>Create Consumer Request File</h3>
                </td>

            </tr>
            <tr>
                <td colspan="3">
                    <telerik:radupload id="RadUpload1" runat="server" controlobjectsvisibility="None" />
                </td>

            </tr>
            <tr>
                <td colspan="3">Note:
                    <asp:TextBox ID="tbSource" runat="server"></asp:TextBox>
                </td>

            </tr>
            <tr>
                <td>
                    <uc1:ProjectsList ID="ProjectsList1" runat="server" IsCompanyUse="false" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="submitArea">
                        <asp:Button runat="server" ID="SubmitButton" Text="Upload file"
                            OnClick="SubmitButton_Click" />
                    </div>
                    <telerik:radprogressarea runat="server" id="ProgressArea1">
            <Localization Uploaded="File upload progress: " UploadedFiles="Imported records: " CurrentFileName="" 
            TotalFiles="Total records: " EstimatedTime="" TransferSpeed="" />
            </telerik:radprogressarea>
                    <br />
                    <asp:HyperLink ID="hlExceptions" runat="server" NavigateUrl="" Visible="false"></asp:HyperLink>
                    <br />
                    <asp:Label runat="server" ID="labelErrors" ForeColor="Red" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <telerik:radgrid id="RadGrid_History" runat="server" width="100%" gridlines="None"
                        autogeneratecolumns="false" pagesize="15" allowsorting="True" allowpaging="True"
                        skin="Windows7"
                        onneeddatasource="RadGrid_History_NeedDataSource"
                        onitemcreated="RadGrid_History_ItemCreated"
                        onitemdatabound="RadGrid_History_ItemDataBound">
                <ExportSettings ExportOnlyData="true" IgnorePaging="true">            
                </ExportSettings>
                <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                 AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true">
                     <SortExpressions>
                          <telerik:GridSortExpression FieldName="UploadTime" SortOrder="Descending" />
                     </SortExpressions>
                <CommandItemSettings                   
                        ShowExportToWordButton="true"
                        ShowExportToExcelButton="true"
                        ShowExportToCsvButton="true"
                        ShowExportToPdfButton="true"
                        />
                <Columns>             
                    <telerik:GridBoundColumn DataField="ProjectName" HeaderText="Project Name" SortExpression="ProjectName"
                            UniqueName="ProjectName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                            EditFormColumnIndex="0">
                            </telerik:GridBoundColumn>
                      <telerik:GridBoundColumn DataField="FileName" HeaderText="File Name" SortExpression="FileName"
                            UniqueName="FileName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                      <telerik:GridBoundColumn DataField="Source" HeaderText="Note" SortExpression="Source"
                            UniqueName="Source" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                          <HeaderStyle Width="100px" />
                            </telerik:GridBoundColumn>
                      <telerik:GridBoundColumn DataField="UploadTime" HeaderText="Import Date" SortExpression="UploadTime"
                            UniqueName="UploadTime" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                      <telerik:GridBoundColumn DataField="TotalRecords" HeaderText="Count Records" SortExpression="TotalRecords"
                            UniqueName="TotalRecords" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                      <telerik:GridBoundColumn DataField="UserName" HeaderText="User Name" SortExpression="UserName"
                            UniqueName="UserName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>    
                            
                      
                      <telerik:GridTemplateColumn SortExpression="FileUploaded" HeaderText="Upload File" DataField="FileUploaded" DataType="System.String" UniqueName="FileUploaded">
                                <ItemTemplate>
                                <asp:HyperLink ID="hl" runat="server" Text="View" NavigateUrl="<%# ((PowerShipping.Entities.CustomerCreateRequestHistory)Container.DataItem).FileUploaded %>"></asp:HyperLink>
                                 <%--   <asp:LinkButton ID="ViewUploaded" runat="server" Text="View" PostBackUrl="<%# ((PowerShipping.Entities.CustomerCreateRequestHistory)Container.DataItem).FileUploaded %>"></asp:LinkButton>--%>
                                   </ItemTemplate>                                
                      </telerik:GridTemplateColumn>    
                      <telerik:GridTemplateColumn SortExpression="FileExceptions" HeaderText="Exception File" DataField="FileExceptions" DataType="System.String" UniqueName="FileExceptions">
                                <ItemTemplate>
                                <asp:HyperLink ID="hl" runat="server" Text="View" NavigateUrl="<%# ((PowerShipping.Entities.CustomerCreateRequestHistory)Container.DataItem).FileExceptions %>"></asp:HyperLink>
                                    <%--<asp:LinkButton ID="ViewExceptions" runat="server" Text="View"  PostBackUrl="<%# ((PowerShipping.Entities.CustomerCreateRequestHistory)Container.DataItem).FileExceptions %>"></asp:LinkButton>--%>
                                   </ItemTemplate>                                
                      </telerik:GridTemplateColumn>
                      <telerik:GridTemplateColumn SortExpression="FileGood" HeaderText="CR File" DataField="FileGood" DataType="System.String" UniqueName="FileGood">
                                <ItemTemplate>
                                <asp:HyperLink ID="hl" runat="server" Text="View" NavigateUrl="<%# ((PowerShipping.Entities.CustomerCreateRequestHistory)Container.DataItem).FileGood %>"></asp:HyperLink>
                             <%--       <asp:LinkButton ID="ViewGood" runat="server" Text="View"  PostBackUrl="<%# ((PowerShipping.Entities.CustomerCreateRequestHistory)Container.DataItem).FileGood %>"></asp:LinkButton>--%>
                                   </ItemTemplate>                                
                      </telerik:GridTemplateColumn>                        
                </Columns>               
                </MasterTableView>
            </telerik:radgrid>
                </td>
            </tr>
        </table>

    </div>
</asp:Content>
