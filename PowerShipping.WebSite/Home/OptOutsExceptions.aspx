﻿<%@ Page Title="OptOut Exceptions" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" CodeBehind="OptOutsExceptions.aspx.cs" Inherits="PowerShipping.WebSite.OptOutsExceptions" Theme="Default"%>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register src="../Controls/ProjectsList.ascx" tagname="ProjectsList" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<div class="pageDiv">
    
    
     <table>
     <tr>
            <td>              
                <h3>OptOut Exceptions</h3>
            </td>
            <td colspan=5 align="center">
                 <uc1:ProjectsList ID="ProjectsList1" runat="server" IsCompanyUse="true"/>
            </td>
        </tr>
        <tr>
        
            <td>
                Account Number 
                <asp:TextBox ID="tbAccountNumberFilter" runat="server"></asp:TextBox>         
            </td>
            <td>
                Account Name
                <asp:TextBox ID="tbAccountNameFilter" runat="server"></asp:TextBox>
            </td>
            <td>
                Phone Number
                <asp:TextBox ID="tbPhoneFilter" runat="server"></asp:TextBox>
            </td>
            <td>
                Address
                <asp:TextBox ID="tbAddressFilter" runat="server"></asp:TextBox>
            </td>            
            <td>
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" CssClass="ptButton"/>
            </td>
            <td>
                <asp:Button ID="btnClearFilter" runat="server" OnClick="btnClearFilter_Click" Text="Clear" CssClass="ptButton"/>
            </td>
        </tr>
    </table>
    
    
   
    <telerik:RadGrid ID="RadGrid_OptOutsEx" runat="server" Width="100%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True" 
              Skin="Windows7" 
        onneeddatasource="RadGrid_OptOutsEx_NeedDataSource"        
        onupdatecommand="RadGrid_OptOutsEx_UpdateCommand"
        OnDeleteCommand="RadGrid_OptOutsEx_DeleteCommand"
        OnItemCreated="RadGrid_OptOutsEx_ItemCreated"
        OnItemDataBound="RadGrid_OptOutsEx_ItemDataBound" 
        OnItemCommand="RadGrid_OptOutsEx_ItemCommand"
        >
        <ExportSettings ExportOnlyData="true" IgnorePaging="true">            
            </ExportSettings>
            <ClientSettings >
                        <Resizing AllowColumnResize="True">
                        </Resizing> 
                        <Selecting AllowRowSelect="true" />                      
            </ClientSettings>
            <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
            AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true">
            <CommandItemSettings
                    ShowExportToWordButton="true"
                    ShowExportToExcelButton="true"
                    ShowExportToCsvButton="true"
                    ShowExportToPdfButton="true"
                    />
            <Columns>             
                <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" >
                <HeaderStyle Width="30px"/>
                    </telerik:GridEditCommandColumn>
              <%--  <telerik:GridButtonColumn UniqueName="DeleteColumn" Text="Delete" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmText="Are you sure delete this row?">
                <HeaderStyle Width="45px"/>
                </telerik:GridButtonColumn>--%>
                 
                 <telerik:GridBoundColumn DataField="OptOutDate" HeaderText="OptOutDate" SortExpression="OptOutDate"
                        UniqueName="OptOutDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        EditFormColumnIndex="0">
                        <HeaderStyle Width="80px"/>
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="AccountNumber" SortExpression="AccountNumber"
                        UniqueName="AccountNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        <HeaderStyle Width="100px"/>
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="AccountName" HeaderText="AccountName" SortExpression="AccountName"
                        UniqueName="AccountName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="Address" HeaderText="Address" SortExpression="Address"
                        UniqueName="Address" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="City" HeaderText="City" SortExpression="City"
                        UniqueName="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="State" HeaderText="State" SortExpression="State"
                        UniqueName="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        <HeaderStyle Width="40px"/>
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="ZipCode" HeaderText="ZipCode" SortExpression="ZipCode"
                        UniqueName="ZipCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        <HeaderStyle Width="80px"/>
                        </telerik:GridBoundColumn>
                   <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email"
                        UniqueName="Email" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>                                         
                                   
                  <telerik:GridMaskedColumn DataField="Phone1" HeaderText="Phone 1" SortExpression="Phone1" Mask="###-###-####"
                        UniqueName="Phone1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        DataFormatString="{0:(###)###-####}">   
                        <HeaderStyle Width="85px"/>                 
                    </telerik:GridMaskedColumn> 
                  <telerik:GridMaskedColumn DataField="Phone2" HeaderText="Phone 2" SortExpression="Phone2" Mask="###-###-####" 
                        UniqueName="Phone2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        DataFormatString="{0:(###)###-####}">                    
                    </telerik:GridMaskedColumn> 
                  
                  <%--<telerik:GridBoundColumn DataField="IsEnergyProgram" HeaderText="Energy Opt Out" SortExpression="IsEnergyProgram"
                        UniqueName="IsEnergyProgram" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        <HeaderStyle Width="100px"/>
                  </telerik:GridBoundColumn> 
                    <telerik:GridBoundColumn DataField="IsTotalDesignation" HeaderText="Total Opt Out" SortExpression="IsTotalDesignation"
                        UniqueName="IsTotalDesignation" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        <HeaderStyle Width="105px"/>
                  </telerik:GridBoundColumn> --%>
                  <telerik:GridCheckBoxColumn DataField="IsEnergyProgram" HeaderText="Program Opt Out" SortExpression="IsEnergyProgram" ItemStyle-HorizontalAlign="Center"
                        UniqueName="IsEnergyProgram" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true" DefaultInsertValue="true">
                        <HeaderStyle Width="65px"/>
                    </telerik:GridCheckBoxColumn>
                   <telerik:GridCheckBoxColumn DataField="IsTotalDesignation" HeaderText="Total Opt Out" SortExpression="IsTotalDesignation"
                        UniqueName="IsTotalDesignation" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true">
                        <HeaderStyle Width="90px"/>
                   </telerik:GridCheckBoxColumn>
                                    
                   <telerik:GridBoundColumn DataField="Reason" HeaderText="Reason" SortExpression="Reason" ReadOnly="true"
                        UniqueName="Reason" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                                   
                   <telerik:GridBoundColumn DataField="Notes" HeaderText="Notes" SortExpression="Notes" UniqueName="Notes">
                        </telerik:GridBoundColumn>  
                        
                                         
                  <telerik:GridTemplateColumn UniqueName="Status" DataField="Status" HeaderText="Status">
                       <ItemTemplate>
                            <%# Eval("Status")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList runat="server" ID="ddlStatus" DataValueField="Value" DataTextField="Text">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <HeaderStyle Width="60px"/>
                    </telerik:GridTemplateColumn>
                    
                     <telerik:GridBoundColumn DataField="CompanyCode" HeaderText="CompanyCode" SortExpression="CompanyCode"
                                    UniqueName="CompanyCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">                                
                                </telerik:GridBoundColumn> 
                          <telerik:GridBoundColumn DataField="ProjectCode" HeaderText="ProjectCode" SortExpression="ProjectCode"
                                    UniqueName="ProjectCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">                                
                                </telerik:GridBoundColumn>   
                 
            </Columns>            
            <EditFormSettings UserControlName="../Controls/OptOutExceptionCard.ascx" EditFormType="WebUserControl">
                <EditColumn UniqueName="EditCommandColumn1">
                </EditColumn>
             </EditFormSettings>
            </MasterTableView>
    </telerik:RadGrid>
    
     <br />
    <table>
    <tr>
         <td>
                <asp:Button ID="btnValidate" runat="server" OnClick="btnValidate_Click" Text="ReSubmit" CssClass="ptButton"/>
            </td>
       </tr>
    <tr>        
            <td>
                <asp:Label ID="lbReuslt" runat="server" Text="" />
            </td>
        </tr>
    </table>
    
     <script type="text/javascript">

        function Expand(itemID) {
            var Grid = $find('<%=RadGrid_OptOutsEx.ClientID %>');
            var scrollArea = document.getElementById('ctl00_ContentPlaceHolder1_RadGrid_OptOutsEx')
            var rowElement = document.getElementById(itemID);
            window.scrollTo(0, rowElement.offsetTop + 200);
        }
        </script>
    
    </div>
</asp:Content>