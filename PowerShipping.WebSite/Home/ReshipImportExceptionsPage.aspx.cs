﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PowerShipping.Business.Presenters;
using PowerShipping.Data;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class ReshipImportExceptionsPage : System.Web.UI.Page
    {
        PowerShipping.Business.Presenters.ReshipImportBatchExceptions presenter = new PowerShipping.Business.Presenters.ReshipImportBatchExceptions();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "ReshipImportExceptionsPage");
            }
            if (Request.QueryString["BatchName"] != null)
            {
                ReshipImportBatchesFilter1.BatchName = Request.QueryString["BatchName"];
            }

            Label lblFilterTitle = (Label)ReshipImportBatchesFilter1.FindControl("LblReshipImportFilter");
            lblFilterTitle.Text = "Reship Import Exception";
            ReshipImportBatchesFilter1.OnFilter += ReshipImportBatchesFilter1_OnFilter;
        }

        protected void RadGrid_Batches_Exceptions_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_Batches_Exceptions.DataSource = GetFilteredList();
        }

        private List<PowerShipping.Entities.ReshipImportException> GetFilteredList()
        {
            string sBatchName = ReshipImportBatchesFilter1.BatchName;
            string sImportedBy = ReshipImportBatchesFilter1.ImportedBy;
            DateTime? FromDate = ReshipImportBatchesFilter1.FromImportDate;
            DateTime? ToDate = ReshipImportBatchesFilter1.ToImportDate;

            if (sBatchName == string.Empty)
            { sBatchName = null; }

            if (sImportedBy == string.Empty)
            { sImportedBy = null; }

            Guid? shipperID = null;

            if (ReshipImportBatchesFilter1.ShipperID != Guid.Empty)
            {
                shipperID = ReshipImportBatchesFilter1.ShipperID;
            }

            return presenter.GetReshipImportBatches(sBatchName, sImportedBy, shipperID, FromDate, ToDate);
        }

        private void ReshipImportBatchesFilter1_OnFilter()
        {
            RadGrid_Batches_Exceptions.Rebind();
        }

        protected void RadGrid_Batches_Exceptions_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                var AddButton_RadGrid = e.Item.FindControl("AddNewRecordButton") as Button;
                AddButton_RadGrid.Visible = false;
                var addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }
    }
}