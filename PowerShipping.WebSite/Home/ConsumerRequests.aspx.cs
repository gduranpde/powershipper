﻿using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.ImportExport;
using PowerShipping.Logging;
using PowerShipping.WebSite.Controls;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite
{
    public partial class ConsumerRequests : Page
    {
        ConsumerRequestsPresenter presenter = new ConsumerRequestsPresenter();
        UserManagementPresenter presenterAdv = new UserManagementPresenter();

        bool isExport = false;

        private bool _isFirstLoad = false;

        protected void Page_Init(object sender, EventArgs e)
        {
            ConsumerRequestFilter1.OnFilter += ConsumerRequestFilter1_OnFilter;
            ConsumerRequestFilter1.OnClear += ConsumerRequestFilter1_OnClear;
            Session["NewLabelRequestIds"] = null;
            Session["NewLabelRequestProjectId"] = null;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Consumer Requests");
                _isFirstLoad = true;    

                ConsumerRequestFilter1.ForLabelRequest = false;
                if (Session["LabelRequestReady"] != null)
                {
                    Session["LabelRequestReady"] = null;

                    ConsumerRequestFilter1.ForLabelRequest = true;
                    _isFirstLoad = false;
                }
            }

            //ConsumerRequestFilter1.OnFilter += ConsumerRequestFilter1_OnFilter;
            //ConsumerRequestFilter1.OnClear += ConsumerRequestFilter1_OnClear;

            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;

            if (!ConsumerRequestFilter1.FillView)
            {
                RadGrid_ConsumerRequests.Columns.FindByDataField("Address2").Visible = false;
                RadGrid_ConsumerRequests.Columns.FindByDataField("Email").Visible = false;
                RadGrid_ConsumerRequests.Columns.FindByDataField("Phone1").Visible = false;
                RadGrid_ConsumerRequests.Columns.FindByDataField("Phone2").Visible = false;
                RadGrid_ConsumerRequests.Columns.FindByDataField("Method").Visible = false;
                RadGrid_ConsumerRequests.Columns.FindByDataField("AuditFailureDate").Visible = false;
                RadGrid_ConsumerRequests.Columns.FindByDataField("WaterHeaterFuel").Visible = false;
                RadGrid_ConsumerRequests.Columns.FindByDataField("HeaterFuel").Visible = false;
                //PG31
                RadGrid_ConsumerRequests.Columns.FindByDataField("FacilityType").Visible = false;
                RadGrid_ConsumerRequests.Columns.FindByDataField("RateCode").Visible = false;
                RadGrid_ConsumerRequests.Columns.FindByDataField("IncomeQualified").Visible = false;
                //PG31
                RadGrid_ConsumerRequests.Columns.FindByDataField("Notes").Visible = false;
                RadGrid_ConsumerRequests.Columns.FindByDataField("BatchLabel").Visible = false;
                RadGrid_ConsumerRequests.Columns.FindByDataField("OutOfState").Visible = false;
                RadGrid_ConsumerRequests.Columns.FindByDataField("Quantity").Visible = false;
                RadGrid_ConsumerRequests.Columns.FindByDataField("PremiseID").Visible = false;

                RadGrid_ConsumerRequests.Columns.FindByDataField("CompanyName").Visible = false;
                RadGrid_ConsumerRequests.Columns.FindByDataField("ServiceAddress1").Visible = false;
                RadGrid_ConsumerRequests.Columns.FindByDataField("ServiceAddress2").Visible = false;
                RadGrid_ConsumerRequests.Columns.FindByDataField("ServiceCity").Visible = false;
                RadGrid_ConsumerRequests.Columns.FindByDataField("ServiceState").Visible = false;
                RadGrid_ConsumerRequests.Columns.FindByDataField("ServiceZip").Visible = false;
                RadGrid_ConsumerRequests.Columns.FindByDataField("AccountNumberOld").Visible = false;
                RadGrid_ConsumerRequests.Columns.FindByDataField("RequestedKit").Visible = false;
                //RadGrid_ConsumerRequests.ClientSettings.Scrolling.AllowScroll = false;

                List<double> widths = new List<double>();

                if (ViewState["ColumnWidths"] != null)
                    widths = (List<double>)ViewState["ColumnWidths"];

                if (widths.Count > 0)
                {
                    for (int i = 0; i < RadGrid_ConsumerRequests.Columns.Count; i++)
                    {
                        RadGrid_ConsumerRequests.Columns[i].HeaderStyle.Width = (Unit)widths[i];
                    }
                }
                else
                {
                    widths.Clear();
                    foreach (GridColumn gc in RadGrid_ConsumerRequests.Columns)
                    {
                        widths.Add(gc.HeaderStyle.Width.Value);
                    }
                    ViewState["ColumnWidths"] = widths;
                }
            }
            else
            {
                RadGrid_ConsumerRequests.Columns.FindByDataField("Address2").Visible = true;
                RadGrid_ConsumerRequests.Columns.FindByDataField("Email").Visible = true;
                RadGrid_ConsumerRequests.Columns.FindByDataField("Phone1").Visible = true;
                RadGrid_ConsumerRequests.Columns.FindByDataField("Phone2").Visible = true;
                RadGrid_ConsumerRequests.Columns.FindByDataField("Method").Visible = true;
                RadGrid_ConsumerRequests.Columns.FindByDataField("AuditFailureDate").Visible = true;
                RadGrid_ConsumerRequests.Columns.FindByDataField("WaterHeaterFuel").Visible = true;
                RadGrid_ConsumerRequests.Columns.FindByDataField("HeaterFuel").Visible = true;
                //PG31
                RadGrid_ConsumerRequests.Columns.FindByDataField("FacilityType").Visible = true;
                RadGrid_ConsumerRequests.Columns.FindByDataField("RateCode").Visible = true;
                RadGrid_ConsumerRequests.Columns.FindByDataField("IncomeQualified").Visible = true;
                //PG31
                RadGrid_ConsumerRequests.Columns.FindByDataField("Notes").Visible = true;
                RadGrid_ConsumerRequests.Columns.FindByDataField("BatchLabel").Visible = true;
                RadGrid_ConsumerRequests.Columns.FindByDataField("OutOfState").Visible = true;
                RadGrid_ConsumerRequests.Columns.FindByDataField("Quantity").Visible = true;
                RadGrid_ConsumerRequests.Columns.FindByDataField("PremiseID").Visible = true;

                RadGrid_ConsumerRequests.Columns.FindByDataField("CompanyName").Visible = true;
                RadGrid_ConsumerRequests.Columns.FindByDataField("ServiceAddress1").Visible = true;
                RadGrid_ConsumerRequests.Columns.FindByDataField("ServiceAddress2").Visible = true;
                RadGrid_ConsumerRequests.Columns.FindByDataField("ServiceCity").Visible = true;
                RadGrid_ConsumerRequests.Columns.FindByDataField("ServiceState").Visible = true;
                RadGrid_ConsumerRequests.Columns.FindByDataField("ServiceZip").Visible = true;
                RadGrid_ConsumerRequests.Columns.FindByDataField("AccountNumberOld").Visible = true;
                RadGrid_ConsumerRequests.Columns.FindByDataField("RequestedKit").Visible = true;

                //RadGrid_ConsumerRequests.ClientSettings.Scrolling.AllowScroll = true;

                foreach (GridColumn gc in RadGrid_ConsumerRequests.Columns)
                {
                    gc.HeaderStyle.Width = 100;
                }
            }
        }

        private void ProjectsList1_OnProjectChanged(string projectId)
        {
            HiddenFiled_CustomQuery.Value = ConsumerRequestFilter1.CurrentQuery;
            RadGrid_ConsumerRequests_NeedDataSource(null, null);
            RadGrid_ConsumerRequests.Rebind();
        }

        private void ConsumerRequestFilter1_OnFilter(string qry)
        {
            HiddenFiled_CustomQuery.Value = qry;
            RadGrid_ConsumerRequests_NeedDataSource(null, null);
            RadGrid_ConsumerRequests.Rebind();
        }

        private void ConsumerRequestFilter1_OnClear(string qry)
        {
            HiddenFiled_CustomQuery.Value = qry;
        }

        protected void RadGrid_ConsumerRequests_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item is GridDataItem) && !e.Item.IsInEditMode)
            {
                GridDataItem gridItem = (GridDataItem)e.Item;
                try
                {
                    Double myDouble = Convert.ToDouble(gridItem["Phone1"].Text);
                    gridItem["Phone1"].Text = myDouble.ToString("###-###-####");
                }
                catch
                {
                }
            }
            if (e.Item is GridDataItem && isExport)
            {
                TableCell cell = (e.Item as GridDataItem)["AccountNumber"];
                LinkButton lb = cell.FindControl("EditButton") as LinkButton;
                cell.Text = lb.Text;
            }
        }

        protected void RadGrid_ConsumerRequests_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.EditCommandName)
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scroll", "setTimeout(function(){ Expand('" + item.ClientID + "');}, 100);", true);
                }
            }
            if (e.CommandName == RadGrid.ExportToCsvCommandName || e.CommandName == RadGrid.ExportToExcelCommandName
                || e.CommandName == RadGrid.ExportToWordCommandName || e.CommandName == RadGrid.ExportToPdfCommandName)
            {
                isExport = true;
            }
        }

        protected void RadGrid_ConsumerRequests_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                Button spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = Common.IsCanEdit();

                LinkButton addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = Common.IsCanEdit();
            }
        }

        protected void RadGrid_ConsumerRequests_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            string userId = "";
            string projectId = "";

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = " AND ProjectId = '" + ProjectsList1.ProjectId + "'";
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = " AND UserId = '" + ProjectsList1.UserId + "'";
            }

            if (!_isFirstLoad)
            {
                if (!string.IsNullOrEmpty(HiddenFiled_CustomQuery.Value))
                {
                    string query = HiddenFiled_CustomQuery.Value;

                    query = GetQuery(userId, projectId, query);

                    RadGrid_ConsumerRequests.DataSource = presenter.GetFiltered(query);
                }
                //else
                //{
                //    string query = ConsumerRequestFilter1.DefaultQuery;

                //    query = GetQuery(userId, projectId, query);

                //    RadGrid_ConsumerRequests.DataSource = presenter.GetFiltered(query);
                //}
            }
        }

        private string GetQuery(string userId, string projectId, string query)
        {
            if (!string.IsNullOrEmpty(userId)) //customer (not admin or user)
            {
                query = query.Replace("[vw_ConsumerRequest]", " [vw_ConsumerRequest]  AS CR INNER JOIN dbo.UserProject up ON up.ProjectId = cr.ProjectId");

                if (!string.IsNullOrEmpty(projectId)) // selected project (not All projects)
                {
                    query = query + " AND cr.ProjectId = '" + ProjectsList1.ProjectId + "'";
                }

                query = query + " AND UserId = '" + ProjectsList1.UserId + "'";
            }
            else
            {
                if (!string.IsNullOrEmpty(projectId)) // selected project (not All projects)
                {
                    query = query + " AND ProjectId = '" + ProjectsList1.ProjectId + "'";
                }
                else
                {
                    if (ProjectsList1.CompanyId != Guid.Empty)
                    {
                        List<Project> pList = presenterAdv.GetProjectByCompany(ProjectsList1.CompanyId);
                        if (pList.Count != 0)
                        {
                            query = query + " AND ProjectId IN (";
                            foreach (Project p in pList)
                            {
                                query = query + "'" + p.Id + "',";
                            }
                            query = query.TrimEnd(',');
                            query = query + ")";
                        }
                    }
                }
            }
            return query;
        }

        protected void RadGrid_ConsumerRequests_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.PerformInsertCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        if (e.Item is GridEditFormInsertItem)
                        {
                            ConsumerRequest cr = new ConsumerRequest();

                            cr.Id = Guid.NewGuid();

                            object o = ((ConsumerRequestCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                            ConsumerRequest crNew = (ConsumerRequest)o;

                            cr.AccountNumber = crNew.AccountNumber;
                            cr.AccountName = crNew.AccountName;
                            cr.Address1 = crNew.Address1;
                            cr.Address2 = crNew.Address2;
                            cr.City = crNew.City;
                            cr.State = crNew.State;
                            cr.ZipCode = crNew.ZipCode;
                            cr.Email = crNew.Email;
                            cr.Phone1 = crNew.Phone1;
                            cr.Phone2 = crNew.Phone2;
                            cr.Method = crNew.Method;

                            cr.CompanyName = crNew.CompanyName;
                            cr.ServiceAddress1 = crNew.ServiceAddress1;
                            cr.ServiceAddress2 = crNew.ServiceAddress2;
                            cr.ServiceCity = crNew.ServiceCity;
                            cr.ServiceState = crNew.ServiceState;
                            cr.ServiceZip = crNew.ServiceZip;

                            cr.IsReship = crNew.IsReship;
                            cr.DoNotShip = crNew.DoNotShip;
                            cr.IsOkayToContact = crNew.IsOkayToContact;

                            cr.AnalysisDate = crNew.AnalysisDate;
                            cr.ReceiptDate = crNew.ReceiptDate;
                            cr.CRImportedDate = crNew.CRImportedDate;
                            cr.AuditFailureDate = crNew.AuditFailureDate;
                            //PG31
                            cr.FacilityType = crNew.FacilityType;
                            cr.IncomeQualified = crNew.IncomeQualified;
                            cr.RateCode = crNew.RateCode;
                            //PG31
                            cr.WaterHeaterFuel = crNew.WaterHeaterFuel;
                            cr.HeaterFuel = crNew.HeaterFuel;
                            cr.OperatingCompany = crNew.OperatingCompany;
                            cr.Notes = crNew.Notes;
                            cr.Status = crNew.Status;
                            cr.OutOfState = crNew.OutOfState;

                            cr.Quantity = crNew.Quantity;
                            cr.PremiseID = crNew.PremiseID;

                            cr.AccountNumberOld = crNew.AccountNumberOld;
                            cr.RequestedKit = crNew.RequestedKit;
                            cr.ProjectId = crNew.ProjectId;

                            if (cr.ProjectId == Guid.Empty)
                            {
                                //Label l = new Label();
                                //l.Text = "Project must be selected.";
                                //l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                                //RadGrid_ConsumerRequests.Controls.Add(l);
                                ErrorShow("Project must be selected.");
                                return;
                            }

                            cr.States = presenterAdv.GetStatesByProject(cr.ProjectId);

                            //TODO: user
                            cr.ChangedBy = Membership.GetUser().UserName;

                            ValidationResult validationResult;

                            if ((cr.Status == ConsumerRequestStatus.OptOut) || (cr.Status == ConsumerRequestStatus.Exception))
                            {
                                validationResult = EntityValidation.Validate(cr, true);

                                //UserManagementPresenter presenter = new UserManagementPresenter();

                                //Project p = presenter.GetProjectById(cr.ProjectId);
                            }
                            else
                            {
                                validationResult = EntityValidation.Validate(cr);
                            }

                            if (validationResult.Success)
                            {
                                presenter.Save(cr);
                                RadGrid_ConsumerRequests.Rebind();
                            }
                            else
                            {
                                //Label l = new Label();
                                //l.Text = validationResult.Errors.ToString();
                                //l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                                //RadGrid_ConsumerRequests.Controls.Add(l);

                                ErrorShow(validationResult.Errors.ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CurrentLogger.Log.Error(ex);
                Log.LogError(ex);

                //Label l = new Label();
                //l.Text = "Unable to Inesert ConsumerRequests. Reason: " + ex.Message;
                //l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                //RadGrid_ConsumerRequests.Controls.Add(l);

                ErrorShow("Unable to Inesert ConsumerRequests. Reason: " + ex.Message);
            }
        }

        protected void RadGrid_ConsumerRequests_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        GridEditFormItem item = (GridEditFormItem)e.Item;
                        Guid id = new Guid(item.GetDataKeyValue("Id").ToString());
                        if (id != Guid.Empty)
                        {
                            if (e.Item is GridEditableItem)
                            {
                                object o = ((ConsumerRequestCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                                ConsumerRequest crNew = (ConsumerRequest)o;

                                ConsumerRequest cr = presenter.GetById(crNew.Id);

                                cr.AccountNumber = crNew.AccountNumber;
                                cr.AccountName = crNew.AccountName;
                                cr.Address1 = crNew.Address1;
                                cr.Address2 = crNew.Address2;
                                cr.City = crNew.City;
                                cr.State = crNew.State;
                                cr.ZipCode = crNew.ZipCode;
                                cr.Email = crNew.Email;
                                cr.Phone1 = crNew.Phone1;
                                cr.Phone2 = crNew.Phone2;

                                cr.CompanyName = crNew.CompanyName;
                                cr.ServiceAddress1 = crNew.ServiceAddress1;
                                cr.ServiceAddress2 = crNew.ServiceAddress2;
                                cr.ServiceCity = crNew.ServiceCity;
                                cr.ServiceState = crNew.ServiceState;
                                cr.ServiceZip = crNew.ServiceZip;

                                cr.IsReship = crNew.IsReship;
                                cr.DoNotShip = crNew.DoNotShip;
                                cr.IsOkayToContact = crNew.IsOkayToContact;

                                cr.AnalysisDate = crNew.AnalysisDate;
                                cr.ReceiptDate = crNew.ReceiptDate;
                                cr.CRImportedDate = crNew.CRImportedDate;
                                cr.AuditFailureDate = crNew.AuditFailureDate;

                                cr.WaterHeaterFuel = crNew.WaterHeaterFuel;
                                cr.HeaterFuel = crNew.HeaterFuel;
                                //PG31
                                cr.FacilityType = crNew.FacilityType;
                                cr.IncomeQualified = crNew.IncomeQualified;
                                cr.RateCode = crNew.RateCode;
                                //PG31
                                cr.OperatingCompany = crNew.OperatingCompany;
                                cr.Notes = crNew.Notes;
                                cr.Status = crNew.Status;
                                cr.OutOfState = crNew.OutOfState;

                                cr.Method = crNew.Method;

                                cr.Quantity = crNew.Quantity;
                                cr.PremiseID = crNew.PremiseID;

                                cr.AccountNumberOld = crNew.AccountNumberOld;
                                cr.RequestedKit = crNew.RequestedKit;
                                //if (ProjectsList1.ProjectId == Guid.Empty)
                                //{
                                //    Label l = new Label();
                                //    l.Text = "Please select project in list.";
                                //    l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                                //    RadGrid_ConsumerRequests.Controls.Add(l);

                                //    return;
                                //}

                                //cr.ProjectId = ProjectsList1.ProjectId;

                                cr.ProjectId = crNew.ProjectId;

                                //for validation
                                cr.States = presenterAdv.GetStatesByProject(cr.ProjectId);

                                //TODO: user
                                cr.ChangedBy = Membership.GetUser().UserName;

                                //if(cr.ReceiptDate == null)
                                //{
                                //    ErrorShow("Receipt Date didn't fill.");
                                //    return;
                                //}

                                ValidationResult validationResult;

                                if ((cr.Status == ConsumerRequestStatus.OptOut) || (cr.Status == ConsumerRequestStatus.Exception))
                                    validationResult = EntityValidation.Validate(cr, true);
                                else
                                    validationResult = EntityValidation.Validate(cr);

                                if (validationResult.Success)
                                {
                                    presenter.Save(cr);
                                    RadGrid_ConsumerRequests.Rebind();
                                }
                                else
                                {
                                    //Label l = new Label();
                                    //l.Text = validationResult.Errors.ToString();
                                    //l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                                    //RadGrid_ConsumerRequests.Controls.Add(l);
                                    ErrorShow(validationResult.Errors.ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CurrentLogger.Log.Error(ex);
                Log.LogError(ex);

                //Label l = new Label();
                //l.Text = "Unable to update ConsumerRequests. Reason: " + ex.Message;
                //l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                //RadGrid_ConsumerRequests.Controls.Add(l);
                ErrorShow("Unable to update ConsumerRequests. Reason: " + ex.Message);
            }
        }

        protected void RadGrid_ConsumerRequests_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.DeleteCommandName)
                {
                    //Guid id = new Guid((e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"].ToString());
                    //ConsumerRequest cr = presenter.GetById(id);
                    //cr.Status = ConsumerRequestStatus.Deleted;

                    //presenter.Save(cr);
                    //RadGrid_ConsumerRequests.Rebind();
                }
            }
            catch (Exception ex)
            {
                CurrentLogger.Log.Error(ex);
                Log.LogError(ex);
                Label l = new Label();
                l.Text = "Unable to update ConsumerRequests. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_ConsumerRequests.Controls.Add(l);
            }
        }

        protected void Button_ClearAllInfo_Click(object sender, EventArgs e)
        {
            presenter.ClearAllInfo();
            Response.Redirect(Request.Url.ToString());
        }

        protected void LinkButton_EditButton_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            if (lb != null)
            {
                int itemIndex = Convert.ToInt32(lb.CommandName);
                GridEditableItem editableItem = (GridEditableItem)this.RadGrid_ConsumerRequests.Items[itemIndex];
                editableItem.Edit = true;
                this.RadGrid_ConsumerRequests.Rebind();
            }
        }

        protected void btnCreateRequest_OnClick(object sender, EventArgs e)
        {
            bool allRequestRecived = true;
            List<ConsumerRequest> list = new List<ConsumerRequest>();

            string userId = "";
            string projectId = "";

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = " AND ProjectId = '" + ProjectsList1.ProjectId + "'";
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = " AND UserId = '" + ProjectsList1.UserId + "'";
            }

            string orderClose = GetOrderClause();

            if (!string.IsNullOrEmpty(HiddenFiled_CustomQuery.Value))
            {
                string query = HiddenFiled_CustomQuery.Value;

                query = GetQuery(userId, projectId, query);

                if (!string.IsNullOrEmpty(orderClose))
                {
                    query = query + " order by " + orderClose;
                }

                list = presenter.GetFiltered(query);
            }
            else
            {
                string query = ConsumerRequestFilter1.DefaultQuery;

                query = GetQuery(userId, projectId, query);

                if (!string.IsNullOrEmpty(orderClose))
                {
                    query = query + " order by " + orderClose;
                }

                list = presenter.GetFiltered(query);
            }

            if (list.Count == 0)
            {
                ErrorShow("No data to process a Label Request.");
                return;
            }

            // check if all RequestRecived
            foreach (ConsumerRequest cr in list)
            {
                if (cr.Status == ConsumerRequestStatus.InTransit || cr.Status == ConsumerRequestStatus.Deleted || cr.Status == ConsumerRequestStatus.Delivered
                    || cr.Status == ConsumerRequestStatus.Exception
                    || cr.Status == ConsumerRequestStatus.NeedResearch
                    || cr.Status == ConsumerRequestStatus.NonDeliverableOrDead
                    || cr.Status == ConsumerRequestStatus.OptOut
                    || cr.Status == ConsumerRequestStatus.PendingShipment
                    || cr.Status == ConsumerRequestStatus.Returned)
                {
                    allRequestRecived = false;
                }
            }

            if (ProjectsList1.ProjectId == Guid.Empty)
            {
                ErrorShow("Project must be selected to process a Label Request.");
                return;
            }

            if (!allRequestRecived)
            {
                string s = "Labels can only be created for Consumer Requests that " +
                            "are at a Request Received status.  Please filter the list to remove any records " +
                            "that are not Request Received status.";
                ErrorShow(s);
                return;
            }

            //create lbrequest
            List<Guid> listCRsIDs = new List<Guid>();
            foreach (ConsumerRequest consumerRequest in list)
            {
                listCRsIDs.Add(consumerRequest.Id);
            }

            Session["NewLabelRequestIds"] = listCRsIDs;
            Session["NewLabelRequestProjectId"] = ProjectsList1.ProjectId;
            Response.Redirect("NewLabelRequest.aspx");
        }

		protected void btnBulkExport_OnClick(object sender, EventArgs e)
		{
			string userId = "";
			string projectId = "";
			List<ConsumerRequest> source = new List<ConsumerRequest>();

			if (ProjectsList1.ProjectId != Guid.Empty)
			{
				projectId = " AND ProjectId = '" + ProjectsList1.ProjectId + "'";
			}

			if (ProjectsList1.UserId != Guid.Empty)
			{
				userId = " AND UserId = '" + ProjectsList1.UserId + "'";
			}

			if (!string.IsNullOrEmpty(HiddenFiled_CustomQuery.Value))
			{
				string query = HiddenFiled_CustomQuery.Value;
				query = GetQuery(userId, projectId, query);

				source = presenter.GetFiltered(query);
			}

			if (source.Count > 0)
			{
				ProduceCSV(source);
			}
		}

		private void ProduceCSV(List<ConsumerRequest> source)
		{
			string[] values = new string[41];

			#region Make the Header
			values[0] = "AccountNumber";
			values[1] = "AccountName";
			values[2] = "CompanyName";
			values[3] = "Address1";
			values[4] = "Address2";
			values[5] = "City";
			values[6] = "State";
			values[7] = "ZipCode";
			values[8] = "ServiceAddress1";
			values[9] = "ServiceAddress2";
			values[10] = "ServiceCity";
			values[11] = "ServiceState";
			values[12] = "ServiceZip";
			values[13] = "CompanyCode";
			values[14] = "ProjectCode";
			values[15] = "StatusSort";
			values[16] = "Status";
			values[17] = "Phone1";
			values[18] = "Phone2";
			values[19] = "IsReship";
			values[20] = "DoNotShip";
			values[21] = "IsOkayToContact";
			values[22] = "AnalysisDate";
			values[23] = "ReceiptDate";
			values[24] = "CRImportedDate";
			values[25] = "Quantity";
			values[26] = "AuditFailureDate";
			values[27] = "OperatingCompany";
			values[28] = "BatchLabel";
			values[29] = "Notes";
			values[30] = "OutOfState";
			values[31] = "Email";
			values[32] = "Method";
			values[33] = "WaterHeaterFuel";
			values[34] = "HeaterFuel";
			values[35] = "FacilityType";
			values[36] = "RateCode";
			values[37] = "IncomeQualified";
			values[38] = "PremiseID";
			values[39] = "AccountNumberOld";
			values[40] = "RequestedKit";
			#endregion

			Response.ClearContent();
			Response.Clear();
			Response.ContentType = "application/CSV";
			Response.AddHeader("Content-Disposition", "attachment;filename=ConsumerRequest.csv");
			Response.Write(string.Join(",", values));
			Response.Write(Environment.NewLine);

			if (source != null)
			{
				foreach (var item in source)
				{
					values = new string[41];
					foreach (PropertyInfo prop in item.GetType().GetProperties())
					{
						var propValue = prop.GetValue(item, null);

						#region Make items
						switch (prop.Name)
						{
							case "AccountNumber":
								values[0] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "AccountName":
								values[1] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "CompanyName":
								values[2] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "Address1":
								values[3] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "Address2":
								values[4] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "City":
								values[5] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "State":
								values[6] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "ZipCode":
								values[7] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "ServiceAddress1":
								values[8] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "ServiceAddress2":
								values[9] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "ServiceCity":
								values[10] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "ServiceState":
								values[11] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "ServiceZip":
								values[12] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "CompanyCode":
								values[13] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "ProjectCode":
								values[14] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "StatusSort":
								values[15] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "Status":
								values[16] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "Phone1":
								values[17] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "Phone2":
								values[18] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "IsReship":
								values[19] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "DoNotShip":
								values[20] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "IsOkayToContact":
								values[21] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "AnalysisDate":
								values[22] = propValue != null ? "\"" + Convert.ToDateTime(propValue).ToString("MM/dd/yyyy") + "\"" : string.Empty;
								break;
							case "ReceiptDate":
								values[23] = propValue != null ? "\"" + Convert.ToDateTime(propValue).ToString("MM/dd/yyyy") + "\"" : string.Empty;
								break;
							case "CRImportedDate":
								values[24] = propValue != null ? "\"" + Convert.ToDateTime(propValue).ToString("MM/dd/yyyy") + "\"" : string.Empty;
								break;
							case "Quantity":
								values[25] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "AuditFailureDate":
								values[26] = propValue != null ? "\"" + Convert.ToDateTime(propValue).ToString("MM/dd/yyyy") + "\"" : string.Empty;
								break;
							case "OperatingCompany":
								values[27] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "BatchLabel":
								values[28] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "Notes":
								values[29] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "OutOfState":
								values[30] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "Email":
								values[31] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "Method":
								values[32] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "WaterHeaterFuel":
								values[33] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "HeaterFuel":
								values[34] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "FacilityType":
								values[35] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "RateCode":
								values[36] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "IncomeQualified":
								values[37] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "PremiseID":
								values[38] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "AccountNumberOld":
								values[39] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
							case "RequestedKit":
								values[40] = propValue != null ? "\"" + propValue.ToString() + "\"" : string.Empty;
								break;
						}
						#endregion
					}
					Response.Write(string.Join(",", values));
					Response.Write(Environment.NewLine);
				}
			}

			Response.Flush();
			Response.End();
		}
		
        private void ErrorShow(string error)
        {
            //Label l = new Label();
            //l.Text = error;
            //l.ControlStyle.ForeColor = System.Drawing.Color.Red;
            //RadGrid_ConsumerRequests.Controls.Add(l);
            lbPopUp.Text = error;
            lbPopUp.ControlStyle.ForeColor = System.Drawing.Color.Red;
            mpe.Show();
        }

        private string GetOrderClause()
        {
            string orderCluase = string.Empty;

            foreach (GridSortExpression exp in this.RadGrid_ConsumerRequests.MasterTableView.SortExpressions)
            {
                orderCluase += string.Format(" [{0}] {1},", exp.FieldName.ToString(), exp.SortOrderAsString());
            }

            return orderCluase.TrimEnd(new char[] { ',' });
        }

    }
}