using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Xml.Linq;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class AEGXMLExceptionReportv3 : AEGReportsV3PageBase
    {
        #region Class Data
        decimal Kw_Impact;
        decimal Kwh_Impact;
        string AuditFailureDate;
        #endregion

        #region Grid Events
        protected void RadGrid_Exceptions_ItemCreated(object sender, GridItemEventArgs e)
        {
            base.RadGrid_ItemCreated(sender, e);
        }
        protected void RadGrid_Exceptions_ItemDataBound(object sender, GridItemEventArgs e)
        {
            base.RadGrid_ItemDataBound(sender, e);
        }
        protected void RadGrid_Exceptions_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            try
            {
                NeedDataSource(e, RadGrid_Exceptions);
            }
            catch (Exception ex)
            {
                Log.LogError(ex);
                AddError(ex.Message);
            }
        }
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }

            if (!IsPostBack)
            {
                base.GetAllKits(RadComboBox_KitType);
                base.SetTitleAndHelpLink("Click here for help", "AEG XML Exception Report - V3");
            }
            ProjectsList.OnProjectChanged += (projectId) => { ProjectChangedHandler(RadGrid_Exceptions); };
            btnSearch.Click += (s, ev) => { base.SearchClickHandler(ProjectsList.ProjectId, RadGrid_Exceptions, lblError, true); };
            btnGenerateXml.Click += (s, ev) => { base.GenerateXMLClick(this.Page, ProjectsList.ProjectId, LblWarningMessage); };
            lblError.Text = string.Empty;
            base.errorLabel = lblError;
        }
        #endregion

        #region Event Handlers
        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            tbPONumberFilter.Text = null;
            dpStartDateFilter.SelectedDate = null;
            dpExceptionEndDate.SelectedDate = null;
            dpReceptionStartDate.SelectedDate = null;
            dpReceiptEndDate.SelectedDate = null;
            RadComboBox_KitType.SelectedValue = null;
        }
        protected void BulkExport_Click(object sender, EventArgs e)
        {
            if (ProjectsList.ProjectId == Guid.Empty)
            {
                LblWarningMessage.Text = "Please select a single project.";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AEGXMLExceptionReportFailed", "AEGXMLReportFailed();", true);
            }
            else
            {
                ProjectPresenter objProjPresenter = new ProjectPresenter();
                string ProgramID = objProjPresenter.GetProgramID(ProjectsList.ProjectId);
                if (ProgramID == null || ProgramID == string.Empty)
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AEGXMLExceptionReportFailed", "AEGXMLReportFailed();", true);
                }
                else
                {
                    ProduceCSV();
                }
            }
        }        
        #endregion

        #region Helpers
        protected override void NodeGenerator()
        {
            string poNum = tbPONumberFilter.Text;
            DateTime? startDate = dpStartDateFilter.SelectedDate;
            DateTime? endDate = dpExceptionEndDate.SelectedDate;
            DateTime? ReceptionStartDate = dpReceptionStartDate.SelectedDate;
            DateTime? ReceptionEndDate = dpReceiptEndDate.SelectedDate;

            if (endDate != null)
                endDate = endDate.Value.AddDays(1);

            //AB 2015-02-27
            if (ReceptionEndDate != null)
                ReceptionEndDate = ReceptionEndDate.Value.AddDays(1);

            Guid? kitType = null;
            string kit = RadComboBox_KitType.SelectedValue;
            if (!string.IsNullOrEmpty(kit))
                kitType = new Guid(RadComboBox_KitType.SelectedValue);

            if (poNum == string.Empty)
                poNum = null;

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList.ProjectId;
            }

            if (ProjectsList.UserId != Guid.Empty)
            {
                userId = ProjectsList.UserId;
            }

            if (ProjectsList.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList.CompanyId;
            }
            var dt = presenter.GetAEGXMLReportExceptionsV3(poNum, startDate, endDate, kitType, userId, projectId, companyId, ReceptionStartDate, ReceptionEndDate).Tables[0];

            XDocument _document = new XDocument();
            XElement content4;
            if (dt != null)
            {   //Creating the parent node
                XElement _root = new XElement("systemRecord");

                string FirstName = string.Empty;
                string LastName = string.Empty;
                foreach (DataRow dr in dt.Rows)
                {
                    if (Convert.ToString(dr["AccountName"]).Contains(' '))
                    {
                        String[] Name = ParseName(Convert.ToString(dr["AccountName"]));
                        FirstName = Convert.ToString(Name[0]);
                        LastName = Convert.ToString(Name[1]);
                    }
                    else
                    {
                        FirstName = Convert.ToString(dr["AccountName"]);
                    }
                    if (dr["AnalysisDate"] != DBNull.Value && Convert.ToString(dr["AnalysisDate"]) != null || Convert.ToString(dr["AnalysisDate"]) != string.Empty)
                    {
                        AnalysisDate = Convert.ToDateTime(dr["AnalysisDate"]).ToString("MM/dd/yyyy");
                    }
                    if (dr["ReceiptDate"] != DBNull.Value && Convert.ToString(dr["ReceiptDate"]) != null || Convert.ToString(dr["ReceiptDate"]) != string.Empty)
                    {
                        ReceiptDate = Convert.ToDateTime(dr["ReceiptDate"]).ToString("MM/dd/yyyy");
                    }
                    if (dr["ShipDate"] != DBNull.Value && Convert.ToString(dr["ShipDate"]) != null || Convert.ToString(dr["ShipDate"]) != string.Empty)
                    {
                        ShipDate = Convert.ToDateTime(dr["ShipDate"]).ToString("MM/dd/yyyy");
                    }
                    if (dr["AuditFailureDate"] != DBNull.Value && Convert.ToString(dr["AuditFailureDate"]) != null || Convert.ToString(dr["AuditFailureDate"]) != string.Empty)
                    {
                        AuditFailureDate = Convert.ToDateTime(dr["AuditFailureDate"]).ToString("MM/dd/yyyy");
                    }
                    // In case of null or '0' the value of CRQuantity need to be set to '1'
                    if (dr["Quantity"] == DBNull.Value || dr["Quantity"] == null || Convert.ToInt32(dr["Quantity"]) == 0)
                    {
                        dr["Quantity"] = 1;
                    }
                    if (dr["KwhImpact"] != DBNull.Value && dr["KwhImpact"] != null)
                    {
                        //shows the power saved in kwh on using the product
                        Kwh_Savings = Convert.ToInt64(dr["KwhImpact"]) * Convert.ToInt32(dr["Quantity"]);
                    }
                    if (dr["KwImpact"] != DBNull.Value && dr["KwImpact"] != null)
                    {
                        //shows the power saved in kw on using the product
                        Kw_savings = Convert.ToInt64(dr["KwImpact"]) * Convert.ToInt32(dr["Quantity"]);
                    }
                    string refID_Full = Convert.ToString(dr["Id"]);
                    int refID_Length = refID_Full.Length;
                    int refID_StartIndex = refID_Length - 4;
                    string RefIDPortion = refID_Full.Substring(refID_StartIndex, 4);
                    string AccountNumber = Convert.ToString(dr["AccountNumber"]);

                    string refIDConcat = "PD_" + RefIDPortion + AccountNumber + "_" + ReceiptDate;
                    string refID;
                    string contactRefIDPortion = string.Empty;

                    string contactRefID = "PD_Pri_Cont_" + Convert.ToString(dr["AccountNumber"]);
                    if (contactRefID.Length > 38)
                    {
                        contactRefIDPortion = contactRefID.Substring(1, 38);
                    }
                    else
                    {
                        contactRefIDPortion = contactRefID;

                    }
                    if (refIDConcat.Length > 50)
                    {
                        //creating the refID as per the rule.
                        refID = refIDConcat.Substring(1, 50);
                    }
                    else
                    {

                        refID = refIDConcat;
                    }
                    if (dr["ReceiptDate"] != DBNull.Value && dr["ReceiptDate"] != null)
                    { MonthShipped = GetFirstDayOfMonth(Convert.ToDateTime(dr["ReceiptDate"])); }
                    //Import node starts

                    content4 = new XElement("importRecord",
                           (dr["ProgramID"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.programNumber, Convert.ToString(dr["ProgramID"])) : null),
                           (refID.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.refID, refID) : null),
                           (FirstName.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.customerFirstname, FirstName.Trim()) : null),
                           (LastName.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.customerLastname, LastName) : null),
                           (dr["AccountNumber"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.customerAccountNumber, Convert.ToString(dr["AccountNumber"])) : null),
                           (dr["AccountNumber"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.primaryContactId, "PD_Pri_Cont_" + Convert.ToString(dr["AccountNumber"])) : null),
                           (dr["AccountNumber"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.premiseContactId, "PD_Pri_Cont_" + Convert.ToString(dr["AccountNumber"])) : null),
                           new XElement(XmlFormatter.status, "Cancelled"),
                           (AnalysisDate.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.applicationDate, AnalysisDate) : null),
                    new XElement("importContacts", ImportContactNode(dr, contactRefIDPortion, FirstName, LastName)),
                    new XElement("importApplication",
                           AnalysisDate.CheckDbNullOrEmpty() ? new XElement("importApplicationItem",
                                new XElement("field", XmlFormatter.AnalysisDate),
                                new XElement("value", AnalysisDate)) : null,
                            ReceiptDate.CheckDbNullOrEmpty() ? new XElement("importApplicationItem",
                                new XElement("field", XmlFormatter.ReceiptDate),
                                new XElement("value", ReceiptDate)) : null,
                            CreateXElement("importApplicationItem", XmlFormatter.Operating_Company, "OperatingCompany", dr),
                            CreateXElement("importApplicationItem", XmlFormatter.Water_Heater_Fuel, "WaterHeaterFuel", dr),
                            CreateXElement("importApplicationItem", XmlFormatter.Heater_Fuel, "HeaterFuel", dr),
                            CreateXElement("importApplicationItem", XmlFormatter.OK_to_Contact, "IsOkayToContact", dr),
                            CreateXElement("importApplicationItem", XmlFormatter.Facility_Type, "FacilityType", dr),
                            CreateXElement("importApplicationItem", XmlFormatter.Exception_Date, "AuditFailureDate", dr),
                            CreateXElement("importApplicationItem", XmlFormatter.Shipment_Date, "ShipDate", dr),
                            CreateXElement("importApplicationItem", XmlFormatter.Rate_Code, "RateCode", dr),
                            CreateXElement("importApplicationItem", XmlFormatter.Enrollment_Completion_Date, "AnalysisDate", dr),
                            CreateXElement("importApplicationItem", XmlFormatter.Income_Qualified, "IncomeQualified", dr),
                            CreateXElement("importApplicationItem", XmlFormatter.Number_Kits_Shipped, "Quantity", dr),
                            Convert.ToString(MonthShipped).CheckDbNullOrEmpty() ? new XElement("importApplicationItem",
                                                (MonthShipped.CheckDbNullOrEmpty() ? new XElement("field", XmlFormatter.Month_Shipped) : null),
                                               (MonthShipped.CheckDbNullOrEmpty() ? new XElement("value", MonthShipped.ToString("MM/dd/yyyy")) : null)) : null,
                          new XElement("importApplicationItem",
                                                new XElement("field", XmlFormatter.Kwh_Savings),
                                                new XElement("value", Kwh_Savings)
                                            ),
                           new XElement("importApplicationItem",
                                                 new XElement("field", XmlFormatter.Kw_savings),
                                                 new XElement("value", Kw_savings))
                                            ),
                                             EquipmentAttributeNode(_root, dr));

                    _root.Add(content4);
                }

                _document.Add(_root);

                _document.Save(Server.MapPath("~/Uploads/AEG_ExceptionReport.xml"), SaveOptions.None);
                string path = Server.MapPath("~/Uploads/AEG_ExceptionReport.xml");
                string filename = "AEG_ExceptionReport.xml";
                Response.ClearContent();
                Response.Clear();
                Response.ContentType = "text/xml";
                Response.AppendHeader("Content-Disposition", "attachment;filename=" + filename + ";");
                Response.TransmitFile(path);
                Response.Flush();
                Response.End();
                _root = null;
                _document = null;
            }
        }
        protected override DataSet GetFilteredList()
        {
            string poNum = tbPONumberFilter.Text;
            DateTime? startDate = dpStartDateFilter.SelectedDate;
            DateTime? ReceptionStartDate = dpReceptionStartDate.SelectedDate;
            DateTime? ReceptionEndDate = dpReceiptEndDate.SelectedDate;
            DateTime? endDate = dpExceptionEndDate.SelectedDate;

            if (endDate != null)
                endDate = endDate.Value.AddDays(1);

            //AB 2015-02-27
            if (ReceptionEndDate != null)
                ReceptionEndDate = ReceptionEndDate.Value.AddDays(1);

            Guid? kitType = null;
            string kit = RadComboBox_KitType.SelectedValue;
            if (!string.IsNullOrEmpty(kit))
                kitType = new Guid(RadComboBox_KitType.SelectedValue);

            if (poNum == string.Empty)
                poNum = null;

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList.ProjectId;
            }

            if (ProjectsList.UserId != Guid.Empty)
            {
                userId = ProjectsList.UserId;
            }

            if (ProjectsList.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList.CompanyId;
            }

            return presenter.GetAEGReportExceptionsV3(poNum, startDate, endDate, kitType, userId, projectId, companyId, ReceptionStartDate, ReceptionEndDate);            
        }
        private XElement EquipmentAttributeNode(XElement _root, DataRow dr)
        {
            XElement content = new XElement("importEquipment");
            EquipmentAttributesDeclarations(dr);
            string description = string.Empty;
            string receiptDateEA = string.Empty;
            foreach (DataRow dre in EquipmentAttributes.Rows)
            {
                if (dre["ReceiptDate"] != DBNull.Value && Convert.ToString(dre["ReceiptDate"]) != null || Convert.ToString(dre["ReceiptDate"]) != string.Empty)
                {
                    receiptDateEA = Convert.ToDateTime(dre["ReceiptDate"]).ToString("MM/dd/yyyy");
                }
                CatalogID = Convert.ToString(dre["AccountNumber"]).Trim() + "_" + Convert.ToString(dre["CatalogID"]).Trim();
                CatalogID = CatalogID.Trim();
                if (dre["Quantity"] == DBNull.Value || dre["Quantity"] == null || Convert.ToInt32(dre["Quantity"]) == 0)
                {
                    dre["Quantity"] = 1;
                }
                if (dre["KiQuantity"] != DBNull.Value && dre["KiQuantity"] != null)
                {
                    iQuantity = Convert.ToInt32(dre["Quantity"]) * Convert.ToInt32(dre["KiQuantity"]);
                }
                description = Convert.ToString(dre["KitName"]).Trim();
                XElement temp =
                           new XElement("importEquipmentItem",
                                      CatalogID.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.equipmentRefId, CatalogID) : null,
                                      new XElement("equipmentAttributes",
                                      description.CheckDbNullOrEmpty() ? new XElement("attribute",

                                             (description.CheckDbNullOrEmpty() ? new XElement("attributeName", XmlFormatter.Description) : null),
                                             (description.CheckDbNullOrEmpty() ? new XElement("attributeValue", description) : null)

                                      ) : null,
                                      dre["CatalogID"].CheckDbNullOrEmpty() ? new XElement("attribute",

                                             (dre["CatalogID"].CheckDbNullOrEmpty() ? new XElement("attributeName", XmlFormatter.CatalogID) : null),
                                             (dre["CatalogID"].CheckDbNullOrEmpty() ? new XElement("attributeValue", Convert.ToString(dre["CatalogID"]).Trim()) : null)

                                      ) : null,
                                      dre["ReceiptDate"].CheckDbNullOrEmpty() ? new XElement("attribute",

                                             (receiptDateEA.CheckDbNullOrEmpty() ? new XElement("attributeName", XmlFormatter.Date_installed) : null),
                                             (receiptDateEA.CheckDbNullOrEmpty() ? new XElement("attributeValue", receiptDateEA) : null)

                                      ) : null,
                                       iQuantity.CheckDbNullOrEmpty() ? new XElement("attribute",

                                              (iQuantity.CheckDbNullOrEmpty() ? new XElement("attributeName", XmlFormatter.Quantity) : null),
                                              (iQuantity.CheckDbNullOrEmpty() ? new XElement("attributeValue", iQuantity) : null)
                                      ) : null,
                                      dre["KwImpact"].CheckDbNullOrEmpty() ? new XElement("attribute",
                                            new XElement("attributeName", XmlFormatter.Kw_Impact),
                                            new XElement("attributeValue", Convert.ToDecimal(dre["KwImpact"]))
                                      ) : null,
                                       dre["KwhImpact"].CheckDbNullOrEmpty() ? new XElement("attribute",
                                           new XElement("attributeName", XmlFormatter.Kwh_Impact),
                                           new XElement("attributeValue", Convert.ToDecimal(dre["KwhImpact"]))
                                       ) : null
                                  ));
                content.Add(temp);
            }
            return content;
        }
        private void ProduceCSV()
        {
            DataTable source = GetFilteredList().Tables[0];

            string[] values = new string[27];
            #region Make the Header
            values[0] = "AccountNumber";
            values[1] = "AccountName";
            values[2] = "ServiceAddress1";
            values[3] = "ServiceAddress2";
            values[4] = "ServiceCity";
            values[5] = "ServiceState";
            values[6] = "ServiceZip";
            values[7] = "Email";
            values[8] = "Phone1";
            values[9] = "Phone2";
            values[10] = "AnalysisDate";
            values[11] = "ReceiptDate";
            values[12] = "AuditFailureDate";
            values[13] = "KitTypeName";
            values[14] = "OperatingCompany";
            values[15] = "WaterHeaterFuel";
            values[16] = "HeaterFuel";
            values[17] = "IsOkayToContact";
            values[18] = "RateCode";
            values[19] = "TransactionType";
            values[20] = "CatalogID";
            values[21] = "Quantity";
            values[22] = "ProgramID";
            values[23] = "CompanyName";
            values[24] = "KwImpact";
            values[25] = "KwhImpact";
            values[26] = "IncomeQualified";
            #endregion

            Response.ClearContent();
            Response.Clear();
            Response.ContentType = "application/CSV";
            Response.AddHeader("Content-Disposition", "attachment;filename=AEG_XML_Exception_Report_V3.csv");
            Response.Write(string.Join(",", values));
            Response.Write(Environment.NewLine);

            if (source != null)
            {
                var ordColumns = new int[values.Length];

                foreach (DataColumn col in source.Columns)
                {
                    for (var i = 0; i < values.Length; i++)
                    {
                        if (col.ColumnName == values[i])
                        {
                            ordColumns[i] = col.Ordinal; break;
                        }
                    }
                }

                foreach (DataRow dr in source.Rows)
                {
                    values = new string[27];

                    for (var i = 0; i < ordColumns.Length; i++)
                    {
                        var val = dr[ordColumns[i]];
                        var col = source.Columns[ordColumns[i]];

                        if (col.DataType == typeof(DateTime))
                        {
                            values[i] = val != DBNull.Value ? "\"" + Convert.ToDateTime(val).ToString("MM/dd/yyyy") + "\"" : string.Empty;
                        }
                        else
                        {
                            values[i] = val != DBNull.Value ? "\"" + val.ToString() + "\"" : string.Empty;
                        }
                    }

                    Response.Write(string.Join(",", values));
                    Response.Write(Environment.NewLine);
                }
            }

            Response.Flush();
            Response.End();
        }
        #endregion
    }
}