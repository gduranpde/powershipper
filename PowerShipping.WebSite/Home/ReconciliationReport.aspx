﻿<%@ Page Title="Reconciliation Report" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" CodeBehind="ReconciliationReport.aspx.cs" Inherits="PowerShipping.WebSite.Home.ReconciliationReport" Theme="Default" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register src="../Controls/StateList.ascx" tagname="StateList" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<div class="pageDiv" >
    
    
    <table>
        <tr>
            <td>              
                  <h3>Reconciliation Report</h3>
            </td>           
            <td colspan=3 align="center">
                 <uc1:StateList ID="StateList1" runat="server"  IsCompanyUse="true"/>
            </td>
        </tr>
        <tr>
            <td>
                Start Date
                <telerik:RadDatePicker ID="dpStartDateFilter" runat="server" MinDate="01-01-1990" MaxDate="01-01-2020"></telerik:RadDatePicker>
            </td>
            <td>
                End Date <!--="RadGrid_Grid_ItemEvent" ClientSettings-ClientEvents-OnColumnClick=""-->
                <telerik:RadDatePicker ID="dpEndDateFilter" runat="server" MinDate="01-01-1990" MaxDate="01-01-2020"></telerik:RadDatePicker>
            </td>            
        <td>
                <asp:Button ID="btnGenerateExcel" runat="server" OnClick="btnGenerateExcel_Click" Text="Generate Report" CssClass="ptButton"/>
            </td>
         </tr>
        <tr>
                <td colspan="2">
                    <asp:Label ID="lbMessages" runat="server" Text=""></asp:Label>
                </td>
            </tr>
    </table>
  
    <br />
      
         <telerik:RadGrid ID="RadGrid_Grid" runat="server" Width="100%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True" 
              Skin="Windows7" 
        onneeddatasource="RadGrid_Grid_NeedDataSource" 
            >
            <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
             AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true" 
             CommandItemSettings-ShowAddNewRecordButton="false" CssClass="rightPadding"
            >
            <Columns>             
                <telerik:GridBoundColumn DataField="StateName" HeaderText="State" SortExpression="StateName"
                        UniqueName="StateName" CurrentFilterFunction="Contains" ShowFilterIcon="false" ReadOnly="true">
                        </telerik:GridBoundColumn>

                <telerik:GridBoundColumn DataField="StartDate" HeaderText="Start Date" SortExpression="StartDate"
                        UniqueName="StartDate" CurrentFilterFunction="Contains" ShowFilterIcon="false" ReadOnly="true">
                        </telerik:GridBoundColumn>

                <telerik:GridBoundColumn DataField="EndDate" HeaderText="End Date" SortExpression="EndDate"
                        UniqueName="EndDate" CurrentFilterFunction="Contains" ShowFilterIcon="false" ReadOnly="true">
                        </telerik:GridBoundColumn>
            
                  <telerik:GridBoundColumn DataField="StatusName" HeaderText="Status" SortExpression="StatusName"
                        UniqueName="StatusName" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        <HeaderStyle Width="100px"/>
                        </telerik:GridBoundColumn>   

                  <telerik:GridDateTimeColumn DataField="StartedOn" HeaderText="Started On" SortExpression="StartedOn"
                        UniqueName="StartedOn" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        DataFormatString="{0:MM/dd/yyyy hh:mm:ss}">
                        <HeaderStyle Width="150px"/>
                        </telerik:GridDateTimeColumn>         
                          
                  <telerik:GridDateTimeColumn DataField="CompletedOn" HeaderText="Completed On" SortExpression="CompletedOn"
                        UniqueName="CompletedOn" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        DataFormatString="{0:MM/dd/yyyy hh:mm:ss}">
                        <HeaderStyle Width="150px"/>                             
                        </telerik:GridDateTimeColumn>  
                
                  <telerik:GridHyperLinkColumn  DataTextField="FilePath" HeaderText="File Path" SortExpression="FilePath"
                        UniqueName="FilePath"  DataTextFormatString="{0}" DataNavigateUrlFields="FilePath"
                       DataNavigateUrlFormatString="./../Reports/{0}"  target="blank"  
                           ItemStyle-Font-Underline="true"> 
                        </telerik:GridHyperLinkColumn> 
                
                <telerik:GridBoundColumn DataField="UserName" HeaderText="User Name" SortExpression="UserName"
                        UniqueName="UserName" CurrentFilterFunction="Contains" ShowFilterIcon="false" ReadOnly="true">
                        </telerik:GridBoundColumn>
                
            </Columns>               
            </MasterTableView>
    </telerik:RadGrid>

</div>
</asp:Content>