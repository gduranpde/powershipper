﻿<%@ Page Title="Address Change Report" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" CodeBehind="AddressChangeReport.aspx.cs" Inherits="PowerShipping.WebSite.Home.AddressChangeReport" Theme="Default" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register src="../Controls/ProjectsList.ascx" tagname="ProjectsList" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<div class="pageDiv">
    
    
    <table>
    <tr>
            <td>              
                  <h3>Address Change Report</h3>
            </td>           
            <td colspan=4 align="center">
                 <uc1:ProjectsList ID="ProjectsList1" runat="server"  IsCompanyUse="true"/>
            </td>
        </tr>
        <tr>
            <td>
                Start Date
                <telerik:RadDatePicker ID="dpStartDateFilter" runat="server" MinDate="01-01-1990" MaxDate="01-01-2020"></telerik:RadDatePicker>
            </td>
            <td>
                End Date
                <telerik:RadDatePicker ID="dpEndDateFilter" runat="server" MinDate="01-01-1990" MaxDate="01-01-2020"></telerik:RadDatePicker>
            </td>            
            <td>
                Account Number
                <asp:TextBox ID="tbAccountNumber" runat="server">                          
                </asp:TextBox>
        </td>
        <td>
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" CssClass="ptButton"/>
            </td>
            <td>
                <asp:Button ID="btnClearFilter" runat="server" OnClick="btnClearFilter_Click" Text="Clear" CssClass="ptButton"/>
            </td> 
        </tr>
    </table>
  
    <telerik:RadGrid ID="RadGrid_AddressChanges" runat="server" Width="100%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True" 
              Skin="Windows7" 
        onneeddatasource="RadGrid_AddressChanges_NeedDataSource"
           OnItemCreated="RadGrid_AddressChanges_ItemCreated"
            OnItemDataBound="RadGrid_AddressChanges_ItemDataBound"   
                  
            >
            <ExportSettings ExportOnlyData="true" IgnorePaging="true">            
            </ExportSettings>
            <MasterTableView DataKeyNames="ConsumerRequestId" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
             AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true"
            >
            <CommandItemSettings                   
                    ShowExportToWordButton="true"
                    ShowExportToExcelButton="true"
                    ShowExportToCsvButton="true"
                    ShowExportToPdfButton="true"
                    />
            <Columns> 
                  <telerik:GridBoundColumn DataField="ChangedBy" HeaderText="ChangedBy" SortExpression="ChangedBy"
                        UniqueName="ChangedBy" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        EditFormColumnIndex="0">
                        </telerik:GridBoundColumn>      
                  <telerik:GridDateTimeColumn DataField="ChangedDate" HeaderText="ChangedDate" SortExpression="ChangedDate"
                        UniqueName="ChangedDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true" 
                        DataFormatString="{0:MM/dd/yyyy}">
                        </telerik:GridDateTimeColumn> 
                        
                        
                <%--<telerik:GridBoundColumn DataField="AccountNumber" HeaderText="AccountNumber" SortExpression="AccountNumber"
                        UniqueName="AccountNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        EditFormColumnIndex="0">
                        </telerik:GridBoundColumn>--%>
                        
                        
               <%--   <telerik:GridBoundColumn DataField="AccountName" HeaderText="Name" SortExpression="AccountName"
                        UniqueName="AccountName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="Address" HeaderText="Address" SortExpression="Address"
                        UniqueName="Address" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="City" HeaderText="City" SortExpression="City"
                        UniqueName="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="State" HeaderText="State" SortExpression="State"
                        UniqueName="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="ZipCode" HeaderText="ZIP" SortExpression="ZipCode"
                        UniqueName="ZipCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" MaxLength="5">
                        </telerik:GridBoundColumn>
                        
                   <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email"
                        UniqueName="Email" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridMaskedColumn DataField="Phone1" HeaderText="Phone 1" SortExpression="Phone1" Mask="###-###-####"
                        UniqueName="Phone1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        DataFormatString="{0:(###)###-####}">                    
                    </telerik:GridMaskedColumn> 
                  <telerik:GridMaskedColumn DataField="Phone2" HeaderText="Phone 2" SortExpression="Phone2" Mask="###-###-####"
                        UniqueName="Phone2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        DataFormatString="{0:(###)###-####}">                    
                    </telerik:GridMaskedColumn> --%>
                    
                   <telerik:GridBoundColumn DataField="OldAccountNumber" HeaderText="Old AccountNumber" SortExpression="OldAccountNumber"
                        UniqueName="OldAccountNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="NewAccountNumber" HeaderText="New AccountNumber" SortExpression="NewAccountNumber"
                        UniqueName="NewAccountNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                    
                   <telerik:GridBoundColumn DataField="OldAccountName" HeaderText="Old Name" SortExpression="OldAccountName"
                        UniqueName="OldAccountName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="NewAccountName" HeaderText="New Name" SortExpression="NewAccountName"
                        UniqueName="NewAccountName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                        
                  <telerik:GridBoundColumn DataField="OldAddress1" HeaderText="Old Address1" SortExpression="OldAddress1"
                        UniqueName="OldAddress1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="NewAddress1" HeaderText="New Address1" SortExpression="NewAddress1"
                        UniqueName="NewAddress1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                        
                  <telerik:GridBoundColumn DataField="OldAddress2" HeaderText="Old Address2" SortExpression="OldAddress2"
                        UniqueName="OldAddress2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="NewAddress2" HeaderText="New Address2" SortExpression="NewAddress2"
                        UniqueName="NewAddress2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                        
                  <telerik:GridBoundColumn DataField="OldCity" HeaderText="Old City" SortExpression="OldCity"
                        UniqueName="OldCity" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="NewCity" HeaderText="New City" SortExpression="NewCity"
                        UniqueName="NewCity" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                        
                  <telerik:GridBoundColumn DataField="OldState" HeaderText="Old State" SortExpression="OldState"
                        UniqueName="OldState" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="NewState" HeaderText="New State" SortExpression="NewState"
                        UniqueName="NewState" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                        
                  <telerik:GridBoundColumn DataField="OldZipCode" HeaderText="Old ZIP" SortExpression="OldZipCode"
                        UniqueName="OldZipCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" MaxLength="10">                        
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="NewZipCode" HeaderText="New ZIP" SortExpression="NewZipCode"
                        UniqueName="NewZipCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" MaxLength="10">
                        </telerik:GridBoundColumn>
                              
                  <telerik:GridBoundColumn DataField="OldEmail" HeaderText="Old Email" SortExpression="OldEmail"
                        UniqueName="OldEmail" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="NewEmail" HeaderText="New Email" SortExpression="NewEmail"
                        UniqueName="NewEmail" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                        
                  <telerik:GridMaskedColumn DataField="OldPhone1" HeaderText="Old Phone 1" SortExpression="OldPhone1" Mask="###-###-####"
                        UniqueName="OldPhone1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        DataFormatString="{0:(###)###-####}">                    
                    </telerik:GridMaskedColumn> 
                  <telerik:GridMaskedColumn DataField="NewPhone1" HeaderText="New Phone 1" SortExpression="NewPhone1" Mask="###-###-####"
                        UniqueName="NewPhone1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        DataFormatString="{0:(###)###-####}">                    
                    </telerik:GridMaskedColumn>
                    
                  <telerik:GridMaskedColumn DataField="OldPhone2" HeaderText="Old Phone 2" SortExpression="OldPhone2" Mask="###-###-####"
                        UniqueName="OldPhone2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        DataFormatString="{0:(###)###-####}">                    
                    </telerik:GridMaskedColumn>
                  <telerik:GridMaskedColumn DataField="NewPhone2" HeaderText="New Phone 2" SortExpression="NewPhone2" Mask="###-###-####"
                        UniqueName="NewPhone2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        DataFormatString="{0:(###)###-####}">                    
                    </telerik:GridMaskedColumn>  
                        
              
            </Columns>               
            </MasterTableView>
    </telerik:RadGrid>
</div>
</asp:Content>

