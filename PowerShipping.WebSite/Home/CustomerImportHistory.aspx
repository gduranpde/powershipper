﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Shared/Default.Master" CodeBehind="CustomerImportHistory.aspx.cs" Inherits="PowerShipping.WebSite.Home.CustomerImportHistory" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../Controls/ProjectsList.ascx" TagName="ProjectsList" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="pageDiv">
        <table>
            <tr>
                <td>
                    <h3>Customer Data Import History</h3>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:radgrid id="RadGrid_History" runat="server" width="100%" gridlines="None"
                        autogeneratecolumns="false" pagesize="15" allowsorting="True" allowpaging="True"
                        skin="Windows7"
                        onneeddatasource="RadGrid_History_NeedDataSource"
                        onitemcreated="RadGrid_History_ItemCreated"
                        onitemdatabound="RadGrid_History_ItemDataBound"
						OnItemCommand="RadGrid_History_OnItemCommand">
            <ExportSettings ExportOnlyData="true" IgnorePaging="true">            
            </ExportSettings>
            <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
             AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true"
            >
            <CommandItemSettings                   
                    ShowExportToWordButton="true"
                    ShowExportToExcelButton="true"
                    ShowExportToCsvButton="true"
                    ShowExportToPdfButton="true"
                    />
            <Columns>             
                <telerik:GridBoundColumn DataField="ProjectName" HeaderText="Project Name" SortExpression="ProjectName"
                        UniqueName="ProjectName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        EditFormColumnIndex="0">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="FileName" HeaderText="File Name" SortExpression="FileName"
                        UniqueName="FileName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="ImportDate" HeaderText="Import Date" SortExpression="ImportDate"
                        UniqueName="ImportDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                   <telerik:GridBoundColumn DataField="UploadType" HeaderText="Upload Type" SortExpression="UploadType"
                        UniqueName="UploadType" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="CountRecords" HeaderText="Count Records" SortExpression="CountRecords"
                        UniqueName="CountRecords" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
				 <telerik:GridTemplateColumn DataField="ExceptionCount" HeaderText="Exception Count" SortExpression="ExceptionCount" 
						UniqueName="ExceptionCount" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
						<ItemTemplate>
							<asp:LinkButton ID="lbtnDownloadExp" runat="server" CommandName="DownloadExp" Text='<%# Eval("ExceptionCount") %>'
								CommandArgument='<%# Eval("ExceptionCount") %>'></asp:LinkButton>
						</ItemTemplate>
				  </telerik:GridTemplateColumn>
                  <telerik:GridBoundColumn DataField="UserName" HeaderText="User Name" SortExpression="UserName"
                        UniqueName="UserName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>                                
            </Columns>               
            </MasterTableView>
    </telerik:radgrid>
                </td>
            </tr>
        </table>


    </div>
</asp:Content>
