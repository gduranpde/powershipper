﻿<%@ Page Title="Tracking Number Batches" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" CodeBehind="TrackingNumberBatches.aspx.cs" Inherits="PowerShipping.WebSite.Home.TrackingNumberBatches" Theme="Default" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register src="../Controls/ProjectsList.ascx" tagname="ProjectsList" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="pageDiv">    
    
     <table>
     <tr>
            <td>              
                  <h3>Tracking Number Batches</h3>
            </td>
            <td>
            </td>  
            <td>
            </td>         
            <td align="center">
                 <uc1:ProjectsList ID="ProjectsList1" runat="server" visible="false" IsCompanyUse="true" />
            </td>
        </tr>
        <tr> 
           <td>
                Start Date
                <telerik:RadDatePicker ID="dpStartDateFilter" runat="server" MinDate="01-01-1990" MaxDate="01-01-2020"></telerik:RadDatePicker>
            </td>            
            <td>
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" CssClass="ptButton"/>
            </td> 
             <td>
                <asp:Button ID="btnClearFilter" runat="server" OnClick="btnClearFilter_Click" Text="Clear" CssClass="ptButton"/>
            </td>           
        </tr>
    </table>
    <br />
    
    <telerik:RadGrid ID="RadGrid_Batches" runat="server" Width="100%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True" 
              Skin="Windows7" 
        onneeddatasource="RadGrid_Batches_NeedDataSource"       
        OnDeleteCommand="RadGrid_Batches_DeleteCommand" 
        OnItemCreated="RadGrid_Batches_ItemCreated" 
        OnItemDataBound="RadGrid_Batches_ItemDataBound"     
        >
        <ExportSettings ExportOnlyData="true" IgnorePaging="true">            
            </ExportSettings>
            <ClientSettings>               
            </ClientSettings>
            <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
            AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true">
            <CommandItemSettings
                    ShowExportToWordButton="true"
                    ShowExportToExcelButton="true"
                    ShowExportToCsvButton="true"
                    ShowExportToPdfButton="true"
                    />
            <Columns>
             
                <telerik:GridButtonColumn UniqueName="DeleteColumn" Text="Delete" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmText="Are you sure delete this batch?"/>
                
               
                  <telerik:GridBoundColumn DataField="FileName" HeaderText="FileName" SortExpression="FileName"
                        UniqueName="FileName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        EditFormColumnIndex="0">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="ImportedBy" HeaderText="ImportedBy" SortExpression="ImportedBy"
                        UniqueName="ImportedBy" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridDateTimeColumn DataField="ImportedTime" HeaderText="ImportedTime" SortExpression="ImportedTime" ReadOnly="true"
                         UniqueName="ImportedTime" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true">                        
                  </telerik:GridDateTimeColumn>
                 <telerik:GridBoundColumn DataField="NumbersInBatch" HeaderText="NumbersInBatch" SortExpression="NumbersInBatch"
                        UniqueName="NumbersInBatch" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="ImportedSuccessfully" HeaderText="ImportedSuccessfully" SortExpression="ImportedSuccessfully"
                        UniqueName="ImportedSuccessfully" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="ImportedWithException" HeaderText="ImportedWithException" SortExpression="ImportedWithException"
                        UniqueName="ImportedWithException" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn> 
            </Columns>
            </MasterTableView>
    </telerik:RadGrid>
    
    <asp:Panel ID="resultPanel" runat="server" Visible="false" BackColor="Red">
        <asp:HiddenField ID="hiddenBatchId" runat="server" />
        <table>
            <tr>
                <td>
                    In Transit: <asp:Label ID="lbInTransit" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Delivered: <asp:Label ID="lbDelivered" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Returned: <asp:Label ID="lbReturned" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnProceed" runat="server" OnClick="btnProceed_Click" Text="Proceed" CssClass="ptButton"/>
                    <asp:Button ID="btnViewList" runat="server" OnClick="btnViewList_Click" Text="View List" CssClass="ptButton"/>
                    <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel" CssClass="ptButton"/>
                </td>
            </tr>
        </table>        
    </asp:Panel>
    </div>
</asp:Content>
