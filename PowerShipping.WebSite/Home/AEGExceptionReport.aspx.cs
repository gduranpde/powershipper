﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class AEGExceptionReport : Page
    {
        private readonly AuditReportPresenter presenter = new AuditReportPresenter();
        private bool _isFirstLoad = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList1.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }
            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;

            if (!IsPostBack)
            {
                List<KitType> list = presenter.GetAllKitTypes();
                var items = new List<ListItem>();
                items.Add(new ListItem("All", ""));
                foreach (KitType kit in list)
                {
                    items.Add(new ListItem(kit.KitName, kit.Id.ToString()));
                }
                RadComboBox_KitType.DataSource = items;
                RadComboBox_KitType.DataBind();
            }

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "AEG Exception Report");
                _isFirstLoad = true;
            }
        }

        private void ProjectsList1_OnProjectChanged(string projectId)
        {
            RadGrid_Exceptions_NeedDataSource(null, null);
            RadGrid_Exceptions.Rebind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            RadGrid_Exceptions_NeedDataSource(null, null);
            RadGrid_Exceptions.Rebind();
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            tbPONumberFilter.Text = null;
            dpStartDateFilter.SelectedDate = null;
            dpEndDateFilter.SelectedDate = null;
            RadComboBox_KitType.SelectedValue = null;
        }

        protected void RadGrid_Exceptions_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                var spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                var addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGrid_Exceptions_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (!_isFirstLoad)
                RadGrid_Exceptions.DataSource = GetFilteredListExceptions();
        }

        protected void RadGrid_Exceptions_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item is GridDataItem) && !e.Item.IsInEditMode)
            {
                var gridItem = (GridDataItem)e.Item;
                try
                {
                    Double myDouble = Convert.ToDouble(gridItem["Phone1"].Text);
                    gridItem["Phone1"].Text = myDouble.ToString("###-###-####");

                    Double myDouble2 = Convert.ToDouble(gridItem["Phone2"].Text);
                    gridItem["Phone2"].Text = myDouble2.ToString("###-###-####");
                }
                catch
                {
                }
            }
        }

        private DataSet GetFilteredListExceptions()
        {
            string poNum = tbPONumberFilter.Text;
            DateTime? startDate = dpStartDateFilter.SelectedDate;
            DateTime? endDate = dpEndDateFilter.SelectedDate;

            if (endDate != null)
                endDate = endDate.Value.AddDays(1);

            Guid? kitType = null;
            string kit = RadComboBox_KitType.SelectedValue;
            if (!string.IsNullOrEmpty(kit))
                kitType = new Guid(RadComboBox_KitType.SelectedValue);

            if (poNum == string.Empty)
                poNum = null;

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList1.ProjectId;
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = ProjectsList1.UserId;
            }

            if (ProjectsList1.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList1.CompanyId;
            }

            return presenter.GetAEGReportExceptions(poNum, startDate, endDate, kitType, userId, projectId, companyId);
        }
    }
}