﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;
using System.Data;
using System.Web.Security;
using System.Drawing;
using PowerShipping.ImportExport;

namespace PowerShipping.WebSite.Home
{
    public partial class ConsumerRequestBatches : Page
    {
        BatchesPresenter presenter = new BatchesPresenter();
        //PG31
        UserManagementPresenter presenterAdv = new UserManagementPresenter();
        ShipmentDetailsPresenter presentersd = new ShipmentDetailsPresenter();
        ShipmentsPresenter PresenterShipment = new ShipmentsPresenter();
        private ConsumerRequestsPresenter presenterCR = new ConsumerRequestsPresenter();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList1.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }

            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Consumer Request Batches");
                //PG31
                ddlShipper.DataSource = presenterAdv.GetAllShipers();
                ddlShipper.DataBind();
                ddlShipper.SelectedIndex = 2;
                ListBox_Kit_type.DataSource = presentersd.GetListOfKitTypes();
                ListBox_Kit_type.DataBind();
                //PG31
            }
        }

        void ProjectsList1_OnProjectChanged(string projectId)
        {
            RadGrid_Batches.Rebind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            RadGrid_Batches.Rebind();
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            dpStartDateFilter.SelectedDate = null;
        }

        protected void RadGrid_Batches_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                Button spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                LinkButton addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGrid_Batches_ItemDataBound(object sender, GridItemEventArgs e)
        {

        }

        protected void RadGrid_Batches_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_Batches.DataSource = GetFilteredList();
        }

        protected void RadGrid_Batches_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.DeleteCommandName)
                {
                    Guid id = new Guid((e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"].ToString());
                    presenter.DeleteConsumerRequestBatch(id);
                    RadGrid_Batches.Rebind();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                Label l = new Label();
                l.Text = "Unable to delete Batch. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_Batches.Controls.Add(l);
            }
        }
        //PG31
        protected void RadGrid_Batches_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "Process")
            {

                Guid id = new Guid((e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"].ToString());
                if(id!=null)
                ViewState["BatchId"] = id;
                bool RequestReceivedPresent = presenter.ConsumerRequestStatusCheck(id);

                DataSet ds = presenter.ProcessImportData(id);
                DataTable dt = ds.Tables[0];
                bool bhaveRelation = false;

                if (dt != null)
                {
                    if (dt.Rows.Count != 0)
                    {
                        bhaveRelation = true;
                    }
                }
                if (RequestReceivedPresent == false || bhaveRelation == true)
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ImportProcessFailed", "ImportProcessFailed();", true);

                }
                else
                {

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ImportActivityPassPopup", "ImportActivityPassPopup();", true);

                }
            }
        }
        private List<ConsumerRequestBatch> GetFilteredList()
        {
            DateTime? startDate = dpStartDateFilter.SelectedDate;

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList1.ProjectId;
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = ProjectsList1.UserId;
            }

            if (ProjectsList1.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList1.CompanyId;
            }

            return presenter.GetConsumerRequestBatches(startDate, userId, projectId, companyId);

        }
        //PG31
        protected void btnSubmit_Click(object sender, EventArgs e)
        { int sortOrder = 0;
        
            var consumerPresenter = new ConsumerRequestsPresenter();
           
            With.Transaction(() =>
             {

                 try
                 {
                     Guid id = new Guid(ViewState["BatchId"].ToString());
                     List<ConsumerRequest> crList = consumerPresenter.GetByBatch(id);

                     Shipment objshipment = new Shipment();
                     objshipment.Id = Guid.NewGuid();
                     objshipment.DateCustomerWillShip = Convert.ToDateTime(TxtReceiptDate.Text);
                     objshipment.DateLabelsNeeded = Convert.ToDateTime(TxtReceiptDate.Text);
                     objshipment.KitTypeId = new Guid(ListBox_Kit_type.SelectedValue);
                     objshipment.ShipperId = new Guid(ddlShipper.SelectedValue);
                     objshipment.PurchaseOrderNumber = Txt_PurchaseOrderNumber.Text;
                     objshipment.CreatedBy = Membership.GetUser().UserName;
                     objshipment.ProjectId = ProjectsList1.ProjectId;
                     PresenterShipment.Save(objshipment);                     
                     var sd = new ShipmentDetail();                    
                     foreach (var critem in crList)
                     {   sortOrder = ++sortOrder;
                         sd.Id = Guid.NewGuid();
                         sd.ShipmentId = objshipment.Id;
                         sd.ConsumerRequestId = critem.Id;
                         sd.ShipmentNumber = Convert.ToString((presentersd.GetMaxShippingNumber() + 1).ToString());
                         sd.SortOrder = sortOrder;
                         sd.HasException = false;
                         sd.IsReship = critem.IsReship;
                         sd.ShipDate = Convert.ToDateTime(TxtReceiptDate.Text);
                         sd.ShippingStatus = ShipmentShippingStatus.Delivered;
                         sd.ReshipStatus = ShipmentReshipStatus.NA;
                         sd.ShippingStatusDate = Convert.ToDateTime(TxtReceiptDate.Text);
                         sd.ProjectId = ProjectsList1.ProjectId;
                         sd.ShipperId = new Guid(ddlShipper.SelectedValue);
                         sd.AccountNumber = critem.AccountNumber;
                         
                         if (!presentersd.IsShipmentNumberAlreadyExists(sd.ShipmentNumber))
                         {
                             critem.Status = ConsumerRequestStatus.Delivered;
                             critem.ReceiptDate = Convert.ToDateTime(TxtReceiptDate.Text);
                             presentersd.Save(sd);
                             presenterCR.Save(critem);

                             RadGrid_Batches.Rebind();
                         }
                         else
                         {
                             var l = new Label();
                             l.Text = "Unable to update Shipment. Reason: " +
                                      "Shipment Number already exists";
                             l.ControlStyle.ForeColor = Color.Red;
                             RadGrid_Batches.Controls.Add(l);
                         }
                     }
                 }
                 catch (Exception ex)
                 {
                     Log.LogError(ex);

                     var l = new Label();
                     l.Text = "Unable to update Shipment. Reason: " + ex.Message;
                     l.ControlStyle.ForeColor = Color.Red;
                     RadGrid_Batches.Controls.Add(l);
                 }

             });
        }
    }
}