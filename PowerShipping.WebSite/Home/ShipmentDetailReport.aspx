﻿<%@ Page Title="Shipment Detail Report" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" CodeBehind="ShipmentDetailReport.aspx.cs" Inherits="PowerShipping.WebSite.Home.ShipmentDetailReport" Theme="Default" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register src="../Controls/ProjectsList.ascx" tagname="ProjectsList" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<div class="pageDiv">
    
    
    <table>
        <tr>
            <td>              
                  <h3>Shipment Detail Report</h3>
            </td>           
            <td colspan=4 align="center">
                 <uc1:ProjectsList ID="ProjectsList1" runat="server"  IsCompanyUse="true"/>
            </td>
        </tr>
        <tr>
            <td>
                Start Date
                <telerik:RadDatePicker ID="dpStartDateFilter" runat="server" MinDate="01-01-1990" MaxDate="01-01-2020"></telerik:RadDatePicker>
            </td>
            <td>
                End Date
                <telerik:RadDatePicker ID="dpEndDateFilter" runat="server" MinDate="01-01-1990" MaxDate="01-01-2020"></telerik:RadDatePicker>
            </td>            
            <td>Status
                <telerik:RadComboBox ID="RadComboBox_Status" runat="server" Width="150px" DataTextField="Text"  DataValueField="Value">                          
            </telerik:RadComboBox>
        </td>
        <td>
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" CssClass="ptButton"/>
            </td>
            <td>
                <asp:Button ID="btnClearFilter" runat="server" OnClick="btnClearFilter_Click" Text="Clear" CssClass="ptButton"/>
            </td> 
        </tr>
    </table>
  
    <telerik:RadGrid ID="RadGrid_Shipments" runat="server" Width="100%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True" 
              Skin="Windows7" 
        onneeddatasource="RadGrid_Shipments_NeedDataSource"
           OnItemCreated="RadGrid_Shipments_ItemCreated"
            OnItemDataBound="RadGrid_Shipments_ItemDataBound"   
                  
            >
            <ExportSettings ExportOnlyData="true" IgnorePaging="true">            
            </ExportSettings>
            <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
             AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true"
            >
            <CommandItemSettings                   
                    ShowExportToWordButton="true"
                    ShowExportToExcelButton="true"
                    ShowExportToCsvButton="true"
                    ShowExportToPdfButton="true"
                    />
            <Columns> 
                 <telerik:GridDateTimeColumn DataField="ShipDate" HeaderText="Ship Date" SortExpression="ShipDate"
                        UniqueName="ShipDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        DataFormatString="{0:MM/dd/yyyy}">
                        </telerik:GridDateTimeColumn>            
                <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="AccountNumber" SortExpression="AccountNumber"
                        UniqueName="AccountNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        EditFormColumnIndex="0">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="AccountName" HeaderText="Name" SortExpression="AccountName"
                        UniqueName="AccountName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="Address1" HeaderText="Address1" SortExpression="Address1"
                        UniqueName="Address1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="Address2" HeaderText="Address2" SortExpression="Address2"
                        UniqueName="Address2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="City" HeaderText="City" SortExpression="City"
                        UniqueName="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="State" HeaderText="State" SortExpression="State"
                        UniqueName="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="ZipCode" HeaderText="ZIP" SortExpression="ZipCode"
                        UniqueName="ZipCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" MaxLength="5">
                        </telerik:GridBoundColumn>
                   <telerik:GridBoundColumn DataField="FedExTrackingNumber" HeaderText="FedExTrackingNumber" SortExpression="FedExTrackingNumber"
                        UniqueName="FedExTrackingNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>              
                   <telerik:GridBoundColumn DataField="PurchaseOrderNumber" HeaderText="PO Number" SortExpression="PurchaseOrderNumber"
                        UniqueName="PurchaseOrderNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="KitTypeName" HeaderText="KitType" SortExpression="KitTypeName"
                        UniqueName="KitTypeName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>    
                    <telerik:GridCheckBoxColumn DataField="IsReship" HeaderText="Reship?" SortExpression="IsReship"
                        UniqueName="IsReship" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true">
                        </telerik:GridCheckBoxColumn>   
                        
                <telerik:GridDateTimeColumn DataField="ShippingStatusDate" HeaderText="ShippingStatusDate" SortExpression="ShippingStatusDate" Visible="false"
                        UniqueName="ShippingStatusDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true" 
                        DataFormatString="{0:MM/dd/yyyy}">
                        </telerik:GridDateTimeColumn>
                  <telerik:GridBoundColumn DataField="FedExStatusCode" HeaderText="FedExStatusCode" SortExpression="FedExStatusCode" Visible="false"
                        UniqueName="FedExStatusCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                        
              
            </Columns>               
            </MasterTableView>
    </telerik:RadGrid>
</div>
</asp:Content>
