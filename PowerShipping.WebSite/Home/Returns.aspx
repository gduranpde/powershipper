﻿<%@ Page Title="Returns" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true"
    CodeBehind="Returns.aspx.cs" Inherits="PowerShipping.WebSite.Home.Returns" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../Controls/ProjectsList.ascx" TagName="ProjectsList" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Controls/ReshipSpecialProcessingCard.ascx" TagName="ReshipSpecialProcessingCard"
    TagPrefix="uc1" %>
<%@ Register Src="../Controls/ReturnsDetailFilter.ascx" TagName="ReturnsDetailFilter"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <uc2:ReturnsDetailFilter ID="ReturnsDetailFilter1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">
        function SavePopup() {

            var a1 = document.getElementById("ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_ShippingAddressdialog_TxtAddress1").value;
            var a2 = document.getElementById("ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_ShippingAddressdialog_TxtAddress2").value;
            var c = document.getElementById("ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_ShippingAddressdialog_TxtCity").value;
            var s = document.getElementById("ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_ShippingAddressdialog_DDLState").value;
            var e = document.getElementById("ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_ShippingAddressdialog_DDLState");
            var strUser = e.options[e.selectedIndex].text;
            //alert(strUser);
            var z = document.getElementById("ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_ShippingAddressdialog_TxtZip").value;
            PageMethods.Ajax_SaveShipmentDetails(a1, a2, c, strUser, z, lblId);
            var lblId = document.getElementById("ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_ShippingAddressdialog_lblGetID").innerHTML;
            document.getElementById("ctl00_ContentPlaceHolder1_pnlList").style.display = "block";
            document.getElementById("ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_ShippingAddressdialog_pnlPopUp2").style.display = "none";
            document.getElementById("ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_backgroundElement1").style.display = "none";
            BindPopup(a1, a2, c, strUser, z, lblId);
            return false;
        }
        function BindPopup(a1, a2, c, s, z, lblId) {

            //alert(lblId);
            var addr1 = "ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_rp_ctl" + lblId + "_lbAddress";
            var city = "ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_rp_ctl" + lblId + "_lbCity";
            var state = "ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_rp_ctl" + lblId + "_lbState";
            var zip = "ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_rp_ctl" + lblId + "_lbZip";
            //            document.getElementById(addr1).innerHTML = a1.toString().toUpperCase();
            //            document.getElementById(city).innerHTML = c.toString().toUpperCase();
            //            document.getElementById(state).innerHTML = s.toString().toUpperCase();
            //            document.getElementById(zip).innerHTML = z.toString().toUpperCase();
            document.getElementById(addr1).innerHTML = a1.toString().toUpperCase();
            document.getElementById(city).innerHTML = c.toString().toUpperCase();
            document.getElementById(state).innerHTML = s.toString().toUpperCase();
            document.getElementById(zip).innerHTML = z.toString().toUpperCase();
        }

        function hidep2() {
            var innerPopup = document.getElementById("ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_ShippingAddressdialog_pnlPopUp2");

            if (innerPopup) {
                innerPopup.style.display = "none";
            }
            document.getElementById("ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_backgroundElement1").style.display = "none";
            return true;
        }
    </script>

    <div class="pageDiv">
        <table>
            <tr>
                <td>
                    <h3>
                        Returns</h3>
                </td>
                <td colspan="4" align="center">
                    <uc1:ProjectsList ID="ProjectsList1" runat="server" IsCompanyUse="true" />
                </td>
            </tr>
           <%-- <tr>
                <td>
                    Set status to selected items:
                    <telerik:RadComboBox ID="RadComboBox_Status" runat="server" DataTextField="Text"
                        DataValueField="Value">
                    </telerik:RadComboBox>
                   <asp:Button ID="btnStatusChange" runat="server" OnClick="btnStatusChange_Click" Text="Save"
                        CssClass="ptButton" />
                </td>
            </tr>--%>
        </table>
        <telerik:RadGrid ID="RadGrid_Returns" runat="server" Width="100%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True"
            Skin="Windows7" OnNeedDataSource="RadGrid_Returns_NeedDataSource"
            OnUpdateCommand="RadGrid_Returns_UpdateCommand" OnItemCreated="RadGrid_Returns_ItemCreated"
            OnItemDataBound="RadGrid_Returns_ItemDataBound" OnItemCommand="RadGrid_Returns_ItemCommand"
            EnableLinqExpressions="false">
            <ExportSettings ExportOnlyData="true" IgnorePaging="true">
            </ExportSettings>
            <ClientSettings>
                <Selecting AllowRowSelect="true" />
                <Resizing AllowColumnResize="True"></Resizing>
            </ClientSettings>
            <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true">
                <CommandItemSettings ShowExportToWordButton="true" ShowExportToExcelButton="true"
                    ShowExportToCsvButton="true" ShowExportToPdfButton="true" />
                <Columns>
                    <%--<telerik:GridClientSelectColumn UniqueName="ClientSelectColumn">
                        <HeaderStyle Width="30px" />
                    </telerik:GridClientSelectColumn>--%>
                    <telerik:GridEditCommandColumn UniqueName="EditCommandColumn">
                        <HeaderStyle Width="30px" />
                    </telerik:GridEditCommandColumn>
                    <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="Account Number" SortExpression="AccountNumber"
                        ReadOnly="true" UniqueName="AccountNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" EditFormColumnIndex="0">
                        <HeaderStyle Width="110px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FedExTrackingNumber" HeaderText="Tracking Number"
                        SortExpression="FedExTrackingNumber" ReadOnly="true" UniqueName="FedExTrackingNumber"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        <HeaderStyle Width="160px" />
                    </telerik:GridBoundColumn>
                    <%--<telerik:GridBoundColumn DataField="FedExStatusCode" HeaderText="Code" SortExpression="FedExStatusCode"
						ReadOnly="true" UniqueName="FedExStatusCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
						ShowFilterIcon="false">
						<HeaderStyle Width="60px" />
					</telerik:GridBoundColumn>--%>
                    <telerik:GridBoundColumn DataField="PurchaseOrderNumber" HeaderText="PO Number" SortExpression="PurchaseOrderNumber"
                        ReadOnly="true" UniqueName="PurchaseOrderNumber" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        <HeaderStyle Width="80px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ShipmentNumber" HeaderText="Shipment #" SortExpression="ShipmentNumber"
                        ReadOnly="true" UniqueName="ShipmentNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                        <HeaderStyle Width="110px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AccountName" HeaderText="Name" SortExpression="AccountName"
                        UniqueName="AccountName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                        <HeaderStyle Width="160px" />
                    </telerik:GridBoundColumn>
                     <telerik:GridBoundColumn DataField="KitTypeName" HeaderText="Kit" SortExpression="KitTypeName"
                        UniqueName="KitTypeName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                         <HeaderStyle Width="120px"/>
                        </telerik:GridBoundColumn>
                    <%--<telerik:GridTemplateColumn UniqueName="KitTypeName" DataField="KitTypeName" HeaderText="Kit"
                        SortExpression="KitTypeName" ReadOnly="true">
                        <ItemTemplate>
                            <%# Eval("KitTypeName")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList runat="server" ID="ddlKitType" DataValueField="Id" DataTextField="TextKitLabel">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <HeaderStyle Width="75px" />
                    </telerik:GridTemplateColumn>--%>
                    <%-- <telerik:GridBoundColumn DataField="Address1" HeaderText="Address1" SortExpression="Address1"
                        UniqueName="Address1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" Visible="false">
                    </telerik:GridBoundColumn>--%>
                    <telerik:GridBoundColumn DataField="Address1" HeaderText="CRAddress1" SortExpression="CRAddress1"
                        UniqueName="CRAddress1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" Display="false">
                        <HeaderStyle Width="75px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Address2" HeaderText="CRAddress2" SortExpression="CRAddress2"
                        UniqueName="CRAddress2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" Display="false">
                        <HeaderStyle Width="75px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="City" HeaderText="CRCity" SortExpression="CRCity"
                        UniqueName="CRCity" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" Display="false">
                        <HeaderStyle Width="75px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="State" HeaderText="CRState" SortExpression="CRState"
                        UniqueName="CRState" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" Display="false">
                        <HeaderStyle Width="75px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ZipCode" HeaderText="CRZipCode" SortExpression="CRZipCode"
                        UniqueName="CRZipCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" MaxLength="5" Display="false">
                        <HeaderStyle Width="75px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email"
                        UniqueName="Email" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" Visible="false">
                    </telerik:GridBoundColumn>
                    <%--<telerik:GridBoundColumn DataField="Phone1" HeaderText="Phone1" SortExpression="Phone1"
                        UniqueName="Phone1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" Visible="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="Phone2" HeaderText="Phone2" SortExpression="Phone2"
                        UniqueName="Phone2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" Visible="false">
                        </telerik:GridBoundColumn>  --%>
                    <telerik:GridMaskedColumn DataField="Phone1" HeaderText="Phone 1" SortExpression="Phone1"
                        Mask="###-###-####" Visible="false" UniqueName="Phone1" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false" DataFormatString="{0:(###)###-####}">
                    </telerik:GridMaskedColumn>
                    <telerik:GridMaskedColumn DataField="Phone2" HeaderText="Phone 2" SortExpression="Phone2"
                        Mask="###-###-####" Visible="false" UniqueName="Phone2" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false" DataFormatString="{0:(###)###-####}">
                    </telerik:GridMaskedColumn>
                    <telerik:GridBoundColumn DataField="ShippingStatusSort" HeaderText="Shipping Status"
                        SortExpression="ShippingStatusSort" ReadOnly="true" UniqueName="ShippingStatusSort"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        <HeaderStyle Width="65px" />
                    </telerik:GridBoundColumn>
                   <%-- <telerik:GridTemplateColumn UniqueName="ShippingStatus" DataField="ShippingStatus"
                        HeaderText="ShippingStatus" ReadOnly="true" Visible="true" SortExpression="ShippingStatus">
                        <ItemTemplate>
                            <%# Eval("ShippingStatus")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList runat="server" ID="ddlShippingStatus" DataValueField="Value" DataTextField="Text">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <HeaderStyle Width="100px" />
                    </telerik:GridTemplateColumn>--%>
                    <telerik:GridBoundColumn DataField="ReshipStatusSort" HeaderText="Reship Status"
                        SortExpression="ReshipStatusSort" ReadOnly="true" UniqueName="ReshipStatusSort"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        <HeaderStyle Width="75px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn UniqueName="ReshipStatus" DataField="ReshipStatus" HeaderText="ReshipStatus"
                        SortExpression="ReshipStatus" Visible="false">
                        <ItemTemplate>
                            <%# Eval("ReshipStatus")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList runat="server" ID="ddlReshipStatus" DataValueField="Value" DataTextField="Text">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <HeaderStyle Width="105px" />
                    </telerik:GridTemplateColumn>
                    <%--<telerik:GridTemplateColumn  UniqueName="ShippingStatusDate" DataField="ShippingStatusDate" HeaderText="Shipping Status Date" >
                          <ItemTemplate>
                            <%# Eval("ShippingStatusDate")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadDatePicker runat="server" ID="datePickerEdit"  MinDate="01-01-1990" MaxDate="01-01-2020" FocusedDate="06-01-2010" >
                            </telerik:RadDatePicker>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn> --%>
                    <telerik:GridDateTimeColumn DataField="ReturnDate" HeaderText="Return Date" SortExpression="ReturnDate"
                        UniqueName="ReturnDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" DataFormatString="{0:MM/dd/yyyy}">
                        <HeaderStyle Width="80px" />
                    </telerik:GridDateTimeColumn>
                    <telerik:GridDateTimeColumn DataField="ShippingStatusDate" HeaderText="Status Date"
                        SortExpression="ShippingStatusDate" UniqueName="ShippingStatusDate" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" DataFormatString="{0:MM/dd/yyyy}">
                        <HeaderStyle Width="80px" />
                    </telerik:GridDateTimeColumn>
                    <telerik:GridBoundColumn DataField="Notes" HeaderText="Notes" SortExpression="Notes"
                        UniqueName="Notes" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ShipperCode" HeaderText="Shipper" SortExpression="ShipperCode"
                        UniqueName="ShipperCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                        <HeaderStyle Width="65px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CompanyCode" HeaderText="Comp" SortExpression="CompanyCode"
                        UniqueName="CompanyCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                        <HeaderStyle Width="55px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="OperatingCompany" HeaderText="Op Co" SortExpression="OperatingCompany"
                        UniqueName="OperatingCompany" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                        <HeaderStyle Width="55px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ProjectCode" HeaderText="Project" SortExpression="ProjectCode"
                        UniqueName="ProjectCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                        <HeaderStyle Width="55px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ShipmentDetailAddress1" HeaderText="Address1"
                        SortExpression="ShipmentDetailAddress1" UniqueName="ShipmentDetailAddress1" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ShipmentDetailCity" HeaderText="City" SortExpression="ShipmentDetailCity"
                        UniqueName="ShipmentDetailCity" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ShipmentDetailState" HeaderText="State" SortExpression="ShipmentDetailState"
                        UniqueName="ShipmentDetailState" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                        <HeaderStyle Width="55px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ShipmentDetailZip" HeaderText="Zip" SortExpression="ShipmentDetailZip"
                        UniqueName="ShipmentDetailZip" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                        <HeaderStyle Width="55px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ReasonCode" HeaderText="Return Reason" SortExpression="ReasonCode"
                        UniqueName="ReasonCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                        <HeaderStyle Width="55px" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings UserControlName="../Controls/ReturnCard.ascx" EditFormType="WebUserControl">
                    <EditColumn UniqueName="EditCommandColumn1">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <ClientSettings EnableRowHoverStyle="true">
                <Selecting AllowRowSelect="True" />
            </ClientSettings>
        </telerik:RadGrid>
        <asp:Panel ID="pnl1" runat="server">
        </asp:Panel>
        <asp:Panel ID="pnlList" runat="server" CssClass="modalPopupList">
            <table>
                <tr>
                    <td>
                        <div style="height: 350px; overflow: auto; border: solid 1px #CCC;">
                            <uc1:ReshipSpecialProcessingCard ID="reshipSpecialProcessingCard" runat="server" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="padding-top: 10px;">
                        <asp:Button ID="btnOkReship" runat="server" Text="Ok" OnClick="btnOkReship_OnClick"
                            Width="80" />
                        <asp:Button ID="btnCancelReship" runat="server" Text="Cancel" Width="80" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="mpeList" runat="server" PopupControlID="pnlList"
            TargetControlID="pnl1" OkControlID="btnCancelReship" BackgroundCssClass="modalBackground">
        </ajaxToolkit:ModalPopupExtender>
        <asp:Panel ID="pnl2" runat="server">
        </asp:Panel>
        <asp:Panel ID="pnlPopUp" runat="server" CssClass="modalPopupError">
            <table width="100%">
                <tr align="center">
                    <td align="center">
                        <asp:Label ID="lbPopUp" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr align="center">
                    <td align="center">
                        <asp:Button ID="btnCancelMpe" runat="server" Text="Ok" Width="80" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="mpe" runat="server" PopupControlID="pnlPopUp"
            TargetControlID="pnl2" OkControlID="btnCancelMpe" BackgroundCssClass="modalBackground">
        </ajaxToolkit:ModalPopupExtender>

        <script type="text/javascript">

            function Expand(itemID) {
                var Grid = $find('<%=RadGrid_Returns.ClientID %>');
                var scrollArea = document.getElementById('ctl00_ContentPlaceHolder1_RadGrid_Returns')
                var rowElement = document.getElementById(itemID);
                window.scrollTo(0, rowElement.offsetTop + 200);
            }
        </script>

    </div>
</asp:Content>