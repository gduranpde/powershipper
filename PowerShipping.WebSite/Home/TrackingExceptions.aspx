﻿<%@ Page Title="Tracking Exceptions" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" CodeBehind="TrackingExceptions.aspx.cs" Inherits="PowerShipping.WebSite.Home.TrackingExceptions" Theme="Default"%>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register src="../Controls/ProjectsList.ascx" tagname="ProjectsList" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<div class="pageDiv">
   
    
    <table>
        <tr>
            <td>              
                  <h3>Tracking Exceptions</h3>
            </td>           
            <td colspan=4 align="center">
                 <uc1:ProjectsList ID="ProjectsList1" runat="server" IsCompanyUse="true" />
            </td>
        </tr>
        <tr>
       
            <td>
                Account Number 
                <asp:TextBox ID="tbAccountNumberFilter" runat="server"></asp:TextBox>         
            </td>
            <td>
                Tracking Number
                <asp:TextBox ID="tbTrackingNumberFilter" runat="server"></asp:TextBox>
            </td>
            <td>
                Purchase Order Number
                <asp:TextBox ID="tbPurchaseOrderNumberFilter" runat="server"></asp:TextBox>
            </td>            
            <td>
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" CssClass="ptButton"/>
            </td>
             <td>
                <asp:Button ID="btnClearFilter" runat="server" OnClick="btnClearFilter_Click" Text="Clear" CssClass="ptButton"/>
            </td>
        </tr>
    </table>
  
    
    <telerik:RadGrid ID="RadGrid_TrackingEx" runat="server" Width="100%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True" 
              Skin="Windows7" 
        onneeddatasource="RadGrid_TrackingEx_NeedDataSource"
        OnUpdateCommand="RadGrid_TrackingEx_UpdateCommand"
        OnDeleteCommand="RadGrid_TrackingEx_DeleteCommand"
        OnItemCreated="RadGrid_TrackingEx_ItemCreated" 
        OnItemDataBound="RadGrid_TrackingEx_ItemDataBound" 
        OnItemCommand="RadGrid_TrackingEx_ItemCommand"    
        >
        <ExportSettings ExportOnlyData="true" IgnorePaging="true">            
            </ExportSettings>
            <ClientSettings >  
                        <Selecting AllowRowSelect="true" />
                        
            </ClientSettings>
            <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
             AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true">
            <CommandItemSettings
                    ShowExportToWordButton="true"
                    ShowExportToExcelButton="true"
                    ShowExportToCsvButton="true"
                    ShowExportToPdfButton="true"
                    />
            <Columns>
             <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" >
                    </telerik:GridEditCommandColumn>
                <telerik:GridButtonColumn UniqueName="DeleteColumn" Text="Delete" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmText="Are you sure delete this row?"/>
                
                
                 <telerik:GridBoundColumn DataField="PurchaseOrderNumber" HeaderText="PO #" SortExpression="PurchaseOrderNumber"
                        UniqueName="PurchaseOrderNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        EditFormColumnIndex="0">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="TrackingNumber" HeaderText="TrackingNumber" SortExpression="TrackingNumber"
                        UniqueName="TrackingNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        EditFormColumnIndex="0">
                        </telerik:GridBoundColumn>
                
                
                 <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="Account #" SortExpression="AccountNumber"
                        UniqueName="AccountNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        EditFormColumnIndex="0">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="AccountName" HeaderText="AccountName" SortExpression="AccountName"
                        UniqueName="AccountName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="Address1" HeaderText="Address1" SortExpression="Address1"
                        UniqueName="Address1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                   <telerik:GridBoundColumn DataField="Address2" HeaderText="Address2" SortExpression="Address2"
                        UniqueName="Address2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="City" HeaderText="City" SortExpression="City"
                        UniqueName="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="State" HeaderText="State" SortExpression="State"
                        UniqueName="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="ZipCode" HeaderText="ZipCode" SortExpression="ZipCode"
                        UniqueName="ZipCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        <HeaderStyle Width="80px"/>
                        </telerik:GridBoundColumn>
                   <telerik:GridBoundColumn DataField="Reference" HeaderText="Reference" SortExpression="Reference"
                        UniqueName="Reference" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>                                         
                   <telerik:GridBoundColumn DataField="Reason" HeaderText="Reason" SortExpression="Reason" ReadOnly="true"
                        UniqueName="Reason" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                  </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="ShipperCode" HeaderText="Shipper" SortExpression="ShipperCode"
                                    UniqueName="ShipperCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">                                
                                </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="CompanyCode" HeaderText="CompanyCode" SortExpression="CompanyCode"
                                    UniqueName="CompanyCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">                                
                                </telerik:GridBoundColumn> 
                  <telerik:GridBoundColumn DataField="ProjectCode" HeaderText="ProjectCode" SortExpression="ProjectCode"
                                    UniqueName="ProjectCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">                                
                                </telerik:GridBoundColumn>            
                
            </Columns>
              <%-- <EditFormSettings ColumnNumber="3" CaptionFormatString="Edit details for Tracking Exception"
                CaptionDataField="Id">
                <FormTableItemStyle Wrap="False"></FormTableItemStyle>
                <FormCaptionStyle CssClass="EditFormHeader"></FormCaptionStyle>
                <FormMainTableStyle GridLines="None" CellSpacing="0" CellPadding="3" Width="100%" />
                <FormTableStyle CellSpacing="0" CellPadding="2" CssClass="module" GridLines="Both"
                    Height="110px" Width="100%"  />
                <FormTableAlternatingItemStyle Wrap="False"></FormTableAlternatingItemStyle>
                <FormStyle Width="100%" BackColor="#eef2ea"></FormStyle>
                <EditColumn UpdateText="Save" UniqueName="EditCommandColumn1" CancelText="Cancel">
                </EditColumn>
                <FormTableButtonRowStyle HorizontalAlign="Left"></FormTableButtonRowStyle>
                
            </EditFormSettings>--%>
            <EditFormSettings UserControlName="../Controls/TrackingExceptionCard.ascx" EditFormType="WebUserControl">
                <EditColumn UniqueName="EditCommandColumn1">
                </EditColumn>
             </EditFormSettings>
            </MasterTableView>
    </telerik:RadGrid>
    
     <br />
    <table>
    <tr>
         <td>
                <asp:Button ID="btnValidate" runat="server" OnClick="btnValidate_Click" Text="ReSubmit" CssClass="ptButton"/>
            </td>
       </tr>
    <tr>        
            <td>
                <asp:Label ID="lbReuslt" runat="server" Text="" />
            </td>
        </tr>
    </table>
    
     <script type="text/javascript">

        function Expand(itemID) {
            var Grid = $find('<%=RadGrid_TrackingEx.ClientID %>');
            var scrollArea = document.getElementById('ctl00_ContentPlaceHolder1_RadGrid_TrackingEx')
            var rowElement = document.getElementById(itemID);
            window.scrollTo(0, rowElement.offsetTop + 200);
        }
        </script>
    
 </div>
</asp:Content>