﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Shared/Default.Master"
    CodeBehind="HEAFileConversion.aspx.cs" Inherits="PowerShipping.WebSite.Home.HEAFileConversion"
    Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../Controls/ProjectsList.ascx" TagName="ProjectsList" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script src="../js/main.js" type="text/javascript"></script>

    <div class="pageDiv">
        <table>
            <tr>
                <td>
                    <h3>
                        HEA File Conversion</h3>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadUpload ID="RadUpload1" runat="server" OnClick="clearValidatorText()"
                        ControlObjectsVisibility="None" />
                </td>
                <td>
                </td>
            </tr>
        </table>
        <div class="submitArea">
            <asp:Button runat="server" ID="SubmitButton" Text="Upload files" OnClick="SubmitButton_Click" />
            <br />
            <asp:Label ID="LblMessage" runat="server"></asp:Label>
        </div>
    </div>
</asp:Content>