﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;
using System.IO;

using System.Text;
using OfficeOpenXml;
using System.Xml;
using System.Drawing;
using OfficeOpenXml.Style;
using PowerShipping.Data;
using PowerShipping.WebSite.Controls;
using System.Threading;
using PowerShipping.Logging;
using System.Web.Security;

namespace PowerShipping.WebSite.Home
{

    
    public partial class ReconciliationReport : System.Web.UI.Page
    {
        ReportPresenter presenter = new ReportPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (StateList1.UserId != Guid.Empty)
            //{
             //   Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            //}
            
        }

        protected void RadGrid_Grid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_Grid.DataSource = presenter.GetAll();
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        { 
        
        }

        protected bool CheckValidDateTimeState(DateTime? startDate, DateTime? endDate, string state)
        {
            bool check = true;
            
            if (startDate != null && endDate != null && state != "----")
            {
                if (startDate.Value != null && endDate.Value != null)
                {
                    DateTime startDateNew = new DateTime(startDate.Value.Year, startDate.Value.Month, 1, 00, 00, 00);
                    DateTime endDateNew = new DateTime(endDate.Value.Year, endDate.Value.Month, 1, 00, 00, 01);
                    if (startDateNew > endDateNew) //|| (startDateNew.AddMonths(12) < endDateNew))
                    {
                        //lbMessages.Text = "End Date must be within 12 months of StartDate";
                        lbMessages.Text = "End Date must be after Start Date";
                        lbMessages.ControlStyle.ForeColor = Color.Red;
                        check = false;
                    }
                    else
                    {
                        List<Project> lstProj = DataProvider.Current.Project.GetActiveByState(Convert.ToInt32(StateList1.StateID));
                        List<OperatingCompany> lstOC = DataProvider.Current.OperatingCompany.GetActiveByState(Convert.ToInt32(StateList1.StateID));

                        if (lstProj.Count == 0)
                        {
                            lbMessages.Text = "There are no Projects for the specified state";
                            lbMessages.ControlStyle.ForeColor = Color.Red;
                            check = false;
                        }
                        else if (lstOC.Count == 0)
                        {
                            lbMessages.Text = "There are no Operating Companies for the specified state";
                            lbMessages.ControlStyle.ForeColor = Color.Red;
                            check = false;
                        }
                    }
                 }
            }
            else
            {
                lbMessages.Text = "Please fill all the report parameters";
                lbMessages.ControlStyle.ForeColor = Color.Red;
                check = false;
            }
            return check;
        }

        protected void btnGenerateExcel_Click(object sender, EventArgs e)
        {

            DateTime? startDateNull = dpStartDateFilter.SelectedDate;
            DateTime? endDateNull = dpEndDateFilter.SelectedDate;

            lbMessages.Text = "";
            

            if (CheckValidDateTimeState(startDateNull, endDateNull, StateList1.StateName)) 
            {
                DateTime startDate = Convert.ToDateTime(startDateNull);
                DateTime endDate = Convert.ToDateTime(endDateNull);

                string startMonthDate = startDate.Month.ToString();
                string endMonthDate = endDate.Month.ToString();
                string startYearDate = startDate.Year.ToString();
                string endYearDate = endDate.Year.ToString();

                string stateName = StateList1.StateName;
                int StateID = Convert.ToInt32(StateList1.StateID);
                string reportParams = stateName + "_" + startMonthDate + '_' + startYearDate + "_" + endMonthDate + '_' + endYearDate;
                string param =stateName + ";" + startMonthDate + '/' + startYearDate + ";" + endMonthDate + '/' + endYearDate;
                string reportName = "ReconciliationReport_" + reportParams;
                string userIDName = User.Identity.Name.ToString();
                string userIDGuidString = Membership.GetUser(userIDName).ProviderUserKey.ToString();
                Guid? userID = new Guid(userIDGuidString);
                int id = DataProvider.Current.Report.AddReport(reportName, param, userID);

                string shortfilePath = String.Format("ReconciliationReport_{0}_{1}.xlsx", reportParams, id);

                String rootPath = Server.MapPath("~");
                DirectoryInfo outputDir = new DirectoryInfo(rootPath);
                string fileName = String.Format("Reports/ReconciliationReport_{0}_{1}.xlsx", reportParams, id);
                string filePath = Path.Combine(outputDir.FullName,fileName);
                ReconReportData data = new ReconReportData();
                ReconReport rep = new ReconReport();

                Thread obj = new Thread(() => rep.GenerateReportForMonth(filePath, startDate, endDate, StateID, id, shortfilePath));
                obj.IsBackground = true;
                obj.Start();

                RadGrid_Grid.Rebind();

                return;
            }
        }
    }

    

}