﻿using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class AEGXMLDeliveryReportPhase2 : System.Web.UI.Page
    {
        private readonly AuditReportPresenter presenter = new AuditReportPresenter();
        ConsumerRequestsPresenter crPresenter = new ConsumerRequestsPresenter();
        private bool _isFirstLoad = false;
        //PG31
        DateTime dtMonthShipped;
        String sCatalogID;
        String AnalysisDate;
        string ReceiptDate;
        string ShipDate;
        Guid gdConsumerRequestId;
        int iQuantity;
        decimal Kw_Impact;
        decimal Kwh_Impact;
        float Kwh_Savings = 0;
        float Kw_savings = 0;
        DataTable dtEquipmentAttributes;
        XElement previousNode;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList1.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }

            if (!IsPostBack)
            {
                List<KitType> list = presenter.GetAllKitTypes();
                var items = new List<ListItem>();
                items.Add(new ListItem("All", ""));
                foreach (KitType kit in list)
                {
                    items.Add(new ListItem(kit.KitName, kit.Id.ToString()));
                }
                RadComboBox_KitType.DataSource = items;
                RadComboBox_KitType.DataBind();
            }

            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "AEG XML Delivery Report - Phase 2");
                _isFirstLoad = true;
            }
            previousNode = null;
        }
        private void ProjectsList1_OnProjectChanged(string projectId)
        {
            RadGrid_Accounts_NeedDataSource(null, null);
            RadGrid_Accounts.Rebind();
        }
        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            tbPONumberFilter.Text = null;
            dpStartDateFilter.SelectedDate = null;
            dpEndDateFilter.SelectedDate = null;
            RadComboBox_KitType.SelectedValue = null;
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            RadGrid_Accounts_NeedDataSource(null, null);
            RadGrid_Accounts.Rebind();
        }


        protected void RadGrid_Accounts_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                var spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                var addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGrid_Accounts_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (!_isFirstLoad)
            {
                DataSet ds = GetFilteredListAccounts();
                DataTable dtDeliveryGridData = ds.Tables[0];
                RadGrid_Accounts.DataSource = dtDeliveryGridData;

            }
        }

        protected void RadGrid_Accounts_ItemDataBound(object sender, GridItemEventArgs e)
        {

            if ((e.Item is GridDataItem) && !e.Item.IsInEditMode)
            {
                var gridItem = (GridDataItem)e.Item;
                try
                {
                    Double myDouble = Convert.ToDouble(gridItem["Phone1"].Text);
                    gridItem["Phone1"].Text = myDouble.ToString("###-###-####");

                    Double myDouble2 = Convert.ToDouble(gridItem["Phone2"].Text);
                    gridItem["Phone2"].Text = myDouble2.ToString("###-###-####");
                }
                catch
                {
                }
            }
        }

        private DataSet GetFilteredListAccounts()
        {
            string poNum = tbPONumberFilter.Text;
            DateTime? startDate = dpStartDateFilter.SelectedDate;
            DateTime? endDate = dpEndDateFilter.SelectedDate;

            if (endDate != null)
                endDate = endDate.Value.AddDays(1);

            Guid? kitType = null;
            string kit = RadComboBox_KitType.SelectedValue;
            if (!string.IsNullOrEmpty(kit))
                kitType = new Guid(RadComboBox_KitType.SelectedValue);

            if (poNum == string.Empty)
                poNum = null;

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList1.ProjectId;
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = ProjectsList1.UserId;
            }

            if (ProjectsList1.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList1.CompanyId;
            }

            return presenter.GetAEGReportPhase2(poNum, startDate, endDate, kitType, userId, projectId, companyId);
            //return presenter.GetAEGXMLReport(poNum, startDate, endDate, kitType, userId, projectId, companyId);
        }
        //PG31
        public string[] ParseName(string FullName)
        {
            string[] FirstLast;

            string[] Temp = FullName.Trim().Split(' ');

            int arrLen = Temp.Length;
            FirstLast = new string[arrLen];
            FirstLast[1] = Temp[arrLen - 1]; //Last Name

            string FirstName = string.Empty;

            for (int i = 0; i < arrLen - 1; i++)
            {
                FirstName = FirstName + " " + Temp[i];
            }

            FirstName = FirstName.Trim(); //PGF25
            FirstLast[0] = FirstName; //First Name

            return FirstLast;
        }
        public DateTime GetFirstDayOfMonth(DateTime dtDate)
        {
            // set return value to the first day of the month
            // for any date passed in to the method

            // create a datetime variable set to the passed in date
            DateTime dtFrom = dtDate;

            // remove all of the days in the month
            // except the first day and set the
            // variable to hold that date
            dtFrom = dtFrom.AddDays(-(dtFrom.Day - 1));

            // return the first day of the month
            return dtFrom;
        }
        protected void GenerateXML_Click(object sender, EventArgs e)
        {   //PG31

            DataTable dt = null;
            ProjectPresenter objProjPresenter = new ProjectPresenter();
            if (ProjectsList1.ProjectId == Guid.Empty)
            {
                LblWarningMessage.Text = "Please select a single project.";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AEGXMLReportFailed", "AEGXMLReportFailed();", true);
            }
            else
            {
                string ProgramID = objProjPresenter.GetProgramID(ProjectsList1.ProjectId);
                if (ProgramID == null || ProgramID == string.Empty)
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AEGXMLReportFailed", "AEGXMLReportFailed();", true);

                }
                else
                {
                    dt = NodeGenerator(dt);
                }
            }

        }
        public static DataTable GetDistinctRecords(DataTable dt, string[] Columns)
        {
            DataTable dtUniqRecords = new DataTable();
            dtUniqRecords = dt.DefaultView.ToTable(true, Columns);
            return dtUniqRecords;
        }
        private DataTable NodeGenerator(DataTable dt)
        {
            string poNum = tbPONumberFilter.Text;
            DateTime? startDate = dpStartDateFilter.SelectedDate;
            DateTime? endDate = dpEndDateFilter.SelectedDate;

            if (endDate != null)
                endDate = endDate.Value.AddDays(1);

            Guid? kitType = null;
            string kit = RadComboBox_KitType.SelectedValue;
            if (!string.IsNullOrEmpty(kit))
                kitType = new Guid(RadComboBox_KitType.SelectedValue);

            if (poNum == string.Empty)
                poNum = null;

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList1.ProjectId;
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = ProjectsList1.UserId;
            }

            if (ProjectsList1.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList1.CompanyId;
            }

            dt = presenter.GetAEGXMLReport(poNum, startDate, endDate, kitType, userId, projectId, companyId).Tables[0];


            XDocument _document = new XDocument();

            XElement content4;
            if (dt != null)
            {
                //Creating the parent node
                XElement _root = new XElement("systemRecord");

                string FirstName = string.Empty;
                string LastName = string.Empty;
                foreach (DataRow dr in dt.Rows)
                {
                    if (Convert.ToString(dr["AccountName"]).Contains(' '))
                    {
                        String[] Name = ParseName(Convert.ToString(dr["AccountName"]));
                        FirstName = Convert.ToString(Name[0]);
                        LastName = Convert.ToString(Name[1]);
                    }
                    else
                    {
                        FirstName = Convert.ToString(dr["AccountName"]);
                    }
                    if (dr["AnalysisDate"] != DBNull.Value && Convert.ToString(dr["AnalysisDate"]) != null || Convert.ToString(dr["AnalysisDate"]) != string.Empty)
                    {
                        AnalysisDate = Convert.ToDateTime(dr["AnalysisDate"]).ToString("MM/dd/yyyy");
                    }
                    if (dr["ReceiptDate"] != DBNull.Value && Convert.ToString(dr["ReceiptDate"]) != null || Convert.ToString(dr["ReceiptDate"]) != string.Empty)
                    {
                        ReceiptDate = Convert.ToDateTime(dr["ReceiptDate"]).ToString("MM/dd/yyyy");
                    }
                    if (dr["ShipDate"] != DBNull.Value && Convert.ToString(dr["ShipDate"]) != null || Convert.ToString(dr["ShipDate"]) != string.Empty)
                    {
                        ShipDate = Convert.ToDateTime(dr["ShipDate"]).ToString("MM/dd/yyyy");
                    }
                    if (dr["CRQuantity"] == DBNull.Value || dr["CRQuantity"] == null || Convert.ToInt32(dr["CRQuantity"]) == 0)
                    {
                        dr["CRQuantity"] = 1;
                    }
                    if (dr["Kwh_Savings"] != DBNull.Value && dr["Kwh_Savings"] != null)
                    {
                        Kwh_Savings = Convert.ToInt64(dr["Kwh_Savings"]) * Convert.ToInt32(dr["CRQuantity"]);
                    }
                    if (dr["Kw_savings"] != DBNull.Value && dr["Kw_savings"] != null)
                    {

                        Kw_savings = Convert.ToInt64(dr["Kw_savings"]) * Convert.ToInt32(dr["CRQuantity"]);
                    }
                    string refID_Full = Convert.ToString(dr["Id"]);
                    int refID_Length = refID_Full.Length;
                    int refID_StartIndex = refID_Length - 4;
                    string RefIDPortion = refID_Full.Substring(refID_StartIndex, 4);
                    string AccountNumber = Convert.ToString(dr["AccountNumber"]);

                    string refIDConcat = "PD_" + RefIDPortion + AccountNumber + "_" + ReceiptDate;
                    string refID;
                    string contactRefIDPortion = string.Empty;

                    string contactRefID = "PD_Pri_Cont_" + Convert.ToString(dr["AccountNumber"]);
                    if (contactRefID.Length > 38)
                    {
                        contactRefIDPortion = contactRefID.Substring(1, 38);
                    }
                    else
                    {
                        contactRefIDPortion = contactRefID;

                    }
                    if (refIDConcat.Length > 50)
                    {

                        refID = refIDConcat.Substring(1, 50);
                    }
                    else
                    {

                        refID = refIDConcat;
                    }
                    if (dr["ReceiptDate"] != DBNull.Value && dr["ReceiptDate"] != null)
                    { dtMonthShipped = GetFirstDayOfMonth(Convert.ToDateTime(dr["ReceiptDate"])); }

                    string ConsumerRequestId;
                    ConsumerRequestId = Convert.ToString(dr["Id"]);
                    gdConsumerRequestId = new Guid(ConsumerRequestId);
                    DataSet dsEA = crPresenter.GetEquipmentAttributes(gdConsumerRequestId);
                    dtEquipmentAttributes = dsEA.Tables[0];
                    content4 = new XElement("importRecord",
                           (dr["ProgramID"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.programNumber, Convert.ToString(dr["ProgramID"])) : null),
                           (refID.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.refID, refID) : null),
                           (FirstName.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.customerFirstname, FirstName.Trim()) : null),
                           (LastName.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.customerLastname, LastName) : null),
                           (dr["AccountNumber"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.customerAccountNumber, Convert.ToString(dr["AccountNumber"])) : null),
                           (dr["AccountNumber"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.primaryContactId, "PD_Pri_Cont_" + Convert.ToString(dr["AccountNumber"])) : null),
                           (dr["AccountNumber"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.premiseContactId, "PD_Pri_Cont_" + Convert.ToString(dr["AccountNumber"])) : null),
                           new XElement(XmlFormatter.status, "Delivered"),
                           (AnalysisDate.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.applicationDate, AnalysisDate) : null),
                    new XElement("importContacts",
                         ImportContactNode(_root, dr, contactRefIDPortion, FirstName, LastName)),
                         ImportApplicationNode(_root, dr),
                         EquipmentAttributeNode(_root, dr)
                    );
                    _root.Add(content4);
                }

                _document.Add(_root);

                _document.Save(Server.MapPath("~/Uploads/AEG_DeliveryReport.xml"), SaveOptions.None);
                string path = Server.MapPath("~/Uploads/AEG_DeliveryReport.xml");
                string filename = "AEG_DeliveryReport.xml";
                Response.ClearContent();
                Response.Clear();
                Response.ContentType = "text/xml";
                Response.AppendHeader("Content-Disposition", "attachment;filename=" + filename + ";");
                Response.TransmitFile(path);
                Response.Flush();
                Response.End();
            }
            return dt;
        }

        private XElement ImportContactNode(XElement _root, DataRow dr, string contactRefIDPortion, string FirstName, string LastName)
        {
            return new XElement("importContact",
                                              (contactRefIDPortion.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.contactRefID, contactRefIDPortion) : null),
                                              new XElement(XmlFormatter.contactType, "Premise"),
                                              (FirstName.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.firstName, FirstName.Trim()) : null),
                                              (LastName.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.lastName, LastName) : null),
                                              (dr["CompanyName"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.company, Convert.ToString(dr["CompanyName"])) : null),
                                              (dr["ServiceAddress1"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.address, Convert.ToString(dr["ServiceAddress1"])) : null),
                                              (dr["ServiceAddress2"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.addressCont, Convert.ToString(dr["ServiceAddress2"])) : null),
                                              (dr["ServiceCity"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.city, Convert.ToString(dr["ServiceCity"])) : null),
                                              (dr["ServiceState"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.state, Convert.ToString(dr["ServiceState"])) : null),
                                              (dr["ServiceZip"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.zip, Convert.ToString(dr["ServiceZip"])) : null),
                                              (dr["AccountNumber"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.accountNumber, Convert.ToString(dr["AccountNumber"])) : null),
                                              (dr["Phone1"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.phone, Convert.ToString(dr["Phone1"])) : null),
                                              (dr["Phone2"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.cell, Convert.ToString(dr["Phone2"])) : null),
                                              (dr["Email"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.email, Convert.ToString(dr["Email"])) : null));
        }

        private XElement ImportApplicationNode(XElement _root, DataRow dr)
        {
            return new XElement("importApplication",
                            AnalysisDate.CheckDbNullOrEmpty() ? new XElement("importApplicationItem",
                                new XElement("field", XmlFormatter.AnalysisDate),
                                new XElement("value", AnalysisDate)) : null,
                            ReceiptDate.CheckDbNullOrEmpty() ? new XElement("importApplicationItem",
                                new XElement("field", XmlFormatter.ReceiptDate),
                                new XElement("value", ReceiptDate)) : null,
                            dr["OperatingCompany"].CheckDbNullOrEmpty() ? new XElement("importApplicationItem",
                                               new XElement("field", XmlFormatter.Operating_Company),
                                               new XElement("value", Convert.ToString(dr["OperatingCompany"]))) : null,
                            dr["WaterHeaterFuel"].CheckDbNullOrEmpty() ? new XElement("importApplicationItem",
                                               new XElement("field", XmlFormatter.Water_Heater_Fuel),
                                               new XElement("value", Convert.ToString(dr["WaterHeaterFuel"]))) : null,
                            dr["HeaterFuel"].CheckDbNullOrEmpty() ? new XElement("importApplicationItem",
                                               new XElement("field", XmlFormatter.Heater_Fuel),
                                               new XElement("value", Convert.ToString(dr["HeaterFuel"]))) : null,
                            dr["IsOkayToContact"].CheckDbNullOrEmpty() ? new XElement("importApplicationItem",
                                               new XElement("field", XmlFormatter.OK_to_Contact),
                                               new XElement("value", Convert.ToString(dr["IsOkayToContact"]))) : null,
                            dr["FacilityType"].CheckDbNullOrEmpty() ? new XElement("importApplicationItem",
                                               new XElement("field", XmlFormatter.Facility_Type),
                                               new XElement("value", Convert.ToString(dr["FacilityType"]))) : null,
                            dr["ShipDate"].CheckDbNullOrEmpty() ? new XElement("importApplicationItem",
                                               new XElement("field", XmlFormatter.Shipment_Date),
                                               new XElement("value", Convert.ToString(ShipDate))) : null,
                            dr["RateCode"].CheckDbNullOrEmpty() ? new XElement("importApplicationItem",
                                               new XElement("field", XmlFormatter.Rate_Code),
                                               new XElement("value", Convert.ToString(dr["RateCode"]))) : null,
                            dr["AnalysisDate"].CheckDbNullOrEmpty() ? new XElement("importApplicationItem",
                                               new XElement("field", XmlFormatter.Enrollment_Completion_Date),
                                               new XElement("value", AnalysisDate)) : null,
                            dr["IncomeQualified"].CheckDbNullOrEmpty() ? new XElement("importApplicationItem",
                                               new XElement("field", XmlFormatter.Income_Qualified),
                                               new XElement("value", Convert.ToString(dr["IncomeQualified"]))) : null,
                            dr["CRQuantity"].CheckDbNullOrEmpty() ? new XElement("importApplicationItem",
                                               new XElement("field", XmlFormatter.Number_Kits_Shipped),
                                               new XElement("value", Convert.ToString(dr["CRQuantity"]))) : null,
                            Convert.ToString(dtMonthShipped).CheckDbNullOrEmpty() ? new XElement("importApplicationItem",
                                               new XElement("field", XmlFormatter.Month_Shipped),
                                               new XElement("value", dtMonthShipped.ToString("MM/dd/yyyy"))) : null,
                           new XElement("importApplicationItem",
                                               new XElement("field", XmlFormatter.Kwh_Savings),
                                               new XElement("value", Kwh_Savings)),
                           new XElement("importApplicationItem",
                                               new XElement("field", XmlFormatter.Kw_savings),
                                               new XElement("value", Kw_savings)));

            //return new XElement("importApplication",
            //    dr["AnalysisDate"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.AnalysisDate, AnalysisDate) : null,
            //    dr["ReceiptDate"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.ReceiptDate, ReceiptDate) : null,
            //    dr["OperatingCompany"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.Operating_Company, Convert.ToString(dr["OperatingCompany"])) : null,
            //    dr["WaterHeaterFuel"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.Water_Heater_Fuel, Convert.ToString(dr["WaterHeaterFuel"])) : null,
            //    dr["HeaterFuel"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.Heater_Fuel, Convert.ToString(dr["HeaterFuel"])) : null,
            //    dr["IsOkayToContact"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.OK_to_Contact, Convert.ToString(dr["IsOkayToContact"])) : null,
            //    dr["FacilityType"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.Facility_Type, Convert.ToString(dr["FacilityType"])) : null,
            //    dr["ShipDate"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.Shipment_Date, ShipDate) : null,
            //    dr["RateCode"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.Rate_Code, Convert.ToString(dr["RateCode"])) : null,
            //    dr["AnalysisDate"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.Enrollment_Completion_Date, AnalysisDate) : null,
            //    dr["IncomeQualified"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.AnalysisDate, Convert.ToString(dr["IncomeQualified"])) : null,
            //    dtMonthShipped.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.AnalysisDate, Convert.ToString(dr["IncomeQualified"])) : null,
            //    dr["CRQuantity"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.Month_Shipped, dtMonthShipped.ToString("MM/dd/yyyy")) : null,
            //    new XElement(XmlFormatter.Kwh_Savings, Kwh_Savings),
            //    new XElement(XmlFormatter.Kw_savings, Kw_savings));
        }
        private String EquipmentAttributesDeclarations(DataRow dr)
        {
            string ConsumerRequestId;
            ConsumerRequestId = Convert.ToString(dr["Id"]);
            gdConsumerRequestId = new Guid(ConsumerRequestId);
            DataSet dsEA = crPresenter.GetEquipmentAttributes(gdConsumerRequestId);
            dtEquipmentAttributes = dsEA.Tables[0];
            String ReceiptDateEA = string.Empty;
            return ReceiptDateEA;
        }
        private XElement EquipmentAttributeNode(XElement _root, DataRow dr)
        {
            //previousNode = null;
            XElement content1 = new XElement("importEquipment");
            String ReceiptDateEA = EquipmentAttributesDeclarations(dr);
            String SDescription = string.Empty; //PG25
            foreach (DataRow dre in dtEquipmentAttributes.Rows)
            {
                if (dre["ReceiptDate"] != DBNull.Value && Convert.ToString(dre["ReceiptDate"]) != null || Convert.ToString(dre["ReceiptDate"]) != string.Empty)
                {
                    ReceiptDateEA = Convert.ToDateTime(dre["ReceiptDate"]).ToString("MM/dd/yyyy");
                }
                sCatalogID = Convert.ToString(dre["AccountNumber"]).Trim() + "_" + Convert.ToString(dre["CatalogID"]).Trim();
                sCatalogID = sCatalogID.Trim();
                if (dre["CrQuantity"] == DBNull.Value || dre["CrQuantity"] == null || Convert.ToInt32(dre["CrQuantity"]) == 0)
                {
                    dre["CrQuantity"] = 1;
                }
                if (dre["KiQuantity"] != DBNull.Value && dre["KiQuantity"] != null)
                {
                    iQuantity = Convert.ToInt32(dre["CrQuantity"]) * Convert.ToInt32(dre["KiQuantity"]);
                }
                //if (dre["Kw_Impact"] != DBNull.Value && dre["Kw_Impact"] != null)
                //{
                //    Kw_Impact = Convert.ToDecimal(dre["Kw_Impact"]);
                //}
                //if (dre["Kwh_Impact"] != DBNull.Value && dre["Kwh_Impact"] != null)
                //{
                //    Kwh_Impact = Convert.ToDecimal(dre["Kwh_Impact"]);
                //}
                SDescription = Convert.ToString(dre["KitName"]).Trim();// +"-" + Convert.ToString(dre["KitItemName"]).Trim();
                XElement temp =

                        new XElement("importEquipmentItem",

                                   sCatalogID.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.equipmentRefId, sCatalogID) : null,
                                   new XElement("equipmentAttributes",
                    //dre["KitItemName"].CheckDbNullOrEmpty() ? new XElement("attribute",

                                   //       (dre["KitItemName"].CheckDbNullOrEmpty() ? new XElement("attributeName", XmlFormatter.Description) : null),
                    //       (dre["KitItemName"].CheckDbNullOrEmpty() ? new XElement("attributeValue", Convert.ToString(dre["KitItemName"])) : null)

                                   //) : null,
                                   SDescription.CheckDbNullOrEmpty() ? new XElement("attribute",

                                          (SDescription.CheckDbNullOrEmpty() ? new XElement("attributeName", XmlFormatter.Description) : null),
                                          (SDescription.CheckDbNullOrEmpty() ? new XElement("attributeValue", SDescription) : null)

                                   ) : null,
                                   dre["CatalogID"].CheckDbNullOrEmpty() ? new XElement("attribute",

                                          (dre["CatalogID"].CheckDbNullOrEmpty() ? new XElement("attributeName", XmlFormatter.CatalogID) : null),
                                          (dre["CatalogID"].CheckDbNullOrEmpty() ? new XElement("attributeValue", Convert.ToString(dre["CatalogID"]).Trim()) : null)

                                   ) : null,
                                   dre["ReceiptDate"].CheckDbNullOrEmpty() ? new XElement("attribute",

                                          (ReceiptDateEA.CheckDbNullOrEmpty() ? new XElement("attributeName", XmlFormatter.Date_installed) : null),
                                          (ReceiptDateEA.CheckDbNullOrEmpty() ? new XElement("attributeValue", ReceiptDateEA) : null)

                                   ) : null,
                                    iQuantity.CheckDbNullOrEmpty() ? new XElement("attribute",

                                          (iQuantity.CheckDbNullOrEmpty() ? new XElement("attributeName", XmlFormatter.Quantity) : null),
                                          (iQuantity.CheckDbNullOrEmpty() ? new XElement("attributeValue", iQuantity) : null)

                                   ) : null,
                                   dre["Kw_Impact"].CheckDbNullOrEmpty() ? new XElement("attribute",
                                        new XElement("attributeName", XmlFormatter.Kw_Impact),
                                        new XElement("attributeValue", Convert.ToDecimal(dre["Kw_Impact"]))
                                   ) : null,
                                   dre["Kwh_Impact"].CheckDbNullOrEmpty() ? new XElement("attribute",
                                       new XElement("attributeName", XmlFormatter.Kwh_Impact),
                                       new XElement("attributeValue", Convert.ToDecimal(dre["Kwh_Impact"]))
                                   ) : null
                               ));

                if (previousNode != null)
                {
                    if (previousNode.Value != temp.Value)
                    {
                        content1.Add(temp);
                    }
                    previousNode = temp;
                }
                else
                {
                    content1.Add(temp);
                    previousNode = temp;
                }
                //content1.Add(temp);
            }
            return content1;
        }
    }
}