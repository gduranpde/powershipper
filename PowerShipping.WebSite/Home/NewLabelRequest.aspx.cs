﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.ImportExport;
using PowerShipping.WebSite.Controls;

namespace PowerShipping.WebSite.Home
{
    public partial class NewLabelRequest : System.Web.UI.Page
    {
        ConsumerRequestsPresenter presenter = new ConsumerRequestsPresenter();
        ShipmentsPresenter presenterShipments = new ShipmentsPresenter();

        public List<Guid> ListOfSelectedRequests
        {
            get { return (ViewState["NewLabelRequestIds"] != null) ? (List<Guid>)ViewState["NewLabelRequestIds"] : (new List<Guid>()); }
        }

        public Shipment ShipmentCommonItem
        {
            get { return (ViewState["ShipmentCommon"] != null) ? (Shipment)ViewState["ShipmentCommon"] : null; }
            set { ViewState["ShipmentCommon"] = value; }
        }

        public Guid ShipmentId
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_Id.Value))
                           ? new Guid(HiddenField_Id.Value)
                           : Guid.Empty;
            }
            set { HiddenField_Id.Value = value.ToString(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                ViewState["ProjectId"] = Session["NewLabelRequestProjectId"];
                ViewState["NewLabelRequestIds"] = Session["NewLabelRequestIds"];
                ((ShipmentCommon)WizardMultiView.Views[1].FindControl("ShipmentCommon_Common")).InitData();
            }
        }

        #region MultiView

        protected void NextButton_Command(object sender, EventArgs e)
        {
            if (WizardMultiView.ActiveViewIndex == 0)
            {
                ShipmentCommonItem = ((ShipmentCommon)WizardMultiView.Views[1].FindControl("ShipmentCommon_Common")).GetEntity();

                try
                {
                    lbError.Text = string.Empty;

                    int i = ShipmentCommonItem.ShipmentDetailsCount;
                    List<Guid> ids = new List<Guid>();
                    foreach (Guid id in ListOfSelectedRequests)
                    {
                        if(i == 0)
                            break;

                        ids.Add(id);
                        i--;
                    }

                    //write to db
                    ShipmentId = presenter.SaveExportDate(ids, ShipmentCommonItem, User.Identity.Name, (Guid)ViewState["ProjectId"]);

                    lbError.ControlStyle.ForeColor = System.Drawing.Color.Green;
                    lbError.Text = "FedEx Label Request generated successfully.";

                    Session["NewLabelRequestIds"] = null;
                    Session["NewLabelRequestProjectId"] = null;
                }
                catch (Exception ex)
                {
                    lbError.ControlStyle.ForeColor = System.Drawing.Color.Red;
                    lbError.Text = ex.Message;
                }

                WizardMultiView.ActiveViewIndex += 1;
            }
            else if (WizardMultiView.ActiveViewIndex == 1)
            {
                Response.Redirect("LabelRequestSend.aspx?Id=" + ShipmentId);
            }
        }

        protected void BackButton_Command(object sender, EventArgs e)
        {
            if (WizardMultiView.ActiveViewIndex == 0)
            {
                Session["NewLabelRequestIds"] = null;
                Session["NewLabelRequestProjectId"] = null;

                Session["LabelRequestReady"] = true;
                Response.Redirect("ConsumerRequests.aspx");
            }
            else if (WizardMultiView.ActiveViewIndex == 1)
            {
                presenterShipments.Delete(ShipmentId);

                Response.Redirect("ConsumerRequests.aspx");
            }
        }

        protected void btnViewFile_OnClick(object sender, EventArgs e)
        {
            //write file to file system
            ExportResult res = presenter.GenerateExportDate(ShipmentId);
            string fileName = res.OutputFilePath.Replace(ConfigurationManager.AppSettings.Get("UploadFolder"), "");
            fileName = fileName.Trim('\\');


            Response.Redirect(@"~/Uploads/" + fileName);
        }

        #endregion

    }
}
