﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace PowerShipping.WebSite.Home
{
    public partial class HEAFileConversionDownload : System.Web.UI.Page
    {
        public string s;

        protected void Page_Load(object sender, EventArgs e)
        {
            LblTotalRecords.Text = Convert.ToString(Session["RecordNumber"]) + " Records";
        }

        protected void BtnDownload_Click(object sender, EventArgs e)
        {
            string[] fileName = Convert.ToString(Session["FileName"]).Split('.');
            string strURL = "~/csvFiles/" + fileName[0] + ".csv";
            WebClient req = new WebClient();
            HttpResponse response = HttpContext.Current.Response;
            response.Clear();
            response.ClearContent();
            response.ClearHeaders();
            response.Buffer = true;
            response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName[0] + ".csv" + "\"");
            byte[] data = req.DownloadData(Server.MapPath(strURL));
            response.BinaryWrite(data);
            response.End();
        }
    }
}