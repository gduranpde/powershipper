﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PowerShipping.Business.Presenters;
using PowerShipping.Data;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class ReshipImportBatches : System.Web.UI.Page
    {
        string BatchName = "";
        string ExceptionCount;
        private readonly BatchesPresenter presenterBatches = new BatchesPresenter();

        PowerShipping.Business.Presenters.ReshipImportBatch presenter = new PowerShipping.Business.Presenters.ReshipImportBatch();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //dpStartDateFilter.SelectedDate = DateTime.Now;
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "ReshipImportBatches");
            }

            ReshipImportBatchesFilter1.OnFilter += ReshipImportBatchesFilter1_OnFilter;
        }

        protected void RadGrid_Batches1_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_Batches1.DataSource = GetFilteredList();
        }

        private List<PowerShipping.Entities.ReshipImportBatch> GetFilteredList()
        {
            string sBatchName = ReshipImportBatchesFilter1.BatchName;
            string sImportedBy = ReshipImportBatchesFilter1.ImportedBy;
            DateTime? FromDate = ReshipImportBatchesFilter1.FromImportDate;
            DateTime? ToDate = ReshipImportBatchesFilter1.ToImportDate;

            if (sBatchName == string.Empty)
            { sBatchName = null; }

            if (sImportedBy == string.Empty)
            { sImportedBy = null; }

            Guid? shipperID = null;

            if (ReshipImportBatchesFilter1.ShipperID != Guid.Empty)
            {
                shipperID = ReshipImportBatchesFilter1.ShipperID;
            }

            return presenter.GetReshipImportBatches(sBatchName, sImportedBy, shipperID, FromDate, ToDate);
        }

        protected void RadGrid_Batches1_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.DeleteCommandName)
                {
                    var id =
                        new Guid(
                            (e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ReshipImportBatchID"].ToString());

                    PowerShipping.Entities.ReshipImportBatch RIB = DataProvider.Current.ReshipImportBatch.GetOne(id);

                    var filePath = Server.MapPath("~/Uploads/") + RIB.SourceFileName;

                    List<ShipmentDetail> list = presenterBatches.DeleteReshipImportBatch(id, filePath);

                    //if (list.Count != 0)
                    //{
                    //    hiddenBatchId.Value = id.ToString();
                    //    int inTrans = 0;
                    //    int delivered = 0;
                    //    int returned = 0;
                    //    foreach (var detail in list)
                    //    {
                    //        if (detail.ShippingStatus == ShipmentShippingStatus.InTransit)
                    //            inTrans++;
                    //        if (detail.ShippingStatus == ShipmentShippingStatus.Delivered)
                    //            delivered++;
                    //        if (detail.ShippingStatus == ShipmentShippingStatus.Returned)
                    //            returned++;
                    //    }

                    //    lbInTransit.Text = inTrans.ToString();
                    //    lbDelivered.Text = delivered.ToString();
                    //    lbReturned.Text = returned.ToString();

                    //    resultPanel.Visible = true;
                    //}

                    RadGrid_Batches1.Rebind();
                }
            }
            catch (Exception ex)
            {
                var l = new Label();
                l.Text = "Unable to delete Batch. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                RadGrid_Batches1.Controls.Add(l);
            }
        }

        private void ReshipImportBatchesFilter1_OnFilter()
        {
            RadGrid_Batches1.Rebind();
        }

        protected void RadGrid_Batches1_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                var AddButton_RadGrid = e.Item.FindControl("AddNewRecordButton") as Button;
                AddButton_RadGrid.Visible = false;
                var addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGrid_Batches1_ItemCommand1(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "ViewFile")
            {
                GridDataItem ditem = (GridDataItem)e.Item;
                string filename = ditem["SourceFileName"].Text; // get the filename from the row in which the download button is clicked
                string path = MapPath("/Uploads/" + filename);
                byte[] bts = System.IO.File.ReadAllBytes(path);
                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", bts.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                Response.BinaryWrite(bts);
                Response.Flush();
                Response.End();
            }
            if (e.CommandName == "ViewException")
            {
                GridDataItem item = (GridDataItem)e.Item;
                BatchName = item["ReshipImportBatchName"].Text;
                ExceptionCount = item["NumberWithExceptions"].Text;
                if (ExceptionCount != "0")
                {
                    Response.Redirect("~/Home/ReshipImportExceptionsPage.aspx?BatchName=" + BatchName);
                }
            }
        }
    }
}