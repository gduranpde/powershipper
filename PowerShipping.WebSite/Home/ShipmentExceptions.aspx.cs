﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.WebSite.Controls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite
{
    public partial class ShipmentExceptions : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Shipment Exceptions");
            }
        }

        protected void RadGrid_ShipmentExceptions_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                Button spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                LinkButton addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGrid_ShipmentExceptions_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                string reason = item["Reason"].Text;
                reason = reason.Replace("|", "<br/>");
                item["Reason"].Text = reason;
            }
        }

        protected void RadGrid_ShipmentDetails_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            //this.RadGrid_ShipmentExceptions.DataSource = presenter.GetAll();
        }

        protected void RadGrid_ShipmentExceptions_InsertCommand(object source, GridCommandEventArgs e)
        {
            //object o = ((Controls.ShipmentDetailInfo)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).GetEntity();
        }

        protected void RadGrid_ShipmentExceptions_UpdateCommand(object source, GridCommandEventArgs e)
        {
           // object o = ((Controls.ShipmentDetailInfo)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).GetEntity();

        }
    }
}
