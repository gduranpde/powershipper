﻿<%@ Page Title="Create Label Request" Language="C#" MasterPageFile="~/Shared/Default.Master"
    AutoEventWireup="true" CodeBehind="Shipments.aspx.cs" Inherits="PowerShipping.WebSite.Home.Shipments"
    Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../Controls/ProjectsList.ascx" TagName="ProjectsList" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Controls/LabelRequestCard.ascx" TagName="LabelRequestCard" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="pageDiv">
        <table>
            <tr>
                <td>
                    <h3>
                        Create Label Request</h3>
                </td>
                <td colspan="4" align="center">
                    <uc1:ProjectsList ID="ProjectsList1" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Purchase Order Number
                    <asp:TextBox ID="tbPurchaseOrderNumberFilter" runat="server"></asp:TextBox>
                </td>
                <td>
                    Start Date
                    <telerik:RadDatePicker ID="dpStartDateFilter" runat="server" MinDate="01-01-1990"
                        MaxDate="01-01-2020">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    End Date
                    <telerik:RadDatePicker ID="dpEndDateFilter" runat="server" MinDate="01-01-1990" MaxDate="01-01-2020">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search"
                        CssClass="ptButton" />
                </td>
                <td>
                    <asp:Button ID="btnClearFilter" runat="server" OnClick="btnClearFilter_Click" Text="Clear"
                        CssClass="ptButton" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnCreate" runat="server" OnClick="btnCreate_Click" Text="New Label Request"
                        CssClass="ptButton" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="RadGrid_Shipments" runat="server" Width="100%" GridLines="None"
            AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True"
            Skin="Windows7" OnNeedDataSource="RadGrid_Shipments_NeedDataSource" OnUpdateCommand="RadGrid_Shipments_UpdateCommand"
            OnDeleteCommand="RadGrid_Shipments_DeleteCommand" OnItemDataBound="RadGrid_Shipments_ItemDataBound"
            OnItemCreated="RadGrid_Shipments_ItemCreated" OnItemCommand="RadGrid_Shipments_ItemCommand">
            <ExportSettings ExportOnlyData="true" IgnorePaging="true">
            </ExportSettings>
            <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true">
                <CommandItemSettings ShowExportToWordButton="true" ShowExportToExcelButton="true"
                    ShowExportToCsvButton="true" ShowExportToPdfButton="true" />
                <Columns>
                    <%-- <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" >
                    </telerik:GridEditCommandColumn>--%>
                    <telerik:GridButtonColumn UniqueName="PreviewColumn" Text="View File" CommandName="Preview" />
                    <telerik:GridButtonColumn UniqueName="SendColumn" Text="Resend" CommandName="Send" />
                    <%--<telerik:GridButtonColumn UniqueName="DeleteColumn" Text="Delete" CommandName="Delete"
                        ConfirmDialogType="RadWindow" ConfirmText="Are you sure delete this row?" />--%>
                    <telerik:GridEditCommandColumn UniqueName="EditCommandColumn">
                        <HeaderStyle Width="30px" />
                    </telerik:GridEditCommandColumn>
                    <telerik:GridBoundColumn DataField="PurchaseOrderNumber" HeaderText="PO Number" SortExpression="PurchaseOrderNumber"
                        UniqueName="PurchaseOrderNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false" EditFormColumnIndex="0">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CreatedBy" HeaderText="CreatedBy" SortExpression="CreatedBy"
                        UniqueName="CreatedBy" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CompanyCode" HeaderText="CompanyCode" SortExpression="CompanyCode"
                        UniqueName="CompanyCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ProjectCode" HeaderText="ProjectCode" SortExpression="ProjectCode"
                        UniqueName="ProjectCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ShipperCode" HeaderText="Shipper" SortExpression="ShipperCode"
                        UniqueName="ShipperCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ShipmentDetailsCount" HeaderText="# of Records"
                        SortExpression="ShipmentDetailsCount" UniqueName="ShipmentDetailsCount" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="KitTypeName" HeaderText="Kit" SortExpression="KitTypeName"
                        UniqueName="KitTypeName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridDateTimeColumn DataField="DateLabelsNeeded" HeaderText="DateLabelsNeeded"
                        SortExpression="DateLabelsNeeded" UniqueName="DateLabelsNeeded" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" Visible="false" DataFormatString="{0:MM/dd/yyyy}">
                    </telerik:GridDateTimeColumn>
                    <telerik:GridDateTimeColumn DataField="DateCustomerWillShip" HeaderText="DateCustomerWillShip"
                        SortExpression="DateCustomerWillShip" UniqueName="DateCustomerWillShip" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="true" Visible="false" DataFormatString="{0:MM/dd/yyyy}">
                    </telerik:GridDateTimeColumn>
                    <telerik:GridDateTimeColumn DataField="CreatedTime" HeaderText="CreatedTime" SortExpression="CreatedTime"
                        UniqueName="CreatedTime" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                        ShowFilterIcon="true" DataFormatString="{0:MM/dd/yyyy}">
                    </telerik:GridDateTimeColumn>
                </Columns>
                <EditFormSettings UserControlName="../Controls/LabelRequestCard.ascx" EditFormType="WebUserControl">
                    <EditColumn UniqueName="EditCommandColumn1">
                    </EditColumn>
                </EditFormSettings>
                <%-- <EditFormSettings ColumnNumber="3" CaptionFormatString="Edit details for Opt Out"
                CaptionDataField="Id">
                <FormTableItemStyle Wrap="False"></FormTableItemStyle>
                <FormCaptionStyle CssClass="EditFormHeader"></FormCaptionStyle>
                <FormMainTableStyle GridLines="None" CellSpacing="0" CellPadding="3" Width="100%" />
                <FormTableStyle CellSpacing="0" CellPadding="2" CssClass="module" GridLines="Both"
                    Height="110px" Width="100%"  />
                <FormTableAlternatingItemStyle Wrap="False"></FormTableAlternatingItemStyle>
                <FormStyle Width="100%" BackColor="#eef2ea"></FormStyle>
                <EditColumn UpdateText="Save" UniqueName="EditCommandColumn1" CancelText="Cancel">
                </EditColumn>
                <FormTableButtonRowStyle HorizontalAlign="Left"></FormTableButtonRowStyle>
            </EditFormSettings>--%>
            </MasterTableView>
        </telerik:RadGrid>
        <asp:Panel ID="pnl1" runat="server">
        </asp:Panel>
        <asp:Panel ID="pnlPopUp" runat="server" CssClass="modalPopupError2">
            <table width="100%">
                <tr align="center">
                    <td align="center">
                        <asp:Label ID="lbPopUp" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr align="center">
                    <td align="center">
                        <asp:Button ID="btnCancelMpe" runat="server" Text="Ok" Width="80" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="mpe" runat="server" PopupControlID="pnlPopUp"
            TargetControlID="pnl1" OkControlID="btnCancelMpe" BackgroundCssClass="modalBackground">
        </ajaxToolkit:ModalPopupExtender>

        <script type="text/javascript">

            function Expand(itemID) {
                var Grid = $find('<%=RadGrid_Shipments.ClientID %>');
                var scrollArea = document.getElementById('ctl00_ContentPlaceHolder1_RadGrid_Shipments')
                var rowElement = document.getElementById(itemID);
                window.scrollTo(0, rowElement.offsetTop + 200);
            }
        </script>

    </div>
</asp:Content>