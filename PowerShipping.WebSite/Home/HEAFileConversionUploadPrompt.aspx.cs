﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PowerShipping.Entities;

namespace PowerShipping.WebSite.Home
{
    public partial class HEAFileConversionUploadPrompt : System.Web.UI.Page
    {
        string FileToConvert;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FileToConvert = Server.MapPath("~/Uploads/" + Convert.ToString(Session["FileName"]));

                ConvertFile();
            }
        }

        protected void BtnConvertFile_Click(object sender, EventArgs e)
        {
            if (HiddenField1.Value == "0")
            {
                LblMessage.Text = "Please select atleast one company";
            }
            else
            {
                CreateCSVfile();
                Response.Redirect("~/Home/HEAFileConversionDownload.aspx");
            }
        }

        private string generateFilterExpression(ArrayList arrLst)
        {
            string expression = string.Empty;

            for (int i = 0; i < arrLst.Count; i++)
            {
                expression = expression + "OP_CO='" + arrLst[i] + "'";

                if (i != arrLst.Count - 1)
                {
                    expression = expression + " OR ";
                }
            }

            return expression;
        }

        protected void ConvertFile()
        {
            try
            {
                string connectionString = string.Empty;

                if (Path.GetExtension(FileToConvert) == ".xls")
                {
                    connectionString = string.Format(Convert.ToString(ConfigurationManager.ConnectionStrings["ExcelXls"]), FileToConvert);
                }
                else if (Path.GetExtension(FileToConvert) == ".xlsx")
                {
                    connectionString = string.Format(Convert.ToString(ConfigurationManager.ConnectionStrings["ExcelXlsx"]), FileToConvert, "\"Excel 12.0;HDR=Yes\"");
                }

                OleDbConnection connection = new OleDbConnection(connectionString);
                connection.Open();

                //this next line assumes that the file is in default Excel format with Sheet1 as the first sheet name, adjust accordingly

                OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$] where OP_CO IS NOT NULL", connection);

                OleDbDataAdapter adapter1 = new OleDbDataAdapter("SELECT COUNT(*) FROM [Sheet1$] where OP_CO IS  NULL", connection);
                DataSet ds1 = new DataSet();
                DataTable dt1 = new DataTable();
                adapter1.Fill(ds1);
                LblBlankRowsCount.Text = "Blank rows:" + Convert.ToString(ds1.Tables[0].Rows[0][0]);

                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                adapter.Fill(ds);
                dt = ds.Tables[0];

                DataRow[] drRemove = dt.Select("OP_CO='totals:'");
                for (int icount = 0; icount < drRemove.Length; icount++)
                {
                    dt.Rows.Remove(drRemove[icount]);
                }

                Session["ExcelRecords"] = dt;

                DataView dwExcel = dt.DefaultView;
                DataTable dtUniqueComapnies = dwExcel.ToTable(true, "OP_CO");

                for (int icount = 0; icount < dtUniqueComapnies.Rows.Count; icount++)
                {
                    string sUniqueCompanies = Convert.ToString(dtUniqueComapnies.Rows[icount]["OP_CO"]);
                    DataRow[] dr = dt.Select("OP_CO='" + sUniqueCompanies + "'");
                    int iCompanyCount = dr.Length;
                    DataRow[] dr1 = dtUniqueComapnies.Select("OP_CO='" + sUniqueCompanies + "'");
                    dr1[0]["OP_CO"] = Convert.ToString(dr1[0]["OP_CO"]) + " ( " + Convert.ToString(iCompanyCount) + "-Records" + ")";
                }

                int iGrandTotal = dt.Rows.Count + Convert.ToInt32(ds1.Tables[0].Rows[0][0]);
                LblTotalRecords.Text = Convert.ToString(iGrandTotal);
                ChkBoxListOperatingCompany.DataSource = dtUniqueComapnies;
                ChkBoxListOperatingCompany.DataTextField = "OP_CO";
                ChkBoxListOperatingCompany.DataValueField = "OP_CO";
                ChkBoxListOperatingCompany.DataBind();

                connection.Close();
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                //Label l = new Label();
                LblMessage.Text = "Unable to Read from Source File. Reason: " + ex.Message;
                LblMessage.ControlStyle.ForeColor = System.Drawing.Color.Red;
                //RadGrid_Customers.Controls.Add(l);
            }
        }

        private void CreateCSVfile()
        {
            try
            {
                ArrayList alExcel = new ArrayList();

                string filterExpression = string.Empty;
                string OP_CO = string.Empty;

                foreach (ListItem li in ChkBoxListOperatingCompany.Items)
                {
                    if (li.Selected)
                    {
                        string temp = Convert.ToString(li.Value);

                        string temp1 = temp.Substring(0, temp.IndexOf('('));

                        alExcel.Add(temp1.Trim());
                    }
                }

                string Expression = generateFilterExpression(alExcel);

                DataTable dt = (DataTable)Session["ExcelRecords"];

                //var query = from abc in dt.AsEnumerable() where Convert.ToString(abc["OP_CO"] == "A") select abc;

                //if (query.Count > 0)
                //{
                //    DataTable dt = query.CopyToDataTable();
                //}

                DataRow[] drCompanyFilter = dt.Select(Expression);

                Session["RecordNumber"] = drCompanyFilter.Length;

                HEAConversionRules objRules = new HEAConversionRules();

                StringBuilder CurrentRecord = new StringBuilder();
                CurrentRecord.Append("METHOD").Append(",").Append("ANALYSIS_DATE").Append(",").Append("USER_ID").Append(",").Append("ACCOUNT_NUMBER").Append(",").Append("NAME").Append(",").Append("ADDRESS1").Append(",").Append("ADDRESS2").Append(",").Append("CITY").Append(",").Append("STATE").Append(",").Append("ZIP").Append(",").Append("EMAIL_ADDRESS").Append(",").Append("PHONE1").Append(",").Append("PHONE2").Append(",").Append("OK_TO_CONTACT").Append(",").Append("OP_CO").Append(",").Append("WATER_HEATER_TYPE").Append(",").Append("QUANTITY").Append(",").Append("PREMISE_ID").Append(",").Append("COMPANY_NAME").Append(",").Append("SERVICE_ADDRESS1").Append(",").Append("SERVICE_ADDRESS2").Append(",").Append("SERVICE_CITY").Append(",").Append("SERVICE_STATE").Append(",").Append("SERVICE_ZIP").Append(",").Append("OldAccountNumber").Append(",").Append("FacilityType").Append(",").Append("RateCode").Append(',').Append("IncomeQualified").Append(',').Append("HeaterFuel").Append(',').Append("RequestedKit").Append("\r\n");
                // CurrentRecord.Append("METHOD").Append(",").Append("ANALYSIS_DATE").Append(",").Append("ACCOUNT_NUMBER").Append(",").Append("NAME").Append(",").Append("ADDRESS1").Append(",").Append("ADDRESS2").Append(",").Append("CITY").Append(",").Append("STATE").Append(",").Append("ZIP").Append(",").Append("EMAIL_ADDRESS").Append(",").Append("PHONE1").Append(",").Append("PHONE2").Append(",").Append("OK_TO_CONTACT").Append(",").Append("OP_CO").Append(",").Append("WATER_HEATER_TYPE").Append(",").Append("QUANTITY").Append(",").Append("PREMISE_ID").Append(",").Append("COMPANY_NAME").Append(",").Append("SERVICE_ADDRESS1").Append(",").Append("SERVICE_ADDRESS2").Append(",").Append("SERVICE_CITY").Append(",").Append("SERVICE_STATE").Append(",").Append("SERVICE_ZIP").Append("\r\n");

                for (int i = 0; i < drCompanyFilter.Length; i++)
                {
                    // METHOD
                    string METHOD = objRules.GetMethod(Convert.ToString(drCompanyFilter[i]["ACLARA"]), Convert.ToString(drCompanyFilter[i]["CBA"]));

                    // ANALYSIS DATE
                    string ANALYSIS_DATE = objRules.GetAnalysisDate(Convert.ToString(drCompanyFilter[i]["LEVEL_1_DATE"]), Convert.ToString(drCompanyFilter[i]["LEVEL_2_DATE"]), Convert.ToString(drCompanyFilter[i]["LEVEL_3_DATE"]));

                    //User_ID

                    string User_ID = "";

                    // ACCOUNT NUMBER

                    string ACCOUNT_NUMBER = Convert.ToString(" " + drCompanyFilter[i]["CONTRACT_ACCT"]);

                    // NAME
                    string NAME = Convert.ToString(drCompanyFilter[i]["BP_NAME"]);

                    //--SERVICE ADDRESS--//

                    string SERVICE_ADDRESS1 = string.Empty;
                    string SERVICE_ADDRESS2 = string.Empty;
                    string SERVICE_CITY = string.Empty;
                    string SERVICE_STATE = string.Empty;
                    string SERVICE_ZIP = string.Empty;

                    //- ADDRESS, CITY, STATE, ZIP -//

                    string ADDRESS1 = string.Empty, ADDRESS2 = string.Empty, CITY = string.Empty, STATE = string.Empty, ZIP = string.Empty;

                    Dictionary<string, string> Address = new Dictionary<string, string>();

                    Address = objRules.GetAddress(Convert.ToString(drCompanyFilter[i]["CA_ADDR4"]), Convert.ToString(drCompanyFilter[i]["CA_ADDR5"]), Convert.ToString(drCompanyFilter[i]["BP_STD_ADDR4"]), Convert.ToString(drCompanyFilter[i]["BP_STD_ADDR5"]), Convert.ToString(drCompanyFilter[i]["PREM_ADDR4"]), Convert.ToString(drCompanyFilter[i]["PREM_ADDR5"]));

                    ADDRESS1 = Address["ADDRESS"];
                    ADDRESS2 = " ";
                    CITY = Address["CITY"];
                    STATE = Address["STATE"];
                    ZIP = Address["ZIP"];

                    //- EMAIL ADDRESS

                    string EMAIL = Convert.ToString(drCompanyFilter[i]["CA_EMAIL_ADDRESS"]);

                    //PHONE 1

                    string PHONE1 = Convert.ToString(drCompanyFilter[i]["BP_PHONE"]);

                    //PHONE 2

                    string PHONE2 = Convert.ToString(drCompanyFilter[i]["PREMISE_PHONE"]);

                    //OK TO CONTACT

                    string OK_TO_CONTACT = objRules.OkToContact(Convert.ToString(drCompanyFilter[i]["OK_TO_CONTACT"]));

                    //OP_CO

                    string OP_COMP = Convert.ToString(drCompanyFilter[i]["OP_CO"]);

                    // WATER HEATER TYPE

                    string WATER_HEATER_TYPE = Convert.ToString(drCompanyFilter[i]["WATER_HEAT_TYPE"]);

                    //QUANTITY

                    string QUANTITY = "1";

                    //PREMISE IDs

                    string PREMISE_ID = " ";

                    //COMPANY NAME

                    string COMPANY_NAME = " ";

                    //SERVICE ADDRESS1

                    //string SERVICE_ADDRESS1 = ADDRESS1;
                    //string SERVICE_ADDRESS2 = ADDRESS2;
                    //string SERVICE_CITY = CITY;
                    //string SERVICE_STATE = STATE;
                    //string SERVICE_ZIP = ZIP;
                    SERVICE_ADDRESS1 = Address["SERVICE_ADDRESS1"];
                    SERVICE_ADDRESS2 = " ";
                    SERVICE_CITY = Address["SERVICE_CITY"];
                    SERVICE_STATE = Address["SERVICE_STATE"];
                    SERVICE_ZIP = Address["SERVICE_ZIP"];

                    string OldAccountNumenber = " ";
                    string FacilityType = " ";
                    string RateCode = Convert.ToString(drCompanyFilter[i]["RATE"]);
                    string IncomeQualified = " ";
                    string HeaterFuel = Convert.ToString(drCompanyFilter[i]["HEAT_TYPE"]);
                    string RequestedKit = " ";

                    CurrentRecord.Append(METHOD).Append(',').Append(ANALYSIS_DATE).Append(',').Append(User_ID).Append(',').Append(ACCOUNT_NUMBER).Append(',').Append(NAME).Append(',').Append(ADDRESS1).Append(',').Append(ADDRESS2).Append(',').Append(CITY).Append(',').Append(STATE).Append(',').Append(ZIP).Append(',').Append(EMAIL).Append(',').Append(PHONE1).Append(',').Append(PHONE2).Append(',').Append(OK_TO_CONTACT).Append(',').Append(OP_COMP).Append(',').Append(WATER_HEATER_TYPE).Append(',').Append(QUANTITY).Append(',').Append(PREMISE_ID).Append(',').Append(COMPANY_NAME).Append(',').Append(SERVICE_ADDRESS1).Append(',').Append(SERVICE_ADDRESS2).Append(',').Append(SERVICE_CITY).Append(',').Append(SERVICE_STATE).Append(',').Append(SERVICE_ZIP).Append(',').Append(OldAccountNumenber).Append(',').Append(FacilityType).Append(',').Append(RateCode).Append(',').Append(IncomeQualified).Append(',').Append(HeaterFuel).Append(',').Append(RequestedKit).Append("\r\n");
                    //CurrentRecord.Append(METHOD).Append(',').Append(ANALYSIS_DATE).Append(',').Append(ACCOUNT_NUMBER).Append(',').Append(NAME).Append(',').Append(ADDRESS1).Append(',').Append(ADDRESS2).Append(',').Append(CITY).Append(',').Append(STATE).Append(',').Append(ZIP).Append(',').Append(EMAIL).Append(',').Append(PHONE1).Append(',').Append(PHONE2).Append(',').Append(OK_TO_CONTACT).Append(',').Append(OP_COMP).Append(',').Append(WATER_HEATER_TYPE).Append(',').Append(QUANTITY).Append(',').Append(PREMISE_ID).Append(',').Append(COMPANY_NAME).Append(',').Append(SERVICE_ADDRESS1).Append(',').Append(SERVICE_ADDRESS2).Append(',').Append(SERVICE_CITY).Append(',').Append(SERVICE_STATE).Append(',').Append(SERVICE_ZIP).Append("\r\n");
                }

                string[] fileName = Convert.ToString(Session["FileName"]).Split('.');

                File.WriteAllText(Server.MapPath("~/csvFiles/" + fileName[0] + ".csv"), CurrentRecord.ToString());
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                LblMessage.Text = "Unable to Convert Source File. Reason: " + ex.Message;
                LblMessage.ControlStyle.ForeColor = System.Drawing.Color.Red;
            }
        }
    }
}