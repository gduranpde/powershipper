﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.Logging;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class CustomerImportHistory : System.Web.UI.Page
    {
        private readonly CustomerImportHistoryPresenter presenter = new CustomerImportHistoryPresenter();
		private readonly CustomerDataImportExceptionPresenter _presenterExeptions = new CustomerDataImportExceptionPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.IsInRole(PDMRoles.ROLE_Administrator))
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Customer Import History");
            }
        }

        protected void RadGrid_History_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                var spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                var addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGrid_History_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_History.DataSource = GetFilteredListAccounts();
        }

        protected void RadGrid_History_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item is GridDataItem) && !e.Item.IsInEditMode)
            {
                var gridItem = (GridDataItem)e.Item;
				var exCount = (LinkButton)gridItem.FindControl("lbtnDownloadExp");

	            if (int.Parse(exCount.Text) <= 0)
	            {
		            exCount.Enabled = false;
	            }
            }
        }

        private DataSet GetFilteredListAccounts()
        {
            return presenter.GetAll();
        }

	    protected void RadGrid_History_OnItemCommand(object sender, GridCommandEventArgs e)
	    {
		    try
		    {
			    if (e.CommandName == "DownloadExp" && (int.Parse(e.CommandArgument.ToString()) > 0))
			    {
				    var dataItem = e.Item as GridDataItem;
				    if (dataItem != null)
				    {
					    var gridItem = dataItem;
					    string historyId = gridItem.GetDataKeyValue("Id").ToString();

					    if (!string.IsNullOrEmpty(historyId))
					    {
						    var values = new string[3];

						    #region Make the Header

						    values[0] = "RecordNumber";
						    values[1] = "Reason";
						    values[2] = "RecordData";

						    #endregion

						    Response.ClearContent();
						    Response.Clear();
						    Response.ContentType = "application/CSV";
						    Response.AddHeader("Content-Disposition", "attachment;filename=ConsumerDataImportExceptions.csv");
						    Response.Write(string.Join(",", values));
						    Response.Write(Environment.NewLine);

						    var source = _presenterExeptions.GetByCustomerDataImportId(new Guid(historyId));

						    foreach (var item in source)
						    {
							    values = new string[3];
							    foreach (var prop in item.GetType().GetProperties())
							    {
								    var propValue = prop.GetValue(item, null);

								    #region Make items

								    switch (prop.Name)
								    {
									    case "RecordNumber":
										    values[0] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
										    break;
									    case "Reason":
										    values[1] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
										    break;
									    case "RecordData":
										    values[2] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
										    break;
								    }

								    #endregion
							    }
							    Response.Write(string.Join(",", values));
							    Response.Write(Environment.NewLine);
						    }

						    Response.Flush();
						    Response.End();
					    }
				    }
			    }
		    }
			catch (Exception ex)
			{
				CurrentLogger.Log.Error(ex);
				Log.LogError(ex);
		    }
	    }
    }
}
