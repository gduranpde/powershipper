﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;
using Telerik.Web.UI.GridExcelBuilder;

namespace PowerShipping.WebSite.Home
{
    public partial class AEGDeliveryReport : Page
    {
        private readonly AuditReportPresenter presenter = new AuditReportPresenter();
        private bool _isFirstLoad = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList1.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }

            if (!IsPostBack)
            {
                
                RadComboBox_KitType.DataBind();
            }

            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "AEG Delivery Report");
                _isFirstLoad = true;
            }

            RadGrid_Accounts.GridExporting += RadGrid_Accounts_GridExporting;
            RadGrid_Accounts.ExcelMLWorkBookCreated += RadGrid_Accounts_ExcelMLWorkBookCreated;
            RadGrid_Accounts.ExcelMLExportRowCreated += RadGrid_Accounts_ExcelMLExportRowCreated;
            RadGrid_Accounts.ClientSettings.AllowColumnsReorder = true;
        }

        void RadGrid_Accounts_ExcelMLExportRowCreated(object sender, GridExportExcelMLRowCreatedArgs e)
        {
            
        }

        void RadGrid_Accounts_ExcelMLWorkBookCreated(object sender, GridExcelMLWorkBookCreatedEventArgs e)
        {
            var grid = sender as RadGrid;
            if (grid != null)
            {
                grid.MasterTableView.SwapColumns("HeaterFuel", "TransactionType");
            }
        }

        void RadGrid_Accounts_GridExporting(object sender, GridExportingArgs e)
        {          
            var grid = sender as RadGrid;            
            if (grid != null)
            {
                ////var col1 = grid.Columns.FindByUniqueName("HeaterFuel");
                ////var col2 = grid.Columns.FindByUniqueName("TransactionType");

                //if (e.ExportType == ExportType.Excel) 
                //{
                //    GridColumn col1 = grid.MasterTableView.Columns.FindByUniqueName("AccountNumber");
                //    //GridColumn col2 = grid.MasterTableView.Columns.FindByUniqueName("AccountName");
                //    //grid.MasterTableView.SwapColumns(col2, col1);
                //    //grid.MasterTableView.ExportToExcel();
                //    grid.MasterTableView.SwapColumns(1, 2);
                //    grid.MasterTableView.ExportToExcel();
                //}

                //GridColumn col1 = grid.MasterTableView.Columns.FindByUniqueName("AccountNumber");
                //GridColumn col2 = grid.MasterTableView.Columns.FindByUniqueName("AccountName");
                //grid.MasterTableView.SwapColumns(col2, col1);
                //grid.MasterTableView.ExportToExcel();
                ////col1.Visible = false;
                ////col2.Visible = false;
                ////grid.Rebind();
            }
        }

        private void ProjectsList1_OnProjectChanged(string projectId)
        {            
            RadGrid_Accounts_NeedDataSource(null, null);
            RadGrid_Accounts.Rebind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            RadGrid_Accounts_NeedDataSource(null, null);
            RadGrid_Accounts.Rebind();
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            tbPONumberFilter.Text = null;
            dpStartDateFilter.SelectedDate = null;
            dpEndDateFilter.SelectedDate = null;
            RadComboBox_KitType.SelectedValue = null;
        }

        protected void RadGrid_Accounts_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                var spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                var addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGrid_Accounts_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (!_isFirstLoad)
                RadGrid_Accounts.DataSource = GetFilteredListAccounts();
        }

        protected void RadGrid_Accounts_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item is GridDataItem) && !e.Item.IsInEditMode)
            {
                var gridItem = (GridDataItem)e.Item;
                try
                {
                    Double myDouble = Convert.ToDouble(gridItem["Phone1"].Text);
                    gridItem["Phone1"].Text = myDouble.ToString("###-###-####");

                    Double myDouble2 = Convert.ToDouble(gridItem["Phone2"].Text);
                    gridItem["Phone2"].Text = myDouble2.ToString("###-###-####");
                }
                catch
                {
                }
            }
        }

        private DataSet GetFilteredListAccounts()
        {
            string poNum = tbPONumberFilter.Text;
            DateTime? startDate = dpStartDateFilter.SelectedDate;
            DateTime? endDate = dpEndDateFilter.SelectedDate;

            if (endDate != null)
                endDate = endDate.Value.AddDays(1);

            Guid? kitType = null;
            string kit = RadComboBox_KitType.SelectedValue;
            if (!string.IsNullOrEmpty(kit))
                kitType = new Guid(RadComboBox_KitType.SelectedValue);

            if (poNum == string.Empty)
                poNum = null;

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList1.ProjectId;
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = ProjectsList1.UserId;
            }

            if (ProjectsList1.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList1.CompanyId;
            }

            return presenter.GetAEGReport(poNum, startDate, endDate, kitType, userId, projectId, companyId);
        }
    }
}