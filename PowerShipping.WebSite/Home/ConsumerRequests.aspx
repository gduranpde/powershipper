<%@ Page Title="Consumer Requests" Language="C#" MasterPageFile="~/Shared/Default.Master"
    AutoEventWireup="true" CodeBehind="ConsumerRequests.aspx.cs" Inherits="PowerShipping.WebSite.ConsumerRequests"
    Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Controls/ConsumerRequestFilter.ascx" TagName="ConsumerRequestFilter"
    TagPrefix="uc1" %>
<%@ Register Src="../Controls/ProjectsList.ascx" TagName="ProjectsList" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <%--<ajaxToolkit:CollapsiblePanelExtender ID="cpe" runat="Server"
    TargetControlID="pFilter"
    CollapsedSize="0"

    Collapsed="True"
    ExpandControlID="lnkbShowHide"
    CollapseControlID="lnkbShowHide"
    AutoCollapse="False"
    AutoExpand="False"
    ScrollContents="True"
    CollapsedText="Show Details..."
    ExpandedText="Hide Details"
    ExpandDirection="Vertical" />
      <asp:Panel ID="pFilter" runat="server" >--%>
    <uc1:ConsumerRequestFilter ID="ConsumerRequestFilter1" runat="server" />
    <%--  </asp:Panel>
    <asp:LinkButton ID="lnkbShowHide" runat="server" Text="Filter" ></asp:LinkButton>
    --%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField runat="server" ID="HiddenFiled_CustomQuery" />
    <div class="pageDiv">
        <table>
            <tr>
                <td>
                    <h3>Consumer Requests</h3>
                </td>
                <td>
                    <uc1:ProjectsList ID="ProjectsList1" runat="server" IsCompanyUse="true" />
                </td>
                <td>
                    <asp:Button ID="btnCreateRequest" runat="server" OnClick="btnCreateRequest_OnClick"
                        Text="Create Label Request" />
                    <asp:Button ID="btnBulkExport" runat="server" OnClick="btnBulkExport_OnClick"
                        Text="Bulk Export" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <telerik:RadGrid ID="RadGrid_ConsumerRequests" runat="server" Width="100%" GridLines="None"
                        AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True"
                        Skin="Windows7" OnNeedDataSource="RadGrid_ConsumerRequests_NeedDataSource" OnUpdateCommand="RadGrid_ConsumerRequests_UpdateCommand"
                        OnDeleteCommand="RadGrid_ConsumerRequests_DeleteCommand" OnItemDataBound="RadGrid_ConsumerRequests_ItemDataBound"
                        OnInsertCommand="RadGrid_ConsumerRequests_InsertCommand" OnItemCommand="RadGrid_ConsumerRequests_ItemCommand"
                        OnItemCreated="RadGrid_ConsumerRequests_ItemCreated">
                        <ExportSettings ExportOnlyData="true" IgnorePaging="true">
                        </ExportSettings>
                        <ClientSettings>
                            <Selecting AllowRowSelect="true" />
                            <Resizing AllowColumnResize="True" />
                        </ClientSettings>
                        <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                            AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true"
                            PagerStyle-Mode="NextPrevNumericAndAdvanced">
                            <CommandItemSettings ShowExportToWordButton="true" ShowExportToExcelButton="true"
                                ShowExportToCsvButton="true" ShowExportToPdfButton="true" />
                            <Columns>
                                <telerik:GridTemplateColumn SortExpression="AccountNumber" HeaderText="AccountNumber"
                                    DataField="AccountNumber" DataType="System.String" UniqueName="AccountNumber">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="EditButton" runat="server" CommandName="Edit" Text="<%# ((PowerShipping.Entities.ConsumerRequest)Container.DataItem).AccountNumber %>"></asp:LinkButton>
                                        <%--<span style=" display : none;">'<%# ((PowerShipping.Entities.ConsumerRequest)Container.DataItem).AccountNumber %>'</span>--%>
                                    </ItemTemplate>
                                    <HeaderStyle Width="100px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="AccountName" HeaderText="Name" SortExpression="AccountName"
                                    UniqueName="AccountName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                    <HeaderStyle Width="160px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CompanyName" HeaderText="Company Name" SortExpression="CompanyName"
                                    UniqueName="CompanyName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                    <HeaderStyle Width="160px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Address1" HeaderText="Address1" SortExpression="Address1"
                                    UniqueName="Address1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                    <HeaderStyle Width="140px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Address2" HeaderText="Address2" SortExpression="Address2"
                                    Visible="false" UniqueName="Address2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="City" HeaderText="City" SortExpression="City"
                                    UniqueName="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="State" HeaderText="State" SortExpression="State"
                                    UniqueName="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                    <HeaderStyle Width="40px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ZipCode" HeaderText="ZIP" SortExpression="ZipCode"
                                    UniqueName="ZipCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false" MaxLength="5">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ServiceAddress1" HeaderText="Service Address 1"
                                    SortExpression="ServiceAddress1" UniqueName="ServiceAddress1" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="140px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ServiceAddress2" HeaderText="Service Address 2"
                                    SortExpression="ServiceAddress2" Visible="false" UniqueName="ServiceAddress2"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ServiceCity" HeaderText="Service City" SortExpression="ServiceCity"
                                    UniqueName="ServiceCity" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ServiceState" HeaderText="ServiceState" SortExpression="ServiceState"
                                    UniqueName="ServiceState" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                    <HeaderStyle Width="40px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ServiceZip" HeaderText="Service Zip" SortExpression="ServiceZip"
                                    UniqueName="ServiceZip" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false" MaxLength="5">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CompanyCode" HeaderText="Comp" SortExpression="CompanyCode"
                                    UniqueName="CompanyCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                    <HeaderStyle Width="50px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ProjectCode" HeaderText="Project" SortExpression="ProjectCode"
                                    UniqueName="ProjectCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                    <HeaderStyle Width="50px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="StatusSort" HeaderText="Status" SortExpression="StatusSort"
                                    ReadOnly="true" UniqueName="StatusSort" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                    <HeaderStyle Width="132px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn UniqueName="Status" DataField="Status" HeaderText="Status"
                                    SortExpression="Status" Visible="false">
                                    <ItemTemplate>
                                        <%# Eval("Status")%>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList runat="server" ID="ddlStatus" DataValueField="Value" DataTextField="Text">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <HeaderStyle Width="132px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridMaskedColumn DataField="Phone1" HeaderText="Phone 1" SortExpression="Phone1"
                                    Mask="###-###-####" Visible="false" UniqueName="Phone1" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="false" DataFormatString="{0:(###)###-####}">
                                    <HeaderStyle Width="85px" />
                                </telerik:GridMaskedColumn>
                                <telerik:GridMaskedColumn DataField="Phone2" HeaderText="Phone 2" SortExpression="Phone2"
                                    Mask="###-###-####" Visible="false" UniqueName="Phone2" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="false" DataFormatString="{0:(###)###-####}">
                                    <HeaderStyle Width="85px" />
                                </telerik:GridMaskedColumn>
                                <telerik:GridCheckBoxColumn DataField="IsReship" HeaderText="Reship?" SortExpression="IsReship"
                                    UniqueName="IsReship" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true">
                                    <HeaderStyle Width="50px" />
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridCheckBoxColumn DataField="DoNotShip" HeaderText="Opt Out" SortExpression="DoNotShip"
                                    UniqueName="DoNotShip" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true">
                                    <HeaderStyle Width="60px" />
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridCheckBoxColumn DataField="IsOkayToContact" HeaderText="Cont..." SortExpression="IsOkayToContact"
                                    UniqueName="IsOkayToContact" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true">
                                    <HeaderStyle Width="60px" />
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridDateTimeColumn DataField="AnalysisDate" HeaderText="AnalysisDate" SortExpression="AnalysisDate"
                                    UniqueName="AnalysisDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" DataFormatString="{0:MM/dd/yyyy}">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridDateTimeColumn DataField="ReceiptDate" HeaderText="ReceiptDate" SortExpression="ReceiptDate"
                                    UniqueName="ReceiptDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" DataFormatString="{0:MM/dd/yyyy}">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridDateTimeColumn DataField="CRImportedDate" HeaderText="Import Date" SortExpression="CRImportedDate"
                                    UniqueName="CRImportedDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true" DataFormatString="{0:MM/dd/yyyy}">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridBoundColumn DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity"
                                    UniqueName="Quantity" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridDateTimeColumn DataField="AuditFailureDate" HeaderText="AuditFailureDate"
                                    SortExpression="AuditFailureDate" Visible="true" ReadOnly="true" UniqueName="AuditFailureDate"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    DataFormatString="{0:MM/dd/yyyy}">
                                </telerik:GridDateTimeColumn>
                                <telerik:GridBoundColumn DataField="OperatingCompany" HeaderText="OpCo" AllowSorting="false">
                                    <HeaderStyle Width="45px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="BatchLabel" HeaderText="Batch ID" SortExpression="BatchLabel"
                                    Visible="false" UniqueName="BatchLabel" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Notes" HeaderText="Notes" SortExpression="Notes"
                                    Visible="false" UniqueName="Notes" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn DataField="OutOfState" HeaderText="OutOfState" SortExpression="OutOfState"
                                    Visible="false" UniqueName="OutOfState" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="true">
                                    <HeaderStyle Width="50px" />
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email"
                                    UniqueName="Email" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Method" HeaderText="Method" SortExpression="Method"
                                    UniqueName="Method" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false" Visible="true" ReadOnly="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="WaterHeaterFuel" HeaderText="WaterHeaterFuel"
                                    AllowSorting="false">
                                    <HeaderStyle Width="40px" />

                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="HeaterFuel" HeaderText="HeaterFuel"
                                    AllowSorting="false">
                                    <HeaderStyle Width="40px" />
                                </telerik:GridBoundColumn>
                                <%-- PG31 --%>
                                <telerik:GridBoundColumn DataField="FacilityType" UniqueName="FacilityType" HeaderText="Facility Type"
                                    AllowSorting="true">
                                    <HeaderStyle Width="40px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="RateCode" UniqueName="RateCode" HeaderText="Rate Code"
                                    AllowSorting="true">
                                    <HeaderStyle Width="40px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="IncomeQualified" UniqueName="IncomeQualified" HeaderText="Income Qualified"
                                    AllowSorting="true">
                                    <HeaderStyle Width="40px" />
                                </telerik:GridBoundColumn>
                                <%-- PG31 --%>
                                <telerik:GridBoundColumn DataField="PremiseID" HeaderText="PremiseID" SortExpression="PremiseID"
                                    UniqueName="PremiseID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="AccountNumberOld" HeaderText="AccountNumberOld"
                                    SortExpression="AccountNumberOld" UniqueName="AccountNumberOld" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="RequestedKit" HeaderText="Requested Kit"
                                    SortExpression="RequestedKit" UniqueName="RequestedKit" AutoPostBackOnFilter="true"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridBoundColumn>
                            </Columns>
                            <EditFormSettings UserControlName="../Controls/ConsumerRequestCard.ascx" EditFormType="WebUserControl">
                                <EditColumn UniqueName="EditCommandColumn1">
                                </EditColumn>
                            </EditFormSettings>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <asp:Panel ID="pnl1" runat="server">
                    </asp:Panel>
                    <asp:Panel ID="pnlPopUp" runat="server" CssClass="modalPopupError2" Style="display: none;">
                        <table width="100%">
                            <tr align="center">
                                <td align="center">
                                    <asp:Label ID="lbPopUp" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr align="center">
                                <td align="center">
                                    <asp:Button ID="btnCancelMpe" runat="server" Text="Ok" Width="80" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <ajaxToolkit:ModalPopupExtender ID="mpe" runat="server" PopupControlID="pnlPopUp"
                        TargetControlID="pnl1" OkControlID="btnCancelMpe" BackgroundCssClass="modalBackground">
                    </ajaxToolkit:ModalPopupExtender>

                    <script type="text/javascript">

                        function Expand(itemID) {
                            var Grid = $find('<%=RadGrid_ConsumerRequests.ClientID %>');
                            var scrollArea = document.getElementById('ctl00_ContentPlaceHolder1_RadGrid_ConsumerRequests')
                            var rowElement = document.getElementById(itemID);
                            window.scrollTo(0, rowElement.offsetTop + 200);
                        }
                    </script>

                </td>
            </tr>
            <%-- <tr>
          <td colspan="2">
              <asp:Literal runat="server" ID="help" Text=""></asp:Literal>
          </td>
   </tr>   --%>
        </table>
    </div>
    <%--<asp:Button runat="server" ID="Button_ClearAllInfo" Text="Clear All Test Info"
        CssClass="ptButton" onclick="Button_ClearAllInfo_Click" TabIndex="-1"  />--%>
</asp:Content>
