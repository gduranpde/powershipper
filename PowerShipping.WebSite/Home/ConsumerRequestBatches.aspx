<%@ Page Title="Consumer Request Batches" Language="C#" MasterPageFile="~/Shared/Default.Master" AutoEventWireup="true" CodeBehind="ConsumerRequestBatches.aspx.cs" Inherits="PowerShipping.WebSite.Home.ConsumerRequestBatches" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../Controls/ProjectsList.ascx" TagName="ProjectsList" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="pageDiv" onmousemove="dragWin(event);">


        <table>
            <tr>
                <td>
                    <h3>Consumer Request Batches</h3>
                </td>
                <td></td>
                <td></td>
                <td>
                    <uc1:ProjectsList ID="ProjectsList1" runat="server" IsCompanyUse="true" />
                </td>
            </tr>
            <tr>
                <td>Start Date
                <telerik:raddatepicker id="dpStartDateFilter" runat="server" mindate="01-01-1990" maxdate="01-01-2020"></telerik:raddatepicker>
                </td>
                <td>
                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" CssClass="ptButton" />
                </td>
                <td>
                    <asp:Button ID="btnClearFilter" runat="server" OnClick="btnClearFilter_Click" Text="Clear" CssClass="ptButton" />
                </td>
            </tr>
        </table>
        <br />

        <telerik:radgrid id="RadGrid_Batches" runat="server" width="100%" gridlines="None"
            autogeneratecolumns="false" pagesize="15" allowsorting="True" allowpaging="True"
            skin="Windows7"
            onneeddatasource="RadGrid_Batches_NeedDataSource"
            ondeletecommand="RadGrid_Batches_DeleteCommand"
            onitemcreated="RadGrid_Batches_ItemCreated"
            onitemdatabound="RadGrid_Batches_ItemDataBound"
            onitemcommand="RadGrid_Batches_ItemCommand">
        <ExportSettings ExportOnlyData="true" IgnorePaging="true">            
            </ExportSettings>
            <ClientSettings>               
            </ClientSettings>
            <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
            AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true">
                <SortExpressions>
                          <telerik:GridSortExpression FieldName="ImportedDate" SortOrder="Descending" />
                        </SortExpressions>
            <CommandItemSettings
                    ShowExportToWordButton="true"
                    ShowExportToExcelButton="true"
                    ShowExportToCsvButton="true"
                    ShowExportToPdfButton="true"
                    />
            <Columns>
             
                <telerik:GridButtonColumn UniqueName="DeleteColumn" Text="Delete" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmText="Are you sure delete this batch?"/>
                 <telerik:GridButtonColumn UniqueName="ProcessColumn" Text="Process" CommandName="Process"/>
                <telerik:GridBoundColumn DataField="BatchLabel" HeaderText="Batch ID" SortExpression="BatchLabel" ReadOnly="true"
                        UniqueName="BatchLabel" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn> 
                  <telerik:GridBoundColumn DataField="FileName" HeaderText="FileName" SortExpression="FileName"
                        UniqueName="FileName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                        EditFormColumnIndex="0">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="ImportedBy" HeaderText="ImportedBy" SortExpression="ImportedBy"
                        UniqueName="ImportedBy" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridDateTimeColumn DataField="ImportedDate" HeaderText="ImportedDate" SortExpression="ImportedDate" ReadOnly="true"
                         UniqueName="ImportedDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true">                        
                  </telerik:GridDateTimeColumn>
                 <telerik:GridBoundColumn DataField="RequestsInBatch" HeaderText="RequestsInBatch" SortExpression="RequestsInBatch"
                        UniqueName="RequestsInBatch" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="ImportedSuccessfully" HeaderText="ImportedSuccessfully" SortExpression="ImportedSuccessfully"
                        UniqueName="ImportedSuccessfully" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="ImportedWithException" HeaderText="ImportedWithException" SortExpression="ImportedWithException"
                        UniqueName="ImportedWithException" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn> 
                  <telerik:GridBoundColumn DataField="CompanyCode" HeaderText="CompanyCode" SortExpression="CompanyCode"
                                    UniqueName="CompanyCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">                                
                                </telerik:GridBoundColumn> 
                          <telerik:GridBoundColumn DataField="ProjectCode" HeaderText="ProjectCode" SortExpression="ProjectCode"
                                    UniqueName="ProjectCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">                                
                                </telerik:GridBoundColumn> 
            </Columns>
            </MasterTableView>
    </telerik:radgrid>
        <%-- PG31--%>
        <asp:Panel ID="Panel1" runat="server">
        </asp:Panel>
        <asp:Panel ID="ProcessImportBatchPanel" runat="server" CssClass="modalPopup" HorizontalAlign="Center" Width="320px">
            <asp:HiddenField ID="hfId" runat="server" />
            <table>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="Label1" runat="server" Text="Batch Process Data" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">Receipt Date
                    </td>
                    <td align="left">
                        <asp:TextBox ID="TxtReceiptDate" runat="server" ToolTip="Please click on the calendar Image"
                            Width="100px"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TxtReceiptDate"
                            PopupButtonID="ImgBtn_calendar" Format="MM/dd/yyyy">
                        </ajaxToolkit:CalendarExtender>
                        <asp:ImageButton ID="ImgBtn_calendar" runat="server" ImageUrl="~/App_Themes/Default/Images/small_calendar.png" />
                        <asp:RequiredFieldValidator ID="RqdFVReceiptDate" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="TxtReceiptDate"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">Kit Type
                    </td>
                    <td align="left">
                        <asp:ListBox runat="server" Rows="3" ID="ListBox_Kit_type" DataValueField="Id" DataTextField="KitName"></asp:ListBox><asp:RequiredFieldValidator ID="RdFdVKittype" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="ListBox_Kit_type"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">Shipper
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlShipper" runat="server" DataTextField="ShipperCode" DataValueField="Id"
                            Width="150px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right">Purchase Order Number
                    </td>
                    <td align="left">
                        <asp:TextBox ID="Txt_PurchaseOrderNumber" runat="server" Rows="3"
                            Width="150px" /><asp:RequiredFieldValidator ID="RqdFdVPurchaseOrder" runat="server" ErrorMessage="* Mandatory" ForeColor="Red" ControlToValidate="Txt_PurchaseOrderNumber"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnOk" runat="server" Text="Cancel" />
                    </td>
                </tr>
            </table>
        </asp:Panel>

        <ajaxToolkit:ModalPopupExtender ID="mpe" runat="server" PopupControlID="ProcessImportBatchPanel"
            TargetControlID="Panel1" OkControlID="btnOk" BackgroundCssClass="modalBackground" BehaviorID="ModalBehaviour">
        </ajaxToolkit:ModalPopupExtender>
        <div id="divBack" style="background-color: black; display: none; height: 100%; opacity: 0.5; position: fixed; top: 0; width: 100%; z-index: 7000; margin-left: -240px;">
        </div>

        <div id="ImportActivityPass" class="div_ImportProcessOuter">
            <div style="width: 400px; height: 30px; cursor: move;" onmousedown="mouse_down(event,'ImportActivityPass');"
                onmouseup="mouse_up(event,'ImportActivityPass');">
                <div style="width: 200px; float: left; padding-top: 10px;">
                    <span style="margin-left: 10px;">Warning Message!</span>
                </div>
                <div style="width: 200px; float: right; padding-top: 10px;">
                    <input id="Button1" type="button" value="" onclick="return ImportActivityPassPopup();"
                        style="background-image: url('../../App_Themes/Default/Images/btnPopupClose.png'); border: none; width: 48px; height: 21px; position: relative; margin-left: 147px; margin-top: -11px; cursor: pointer;" />
                </div>
            </div>
            <div style="width: 400px; margin-top: 27px;">
                <div style="height: 10px; margin-left: 15px; right: -1px; width: 362px;">
                    <asp:Label ID="Label2" runat="server" Text="Warning: This process will mark all Consumer Request records in the batch as delivered and create the associated shipment records. This process cannot be undone."
                        ForeColor="Red" Style="margin-left: 10px;"></asp:Label>
                </div>
            </div>
            <br />
            <br />
            <div style="width: 236px; margin-top: 65px; height: 43px; padding-left: 77px;">
                <div style="width: 158px; float: left; height: 29px;">
                    <input id="Btn_Proceed" type="button" value="Proceed" onclick="return ShowModalPopup();"
                        style="cursor: pointer;" />
                </div>
                <div style="width: 200px; float: right;">
                    <input id="Btn_Cancel" type="button" value="Cancel" onclick="return ImportActivityPassPopup();"
                        style="margin-left: 176px; margin-top: -22px; cursor: pointer;" />
                </div>
            </div>
        </div>

        <div id="ImportProcessFailed" class="div_ImportProcessOuter">
            <div style="width: 400px; height: 30px; cursor: move;" onmousedown="mouse_down(event,'ImportProcessFailed');"
                onmouseup="mouse_up(event,'ImportProcessFailed');">
                <div style="width: 200px; float: left; padding-top: 10px;">
                    <span style="margin-left: 10px;">Error Message!</span>
                </div>
                <div style="width: 200px; float: right; padding-top: 10px;">
                    <input id="btnCloseDeviceID" type="button" value="" onclick="return ImportProcessFailed();"
                        style="background-image: url('../../App_Themes/Default/Images/btnPopupClose.png'); border: none; width: 48px; height: 21px; position: relative; margin-left: 147px; margin-top: -11px; cursor: pointer;" />
                </div>
            </div>
            <div style="width: 400px; margin-top: 27px;">
                <div style="height: 10px; margin-left: 15px; right: -1px; width: 362px;">
                    <asp:Label ID="LblWarningMessage" runat="server" Text="�Error: One or more records in the batch have already been process. Process aborted.�"
                        ForeColor="Red" Style="margin-left: 10px;"></asp:Label>
                </div>
            </div>
            <br />
            <br />
            <div style="width: 400px; margin-top: 55px;">
                <div style="width: 200px; float: right;">
                    <input id="cancel_ImportProcessFailed" type="button" value="Ok" onclick="return ImportProcessFailed();"
                        style="margin-left: -29px; margin-top: -22px; cursor: pointer;" />
                </div>
            </div>
        </div>
        <%-- PG31--%>
    </div>
</asp:Content>
