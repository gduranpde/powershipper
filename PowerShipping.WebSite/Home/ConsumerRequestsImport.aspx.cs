﻿using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.ImportExport;
using PowerShipping.Logging;
using System;
using System.Drawing;
using System.Web.UI;
using Telerik.Web.UI;

namespace PowerShipping.WebSite
{
    public partial class ConsumerRequestsImport : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList1.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }

            //if (!Page.IsPostBack)
            //    this.help.Text = string.Format("<a href='../Help.aspx?type={0}' target='_blank'\">import help</a>", typeof(ConsumerRequest).ToString());

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Consumer Requests Import");
            }

            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;
            if (ProjectsList1.ProjectId == Guid.Empty)
                RadUpload1.Enabled = false;
        }

        private void ProjectsList1_OnProjectChanged(string projectId)
        {
            hlExceptions.Visible = false;

            labelErrors.Visible = false;
            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                RadUpload1.Enabled = true;
                //ProgressArea1.Visible = true;
            }
            else
            {
                RadUpload1.Enabled = false;
                ProgressArea1.Visible = false;
            }
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            ProgressArea1.Visible = true;
            labelErrors.Visible = true;
            try
            {
                foreach (string fileInputID in Request.Files)
                {
                    UploadedFile file = UploadedFile.FromHttpPostedFile(Request.Files[fileInputID]);

                    if (file.GetExtension() != ".csv")
                    {
                        throw new Exception("Invalid file format");
                    }

                    if (file.ContentLength > 0)
                    {

                        //var path = Path.Combine(ConfigurationManager.AppSettings["UploadFolder"], file.GetName());
                        //file.SaveAs(path);
                        var path = Server.MapPath("~/Uploads/") + file.GetName();
                        file.SaveAs(path);

                        if (ProjectsList1.ProjectId == Guid.Empty)
                        {
                            labelErrors.ForeColor = Color.Red;
                            labelErrors.Text = "Please select project.";
                            return;
                        }

                        var importer = new ConsumerRequestImporter(path, User.Identity.Name, ProjectsList1.ProjectId);
                        importer.ImportProgress += ShowImportProgress;
                        ConsumerRequestBatch batch = importer.Import();

                        labelErrors.Text = string.Empty;
                        if (!importer.ImportErrors.IsEmpty)
                        {
                            labelErrors.ForeColor = Color.Red;
                            importer.ImportErrors.ForEach(err => labelErrors.Text += err + "<br>");
                        }
                        else
                        {
                            hlExceptions.Visible = false;

                            labelErrors.ForeColor = Color.Black;
                            labelErrors.Text = "Total requests: " + batch.RequestsInBatch + ". <br/>Imported Successfully: " + batch.ImportedSuccessfully +
                                               ". <br/>Imported with exceptions: " + batch.ImportedWithException + ". <br/>Batch Label: " + batch.BatchLabel + ".";

                            if (batch.ImportedWithException > 0)
                            {
                                hlExceptions.Text = "View list of exceptions";
                                hlExceptions.NavigateUrl = "~/Home/ConsumerExceptions.aspx?BatchId=" + batch.Id;
                                hlExceptions.Visible = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                labelErrors.Text = ex.Message;
                CurrentLogger.Log.Error(ex);
            }         
        }

        private void ShowImportProgress(object sender, ImportExportProgressEventArgs e)
        {
            if (Response.IsClientConnected)
            {
                var progress = RadProgressContext.Current;
                progress.SecondaryTotal = e.Total;
                progress.SecondaryValue = e.Value;
                progress.SecondaryPercent = e.Total == 0 ? 0 : Convert.ToInt32((100 * Convert.ToDouble(e.Value)) / Convert.ToDouble(e.Total));
                progress.CurrentOperationText = e.Message;
            }
        }
    }
}