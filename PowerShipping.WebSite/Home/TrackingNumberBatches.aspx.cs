﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class TrackingNumberBatches : Page
    {
        private readonly BatchesPresenter presenter = new BatchesPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList1.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Tracking Number Batches");
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            RadGrid_Batches.Rebind();
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            dpStartDateFilter.SelectedDate = null;
        }

        protected void RadGrid_Batches_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                var spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                var addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGrid_Batches_ItemDataBound(object sender, GridItemEventArgs e)
        {
        }

        protected void RadGrid_Batches_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_Batches.DataSource = GetFilteredList();
        }

        protected void RadGrid_Batches_DeleteCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.DeleteCommandName)
                {
                    var id =
                        new Guid(
                            (e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"].ToString());
                    //presenter.DeleteTrackingNumberBatch(id);
                    List<ShipmentDetail> list = presenter.DeleteTrackingNumberBatch(id);
                    if (list.Count !=0)
                    {
                        hiddenBatchId.Value = id.ToString();
                        int inTrans = 0;
                        int delivered = 0;
                        int returned = 0;
                        foreach (var detail in list)
                        {
                            if (detail.ShippingStatus == ShipmentShippingStatus.InTransit)
                                inTrans++;
                            if (detail.ShippingStatus == ShipmentShippingStatus.Delivered)
                                delivered++;
                            if (detail.ShippingStatus == ShipmentShippingStatus.Returned)
                                returned++;
                        }

                        lbInTransit.Text = inTrans.ToString();
                        lbDelivered.Text = delivered.ToString();
                        lbReturned.Text = returned.ToString();

                        resultPanel.Visible = true;
                    }
                    RadGrid_Batches.Rebind();
                }
            }
            catch (Exception ex)
            {
                var l = new Label();
                l.Text = "Unable to delete Batch. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                RadGrid_Batches.Controls.Add(l);
            }
        }

        private List<TrackingNumberBatch> GetFilteredList()
        {
            DateTime? startDate = dpStartDateFilter.SelectedDate;

            return presenter.GetTrackingNumberBatches(startDate);
        }

        protected void btnProceed_Click(object sender, EventArgs e)
        {
            //delete
            presenter.DeleteTrackingNumberBatch(new Guid(hiddenBatchId.Value), false);
            resultPanel.Visible = false;
            RadGrid_Batches.Rebind();
        }

        protected void btnViewList_Click(object sender, EventArgs e)
        {
            //redirect
            Response.Redirect("ShipmentDetails.aspx?FedExBatchId=" + hiddenBatchId.Value);
            resultPanel.Visible = false;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            resultPanel.Visible = false;
        }
    }
}