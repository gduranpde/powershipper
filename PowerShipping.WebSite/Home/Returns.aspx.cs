﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.ImportExport;
using PowerShipping.WebSite.Controls;
using Telerik.Web.UI;
using Label = System.Web.UI.WebControls.Label;

namespace PowerShipping.WebSite.Home
{
    public partial class Returns : System.Web.UI.Page
    {
        ReturnsPresenter presenter = new ReturnsPresenter();
        ConsumerRequestsPresenter presenterCR = new ConsumerRequestsPresenter();
        KitTypesPresenter presenterKT = new KitTypesPresenter();
        ShipmentDetailsPresenter presenterSD = new ShipmentDetailsPresenter();
        UserManagementPresenter presenterAdv = new UserManagementPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            ReturnsDetailFilter1.OnFilter += ReturnsDetailFilter1_OnFilter;

            AjaxPro.Utility.RegisterTypeForAjax(typeof(Returns));
            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;
            //btnStatusChange.Enabled = Common.IsCanEdit();

            //tbShipmentNumber.Focus();

            if (!IsPostBack)
            {
                EnumListItem[] list = EnumUtils.EnumToListItemsWithNull(typeof(ShipmentReshipStatus));
                list.SetValue(new EnumListItem("All", null), 0);

                //ddReshipStatusFilter.DataSource = list;
                //ddReshipStatusFilter.DataBind();

                EnumListItem[] list2 = EnumUtils.EnumToListItems(typeof(ShipmentReshipStatus));
                //RadComboBox_Status.DataSource = list2;
                //RadComboBox_Status.DataBind();
            }

            //RadGrid_Returns.Columns.FindByUniqueName("ClientSelectColumn").Visible = Common.IsCanEdit();
            //btnStatusChange.Enabled = Common.IsCanEdit();

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Returns");
            }
        }

        private void ProjectsList1_OnProjectChanged(string projectId)
        {
            RadGrid_Returns.Rebind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            RadGrid_Returns.Rebind();
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            //ddReshipStatusFilter.SelectedValue = null;
            //dpStartDateFilter.SelectedDate = null;
            //dpEndDateFilter.SelectedDate = null;
            //tbAccountNumberFilter.Text = null;
            //tbFedExTrackingNumber.Text = null;
            //tbShipmentNumber.Text = null;
        }

        protected void btnOkReship_OnClick(object sender, EventArgs e)
        {
            if (ProjectsList1.ProjectId == Guid.Empty)
            {
                reshipSpecialProcessingCard.ErrorText = "Please select project.";
                mpeList.Show();
                return;
            }

            if (reshipSpecialProcessingCard.IsAllFilled)
            {
                List<ShipmentDetail> listBase = reshipSpecialProcessingCard.ShipmentDetailsList;
                foreach (ShipmentDetail shipmentDetail in listBase)
                {
                    ConsumerRequest cr = presenterCR.GetById(shipmentDetail.ConsumerRequestId);

                    shipmentDetail.ReshipStatus = ShipmentReshipStatus.Reshipped;

                    //shipmentDetail.ShipperCode=RadComboBox_Shipper
                    //shipmentDetail.ReturnDate=
                    //for validation
                    cr.States = presenterAdv.GetStatesByProject(cr.ProjectId);
                    cr.IsReship = true;
                    cr.Status = ConsumerRequestStatus.PendingShipment;
                    //Rules.RulesForReship(shipmentDetail, cr);

                    var validationResult = EntityValidation.Validate(cr);
                    if (validationResult.Success)
                    {
                        cr.ChangedBy = Membership.GetUser().UserName;

                        //-- New --//

                        //shipmentDetail.ShipmentDetailAddress1 = cr.Address1;
                        //shipmentDetail.ShipmentDetailAddress2 = cr.Address2;
                        //shipmentDetail.ShipmentDetailCity = cr.City;
                        //shipmentDetail.ShipmentDetailState = cr.State;
                        //shipmentDetail.ShipmentDetailZip = cr.ZipCode;

                        //---  End ---//

                        presenter.Save(shipmentDetail, cr);
                    }
                    else
                    {
                        //Label l = new Label();
                        //l.Text = validationResult.Errors.ToString();
                        //l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                        //RadGrid_Returns.Controls.Add(l);
                        ErrorShow(validationResult.Errors.ToString());
                        return;
                    }
                }

                List<ShipmentDetail> list = reshipSpecialProcessingCard.ResultList;

                foreach (ShipmentDetail shipmentDetail in list)
                {
                    ConsumerRequest cr = presenterCR.GetByAccountNumber(shipmentDetail.AccountNumber, ProjectsList1.ProjectId);

                    shipmentDetail.ConsumerRequestId = cr.Id;
                    shipmentDetail.IsReship = cr.IsReship;
                    shipmentDetail.ProjectId = cr.ProjectId;

                    //----------- New --------------//

                    shipmentDetail.ShipmentDetailAddress1 = cr.Address1;
                    shipmentDetail.ShipmentDetailAddress2 = cr.Address2;
                    shipmentDetail.ShipmentDetailCity = cr.City;
                    shipmentDetail.State = cr.State;
                    shipmentDetail.ZipCode = cr.ZipCode;

                    presenterSD.Save(shipmentDetail);

                    //cr.IsReship = true;
                    //cr.Status = ConsumerRequestStatus.PendingShipment;
                    //presenterCR.Save(cr);
                }

                reshipSpecialProcessingCard.ShipmentDetailsList = null;
                reshipSpecialProcessingCard.ErrorText = null;
                mpeList.Hide();
                RadGrid_Returns.Rebind();
            }
            else
            {
                reshipSpecialProcessingCard.ErrorText = "Fill all fields.";

                //- New Aug6 -//
                reshipSpecialProcessingCard.BindData();
                //----//

                mpeList.Show();
            }

            Page.ClientScript.RegisterStartupScript(this.GetType(), "hidep2", "hidep2();", true);

            //-----------------------------------

            List<Guid> lst = (List<Guid>)Session["Lst"];

            if (lst != null)
            {
                List<ShipmentDetail> list1 = new List<ShipmentDetail>();

                foreach (Guid val in lst)
                {
                    ShipmentDetail sd = presenter.GetById(val);
                    list1.Add(sd);
                }

                //reshipSpecialProcessingCard.ErrorText = null;
                reshipSpecialProcessingCard.ShipmentDetailsList = list1;
                reshipSpecialProcessingCard.BindData();
            }
        }

        //protected void btnCancelReship_OnClick(object sender, EventArgs e)
        //{
        //    reshipSpecialProcessingCard.ShipmentDetailsList = null;
        //    mpeList.Hide();
        //}

        #region AjaxMethods

        [WebMethod]
        public static void Ajax_SaveShipmentDetails(string sAddress1, string sAddress2, string sCity, string sState, string sZipcode)
        {
            ShipmentDetailsPresenter presenterSD = new ShipmentDetailsPresenter();

            ShipmentDetail sd = (ShipmentDetail)System.Web.HttpContext.Current.Session["ShipmentDetailObject"];
            //sd.ShipmentDetailAddress1 = sAddress1;
            //sd.ShipmentDetailAddress2 = sAddress2;

            //sd.ShipmentDetailCity = sCity;

            //sd.ShipmentDetailZip = sZipcode;
            //sd.ShipmentDetailState = sState;

            //presenterSD.Save(sd);

            //-- New Aug 6 --//

            ConsumerRequestsPresenter presenterCR = new ConsumerRequestsPresenter();

            Guid crID = new Guid(sd.ConsumerRequestId.ToString());

            ConsumerRequest CR = presenterCR.GetById(crID);

            CR.Address1 = sAddress1.ToUpper();
            CR.Address2 = sAddress2.ToUpper();
            CR.City = sCity.ToUpper();
            CR.State = sState;
            CR.ZipCode = sZipcode;
            CR.ChangedBy = Membership.GetUser().UserName;

            presenterCR.Save(CR);

            //---------------//

            System.Web.HttpContext.Current.Session["ShipmentDetailObject"] = "";
        }

        #endregion AjaxMethods

        //protected void btnStatusChange_Click(object sender, EventArgs e)
        //{
        //    if ((ShipmentReshipStatus)Convert.ToInt32(RadComboBox_Status.SelectedValue) == ShipmentReshipStatus.Reshipped)
        //    {
        //        if (ProjectsList1.ProjectId == Guid.Empty)
        //        {
        //            //Label l = new Label();
        //            //l.Text = "Select project";
        //            //l.ControlStyle.ForeColor = System.Drawing.Color.Red;
        //            //RadGrid_Returns.Controls.Add(l);
        //            ErrorShow("Please select project.");
        //            return;
        //        }

        //        List<Guid> lstInt = new List<Guid>();

        //        List<ShipmentDetail> list = new List<ShipmentDetail>();
        //        foreach (GridDataItem dataItem in RadGrid_Returns.SelectedItems)
        //        {
        //            Guid id = new Guid(dataItem.GetDataKeyValue("Id").ToString());
        //            ShipmentDetail sd = presenter.GetById(id);
        //            list.Add(sd);
        //            lstInt.Add(id);
        //        }

        //        Session["Lst"] = lstInt;

        //        //reshipSpecialProcessingCard.ProjectId = ProjectsList1.ProjectId;
        //        reshipSpecialProcessingCard.ErrorText = null;
        //        reshipSpecialProcessingCard.ShipmentDetailsList = list;
        //        reshipSpecialProcessingCard.BindData();
        //        mpeList.Show();
        //    }
        //    else
        //    {
        //        foreach (GridDataItem dataItem in RadGrid_Returns.SelectedItems)
        //        {
        //            Guid id = new Guid(dataItem.GetDataKeyValue("Id").ToString());
        //            ShipmentDetail sd = presenter.GetById(id);
        //            sd.ReshipStatus = (ShipmentReshipStatus)Convert.ToInt32(RadComboBox_Status.SelectedValue);

        //            ConsumerRequest cr = null;
        //            if (sd.ReshipStatus == ShipmentReshipStatus.Dead ||
        //                sd.ReshipStatus == ShipmentReshipStatus.Reshipped)
        //            {
        //                cr = presenterCR.GetById(sd.ConsumerRequestId);
        //                Rules.RulesForReship(sd, cr);
        //            }
        //            presenter.Save(sd);

        //            if (cr != null)
        //                presenterCR.Save(cr);
        //        }
        //    }
        //    RadGrid_Returns.Rebind();
        //    UserControl objofShippingAddressdialog = (UserControl)reshipSpecialProcessingCard.FindControl("ShippingAddressdialog");
        //    objofShippingAddressdialog.Visible = false;
        //    HtmlGenericControl htm = (HtmlGenericControl)reshipSpecialProcessingCard.FindControl("backgroundElement1");
        //    htm.Style.Add("display", "none");
        //}

        protected void RadGrid_Returns_ItemCommand(object source, GridCommandEventArgs e)
        {
            
            if (e.CommandName == RadGrid.EditCommandName)
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scroll", "setTimeout(function(){ Expand('" + item.ClientID + "');}, 100);", true);   
                }
            }

            //-- New Aug 7 --//

            if (e.CommandName == "ExportToExcel")
            {
                this.RadGrid_Returns.MasterTableView.GetColumn("CRAddress1").Display = true;
                this.RadGrid_Returns.MasterTableView.GetColumn("CRAddress2").Display = true;
                this.RadGrid_Returns.MasterTableView.GetColumn("CRCity").Display = true;
                this.RadGrid_Returns.MasterTableView.GetColumn("CRState").Display = true;
                this.RadGrid_Returns.MasterTableView.GetColumn("CRZipCode").Display = true; 
            }

            //this.RadGrid_Returns.MasterTableView.Columns.FindByUniqueName("EditCommandColumn").Visible = false;
            //this.RadGrid_Returns.MasterTableView.Columns.FindByUniqueName("ClientSelectColumn").Visible = false;
            

            //----//
        }

        protected void RadGrid_Returns_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                Button spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                LinkButton addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGrid_Returns_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item is GridEditFormItem) && e.Item.IsInEditMode)
            {
                GridEditFormItem gridEditFormItem = (GridEditFormItem)e.Item;
                //DropDownList dropDownList = (DropDownList)gridEditFormItem["ShippingStatus"].FindControl("ddlShippingStatus");
                //dropDownList.DataSource = Utils.EnumToListItems(typeof(ShipmentShippingStatus));
                //dropDownList.DataBind();

                //dropDownList.SelectedValue = Convert.ToInt32(((ShipmentDetail) gridEditFormItem.DataItem).ShippingStatus).ToString();

                //DropDownList dropDownList2 = (DropDownList)gridEditFormItem["ReshipStatus"].FindControl("ddlReshipStatus");
                //dropDownList2.DataSource = Utils.EnumToListItems(typeof(ShipmentReshipStatus));
                //dropDownList2.DataBind();

                //dropDownList2.SelectedValue = Convert.ToInt32(((ShipmentDetail)gridEditFormItem.DataItem).ReshipStatus).ToString();

                //RadDatePicker datePicker = (RadDatePicker)gridEditFormItem["ShippingStatusDate"].FindControl("datePickerEdit");
                //datePicker.SelectedDate = ((ShipmentDetail) gridEditFormItem.DataItem).ShippingStatusDate;

                //DropDownList dropDownList3 = (DropDownList)gridEditFormItem["KitTypeName"].FindControl("ddlKitType");
                //dropDownList3.DataSource = presenterKT.GetAll();
                //dropDownList3.DataBind();

                //dropDownList3.SelectedValue = ((ShipmentDetail)gridEditFormItem.DataItem).KitTypeId.ToString();
            }
        }

        protected void RadGrid_Returns_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_Returns.DataSource = GetFilteredList();
        }

        protected void RadGrid_Returns_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    UserControl uc = (UserControl)reshipSpecialProcessingCard.FindControl("ShippingAddressdialog");
                    Panel pnl2 = (Panel)uc.FindControl("pnlPopUp2");
                    pnl2.Style.Add("display", "none");
                    HtmlGenericControl div = (HtmlGenericControl)reshipSpecialProcessingCard.FindControl("backgroundElement1");
                    div.Style.Add("display", "none");
                    if (e.Item is GridEditFormItem)
                    {
                        GridEditFormItem item = (GridEditFormItem)e.Item;
                        Guid id = new Guid(item.GetDataKeyValue("Id").ToString());
                        if (id != Guid.Empty)
                        {
                            if (e.Item is GridEditableItem)
                            {
                                object o = ((ReturnCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                                ShipmentDetail sDetail = (ShipmentDetail)o;

                                ShipmentDetail sd = presenter.GetById(sDetail.Id);

                                //--- New --//

                                //sd.Address1 = sDetail.Address1;
                                //sd.Address2 = sDetail.Address2;
                                //sd.City = sDetail.City;
                                //sd.State = sDetail.State;
                                //sd.ZipCode = sDetail.ZipCode;

                                //sd.ShipmentDetailAddress1 = sDetail.Address1;
                                //sd.ShipmentDetailAddress2 = sDetail.Address2;
                                //sd.ShipmentDetailCity = sDetail.City;
                                //sd.ShipmentDetailState = sDetail.State;
                                //sd.ShipmentDetailZip = sDetail.ZipCode;

                                //--//
                                ConsumerRequest cr = presenterCR.GetById(sDetail.ConsumerRequestId);

                                sd.ShippingStatusDate = sDetail.ShippingStatusDate;

                                sd.ReshipStatus = sDetail.ReshipStatus;

                                sd.Notes = sDetail.Notes;
                                sd.ReturnDate = sDetail.ReturnDate;
                                sd.ShipperId = sDetail.ShipperId;

                                sd.ReasonId = sDetail.ReasonId;

                                cr.AccountName = sDetail.AccountName;
                                if (!string.IsNullOrEmpty(cr.AccountName))
                                    cr.AccountName = cr.AccountName.ToUpperInvariant();

                                cr.Address1 = sDetail.Address1;
                                if (!string.IsNullOrEmpty(cr.Address1))
                                    cr.Address1 = cr.Address1.ToUpperInvariant();

                                cr.Address2 = sDetail.Address2;
                                if (!string.IsNullOrEmpty(cr.Address2))
                                    cr.Address2 = cr.Address2.ToUpperInvariant();

                                cr.City = sDetail.City;
                                if (!string.IsNullOrEmpty(cr.City))
                                    cr.City = cr.City.ToUpperInvariant();

                                cr.Email = sd.Email;
                                cr.Phone1 = EnumUtils.GetPhoneDigits(sDetail.Phone1);
                                cr.Phone2 = EnumUtils.GetPhoneDigits(sDetail.Phone2);

                                cr.State = sDetail.State;
                                if (!string.IsNullOrEmpty(cr.State))
                                    cr.State = cr.State.ToUpperInvariant();

                                cr.ZipCode = sDetail.ZipCode;

                                //cr.ProjectId = ProjectsList1.ProjectId;

                                //for validation
                                cr.States = presenterAdv.GetStatesByProject(cr.ProjectId);

                                if (sd.ReshipStatus == ShipmentReshipStatus.Reshipped)
                                {
                                    //-- New --//
                                    sd.Address1 = sDetail.Address1;
                                    sd.Address2 = sDetail.Address2;
                                    sd.City = sDetail.City;
                                    sd.State = sDetail.State;
                                    sd.ZipCode = sDetail.ZipCode;
                                    //--New-//

                                    List<ShipmentDetail> list = new List<ShipmentDetail>();
                                    list.Add(sd);

                                    reshipSpecialProcessingCard.ErrorText = null;
                                    reshipSpecialProcessingCard.ShipmentDetailsList = list;
                                    reshipSpecialProcessingCard.BindData();

                                    //-- New //

                                    Rules.RulesForReship(sd, cr);

                                    var validationResult1 = EntityValidation.Validate(cr);
                                    if (validationResult1.Success)
                                    {
                                        cr.ChangedBy = Membership.GetUser().UserName;

                                        presenter.Save(sd, cr);
                                        RadGrid_Returns.Rebind();
                                    }
                                    else
                                    {
                                        //Label l = new Label();
                                        //l.Text = validationResult.Errors.ToString();
                                        //l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                                        //RadGrid_Returns.Controls.Add(l);
                                        ErrorShow(validationResult1.Errors.ToString());
                                    }

                                    //-- End --//

                                    mpeList.Show();
                                    return;
                                }

                                Rules.RulesForReship(sd, cr);

                                var validationResult = EntityValidation.Validate(cr);
                                if (validationResult.Success)
                                {
                                    cr.ChangedBy = Membership.GetUser().UserName;

                                    presenter.Save(sd, cr);
                                    RadGrid_Returns.Rebind();
                                }
                                else
                                {
                                    //Label l = new Label();
                                    //l.Text = validationResult.Errors.ToString();
                                    //l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                                    //RadGrid_Returns.Controls.Add(l);
                                    ErrorShow(validationResult.Errors.ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                //Label l = new Label();
                //l.Text = "Unable to update Return. Reason: " + ex.Message;
                //l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                //RadGrid_Returns.Controls.Add(l);
                ErrorShow("Unable to update Return. Reason: " + ex.Message);
            }
        }

        private List<ShipmentDetail> GetFilteredList()
        {
            //int? status = null;
            //if (!string.IsNullOrEmpty(ddReshipStatusFilter.SelectedValue))
            //{
            //    status = Convert.ToInt32(ddReshipStatusFilter.SelectedValue);
            //}

            ////DateTime? startDate = dpStartDateFilter.SelectedDate;
            ////DateTime? endDate = dpEndDateFilter.SelectedDate;
            //string shipmentNum = tbShipmentNumber.Text;
            //string accNum = tbAccountNumberFilter.Text;
            //string fedNum = tbFedExTrackingNumber.Text;

            //--------------------------------------------------------------------//

            int? status = null;
            if (!string.IsNullOrEmpty(ReturnsDetailFilter1.ReshipStatus.ToString()))
            {
                status = Convert.ToInt32(ReturnsDetailFilter1.ReshipStatus);
            }

            string shipmentNum = ReturnsDetailFilter1.ShipmentNumber;
            string accNum = ReturnsDetailFilter1.AccountNumber;
            string fedNum = ReturnsDetailFilter1.FedexNumber;
            string accName = ReturnsDetailFilter1.AccountName;
            string PONumber = ReturnsDetailFilter1.PONumber;
            string OpCo = ReturnsDetailFilter1.OpCo;
            DateTime? startDate = ReturnsDetailFilter1.StartDate;
            DateTime? endDate = ReturnsDetailFilter1.EndDate;

            // Guid? kitTypeID = new Guid(ReturnsDetailFilter1.KitTypeID.ToString());

            if (accNum == string.Empty)
                accNum = null;
            if (fedNum == string.Empty)
                fedNum = null;
            if (shipmentNum == string.Empty)
                shipmentNum = null;
            if (accName == string.Empty)
                accName = null;
            if (PONumber == string.Empty)
                PONumber = null;
            if (OpCo == string.Empty)
                OpCo = null;

            //if (kitTypeID == Guid.Empty)
            //    kitTypeID = null;

            //if (fedNum != null)
            //{
            //    if (fedNum.Length > 20)
            //        fedNum = fedNum.Substring(fedNum.Length - 20);
            //}

            if (shipmentNum != null)
            {
                if (shipmentNum.Length > 10)
                    shipmentNum = shipmentNum.Substring(0, 10);
            }

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            Guid? kitTypeID = null;
            Guid? shipperID = null;
            Guid? reasonID = null;

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList1.ProjectId;
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = ProjectsList1.UserId;
            }

            if (ProjectsList1.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList1.CompanyId;
            }

            if (ReturnsDetailFilter1.KitTypeID != Guid.Empty)
            {
                kitTypeID = ReturnsDetailFilter1.KitTypeID;
            }

            if (ReturnsDetailFilter1.ShipperID != Guid.Empty)
            {
                shipperID = ReturnsDetailFilter1.ShipperID;
            }

            if (ReturnsDetailFilter1.ReasonID != Guid.Empty)
            {
                reasonID = ReturnsDetailFilter1.ReasonID;
            }

            return presenter.GetAll(status, shipmentNum, accNum, fedNum, userId, projectId, companyId, accName, PONumber, kitTypeID, shipperID, OpCo, reasonID, startDate, endDate);
        }

        private void ErrorShow(string error)
        {
            lbPopUp.Text = error;
            lbPopUp.ControlStyle.ForeColor = System.Drawing.Color.Red;
            mpe.Show();
            //mpeList.Show();
        }

        private void ReturnsDetailFilter1_OnFilter()
        {
            RadGrid_Returns.Rebind();

            //HiddenFiled_CustomQuery.Value = qry;
            // RadGrid_ShipmentDetails_NeedDataSource(null, null);
            //RadGrid_ShipmentDetails.Rebind();
        }
    }
}