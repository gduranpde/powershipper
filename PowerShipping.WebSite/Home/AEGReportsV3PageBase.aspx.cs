﻿using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.WebSite.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class AEGReportsV3PageBase : System.Web.UI.Page
    {
        #region Class Data
        protected readonly AuditReportPresenter presenter = new AuditReportPresenter();
        protected readonly ConsumerRequestsPresenter crPresenter = new ConsumerRequestsPresenter();
        protected bool _isFirstLoad = false;
        protected DateTime MonthShipped;
        protected string CatalogID;
        protected string AnalysisDate;
        protected string ReceiptDate;
        protected string ShipDate;
        protected int iQuantity;
        protected float Kwh_Savings = 0;
        protected float Kw_savings = 0;
        protected DataTable EquipmentAttributes;
        protected bool _reportRun = false;
        protected Label errorLabel = null;
        #endregion

        protected void ExtractPhone(GridDataItem gridItem, string phoneText)
        {
            try
            {
                if (gridItem[phoneText].Text.Replace("&nbsp;", string.Empty).Trim() != string.Empty)
                {
                    Double myDouble = Convert.ToDouble(gridItem[phoneText].Text);
                    gridItem[phoneText].Text = myDouble.ToString("###-###-####");
                }
            }
            catch
            {
            }
        }

        protected bool HasInvalidSavings(DataTable dataTable)
        {
            return dataTable.Select().Any(row => !row["KwImpact"].IsValid() || !row["KwhImpact"].IsValid());
        }

        protected bool HasInvalidCatalogID(DataTable dataTable)
        {
            return dataTable.Select().Any(row => !row["CatalogID"].IsValid());
        }

        protected bool HasInvalidKit(DataTable dataTable)
        {
            var invalid = dataTable.Select().Any(row => !row["ItemName"].IsValid());

            if (invalid)
            {
                var badKits = dataTable
                                .Select()
                                .Where(row => !row["ItemName"].IsValid())
                                .Select(row => row["KitTypeName"].ToString())
                                .Distinct();

                AddError(string.Format("Error: report could not be completed because kits {0} do not have any items.", string.Join(",", badKits.ToArray())));
            }
            return invalid;
        }

        protected void RadGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item is GridDataItem) && !e.Item.IsInEditMode)
            {
                var gridItem = (GridDataItem)e.Item;
                ExtractPhone(gridItem, "Phone1");
                ExtractPhone(gridItem, "Phone2");
            }
        }

        protected void RadGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                var spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                var addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected string[] ParseName(string FullName)
        {
            string[] FirstLast;
            string[] Temp = FullName.Trim().Split(' ');
            int arrLen = Temp.Length;
            FirstLast = new string[arrLen];
            FirstLast[1] = Temp[arrLen - 1]; //Last Name
            string FirstName = string.Empty;
            for (int i = 0; i < arrLen - 1; i++)
            {
                FirstName = FirstName + " " + Temp[i];
            }
            FirstName = FirstName.Trim(); //PGF25
            FirstLast[0] = FirstName; //First Name           
            return FirstLast;
        }

        protected DateTime GetFirstDayOfMonth(DateTime dtDate)
        {
            // set return value to the first day of the month
            // for any date passed in to the method

            // create a datetime variable set to the passed in date
            DateTime dtFrom = dtDate;

            // remove all of the days in the month
            // except the first day and set the
            // variable to hold that date
            dtFrom = dtFrom.AddDays(-(dtFrom.Day - 1));

            // return the first day of the month
            return dtFrom;
        }

        protected void GenerateXMLClick(Page page, Guid projectId, Label label)
        {
            ProjectPresenter objProjPresenter = new ProjectPresenter();
            if (projectId == Guid.Empty)
            {
                label.Text = "Please select a single project.";
                ScriptManager.RegisterStartupScript(page, page.GetType(), "AEGXMLReportFailed", "AEGXMLReportFailed();", true);
            }
            else
            {
                string ProgramID = objProjPresenter.GetProgramID(projectId);
                if (ProgramID == null || ProgramID == string.Empty)
                {
                    ScriptManager.RegisterStartupScript(page, page.GetType(), "AEGXMLReportFailed", "AEGXMLReportFailed();", true);

                }
                else
                {
                    NodeGenerator();
                }
            }
        }

        protected virtual void NodeGenerator()
        { 
        }

        protected void SearchClickHandler(Guid projectId, RadGrid grid, Label errorLabel, bool allAllowed)
        {
            if (allAllowed)
            {
                grid.Rebind();
                return;
            }

            if (projectId != Guid.Empty)
            {
                grid.Rebind();
            }
            else
            {
                AddError("Please select a Project");
            }
        }

        protected void ProjectChangedHandler(RadGrid grid)
        {
            //grid.Rebind();
        }

        protected void AddError(string message)
        {
            errorLabel.Text = message;
        }

        protected void GetAllKits(RadComboBox comboBox)
        {
            List<KitType> list = presenter.GetAllKitTypes();
            var items = new List<ListItem>();
            items.Add(new ListItem("All", ""));
            foreach (KitType kit in list)
            {
                items.Add(new ListItem(kit.KitName, kit.Id.ToString()));
            }
            comboBox.DataSource = items;
            comboBox.DataBind();
        }

        protected void SetTitleAndHelpLink(string title, string message)
        {
            Default MasterPage = (Default)Page.Master;
            MasterPage.Help = Common.MakeHelpLink(this, title, message);
            _isFirstLoad = true;
        }

        protected void NeedDataSource(GridNeedDataSourceEventArgs e, RadGrid grid)
        {
            if (!(e.RebindReason == GridRebindReason.InitialLoad) && !_isFirstLoad && !_reportRun)
            {
                DataSet ds = GetFilteredList();

                if (!ds.Tables[0].Select().Any())
                {
                    AddError("There are no exceptions for the exception time frame indicated.");
                    grid.DataSource = "";
                    return;
                }
                
                // check if the kit has items
                if (HasInvalidKit(ds.Tables[0]))
                {
                    grid.DataSource = "";
                    return;
                }

                var projectList = new Dictionary<string, string>();
                var umPresenter = new UserManagementPresenter();

                var hasInvalidCatalogID = HasInvalidCatalogID(ds.Tables[0]);
                if (hasInvalidCatalogID)
                {
                    var invalidItems = ds.Tables[0]
                            .Select()
                            .Where(row => !row["CatalogID"].IsValid())
                            .Select(row => new { ItemName = row["ItemName"].ToString(), ProjectID = row["ProjectId"].ToString() })
                            .Distinct();

                    var error = new StringBuilder("Error:");
                    error.Append("<br/>");
                    invalidItems.ToList().ForEach(item =>
                    {
                        if (!projectList.ContainsKey(item.ProjectID.ToString()))
                        {
                            projectList[item.ProjectID.ToString()] = umPresenter.GetProjectById(new Guid(item.ProjectID)).ProjectName;
                        }
                        error.AppendLine(string.Format("{0} does not have a Catalog ID for the {1} project <br />", item.ItemName, projectList[item.ProjectID]));
                    });

                    AddError(error.ToString());
                    grid.DataSource = "";
                    return;
                }

                var hasInvalidSavings = HasInvalidSavings(ds.Tables[0]);
                if (hasInvalidSavings)
                {
                    var invalidItemSavings = ds.Tables[0]
                            .Select()
                            .Where(row => !row["KwImpact"].IsValid() || !row["KwhImpact"].IsValid())
                            .Select(row => new
                            {
                                ItemName = row["ItemName"].ToString(),
                                ProjectID = row["ProjectId"].ToString(),
                                OperatingCompany = row["OperatingCompany"].ToString()
                            })
                            .Distinct();

                    var error = new StringBuilder("Error:");
                    error.Append("<br/>");
                    invalidItemSavings.ToList().ForEach(item =>
                    {
                        if (!projectList.ContainsKey(item.ProjectID.ToString()))
                        {
                            projectList[item.ProjectID.ToString()] = umPresenter.GetProjectById(new Guid(item.ProjectID)).ProjectName;
                        }
                        error.AppendLine(string.Format("{0} is not set up for {1} - {2} <br />", item.ItemName, projectList[item.ProjectID], item.OperatingCompany));
                    });

                    AddError(error.ToString());
                    grid.DataSource = "";
                    return;
                }

                grid.DataSource = ds.Tables[0];
                _reportRun = true;
            }
        }

        protected virtual DataSet GetFilteredList()
        {
            return null;
        }

        protected void EquipmentAttributesDeclarations(DataRow dr)
        {
            DataSet dsEA = crPresenter.GetEquipmentAttributesV3(new Guid(dr["Id"].ToString()));
            EquipmentAttributes = dsEA.Tables[0];
        }

        protected XElement ImportContactNode(DataRow dr, string contactRefIDPortion, string FirstName, string LastName)
        {
            var xel = new XElement("importContact",
                                              (contactRefIDPortion.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.contactRefID, contactRefIDPortion) : null),
                                              new XElement(XmlFormatter.contactType, "Premise"),
                                              (FirstName.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.firstName, FirstName.Trim()) : null),
                                              (LastName.CheckDbNullOrEmpty() ? new XElement(XmlFormatter.lastName, LastName) : null),
                                              (dr["CompanyName"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.company, Convert.ToString(dr["CompanyName"])) : null),
                                              (dr["ServiceAddress1"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.address, Convert.ToString(dr["ServiceAddress1"])) : null),
                                              (dr["ServiceAddress2"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.addressCont, Convert.ToString(dr["ServiceAddress2"])) : null),
                                              (dr["ServiceCity"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.city, Convert.ToString(dr["ServiceCity"])) : null),
                                              (dr["ServiceState"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.state, Convert.ToString(dr["ServiceState"])) : null),
                                              (dr["ServiceZip"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.zip, Convert.ToString(dr["ServiceZip"])) : null),
                                              (dr["AccountNumber"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.accountNumber, Convert.ToString(dr["AccountNumber"])) : null),
                                              (dr["Phone1"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.phone, Convert.ToString(dr["Phone1"])) : null),
                                              (dr["Phone2"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.cell, Convert.ToString(dr["Phone2"])) : null),
                                              (dr["Email"].CheckDbNullOrEmpty() ? new XElement(XmlFormatter.email, Convert.ToString(dr["Email"])) : null));
            return xel;
        }

        protected XElement CreateXElement(string xType, string formatter, string field, DataRow row)
        {
            return row[field].CheckDbNullOrEmpty() ? new XElement(xType,
                               new XElement("field", formatter),
                               new XElement("value", row[field])) : null;
        }
    }
}