﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class ShipmentDetailReport : System.Web.UI.Page
    {
        ShipmentDetailReportPresenter presenter = new ShipmentDetailReportPresenter();
        private bool _isFirstLoad = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList1.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }

            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;

            if (!IsPostBack)
            {
                List<EnumListItem> list = EnumUtils.EnumToListWithNull(typeof(ShipmentShippingStatus));
                list.RemoveRange(0,3);

                RadComboBox_Status.DataSource = list;
                RadComboBox_Status.DataBind();
            }

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Shipment Detail Report");
                _isFirstLoad = true;
            }
        }

        void ProjectsList1_OnProjectChanged(string projectId)
        {
            RadGrid_Shipments_NeedDataSource(null, null);
            RadGrid_Shipments.Rebind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (RadComboBox_Status.SelectedValue == ((int)ShipmentShippingStatus.Returned).ToString())
            {
                RadGrid_Shipments.MasterTableView.Columns.FindByDataField("IsReship").Visible = false;
                RadGrid_Shipments.MasterTableView.Columns.FindByDataField("ShippingStatusDate").Visible = true;
                RadGrid_Shipments.MasterTableView.Columns.FindByDataField("FedExStatusCode").Visible = true;
            }
            else
            {
                RadGrid_Shipments.MasterTableView.Columns.FindByDataField("IsReship").Visible = true;
                RadGrid_Shipments.MasterTableView.Columns.FindByDataField("ShippingStatusDate").Visible = false;
                RadGrid_Shipments.MasterTableView.Columns.FindByDataField("FedExStatusCode").Visible = false;
            }
            RadGrid_Shipments_NeedDataSource(null, null);
            RadGrid_Shipments.Rebind();
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            dpStartDateFilter.SelectedDate = null;
            dpEndDateFilter.SelectedDate = null;
            RadComboBox_Status.SelectedValue = ((int)ShipmentShippingStatus.Delivered).ToString();
        }

        protected void RadGrid_Shipments_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                var spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                var addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGrid_Shipments_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (!_isFirstLoad)
                RadGrid_Shipments.DataSource = GetFilteredList();
        }

        protected void RadGrid_Shipments_ItemDataBound(object sender, GridItemEventArgs e)
        {
            
        }

        private List<ShipmentDetail> GetFilteredList()
        {
            DateTime? startDate = dpStartDateFilter.SelectedDate;
            DateTime? endDate = dpEndDateFilter.SelectedDate;

            if (endDate != null)
                endDate = endDate.Value.AddDays(1);

            int? status = null;
            string s = RadComboBox_Status.SelectedValue;
            if (!string.IsNullOrEmpty(s))
                status = Convert.ToInt32(RadComboBox_Status.SelectedValue);

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList1.ProjectId;
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = ProjectsList1.UserId;
            }

            if (ProjectsList1.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList1.CompanyId;
            }

            return presenter.GetAll(startDate, endDate, status, userId, projectId, companyId);
        }
    }
}
