﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class ShipmentPreview : Page
    {
        ShipmentDetailsPresenter presenter = new ShipmentDetailsPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Label Request Preview");
            }
        }

        protected void RadGrid_ShipmentDetails_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                var spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                var addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGrid_ShipmentDetails_ItemDataBound(object sender, GridItemEventArgs e)
        {
        }

        protected void RadGrid_ShipmentDetails_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_ShipmentDetails.DataSource = GetFilteredList();
        }

        private List<FedExLabel> GetFilteredList()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Id"]))
                return presenter.GetByShipmentId(new Guid(Request.QueryString["Id"]));
           
            return new List<FedExLabel>();
        }
    }
}