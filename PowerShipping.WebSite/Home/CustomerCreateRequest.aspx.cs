﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using PowerShipping.ImportExport;
using Telerik.Web.UI;
using PowerShipping.Logging;

namespace PowerShipping.WebSite.Home
{
    public partial class CustomerCreateRequest : System.Web.UI.Page
    {
        private readonly CustomerCreateRequestPresenter presenter = new CustomerCreateRequestPresenter();

        protected void Page_Init(object sender, EventArgs e)
        {
            CustomerCreateRequestFilter1.OnFilter += CustomerCreateRequestFilter1_OnFilter;
            CustomerCreateRequestFilter1.OnClear += CustomerCreateRequestFilter1_OnClear;

            Session["NewLabelRequestIds"] = null;
            Session["NewLabelRequestProjectId"] = null;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.IsInRole(PDMRoles.ROLE_Administrator))
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Create Consumer Request File");
            }
        }

        void CustomerCreateRequestFilter1_OnFilter(string qry)
        {
            HiddenFiled_CustomQuery.Value = qry;
            RadGrid_History_NeedDataSource(null, null);
            RadGrid_History.Rebind();
        }

        void CustomerCreateRequestFilter1_OnClear(string qry)
        {
            HiddenFiled_CustomQuery.Value = qry;
        }

        protected void RadGrid_History_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                var spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                var addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGrid_History_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            string query = "";
            if (!string.IsNullOrEmpty(HiddenFiled_CustomQuery.Value))
                query = HiddenFiled_CustomQuery.Value;
            else
                query = CustomerCreateRequestFilter1.DefaultQuery;

            RadGrid_History.DataSource = presenter.GetFiltered(query);
        }

        protected void RadGrid_History_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if ((e.Item is GridDataItem) && !e.Item.IsInEditMode)
            {

            }
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            labelErrors.Text = string.Empty;
            try
            {
                foreach (string fileInputID in Request.Files)
                {
                    UploadedFile file = UploadedFile.FromHttpPostedFile(Request.Files[fileInputID]);
                    if (file.ContentLength > 0)
                    {
                        if (!Directory.Exists(Server.MapPath(ConfigurationManager.AppSettings["CustomerRequestFolder"])))
                            Directory.CreateDirectory(Server.MapPath(ConfigurationManager.AppSettings["CustomerRequestFolder"]));

                        var path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["CustomerRequestFolder"]), file.GetName());
                        file.SaveAs(path);

                        CurrentLogger.Log.Info("File saved at this path:" + path);

                        if (ProjectsList1.ProjectId == Guid.Empty)
                        {
                            labelErrors.ForeColor = Color.Red;
                            labelErrors.Text = "Please select project.";
                            return;
                        }

                        MembershipUser user = Membership.GetUser(User.Identity.Name);
                        string userId = user.ProviderUserKey.ToString();

                        var creator = new CreatorLabelRequestFile(path, Server.MapPath(ConfigurationManager.AppSettings["CustomerRequestFolder"]), userId, ProjectsList1.ProjectId, tbSource.Text);
                        creator.ImportProgress += ShowImportProgress;
                        CurrentLogger.Log.Info("Before - creator.Create();");
                        CustomerCreateRequestHistory history = creator.Create();
                        CurrentLogger.Log.Info("After - creator.Create();");

                        if (!creator.ImportErrors.IsEmpty)
                        {
                            labelErrors.ForeColor = Color.Red;
                            creator.ImportErrors.ForEach(err => labelErrors.Text += err + "<br>");
                        }
                        else
                        {
                            hlExceptions.Visible = false;

                            labelErrors.ForeColor = Color.Black;
                            labelErrors.Text = "Total requests: " + history.TotalRecords + ". <br/>Uploaded Successfully: " + history.SuccessfullyUploaded +
                                               ". <br/>Uploaded with exceptions: " + history.UploadedWithExceptions + ". ";

                            RadGrid_History.Rebind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CurrentLogger.Log.Error(ex);
                Log.LogError(ex);

                CurrentLogger.Log.Error(ex);
            }
        }

        private void ShowImportProgress(object sender, ImportExportProgressEventArgs e)
        {
            if (Response.IsClientConnected)
            {
                var progress = RadProgressContext.Current;
                progress.SecondaryTotal = e.Total;
                progress.SecondaryValue = e.Value;
                progress.SecondaryPercent = e.Total == 0 ? 0 : Convert.ToInt32((100 * Convert.ToDouble(e.Value)) / Convert.ToDouble(e.Total));
                progress.CurrentOperationText = e.Message;
            }
        }
    }
}
