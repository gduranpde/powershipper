<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Shared/Default.Master"
    CodeBehind="HEAFileConversionUploadPrompt.aspx.cs" Inherits="PowerShipping.WebSite.Home.HEAFileConversionUploadPrompt"
    Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_ClientLogo" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="../App_Themes/Default/Main.css" rel="stylesheet" type="text/css" />

    <script src="../js/main.js" type="text/javascript"></script>

    <div class="pageDiv">
        <div style="border: solid 1px grey; margin: 11px 157px 10px; padding-left: 20px;
            padding-right: 20px; width: 500px;">
            <table>
                <tr style="height: 10px">
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblNoOfRecords" runat="server" Text="# of Records in Source File: "
                            Font-Bold="True" Style="margin-left: 8px;"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LblTotalRecords" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel1" runat="server" Visible="true" Style="border-color: grey; border-style: solid;
                border-width: 1px; width: 380px;">
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="LblSelectOperatingCompany" runat="server" Text="SelectOperatingCompany:"
                                Font-Underline="True" Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBoxList ID="ChkBoxListOperatingCompany" runat="server" onClick="readListControl()">
                            </asp:CheckBoxList>
                            <br />
                            <asp:Label ID="Label1" runat="server" Text="Total : 0" CssClass="labelTotal"></asp:Label>
                        </td>
                    </tr>
                    <tr style="height: 10px">
                        <td>
                            <asp:Label ID="LblBlankRowsCount" CssClass="labelTotal" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr style="height: 10px">
                    </tr>
                </table>
            </asp:Panel>
            <table>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="BtnConvertFile" runat="server" Text="Convert File" OnClientClick="IsSelected()"
                            OnClick="BtnConvertFile_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblMessage" runat="server" ForeColor="#FF3300"></asp:Label>
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>