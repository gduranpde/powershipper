﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Home
{
    public partial class AccountsShippedReport : System.Web.UI.Page
    {
        AccountsShippedReportPresenter presenter = new AccountsShippedReportPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ProjectsList1.UserId != Guid.Empty)
            {
                Response.Redirect("~/Error.aspx?Error=" + (int)Errors.HaventRights);
            }
            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;

            if (!Page.IsPostBack)
            {
                Shared.Default MasterPage = (Shared.Default)Page.Master;
                MasterPage.Help = Common.MakeHelpLink(this, "Click here for help", "Accounts Shipped Report");
            }
        }

        void ProjectsList1_OnProjectChanged(string projectId)
        {
            RadGrid_Grid.Rebind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            RadGrid_Grid.Rebind();
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            dpStartDateFilter.SelectedDate = null;
            dpEndDateFilter.SelectedDate = null;
        }

        protected void RadGrid_Grid_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                var spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                spriteAddButton.Visible = false;

                var addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                addButton.Visible = false;
            }
        }

        protected void RadGrid_Grid_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid_Grid.DataSource = GetFilteredListAccounts();
        }

        protected void RadGrid_Grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            
        }

        private DataSet GetFilteredListAccounts()
        {
            DateTime? startDate = dpStartDateFilter.SelectedDate;
            DateTime? endDate = dpEndDateFilter.SelectedDate;

            if (endDate != null)
                endDate = endDate.Value.AddDays(1);

            Guid? userId = null;
            Guid? projectId = null;
            Guid? companyId = null;

            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList1.ProjectId;
            }

            if (ProjectsList1.UserId != Guid.Empty)
            {
                userId = ProjectsList1.UserId;
            }

            if (ProjectsList1.CompanyId != Guid.Empty)
            {
                companyId = ProjectsList1.CompanyId;
            }


            return presenter.GetAll(startDate, endDate, userId, projectId, companyId);
        }
    }
}
