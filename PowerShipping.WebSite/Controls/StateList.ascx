﻿
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StateList.ascx.cs" Inherits="PowerShipping.WebSite.Controls.StateList" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<table>
    <tr>
        <td>
            Select State:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBox_States" runat="server" 
                DataValueField="Value" DataTextField="Text"  
                Width="400px">
            </telerik:RadComboBox>
        </td>
        <td>
            
        </td>
    </tr>
</table>