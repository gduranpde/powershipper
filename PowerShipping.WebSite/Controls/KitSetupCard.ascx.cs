﻿using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class KitSetupCard : System.Web.UI.UserControl
    {


        private readonly UserManagementPresenter presenter = new UserManagementPresenter();

        public Guid KitTypeId
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_Id.Value))
                           ? new Guid(HiddenField_Id.Value)
                           : Guid.Empty;
            }
            set { HiddenField_Id.Value = value.ToString(); }
        }

        public object DataItem { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnInit(EventArgs e)
        {
            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');");

            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
            DataBinding += Init_DataBinding;
        }

        private void Init_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        public void InitData()
        {
            InitDropDowns();
        }

        private void InitDropDowns()
        {
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (DataItem == null)
                return;

            if ((DataItem != null) & (DataItem.GetType() != typeof(GridInsertionObject)))
            {
                var entity = (KitType)DataItem;
                KitTypeId = entity.Id;

                RadListBox_Items.DataSource = presenter.GetItemsByKitType(entity.Id);
                RadListBox_Items.DataBind();

                RadTextBox_KitName.Text = entity.KitName;
                RadNumericTextBox_Height.Value = entity.Height;
                RadNumericTextBox_Width.Value = entity.Width;
                RadNumericTextBox_Length.Value = entity.Length;
                RadNumericTextBox_Weight.Value = entity.Weight;
                chkEditIsActive.Checked = entity.IsActive;


                var kitContentsList = new KitTypesPresenter().GetFromKitContentsByKitType(KitTypeId);
                entity.Items = presenter.GetItemsByKitType(entity.Id);
                string kitContents = string.Empty;

                foreach (Item ki in entity.Items)
                {
                    RadListBoxItem item = RadListBox_Items.FindItemByValue(ki.ItemID.ToString());
                    if (item != null)
                        item.Checked = true;
                }
                if (kitContentsList.Count == 0)
                {
                    entity.KitContents = kitContents;
                }
                else
                {
                    foreach (var item in kitContentsList)
                    {
                        kitContents = kitContents + item.Quantity + "-" + item.ItemName + ", ";
                    }

                    if (!string.IsNullOrEmpty(kitContents))
                    {
                        entity.KitContents = kitContents.Remove(kitContents.Length - 2);
                        txtKitContents.Text = entity.KitContents;
                    }
                }
            }
            else
                if (DataItem.GetType() == typeof(GridInsertionObject))
                {
                    lnkBtnItems.Visible = false;
                }

            base.OnPreRender(e);
        }


        public KitType FillEntity()
        {
            var entity = new KitType();
            if (KitTypeId != Guid.Empty)
            {
                entity.Id = KitTypeId;
            }
            else
            {
                entity.Id = Guid.NewGuid();
            }
            entity.KitName = RadTextBox_KitName.Text;
            entity.Height = Convert.ToDouble(RadNumericTextBox_Height.Value);
            entity.Width = Convert.ToDouble(RadNumericTextBox_Width.Value);
            entity.Length = Convert.ToDouble(RadNumericTextBox_Length.Value);
            entity.Weight = Convert.ToDouble(RadNumericTextBox_Weight.Value);
            entity.KitContents = txtKitContents.Text;
            entity.IsActive = chkEditIsActive.Checked;
            entity.Items = presenter.GetItemsByKitType(entity.Id);

            return entity;
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                presenter.DeleteKitTypeNew(KitTypeId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to delete Kit. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;

                Helper.ErrorInGrid("RadGrid_KitSetup", Page, l);
            }
        }

        protected void lnkBtnItems_OnClick(object sender, EventArgs e)
        {
            Guid id = KitTypeId;
            Response.Redirect("KitContents.aspx?KitTypeId=" + KitTypeId + "&KitName=" + Server.UrlEncode(RadTextBox_KitName.Text));
        }

    }
}