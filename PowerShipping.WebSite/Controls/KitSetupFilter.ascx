﻿<%@ Control Language="C#" AutoEventWireup="true" 
    CodeBehind="KitSetupFilter.ascx.cs" 
    Inherits="PowerShipping.WebSite.Controls.KitSetupFilter" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div class="jobInfo" runat="server" id="div_kitSetupInfo">
    <div style="text-align: center;">
        <b>Kit Type Filter</b>
    </div>
    <table>
        <tr>
            <td colspan="2" align="center"></td>
        </tr>
        <tr class="kitSetupInfo_td">
            <td>IsActive</td>
            <td class="kitSetupInfo_info">
                <asp:CheckBox ID="chkIsActive" runat="server" Checked="true"></asp:CheckBox>
            </td>
        </tr>
        <tr class="kitSetupInfo_td">
            <td>Kit Name</td>
            <td>
                <telerik:RadTextBox ID="txtKitName" runat="server">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button runat="server" ID="btnSearchBottom" Text="Search" Width="80px" CssClass="ptButton"
                    OnClick="Button_Filter_Click" />
                <asp:Button runat="server" ID="btnClearBottom" Text="Clear" Width="80px" CssClass="ptButton"
                    OnClick="Button_Clear_Click" />
            </td>
        </tr>
    </table>
</div>
