﻿using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Web.Script.Serialization;

namespace PowerShipping.WebSite.Controls
{
    public partial class ProjectCard : System.Web.UI.UserControl
    {
        #region Class Data
        private readonly UserManagementPresenter presenter = new UserManagementPresenter();
        #endregion

        #region Properties
        public Guid ProjectId
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_Id.Value))
                           ? new Guid(HiddenField_Id.Value)
                           : Guid.Empty;
            }
            set { HiddenField_Id.Value = value.ToString(); }
        }

        public object DataItem { get; set; }
        #endregion

        #region Event Handlers
        private void SetSelectedStatesVisibility()
        {
            if (selectedStates.Items.Count > 0)
            {
                tr_SelectedStates.Visible = true;
                selectedStates.Visible = true;
                Button_RemoveState.Visible = true;
            }
            else
            {
                tr_SelectedStates.Visible = false;
                selectedStates.Visible = false;
                Button_RemoveState.Visible = false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnInit(EventArgs e)
        {
            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');");

            InitializeComponent();
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (DataItem == null)
                return;

            if ((DataItem != null) & (DataItem.GetType() != typeof(GridInsertionObject)))
            {
                var entity = (Project)DataItem;

                RadTextBox_ProjectName.Text = entity.ProjectName;
                RadTextBox_ProjectCode.Text = entity.ProjectCode;
                RadComboBox_Companies.SelectedValue = entity.CompanyId.ToString();

                CheckBox_IsActive.Checked = entity.IsActive;
                CheckBox_IsAllowDuplicates.Checked = entity.IsAllowDuplicates;
                CheckBox_DuplicateExceptions.Checked = entity.DuplicateExceptions;
                CheckBox_IsAllowDupsInProject.Checked = entity.IsAllowDupsInProject;
                CheckBox_ReturnsAllowed.Checked = entity.ReturnsAllowed;
                entity.States = presenter.GetStatesByProject(entity.Id);

                var states = presenter.GetAllStates().Select(s => new { Id = s.Id, Name = s.Name }).ToList();
                cmbAvailableStates.DataSource = states;
                cmbAvailableStates.DataBind();

                foreach (State s in entity.States)
                {
                    selectedStates.Items.Add(new RadListBoxItem { Value = s.Id.ToString(), Text = s.Name });                    
                }

                RadListBox_Projects.DataSource = presenter.GetAllSelectedProjects(entity.Id);
                RadListBox_Projects.DataBind();

                var projects = presenter.GetUnselectedProjects(entity.Id);
                RadComboBox_Projects.DataSource = projects.ToList().Select(p => new { Id = p.Id, ProjectName = p.ProjectCode + " - " + p.ProjectName });
                RadComboBox_Projects.DataBind();
                
                if (entity.StateId != 0)
                {
                    cmbStates.SelectedValue = entity.StateId.ToString();
                }

                cmbStates.DataSource = states;
                cmbStates.DataBind();
                
                if (entity.IsAllowDuplicates == false)
                {
                    if (RadListBox_Projects.Items.Count > 0)
                    {
                        tr_RemoveProject.Visible = true;
                        RadListBox_Projects.Visible = true;
                        Button_RemoveProject.Enabled = true;
                        Button_RemoveProject.Visible = true;
                        CheckBox_DuplicateExceptions.Checked = true;
                        tr_Projects.Visible = true;
                    }
                    else
                    {
                        tr_RemoveProject.Visible = false;
                        RadListBox_Projects.Visible = false;
                        Button_RemoveProject.Enabled = false;
                        Button_RemoveProject.Visible = false;
                        CheckBox_DuplicateExceptions.Checked = false;
                        tr_Projects.Visible = false;
                        tr_ComboBoxProjects.Visible = false;
                        RadComboBox_Projects.Visible = false;
                        Button_AddProject.Enabled = false;
                        Button_AddProject.Visible = false;
                        tr_AddProject.Visible = false;

                    }                                   
                }

                SetComboProjectsVisibility();
                SetSelectedStatesVisibility();     

                RadTextBox_ProgramID.Text = entity.ProgramID;
                ProjectId = entity.Id;
            }

            base.OnPreRender(e);
        }        

        protected void chb_StatesAll_CheckedChanged(object sender, EventArgs e)
        {
            if (chb_StatesAll.Checked)
            {
                foreach (RadComboBoxItem item in cmbAvailableStates.Items)
                {
                    if (!selectedStates.Items.Any(it => it.Value == item.Value))
                    {
                        selectedStates.Items.Add(new RadListBoxItem { Value = item.Value, Text = item.Text });
                    }
                }                
            }
            else
            {
                selectedStates.Items.Clear();               
            }
            SetSelectedStatesVisibility();
        }

        protected void chb_IsAllowDuplicates_CheckedChanged(object sender, EventArgs e)
        {
            if (!CheckBox_IsAllowDuplicates.Checked)
            {
                SetListProjectsVisibility();
                SetComboProjectsVisibility();                
            }
            else
            {
                tr_RemoveProject.Visible = false;
                RadListBox_Projects.Visible = false;
                Button_RemoveProject.Enabled = false;
                Button_RemoveProject.Visible = false;
                CheckBox_DuplicateExceptions.Checked = false;
                tr_Projects.Visible = false;
                tr_ComboBoxProjects.Visible = false;
                RadComboBox_Projects.Visible = false;
                Button_AddProject.Enabled = false;
                Button_AddProject.Visible = false;
                tr_AddProject.Visible = false;

                List<Project> temp = (List<Project>)Session["addedProjectDuplicateExList"];
                if (temp != null)
                {
                    foreach (Project proj in temp)
                    {
                        RadComboBoxItem aux = new RadComboBoxItem();
                        aux.Value = proj.Id.ToString();
                        aux.Text = proj.ProjectName;
                        RadComboBox_Projects.Items.Add(aux);

                        RadListBoxItem listBoxItem = new RadListBoxItem();
                        listBoxItem.Value = proj.Id.ToString();
                        listBoxItem.Text = proj.ProjectName;

                        RadListBoxItem itemToRemove = RadListBox_Projects.FindItemByText(aux.Text);
                        RadListBox_Projects.Items.Remove(itemToRemove);
                    }

                    temp.Clear();
                    Session["addedProjectDuplicateExList"] = temp;
                }

                temp = (List<Project>)Session["removedProjectDuplicateExList"];
                temp.Clear();
                Session["removedProjectDuplicateExList"] = temp;
            }
        }
        
        /// <summary>
        /// Deletes entire project
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                Project p = presenter.GetProjectById(ProjectId);
                presenter.DeleteProject(p);
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to delete project. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                //var grid =
                //    (Page.Controls[0].Controls[3].Controls[13].FindControl("RadGrid_Projects") as RadGrid);
                //grid.Controls.Add(l);
                Helper.ErrorInGrid("RadGrid_Projects", Page, l);
            }
        }

        /// <summary>
        /// Removes slected projects 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemoveProject_OnClick(object sender, EventArgs e)
        {
            RemoveExceptionProjects();
        }

        /// <summary>
        /// Removes slected states 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemoveState_OnClick(object sender, EventArgs e)
        {
            RemoveSelectedState();
        }

        /// <summary>
        /// Adds slected states
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddState_OnClick(object sender, EventArgs e)
        {
            AddSelectedStates();
        }

        /// <summary>
        /// Cancel operations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            Session["removedProjectDuplicateExList"] = new List<Project>();
            Session["addedProjectDuplicateExList"] = new List<Project>();
        }

        /// <summary>
        /// Adds selected projects
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddProject_OnClick(object sender, EventArgs e)
        {
            AddExceptionProjects();
        }
        #endregion

        #region Methods
        public void InitData()
        {
            InitDropDowns();
        }

        public Project FillEntity()
        {
            Project entity = new Project();
            if (ProjectId != Guid.Empty)
            {
                entity.Id = ProjectId;
            }
            else
            {
                entity.Id = Guid.NewGuid();
            }

            entity.ProjectName = RadTextBox_ProjectName.Text;
            entity.ProjectCode = RadTextBox_ProjectCode.Text;
            entity.CompanyId = new Guid(RadComboBox_Companies.SelectedValue);
            entity.IsActive = CheckBox_IsActive.Checked;
            entity.IsAllowDuplicates = CheckBox_IsAllowDuplicates.Checked;
            entity.DuplicateExceptions = CheckBox_DuplicateExceptions.Checked;

            entity.IsAllowDupsInProject = CheckBox_IsAllowDupsInProject.Checked;
            entity.ReturnsAllowed = CheckBox_ReturnsAllowed.Checked;
            entity.States = new List<State>();
            foreach (RadListBoxItem item in selectedStates.Items)
            {
                entity.States.Add(presenter.GetStateById(Convert.ToInt32(item.Value)));
            }
            entity.ProgramID = RadTextBox_ProgramID.Text;
            entity.StateId = int.Parse(cmbStates.SelectedValue);
            entity.AddedExceptionProjects = (List<Project>)Session["addedProjectDuplicateExList"];
            entity.RemovedExceptionProjects = (List<Project>)Session["removedProjectDuplicateExList"];

            return entity;
        }
        #endregion

        #region Helpers
        private void SetComboProjectsVisibility()
        {
            if (RadComboBox_Projects.Items.Count > 0)
            {
                tr_ComboBoxProjects.Visible = true;
                RadComboBox_Projects.Visible = true;
                Button_AddProject.Enabled = true;
                Button_AddProject.Visible = true;
                tr_AddProject.Visible = true;
            }
            else
            {
                tr_ComboBoxProjects.Visible = false;
                RadComboBox_Projects.Visible = false;
                Button_AddProject.Enabled = false;
                Button_AddProject.Visible = false;
                tr_AddProject.Visible = false;
            }
        }

        private void SetListProjectsVisibility()
        {
            if (RadListBox_Projects.Items.Count > 0)
            {
                tr_RemoveProject.Visible = true;
                RadListBox_Projects.Visible = true;
                Button_RemoveProject.Enabled = true;
                Button_RemoveProject.Visible = true;
                CheckBox_DuplicateExceptions.Checked = true;
                tr_Projects.Visible = true;
            }
            else
            {
                tr_RemoveProject.Visible = false;
                RadListBox_Projects.Visible = false;
                Button_RemoveProject.Enabled = false;
                Button_RemoveProject.Visible = false;
                CheckBox_DuplicateExceptions.Checked = false;
                tr_Projects.Visible = false;
            }
        }

        private void AddSelectedStates()
        {
            if (!selectedStates.Items.Any(it => it.Value == cmbAvailableStates.SelectedItem.Value))
            {
                selectedStates.Items.Add(new RadListBoxItem { Value = cmbAvailableStates.SelectedValue, Text = cmbAvailableStates.SelectedItem.Text });
            }
            SetSelectedStatesVisibility();
        }

        private void RemoveSelectedState()
        {
            foreach (var item in selectedStates.SelectedItems)
            {
                selectedStates.Items.Remove(item);
            }
            SetSelectedStatesVisibility();
        }

        /// <summary>
        /// Remove projects from Duplicate Exceptions list
        /// </summary>
        private void RemoveExceptionProjects()
        {
            try
            {
                //add project to removelist
                List<Project> removedProjectsList = new List<Project>();
                foreach (var selectedItem in RadListBox_Projects.SelectedItems)
                {
                    //create new project
                    var currentProject = new Project { Id = new Guid(selectedItem.Value), ProjectName = selectedItem.Text };
                    removedProjectsList.Add(currentProject);

                    // put it back to available exceptions projects
                    RadComboBoxItem aux = new RadComboBoxItem();
                    aux.Value = RadListBox_Projects.SelectedItem.Value;
                    aux.Text = currentProject.ProjectName;
                    RadComboBox_Projects.Items.Add(aux);
                }

                //remove selected projects from listbox                
                foreach (var selectedItem in RadListBox_Projects.SelectedItems)
                {
                    RadListBox_Projects.Items.Remove(selectedItem);
                }
                Session["removedProjectDuplicateExList"] = removedProjectsList;

                //if removelist empty - do not display listbox & disable removeButton
                if (RadListBox_Projects.Items.Count == 0)
                {
                    tr_RemoveProject.Visible = false;
                    RadListBox_Projects.Visible = false;
                    Button_RemoveProject.Enabled = false;
                    Button_RemoveProject.Visible = false;
                    CheckBox_DuplicateExceptions.Checked = false;
                    tr_Projects.Visible = false;
                }

                //if combobox empty - do not display combobox & disable AddButton
                if (RadComboBox_Projects.Items.Count > 0)
                {
                    tr_ComboBoxProjects.Visible = true;
                    RadComboBox_Projects.Visible = true;
                    Button_AddProject.Enabled = true;
                    Button_AddProject.Visible = true;
                    tr_AddProject.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);
            }
        }

        /// <summary>
        /// Adds selected exception projects
        /// </summary>
        private void AddExceptionProjects()
        {
            try
            {
                //selected proj from combobox
                Project project = new Project();
                project.Id = new Guid(RadComboBox_Projects.SelectedItem.Value);
                project.ProjectName = RadComboBox_Projects.SelectedItem.Text;

                //add project to listbox
                RadListBoxItem aux = new RadListBoxItem();
                aux.Value = RadComboBox_Projects.SelectedItem.Value;
                aux.Text = project.ProjectName;
                RadListBox_Projects.Items.Add(aux);

                //add project to addedProjectDuplicateExList
                List<Project> temp = (List<Project>)Session["addedProjectDuplicateExList"];
                temp.Add(project);
                Session["addedProjectDuplicateExList"] = temp;

                //remove proj from combobox
                RadComboBox_Projects.SelectedItem.Remove();

                //if tempList > 0 make the list visible
                if (RadListBox_Projects.Items.Count > 0)
                {
                    tr_RemoveProject.Visible = true;
                    RadListBox_Projects.Visible = true;
                    Button_RemoveProject.Enabled = true;
                    Button_RemoveProject.Visible = true;
                    CheckBox_DuplicateExceptions.Checked = true;
                    tr_Projects.Visible = true;
                }

                //if combobox empty - do not display combobox & disable AddButton
                if (RadComboBox_Projects.Items.Count == 0)
                {
                    tr_ComboBoxProjects.Visible = false;
                    RadComboBox_Projects.Visible = false;
                    Button_AddProject.Enabled = false;
                    Button_AddProject.Visible = false;
                    tr_AddProject.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);
            }
        }

        private void InitializeComponent()
        {
            DataBinding += Init_DataBinding;
        }

        private void Init_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        private void InitDropDowns()
        {
            RadComboBox_Companies.DataSource = presenter.GetCompanyByUser(null);
            RadComboBox_Companies.DataBind();

            cmbStates.DataSource = presenter.GetAllStates();
            cmbStates.DataBind();

            cmbAvailableStates.DataSource = presenter.GetAllStates();
            cmbStates.DataBind();
        }
        #endregion
    }
}