﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;

namespace PowerShipping.WebSite.Controls
{
    public partial class CustomerImportProjects : System.Web.UI.UserControl
    {
        public delegate void ProjectsListHandler(string projectId);
        public event ProjectsListHandler OnProjectChanged;

        private readonly UserManagementPresenter presenter = new UserManagementPresenter();

        public Guid ProjectId
        {
            get
            {
                if (presenter.GetProjectById(new Guid(RadComboBox_Projects.SelectedValue)) != null)
                    return new Guid(RadComboBox_Projects.SelectedValue);

                return Guid.Empty;
            }
        }

        public Guid UserId
        {
            get
            {
                User u = presenter.GetOne(HttpContext.Current.User.Identity.Name);

                if (u.Roles.Contains(PDMRoles.ROLE_Customer) || u.Roles.Contains(PDMRoles.ROLE_CallCenter))
                    return u.UserID;
                return Guid.Empty;
            }
        }

        public Guid CompanyId
        {
            get
            {
                if (presenter.GetCompanyById(new Guid(RadComboBox_Projects.SelectedValue)) != null)
                    return new Guid(RadComboBox_Projects.SelectedValue);

                return Guid.Empty;
            }
        }

        private bool _isCompanyUse;
        public bool IsCompanyUse
        {
            get { return _isCompanyUse; }
            set { _isCompanyUse = value; }
        }

        protected override void OnInit(EventArgs e)
        {
            InitControls();

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void RadComboBox_Projects_SelectedIndexChangedOnCustomerLookup(object sender, EventArgs e)
        {
            if (HttpContext.Current.Session["CompanyProject"] == null)
                HttpContext.Current.Session.Add("CompanyProject", RadComboBox_Projects.SelectedValue);
            else
                HttpContext.Current.Session["CompanyProject"] = RadComboBox_Projects.SelectedValue;

            Company c = presenter.GetCompanyById(new Guid(RadComboBox_Projects.SelectedValue));
            if (c == null)
            {
                Project p = presenter.GetProjectById(new Guid(RadComboBox_Projects.SelectedValue));
                if (p != null)
                    c = presenter.GetCompanyById(p.CompanyId);
            }

            HttpContext.Current.Session["ComapanyForLogo"] = c;

            if (OnProjectChanged != null)
                OnProjectChanged(RadComboBox_Projects.SelectedValue);
        }

        private void InitControls()
        {
            User u = presenter.GetOne(HttpContext.Current.User.Identity.Name);

            List<ListItem> list = new List<ListItem>();
            list.Add(new ListItem("--- All ---", Guid.Empty.ToString()));

            if (u.Roles.Contains(PDMRoles.ROLE_Customer) || u.Roles.Contains(PDMRoles.ROLE_CallCenter))
            {
                List<Project> listP = presenter.GetProjectByUser(u.UserID);

                foreach (Project project in listP)
                {
                    list.Add(new ListItem(project.FullName, project.Id.ToString()));
                }
            }
            else
            {
                if (IsCompanyUse)
                {
                    List<Company> listC = presenter.GetCompanyByUser(null);

                    foreach (Company company in listC)
                    {
                        company.Projects = presenter.GetProjectByCompany(company.Id);
                        list.Add(new ListItem(company.CompanyName, company.Id.ToString()));

                        foreach (Project p in company.Projects)
                        {
                            list.Add(new ListItem(">>>" + p.FullName, p.Id.ToString()));
                        }
                    }
                }
                else
                {
                    List<Project> listP = presenter.GetProjectByUser(null);

                    foreach (Project project in listP)
                    {
                        list.Add(new ListItem(project.FullName, project.Id.ToString()));
                    }
                }
            }

            RadComboBox_Projects.DataSource = list;
            RadComboBox_Projects.DataBind();

            if (HttpContext.Current.Session["CompanyProject"] != null)
            {
                if (RadComboBox_Projects.FindItemByValue(HttpContext.Current.Session["CompanyProject"].ToString()) != null)
                {
                    RadComboBox_Projects.SelectedValue = HttpContext.Current.Session["CompanyProject"].ToString();
                }
            }
        }
    }
}