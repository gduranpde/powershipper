﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class UserManagementCard : System.Web.UI.UserControl
    {
        private object _dataItem = null;
        private UserManagementPresenter presenter = new UserManagementPresenter();
        public Guid UserId
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_Id.Value))
                           ? new Guid(HiddenField_Id.Value)
                           : Guid.Empty;
            }
            set { HiddenField_Id.Value = value.ToString(); }
        }

        public object DataItem
        {
            get { return _dataItem; }
            set { _dataItem = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected override void OnInit(EventArgs e)
        {
            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');");

            InitializeComponent();
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            if(DataItem == null)
                return;

            if ((DataItem != null) & (DataItem.GetType() != typeof(GridInsertionObject)))
            {
                var entity = (User)DataItem;


                RadTextBox_UserName.Text = entity.UserName;
                RadTextBox_Password.Text = entity.Password;

                SelectedRoles = entity.Roles;

                if (RadComboBox_Roles.SelectedValue == PDMRoles.ROLE_Administrator || RadComboBox_Roles.SelectedValue == PDMRoles.ROLE_User)
                {
                    RadComboBox_Companies.SelectedIndex = 0;
                    RadComboBox_Companies.Enabled = false;

                    RadListBox_Projects.Enabled = false;
                    //chbl_Projects.Enabled = false;
                }
                else if (RadComboBox_Roles.SelectedValue == PDMRoles.ROLE_Customer || RadComboBox_Roles.SelectedValue == PDMRoles.ROLE_CallCenter)
                {
                    RadComboBox_Companies.Enabled = true;
                    RadListBox_Projects.Enabled = true;
                    //chbl_Projects.Enabled = true;

                    List<Company> listCompanies = presenter.GetCompanyByUser(new Guid(entity.ProviderUserKey.ToString()));
                    if (listCompanies.Count > 0)
                    {
                        entity.Company = listCompanies[0];
                        RadComboBox_Companies.SelectedValue = entity.Company.Id.ToString();
                        Companies_SelectedIndexChanged(null, null);
                    }

                    if (entity.Company != null)
                    {
                        entity.Projects = presenter.GetProjectByUser(new Guid(entity.ProviderUserKey.ToString()));
                    }
                    if (entity.Projects != null)
                    {
                        foreach (Project p in entity.Projects)
                        {
                            //foreach (RadListBoxItem item in RadListBox_Projects.Items)
                            //{
                            //    if (p.Id.ToString() == item.Value)
                            //    {
                            //        item.Checked = true;
                            //    }
                            //}

                            RadListBoxItem item = RadListBox_Projects.FindItemByValue(p.Id.ToString());
                            if (item != null)
                                item.Checked = true;



                            //ListItem li = chbl_Projects.Items.FindByValue(p.Id.ToString());
                            //if (li != null)
                            //{
                            //    li.Selected = true;
                            //}
                        }
                    }
                }

                UserId = new Guid(entity.ProviderUserKey.ToString());
            }

            base.OnPreRender(e);
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                User u = presenter.GetById(UserId);
                presenter.Remove(u);
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to delete User. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                //var grid =
                //    (Page.Controls[0].Controls[3].Controls[13].FindControl("RadGrid_Users") as RadGrid);
                //grid.Controls.Add(l);
                Helper.ErrorInGrid("RadGrid_Users", Page, l);
            }
        }

        //protected void RadListBox_Projects_DataBound(object sender, EventArgs e)
        //{
        //    List<Project> l = (sender as RadListBox).DataSource as List<Project>;
        //    foreach (RadListBoxItem item in (sender as RadListBox).SelectedItems)
        //        item.Checked = true;

        //    //RadListBoxItem item = (sender as RadListBox).FindItemByValue(p.Id.ToString());
        //    //if (item != null)
        //    //    item.Checked = true;
        //}


        private void InitializeComponent()
        {
            DataBinding += new EventHandler(Init_DataBinding);
        }

        private void Init_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        public void InitData()
        {
            InitDropDowns();


        }

        public User FillEntity()
        {
            var entity = new User();
            if (UserId != Guid.Empty)
            {
                entity.UserID = UserId;
            }

            entity.UserName = RadTextBox_UserName.Text;

            if (!string.IsNullOrEmpty(RadTextBox_Password.Text))
                entity.Password = RadTextBox_Password.Text;
            else
                entity.Password = null;

            entity.Roles = SelectedRoles;

            if (RadComboBox_Roles.SelectedValue == PDMRoles.ROLE_Customer || RadComboBox_Roles.SelectedValue == PDMRoles.ROLE_CallCenter)
            {
                if (new Guid(RadComboBox_Companies.SelectedValue) != new Guid())
                    entity.Company = presenter.GetCompanyById(new Guid(RadComboBox_Companies.SelectedValue));

                entity.Projects = new List<Project>();
                foreach (RadListBoxItem item in RadListBox_Projects.CheckedItems)
                {
                    entity.Projects.Add(presenter.GetProjectById(new Guid(item.Value)));
                }
                //foreach (ListItem item in chbl_Projects.Items)
                //{
                //    if (item.Selected)
                //        entity.Projects.Add(presenter.GetProjectById(new Guid(item.Value)));
                //}
            }
           
            return entity;
        }

        private void InitDropDowns()
        {
            RadComboBox_Roles.DataSource = PDMRoles.Roles;
            RadComboBox_Roles.DataBind();

            List<Company> listC = presenter.GetCompanyByUser(null);
            listC.Insert(0,new Company(){Id= new Guid(), CompanyName = "--- Select ---", CompanyCode = ""});
            RadComboBox_Companies.DataSource = listC;
            RadComboBox_Companies.DataBind();

            RadListBox_Projects.DataSource = presenter.GetProjectByCompany(null);
            RadListBox_Projects.DataBind();

            //chbl_Projects.DataSource = presenter.GetProjectByCompany(null);
            //chbl_Projects.DataBind();
        }

        public string[] SelectedRoles
        {
            get
            {
                var roles = new List<string>();

                foreach (RadComboBoxItem item in RadComboBox_Roles.Items)
                {
                    if (item.Selected)
                        roles.Add(item.Value);

                }

                return roles.ToArray();

            }
            set
            {
                if(value == null)
                    return;
                if(value.ToArray().Length == 0)
                    return;

                foreach (RadComboBoxItem item in RadComboBox_Roles.Items)
                {
                    foreach (string s in value.ToArray())
                    {
                         if(s == item.Value)
                         {
                             RadComboBox_Roles.SelectedValue = s;
                             return;
                         }
                    }
                }
            }
        }

        public void Roles_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(RadComboBox_Roles.SelectedValue == PDMRoles.ROLE_Administrator || RadComboBox_Roles.SelectedValue == PDMRoles.ROLE_User)
            {
                RadComboBox_Companies.SelectedIndex = 0;
                RadComboBox_Companies.Enabled = false;

                RadListBox_Projects.Enabled = false;
                //chbl_Projects.Enabled = false;
            }
            else if(RadComboBox_Roles.SelectedValue == PDMRoles.ROLE_Customer)
            {
                RadComboBox_Companies.Enabled = true;
                RadListBox_Projects.Enabled = true;
                //chbl_Projects.Enabled = true;
            }
        }

        public void Companies_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(RadComboBox_Companies.SelectedValue))
            {
                RadListBox_Projects.DataSource =
                    presenter.GetProjectByCompany(new Guid(RadComboBox_Companies.SelectedValue));
                //chbl_Projects.DataSource =
                //    presenter.GetProjectByCompany(new Guid(RadComboBox_Companies.SelectedValue));
            }
            else
            {
                RadListBox_Projects.DataSource = presenter.GetProjectByCompany(null);
                //chbl_Projects.DataSource = presenter.GetProjectByCompany(null);
            }

            RadListBox_Projects.DataBind();
            //chbl_Projects.DataBind();
        }
    }
}