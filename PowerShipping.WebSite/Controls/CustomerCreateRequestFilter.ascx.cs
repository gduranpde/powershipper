﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;

namespace PowerShipping.WebSite.Controls
{
    public partial class CustomerCreateRequestFilter : System.Web.UI.UserControl
    {
        public delegate void CustomerCreateRequestFilterHandler(string customQuery);
        public event CustomerCreateRequestFilterHandler OnFilter;

        public delegate void CustomerCreateRequestFilterClearHandler(string customQuery);
        public event CustomerCreateRequestFilterClearHandler OnClear;

        CustomerCreateRequestPresenter presenter = new CustomerCreateRequestPresenter();
        UserManagementPresenter umpresenter = new UserManagementPresenter();

        public string DefaultQuery
        {
            get { return "select * from [vw_CustomerCreateRequestHistory]"; }
        }

        public string CurrentQuery
        {
            get
            {
                return BuildQuery();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                User u = umpresenter.GetOne(HttpContext.Current.User.Identity.Name);

                List<ListItem> list = new List<ListItem>();
                list.Add(new ListItem("--- All ---", Guid.Empty.ToString()));

                if (u.Roles.Contains(PDMRoles.ROLE_Customer))
                {
                    List<Project> listP = umpresenter.GetProjectByUser(u.UserID);

                    foreach (Project project in listP)
                    {
                        list.Add(new ListItem(project.FullName, project.Id.ToString()));
                    }
                }
                else
                {
                    List<Project> listP = umpresenter.GetProjectByUser(null);

                    foreach (Project project in listP)
                    {
                        list.Add(new ListItem(project.FullName, project.Id.ToString()));
                    }
                }

                RadComboBox_Projects.DataSource = list;
                RadComboBox_Projects.DataBind();
            }
        }

        protected void Button_Filter_Click(object sender, EventArgs e)
        {
            string query = BuildQuery();
            OnFilter(query);
        }

        protected void Button_Clear_Click(object sender, EventArgs e)
        {
            Clear();
            string query = BuildQuery();
            OnClear(query);
        }

        private void Clear()
        {
            RadTextBox_User.Text = null;
            RadTextBox_File.Text = null;
            RadTextBox_Source.Text = null;
            RadComboBox_Projects.SelectedIndex = 0;
            RadDatePicker_ImportStartDate.SelectedDate = null;
            RadDatePicker_ImportEndDate.SelectedDate = null;
        }

        private string BuildQuery()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("select * from [vw_CustomerCreateRequestHistory] where 1 = 1   ");
            if (!string.IsNullOrEmpty(RadTextBox_User.Text.Trim()))
            {
                if (RadTextBox_User.Text.Trim() == "null")
                    sb.AppendFormat(" and ([UserName] IS NULL OR [UserName] = '')");
                else
                    sb.AppendFormat(" and ([UserName] LIKE '%'+ '{0}' +'%')   ", RadTextBox_User.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_File.Text.Trim()))
            {
                if (RadTextBox_File.Text.Trim() == "null")
                    sb.AppendFormat(" and ([FileName] IS NULL OR [FileName] = '')");
                else
                    sb.AppendFormat(" and ([FileName] LIKE '%'+ '{0}' +'%')   ", RadTextBox_File.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_Source.Text.Trim()))
            {
                if (RadTextBox_Source.Text.Trim() == "null")
                    sb.AppendFormat(" and ([Source] IS NULL OR [Source] = '')");
                else
                    sb.AppendFormat(" and ([Source] LIKE '%'+ '{0}' +'%')   ", RadTextBox_Source.Text);
            }
            if (this.RadDatePicker_ImportEndDate.SelectedDate.HasValue || this.RadDatePicker_ImportStartDate.SelectedDate.HasValue)
            {
                sb.AppendFormat(" and ( ");
                if (this.RadDatePicker_ImportStartDate.SelectedDate.HasValue)
                    sb.AppendFormat("[UploadTime] >= '{0}'", this.RadDatePicker_ImportStartDate.SelectedDate.Value.ToString("MM/dd/yyyy"));
                if (this.RadDatePicker_ImportEndDate.SelectedDate.HasValue & this.RadDatePicker_ImportStartDate.SelectedDate.HasValue)
                    sb.AppendFormat(" and ");
                if (this.RadDatePicker_ImportEndDate.SelectedDate.HasValue)
                    sb.AppendFormat("[UploadTime] < '{0}'", this.RadDatePicker_ImportEndDate.SelectedDate.Value.AddDays(1).ToString("MM/dd/yyyy"));
                sb.AppendFormat(" )   ");
            }

            if (new Guid(RadComboBox_Projects.SelectedValue) != Guid.Empty)
            {
                sb.AppendFormat(" AND ProjectId = '" + RadComboBox_Projects.SelectedValue + "'");
            }

            return sb.ToString();

        }
    }
}