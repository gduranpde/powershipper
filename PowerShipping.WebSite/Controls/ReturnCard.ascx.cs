﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;
using Telerik.Web.UI.GridExcelBuilder;

namespace PowerShipping.WebSite.Controls
{
    public partial class ReturnCard : System.Web.UI.UserControl
    {
        private object _dataItem = null;
        private ShipmentDetailsPresenter presenter = new ShipmentDetailsPresenter();
        UserManagementPresenter presenterAdv = new UserManagementPresenter();

        public Guid ShipmentId
        {
            get { return (!string.IsNullOrEmpty(HiddenField_Id.Value)) ? new Guid(HiddenField_Id.Value) : Guid.Empty; }
            set { HiddenField_Id.Value = value.ToString(); }
        }

        public Guid ConsumerRequestId
        {
            get { return (!string.IsNullOrEmpty(HiddenField_ConsumerRequestId.Value)) ? new Guid(HiddenField_ConsumerRequestId.Value) : Guid.Empty; }
            set { HiddenField_ConsumerRequestId.Value = value.ToString(); }
        }

        public object DataItem
        {
            get { return _dataItem; }
            set { _dataItem = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!Common.IsCanEdit())
            {
                btnUpdate.Visible = false;
                btnInsert.Visible = false;
            }

            base.OnPreRender(e);
        }

        private void InitializeComponent()
        {
            DataBinding += new EventHandler(Init_DataBinding);
        }

        private void Init_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        public void InitData()
        {
            InitDropDowns();

            if ((DataItem != null) & (DataItem.GetType() != typeof(GridInsertionObject)))
            {
                var entity = (ShipmentDetail)DataItem;

                RadTextBox_AccountName.Text = entity.AccountName;
                RadTextBox_Address1.Text = entity.Address1;
                RadTextBox_Address2.Text = entity.Address2;
                RadTextBox_City.Text = entity.City;
                RadTextBox_State.Text = entity.State;
                RadTextBox_ZipCode.Text = entity.ZipCode;
                RadTextBox_Email.Text = entity.Email;
                RadMaskedTextBox_Phone1.Text = EnumUtils.GetPhoneDigits(entity.Phone1);
                RadMaskedTextBox_Phone2.Text = EnumUtils.GetPhoneDigits(entity.Phone2);

                RadComboBox_ReshipStatus.SelectedValue = Convert.ToInt32(entity.ReshipStatus).ToString();
                RadDatePicker_ShippingStatusDate.SelectedDate = entity.ShippingStatusDate;
                RadDatePicker_ReturnDate.SelectedDate = entity.ReturnDate;
                RadTextBox_Notes.Text = entity.Notes;

                RadComboBox_Shipper.SelectedValue = entity.ShipperId.ToString();
                RadComboBox_Project.SelectedValue = entity.ProjectId.ToString();

                if (entity.ReasonId != null)
                    RadComboBox_Reason.SelectedValue = entity.ReasonId.ToString();

                RadTextBox_OSAddress1.Text = entity.ShipmentDetailAddress1;
                RadTextBox_OSAddress2.Text = entity.ShipmentDetailAddress2;
                RadTextBox_OSCity.Text = entity.ShipmentDetailCity;
                RadTextBox_OSState.Text = entity.ShipmentDetailState;
                RadTextBox_OSZip.Text = entity.ShipmentDetailZip;

                ShipmentId = entity.Id;
                ConsumerRequestId = entity.ConsumerRequestId;

                SetVisibility(true);
            }
            else
            {
                SetVisibility(false);
            }
        }

        public ShipmentDetail FillEntity()
        {
            var entity = new ShipmentDetail();

            if (ShipmentId != Guid.Empty)
            {
                entity.AccountName = RadTextBox_AccountName.Text.ToUpperInvariant();
                entity.Address1 = RadTextBox_Address1.Text.ToUpperInvariant();
                entity.Address2 = RadTextBox_Address2.Text.ToUpperInvariant();
                entity.City = RadTextBox_City.Text.ToUpperInvariant();
                entity.State = RadTextBox_State.Text.ToUpperInvariant();
                entity.ZipCode = RadTextBox_ZipCode.Text;
                entity.Email = RadTextBox_Email.Text;
                entity.Phone1 = EnumUtils.GetPhoneDigits(RadMaskedTextBox_Phone1.Text);
                entity.Phone2 = EnumUtils.GetPhoneDigits(RadMaskedTextBox_Phone2.Text);

                entity.ReshipStatus = (ShipmentReshipStatus)Convert.ToInt32(RadComboBox_ReshipStatus.SelectedValue);

                if (RadDatePicker_ShippingStatusDate.SelectedDate != null)
                    entity.ShippingStatusDate = (DateTime)RadDatePicker_ShippingStatusDate.SelectedDate;
                if (RadDatePicker_ReturnDate.SelectedDate != null)
                    entity.ReturnDate = (DateTime)RadDatePicker_ReturnDate.SelectedDate;
                entity.Notes = RadTextBox_Notes.Text;

                if (RadComboBox_Reason.SelectedValue != Guid.Empty.ToString())
                    entity.ReasonId = new Guid(RadComboBox_Reason.SelectedValue);
                else
                    entity.ReasonId = null;

                entity.Id = ShipmentId;
                entity.ConsumerRequestId = ConsumerRequestId;
            }
            else
            {
            }

            entity.ShipperId = new Guid(RadComboBox_Shipper.SelectedValue);
            //entity.ProjectId = new Guid(RadComboBox_Project.SelectedValue);

            return entity;
        }

        public void DeleteEntity()
        {
        }

        private void InitDropDowns()
        {
            RadComboBox_ReshipStatus.DataSource = EnumUtils.EnumToListItems(typeof(ShipmentReshipStatus));
            RadComboBox_ReshipStatus.DataBind();

            RadComboBox_Shipper.DataSource = presenterAdv.GetAllShipers();
            RadComboBox_Shipper.DataBind();

            User u = presenterAdv.GetOne(HttpContext.Current.User.Identity.Name);
            if (u.Roles.Contains(PDMRoles.ROLE_Customer))
                RadComboBox_Project.DataSource = presenterAdv.GetProjectByUser(u.UserID);
            else
                RadComboBox_Project.DataSource = presenterAdv.GetProjectByUser(null);

            RadComboBox_Project.DataBind();

            List<Reason> listReasons = new List<Reason>();
            listReasons.Add(new Reason() { ReasonCode = "", ReasonDescription = "", ReasonID = Guid.Empty });
            List<Reason> listR = presenter.GetAllReasons();
            foreach (Reason reason in listR)
            {
                listReasons.Add(reason);
            }

            RadComboBox_Reason.DataSource = listReasons;
            RadComboBox_Reason.DataBind();
        }

        private void SetVisibility(bool forUpdate)
        {
            if (forUpdate)
            {
            }
            else
            {
            }
        }
    }
}