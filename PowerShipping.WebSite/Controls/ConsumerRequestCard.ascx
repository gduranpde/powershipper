﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConsumerRequestCard.ascx.cs"
    Inherits="PowerShipping.WebSite.Controls.ConsumerRequestCard" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:HiddenField ID="HiddenField_Id" runat="server" />
<table>
    <tr>
        <td id="tr_AccountNumber" runat="server">
            AccountNumber
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_AccountNumber" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_AccountName" runat="server">
        <td>
            Name
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_AccountName" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_CompanyName" runat="server">
        <td>
            Company Name
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_CompanyName" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_Address1" runat="server">
        <td>
            Address 1
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_Address1" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_Address2" runat="server">
        <td>
            Address 2
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_Address2" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_City" runat="server">
        <td>
            City
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_City" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_State" runat="server">
        <td>
            State
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_State" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_ZipCode" runat="server">
        <td>
            Zip
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_ZipCode" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_ServiceAddress1" runat="server">
        <td>
            ServiceAddress1
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_ServiceAddress1" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_ServiceAddress2" runat="server">
        <td>
            ServiceAddress2
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_ServiceAddress2" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_ServiceCity" runat="server">
        <td>
            ServiceCity
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_ServiceCity" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_ServiceState" runat="server">
        <td>
            ServiceState
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_ServiceState" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_ServiceZip" runat="server">
        <td>
            ServiceZip
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_ServiceZip" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr runat="server" id="tr_Project">
        <td>
            Project
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBox_Project" runat="server" DataValueField="Id"
                DataTextField="FullName" Width="300px">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr id="tr_Email" runat="server">
        <td>
            Email
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_Email" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_Phone1" runat="server">
        <td>
            Phone1
        </td>
        <td>
            <telerik:RadMaskedTextBox ID="RadMaskedTextBox_Phone1" runat="server" Mask="###-###-####">
            </telerik:RadMaskedTextBox>
        </td>
    </tr>
    <tr id="tr_Phone2" runat="server">
        <td>
            Phone2
        </td>
        <td>
            <telerik:RadMaskedTextBox ID="RadMaskedTextBox_Phone2" runat="server" Mask="###-###-####">
            </telerik:RadMaskedTextBox>
        </td>
    </tr>
    <tr id="tr_Method" runat="server">
        <td>
            Method
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_Method" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_IsReship" runat="server">
        <td>
            Reship?
        </td>
        <td>
            <asp:CheckBox ID="CheckBox_IsReship" runat="server"></asp:CheckBox>
        </td>
    </tr>
    <tr id="tr_IsOptOut" runat="server">
        <td>
            Opt Out
        </td>
        <td>
            <asp:CheckBox ID="CheckBox_IsOptOut" runat="server"></asp:CheckBox>
        </td>
    </tr>
    <tr id="tr_IsContact" runat="server">
        <td>
            Contact?
        </td>
        <td>
            <asp:CheckBox ID="CheckBox_IsContact" runat="server"></asp:CheckBox>
        </td>
    </tr>
    <tr id="tr_AnalysisDate" runat="server">
        <td>
            AnalysisDate
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDatePicker_AnalysisDate" runat="server" Skin="Telerik"
                Width="135px">
                <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                    ViewSelectorText="x">
                </Calendar>
                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                </DateInput>
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr id="tr_ReceiptDate" runat="server">
        <td>
            ReceiptDate
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDatePicker_ReceiptDate" runat="server" Skin="Telerik"
                Width="135px">
                <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                    ViewSelectorText="x">
                </Calendar>
                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                </DateInput>
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr id="tr_ImportedDate" runat="server">
        <td>
            Import Date
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDatePicker_ImportedDate" runat="server" Skin="Telerik"
                Width="135px">
                <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                    ViewSelectorText="x">
                </Calendar>
                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                </DateInput>
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr id="tr_Quantity" runat="server">
        <td>
            Quantity
        </td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBox_Quantity" runat="server" NumberFormat-DecimalDigits="0">
                <%--Value="1" DataType="System.Int"--%>
            </telerik:RadNumericTextBox>
        </td>
    </tr>
    <tr id="tr_AuditFailureDate" runat="server">
        <td>
            Audit Failure Date
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDatePicker_AuditFailureDate" runat="server" Skin="Telerik"
                Width="135px">
                <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                    ViewSelectorText="x">
                </Calendar>
                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                </DateInput>
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr id="tr_WaterHeaterFuel" runat="server">
        <td>
            WaterHeaterFuel
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_WaterHeaterFuel" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_HeaterFuel" runat="server">
        <td>
            HeaterFuel
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_HeaterFuel" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
        <%--//PG31--%>
     <tr id="tr_FacilityType" runat="server">
        <td>
            Facility Type
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_FacilityType" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
     <tr id="tr_RateCode" runat="server">
        <td>
            Rate Code
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_RateCode" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
     <tr id="tr_IncomeQualified" runat="server">
        <td>
            Income Qualified
        </td>
        
        <td>
            <telerik:RadTextBox ID="RadTextBox_IncomeQualified" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <%--//PG31--%>
    <tr id="tr_PremiseID" runat="server">
        <td>
            PremiseID
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_PremiseID" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>

    <tr id="tr_OperatingCompany" runat="server">
        <td>
            OperatingCompany
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_OperatingCompany" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_Notes" runat="server">
        <td>
            Notes
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_Notes" runat="server" TextMode="MultiLine" Width="500px"
                Height="50px" MaxLength="1024">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr runat="server" id="tr_Status" visible="true">
        <td>
            Status
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBox_Status" runat="server" DataValueField="Value"
                DataTextField="Text">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr id="tr_OutOfState" runat="server">
        <td>
            OutOfState
        </td>
        <td>
            <asp:CheckBox ID="CheckBox_OutOfState" runat="server"></asp:CheckBox>
        </td>
    </tr>
    <tr id="tr_Batch" runat="server">
        <td>
            Batch ID
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_BatchId" runat="server" ReadOnly="true" Width="220px">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_AccountNumberOld" runat="server">
        <td>
            Account# Old
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_AccountNumberOld" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr1" runat="server">
        <td>
        </td>
        <td>
            <asp:LinkButton ID="lnkBtnShipmentDet" runat="server" Text="View list of shipments"
                OnClick="lnkBtnShipmentDet_OnClick"></asp:LinkButton>
        </td>
    </tr>
      <tr id="tr_RequestedKit" runat="server">
        <td>
           Requested Kit
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_RequestedKit" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr2" runat="server">
        <td>
        </td>
        <td>
        </td>
    </tr>
    <tr runat="server" id="td_CommandCell">
        <td colspan="2">
            <asp:LinkButton ID="btnUpdate" Text="Save" runat="server" CommandName="Update" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>'
                ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnInsert" Text="Save" runat="server" CommandName="PerformInsert"
                Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>' ValidationGroup="Card"
                Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False"
                CommandName="Cancel" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnDelete" Text="Delete" runat="server" CausesValidation="False"
                CommandName="Cancel" OnClick="btnDelete_OnClick" Font-Size="Medium"></asp:LinkButton>
        </td>
    </tr>
</table>