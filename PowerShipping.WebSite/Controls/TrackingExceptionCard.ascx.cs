﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class TrackingExceptionCard : System.Web.UI.UserControl
    {
        private object _dataItem = null;
        private TrackingExceptionsPresenter presenter = new TrackingExceptionsPresenter();

        public Guid TrackingNumberExceptionId
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_ConsumerRequestExceptionId.Value))
                           ? new Guid(HiddenField_ConsumerRequestExceptionId.Value)
                           : Guid.Empty;
            }
            set { HiddenField_ConsumerRequestExceptionId.Value = value.ToString(); }
        }

        public object DataItem
        {
            get { return _dataItem; }
            set { _dataItem = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
            DataBinding += new EventHandler(Init_DataBinding);
        }

        private void Init_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        public void InitData()
        {
            InitDropDowns();
            if ((DataItem != null) & (DataItem.GetType() != typeof(GridInsertionObject)))
            {
                var entity = (TrackingNumberException)DataItem;

                RadTextBox_PurchaseOrderNumber.Text = entity.PurchaseOrderNumber;
                RadTextBox_FedExTrackingNumber.Text = entity.TrackingNumber;
                RadTextBox_AccountNumber.Text = entity.AccountNumber;
                RadTextBox_AccountName.Text = entity.AccountName;
                RadTextBox_Address1.Text = entity.Address1;
                RadTextBox_Address2.Text = entity.Address2;
                RadTextBox_City.Text = entity.City;
                RadTextBox_State.Text = entity.State;
                RadTextBox_ZipCode.Text = entity.ZipCode;

                RadTextBox_Reference.Text = entity.Reference;
             
                TrackingNumberExceptionId = entity.Id;
                SetVisibility(true);
            }
            else
            {
                SetVisibility(false);
            }
        }

        public TrackingNumberException FillEntity()
        {
            var entity = new TrackingNumberException();

            if (TrackingNumberExceptionId != Guid.Empty)
            {
                entity.PurchaseOrderNumber = RadTextBox_PurchaseOrderNumber.Text;
                entity.TrackingNumber = RadTextBox_FedExTrackingNumber.Text;
                entity.AccountNumber = RadTextBox_AccountNumber.Text;
                entity.AccountName = RadTextBox_AccountName.Text.ToUpperInvariant();
                entity.Address1 = RadTextBox_Address1.Text.ToUpperInvariant();
                entity.Address2 = RadTextBox_Address2.Text.ToUpperInvariant();
                entity.City = RadTextBox_City.Text.ToUpperInvariant();
                entity.State = RadTextBox_State.Text.ToUpperInvariant();
                entity.ZipCode = RadTextBox_ZipCode.Text;

                entity.Reference = RadTextBox_Reference.Text;

                entity.Id = TrackingNumberExceptionId;
            }

            return entity;
        }

        public void DeleteEntity()
        {

        }

        private void InitDropDowns()
        {
            
        }

        private void SetVisibility(bool forUpdate)
        {
            if (forUpdate)
            {

            }
            else
            {

            }
        }
    }
}