﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShipmentCommon.ascx.cs"
    Inherits="PowerShipping.WebSite.Controls.ShipmentCommon" %>
<asp:HiddenField ID="HiddenField_ClientId" runat="server" />
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<table style="width: 350px;">
    <tr>
        <td colspan="2">
            <h3>
                Enter Shipment Information</h3>
        </td>
    </tr>
    <tr>
        <td class="contentTableCell">
            <asp:Label ID="lbNumOfLabelsText" runat="server" Text="# of Labels:"></asp:Label>
        </td>
        <td style="width: 200px;">
            <telerik:RadNumericTextBox ID="RadTextBox_CountToSelect" runat="server" Width="100px">
                <%--DataType="System.Int32"--%>
                <NumberFormat DecimalDigits="0" />
            </telerik:RadNumericTextBox>
            <asp:Label ID="lbMaxResults" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="contentTableCell">
            PO Number:
        </td>
        <td style="width: 250px;">
            <telerik:RadTextBox ID="RadTextBox_PONumber" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td class="contentTableCell">
            Kit Type:
        </td>
        <td style="width: 200px;">
            <telerik:RadComboBox ID="RadComboBox_KitType" runat="server" DataTextField="TextKitLabel"
                DataValueField="Id" Width="250px">
                <%--<ItemTemplate><%# ((PowerShipping.Entities.KitType)Container.DataItem).KitName + "(" + ((PowerShipping.Entities.KitType)Container.DataItem).KitName  + ")"%></ItemTemplate>--%>
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td class="contentTableCell">
            Date Label Needed:
        </td>
        <td style="width: 200px;">
            <telerik:RadDatePicker ID="RadDatePicker_DateLabelNeeded" runat="server" Skin="Telerik"
                Width="155px">
                <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                    ViewSelectorText="x">
                </Calendar>
                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy" AutoPostBack="True">
                </DateInput>
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr>
        <td class="contentTableCell">
            Data Shipped:
        </td>
        <td style="width: 200px;">
            <telerik:RadDatePicker ID="RadDatePicker_DateShipped" runat="server" Skin="Telerik"
                Width="155px">
                <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                    ViewSelectorText="x">
                </Calendar>
                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy" AutoPostBack="True">
                </DateInput>
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr>
        <td class="contentTableCell">
            Shipper:
        </td>
        <td style="width: 200px;">
            <telerik:RadComboBox ID="RadComboBox_Shipper" runat="server" DataValueField="Id"
                DataTextField="FullName">
            </telerik:RadComboBox>
        </td>
    </tr>
</table>