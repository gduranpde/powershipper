﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserManagementCard.ascx.cs" Inherits="PowerShipping.WebSite.Controls.UserManagementCard" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:HiddenField  ID="HiddenField_Id" runat="server" />
<table> 
    <tr>
        <td id="tr_UserName" runat="server">UserName</td>
        <td><telerik:RadTextBox ID="RadTextBox_UserName" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_Password" runat="server">
        <td>Password</td>
        <td><telerik:RadTextBox ID="RadTextBox_Password" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_Roles" runat="server">
        <td>Roles</td>
        <td><telerik:RadComboBox ID="RadComboBox_Roles" runat="server" AutoPostBack = "true" OnSelectedIndexChanged="Roles_SelectedIndexChanged"></telerik:RadComboBox></td>
    </tr> 
    <tr id="tr_Companies" runat="server">
        <td>Companies</td>
        <td><telerik:RadComboBox ID="RadComboBox_Companies" runat="server" DataValueField="Id" DataTextField="FullName" AutoPostBack = "true" OnSelectedIndexChanged="Companies_SelectedIndexChanged"></telerik:RadComboBox></td>
    </tr>
    <tr id="tr_Projects" runat="server">
        <td>Projects</td>
        <td>
        <telerik:RadListBox ID="RadListBox_Projects" runat="server" CheckBoxes="true" SelectionMode="Multiple" 
            DataValueField="Id" DataTextField="FullName" Width="300px"></telerik:RadListBox>
         <%--<asp:ListBox ID="listBox_Projects" runat="server" DataValueField="Id" DataTextField="ProjectName" SelectionMode="Multiple" ></asp:ListBox>
         --%>
        <%-- <asp:CheckBoxList ID="chbl_Projects" runat="server" DataValueField="Id" DataTextField="ProjectName" ></asp:CheckBoxList>--%>
         </td>
    </tr>
    <tr runat="server" id="td_CommandCell">
        <td colspan="2">            
            <asp:LinkButton id="btnUpdate" text="Save" runat="server" CommandName="Update" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton id="btnInsert" text="Save" runat="server" CommandName="PerformInsert" Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnCancel" text="Cancel" runat="server" causesvalidation="False" commandname="Cancel" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnDelete" text="Delete" runat="server" causesvalidation="False" CommandName="Cancel" OnClick="btnDelete_OnClick" Font-Size="Medium"></asp:LinkButton>
         </td>
    </tr>   
</table>