﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;
using System.Linq;

namespace PowerShipping.WebSite.Controls
{
    public partial class OptOutCard : System.Web.UI.UserControl
    {
        private object _dataItem = null;
        private OptOutsPresenter presenter = new OptOutsPresenter();
        private UserManagementPresenter presenterAdv = new UserManagementPresenter();

        public Guid OptOutId
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_Id.Value))
                           ? new Guid(HiddenField_Id.Value)
                           : Guid.Empty;
            }
            set { HiddenField_Id.Value = value.ToString(); }
        }

        public object DataItem
        {
            get { return _dataItem; }
            set { _dataItem = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnInit(EventArgs e)
        {
            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');");

            InitializeComponent();
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!Common.IsCanEdit())
            {
                btnUpdate.Visible = false;
                btnInsert.Visible = false;
                btnDelete.Visible = false;
            }

            base.OnPreRender(e);
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                OptOut oo = presenter.GetById(OptOutId);

                presenter.RemoveOptOut(oo.Id);
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                Label l = new Label();
                l.Text = "Unable to delete OptOut. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                
                //Helper.ErrorInGrid("RadGrid_OptOuts", Page, l);
                Helper.ErrorInGrid(Page, "Unable to delete OptOut. Reason: " + ex.Message);
            }

        }

        private void InitializeComponent()
        {
            DataBinding += new EventHandler(Init_DataBinding);
        }

        private void Init_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        public void InitData()
        {
            InitDropDowns();

            if ((DataItem != null) & (DataItem.GetType() != typeof(GridInsertionObject)))
            {
                var entity = (OptOut)DataItem;

                RadDatePicker_OptOutDate.SelectedDate = entity.OptOutDate;
                RadTextBox_AccountNumber.Text = entity.AccountNumber;
                RadTextBox_AccountName.Text = entity.AccountName;
                RadTextBox_Address1.Text = entity.Address;
                RadTextBox_City.Text = entity.City;
                RadTextBox_State.Text = entity.State;
                RadTextBox_ZipCode.Text = entity.ZipCode;
                RadTextBox_Email.Text = entity.Email;
                RadMaskedTextBox_Phone1.Text = EnumUtils.GetPhoneDigits(entity.Phone1);
                RadMaskedTextBox_Phone2.Text = EnumUtils.GetPhoneDigits(entity.Phone2);

                CheckBox_IsEnergyProgram.Checked = entity.IsEnergyProgram;
                CheckBox_IsTotalDesignation.Checked = entity.IsTotalDesignation;

                RadTextBox_Notes.Text = entity.Notes;

                RadComboBox_Project.SelectedValue = entity.ProjectId.ToString();


                OptOutId = entity.Id;
                SetVisibility(true);
            }
            else
            {
                SetVisibility(false);
            }
        }

        public OptOut FillEntity()
        {
            var entity = new OptOut();

            if (OptOutId != Guid.Empty)
            {
                if (RadDatePicker_OptOutDate.SelectedDate != null)
                    entity.OptOutDate = (DateTime)RadDatePicker_OptOutDate.SelectedDate;
                else
                    entity.OptOutDate = DateTime.Now;

                entity.AccountNumber = RadTextBox_AccountNumber.Text;
                entity.AccountName = RadTextBox_AccountName.Text.ToUpperInvariant();
                entity.Address = RadTextBox_Address1.Text.ToUpperInvariant();
                entity.City = RadTextBox_City.Text.ToUpperInvariant();
                entity.State = RadTextBox_State.Text.ToUpperInvariant();
                entity.ZipCode = RadTextBox_ZipCode.Text;
                entity.Email = RadTextBox_Email.Text;
                entity.Phone1 = EnumUtils.GetPhoneDigits(RadMaskedTextBox_Phone1.Text);
                entity.Phone2 = EnumUtils.GetPhoneDigits(RadMaskedTextBox_Phone2.Text);

                entity.IsEnergyProgram = CheckBox_IsEnergyProgram.Checked;
                entity.IsTotalDesignation = CheckBox_IsTotalDesignation.Checked;
                
                entity.Notes = RadTextBox_Notes.Text;

                entity.Id = OptOutId;
            }
            else
            {
                if (RadDatePicker_OptOutDate.SelectedDate != null)
                    entity.OptOutDate = (DateTime)RadDatePicker_OptOutDate.SelectedDate;
                else
                    entity.OptOutDate = DateTime.Now;

                entity.AccountNumber = RadTextBox_AccountNumber.Text;
                entity.AccountName = RadTextBox_AccountName.Text.ToUpperInvariant();
                entity.Address = RadTextBox_Address1.Text.ToUpperInvariant();
                entity.City = RadTextBox_City.Text.ToUpperInvariant();
                entity.State = RadTextBox_State.Text.ToUpperInvariant();
                entity.ZipCode = RadTextBox_ZipCode.Text;
                entity.Email = RadTextBox_Email.Text;
                entity.Phone1 = EnumUtils.GetPhoneDigits(RadMaskedTextBox_Phone1.Text);
                entity.Phone2 = EnumUtils.GetPhoneDigits(RadMaskedTextBox_Phone2.Text);

                entity.IsEnergyProgram = CheckBox_IsEnergyProgram.Checked;
                entity.IsTotalDesignation = CheckBox_IsTotalDesignation.Checked;

                entity.Notes = RadTextBox_Notes.Text;

                entity.Id = Guid.NewGuid();
            }

            entity.ProjectId = new Guid(RadComboBox_Project.SelectedValue);

            return entity;
        }

        public void DeleteEntity()
        {

        }

        private void InitDropDowns()
        {
            User u = presenterAdv.GetOne(HttpContext.Current.User.Identity.Name);

            List<Project> list = new List<Project>();
            if (u.Roles.Contains(PDMRoles.ROLE_Customer))
                list = presenterAdv.GetProjectByUser(u.UserID);
            else
                list = presenterAdv.GetProjectByUser(null);

            if (DataItem as OptOut == null)
            {
                list.Insert(0, new Project() { Id = Guid.Empty });
            }

            RadComboBox_Project.DataSource = list;
            RadComboBox_Project.DataBind();
        }

        private void SetVisibility(bool forUpdate)
        {
            if (forUpdate)
            {

            }
            else
            {

            }
        }
    }
}