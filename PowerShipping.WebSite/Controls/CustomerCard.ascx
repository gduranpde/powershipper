﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerCard.ascx.cs" Inherits="PowerShipping.WebSite.Controls.CustomerCard" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:HiddenField  ID="HiddenField_Id" runat="server" />
<table> 
     <tr>
        <td id="tr_InvitationCode" runat="server">InvitationCode</td>
        <td><telerik:RadTextBox ID="RadTextBox_InvitationCode" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td id="tr_AccountNumber" runat="server">AccountNumber</td>
        <td><telerik:RadTextBox ID="RadTextBox_AccountNumber" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_AccountName" runat="server">
        <td>Name</td>
        <td><telerik:RadTextBox ID="RadTextBox_AccountName" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_CompanyName" runat="server">
        <td>Company Name</td>
        <td><telerik:RadTextBox ID="RadTextBox_CompanyName" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_Address1" runat="server">
        <td>Address 1</td>
        <td><telerik:RadTextBox ID="RadTextBox_Address1" runat="server"></telerik:RadTextBox></td>
    </tr> 
    <tr id="tr_Address2" runat="server">
        <td>Address 2</td>
        <td><telerik:RadTextBox ID="RadTextBox_Address2" runat="server"></telerik:RadTextBox></td>
    </tr>     
    <tr id="tr_City" runat="server">
        <td>City</td>
        <td><telerik:RadTextBox ID="RadTextBox_City" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_State" runat="server">
        <td>State</td>
        <td><telerik:RadTextBox ID="RadTextBox_State" runat="server" MaxLength="2"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_ZipCode" runat="server">
        <td>Zip</td>
        <td><telerik:RadTextBox ID="RadTextBox_ZipCode" runat="server" MaxLength="5"></telerik:RadTextBox></td>
    </tr>
    
    <tr id="tr_ServiceAddress1" runat="server">
        <td>ServiceAddress1</td>
        <td><telerik:RadTextBox ID="RadTextBox_ServiceAddress1" runat="server"></telerik:RadTextBox></td>
    </tr> 
    <tr id="tr_ServiceAddress2" runat="server">
        <td>ServiceAddress2</td>
        <td><telerik:RadTextBox ID="RadTextBox_ServiceAddress2" runat="server"></telerik:RadTextBox></td>
    </tr>     
    <tr id="tr_ServiceCity" runat="server">
        <td>ServiceCity</td>
        <td><telerik:RadTextBox ID="RadTextBox_ServiceCity" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_ServiceState" runat="server">
        <td>ServiceState</td>
        <td><telerik:RadTextBox ID="RadTextBox_ServiceState" runat="server" MaxLength="2"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_ServiceZip" runat="server">
        <td>ServiceZip</td>
        <td><telerik:RadTextBox ID="RadTextBox_ServiceZip" runat="server"  MaxLength="5"></telerik:RadTextBox></td>
    </tr>
    
    <tr runat="server" id="tr_Project">
        <td>Project</td>
        <td><telerik:RadComboBox ID="RadComboBox_Project" runat="server" DataValueField="Id" DataTextField="FullName" Width="300px"></telerik:RadComboBox></td>
    </tr>
    <tr id="tr_Email" runat="server">
        <td>Email 1</td>
        <td><telerik:RadTextBox ID="RadTextBox_Email" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_Email2" runat="server">
        <td>Email 2</td>
        <td><telerik:RadTextBox ID="RadTextBox_Email2" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_Phone1" runat="server">
        <td>Phone1</td>
        <td><telerik:RadMaskedTextBox ID="RadMaskedTextBox_Phone1" runat="server" Mask="###-###-####" ></telerik:RadMaskedTextBox></td>
    </tr>        
    <tr id="tr_Phone2" runat="server">
        <td>Phone2</td>
        <td><telerik:RadMaskedTextBox ID="RadMaskedTextBox_Phone2" runat="server" Mask="###-###-####" ></telerik:RadMaskedTextBox></td>
    </tr> 
    
   
     <tr id="tr_IsOptOut" runat="server">
        <td>Opt Out</td>
        <td><telerik:RadComboBox ID="RadComboBox_OptOut" runat="server" >
            <Items>
                <telerik:RadComboBoxItem Text="--- None ---" Value=""/>
                <telerik:RadComboBoxItem Text="Yes" Value="True"/>
                <telerik:RadComboBoxItem Text="No" Value="False"/>
            </Items>
        </telerik:RadComboBox></td>
    </tr> 
     <tr id="tr_IsContact" runat="server">
        <td>Contact?</td>
        <td><telerik:RadComboBox ID="RadComboBox_OkContact" runat="server" >
            <Items>
                <telerik:RadComboBoxItem Text="--- None ---" Value=""/>
                <telerik:RadComboBoxItem Text="Yes" Value="True"/>
                <telerik:RadComboBoxItem Text="No" Value="False"/>
            </Items>
        </telerik:RadComboBox></td>
    </tr> 
    
   
    
    
     <tr id="tr_WaterHeaterFuel" runat="server">
        <td>WaterHeaterFuel</td>
        <td><telerik:RadTextBox ID="RadTextBox_WaterHeaterFuel" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_PremiseID" runat="server">
        <td >PremiseID</td>
        <td><telerik:RadTextBox ID="RadTextBox_PremiseID" runat="server"></telerik:RadTextBox></td>
    </tr>
     <tr id="tr_OperatingCompany" runat="server">
        <td>OperatingCompany</td>
        <td><telerik:RadTextBox ID="RadTextBox_OperatingCompany" runat="server"></telerik:RadTextBox></td>
    </tr>

	<tr id="tr1" runat="server">
        <td>RateCode</td>
        <td><telerik:RadTextBox ID="RadTextBox_RateCode" runat="server"></telerik:RadTextBox></td>
    </tr>
          
    <tr id="tr2" runat="server">
        <td></td>
        <td></td>
    </tr>
    
    <tr runat="server" id="td_CommandCell">
        <td colspan="2">            
            <%--<asp:LinkButton id="btnUpdate" text="Save" runat="server" CommandName="Update" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>--%>
            <%--<asp:LinkButton id="btnInsert" text="Save" runat="server" CommandName="PerformInsert" Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>--%>
            <asp:LinkButton ID="btnCancel" text="Cancel" runat="server" causesvalidation="False" commandname="Cancel" Font-Size="Medium"></asp:LinkButton>
            <%--<asp:LinkButton ID="btnDelete" text="Delete" runat="server" causesvalidation="False" CommandName="Cancel" OnClick="btnDelete_OnClick" Font-Size="Medium"></asp:LinkButton>--%>
         </td>
    </tr>                      
</table>