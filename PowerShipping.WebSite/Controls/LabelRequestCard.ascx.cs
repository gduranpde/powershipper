﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class LabelRequestCard : System.Web.UI.UserControl
    {
        private object _dataItem = null;
        UserManagementPresenter presenterAdv = new UserManagementPresenter();
        ShipmentDetailsPresenter presenter = new ShipmentDetailsPresenter();
        ShipmentsPresenter presenterShipment = new ShipmentsPresenter();

        public Guid ShipmentId
        {
            get { return (!string.IsNullOrEmpty(HiddenField_Id.Value)) ? new Guid(HiddenField_Id.Value) : Guid.Empty; }
            set { HiddenField_Id.Value = value.ToString(); }
        }

        public object DataItem
        {
            get { return _dataItem; }
            set { _dataItem = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!Common.IsCanEdit())
            {
                btnUpdate.Visible = false;
                btnInsert.Visible = false;
                btnDelete.Visible = false;
            }

            base.OnPreRender(e);
        }

        protected override void OnInit(EventArgs e)
        {
            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');");

            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
            DataBinding += new EventHandler(Init_DataBinding);
        }

        private void Init_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        public void InitData()
        {
            InitDropDowns();

            if ((DataItem != null) & (DataItem.GetType() != typeof(GridInsertionObject)))
            {
                var entity = (Shipment)DataItem;

                RadTextBox_PONumber.Text = entity.PurchaseOrderNumber;
                RadComboBox_Shipper.SelectedValue = entity.ShipperId.ToString();
                RadComboBox_Kit.SelectedValue = entity.KitTypeId.ToString();

                ShipmentId = entity.Id;

                //SetVisibility(true);
            }
            else
            {
                //SetVisibility(false);
            }
        }

        public Shipment FillEntity()
        {
            var entity = new Shipment();

            if (ShipmentId != Guid.Empty)
            {
                entity.PurchaseOrderNumber = RadTextBox_PONumber.Text;

                if (RadComboBox_Shipper.SelectedValue != Guid.Empty.ToString())
                    entity.ShipperId = new Guid(RadComboBox_Shipper.SelectedValue);

                if (RadComboBox_Kit.SelectedValue != Guid.Empty.ToString())
                    entity.KitTypeId = new Guid(RadComboBox_Kit.SelectedValue);

                entity.Id = ShipmentId;
            }

            return entity;
        }

        private void InitDropDowns()
        {
            RadComboBox_Shipper.DataSource = presenterAdv.GetAllShipers();
            RadComboBox_Shipper.DataBind();

            RadComboBox_Kit.DataSource = presenter.GetListOfKitTypes();
            RadComboBox_Kit.DataBind();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            presenterShipment.Delete(ShipmentId);
        }
    }
}