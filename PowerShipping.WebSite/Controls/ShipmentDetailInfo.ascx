﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShipmentDetailInfo.ascx.cs" Inherits="PowerShipping.WebSite.Controls.ShipmentDetailInfo" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<h3>Edit Shipment Details</h3>
<table style=" width : 450px;">
    <tr>
        <td  style=" width : 70px;">
            <asp:Label ID="Label1" runat="server" Text="Zip Code:"></asp:Label>
        </td>
         <td style=" width : 260px;">
           <telerik:RadTextBox ID="RadTexBox_AmoutDelivered" runat="server" ></telerik:RadTextBox>
        </td>
    </tr>
    <tr runat="server" id="td_CommandCell">
        <td colspan="2">
            <asp:LinkButton ID="btnCancel" text="Cancel" runat="server" causesvalidation="False" commandname="Cancel"></asp:LinkButton>
            <asp:LinkButton id="btnUpdate" text="Update" runat="server" CommandName="Update" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>' ValidationGroup="Card"></asp:LinkButton>
            <asp:LinkButton id="btnInsert" text="Insert" runat="server" CommandName="PerformInsert" Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>' ValidationGroup="Card"></asp:LinkButton>
        </td>
    </tr>
</table>    
    