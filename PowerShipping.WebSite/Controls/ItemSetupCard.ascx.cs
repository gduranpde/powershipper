﻿using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class ItemSetupCard : System.Web.UI.UserControl
    {

        private readonly ItemsSetupPresenter presenter = new ItemsSetupPresenter();


        public Guid ItemID
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_Id.Value))
                           ? new Guid(HiddenField_Id.Value)
                           : Guid.Empty;
            }
            set { HiddenField_Id.Value = value.ToString(); }
        }


        public object DataItem { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void InitData()
        {

            if ((DataItem != null) & (DataItem.GetType() != typeof(GridInsertionObject)))
            {
                var entity = (Item)DataItem;


                RadTextBox_ItemName.Text = entity.ItemName;
                RadTextBox_ItemDescription.Text = entity.ItemDescription;
                RadNumericTextBox_Default_Kw_Impact.Value = (double)entity.DefaultKwImpact;
                RadNumericTextBox_Default_Kwh_Impact.Value = (double)entity.DefaultKwhImpact;
                CheckBox_IsActive.Checked = entity.IsActive;

                ItemID = entity.ItemID;
            }
        }


        private void Init_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        private void InitializeComponent()
        {
            DataBinding += Init_DataBinding;
        }

        protected override void OnInit(EventArgs e)
        {
            //btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');");

            InitializeComponent();
            base.OnInit(e);
        }


        public Item FillEntity()
        {
            var entity = new Item();
            if (ItemID != Guid.Empty)
            {
                entity.ItemID = ItemID;
            }
            else
            {
                entity.ItemID = Guid.NewGuid();
            }

            entity.ItemName = RadTextBox_ItemName.Text;
            entity.ItemDescription = RadTextBox_ItemDescription.Text;
            entity.DefaultKwImpact = Convert.ToDecimal(RadNumericTextBox_Default_Kw_Impact.Value);
            entity.DefaultKwhImpact = Convert.ToDecimal(RadNumericTextBox_Default_Kwh_Impact.Value);
            entity.IsActive = CheckBox_IsActive.Checked;


            return entity;
        }

    }
}