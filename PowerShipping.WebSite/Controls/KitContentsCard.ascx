﻿<%@ Control Language="C#" AutoEventWireup="true"
    CodeBehind="KitContentsCard.ascx.cs"
    Inherits="PowerShipping.WebSite.Controls.KitContentsCard" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:HiddenField ID="HiddenField_Id" runat="server" />

<table>
    <tr id="tr_ItemName" runat="server">
        <td>Name</td>
        <td>
            <telerik:RadComboBox ID="cmbItems" runat="server" DataValueField="ItemID" DataTextField="ItemName" EmptyMessage="Select an Item..." 
                MaxHeight="350" OnSelectedIndexChanged="cmbItems_SelectedIndexChanged" AutoPostBack="true">
            </telerik:RadComboBox>
        </td>
    </tr>

    <tr id="tr_ItemDescription" runat="server">
        <td>Description
        </td>
        <td>             
            <telerik:RadTextBox ID="RadTextBox_ItemDescription" runat="server" ReadOnly="true">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_Quantity" runat="server">
        <td>Quantity
        </td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBox_Quantity" runat="server" Value="1" numberformat-NumberFormat-DecimalDigits="0">
            </telerik:RadNumericTextBox>
        </td>
    </tr>
    <tr id="tr_DefaultKwImpact" runat="server">
        <td>Default kw Impact
        </td>
        <td>            
            <telerik:RadTextBox ID="RadTextBox_Kw" runat="server" ReadOnly="true">
            </telerik:RadTextBox>
        </td>
    </tr>

    <tr id="tr_DefaultKwhImpact" runat="server">
        <td>Default kwh Impact
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_Kwh" runat="server" ReadOnly="true">
            </telerik:RadTextBox>
        </td>
    </tr>

    <tr runat="server" id="td_CommandCell">
        <td colspan="2">
            <asp:LinkButton ID="btnUpdate" Text="Save" runat="server" CommandName="Update" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnInsert" Text="Save" runat="server" CommandName="PerformInsert" Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False" CommandName="Cancel" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnDelete" Text="Delete" runat="server" CausesValidation="False" CommandName="Cancel" OnClick="btnDelete_OnClick" Font-Size="Medium"></asp:LinkButton>
        </td>
    </tr>
</table>
