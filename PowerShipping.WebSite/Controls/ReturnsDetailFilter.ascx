﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReturnsDetailFilter.ascx.cs"
    Inherits="PowerShipping.WebSite.Controls.ReturnsEditInfo" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:HiddenField ID="HiddenField_Returns_ShipmentNumber" runat="server" />
<asp:HiddenField ID="HiddenField_Returns_ReshipStatus" runat="server" />
<asp:HiddenField ID="HiddenField_Returns_AccountNumber" runat="server" />
<asp:HiddenField ID="HiddenField_Returns_FedexNumber" runat="server" />
<asp:HiddenField ID="HiddenField_Returns_AccountName" runat="server" />
<asp:HiddenField ID="HiddenField_Returns_PONumber" runat="server" />
<asp:HiddenField ID="HiddenField_Returns_KitTypeID" runat="server" />
<asp:HiddenField ID="HiddenField_Returns_ShipperID" runat="server" />
<asp:HiddenField ID="HiddenField_Returns_OpCo" runat="server" />
<asp:HiddenField ID="HiddenField_Returns_ReasonID" runat="server" />
<asp:HiddenField ID="HiddenField_Returns_StartDate" runat="server" />
<asp:HiddenField ID="HiddenField_Returns_EndDate" runat="server" />
<div class="jobInfo" runat="server" id="div_jobInfo">
    <div style="text-align: center;">
        <b>Return Details Filter</b></div>
    <table>
        <tr>
            <td colspan="2" align="center">
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button runat="server" ID="btnSearchTop" Text="Search" Width="80px" CssClass="ptButton"
                    OnClick="btnSearchTop_Click" />
                <asp:Button runat="server" ID="btnClearTop" Text="Clear" Width="80px" CssClass="ptButton"
                    OnClick="btnClearTop_Click" />
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Reship Status
            </td>
            <td>
                <div style="padding: 0px; margin-left: -5px;">
                    <telerik:RadComboBox ID="ddReshipStatusFilter" runat="server" AutoPostBack="true"
                        DataTextField="Text" DataValueField="Value" Width="100px">
                        <Items>
                            <telerik:RadComboBoxItem Text="All" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Shipment #:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBox_ShipmentNumber" runat="server" Width="100px">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Account #:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBox_AccountNumberFilter" runat="server" Width="100px">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Tracking #:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBox_FedExTrackingNumber" runat="server" Width="100px">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr id="tr_AccountName" runat="server">
            <td class="jobInfo_td">
                Name:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBox_AccountName" runat="server" Width="100px">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr id="tr_ReturnDate" runat="server" style="height: 30px">
            <td class="jobInfo_td">
                ReturnDate:
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2">
                <div style="padding: 0px; height: 26px;">
                    <span>From:</span>
                    <telerik:RadDatePicker ID="RadDatePicker_ReturnDateStartDate" runat="server" Skin="Telerik"
                        Width="100px">
                        <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                            ViewSelectorText="x">
                        </Calendar>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                        <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                        </DateInput>
                    </telerik:RadDatePicker>
                </div>
                <div style="padding: 0px; height: 26px;">
                    <span>To:</span>
                    <telerik:RadDatePicker ID="RadDatePicker_ReturnDateEndDate" runat="server" Skin="Telerik"
                        Width="100px">
                        <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                            ViewSelectorText="x">
                        </Calendar>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                        <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                        </DateInput>
                    </telerik:RadDatePicker>
                </div>
            </td>
        </tr>
        <tr style="height: 50px">
            <td id="tr_PurchaseOrderNumber" runat="server" class="jobInfo_td">
                PO Number
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBox_PurchaseOrderNumber" runat="server" Width="100px">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr runat="server" id="tr_KitType">
            <td class="jobInfo_td" style="width: 85px;">
                Kit Type Name:
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:ListBox runat="server" Rows="5" SelectionMode="Multiple" ID="ListBoxKitType"
                    DataTextField="KitName" DataValueField="Id"></asp:ListBox>
            </td>
        </tr>
        <tr runat="server" id="tr_Shipper" style="height: 100px">
            <td class="jobInfo_td">
                Shipper:
            </td>
            <td colspan="2" align="center">
                <asp:ListBox runat="server" Rows="3" SelectionMode="Multiple" ID="ListBox_Shipper"
                    DataTextField="ShipperName" DataValueField="Id"></asp:ListBox>
            </td>
        </tr>
        <tr id="tr_CompanyName" runat="server">
            <td class="jobInfo_td">
                Operating Company:
            </td>
            <td><%--class="jobInfo_td"--%>
                <telerik:RadTextBox ID="RadTextBox_OperatingCompany" runat="server" Width="100px">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; padding: 0px;">
                <span style="float: left; margin-left: 7px;">Return</span>
            </td>
            <td style="text-align: right; font-weight: bold; padding: 0px;">
                <span style="float: left; margin-left: -41px;">Reason:</span>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:ListBox runat="server" Rows="5" SelectionMode="Multiple" ID="LstBxReasonforReturn"
                    DataTextField="ReasonFull" DataValueField="ReasonID"></asp:ListBox>
            </td>
        </tr>
        <tr style="height: 50px">
            <td colspan="2" align="center">
                <asp:Button runat="server" ID="btnSearchDown" Text="Search" Width="80px" CssClass="ptButton"
                    OnClick="btnSearchDown_Click" />
                <asp:Button runat="server" ID="btnClearDown" Text="Clear" Width="80px" CssClass="ptButton"
                    OnClick="btnClearDown_Click" />
            </td>
        </tr>
    </table>
</div>