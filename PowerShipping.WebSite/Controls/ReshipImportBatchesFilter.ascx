﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReshipImportBatchesFilter.ascx.cs"
    Inherits="PowerShipping.WebSite.Controls.ReshipImportBatchesFilter" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:HiddenField ID="HiddenField_Returns_BatchName" runat="server" />
<asp:HiddenField ID="HiddenField_Returns_ShipperID" runat="server" />
<asp:HiddenField ID="HiddenFieldImportedBy" runat="server" />
<asp:HiddenField ID="HiddenField_Returns_FromDate" runat="server" />
<asp:HiddenField ID="HiddenField_Returns_ToDate" runat="server" />
<div class="jobInfo" runat="server" id="div_jobInfo">
    <div style="text-align: center;">
        <b>
            <asp:Label ID="LblReshipImportFilter" runat="server" Text="Reship Import Batches"></asp:Label></b></div>
    <table>
        <tr>
            <td colspan="2" align="center">
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button runat="server" ID="btnSearchTop" Text="Search" Width="80px" CssClass="ptButton"
                    OnClick="btnSearchTop_Click" />
                <asp:Button runat="server" ID="btnClearTop" Text="Clear" Width="80px" CssClass="ptButton"
                    OnClick="btnClearTop_Click" />
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Batch Name:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBox_BatchName" runat="server" Width="100px">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr runat="server" id="tr_Shipper" style="height: 100px">
            <td class="jobInfo_td">
                Shipper:
            </td>
            <td colspan="2" align="center">
                <asp:ListBox runat="server" Rows="3" SelectionMode="Multiple" ID="ListBox_Shipper"
                    DataTextField="ShipperName" DataValueField="Id"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Imported By:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBox_ImportedBy" runat="server" Width="100px">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr id="tr_ReturnDate" runat="server" style="height: 30px">
            <td class="jobInfo_td">
                Imported Time:
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2">
                <div style="padding: 0px; height: 26px;">
                    <span>From:</span>
                    <telerik:RadDatePicker ID="RadDatePicker_FromImportedTime" runat="server" Skin="Telerik"
                        Width="100px">
                        <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                            ViewSelectorText="x">
                        </Calendar>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                        <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                        </DateInput>
                    </telerik:RadDatePicker>
                </div>
                <div style="padding: 0px; height: 26px;">
                    <span>To:</span>
                    <telerik:RadDatePicker ID="RadDatePicker_ToImportedTime" runat="server" Skin="Telerik"
                        Width="100px">
                        <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                            ViewSelectorText="x">
                        </Calendar>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                        <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                        </DateInput>
                    </telerik:RadDatePicker>
                </div>
            </td>
        </tr>
        <tr style="height: 50px">
            <td colspan="2" align="center">
                <asp:Button runat="server" ID="btnSearchDown" Text="Search" Width="80px" CssClass="ptButton"
                    OnClick="btnSearchDown_Click" />
                <asp:Button runat="server" ID="btnClearDown" Text="Clear" Width="80px" CssClass="ptButton"
                    OnClick="btnClearDown_Click" />
            </td>
        </tr>
    </table>
</div>