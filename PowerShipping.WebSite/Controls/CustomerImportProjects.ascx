﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerImportProjects.ascx.cs" Inherits="PowerShipping.WebSite.Controls.CustomerImportProjects" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<table>
    <tr>
        <td>Select project:
        </td>
        <td>
            <telerik:radcombobox id="RadComboBox_Projects" runat="server"
                datavaluefield="Value" datatextfield="Text" AutoPostBack="false" onselectedindexchanged="RadComboBox_Projects_SelectedIndexChangedOnCustomerLookup"
                width="400px">
            </telerik:radcombobox>
        </td>
        <td></td>
    </tr>
</table>
