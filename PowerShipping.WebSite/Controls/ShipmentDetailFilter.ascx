﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShipmentDetailFilter.ascx.cs"
    Inherits="PowerShipping.WebSite.Controls.ShipmentDetailFilter" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div class="jobInfo" runat="server" id="div_jobInfo">
    <div style="text-align: center;">
        <b>Shipment Details Filter</b></div>
    <table>
        <tr>
            <td colspan="2" align="center">
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button runat="server" ID="Button_Filter" Text="Search" Width="80px" CssClass="ptButton"
                    OnClick="Button_Filter_Click" />
                <asp:Button runat="server" ID="Button_Clear" Text="Clear" Width="80px" CssClass="ptButton"
                    OnClick="Button_Clear_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Full View:
            </td>
            <td class="jobInfo_info">
                <asp:CheckBox ID="chbFullView" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Account #:
            </td>
            <td class="jobInfo_info">
                <telerik:RadTextBox Width="100px" runat="server" ID="RadTextBox_AccountNumber">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Shipment #:
            </td>
            <td class="jobInfo_info">
                <telerik:RadTextBox Width="100px" runat="server" ID="RadTextBox_ShipmentNumber">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Name:
            </td>
            <td class="jobInfo_info">
                <telerik:RadTextBox Width="100px" runat="server" ID="RadTextBox_AccountName">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Comp Name:
            </td>
            <td class="jobInfo_info">
                <telerik:RadTextBox Width="100px" runat="server" ID="RadTextBox_CompanyName">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                P.O. #:
            </td>
            <td class="jobInfo_info">
                <telerik:RadTextBox Width="100px" runat="server" ID="RadTextBox_PurchaseOrderNumber">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Tracking #:
            </td>
            <td class="jobInfo_info">
                <telerik:RadTextBox Width="100px" runat="server" ID="RadTextBox_FedExTrackingNumber">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Operating Company:
            </td>
            <td class="jobInfo_info">
                <telerik:RadTextBox Width="100px" runat="server" ID="RadTextBox_OperatingCompany">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Shipper:
            </td>
            <td class="jobInfo_info">
                <asp:ListBox Width="100px" runat="server" ID="ListBox_Shipper" DataTextField="ShipperCode"
                    DataValueField="Id" Rows="3" SelectionMode="Multiple"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                KitTypeName:
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">

                <div style="OVERFLOW: auto; width: 200px;">
                <asp:ListBox runat="server" Rows="5" SelectionMode="Multiple" ID="ListBox_KitTypeName" Width="400px"
                    DataValueField="Id" DataTextField="KitName"></asp:ListBox>                

                </div>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                ShippingStatus:
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:ListBox runat="server" Rows="5" SelectionMode="Multiple" ID="ListBox_ShippingStatus"
                    DataTextField="Text" DataValueField="Value"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                ReshipStatus:
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:ListBox runat="server" Rows="5" SelectionMode="Multiple" ID="ListBox_ReshipStatus"
                    DataTextField="Text" DataValueField="Value"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Shipping Status Date:
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2">
                From:
                <telerik:RadDatePicker ID="RadDatePicker_ShippingStatusDateStartDate" runat="server"
                    Skin="Telerik" Width="120px">
                    <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                        ViewSelectorText="x">
                    </Calendar>
                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                    </DateInput>
                </telerik:RadDatePicker>
                <br />
                To:
                <telerik:RadDatePicker ID="RadDatePicker_ShippingStatusDateEndDate" runat="server"
                    Skin="Telerik" Width="120px">
                    <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                        ViewSelectorText="x">
                    </Calendar>
                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                    </DateInput>
                </telerik:RadDatePicker>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Ship Date:
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2">
                From:
                <telerik:RadDatePicker ID="RadDatePicker_ShipDateStartDate" runat="server" Skin="Telerik"
                    Width="120px">
                    <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                        ViewSelectorText="x">
                    </Calendar>
                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                    </DateInput>
                </telerik:RadDatePicker>
                <br />
                To:
                <telerik:RadDatePicker ID="RadDatePicker_ShipDateEndDate" runat="server" Skin="Telerik"
                    Width="120px">
                    <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                        ViewSelectorText="x">
                    </Calendar>
                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                    </DateInput>
                </telerik:RadDatePicker>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Return Date:
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2">
                From:
                <telerik:RadDatePicker ID="RadDatePicker_ReturnDateStartDate" runat="server" Skin="Telerik"
                    Width="120px">
                    <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                        ViewSelectorText="x">
                    </Calendar>
                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                    </DateInput>
                </telerik:RadDatePicker>
                <br />
                To:
                <telerik:RadDatePicker ID="RadDatePicker_ReturnDateEndDate" runat="server" Skin="Telerik"
                    Width="120px">
                    <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                        ViewSelectorText="x">
                    </Calendar>
                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                    </DateInput>
                </telerik:RadDatePicker>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Reship?:
            </td>
            <td class="jobInfo_info">
                <asp:RadioButtonList runat="server" ID="RadioButtonList_Reship" RepeatDirection="Vertical">
                    <asp:ListItem Text="All" Selected="True" Value="-1"></asp:ListItem>
                    <asp:ListItem Text="Reship" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Not Reship" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Return Reason:
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:ListBox runat="server" Rows="5" SelectionMode="Multiple" ID="ListBox_Reason"
                    DataTextField="ReasonFull" DataValueField="ReasonID"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Address:
            </td>
            <td class="jobInfo_info">
                <telerik:RadTextBox Width="100px" runat="server" ID="RadTextBox_Address">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                City:
            </td>
            <td class="jobInfo_info">
                <telerik:RadTextBox Width="100px" runat="server" ID="RadTextBox_City">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Zip:
            </td>
            <td class="jobInfo_info">
                <telerik:RadTextBox Width="100px" runat="server" ID="RadTextBox_ZipCode">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                StatusCode:
            </td>
            <td class="jobInfo_info">
                <telerik:RadTextBox Width="100px" runat="server" ID="RadTextBox_FedExStatusCode">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">
                Shipper Notes:
            </td>
            <td class="jobInfo_info">
                <telerik:RadTextBox Width="100px" runat="server" ID="RadTextBox_FedExLastUpdateStatus">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button runat="server" ID="Button1" Text="Search" Width="80px" CssClass="ptButton"
                    OnClick="Button_Filter_Click" />
                <asp:Button runat="server" ID="Button2" Text="Clear" Width="80px" CssClass="ptButton"
                    OnClick="Button_Clear_Click" />
            </td>
        </tr>
    </table>
</div>
