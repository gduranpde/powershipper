﻿using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class CompanyCard : UserControl
    {
        private readonly UserManagementPresenter presenter = new UserManagementPresenter();

        public Guid CompanyId
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_Id.Value))
                           ? new Guid(HiddenField_Id.Value)
                           : Guid.Empty;
            }
            set { HiddenField_Id.Value = value.ToString(); }
        }

        public object DataItem { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {
            tr_CompanyLogo.Visible = false;
        }

        protected override void OnInit(EventArgs e)
        {
            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');");

            InitializeComponent();
            base.OnInit(e);
        }

        protected void UploadButton_Click(object sender, EventArgs e)
        {
            foreach (string fileInputID in Request.Files)
            {
                UploadedFile file = UploadedFile.FromHttpPostedFile(Request.Files[fileInputID]);
                if (file.ContentLength > 0)
                {
                    if (CompanyId == Guid.Empty)
                        CompanyId = Guid.NewGuid();
                    
                    var path = Path.Combine(ConfigurationManager.AppSettings["LogoFolder"], "Comapny_" + CompanyId + ".jpg");
                    //var path = Path.Combine(ConfigurationManager.AppSettings["LogoFolder"], "logo.jpg");
                    file.SaveAs(path);

                    RadBinaryImage_Logo.ImageUrl = "~/Logo/Comapny_" + CompanyId + ".jpg";
                    //RadBinaryImage_Logo.ImageUrl = "~/Logo/" + "logo.jpg";
                    RadTextBoxLogoUrl.Text = RadBinaryImage_Logo.ImageUrl;
                }
            }
        }


        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                Company c = presenter.GetCompanyById(CompanyId);
                presenter.DeleteCompany(c);
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to delete Company. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;

                Helper.ErrorInGrid("RadGrid_Companies", Page, l);
            }
        }

        

        private void InitializeComponent()
        {
            DataBinding += Init_DataBinding;
        }

        private void Init_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        public void InitData()
        {
            InitDropDowns();

            if ((DataItem != null) & (DataItem.GetType() != typeof (GridInsertionObject)))
            {
                var entity = (Company) DataItem;

                RadTextBox_CompanyName.Text = entity.CompanyName;
                RadTextBox_CompanyCode.Text = entity.CompanyCode;
                RadBinaryImage_Logo.ImageUrl = entity.CompanyLogo;
                RadTextBoxLogoUrl.Text = entity.CompanyLogo;
                CheckBox_IsActive.Checked = entity.IsActive;

                CompanyId = entity.Id;
            }
        }

        public Company FillEntity()
        {
            var entity = new Company();
            if (CompanyId != Guid.Empty)
            {
                entity.Id = CompanyId;
            }
            else
            {
                entity.Id = Guid.NewGuid();
            }

            entity.CompanyName = RadTextBox_CompanyName.Text;
            entity.CompanyCode = RadTextBox_CompanyCode.Text;
            entity.CompanyLogo = RadTextBoxLogoUrl.Text;
            entity.IsActive = CheckBox_IsActive.Checked;

            return entity;
        }

        private void InitDropDowns()
        {
        }
    }
}