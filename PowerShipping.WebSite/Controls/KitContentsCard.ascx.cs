﻿using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using System;
using System.Drawing;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class KitContentsCard : System.Web.UI.UserControl
    {
        private readonly KitTypesPresenter presenter = new KitTypesPresenter();

        public Guid KitContentID
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_Id.Value))
                           ? new Guid(HiddenField_Id.Value)
                           : Guid.Empty;
            }
            set { HiddenField_Id.Value = value.ToString(); }
        }

        public object DataItem { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnInit(EventArgs e)
        {
            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');");

            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
            DataBinding += Init_DataBinding;
        }

        private void Init_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        public void InitData()
        {
            InitDropDowns();            
        }

        protected override void OnPreRender(EventArgs e)
        {
            if ((DataItem != null) && (DataItem.GetType() != typeof(GridInsertionObject)))
            {
                var entity = (KitContent)DataItem;

                RadTextBox_ItemDescription.Text = entity.ItemDescription;
                RadNumericTextBox_Quantity.Text = entity.Quantity.ToString();
                RadTextBox_Kw.Text = entity.DefaultKwImpact.ToString();
                RadTextBox_Kwh.Text = entity.DefaultKwhImpact.ToString();
                KitContentID = entity.KitContentID;
                cmbItems.SelectedValue = entity.ItemID.ToString();

                btnUpdate.Focus();
            }
        }

        public KitContent FillEntity()
        {
            var entity = new KitContent();
            if (KitContentID != Guid.Empty)
            {
                entity.KitContentID = KitContentID;
            }
            else
            {
                entity.KitContentID = Guid.NewGuid();
            }
            entity.ItemName = cmbItems.Text;
            entity.Quantity = Convert.ToInt32(RadNumericTextBox_Quantity.Value);
            entity.ItemID = new Guid(cmbItems.SelectedValue);

            return entity;
        }

        private void InitDropDowns()
        {
            cmbItems.DataSource = presenter.GetAllItemNames();
            cmbItems.DataBind();
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                var kitContentItem = presenter.GetKitContentById(KitContentID);                
                presenter.DeleteKitContentItemById(KitContentID, kitContentItem.KitID, kitContentItem.ItemID);      
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to delete Content Item. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                Helper.ErrorInGrid("RadGrid_Items", Page, l);
            }
        }

        protected void cmbItems_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            var itemsPresenter = new ItemsSetupPresenter();
            var item = itemsPresenter.GetItemById(new Guid(e.Value));

            RadTextBox_ItemDescription.Text = item.ItemDescription;
            RadTextBox_Kw.Text = item.DefaultKwImpact.ToString();
            RadTextBox_Kwh.Text = item.DefaultKwhImpact.ToString();
        } 
    }
}