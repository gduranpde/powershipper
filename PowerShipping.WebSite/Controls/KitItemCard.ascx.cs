﻿using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class KitItemCard : UserControl
    {
        private readonly UserManagementPresenter presenter = new UserManagementPresenter();

        public Guid KitItemId
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_Id.Value))
                           ? new Guid(HiddenField_Id.Value)
                           : Guid.Empty;
            }
            set { HiddenField_Id.Value = value.ToString(); }
        }

        public object DataItem { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnInit(EventArgs e)
        {
            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');");

            InitializeComponent();
            base.OnInit(e);
        }


        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                KitItem k = presenter.GetKitItemById(KitItemId);
                presenter.DeleteKitItem(k);
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to delete KitItem. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                //var grid =
                //    (Page.Controls[0].Controls[3].Controls[21].FindControl("RadGrid_Items") as RadGrid);
                //grid.Controls.Add(l);
                Helper.ErrorInGrid("RadGrid_Items", Page, l);
            }
        }

        private void InitializeComponent()
        {
            DataBinding += Init_DataBinding;
        }

        private void Init_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        public void InitData()
        {
            InitDropDowns();

            if ((DataItem != null) & (DataItem.GetType() != typeof (GridInsertionObject)))
            {
                var entity = (KitItem) DataItem;

                RadTextBox_Name.Text = entity.Name;
                RadTextBox_CatalogId.Text = entity.CatalogID;
                RadNumericTextBox_Quantity.Value = entity.Quantity;
                RadNumericTextBox_Kw_Impact.Value = (double)entity.Kw_Impact;
                RadNumericTextBox_Kwh_Impact.Value = (double)entity.Kwh_Impact;

                KitItemId = entity.Id;
            }
        }

        public KitItem FillEntity()
        {
            var entity = new KitItem();
            if (KitItemId != Guid.Empty)
            {
                entity.Id = KitItemId;
            }
            else
            {
                entity.Id = Guid.NewGuid();
            }
            entity.Name = RadTextBox_Name.Text;
            entity.CatalogID = RadTextBox_CatalogId.Text;
            entity.Quantity = Convert.ToInt32(RadNumericTextBox_Quantity.Value);
            entity.Kw_Impact = Convert.ToDecimal(RadNumericTextBox_Kw_Impact.Value);
            entity.Kwh_Impact = Convert.ToDecimal(RadNumericTextBox_Kwh_Impact.Value);

            return entity;
        }

        private void InitDropDowns()
        {
        }
    }
}