﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;
using System.Linq;
using System.Linq.Expressions;

namespace PowerShipping.WebSite.Controls
{
    public partial class ConsumerRequestList : System.Web.UI.UserControl
    {
        ConsumerRequestsPresenter presenter = new ConsumerRequestsPresenter();
        Sorter<ConsumerRequest> sorter = new Sorter<ConsumerRequest>();

        public delegate void ConsumerRequestListHandler(Guid projectId);
        public event ConsumerRequestListHandler OnNext;

        protected void Page_Load(object sender, EventArgs e)
        {
            //if(!IsPostBack)
            //{
            //    RadTextBox_CountToSelect.Text = "100";
            //}
            ProjectsList1.OnProjectChanged += ProjectsList1_OnProjectChanged;

            ViewState["RadTextBox_CountToSelect"] = RadTextBox_CountToSelect.Text;
        }

        void ProjectsList1_OnProjectChanged(string projectId)
        {
            lbErr.Text = "";
            RadGrid_ConsumerRequests.Rebind();
        }

        public void InitData()
        {
            
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            RadGrid_ConsumerRequests.MasterTableView.FilterExpression = string.Empty;

            foreach (GridColumn column in RadGrid_ConsumerRequests.MasterTableView.RenderColumns)
            {
                if (column is GridBoundColumn)
                {
                    GridBoundColumn boundColumn = column as GridBoundColumn;
                    boundColumn.CurrentFilterValue = string.Empty;
                }
            }
            RadTextBox_CountToSelect.Text = "100";
            RadGrid_ConsumerRequests.MasterTableView.Rebind();
        }
        
        protected void RadGrid_ConsumerRequests_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            int total = 0;
            //List<ConsumerRequest> list = GetFilteredList(ref total, GetOrderClause());
            //List<ConsumerRequest> list = GetFilteredList(ref total, OrderClause);
            List<ConsumerRequest> list = GetFilteredList(ref total, OrderClause, RadGrid_ConsumerRequests.MasterTableView.FilterExpression);
            this.RadGrid_ConsumerRequests.DataSource = list;
            lbMaxResults.Text = "(Max = " + total + ")";
            if (MaxCount == 0)
                MaxCount = total;

            RadTextBox_CountToSelect.Text = total.ToString();
        }

        private string GetOrderClause()
        {
            string orderCluase = string.Empty;

            foreach (GridSortExpression exp in this.RadGrid_ConsumerRequests.MasterTableView.SortExpressions)
            {
                orderCluase += string.Format(" cr.[{0}] {1},", exp.FieldName.ToString(), exp.SortOrderAsString());
            }

            return orderCluase.TrimEnd(new char[] { ',' });
        }

        private void InitCheckBoxes()
        {
            foreach (Telerik.Web.UI.GridDataItem gridItem in RadGrid_ConsumerRequests.Items)
            {
                CheckBox check = (CheckBox)gridItem.FindControl("CheckBox_Choosed");
                if (check != null)
                    check.Checked = true;
            }
        }

        protected void Button_Filter_Click(object sender, EventArgs e)
        {
            lbErr.Text = "";

            if (ProjectsList1.ProjectId == Guid.Empty)
            {
                lbErr.ForeColor = Color.Red;
                lbErr.Text = "Please select project.";
                return;
            }

            if (OrderClause == null || RadGrid_ConsumerRequests.Items.Count == MaxCount)
                OrderClause = GetOrderClause();

            RadGrid_ConsumerRequests.Rebind();

            if (OnNext != null)
                OnNext(ProjectsList1.ProjectId);
         }

        public List<Guid> GetListOfSelectedItems()
        {
            List<Guid> listOfSelected = new List<Guid>();
            //foreach (Telerik.Web.UI.GridDataItem gridItem in RadGrid_ConsumerRequests.Items)
            //{
            //    CheckBox check = (CheckBox)gridItem.FindControl("CheckBox_Choosed");
            //    if (check != null)
            //        if (check.Checked)
            //            listOfSelected.Add((Guid)gridItem.GetDataKeyValue("Id"));
            //}
            int total = 0;
            //List<ConsumerRequest> selected = GetFilteredList(ref total, GetOrderClause());


            List<ConsumerRequest> selected = GetFilteredList(ref total, OrderClause, RadGrid_ConsumerRequests.MasterTableView.FilterExpression);
           
            //List<ConsumerRequest> selected = new List<ConsumerRequest>();
            //foreach(GridDataItem gdi in RadGrid_ConsumerRequests.MasterTableView.Items)
            //{
            //    selected.Add((ConsumerRequest)gdi.DataItem);
            //}

            if (!string.IsNullOrEmpty(RadGrid_ConsumerRequests.MasterTableView.SortExpressions.GetSortString()))
            {
                sorter.SortString = RadGrid_ConsumerRequests.MasterTableView.SortExpressions.GetSortString();
                selected.Sort(sorter);
            }

            int count = Convert.ToInt32(ViewState["RadTextBox_CountToSelect"]);

            foreach (var cr in selected)
            {
                if(count < 1)
                    break;

                listOfSelected.Add(cr.Id);
                count--;
            }
            return listOfSelected;
        }

        protected void RadGrid_ConsumerRequests_DataBound(object sender, EventArgs e)
        {
            InitCheckBoxes();

            //ViewState["RadTextBox_CountToSelect"] = RadTextBox_CountToSelect.Text;
            //RadTextBox_CountToSelect.Text = RadGrid_ConsumerRequests.Items.Count.ToString();
            //lbMaxResults.Text = "(Max = " + RadGrid_ConsumerRequests.Items.Count + ")";
        }

        private List<ConsumerRequest> GetFilteredList(ref int total, string orderClause, string filerExpr)
        {
            int count = 0;
            try
            {
                count = Convert.ToInt32(this.RadTextBox_CountToSelect.Text);
            }
            catch
            {
                count = 0;
            }
           
            
            Guid? projectId = null;
            if (ProjectsList1.ProjectId != Guid.Empty)
            {
                projectId = ProjectsList1.ProjectId;
            }

            return presenter.GetAllByQuantity(0, null, null, null, null, null, ref total, orderClause, projectId, filerExpr);
        }

        public string OrderClause
        {
            get { return (ViewState["OrderClause"] != null) ? (string)ViewState["OrderClause"] : null; }
            set { ViewState["OrderClause"] = value; }
        }

        public int MaxCount
        {
            get { return (ViewState["MaxCount"] != null) ? (int)ViewState["MaxCount"] : 0; }
            set { ViewState["MaxCount"] = value; }
        }

        private bool FilterListCRs(ConsumerRequest cr)
        {
            if(cr.AccountNumber != null && cr.AccountNumber.ToString().ToUpper().Contains("3".ToUpper()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}