﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShipmentEditInfo.ascx.cs"
    Inherits="PowerShipping.WebSite.Controls.ShipmentEditInfo" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:HiddenField ID="HiddenField_ShipmentId" runat="server" />
<asp:HiddenField ID="HiddenField_ConsumerRequestId" runat="server" />
<table>
    <tr>
        <td id="tr_PurchaseOrderNumber" runat="server">
            PO #
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_PurchaseOrderNumber" runat="server" Width="250px">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr runat="server" id="tr_Shipper" visible="true">
        <td>
            Shipper
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBox_Shipper" runat="server" DataValueField="Id"
                DataTextField="FullName" Width="250px">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr id="tr_FedExTrackingNumber" runat="server">
        <td>
            Tracking Number
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_FedExTrackingNumber" runat="server" Width="250px">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_FedExStatusCode" runat="server">
        <td>
            Shipper Status Code
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_FedExStatusCode" runat="server" Width="250px">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr runat="server" id="tr_AccountNumber" visible="false">
        <td>
            Account Number
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_AccountNumber" runat="server" Width="250px">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr runat="server" id="tr_ShippingNumber" visible="false">
        <td>
            Shipping Number
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_ShippingNumber" runat="server" Width="250px">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr runat="server" id="tr_KitType" visible="false">
        <td>
            Kit Type
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBox_KitTypes" runat="server" DataValueField="Id"
                DataTextField="KitName" Width="250px">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr id="tr_AccountName" runat="server">
        <td>
            Account Name
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_AccountName" runat="server" Width="250px">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_CompanyName" runat="server">
        <td>
            Company Name
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_CompanyName" runat="server" Width="250px">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_Address1" runat="server">
        <td>
            Address1
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_Address1" runat="server" Width="250px">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_Address2" runat="server">
        <td>
            Address2
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_Address2" runat="server" Width="250px"> 
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_City" runat="server">
        <td>
            City
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_City" runat="server" Width="250px">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_State" runat="server">
        <td>
            State
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_State" runat="server" Width="250px">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_ZipCode" runat="server">
        <td>
            Zip
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_ZipCode" runat="server" Width="250px">
            </telerik:RadTextBox>
        </td>
    </tr>
    <%-- <tr runat="server" id="tr_Project">
        <td>Project</td>
        <td><telerik:RadComboBox ID="RadComboBox_Project" runat="server" DataValueField="Id" DataTextField="FullName" Width="300px"></telerik:RadComboBox></td>
    </tr>--%>
    <tr id="tr_Email" runat="server">
        <td>
            Email
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_Email" runat="server" Width="250px">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_Phone1" runat="server">
        <td>
            Phone1
        </td>
        <td>
            <telerik:RadMaskedTextBox ID="RadMaskedTextBox_Phone1" runat="server" Mask="###-###-####"
                Width="250px">
            </telerik:RadMaskedTextBox>
        </td>
    </tr>
    <tr id="tr_Phone2" runat="server">
        <td>
            Phone2
        </td>
        <td>
            <telerik:RadMaskedTextBox ID="RadMaskedTextBox_Phone2" runat="server" Mask="###-###-####"
                Width="250px">
            </telerik:RadMaskedTextBox>
        </td>
    </tr>
    <tr id="tr_ShppingStatus" runat="server">
        <td>
            Shipping Status
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBox_ShppingStatus" runat="server" DataValueField="Value"
                DataTextField="Text" Width="250px">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr id="tr_ReshipStatus" runat="server">
        <td>
            Reship Status
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBox_ReshipStatus" runat="server" DataValueField="Value"
                DataTextField="Text" Width="250px">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr id="tr_StatusDate" runat="server">
        <td>
            Status Date
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDatePicker_StatusDate" runat="server" Skin="Telerik"
                Width="200px">
                <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                    ViewSelectorText="x">
                </Calendar>
                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                </DateInput>
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr id="tr_ShipDate" runat="server">
        <td>
            ShipDate
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDatePicker_ShipDate" runat="server" Skin="Telerik"
                Width="200px">
                <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                    ViewSelectorText="x">
                </Calendar>
                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                </DateInput>
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr id="tr_ReturnDate" runat="server">
        <td>
            ReturnDate
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDatePicker_ReturnDate" runat="server" Skin="Telerik"
                Width="200px">
                <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                    ViewSelectorText="x">
                </Calendar>
                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                </DateInput>
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr id="tr_IsReship" runat="server">
        <td>
            Reship?
        </td>
        <td>
            <asp:CheckBox ID="chbIsReship" runat="server"></asp:CheckBox>
        </td>
    </tr>
    <tr id="tr_Reason" runat="server">
        <td>
            Reason for Return
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBox_Reason" runat="server" DataValueField="ReasonID"
                DataTextField="ReasonFull" Width="250px">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr id="tr_Notes" runat="server">
        <td>
            Notes
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_Notes" runat="server" TextMode="MultiLine" Width="500px"
                Height="50px" MaxLength="1024">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_BatchID" runat="server">
        <td>
            FedEx Batch Import ID
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_BatchID" runat="server" Width="250px" ReadOnly="true">
            </telerik:RadTextBox>
        </td>
    </tr>
    <%-- <tr id="tr_FedExLastUpdateStatus" runat="server">
        <td>Shipping Status</td>
        <td><telerik:RadTextBox ID="RadTextBox_FedExLastUpdateStatus" runat="server"></telerik:RadTextBox></td>
    </tr>--%>
    <tr runat="server" id="td_CommandCell">
        <td colspan="2">
            <asp:LinkButton ID="btnUpdate" Text="Save" runat="server" CommandName="Update" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>'
                ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnInsert" Text="Save" runat="server" CommandName="PerformInsert"
                Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>' ValidationGroup="Card"
                Font-Size="Medium" OnClick="btnInsert_Click"></asp:LinkButton>
            <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False"
                CommandName="Cancel" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnDelete" Text="Delete" runat="server" CausesValidation="False"
                CommandName="Cancel" OnClick="btnDelete_OnClick" Font-Size="Medium"></asp:LinkButton>
        </td>
    </tr>
</table>
