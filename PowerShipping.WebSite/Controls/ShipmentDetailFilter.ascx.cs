﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;
using System.Configuration;

namespace PowerShipping.WebSite.Controls
{
    public partial class ShipmentDetailFilter : System.Web.UI.UserControl
    {
        public delegate void ShipmentDetailFilterHandler(string customQuery);
        public event ShipmentDetailFilterHandler OnFilter;

        public delegate void ShipmentDetailFilterClearHandler(string customQuery);
        public event ShipmentDetailFilterClearHandler OnClear;

        ShipmentDetailsPresenter presenter = new ShipmentDetailsPresenter();
        UserManagementPresenter presenterAdv = new UserManagementPresenter();


        public string AccountNumber { get; set; }
        public Guid? FedExBatchId { get; set; }
        public string DefaultQuery { get; set; }

        public bool FillView
        {
            get
            {
                return chbFullView.Checked;
            }
        }

        public string CurrentQuery
        {
            get
            {
                return BuildQuery();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                FillLists();

            if (FedExBatchId != null)
            {
                DefaultQuery = string.Format("select * from [vw_ShipmentDetail] where 1 = 1 AND [FedExBatchId] = '{0}' ORDER BY [AccountNumber]", FedExBatchId);
                return;
            }

            if (!string.IsNullOrEmpty(AccountNumber))
            {
                RadTextBox_AccountNumber.Text = AccountNumber;
                DefaultQuery = string.Format("select * from [vw_ShipmentDetail] where  1 = 1 AND [AccountNumber] = '{0}' ORDER BY [AccountNumber]", AccountNumber.Trim());

                return;
            }

            DefaultQuery = "select * from [vw_ShipmentDetail] where 1 = 1 ORDER BY [AccountNumber]";
            
        }

        protected void Button_Filter_Click(object sender, EventArgs e)
        {
            string query = BuildQuery();
            OnFilter(query);
        }

        protected void Button_Clear_Click(object sender, EventArgs e)
        {
            Clear();
            string query = BuildQuery();
            OnClear(query);
        }

        private void Clear()
        {
            if (string.IsNullOrEmpty(AccountNumber))
                RadTextBox_AccountNumber.Text = null;

            RadTextBox_ShipmentNumber.Text = null;
            RadTextBox_PurchaseOrderNumber.Text = null;
            RadTextBox_FedExTrackingNumber.Text = null;
            RadTextBox_FedExStatusCode.Text = null;
            RadTextBox_FedExLastUpdateStatus.Text = null;
            RadTextBox_AccountName.Text = null;
            RadTextBox_OperatingCompany.Text = null;
            ListBox_KitTypeName.SelectedValue = null;
            ListBox_ShippingStatus.SelectedValue = null;
            ListBox_ReshipStatus.SelectedValue = null;

            RadDatePicker_ShippingStatusDateStartDate.SelectedDate = null;
            RadDatePicker_ShippingStatusDateEndDate.SelectedDate = null;
            RadDatePicker_ReturnDateStartDate.SelectedDate = null;
            RadDatePicker_ReturnDateEndDate.SelectedDate = null;
            RadDatePicker_ShipDateStartDate.SelectedDate = null;
            RadDatePicker_ShipDateEndDate.SelectedDate = null;
            
            RadioButtonList_Reship.SelectedIndex = 0;

            RadTextBox_CompanyName.Text = null;
            ListBox_Reason.SelectedValue = null;
            RadTextBox_Address.Text = null;
            RadTextBox_City.Text = null;
            RadTextBox_ZipCode.Text = null;
            ListBox_Shipper.SelectedValue = null;
        }

        private string BuildQuery()
        {
            StringBuilder sb = new StringBuilder();
            string maxReturnedRows = ConfigurationManager.AppSettings.Get("MaxReturnedRowNumber");
            sb.AppendFormat(String.Format("select TOP {0} * from [vw_ShipmentDetail] where 1 = 1  ", maxReturnedRows));

            if (!string.IsNullOrEmpty(RadTextBox_AccountNumber.Text.Trim()))
            {
                if (RadTextBox_AccountNumber.Text.Trim() == "null")
                    sb.AppendFormat(" and ([AccountNumber] IS NULL OR [AccountNumber] = '')");
                else
                    sb.AppendFormat("and ([AccountNumber] LIKE '%'+ '{0}' +'%')   ", RadTextBox_AccountNumber.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_AccountName.Text.Trim()))
            {
                if (RadTextBox_AccountName.Text.Trim() == "null")
                    sb.AppendFormat(" and ([AccountName] IS NULL OR [AccountName] = '')");
                else
                    sb.AppendFormat(" and ([AccountName] LIKE '%'+ '{0}' +'%')   ", RadTextBox_AccountName.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_ShipmentNumber.Text.Trim()))
            {
                if (RadTextBox_ShipmentNumber.Text.Trim() == "null")
                    sb.AppendFormat(" and ([ShipmentNumber] IS NULL OR [ShipmentNumber] = '')");
                else
                    sb.AppendFormat(" and ([ShipmentNumber] LIKE '%'+ '{0}' +'%')   ", RadTextBox_ShipmentNumber.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_PurchaseOrderNumber.Text.Trim()))
            {
                if (RadTextBox_PurchaseOrderNumber.Text.Trim() == "null")
                    sb.AppendFormat(" and ([PurchaseOrderNumber] IS NULL OR [PurchaseOrderNumber] = '')");
                else
                    sb.AppendFormat(" and ([PurchaseOrderNumber] LIKE '%'+ '{0}' +'%')   ",RadTextBox_PurchaseOrderNumber.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_FedExTrackingNumber.Text.Trim()))
            {
                if (RadTextBox_FedExTrackingNumber.Text.Trim() == "null")
                    sb.AppendFormat(" and ([FedExTrackingNumber] IS NULL OR [FedExTrackingNumber] = '')");
                else
                    sb.AppendFormat(" and ([FedExTrackingNumber] LIKE '%'+ '{0}' +'%')   ",RadTextBox_FedExTrackingNumber.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_OperatingCompany.Text.Trim()))
            {
                if (RadTextBox_OperatingCompany.Text.Trim() == "null")
                    sb.AppendFormat(" and ([OperatingCompany] IS NULL OR [OperatingCompany] = '')");
                else
                    sb.AppendFormat(" and ([OperatingCompany] LIKE '%'+ '{0}' +'%')   ", RadTextBox_OperatingCompany.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_FedExStatusCode.Text.Trim()))
            {
                if (RadTextBox_FedExStatusCode.Text.Trim() == "null")
                    sb.AppendFormat(" and ([FedExStatusCode] IS NULL OR [FedExStatusCode] = '')");
                else
                    sb.AppendFormat(" and ([FedExStatusCode] LIKE '%'+ '{0}' +'%')   ", RadTextBox_FedExStatusCode.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_FedExLastUpdateStatus.Text.Trim()))
            {
                if (RadTextBox_FedExLastUpdateStatus.Text.Trim() == "null")
                    sb.AppendFormat(" and ([FedExLastUpdateStatus] IS NULL OR [FedExLastUpdateStatus] = '')");
                else
                    sb.AppendFormat(" and ([FedExLastUpdateStatus] LIKE '%'+ '{0}' +'%')   ",RadTextBox_FedExLastUpdateStatus.Text);
            }


            if (!string.IsNullOrEmpty(RadTextBox_CompanyName.Text.Trim()))
            {
                if (RadTextBox_CompanyName.Text.Trim() == "null")
                    sb.AppendFormat(" and ([CompanyName] IS NULL OR [CompanyName] = '')");
                else
                    sb.AppendFormat(" and ([CompanyName] LIKE '%'+ '{0}' +'%')   ", RadTextBox_CompanyName.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_Address.Text.Trim()))
            {
                if (RadTextBox_Address.Text.Trim() == "null")
                    sb.AppendFormat(" and ([ShipmentDetailAddress1] IS NULL OR [ShipmentDetailAddress1] = '')");
                else
                    sb.AppendFormat(" and ([ShipmentDetailAddress1] LIKE '%'+ '{0}' +'%')   ", RadTextBox_Address.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_City.Text.Trim()))
            {
                if (RadTextBox_City.Text.Trim() == "null")
                    sb.AppendFormat(" and ([ShipmentDetailCity] IS NULL OR [ShipmentDetailCity] = '')");
                else
                    sb.AppendFormat(" and ([ShipmentDetailCity] LIKE '%'+ '{0}' +'%')   ", RadTextBox_City.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_ZipCode.Text.Trim()))
            {
                if (RadTextBox_ZipCode.Text.Trim() == "null")
                    sb.AppendFormat(" and ([ShipmentDetailZip] IS NULL OR [ShipmentDetailZip] = '')");
                else
                    sb.AppendFormat(" and ([ShipmentDetailZip] LIKE '%'+ '{0}' +'%')   ", RadTextBox_ZipCode.Text);
            }
            string selectedRangeReason = string.Empty;
            foreach (ListItem item in ListBox_Reason.Items)
            {
                if (item.Selected)
                {
                    selectedRangeReason += string.Format("'{0}',", item.Value);
                }
            }
            if (!string.IsNullOrEmpty(selectedRangeReason))
            {
                selectedRangeReason = selectedRangeReason.TrimEnd(',');
                sb.AppendFormat(" and ([ReasonId] in ({0}))   ", selectedRangeReason);
            }



            string selectedRangeKitTypeName = string.Empty;
            foreach (ListItem item in ListBox_KitTypeName.Items)
            {
                if (item.Selected)
                {
                    selectedRangeKitTypeName += string.Format("'{0}',", item.Value);
                }
            }
            if (!string.IsNullOrEmpty(selectedRangeKitTypeName))
            {
                selectedRangeKitTypeName = selectedRangeKitTypeName.TrimEnd(',');
                sb.AppendFormat(" and ([KitTypeId] in ({0}))   ", selectedRangeKitTypeName);
            }


            string selectedRangeShippingStatus = string.Empty;
            foreach (ListItem item in ListBox_ShippingStatus.Items)
            {
                if (item.Selected)
                {
                    selectedRangeShippingStatus += string.Format("'{0}',", item.Value);
                }
            }
            if (!string.IsNullOrEmpty(selectedRangeShippingStatus))
            {
                selectedRangeShippingStatus = selectedRangeShippingStatus.TrimEnd(',');
                sb.AppendFormat(" and ([ShippingStatus] in ({0}))   ", selectedRangeShippingStatus);
            }

            string selectedRangeReshipStatus = string.Empty;
            foreach (ListItem item in ListBox_ReshipStatus.Items)
            {
                if (item.Selected)
                {
                    selectedRangeReshipStatus += string.Format("'{0}',", item.Value);
                }
            }
            if (!string.IsNullOrEmpty(selectedRangeReshipStatus))
            {
                selectedRangeReshipStatus = selectedRangeReshipStatus.TrimEnd(',');
                sb.AppendFormat(" and ([ReshipStatus] in ({0}))   ", selectedRangeReshipStatus);
            }




            if (this.RadioButtonList_Reship.SelectedValue != "-1")
            {
                sb.AppendFormat(" and ([IsReship] = '{0}')   ", this.RadioButtonList_Reship.SelectedValue);
            }

            if (this.RadDatePicker_ShippingStatusDateEndDate.SelectedDate.HasValue || this.RadDatePicker_ShippingStatusDateStartDate.SelectedDate.HasValue)
            {
                sb.AppendFormat(" and ( ");
                if (this.RadDatePicker_ShippingStatusDateStartDate.SelectedDate.HasValue)
                    sb.AppendFormat("[ShippingStatusDate] >= '{0}'", this.RadDatePicker_ShippingStatusDateStartDate.SelectedDate.Value.ToString("MM/dd/yyyy"));
                if (this.RadDatePicker_ShippingStatusDateEndDate.SelectedDate.HasValue & this.RadDatePicker_ShippingStatusDateStartDate.SelectedDate.HasValue)
                    sb.AppendFormat(" and ");
                if (this.RadDatePicker_ShippingStatusDateEndDate.SelectedDate.HasValue)
                    sb.AppendFormat("[ShippingStatusDate] < '{0}'", this.RadDatePicker_ShippingStatusDateEndDate.SelectedDate.Value.AddDays(1).ToString("MM/dd/yyyy"));
                sb.AppendFormat(" )   ");
            }

            if (this.RadDatePicker_ShipDateEndDate.SelectedDate.HasValue || this.RadDatePicker_ShipDateStartDate.SelectedDate.HasValue)
            {
                sb.AppendFormat(" and ( ");
                if (this.RadDatePicker_ShipDateStartDate.SelectedDate.HasValue)
                    sb.AppendFormat("[ShipDate] >= '{0}'", this.RadDatePicker_ShipDateStartDate.SelectedDate.Value.ToString("MM/dd/yyyy"));
                if (this.RadDatePicker_ShipDateEndDate.SelectedDate.HasValue & this.RadDatePicker_ShipDateStartDate.SelectedDate.HasValue)
                    sb.AppendFormat(" and ");
                if (this.RadDatePicker_ShipDateEndDate.SelectedDate.HasValue)
                    sb.AppendFormat("[ShipDate] < '{0}'", this.RadDatePicker_ShipDateEndDate.SelectedDate.Value.AddDays(1).ToString("MM/dd/yyyy"));
                sb.AppendFormat(" )   ");
            }
            if (this.RadDatePicker_ReturnDateEndDate.SelectedDate.HasValue || this.RadDatePicker_ReturnDateStartDate.SelectedDate.HasValue)
            {
                sb.AppendFormat(" and ( ");
                if (this.RadDatePicker_ReturnDateStartDate.SelectedDate.HasValue)
                    sb.AppendFormat("[ReturnDate] >= '{0}'", this.RadDatePicker_ReturnDateStartDate.SelectedDate.Value.ToString("MM/dd/yyyy"));
                if (this.RadDatePicker_ReturnDateEndDate.SelectedDate.HasValue & this.RadDatePicker_ReturnDateStartDate.SelectedDate.HasValue)
                    sb.AppendFormat(" and ");
                if (this.RadDatePicker_ReturnDateEndDate.SelectedDate.HasValue)
                    sb.AppendFormat("[ReturnDate] < '{0}'", this.RadDatePicker_ReturnDateEndDate.SelectedDate.Value.AddDays(1).ToString("MM/dd/yyyy"));
                sb.AppendFormat(" )   ");
            }

            string selectedRangeShipper = string.Empty;
            foreach (ListItem item in ListBox_Shipper.Items)
            {
                if (item.Selected)
                {
                    selectedRangeShipper += string.Format("'{0}',", item.Value);
                }
            }
            if (!string.IsNullOrEmpty(selectedRangeShipper))
            {
                selectedRangeShipper = selectedRangeShipper.TrimEnd(',');
                sb.AppendFormat(" and ([ShipperId] in ({0}))   ", selectedRangeShipper);
            }



            if (FedExBatchId != null)
                sb.AppendFormat(" and ([FedExBatchId] = '{0}')  ", FedExBatchId);

            sb.AppendFormat(" ORDER BY [AccountNumber]");

            return sb.ToString();

        }

        private void FillLists()
        {
            ListBox_KitTypeName.DataSource = presenter.GetAllKitTypes();
            ListBox_KitTypeName.DataBind();

            ListBox_ShippingStatus.DataSource = EnumUtils.EnumToListItems(typeof(ShipmentShippingStatus));
            ListBox_ShippingStatus.DataBind();

            ListBox_ReshipStatus.DataSource = EnumUtils.EnumToListItems(typeof(ShipmentReshipStatus));
            ListBox_ReshipStatus.DataBind();

            ListBox_Shipper.DataSource = presenterAdv.GetAllShipers();
            ListBox_Shipper.DataBind();

            ListBox_Reason.DataSource = presenter.GetAllReasons();
            ListBox_Reason.DataBind();
        }
    }
}