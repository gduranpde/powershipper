﻿using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PowerShipping.WebSite.Controls
{
    public partial class KitSetupFilter : System.Web.UI.UserControl
    {
        public delegate void KitSetupFilterHandler(List<KitType> kitTypes);
        public event KitSetupFilterHandler OnFilter;

        public delegate void KitSetupFilterClearHandler();
        public event KitSetupFilterClearHandler OnClear;

        KitTypesPresenter presenter = new KitTypesPresenter();

        public bool IsActive
        {
            get
            {
                return chkIsActive.Checked;
            }
        }

        protected void Button_Filter_Click(object sender, EventArgs e)
        {
            FilterData();
        }

        private void FilterData()
        {
            var kitTypes = presenter.GetKitTypeByKitName(null);
            kitTypes = IsActive ?
                kitTypes.Where(p => p.IsActive).ToList() :
                kitTypes.Where(p => !p.IsActive).ToList();
            if (!string.IsNullOrEmpty(txtKitName.Text))
            {
                kitTypes = kitTypes.Where(p => p.KitName.ToLower().Contains(txtKitName.Text.ToLower())).ToList();
            }
            OnFilter(kitTypes);
        }

        protected void Button_Clear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            chkIsActive.Checked = true;
            txtKitName.Text = string.Empty;
            // raise event to clear filter on page and get all projects back
            OnClear();
        }
    }
}