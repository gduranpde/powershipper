﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class ConsumerRequestCard : UserControl
    {
        private object _dataItem = null;
        private ConsumerRequestsPresenter presenter = new ConsumerRequestsPresenter();
        private ShipmentDetailsPresenter presenterSD = new ShipmentDetailsPresenter();
        private UserManagementPresenter presenterAdv = new UserManagementPresenter();

        public Guid ConsumerRequestId
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_Id.Value))
                           ? new Guid(HiddenField_Id.Value)
                           : Guid.Empty;
            }
            set { HiddenField_Id.Value = value.ToString(); }
        }

        public object DataItem
        {
            get { return _dataItem; }
            set { _dataItem = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!Common.IsCanEdit())
            {
                btnUpdate.Visible = false;
                btnInsert.Visible = false;
                btnDelete.Visible = false;
            }

            base.OnPreRender(e);
        }

        protected override void OnInit(EventArgs e)
        {
            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');");

            InitializeComponent();
            base.OnInit(e);
        }

        protected void lnkBtnShipmentDet_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("ShipmentDetails.aspx?AccountNum=" + RadTextBox_AccountNumber.Text);
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                //throw new Exception("fdgdgdgdgdfgd");
                ConsumerRequest cr = presenter.GetById(ConsumerRequestId);
                List<ShipmentDetail> list = presenterSD.GetByAccountNumber(cr.AccountNumber);

                if (list.Count == 0)
                {
                    presenter.Delete(cr.Id);
                }
                else
                {
                    //var l = new Label();
                    //l.Text = "Unable to delete ConsumerRequest " + cr.AccountNumber + ". Reason: Consumer Request has shipments";
                    //l.ControlStyle.ForeColor = Color.Red;
                    //Helper.ErrorInGrid("RadGrid_ConsumerRequests", Page, l);

                    Helper.ErrorOnPagePopUp(Page, "lbPopUp", "mpe", "Unable to delete ConsumerRequest " + cr.AccountNumber + ". Reason: Consumer Request has shipments");
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                //var l = new Label();
                //l.Text = "Unable to delete ConsumerRequest. Reason: " + ex.Message;
                //l.ControlStyle.ForeColor = Color.Red;
                //Helper.ErrorInGrid("RadGrid_ConsumerRequests", Page, l);

                Helper.ErrorOnPagePopUp(Page, "lbPopUp", "mpe", "Unable to delete ConsumerRequest. Reason: " + ex.Message);
            }
        }

        private void InitializeComponent()
        {
            DataBinding += new EventHandler(Init_DataBinding);
        }

        private void Init_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        public void InitData()
        {
            InitDropDowns();

            if ((DataItem != null) & (DataItem.GetType() != typeof(GridInsertionObject)))
            {
                var entity = (ConsumerRequest)DataItem;

                RadTextBox_AccountNumber.Text = entity.AccountNumber;
                RadTextBox_AccountName.Text = entity.AccountName;
                RadTextBox_Address1.Text = entity.Address1;
                RadTextBox_Address2.Text = entity.Address2;
                RadTextBox_City.Text = entity.City;
                RadTextBox_State.Text = entity.State;
                RadTextBox_ZipCode.Text = entity.ZipCode;
                RadTextBox_Email.Text = entity.Email;
                RadMaskedTextBox_Phone1.Text = EnumUtils.GetPhoneDigits(entity.Phone1);
                RadMaskedTextBox_Phone2.Text = EnumUtils.GetPhoneDigits(entity.Phone2);

                RadTextBox_CompanyName.Text = entity.CompanyName;
                RadTextBox_ServiceAddress1.Text = entity.ServiceAddress1;
                RadTextBox_ServiceAddress2.Text = entity.ServiceAddress2;
                RadTextBox_ServiceCity.Text = entity.ServiceCity;
                RadTextBox_ServiceState.Text = entity.ServiceState;
                RadTextBox_ServiceZip.Text = entity.ServiceZip;

                RadTextBox_Method.Text = entity.Method;

                CheckBox_IsReship.Checked = entity.IsReship;
                CheckBox_IsOptOut.Checked = entity.DoNotShip;
                if (entity.IsOkayToContact != null)
                    CheckBox_IsContact.Checked = (bool)entity.IsOkayToContact;

                RadDatePicker_AnalysisDate.SelectedDate = entity.AnalysisDate;
                RadDatePicker_ReceiptDate.SelectedDate = entity.ReceiptDate;
                RadDatePicker_ImportedDate.SelectedDate = entity.CRImportedDate;
                RadDatePicker_AuditFailureDate.SelectedDate = entity.AuditFailureDate;

                RadTextBox_WaterHeaterFuel.Text = entity.WaterHeaterFuel;
                RadTextBox_HeaterFuel.Text = entity.HeaterFuel;
                //PG31 
                RadTextBox_FacilityType.Text = entity.FacilityType;
                RadTextBox_IncomeQualified.Text = entity.IncomeQualified;
                RadTextBox_RateCode.Text = entity.RateCode;
                //PG31 
                RadTextBox_OperatingCompany.Text = entity.OperatingCompany;

                RadTextBox_Notes.Text = entity.Notes;

                RadComboBox_Status.SelectedValue = Convert.ToInt32(entity.Status).ToString();
                CheckBox_OutOfState.Checked = entity.OutOfState;

                RadTextBox_BatchId.Text = entity.BatchLabel;

                RadNumericTextBox_Quantity.Value = entity.Quantity;
                RadTextBox_PremiseID.Text = entity.PremiseID;

                RadComboBox_Project.SelectedValue = entity.ProjectId.ToString();

                RadTextBox_AccountNumberOld.Text = entity.AccountNumberOld;
                RadTextBox_RequestedKit.Text = entity.RequestedKit;

                ConsumerRequestId = entity.Id;
                SetVisibility(true);
            }
            else
            {
                SetVisibility(false);
            }
        }

        public ConsumerRequest FillEntity()
        {
            var entity = new ConsumerRequest();

            if (ConsumerRequestId != Guid.Empty)
            {
                entity.AccountNumber = RadTextBox_AccountNumber.Text;
                entity.AccountName = RadTextBox_AccountName.Text.ToUpperInvariant();
                entity.Address1 = RadTextBox_Address1.Text.ToUpperInvariant();
                entity.Address2 = RadTextBox_Address2.Text.ToUpperInvariant();
                entity.City = RadTextBox_City.Text.ToUpperInvariant();
                entity.State = RadTextBox_State.Text.ToUpperInvariant();
                entity.ZipCode = RadTextBox_ZipCode.Text;
                entity.Email = RadTextBox_Email.Text;
                entity.Phone1 = EnumUtils.GetPhoneDigits(RadMaskedTextBox_Phone1.Text);
                entity.Phone2 = EnumUtils.GetPhoneDigits(RadMaskedTextBox_Phone2.Text);

                entity.CompanyName = RadTextBox_CompanyName.Text.ToUpperInvariant();
                entity.ServiceAddress1 = RadTextBox_ServiceAddress1.Text.ToUpperInvariant();
                entity.ServiceAddress2 = RadTextBox_ServiceAddress2.Text.ToUpperInvariant();
                entity.ServiceCity = RadTextBox_ServiceCity.Text.ToUpperInvariant();
                entity.ServiceState = RadTextBox_ServiceState.Text.ToUpperInvariant();
                entity.ServiceZip = RadTextBox_ServiceZip.Text;

                entity.Method = RadTextBox_Method.Text;

                entity.IsReship = CheckBox_IsReship.Checked;
                entity.DoNotShip = CheckBox_IsOptOut.Checked;
                entity.IsOkayToContact = CheckBox_IsContact.Checked;

                entity.AnalysisDate = RadDatePicker_AnalysisDate.SelectedDate;
                entity.ReceiptDate = RadDatePicker_ReceiptDate.SelectedDate;
                entity.CRImportedDate = RadDatePicker_ImportedDate.SelectedDate;
                entity.AuditFailureDate = RadDatePicker_AuditFailureDate.SelectedDate;

                entity.WaterHeaterFuel = RadTextBox_WaterHeaterFuel.Text;
                entity.HeaterFuel = RadTextBox_HeaterFuel.Text;
                entity.OperatingCompany = RadTextBox_OperatingCompany.Text;

                entity.Notes = RadTextBox_Notes.Text;

                entity.Status = (ConsumerRequestStatus)Convert.ToInt32(RadComboBox_Status.SelectedValue);
                //PG31
                entity.RateCode = RadTextBox_RateCode.Text;
                entity.FacilityType = RadTextBox_FacilityType.Text;
                entity.IncomeQualified = RadTextBox_IncomeQualified.Text;
                //PG31 
                
                entity.OutOfState = CheckBox_OutOfState.Checked;

                entity.Quantity = Convert.ToInt32(RadNumericTextBox_Quantity.Value);
                entity.PremiseID = RadTextBox_PremiseID.Text;

                entity.AccountNumberOld = RadTextBox_AccountNumberOld.Text;
                entity.RequestedKit = RadTextBox_RequestedKit.Text;

                //entity.BatchLabel =RadTextBox_BatchId.Text;

                entity.Id = ConsumerRequestId;
            }
            else
            {
                entity.AccountNumber = RadTextBox_AccountNumber.Text;
                entity.AccountName = RadTextBox_AccountName.Text.ToUpperInvariant();
                entity.Address1 = RadTextBox_Address1.Text.ToUpperInvariant();
                entity.Address2 = RadTextBox_Address2.Text.ToUpperInvariant();
                entity.City = RadTextBox_City.Text.ToUpperInvariant();
                entity.State = RadTextBox_State.Text.ToUpperInvariant();
                entity.ZipCode = RadTextBox_ZipCode.Text;
                entity.Email = RadTextBox_Email.Text;
                entity.Phone1 = EnumUtils.GetPhoneDigits(RadMaskedTextBox_Phone1.Text);
                entity.Phone2 = EnumUtils.GetPhoneDigits(RadMaskedTextBox_Phone2.Text);

                entity.CompanyName = RadTextBox_CompanyName.Text.ToUpperInvariant();
                entity.ServiceAddress1 = RadTextBox_ServiceAddress1.Text.ToUpperInvariant();
                entity.ServiceAddress2 = RadTextBox_ServiceAddress2.Text.ToUpperInvariant();
                entity.ServiceCity = RadTextBox_ServiceCity.Text.ToUpperInvariant();
                entity.ServiceState = RadTextBox_ServiceState.Text.ToUpperInvariant();
                entity.ServiceZip = RadTextBox_ServiceZip.Text;

                entity.Method = RadTextBox_Method.Text;

                entity.IsReship = CheckBox_IsReship.Checked;
                entity.DoNotShip = CheckBox_IsOptOut.Checked;
                entity.IsOkayToContact = CheckBox_IsContact.Checked;

                entity.AnalysisDate = RadDatePicker_AnalysisDate.SelectedDate;
                entity.ReceiptDate = RadDatePicker_ReceiptDate.SelectedDate;
                entity.CRImportedDate = RadDatePicker_ImportedDate.SelectedDate;

                if (entity.CRImportedDate == null)
                    entity.CRImportedDate = DateTime.Now;

                entity.AuditFailureDate = RadDatePicker_AuditFailureDate.SelectedDate;

                entity.WaterHeaterFuel = RadTextBox_WaterHeaterFuel.Text;
                entity.HeaterFuel = RadTextBox_HeaterFuel.Text;
                entity.OperatingCompany = RadTextBox_OperatingCompany.Text;

                entity.Notes = RadTextBox_Notes.Text;

                entity.Status = (ConsumerRequestStatus)Convert.ToInt32(RadComboBox_Status.SelectedValue);

                entity.OutOfState = CheckBox_OutOfState.Checked;

                entity.Quantity = Convert.ToInt32(RadNumericTextBox_Quantity.Value);
                entity.PremiseID = RadTextBox_PremiseID.Text;
                //PG31 
                entity.RateCode = RadTextBox_RateCode.Text;
                entity.FacilityType = RadTextBox_FacilityType.Text;
                entity.IncomeQualified = RadTextBox_IncomeQualified.Text;
                //PG31 
                entity.AccountNumberOld = RadTextBox_AccountNumberOld.Text;
                entity.RequestedKit = RadTextBox_RequestedKit.Text;
                entity.Id = Guid.NewGuid();
            }

            entity.ProjectId = new Guid(RadComboBox_Project.SelectedValue);

            return entity;
        }

        public void DeleteEntity()
        {
        }

        private void InitDropDowns()
        {
            RadComboBox_Status.DataSource = EnumUtils.EnumToListItems(typeof(ConsumerRequestStatus));
            RadComboBox_Status.DataBind();

            User u = presenterAdv.GetOne(HttpContext.Current.User.Identity.Name);
            List<Project> list = new List<Project>();

            if (u.Roles.Contains(PDMRoles.ROLE_Customer))
                list = presenterAdv.GetProjectByUser(u.UserID);
            else
                list = presenterAdv.GetProjectByUser(null);

            if (DataItem as OptOut == null)
            {
                list.Insert(0, new Project() { Id = Guid.Empty });
            }

            RadComboBox_Project.DataSource = list;
            RadComboBox_Project.DataBind();
        }

        private void SetVisibility(bool forUpdate)
        {
            if (forUpdate)
            {
                RadTextBox_AccountNumber.ReadOnly = false;
                //RadTextBox_Method.ReadOnly = true;
                //RadComboBox_Project.Enabled = false;
            }
            else
            {
                RadTextBox_AccountNumber.ReadOnly = false;
                //RadTextBox_Method.ReadOnly = false;
                //RadComboBox_Project.Enabled = true;
            }
        }
    }
}