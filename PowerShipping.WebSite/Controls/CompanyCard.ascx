﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CompanyCard.ascx.cs" Inherits="PowerShipping.WebSite.Controls.CompanyCard" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:HiddenField  ID="HiddenField_Id" runat="server" />

 <telerik:RadProgressManager ID="Radprogressmanager1" runat="server" />
<table> 
    <tr>
        <td id="tr_CompanyName" runat="server">CompanyName</td>
        <td><telerik:RadTextBox ID="RadTextBox_CompanyName" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_CompanyCode" runat="server">
        <td>CompanyCode</td>
        <td><telerik:RadTextBox ID="RadTextBox_CompanyCode" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_CompanyLogo" runat="server">
        <td>CompanyLogo</td>
        <td>
            <telerik:RadBinaryImage runat="server" ID="RadBinaryImage_Logo" />
        </td>
    </tr>
    <tr id="tr_LogoUrl" runat="server"> 
         <td>
               Logo url                     
         </td>
         <td>                                   
             <telerik:RadTextBox ID="RadTextBoxLogoUrl" runat="server" Width = "300px"></telerik:RadTextBox>
         </td>
    </tr>
    <tr id="tr_CompanyLogoUpload" runat="server"> 
         <td>
               Upload logo                     
         </td>
         <td>                                   
             <telerik:RadUpload ID="RadUpload1" runat="server" ControlObjectsVisibility="None" />
             
              <asp:Button runat="server" ID="UploadButton" Text="Upload logo"
                onclick="UploadButton_Click" />
         </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <telerik:RadProgressArea runat="server" ID="ProgressArea1" >
            <Localization Uploaded="Logo upload progress: " />
            </telerik:RadProgressArea>
        </td>
    </tr> 
    <tr id="tr_Active" runat="server">
        <td>IsActive</td>
        <td><asp:CheckBox ID="CheckBox_IsActive" runat="server"></asp:CheckBox></td>
    </tr>    
    <tr runat="server" id="td_CommandCell">
        <td colspan="2">            
            <asp:LinkButton id="btnUpdate" text="Save" runat="server" CommandName="Update" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton id="btnInsert" text="Save" runat="server" CommandName="PerformInsert" Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnCancel" text="Cancel" runat="server" causesvalidation="False" commandname="Cancel" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnDelete" text="Delete" runat="server" causesvalidation="False" CommandName="Cancel" OnClick="btnDelete_OnClick" Font-Size="Medium"></asp:LinkButton>
         </td>
    </tr>   
</table>