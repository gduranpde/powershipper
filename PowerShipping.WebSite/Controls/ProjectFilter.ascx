﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProjectFilter.ascx.cs"
    Inherits="PowerShipping.WebSite.Controls.ProjectFilter" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div class="jobInfo" runat="server" id="div_jobInfo">
    <div style="text-align: center;">
        <b>Project Filter</b>
    </div>
    <table>
        <tr>
            <td colspan="2" align="center"></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button runat="server" ID="btnSearchTop" Text="Search" Width="80px" CssClass="ptButton"
                    OnClick="Button_Filter_Click" />
                <asp:Button runat="server" ID="btnClearTop" Text="Clear" Width="80px" CssClass="ptButton"
                    OnClick="Button_Clear_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center"></td>
        </tr>
        <tr class="jobInfo_td">
            <td>IsActive</td>
            <td class="jobInfo_info">
                <asp:CheckBox ID="chkIsActive" runat="server"></asp:CheckBox>
            </td>
        </tr>
        <tr class="jobInfo_td">
            <td>Project Code</td>
            <td>
                <telerik:RadTextBox ID="txtProjectCode" runat="server">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr class="jobInfo_td">
            <td>Company Name</td>
            <td>
                <telerik:RadTextBox ID="txtCompanyName" runat="server">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">Service State:
            </td>
            <td class="jobInfo_info">
                <telerik:RadTextBox runat="server" ID="txtState">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button runat="server" ID="btnSearchBottom" Text="Search" Width="80px" CssClass="ptButton"
                    OnClick="Button_Filter_Click" />
                <asp:Button runat="server" ID="btnClearBottom" Text="Clear" Width="80px" CssClass="ptButton"
                    OnClick="Button_Clear_Click" />
            </td>
        </tr>
    </table>
</div>
