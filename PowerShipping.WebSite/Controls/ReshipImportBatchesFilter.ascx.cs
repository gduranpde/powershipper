﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class ReshipImportBatchesFilter : System.Web.UI.UserControl
    {
        UserManagementPresenter presenterAdv = new UserManagementPresenter();

        public delegate void ReshipImportBatchesFilterHandler();
        public event ReshipImportBatchesFilterHandler OnFilter;

        public delegate void ReshipImportBatchesFilterClearHandler();
        public event ReshipImportBatchesFilterClearHandler OnClear;

        public string BatchName
        {
            get { return (!string.IsNullOrEmpty(HiddenField_Returns_BatchName.Value)) ? Convert.ToString(HiddenField_Returns_BatchName.Value) : string.Empty; }
            set { HiddenField_Returns_BatchName.Value = value.ToString(); }
        }

        public Guid? ShipperID
        {
            get { return (!string.IsNullOrEmpty(HiddenField_Returns_ShipperID.Value)) ? new Guid(HiddenField_Returns_ShipperID.Value) : Guid.Empty; }
            set { HiddenField_Returns_ShipperID.Value = value.ToString(); }
        }

        public string ImportedBy
        {
            get { return (!string.IsNullOrEmpty(HiddenFieldImportedBy.Value)) ? Convert.ToString(HiddenFieldImportedBy.Value) : string.Empty; }
            set { HiddenFieldImportedBy.Value = value.ToString(); }
        }

        DateTime? _FromImportDate = null;

        public DateTime? FromImportDate
        {
            get
            {
                if (!string.IsNullOrEmpty(HiddenField_Returns_FromDate.Value))
                {
                    _FromImportDate = Convert.ToDateTime(HiddenField_Returns_FromDate.Value);
                }
                else
                {
                    _FromImportDate = null;
                }
                return _FromImportDate;
            }

            set { HiddenField_Returns_FromDate.Value = value.ToString(); }
        }

        DateTime? _ToImportDate = null;

        public DateTime? ToImportDate
        {
            get
            {
                if (!string.IsNullOrEmpty(HiddenField_Returns_ToDate.Value))
                {
                    _FromImportDate = Convert.ToDateTime(HiddenField_Returns_ToDate.Value);
                }
                else
                {
                    _ToImportDate = null;
                }
                return _ToImportDate;
            }

            set { HiddenField_Returns_ToDate.Value = value.ToString(); }
        }

        private void Search()
        {
            BatchName = RadTextBox_BatchName.Text;

            ImportedBy = RadTextBox_ImportedBy.Text;
            FromImportDate = RadDatePicker_FromImportedTime.SelectedDate;
            ToImportDate = RadDatePicker_ToImportedTime.SelectedDate;

            if (!string.IsNullOrEmpty(ListBox_Shipper.SelectedValue.ToString()))
            {
                ShipperID = new Guid(ListBox_Shipper.SelectedValue);
            }

            if (RadDatePicker_FromImportedTime.SelectedDate.HasValue)
            {
                FromImportDate = RadDatePicker_FromImportedTime.SelectedDate;
            }
            else
            {
                FromImportDate = null;
            }

            if (RadDatePicker_ToImportedTime.SelectedDate.HasValue)
            {
                ToImportDate = RadDatePicker_ToImportedTime.SelectedDate;
            }
            else
            {
                ToImportDate = null;
            }

            OnFilter();
        }

        private void clear()
        {
            RadTextBox_BatchName.Text = "";
            ListBox_Shipper.SelectedValue = null;
            RadTextBox_ImportedBy.Text = "";
            RadDatePicker_FromImportedTime.SelectedDate = null;
            RadDatePicker_ToImportedTime.SelectedDate = null;

            BatchName = "";
            ShipperID = null;
            ImportedBy = "";
            FromImportDate = null;
            ToImportDate = null;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ListBox_Shipper.DataSource = presenterAdv.GetAllShipers();
                ListBox_Shipper.DataBind();
            }
        }

        protected void btnSearchDown_Click(object sender, EventArgs e)
        {
            Search();
        }

        protected void btnSearchTop_Click(object sender, EventArgs e)
        {
            Search();
        }

        protected void btnClearTop_Click(object sender, EventArgs e)
        {
            clear();
        }

        protected void btnClearDown_Click(object sender, EventArgs e)
        {
            clear();
        }
    }
}