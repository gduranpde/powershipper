﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OperatingCompanyCard.ascx.cs" Inherits="PowerShipping.WebSite.Controls.OperatingCompanyCard" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:HiddenField ID="HiddenField_Id" runat="server" />


<telerik:RadProgressManager ID="Radprogressmanager1" runat="server" />
       
<table id="MainTable">
    <tr runat="server" id="TrData">
        <td id="firstColumn">
            <table id="secondTable">
                <tr id="tr_OperatingCompanyCode" runat="server">
                    <td>Operating Company Code</td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBox_OperatingCompanyCode" runat="server"></telerik:RadTextBox></td>
                </tr>

                <tr id="tr_OperatingCompanyName" runat="server">
                    <td>Operating Company Name</td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBox_OperatingCompanyName" runat="server"></telerik:RadTextBox></td>
                </tr>

                <tr id="tr_ParentCompany" runat="server">
                    <td>Company Name</td>
                    <td>
                        <telerik:RadComboBox ID="RadComboBox_Companies" runat="server" DataValueField="Id"
                            DataTextField="CompanyName">
                        </telerik:RadComboBox>
                    </td>
                </tr>
                 <tr id="tr_ServiceState" runat="server">
                    <td>Service State</td>
                    <td>
                        <telerik:RadComboBox ID="cmbStates" runat="server" DataValueField="Id" DataTextField="Name" MaxHeight="350">
                        </telerik:RadComboBox>
                    </td>
                </tr>

                <tr id="tr_Active" runat="server">
                    <td>IsActive</td>
                    <td>
                        <asp:CheckBox ID="CheckBox_IsActive" Checked="true"  runat="server"></asp:CheckBox></td>
                </tr>
            </table>
        </td>
        <td id="secondColumn">
            <table id="Table1" style="margin:30px">
                <tr id="tr_CreatedBy" runat="server">
                    <td align="right">Created By:
                    </td>
                    <td>
                        <%#Eval("CreatedBy")%>
                    </td>
                </tr>
                <tr id="tr_CreatedDate" runat="server">
                    <td align="right">Created Date:</td>
                    <td>
                        <%#Eval("CreatedDate")%>
                    </td>
                </tr>

                <tr id="tr_ModifiedBY" runat="server">
                    <td align="right">Last Modified By: </td>
                    <td>
                        <%#Eval("LastModifiedBy")%>
                    </td>
                </tr>

                <tr id="tr_ModifiedDate" runat="server">
                    <td align="right">Last Modified Date: 
                    </td>
                    <td>
                        <%#Eval("LastModifiedDate")%>
                    </td>
                </tr>

                <tr id="tr1" runat="server">
                    <td></td>
                    <td></td>
                </tr>
                <tr id="tr2" runat="server">
                    <td></td>
                    <td></td>
                </tr>
                <tr id="tr3" runat="server">
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </td>
    </tr>

    <tr runat="server" id="td_CommandCell">
        <td colspan="2">
            <asp:LinkButton ID="btnUpdate" Text="Save" runat="server" CommandName="Update" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnInsert" Text="Save" runat="server" CommandName="PerformInsert" Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False" CommandName="Cancel" Font-Size="Medium"></asp:LinkButton>
        </td>
    </tr>
</table>
