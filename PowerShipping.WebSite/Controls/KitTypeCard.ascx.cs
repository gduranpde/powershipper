﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class KitTypeCard : UserControl
    {
        private readonly UserManagementPresenter presenter = new UserManagementPresenter();

        public Guid KitTypeId
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_Id.Value))
                           ? new Guid(HiddenField_Id.Value)
                           : Guid.Empty;
            }
            set { HiddenField_Id.Value = value.ToString(); }
        }

        public object DataItem { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected override void OnInit(EventArgs e)
        {
            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');");

            InitializeComponent();
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (DataItem == null)
                return;

            if ((DataItem != null) & (DataItem.GetType() != typeof(GridInsertionObject)))
            {
                var entity = (KitType)DataItem;

                RadListBox_Items.DataSource = presenter.GetKitItemsByKitType(entity.Id);
                RadListBox_Items.DataBind();

                RadTextBox_KitName.Text = entity.KitName;
                RadTextBox_KitContents.Text = entity.KitContents;
                RadNumericTextBox_Height.Value = entity.Height;
                //PG31
                RadNumericTextBox_Kw_Savings.Value = Convert.ToDouble(entity.Kw_Savings);
                RadNumericTextBox_Kwh_Savings.Value = Convert.ToDouble(entity.Kwh_Savings);
                //PG31
                RadNumericTextBox_Width.Value = entity.Width;
                RadNumericTextBox_Length.Value = entity.Length;
                RadNumericTextBox_Weight.Value = entity.Weight;

                entity.KitItems = presenter.GetKitItemsByKitType(entity.Id);

                foreach (KitItem ki in entity.KitItems)
                {
                    RadListBoxItem item = RadListBox_Items.FindItemByValue(ki.Id.ToString());
                    if (item != null)
                        item.Checked = true;
                }

                KitTypeId = entity.Id;
            }

            base.OnPreRender(e);
        }

        protected void lnkBtnItems_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("KitItems.aspx?KitTypeId=" + KitTypeId);
        }
        
        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                KitType k = presenter.GetKitTypeById(KitTypeId);
                presenter.DeleteKitType(k.Id);
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to delete Kit. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
               
                Helper.ErrorInGrid("RadGrid_KitType", Page, l);
            }
        }

        private void InitializeComponent()
        {
            DataBinding += Init_DataBinding;
        }

        private void Init_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        public void InitData()
        {
            InitDropDowns();

           
        }

        public KitType FillEntity()
        {
            var entity = new KitType();
            if (KitTypeId != Guid.Empty)
            {
                entity.Id = KitTypeId;
            }
            else
            {
                entity.Id = Guid.NewGuid();
            }
            entity.KitName = RadTextBox_KitName.Text;
            entity.KitContents = RadTextBox_KitContents.Text;
            entity.Height = Convert.ToDouble(RadNumericTextBox_Height.Value);
            //PG31
            entity.Kw_Savings = Convert.ToDouble(RadNumericTextBox_Kw_Savings.Value);
            entity.Kwh_Savings = Convert.ToDouble(RadNumericTextBox_Kwh_Savings.Value);
            //PG31
            entity.Width = Convert.ToDouble(RadNumericTextBox_Width.Value);
            entity.Length = Convert.ToDouble(RadNumericTextBox_Length.Value);
            entity.Weight = Convert.ToDouble(RadNumericTextBox_Weight.Value);

            entity.KitItems = new List<KitItem>();
            foreach (RadListBoxItem item in RadListBox_Items.CheckedItems)
            {
                entity.KitItems.Add(presenter.GetKitItemById(new Guid(item.Value)));
            }

            return entity;
        }

        private void InitDropDowns()
        {
            //RadListBox_Items.DataSource = presenter.GetKitItemsByKitType(null);
            //RadListBox_Items.DataBind();
        }
    }
}