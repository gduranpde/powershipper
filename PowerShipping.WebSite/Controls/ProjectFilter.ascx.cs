﻿using System;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using System.Collections.Generic;

namespace PowerShipping.WebSite.Controls
{
    public partial class ProjectFilter : System.Web.UI.UserControl
    {
        public delegate void ProjectFilterHandler(List<Project> projects);
        public event ProjectFilterHandler OnFilter;

        public delegate void ProjectFilterClearHandler();
        public event ProjectFilterClearHandler OnClear;

        UserManagementPresenter presenter = new UserManagementPresenter();

        public bool IsActive
        {
            get
            {
                return chkIsActive.Checked;
            }
        }

        protected void Button_Filter_Click(object sender, EventArgs e)
        {
            FilerData();
        }

        private void FilerData()
        {
            var projects = presenter.GetProjectByCompany(null);
            projects = IsActive ?
                projects.Where(p => p.IsActive).ToList() :
                projects.Where(p => !p.IsActive).ToList();
            if (!string.IsNullOrEmpty(txtCompanyName.Text))
            {
                projects = projects.Where(p => p.CompanyName.ToLower().Contains(txtCompanyName.Text.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(txtState.Text))
            {
                projects = projects.Where(p => p.StateName.ToLower().Contains(txtState.Text.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(txtProjectCode.Text))
            {
                projects = projects.Where(p => p.ProjectCode.ToLower().Contains(txtProjectCode.Text.ToLower())).ToList();
            }
            OnFilter(projects);
        }

        protected void Button_Clear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            chkIsActive.Checked = true;
            txtCompanyName.Text = string.Empty;
            txtProjectCode.Text = string.Empty;
            txtState.Text = string.Empty;
            // raise event to clear filter on page and get all projects back
            OnClear();
        }
    }
}