﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Data;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class ReshipSpecialProcessingCard : System.Web.UI.UserControl
    {
        KitTypesPresenter _kitTypesPresenter = new KitTypesPresenter();
        UserManagementPresenter _userManagementPresenter = new UserManagementPresenter();
        ShipmentDetailsPresenter _shipmentDetailsPresenter = new ShipmentDetailsPresenter();
        ConsumerRequestsPresenter _consumerRequestsPresenter = new ConsumerRequestsPresenter();
        ReturnsPresenter presenter = new ReturnsPresenter();

        public List<ShipmentDetail> ShipmentDetailsList
        {
            get { return (List<ShipmentDetail>)ViewState["ShipmentDetailsList"]; }
            set { ViewState["ShipmentDetailsList"] = value; }
        }

        public List<ShipmentDetail> ResultList
        {
            get
            {
                List<ShipmentDetail> list = new List<ShipmentDetail>();
                foreach (RepeaterItem item in rp.Items)
                {
                    ShipmentDetail sdNew = new ShipmentDetail();
                    sdNew.Id = Guid.NewGuid();

                    Label lbAccNum = (Label)item.FindControl("lbAccNum");
                    sdNew.AccountNumber = lbAccNum.Text;

                    TextBox tb = (TextBox)item.FindControl("tbTrackNum");
                    sdNew.FedExTrackingNumber = tb.Text;

                    sdNew.ShipmentNumber = (_shipmentDetailsPresenter.GetMaxShippingNumber() + 1).ToString();
                    sdNew.ShippingStatus = ShipmentShippingStatus.Pending;
                    sdNew.ReshipStatus = ShipmentReshipStatus.NA;

                    //RadComboBox RadComboBox_KitType = (RadComboBox)item.FindControl("RadComboBox_KitType");
                    //sdNew.KitTypeId = new Guid(RadComboBox_KitType.SelectedValue);

                    Label lbKitType = (Label)item.FindControl("lbKitType");
                    sdNew.KitTypeId = new Guid(ViewState["KitTypeID"].ToString());

                    sdNew.ShippingStatusDate = DateTime.Now;
                    DropDownList DDL_Shipper = (DropDownList)item.FindControl("DDL_Shipper");
                    sdNew.ShipperId = new Guid(DDL_Shipper.SelectedValue);

                    //RadComboBox RadComboBox_Shipper = (RadComboBox)item.FindControl("RadComboBox_Shipper");
                    //sdNew.ShipperId = new Guid(RadComboBox_Shipper.SelectedValue);

                    TextBox tbPONum = (TextBox)item.FindControl("tbPONum");
                    sdNew.PurchaseOrderNumber = tbPONum.Text;

                    ShipmentDetail oldSd = ShipmentDetailsList.Find(sd => sd.AccountNumber == sdNew.AccountNumber);
                    sdNew.ShipmentDetailAddress1 = oldSd.ShipmentDetailAddress1;
                    sdNew.ShipmentDetailAddress2 = oldSd.ShipmentDetailAddress2;
                    sdNew.ShipmentDetailCity = oldSd.ShipmentDetailCity;
                    sdNew.ShipmentDetailState = oldSd.ShipmentDetailState;
                    sdNew.ShipmentDetailZip = oldSd.ShipmentDetailZip;

                    list.Add(sdNew);
                }
                ViewState["ShipmentDetailsList"] = list;
                return list;
            }
        }

        public bool IsAllFilled
        {
            get
            {
                foreach (RepeaterItem item in rp.Items)
                {
                    TextBox tb = (TextBox)item.FindControl("tbTrackNum");
                    if (string.IsNullOrEmpty(tb.Text))
                        return false;
                }
                return true;
            }
        }

        public string ErrorText
        {
            get { return lbError.Text; }
            set { lbError.Text = value; }
        }

        //public Guid ProjectId { get; set; }

        public void BindData()
        {
            rp.DataSource = ShipmentDetailsList;
            rp.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void rp_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ShipmentDetail sd = (ShipmentDetail)e.Item.DataItem;
                //HiddenField hfId = (HiddenField)e.Item.FindControl("hfId");

                //StateTable objAllState =  new StateTable();

                //hfId.Value = sd.Id.ToString();

                HiddenField hfId = (HiddenField)e.Item.FindControl("hfId");
                hfId.Value = sd.Id.ToString();

                Label lbAccNum = (Label)e.Item.FindControl("lbAccNum");
                lbAccNum.Text = sd.AccountNumber;

                Label lbName = (Label)e.Item.FindControl("lbName");
                lbName.Text = sd.AccountName;

                Label lbAddress = (Label)e.Item.FindControl("lbAddress");
                //lbAddress.Text = sd.ShipmentDetailAddress1;
                lbAddress.Text = sd.Address1;

                Label lbCity = (Label)e.Item.FindControl("lbCity");
                //lbCity.Text = sd.ShipmentDetailCity;
                lbCity.Text = sd.City;

                Label lbState = (Label)e.Item.FindControl("lbState");
                //lbState.Text = sd.ShipmentDetailState;
                lbState.Text = sd.State;

                Label lbZip = (Label)e.Item.FindControl("lbZip");
                //lbZip.Text = sd.ShipmentDetailZip;
                lbZip.Text = sd.ZipCode;

                //RadComboBox RadComboBox_KitType = (RadComboBox)e.Item.FindControl("RadComboBox_KitType");
                //RadComboBox_KitType.DataSource = _kitTypesPresenter.GetAll();
                //RadComboBox_KitType.DataBind();

                Label lbKitType = (Label)e.Item.FindControl("lbKitType");
                lbKitType.Text = sd.KitTypeName;
                ViewState["KitTypeID"] = sd.KitTypeId;
                DropDownList ddlShip = (DropDownList)e.Item.FindControl("DDL_Shipper");
                ddlShip.DataSource = _userManagementPresenter.GetAllShipers();
                ddlShip.DataTextField = "ShipperName";
                ddlShip.DataValueField = "Id";
                ddlShip.SelectedIndex = -2;
                ddlShip.DataBind();

                //RadComboBox RadComboBox_Shipper = (RadComboBox)e.Item.FindControl("RadComboBox_Shipper");
                //RadComboBox_Shipper.DataSource = _userManagementPresenter.GetAllShipers();
                //RadComboBox_Shipper.SelectedIndex = 2;
                //RadComboBox_Shipper.DataBind();
            }
        }

        protected void rp_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "linkedit")
            {
                int irepeateritemscount = rp.Items.Count;
                Panel pnl11 = (Panel)ShippingAddressdialog.FindControl("pnlPopUp2");
                if (irepeateritemscount > 7)
                {
                    int margin = irepeateritemscount * (-41);

                    pnl11.Style.Add("margin-top", Convert.ToString(margin) + "px");
                }
                else
                {
                    pnl11.Style.Add("margin-top", "-50px");
                }

                ShipmentDetail sd = (ShipmentDetail)e.Item.DataItem;
                HiddenField hfId = (HiddenField)e.Item.FindControl("hfId");
                Guid gdid = new Guid(hfId.Value);
                sd = presenter.GetById(gdid);

                Label LblAccountNo = (Label)ShippingAddressdialog.FindControl("LblAccountNo");
                LblAccountNo.Text = sd.AccountNumber;
                Label LblFullName = (Label)ShippingAddressdialog.FindControl("LblFullName");
                LblFullName.Text = sd.AccountName;
                TextBox TxtAddress1 = (TextBox)ShippingAddressdialog.FindControl("TxtAddress1");
                //TxtAddress1.Text = sd.ShipmentDetailAddress1;
                TxtAddress1.Text = sd.Address1.ToUpper();
                TextBox TxtAddress2 = (TextBox)ShippingAddressdialog.FindControl("TxtAddress2");
                //TxtAddress2.Text = sd.ShipmentDetailAddress2;
                TxtAddress2.Text = sd.Address2.ToUpper();
                TextBox TxtCity = (TextBox)ShippingAddressdialog.FindControl("TxtCity");
                //TxtCity.Text = sd.ShipmentDetailCity;
                TxtCity.Text = sd.City.ToUpper();

                DropDownList DDLState = (DropDownList)ShippingAddressdialog.FindControl("DDLState");
                //List<State> s = objAllState.GetAllStates();
                DDLState.DataSource = _userManagementPresenter.GetAllStates();
                DDLState.DataTextField = "Code";
                DDLState.DataValueField = "Id";
                DDLState.DataBind();
                //DDLState.SelectedItem.Text = sd.ShipmentDetailState;
                DDLState.SelectedItem.Text = sd.State;

                TextBox TxtZip = (TextBox)ShippingAddressdialog.FindControl("TxtZip");
                //TxtZip.Text = sd.ShipmentDetailZip;
                TxtZip.Text = sd.ZipCode;

                Session["ShipmentDetailObject"] = sd;

                ShippingAddressdialog.Visible = true;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowPopup", "return ShowPopup();", true);
                // Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowPopup", "ShowPopup();", true);
                Panel pnl2 = (Panel)ShippingAddressdialog.FindControl("pnlPopUp2");
                pnl2.Style.Add("display", "block");
                Panel pnl = (Panel)Parent.FindControl("pnlList");
                pnl.CssClass = "modalPopupList retainPopup";
                Panel pnl1 = (Panel)Parent.FindControl("pnlPopUp");
                pnl1.Style.Add("display", "none");
                backgroundElement1.Style.Add("display", "block");
                backgroundElement1.Style.Add("z-index", "10003");
                Label lblGetId = (Label)ShippingAddressdialog.FindControl("lblGetID");
                int currentIndex = e.Item.ItemIndex + 1;
                if (currentIndex <= 9)
                {
                    lblGetId.Text = "0" + Convert.ToString(currentIndex);
                }
                else
                {
                    lblGetId.Text = Convert.ToString(currentIndex);
                }

                //------------------------------

                List<Guid> lst = (List<Guid>)Session["Lst"];

                List<ShipmentDetail> list1 = new List<ShipmentDetail>();

                if (lst != null)
                {
                    foreach (Guid val in lst)
                    {
                        ShipmentDetail sd1 = presenter.GetById(val);
                        list1.Add(sd1);
                    }

                    //reshipSpecialProcessingCard.ErrorText = null;
                    rp.DataSource = list1;
                    rp.DataBind();
                    //reshipSpecialProcessingCard.ShipmentDetailsList = list1;
                    //reshipSpecialProcessingCard.BindData();
                }
            }
        }
    }
}