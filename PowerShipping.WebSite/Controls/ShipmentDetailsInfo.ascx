﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShipmentDetailsInfo.ascx.cs" Inherits="PowerShipping.WebSite.Controls.ShipmentDetailsInfo" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<table style="width: 500px;">
    <tr>
        <td class="contentTableCell">ShipperFedExAccountNumber:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_ShipperFedExAccountNumber" runat="server" Width="200px"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">ShipperCompanyName:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_ShipperCompanyName" runat="server" Width="200px"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">ShipperPhysicalAddress:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_ShipperPhysicalAddress" runat="server" Width="200px" TextMode="MultiLine" Rows="5"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">ShipperContactName:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_ShipperContactName" runat="server" Width="200px"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">ShipperContactPhone:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_ShipperContactPhone" runat="server" Width="200px"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">PackagePickupLocation:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_PackagePickupLocation" runat="server" Width="200px"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">MoreThanOnePackageForEachRecipient:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_MoreThanOnePackageForEachRecipient" runat="server" Width="200px"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">FedExServiceType:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_FedExServiceType" runat="server" Width="200px"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">DomesticOrInternational:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_DomesticOrInternational" runat="server" Width="200px"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">AnyPackagesToResidentialAddresses:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_AnyPackagesToResidentialAddresses" runat="server" Width="200px"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">SignatureRequired:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_SignatureRequired" runat="server" Width="200px"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">BulkLabelSupportSpecialHandling:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_BulkLabelSupportSpecialHandling" runat="server" Width="200px"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">PackageType:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_PackageType" runat="server" Width="200px"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">DeclaredValueAmount:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_DeclaredValueAmount" runat="server" Width="200px"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">PaymentType:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_PaymentType" runat="server" Width="200px"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">FedExAccountNumberForBilling:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_FedExAccountNumberForBilling" runat="server" Width="200px"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">DeliveryServiceForLabels:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_DeliveryServiceForLabels" runat="server" Width="200px"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">AddressToSendLabels:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_AddressToSendLabels" runat="server" Width="200px"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">FedExBulkLabelEmailAddress:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_FedExBulkLabelEmailAddress" runat="server" Width="200px"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">OtherCCEmailAddresses:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_OtherCCEmailAddresses" runat="server" Width="200px" TextMode="MultiLine" Rows="5"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">ConsumerCustomerServicePhoneNumber:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_ConsumerCustomerServicePhoneNumber" runat="server" Width="200px"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="contentTableCell">PDMShippingExceptionEmailAddress:</td>
        <td style="width: 200px;">
            <telerik:RadTextBox ID="RadTextBox_PDMShippingExceptionEmailAddress" runat="server" Width="200px"></telerik:RadTextBox></td>
    </tr>
</table>
