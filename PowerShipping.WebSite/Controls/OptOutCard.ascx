﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OptOutCard.ascx.cs" Inherits="PowerShipping.WebSite.Controls.OptOutCard" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:HiddenField  ID="HiddenField_Id" runat="server" />
<table>    
    <tr id="tr1" runat="server">
        <td>Opt Out Date</td>
        <td><telerik:RadDatePicker ID="RadDatePicker_OptOutDate" Runat="server" Skin="Telerik"  Width="135px" >
                                                    <calendar skin="Telerik" usecolumnheadersasselectors="False" 
                                                        userowheadersasselectors="False" viewselectortext="x">
                                                    </calendar>
                                                    <datepopupbutton hoverimageurl="" imageurl="" />
                                                    <dateinput dateformat="MM/dd/yyyy" displaydateformat="MM/dd/yyyy">
                                                    </dateinput>
                                                </telerik:RadDatePicker>
         </td>
    </tr>
    <tr>
        <td id="tr_AccountNumber" runat="server">AccountNumber</td>
        <td><telerik:RadTextBox ID="RadTextBox_AccountNumber" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_AccountName" runat="server">
        <td>Name</td>
        <td><telerik:RadTextBox ID="RadTextBox_AccountName" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_Address1" runat="server">
        <td>Address</td>
        <td><telerik:RadTextBox ID="RadTextBox_Address1" runat="server"></telerik:RadTextBox></td>
    </tr>      
    <tr id="tr_City" runat="server">
        <td>City</td>
        <td><telerik:RadTextBox ID="RadTextBox_City" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_State" runat="server">
        <td>State</td>
        <td><telerik:RadTextBox ID="RadTextBox_State" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_ZipCode" runat="server">
        <td>Zip</td>
        <td><telerik:RadTextBox ID="RadTextBox_ZipCode" runat="server"></telerik:RadTextBox></td>
    </tr>
      <tr runat="server" id="tr_Project">
        <td>Project</td>
        <td><telerik:RadComboBox ID="RadComboBox_Project" runat="server" DataValueField="Id" DataTextField="FullName" Width="300px"></telerik:RadComboBox></td>
    </tr>
    <tr id="tr_Email" runat="server">
        <td>Email</td>
        <td><telerik:RadTextBox ID="RadTextBox_Email" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_Phone1" runat="server">
        <td>Phone1</td>
        <td><telerik:RadMaskedTextBox ID="RadMaskedTextBox_Phone1" runat="server" Mask="###-###-####" ></telerik:RadMaskedTextBox></td>
    </tr>        
    <tr id="tr_Phone2" runat="server">
        <td>Phone2</td>
        <td><telerik:RadMaskedTextBox ID="RadMaskedTextBox_Phone2" runat="server" Mask="###-###-####" ></telerik:RadMaskedTextBox></td>
    </tr> 
     
     <tr id="tr_IsEnergyProgram" runat="server">
        <td>Program Opt Out</td>
        <td><asp:CheckBox ID="CheckBox_IsEnergyProgram" runat="server"></asp:CheckBox></td>
    </tr>  
     <tr id="tr_IsTotalDesignation" runat="server">
        <td>Total Opt Out</td>
        <td><asp:CheckBox ID="CheckBox_IsTotalDesignation" runat="server"></asp:CheckBox></td>
    </tr>    
     <tr id="tr_Notes" runat="server">
        <td >Notes</td>
        <td><telerik:RadTextBox ID="RadTextBox_Notes" runat="server" TextMode="MultiLine" Width="500px" Height="50px" MaxLength="1024"></telerik:RadTextBox></td>
    </tr>
    
    <tr runat="server" id="td_CommandCell">
        <td colspan="2">            
            <asp:LinkButton id="btnUpdate" text="Save" runat="server" CommandName="Update" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton id="btnInsert" text="Save" runat="server" CommandName="PerformInsert" Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnCancel" text="Cancel" runat="server" causesvalidation="False" commandname="Cancel" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnDelete" text="Delete" runat="server" causesvalidation="False" CommandName="Cancel" OnClick="btnDelete_OnClick" Font-Size="Medium"></asp:LinkButton>
         </td>
    </tr>                      
</table>