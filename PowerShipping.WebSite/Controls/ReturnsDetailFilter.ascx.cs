﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class ReturnsEditInfo : System.Web.UI.UserControl
    {
        public delegate void ReturnDetailFilterHandler();
        public event ReturnDetailFilterHandler OnFilter;

        public delegate void ReturnDetailFilterClearHandler();
        public event ReturnDetailFilterClearHandler OnClear;

        ShipmentDetailsPresenter presenter = new ShipmentDetailsPresenter();
        UserManagementPresenter presenterAdv = new UserManagementPresenter();

        //public int ReshipStatus
        //{
        //    get { return (!string.IsNullOrEmpty(HiddenField_Returns_ReshipStatus.Value)) ? Convert.ToInt32(HiddenField_Returns_ReshipStatus.Value) : 0; }
        //    set { HiddenField_Returns_ReshipStatus.Value = value.ToString(); }
        //}

        int? _ReshipStatus = null;

        public int? ReshipStatus
        {
            get
            {
                if (!string.IsNullOrEmpty(HiddenField_Returns_ReshipStatus.Value))
                {
                    _ReshipStatus = Convert.ToInt32(HiddenField_Returns_ReshipStatus.Value);
                }
                else
                {
                    _ReshipStatus = null;
                }
                return _ReshipStatus;
            }

            set { HiddenField_Returns_ReshipStatus.Value = value.ToString(); }
        }

        public string ShipmentNumber
        {
            get { return (!string.IsNullOrEmpty(HiddenField_Returns_ShipmentNumber.Value)) ? Convert.ToString(HiddenField_Returns_ShipmentNumber.Value) : string.Empty; }
            set { HiddenField_Returns_ShipmentNumber.Value = value.ToString(); }
        }

        public string AccountNumber
        {
            get { return (!string.IsNullOrEmpty(HiddenField_Returns_AccountNumber.Value)) ? Convert.ToString(HiddenField_Returns_AccountNumber.Value) : string.Empty; }
            set { HiddenField_Returns_AccountNumber.Value = value.ToString(); }
        }

        public string FedexNumber
        {
            get { return (!string.IsNullOrEmpty(HiddenField_Returns_FedexNumber.Value)) ? Convert.ToString(HiddenField_Returns_FedexNumber.Value) : string.Empty; }
            set { HiddenField_Returns_FedexNumber.Value = value.ToString(); }
        }

        public string AccountName
        {
            get { return (!string.IsNullOrEmpty(HiddenField_Returns_AccountName.Value)) ? Convert.ToString(HiddenField_Returns_AccountName.Value) : string.Empty; }
            set { HiddenField_Returns_AccountName.Value = value.ToString(); }
        }

        public string PONumber
        {
            get { return (!string.IsNullOrEmpty(HiddenField_Returns_PONumber.Value)) ? Convert.ToString(HiddenField_Returns_PONumber.Value) : string.Empty; }
            set { HiddenField_Returns_PONumber.Value = value.ToString(); }
        }

        public Guid? KitTypeID
        {
            get { return (!string.IsNullOrEmpty(HiddenField_Returns_KitTypeID.Value)) ? new Guid(HiddenField_Returns_KitTypeID.Value) : Guid.Empty; }
            set { HiddenField_Returns_KitTypeID.Value = value.ToString(); }
        }

        public Guid? ShipperID
        {
            get { return (!string.IsNullOrEmpty(HiddenField_Returns_ShipperID.Value)) ? new Guid(HiddenField_Returns_ShipperID.Value) : Guid.Empty; }
            set { HiddenField_Returns_ShipperID.Value = value.ToString(); }
        }

        public string OpCo
        {
            get { return (!string.IsNullOrEmpty(HiddenField_Returns_OpCo.Value)) ? Convert.ToString(HiddenField_Returns_OpCo.Value) : string.Empty; }
            set { HiddenField_Returns_OpCo.Value = value.ToString(); }
        }

        public Guid? ReasonID
        {
            get { return (!string.IsNullOrEmpty(HiddenField_Returns_ReasonID.Value)) ? new Guid(HiddenField_Returns_ReasonID.Value) : Guid.Empty; }
            set { HiddenField_Returns_ReasonID.Value = value.ToString(); }
        }

        //public DateTime? StartDate
        //{
        //    get { return (!string.IsNullOrEmpty(HiddenField_Returns_StartDate.Value)) ? Convert.ToDateTime(HiddenField_Returns_StartDate.Value) : DateTime.Now; }
        //    set { HiddenField_Returns_StartDate.Value = value.ToString(); }
        //}

        //public DateTime? EndDate
        //{
        //    get { return (!string.IsNullOrEmpty(HiddenField_Returns_EndDate.Value)) ? Convert.ToDateTime(HiddenField_Returns_EndDate.Value) : DateTime.Now; }
        //    set { HiddenField_Returns_EndDate.Value = value.ToString(); }
        //}

        DateTime? _startDate = null;

        public DateTime? StartDate
        {
            get
            {
                if (!string.IsNullOrEmpty(HiddenField_Returns_StartDate.Value))
                {
                    _startDate = Convert.ToDateTime(HiddenField_Returns_StartDate.Value);
                }
                else
                {
                    _startDate = null;
                }
                return _startDate;
            }

            set { HiddenField_Returns_StartDate.Value = value.ToString(); }
        }

        DateTime? _endDate = null;

        public DateTime? EndDate
        {
            get
            {
                if (!string.IsNullOrEmpty(HiddenField_Returns_EndDate.Value))
                {
                    _endDate = Convert.ToDateTime(HiddenField_Returns_EndDate.Value);
                }
                else
                {
                    _endDate = null;
                }
                return _endDate;
            }

            set { HiddenField_Returns_EndDate.Value = value.ToString(); }
        }

        private void FillLists()
        {
            ListBoxKitType.DataSource = presenter.GetListOfKitTypes();

            ListBoxKitType.DataBind();

            ListBox_Shipper.DataSource = presenterAdv.GetAllShipers();
            ListBox_Shipper.DataBind();

            LstBxReasonforReturn.DataSource = presenter.GetAllReasons();
            LstBxReasonforReturn.DataBind();

            EnumListItem[] list = EnumUtils.EnumToListItemsWithNull(typeof(ShipmentReshipStatus));
            list.SetValue(new EnumListItem("All", null), 0);

            ddReshipStatusFilter.DataSource = list;
            ddReshipStatusFilter.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                FillLists();
        }

        protected void btnSearchTop_Click(object sender, EventArgs e)
        {
            Search();
        }

        protected void btnClearTop_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Search()
        {
            //if (!string.IsNullOrEmpty(ddReshipStatusFilter.SelectedValue.ToString()))
            //{
            //    ReshipStatus = Convert.ToInt32(ddReshipStatusFilter.SelectedValue);
            //}

            if (ddReshipStatusFilter.SelectedItem.Text != "All")
            {
                ReshipStatus = Convert.ToInt32(ddReshipStatusFilter.SelectedValue);
            }
            else
            {
                ReshipStatus = null;
            }

            ShipmentNumber = RadTextBox_ShipmentNumber.Text;
            AccountNumber = RadTextBox_AccountNumberFilter.Text;
            FedexNumber = RadTextBox_FedExTrackingNumber.Text;
            AccountName = RadTextBox_AccountName.Text;
            PONumber = RadTextBox_PurchaseOrderNumber.Text;
            OpCo = RadTextBox_OperatingCompany.Text;

            if (!string.IsNullOrEmpty(ListBoxKitType.SelectedValue.ToString()))
            {
                KitTypeID = new Guid(ListBoxKitType.SelectedValue);
            }

            if (!string.IsNullOrEmpty(ListBox_Shipper.SelectedValue.ToString()))
            {
                ShipperID = new Guid(ListBox_Shipper.SelectedValue);
            }

            if (!string.IsNullOrEmpty(LstBxReasonforReturn.SelectedValue.ToString()))
            {
                ReasonID = new Guid(LstBxReasonforReturn.SelectedValue);
            }

            if (RadDatePicker_ReturnDateStartDate.SelectedDate.HasValue)
            {
                StartDate = RadDatePicker_ReturnDateStartDate.SelectedDate;
            }
            else
            {
                StartDate = null;
            }

            if (RadDatePicker_ReturnDateEndDate.SelectedDate.HasValue)
            {
                EndDate = RadDatePicker_ReturnDateEndDate.SelectedDate;
            }
            else
            {
                EndDate = null;
            }

            OnFilter();
        }

        private void Clear()
        {
            ddReshipStatusFilter.SelectedValue = null;
            RadTextBox_ShipmentNumber.Text = "";
            RadTextBox_AccountNumberFilter.Text = "";
            RadTextBox_FedExTrackingNumber.Text = "";
            RadTextBox_AccountName.Text = "";
            RadTextBox_PurchaseOrderNumber.Text = "";
            ListBoxKitType.SelectedValue = null;
            ListBox_Shipper.SelectedValue = null;
            RadTextBox_OperatingCompany.Text = "";
            LstBxReasonforReturn.SelectedValue = null;
            RadDatePicker_ReturnDateStartDate.SelectedDate = null;
            RadDatePicker_ReturnDateEndDate.SelectedDate = null;

            ReshipStatus = null;
            ShipmentNumber = "";
            AccountNumber = "";
            FedexNumber = "";
            AccountName = "";
            PONumber = "";
            KitTypeID = null;
            ShipperID = null;
            OpCo = "";
            ReasonID = null;
            StartDate = null;
            EndDate = null;
        }

        protected void btnSearchDown_Click(object sender, EventArgs e)
        {
            Search();
        }

        protected void btnClearDown_Click(object sender, EventArgs e)
        {
            Clear();
        }
    }
}