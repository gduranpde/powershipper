﻿using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class ItemDetailCard : System.Web.UI.UserControl
    {
        #region Class Data
        private readonly ItemsSetupPresenter presenter = new ItemsSetupPresenter();
        private readonly UserManagementPresenter umPresenter = new UserManagementPresenter();
        private readonly ItemSavingsPresenter itemSavingsPresenter = new ItemSavingsPresenter();
        #endregion

        #region Properties
        public Guid ItemID
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_Id.Value))
                           ? new Guid(HiddenField_Id.Value)
                           : Guid.Empty;
            }
            set { HiddenField_Id.Value = value.ToString(); }
        }

        public object DataItem { get; set; }
        #endregion

        #region Methods
        protected override void OnInit(EventArgs e)
        {
            DataBinding += (s, ev) => { InitItemDetails(); };
            base.OnInit(e);
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            InitStates();
        }
        public void InitItemDetails()
        {
            if ((DataItem != null) && (DataItem.GetType() != typeof(GridInsertionObject)))
            {
                var entity = (Item)DataItem;

                RadTextBox_ItemName.Text = entity.ItemName;
                RadTextBox_ItemDescription.Text = entity.ItemDescription;
                RadNumericTextBox_Default_Kw_Impact.Value = (double)entity.DefaultKwImpact;
                RadNumericTextBox_Default_Kwh_Impact.Value = (double)entity.DefaultKwhImpact;
                CheckBox_IsActive.Checked = entity.IsActive;

                ItemID = entity.ItemID;
            }
            else
            {
                Panel1.Visible = false;
            }
        }
        public Item FillEntity()
        {
            var entity = new Item();
            if (ItemID != Guid.Empty)
            {
                entity.ItemID = ItemID;
            }
            else
            {
                entity.ItemID = Guid.NewGuid();
            }

            entity.ItemName = RadTextBox_ItemName.Text;
            entity.ItemDescription = RadTextBox_ItemDescription.Text;
            entity.DefaultKwImpact = Convert.ToDecimal(RadNumericTextBox_Default_Kw_Impact.Value);
            entity.DefaultKwhImpact = Convert.ToDecimal(RadNumericTextBox_Default_Kwh_Impact.Value);
            entity.IsActive = CheckBox_IsActive.Checked;

            return entity;
        }
        protected void RadGrid_SavingItems_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                (e.Item as GridEditableItem)["ProjectID"].Visible = false;
                (e.Item as GridEditableItem)["Project"].Enabled = false;
            }
        }
        protected void RadGrid_SavingItems_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            if (e.Column.UniqueName == "Project")
            {
                e.Column.FilterControlWidth = Unit.Pixel(200);//for changing the width of the column                    
            }

            if (e.Column.UniqueName == "ProjectID")
            {
                e.Column.Visible = false;
            }
        }
        protected void RadGrid_SavingItems_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            if ((e.Item is GridEditableItem) && (e.Item.IsInEditMode))
            {
                GridDataItem editItem = (GridDataItem)e.Item;
                var catalogID = (editItem["CatalogID"].Controls[0] as TextBox).Text;
                var projectID = (editItem["ProjectID"].Controls[0] as TextBox).Text;

                if (editItem.SavedOldValues["CatalogID"] != null && editItem.SavedOldValues["CatalogID"].ToString() != catalogID ||
                    editItem.SavedOldValues["CatalogID"] == null)
                {
                    UpdateCatalogID(projectID, catalogID);
                }

                if (ViewState["CompaniesList"] != null)
                {
                    foreach (var company in ViewState["CompaniesList"] as Dictionary<string, string>)
                    {
                        // get if the value is kw or kwh
                        var valueType = (KwType)Enum.Parse(typeof(KwType), company.Key.Substring(company.Key.LastIndexOf(' ')));
                        var oldValue = -1.0;
                        if (editItem.SavedOldValues[company.Key] != null)
                        {
                            double.TryParse(editItem.SavedOldValues[company.Key].ToString(), out oldValue);
                        }
                        var newValue = -1.0;
                        var textValue = (editItem[company.Key].Controls[0] as TextBox).Text;
                        if (!string.IsNullOrEmpty(textValue))
                        {
                            double.TryParse(textValue, out newValue);
                        }
                        if (newValue != oldValue)
                        {
                            // todo: update the value for the pair: itemID, projectID, op companyID
                            UpdateItemSavings(ItemID, projectID, company.Value, valueType, newValue, catalogID);
                        }
                    }
                }

                //take each company and check if the value was modified
            }
        }
        protected void RadGrid_SavingItems_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (e.RebindReason != GridRebindReason.InitialLoad)
            {
                var itemSavings = itemSavingsPresenter.GetProjectAndOperatingCompaniesByState(Int32.Parse(cmbStates.SelectedValue), ItemID, ShowActiveProjectsOnly.Checked);

                RadGrid_SavingItems.DataSource = CreateDataSource(itemSavings);
            }
        }
        protected void RadGrid_SavingItems_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "Copy")
            {
                GridDataItem editItem = (GridDataItem)e.Item;
                var catalogID = editItem["CatalogID"].Text;
                // work arround 
                // telerik bug : http://www.telerik.com/forums/after-upgrade-cells-return-nbsp-values
                catalogID = catalogID == "&nbsp;" ? null : catalogID;
                var projectID = editItem["ProjectID"].Text;

                if (ViewState["CompaniesList"] != null)
                {
                    string format = "{0:n" + RadNumericTextBox_Default_Kwh_Impact.NumberFormat.DecimalDigits + "}";
                    foreach (var company in ViewState["CompaniesList"] as Dictionary<string, string>)
                    {
                        var valueType = (KwType)Enum.Parse(typeof(KwType), company.Key.Substring(company.Key.LastIndexOf(' ')));
                        if (valueType == KwType.Kw)
                        {
                            editItem[company.Key].Text = string.Format(format, RadNumericTextBox_Default_Kw_Impact.Value);
                        }
                        else
                        {
                            editItem[company.Key].Text = string.Format(format, RadNumericTextBox_Default_Kwh_Impact.Value);
                        }

                        UpdateItemSavings(ItemID, projectID, company.Value, valueType, double.Parse(editItem[company.Key].Text), catalogID);
                    }
                }
            }
        }
        protected void States_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbStates.SelectedIndex > 0)
            {
                RadGrid_SavingItems.Rebind();
            }
        }
        protected void ShowActiveProjectsOnly_CheckedChanged(object sender, EventArgs e)
        {
            RadGrid_SavingItems.Rebind();
        }
        #endregion

        #region Helpers
        private void UpdateCatalogID(string projectID, string catalogID)
        {
            try
            {
                itemSavingsPresenter.UpdateItemCatalog(ItemID, projectID, catalogID);
            }
            catch (Exception ex)
            {
                Label l = new Label();
                l.Text = "Unable to update item. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_SavingItems.Controls.Add(l);
            }
        }

        private void UpdateItemSavings(Guid ItemID, string projectID, string operatingCompanyID, KwType valueType, double value, string catalogID)
        {
            try
            {
                itemSavingsPresenter.UpdateItemSavings(ItemID, projectID, operatingCompanyID, valueType, value, Thread.CurrentPrincipal.Identity.Name, catalogID);
            }
            catch (Exception ex)
            {
                Label l = new Label();
                l.Text = "Unable to update item. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_SavingItems.Controls.Add(l);
            }
        }

        private void GenerateBoundColumns(List<ItemSavings> itemSavings)
        {
            if (Session["ColumnsGenerated"] == null ||
                (Session["ColumnsGenerated"] != null && bool.Parse(Session["ColumnsGenerated"].ToString()) == false))
            {
                RadGrid_SavingItems.MasterTableView.AutoGenerateColumns = false;

                var boundColumn = new GridBoundColumn();
                boundColumn.HeaderText = "ProjectID";
                boundColumn.DataField = "ProjectID";
                boundColumn.UniqueName = "ProjectID";
                boundColumn.Visible = false;
                RadGrid_SavingItems.MasterTableView.Columns.Add(boundColumn);

                boundColumn = new GridBoundColumn();
                boundColumn.HeaderText = "Project Name";
                boundColumn.DataField = "Project";
                boundColumn.UniqueName = "Project";
                RadGrid_SavingItems.MasterTableView.Columns.Add(boundColumn);

                boundColumn = new GridBoundColumn();
                boundColumn.HeaderText = "Catalog ID";
                boundColumn.DataField = "CatalogID";
                boundColumn.UniqueName = "CatalogID";
                RadGrid_SavingItems.MasterTableView.Columns.Add(boundColumn);

                var groupedItemsByPrj = itemSavings
                    .GroupBy(g => new Key { Project = g.Project, CatalogID = g.CatalogID })
                    .ToDictionary(it => it.Key, it => it.ToList());

                var companiesList = new List<string>();
                foreach (KeyValuePair<Key, List<ItemSavings>> item in groupedItemsByPrj)
                {
                    foreach (var element in item.Value)
                    {
                        string columnTitleKw = element.OperatingCompanyName + " Kw";
                        string columnTitleKwh = element.OperatingCompanyName + " Kwh";

                        // add distinct columns
                        if (!companiesList.Contains(columnTitleKw))
                        {
                            boundColumn = new GridBoundColumn();
                            boundColumn.HeaderText = columnTitleKw;
                            boundColumn.DataField = "KwImpact";
                            boundColumn.UniqueName = element.OperatingCompanyID.ToString();
                            boundColumn.FilterControlWidth = Unit.Pixel(100);
                            RadGrid_SavingItems.MasterTableView.Columns.Add(boundColumn);

                            boundColumn = new GridBoundColumn();
                            boundColumn.HeaderText = columnTitleKwh;
                            boundColumn.DataField = "KwhImpact";
                            boundColumn.UniqueName = element.OperatingCompanyID.ToString();
                            boundColumn.FilterControlWidth = Unit.Pixel(100);
                            RadGrid_SavingItems.MasterTableView.Columns.Add(boundColumn);

                            companiesList.AddRange(new[] { columnTitleKw, columnTitleKwh });
                        }
                    }
                }

                Session["ColumnsGenerated"] = true;
            }
        }

        private DataTable CreateDataSource(List<ItemSavings> itemSavings)
        {
            try
            {
                var groupedItemsByPrj = itemSavings
                    .GroupBy(g => new Key { Project = g.Project, CatalogID = g.CatalogID, ProjectID = g.ProjectID })
                    .ToDictionary(it => it.Key, it => it.ToList());


                //var sortedGroupedItemsByPrj = groupedItemsByPrj.OrderBy(key => key.Value);
                var companiesList = new Dictionary<string, string>();

                DataTable dataSourceGroupedByPrj = new DataTable("ItemSavings");

                dataSourceGroupedByPrj.Columns.Add(new DataColumn("ProjectID"));
                dataSourceGroupedByPrj.Columns.Add(new DataColumn("Project"));
                dataSourceGroupedByPrj.Columns.Add(new DataColumn("CatalogID"));

                foreach (KeyValuePair<Key, List<ItemSavings>> item in groupedItemsByPrj)
                {
                    foreach (var element in item.Value)
                    {
                        string columnTitleKw = element.OperatingCompanyName + " Kw";
                        string columnTitleKwh = element.OperatingCompanyName + " Kwh";
                        if (!dataSourceGroupedByPrj.Columns.Contains(columnTitleKw) || !dataSourceGroupedByPrj.Columns.Contains(columnTitleKwh))
                        {
                            dataSourceGroupedByPrj.Columns.Add(new DataColumn(columnTitleKw));
                            dataSourceGroupedByPrj.Columns.Add(new DataColumn(columnTitleKwh));

                            companiesList.Add(columnTitleKw, element.OperatingCompanyID.ToString());
                            companiesList.Add(columnTitleKwh, element.OperatingCompanyID.ToString());
                        }
                    }
                }

                ViewState["CompaniesList"] = companiesList;
                DataTable dataSource = PopulateDynamicTable(groupedItemsByPrj, dataSourceGroupedByPrj);
                return dataSource;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private DataTable PopulateDynamicTable(Dictionary<Key, List<ItemSavings>> groupedItemsByPrj, DataTable dataSourceGroupedByPrj)
        {
            try
            {
                var groupedItemsByProject = groupedItemsByPrj
                    .GroupBy(g => new { g.Key.Project, g.Key.CatalogID, g.Key.ProjectID });

                foreach (var item in groupedItemsByProject)
                {
                    DataRow newRow = dataSourceGroupedByPrj.NewRow();
                    newRow[0] = item.Key.ProjectID;
                    newRow[1] = item.Key.Project;
                    newRow[2] = item.Key.CatalogID;

                    int columnIndex = 3;
                    foreach (var element in item)
                    {
                        foreach (var it in element.Value)
                        {
                            string columnTitleKw = it.OperatingCompanyName + " Kw";
                            string columnTitleKwh = it.OperatingCompanyName + " Kwh";
                            int columnIndexKwForCurrentElement = dataSourceGroupedByPrj.Columns[columnTitleKw].Ordinal;
                            int columnIndexKwhForCurrentElement = dataSourceGroupedByPrj.Columns[columnTitleKwh].Ordinal;

                            if (dataSourceGroupedByPrj.Columns.Contains(columnTitleKw))
                            {
                                newRow[columnIndexKwForCurrentElement] = it.KwImpact;
                                columnIndex++;
                            }
                            if (dataSourceGroupedByPrj.Columns.Contains(columnTitleKwh))
                            {
                                newRow[columnIndexKwhForCurrentElement] = it.KwhImpact;
                                columnIndex++;
                            }
                        }
                    }
                    dataSourceGroupedByPrj.Rows.Add(newRow);

                }

                return dataSourceGroupedByPrj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void InitStates()
        {
            if (!cmbStates.Items.Any())
            {
                var states = umPresenter.GetAllStates().ToList();
                states.Insert(0, new State { Id = -1, Name = "Select a state" });
                cmbStates.DataSource = states;
                cmbStates.DataBind();
            }
        }

        private class Key
        {
            public Guid ProjectID { get; set; }
            public string Project { get; set; }
            public string CatalogID { get; set; }
        }
        #endregion
    }
}