﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerFilter.ascx.cs" Inherits="PowerShipping.WebSite.Controls.CustomerFilter" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<div class="jobInfo" runat="server" id="div_jobInfo">
 <div style=" text-align : center;"><b>Customer Filter</b></div>
 <table>
    <tr>
        <td colspan="2" align="center">           
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <asp:Button runat="server" ID="Button_Filter" Text="Search"  Width="80px"
                CssClass="ptButton" onclick="Button_Filter_Click" />
            <asp:Button runat="server" ID="Button_Clear" Text="Clear"  Width="80px"
                CssClass="ptButton" onclick="Button_Clear_Click" />
        </td>
    </tr>
   <tr>
        <td colspan="2" align="center">           
        </td>
    </tr> 
    
    <tr>
        <td class="jobInfo_td">Inv. Code:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_InvitationCode"></telerik:RadTextBox></td>
    </tr> 
    <tr>
        <td class="jobInfo_td">Account#:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_AccountNumber"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="jobInfo_td">Cont. Name:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_AccountName"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="jobInfo_td">Comp Name:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_CompanyName"></telerik:RadTextBox></td>
    </tr>
    
    
    <tr>
        <td class="jobInfo_td">Mailing Address:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_Address"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="jobInfo_td">Mailing Address2:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_Address2"></telerik:RadTextBox></td>
    </tr> 
    <tr>
        <td class="jobInfo_td">Mailing City:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_City"></telerik:RadTextBox></td>
    </tr>     
    <tr>
        <td class="jobInfo_td">Mailing State:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_State"></telerik:RadTextBox></td>
    </tr>    
    <tr>
        <td class="jobInfo_td">Mailing Zip:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_ZipCode"></telerik:RadTextBox></td>
    </tr>   
     
    <tr>
        <td class="jobInfo_td">Email1:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_Email1"></telerik:RadTextBox></td>
    </tr> 
    <tr>
        <td class="jobInfo_td">Email2:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_Email2"></telerik:RadTextBox></td>
    </tr> 
    <tr>
        <td class="jobInfo_td">Phone1:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_Phone1"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="jobInfo_td">Phone2:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_Phone2"></telerik:RadTextBox></td>
    </tr>
    
       
    
    <tr>
        <td class="jobInfo_td">Serv. Addr.1:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_ServiceAddress1"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="jobInfo_td">Serv. Addr.2:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_ServiceAddress2"></telerik:RadTextBox></td>
    </tr>             
     <tr>
        <td class="jobInfo_td">Serv. City:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_ServiceCity"></telerik:RadTextBox></td>
    </tr>     
    <tr>
        <td class="jobInfo_td">Serv. State:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_ServiceState"></telerik:RadTextBox></td>
    </tr>    
    <tr>
        <td class="jobInfo_td">Serv. Zip:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_ServiceZip"></telerik:RadTextBox></td>
    </tr>   
    
    
    
     <tr>
        <td class="jobInfo_td">WaterHeater:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_WaterHeater"></telerik:RadTextBox></td>
    </tr> 
    <tr>
        <td class="jobInfo_td">Operating Company:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_OperatingCompany"></telerik:RadTextBox></td>
    </tr> 
     <tr>
        <td class="jobInfo_td">PremiseID:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_PremiseID"></telerik:RadTextBox></td>
    </tr>  
   
    <tr>
        <td class="jobInfo_td">Opt Out:</td>
        <td class="jobInfo_info">
            <asp:RadioButtonList runat="server" ID="RadioButtonList_OptOut" RepeatDirection="Vertical">
                <asp:ListItem Text="All" Selected="True" Value="-1"></asp:ListItem>
                <asp:ListItem Text="OptOut" Value="1"></asp:ListItem>
                <asp:ListItem Text="Not OptOut" Value="0"></asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>  
    <tr>
        <td class="jobInfo_td">Contact:</td>
        <td class="jobInfo_info">
            <asp:RadioButtonList runat="server" ID="RadioButtonList_Contact" RepeatDirection="Vertical">
                <asp:ListItem Text="All" Selected="True" Value="-1"></asp:ListItem>
                <asp:ListItem Text="With Contact" Value="1"></asp:ListItem>
                <asp:ListItem Text="W.out Contact" Value="0"></asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>   
     <tr>
        <td class="jobInfo_td">Rate Code:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_RateCode"></telerik:RadTextBox></td>
    </tr>  
     
   
    <tr>
        <td colspan="2" align="center">
            <asp:Button runat="server" ID="Button1" Text="Search"  Width="80px"
                CssClass="ptButton" onclick="Button_Filter_Click" />
            <asp:Button runat="server" ID="Button2" Text="Clear"  Width="80px"
                CssClass="ptButton" onclick="Button_Clear_Click" />
        </td>
    </tr>                                
 </table>
</div>