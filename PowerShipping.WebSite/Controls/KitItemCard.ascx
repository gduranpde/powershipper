﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="KitItemCard.ascx.cs"
    Inherits="PowerShipping.WebSite.Controls.KitItemCard" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:HiddenField ID="HiddenField_Id" runat="server" />
<table>
    <tr>
        <td id="tr_Name" runat="server">Name
        </td>
        <td>
            <telerik:radtextbox id="RadTextBox_Name" runat="server">
            </telerik:radtextbox>
        </td>
    </tr>
    <tr id="tr_CatalogId" runat="server">
        <td>CatalogID
        </td>
        <td>
            <telerik:radtextbox id="RadTextBox_CatalogId" runat="server">
            </telerik:radtextbox>
        </td>
    </tr>
    <tr id="tr_Quantity" runat="server">
        <td>Quantity
        </td>
        <%--<td><telerik:RadNumericTextBox ID="RadNumericTextBox_Quantity" runat="server" Value="1" DataType="System.Int" NumberFormat-DecimalDigits="0" ></telerik:RadNumericTextBox></td>--%>
        <td>
            <telerik:radnumerictextbox id="RadNumericTextBox_Quantity" runat="server" value="1" numberformat-
                numberformat-decimaldigits="0">
            </telerik:radnumerictextbox>
        </td>
    </tr>
    <tr id="tr1" runat="server">
        <td>Kw Impact
        </td>
        <td>
            <telerik:radnumerictextbox id="RadNumericTextBox_Kw_Impact" runat="server"
                 AllowRounding="false" KeepNotRoundedValue="true" numberformat-decimaldigits="6">
            </telerik:radnumerictextbox>
        </td>
    </tr>
    <tr id="tr2" runat="server">
        <td style="width:70px">Kwh Impact
        </td>
        <td>
            <telerik:radnumerictextbox id="RadNumericTextBox_Kwh_Impact" runat="server"
                AllowRounding="false" KeepNotRoundedValue="true" numberformat-decimaldigits="6">
            </telerik:radnumerictextbox>
        </td>
    </tr>
    <tr runat="server" id="td_CommandCell">
        <td colspan="2">
            <asp:LinkButton ID="btnUpdate" Text="Save" runat="server" CommandName="Update" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>'
                ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnInsert" Text="Save" runat="server" CommandName="PerformInsert"
                Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>' ValidationGroup="Card"
                Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False"
                CommandName="Cancel" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnDelete" Text="Delete" runat="server" CausesValidation="False"
                CommandName="Cancel" OnClick="btnDelete_OnClick" Font-Size="Medium"></asp:LinkButton>
        </td>
    </tr>
</table>
