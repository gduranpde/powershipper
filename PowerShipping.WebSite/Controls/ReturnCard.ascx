﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReturnCard.ascx.cs"
    Inherits="PowerShipping.WebSite.Controls.ReturnCard" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:HiddenField ID="HiddenField_Id" runat="server" />
<asp:HiddenField ID="HiddenField_ConsumerRequestId" runat="server" />
<table>
    <tr id="tr_AccountName" runat="server">
        <td>
            Name
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_AccountName" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_Address1" runat="server">
        <td>
            Address 1
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_Address1" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_Address2" runat="server">
        <td>
            Address 2
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_Address2" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_City" runat="server">
        <td>
            City
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_City" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_State" runat="server">
        <td>
            State
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_State" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_ZipCode" runat="server">
        <td>
            Zip
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_ZipCode" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_Email" runat="server">
        <td>
            Email
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_Email" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_Phone1" runat="server">
        <td>
            Phone1
        </td>
        <td>
            <telerik:RadMaskedTextBox ID="RadMaskedTextBox_Phone1" runat="server" Mask="###-###-####">
            </telerik:RadMaskedTextBox>
        </td>
    </tr>
    <tr id="tr_Phone2" runat="server">
        <td>
            Phone2
        </td>
        <td>
            <telerik:RadMaskedTextBox ID="RadMaskedTextBox_Phone2" runat="server" Mask="###-###-####">
            </telerik:RadMaskedTextBox>
        </td>
    </tr>
    <tr id="tr_ReshipStatus" runat="server">
        <td>
            ReshipStatus
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBox_ReshipStatus" runat="server" DataValueField="Value"
                DataTextField="Text">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr id="tr1" runat="server">
        <td>
            ReturnDate
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDatePicker_ReturnDate" runat="server" Skin="Telerik"
                Width="135px">
                <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                    ViewSelectorText="x">
                </Calendar>
                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                </DateInput>
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr id="tr_ShippingStatusDate" runat="server">
        <td>
            ShippingStatusDate
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDatePicker_ShippingStatusDate" runat="server" Skin="Telerik"
                Width="135px">
                <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                    ViewSelectorText="x">
                </Calendar>
                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                </DateInput>
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr id="tr_Notes" runat="server">
        <td>
            Notes
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_Notes" runat="server" TextMode="MultiLine" Width="500px"
                Height="50px" MaxLength="1024">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr runat="server" id="tr_Shipper" visible="true">
        <td>
            Shipper
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBox_Shipper" runat="server" DataValueField="Id"
                DataTextField="FullName">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr runat="server" id="tr_Project">
        <td>
            Project
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBox_Project" runat="server" DataValueField="Id"
                DataTextField="FullName" Width="300px" Enabled="false">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr id="tr_Reason" runat="server">
        <td>
            Reason for Return
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBox_Reason" runat="server" DataValueField="ReasonID"
                DataTextField="ReasonFull">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr id="tr_OSAddress1" runat="server">
        <td>
            Address 1
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_OSAddress1" runat="server" ReadOnly="true">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_OSAddress2" runat="server">
        <td>
            Address 2
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_OSAddress2" runat="server" ReadOnly="true">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_OSCity" runat="server">
        <td>
            City
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_OSCity" runat="server" ReadOnly="true">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_OSState" runat="server">
        <td>
            State
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_OSState" runat="server" ReadOnly="true">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr id="tr_OSZip" runat="server">
        <td>
            Zip
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_OSZip" runat="server" ReadOnly="true">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr runat="server" id="td_CommandCell">
        <td colspan="2">
            <asp:LinkButton ID="btnUpdate" Text="Save" runat="server" CommandName="Update" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>'
                ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnInsert" Text="Save" runat="server" CommandName="PerformInsert"
                Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>' ValidationGroup="Card"
                Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False"
                CommandName="Cancel" Font-Size="Medium"></asp:LinkButton>
        </td>
    </tr>
</table>