﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ItemDetailCard.ascx.cs" Inherits="PowerShipping.WebSite.Controls.ItemDetailCard" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:HiddenField ID="HiddenField_Id" runat="server" />


<telerik:RadProgressManager ID="Radprogressmanager1" runat="server" />
<div style="float: left">
    <fieldset style="height: auto; width: 98%" class="rfdRoundedCorners">
        <legend style="font-size: small">Item Details
        </legend>
        <table id="MainTable">
            <tr runat="server" id="TrData">
                <td id="firstColumn">
                    <table id="secondTable">
                        <tr id="tr_ItemName" runat="server">
                            <td>Item Name</td>
                            <td>
                                <telerik:RadTextBox ID="RadTextBox_ItemName" runat="server" Width="300px"></telerik:RadTextBox></td>
                        </tr>

                        <tr id="tr_DefaultKwImpact" runat="server">
                            <td>Default Kw Impact</td>
                            <td>
                                <telerik:RadNumericTextBox ID="RadNumericTextBox_Default_Kw_Impact" runat="server"
                                    AllowRounding="true" KeepNotRoundedValue="true" NumberFormat-DecimalDigits="6">
                                </telerik:RadNumericTextBox>
                            </td>
                        </tr>

                        <tr id="tr_DefaultKwhImpact" runat="server">
                            <td>Default Kwh Impact</td>
                            <td>
                                <telerik:RadNumericTextBox ID="RadNumericTextBox_Default_Kwh_Impact" runat="server"
                                    AllowRounding="false" KeepNotRoundedValue="true" NumberFormat-DecimalDigits="6">
                                </telerik:RadNumericTextBox>
                            </td>
                        </tr>

                        <tr id="tr_ItemDescription" runat="server">
                            <td>Description</td>
                            <td>
                                <telerik:RadTextBox Width="400px" ID="RadTextBox_ItemDescription" runat="server"
                                    TextMode="MultiLine" Resize="Both" Height="100px">
                                </telerik:RadTextBox></td>
                        </tr>
                    </table>
                </td>
                <td id="secondColumn">
                    <table id="Table1" style="margin: 30px">
                        <tr id="tr_CreatedBy" runat="server">
                            <td align="right">Created By:
                            </td>
                            <td>
                                <%#Eval("CreatedBy")%>
                            </td>
                        </tr>
                        <tr id="tr_CreatedDate" runat="server">
                            <td align="right">Created Date:</td>
                            <td>
                                <%#Eval("CreatedDate")%>
                            </td>
                        </tr>

                        <tr id="tr_ModifiedBY" runat="server">
                            <td align="right">Last Modified By: </td>
                            <td>
                                <%#Eval("LastModifiedBy")%>
                            </td>
                        </tr>

                        <tr id="tr_ModifiedDate" runat="server">
                            <td align="right">Last Modified Date: 
                            </td>
                            <td>
                                <%#Eval("LastModifiedDate")%>
                            </td>
                        </tr>

                        <tr id="tr_Active" runat="server">
                            <td align="right">IsActive</td>
                            <td>
                                <asp:CheckBox ID="CheckBox_IsActive" runat="server" Checked="true"></asp:CheckBox></td>
                        </tr>
                        <tr runat="server" id="td_CommandCell" valign="bottom" style="height: 50px">
                            <td colspan="2">
                                <asp:LinkButton ID="btnSave" runat="server" Text="Save" CommandName="Update" Font-Size="Medium" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>'></asp:LinkButton>
                                <asp:LinkButton ID="btnInsert" Text="Save&Return" runat="server" CommandName="PerformInsert" Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
                                <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False" CommandName="Cancel" Font-Size="Medium"></asp:LinkButton>
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>

        </table>
    </fieldset>
</div>

<asp:Panel ID="Panel1" runat="server">
    <fieldset style="height: auto; width: 98%" class="rfdRoundedCorners">
        <legend style="font-size: small">Savings by Project & Operating Company</legend>
        <br />
        <telerik:RadComboBox ID="cmbStates" runat="server" DataValueField="Id" DataTextField="Name" MaxHeight="350" Label="State: "
            AutoPostBack="true" OnSelectedIndexChanged="States_SelectedIndexChanged">
        </telerik:RadComboBox>
        <div id="checkBox" style="float: right">
            <asp:CheckBox ID="ShowActiveProjectsOnly" runat="server" Checked="true" AutoPostBack="true"
                OnCheckedChanged="ShowActiveProjectsOnly_CheckedChanged" Text="Show Active Projects Only"></asp:CheckBox>
        </div>
        <br />

        <div>
            <telerik:RadGrid ID="RadGrid_SavingItems" runat="server" MasterTableView-TableLayout="Fixed"
                AllowMultiRowEdit="true"
                PageSize="15" AllowPaging="True" Skin="Windows7"
                OnNeedDataSource="RadGrid_SavingItems_NeedDataSource"
                AllowFilteringByColumn="false"                
                OnItemCreated="RadGrid_SavingItems_ItemDataBound"
                OnColumnCreated="RadGrid_SavingItems_ColumnCreated"
                OnUpdateCommand="RadGrid_SavingItems_UpdateCommand"
                OnItemCommand="RadGrid_SavingItems_ItemCommand">
                <ClientSettings>
                    <Scrolling AllowScroll="True" UseStaticHeaders="true" SaveScrollPosition="true" FrozenColumnsCount="2"></Scrolling>
                </ClientSettings>
                <MasterTableView CommandItemDisplay="None" EditMode="InPlace">
                    <Columns>
                        <telerik:GridButtonColumn CommandName="Copy" Text="Copy" UniqueName="Copy" HeaderText="Copy">
                        </telerik:GridButtonColumn>
                        <telerik:GridEditCommandColumn></telerik:GridEditCommandColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </div>
        <br />        
    </fieldset>
</asp:Panel>
