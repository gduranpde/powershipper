﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TrackingExceptionCard.ascx.cs" Inherits="PowerShipping.WebSite.Controls.TrackingExceptionCard" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:HiddenField  ID="HiddenField_ConsumerRequestExceptionId" runat="server" />
<table>
     <tr>
        <td id="tr_PurchaseOrderNumber" runat="server">Purchase Order Number</td>
        <td><telerik:RadTextBox ID="RadTextBox_PurchaseOrderNumber" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_FedExTrackingNumber" runat="server">
        <td>Tracking Number</td>
        <td><telerik:RadTextBox ID="RadTextBox_FedExTrackingNumber" runat="server"></telerik:RadTextBox></td>
    </tr>  
    <tr>
        <td id="tr_AccountNumber" runat="server">Account Number</td>
        <td><telerik:RadTextBox ID="RadTextBox_AccountNumber" runat="server"></telerik:RadTextBox></td>
    </tr>
    
    <tr id="tr_AccountName" runat="server">
        <td>Name</td>
        <td><telerik:RadTextBox ID="RadTextBox_AccountName" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_Address1" runat="server">
        <td>Address1</td>
        <td><telerik:RadTextBox ID="RadTextBox_Address1" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr runat="server" id="tr_Address2">
        <td>Address2</td>
        <td><telerik:RadTextBox ID="RadTextBox_Address2" runat="server"></telerik:RadTextBox></td>
    </tr>    
    <tr id="tr_City" runat="server">
        <td>City</td>
        <td><telerik:RadTextBox ID="RadTextBox_City" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_State" runat="server">
        <td>State</td>
        <td><telerik:RadTextBox ID="RadTextBox_State" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_ZipCode" runat="server">
        <td>ZipCode</td>
        <td><telerik:RadTextBox ID="RadTextBox_ZipCode" runat="server"></telerik:RadTextBox></td>
    </tr>
   
    <tr id="tr_Reference" runat="server">
        <td>Reference</td>
        <td><telerik:RadTextBox ID="RadTextBox_Reference" runat="server"></telerik:RadTextBox></td>
    </tr>   
     
    <tr runat="server" id="td_CommandCell">
        <td colspan="2">            
            <asp:LinkButton id="btnUpdate" text="Save" runat="server" CommandName="Update" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton id="btnInsert" text="Save" runat="server" CommandName="PerformInsert" Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnCancel" text="Cancel" runat="server" causesvalidation="False" commandname="Cancel" Font-Size="Medium"></asp:LinkButton>
        </td>
    </tr>                      
</table>