﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConsumerRequestFilter.ascx.cs"
    Inherits="PowerShipping.WebSite.Controls.ConsumerRequestFilter" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div class="jobInfo" runat="server" id="div_jobInfo">
    <div style="text-align: center;">
        <b>Consumer Request Filter</b>
    </div>
    <table>
        <tr>
            <td colspan="2" align="center"></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button runat="server" ID="Button_Filter" Text="Search" Width="80px" CssClass="ptButton"
                    OnClick="Button_Filter_Click" />
                <asp:Button runat="server" ID="Button_Clear" Text="Clear" Width="80px" CssClass="ptButton"
                    OnClick="Button_Clear_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center"></td>
        </tr>
        <tr>
            <td class="jobInfo_td">Status:
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:ListBox runat="server" Rows="5" SelectionMode="Multiple" ID="ListBox_Status"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">Batch ID:
            </td>
            <td class="jobInfo_info">
                <telerik:radtextbox width="100px" runat="server" id="RadTextBox_BatchId">
                </telerik:radtextbox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">Account #:
            </td>
            <td class="jobInfo_info">
                <telerik:radtextbox width="100px" runat="server" id="RadTextBox_AccountNumber">
                </telerik:radtextbox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">Cont. Name:
            </td>
            <td class="jobInfo_info">
                <telerik:radtextbox width="100px" runat="server" id="RadTextBox_AccountName">
                </telerik:radtextbox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">Comp Name:
            </td>
            <td class="jobInfo_info">
                <telerik:radtextbox width="100px" runat="server" id="RadTextBox_CompanyName">
                </telerik:radtextbox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">Address:
            </td>
            <td class="jobInfo_info">
                <telerik:radtextbox width="100px" runat="server" id="RadTextBox_Address">
                </telerik:radtextbox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">Address2:
            </td>
            <td class="jobInfo_info">
                <telerik:radtextbox width="100px" runat="server" id="RadTextBox_Address2">
                </telerik:radtextbox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">City:
            </td>
            <td class="jobInfo_info">
                <telerik:radtextbox width="100px" runat="server" id="RadTextBox_City">
                </telerik:radtextbox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">State:
            </td>
            <td class="jobInfo_info">
                <telerik:radtextbox width="100px" runat="server" id="RadTextBox_State">
                </telerik:radtextbox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">Zip:
            </td>
            <td class="jobInfo_info">
                <telerik:radtextbox width="100px" runat="server" id="RadTextBox_ZipCode">
                </telerik:radtextbox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">Email:
            </td>
            <td class="jobInfo_info">
                <telerik:radtextbox width="100px" runat="server" id="RadTextBox_Email">
                </telerik:radtextbox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">Serv. Addr.1:
            </td>
            <td class="jobInfo_info">
                <telerik:radtextbox width="100px" runat="server" id="RadTextBox_ServiceAddress1">
                </telerik:radtextbox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">Phone1:
            </td>
            <td class="jobInfo_info">
                <telerik:radtextbox width="100px" runat="server" id="RadTextBox_Phone1">
                </telerik:radtextbox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">Premise ID:
            </td>
            <td class="jobInfo_info">
                <telerik:radtextbox width="100px" runat="server" id="RadTextBox_PremiseID">
                </telerik:radtextbox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">Method:
            </td>
            <td class="jobInfo_info">
                <telerik:radtextbox width="100px" runat="server" id="RadTextBox_Method">
                </telerik:radtextbox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">Reship:
            </td>
            <td class="jobInfo_info">
                <asp:RadioButtonList runat="server" ID="RadioButtonList_Reship" RepeatDirection="Vertical">
                    <asp:ListItem Text="All" Selected="True" Value="-1"></asp:ListItem>
                    <asp:ListItem Text="Reship" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Not Reship" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">Opt Out:
            </td>
            <td class="jobInfo_info">
                <asp:RadioButtonList runat="server" ID="RadioButtonList_OptOut" RepeatDirection="Vertical">
                    <asp:ListItem Text="All" Selected="True" Value="-1"></asp:ListItem>
                    <asp:ListItem Text="OptOut" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Not OptOut" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">Contact:
            </td>
            <td class="jobInfo_info">
                <asp:RadioButtonList runat="server" ID="RadioButtonList_Contact" RepeatDirection="Vertical">
                    <asp:ListItem Text="All" Selected="True" Value="-1"></asp:ListItem>
                    <asp:ListItem Text="With Contact" Value="1"></asp:ListItem>
                    <asp:ListItem Text="W.out Contact" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <b>Analysis Date:</b>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:CheckBox ID="cbAnalysisDate" runat="server" Text="Is NULL" />
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2">From:
                <telerik:raddatepicker id="RadDatePicker_AnalysisStartDate" runat="server" skin="Telerik"
                    width="120px">
                    <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                        ViewSelectorText="x">
                    </Calendar>
                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                    </DateInput>
                </telerik:raddatepicker>
                <br />
                To:
                <telerik:raddatepicker id="RadDatePicker_AnalysisEndDate" runat="server" skin="Telerik"
                    width="120px">
                    <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                        ViewSelectorText="x">
                    </Calendar>
                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                    </DateInput>
                </telerik:raddatepicker>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <b>Receipt Date:</b>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:CheckBox ID="cbReceiptDate" runat="server" Text="Is NULL" />
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2">From:
                <telerik:raddatepicker id="RadDatePicker_RecipeStartDate" runat="server" skin="Telerik"
                    width="120px">
                    <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                        ViewSelectorText="x">
                    </Calendar>
                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                    </DateInput>
                </telerik:raddatepicker>
                <br />
                To:
                <telerik:raddatepicker id="RadDatePicker_RecipeEndDate" runat="server" skin="Telerik"
                    width="120px">
                    <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                        ViewSelectorText="x">
                    </Calendar>
                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                    </DateInput>
                </telerik:raddatepicker>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <b>Import Date:</b>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:CheckBox ID="cbImportDate" runat="server" Text="Is NULL" />
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2">From:
                <telerik:raddatepicker id="RadDatePicker_ImportStartDate" runat="server" skin="Telerik"
                    width="120px">
                    <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                        ViewSelectorText="x">
                    </Calendar>
                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                    </DateInput>
                </telerik:raddatepicker>
                <br />
                To:
                <telerik:raddatepicker id="RadDatePicker_ImportEndDate" runat="server" skin="Telerik"
                    width="120px">
                    <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                        ViewSelectorText="x">
                    </Calendar>
                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                    </DateInput>
                </telerik:raddatepicker>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <b>Audit Failure Date</b>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:CheckBox ID="cbAuditFailureDate" runat="server" Text="Is NULL" />
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2">From:
                <telerik:raddatepicker id="RadDatePicker_AuditFailureStartDate" runat="server" skin="Telerik"
                    width="120px">
                    <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                        ViewSelectorText="x">
                    </Calendar>
                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                    </DateInput>
                </telerik:raddatepicker>
                <br />
                To:
                <telerik:raddatepicker id="RadDatePicker_AuditFailureEndDate" runat="server" skin="Telerik"
                    width="120px">
                    <Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                        ViewSelectorText="x">
                    </Calendar>
                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    <DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
                    </DateInput>
                </telerik:raddatepicker>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">WaterHeater:
            </td>
            <td class="jobInfo_info">
                <telerik:radtextbox width="100px" runat="server" id="RadTextBox_WaterHeater">
                </telerik:radtextbox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">Heater Fuel:
            </td>
            <td class="jobInfo_info">
                <telerik:radtextbox width="100px" runat="server" id="RadTextBox_HeaterFuel">
                </telerik:radtextbox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">Operating Company:
            </td>
            <td class="jobInfo_info">
                <telerik:radtextbox width="100px" runat="server" id="RadTextBox_OperatingCompany">
                </telerik:radtextbox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">Notes:
            </td>
            <td class="jobInfo_info">
                <telerik:radtextbox width="100px" runat="server" id="RadTextBox_Notes">
                </telerik:radtextbox>
            </td>
        </tr>
        <tr>
            <td class="jobInfo_td">Account#Old:
            </td>
            <td class="jobInfo_info">
                <telerik:radtextbox width="100px" runat="server" id="RadTextBox_AccountNumberOld">
                </telerik:radtextbox>
            </td>
        </tr>
        <%--PG31--%>
        <tr>
            <td class="jobInfo_td">
                <asp:Label ID="LblFacilityType" runat="server" Text="Facility Type:"></asp:Label></td>
            <td>
                <asp:TextBox ID="TxtFacilityType" runat="server" Width="100px"></asp:TextBox></td>
        </tr>
         <tr>
            <td class="jobInfo_td">Requested Kit:
            </td>
            <td class="jobInfo_info">
                <telerik:radtextbox width="100px" runat="server" id="RadTextBox_RequestedKit">
                </telerik:radtextbox>
            </td>
        </tr>
        <%--PG31--%>
        <tr>
            <td class="jobInfo_td">Full View:
            </td>
            <td class="jobInfo_info">
                <asp:CheckBox ID="chbFullView" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button runat="server" ID="Button1" Text="Search" Width="80px" CssClass="ptButton"
                    OnClick="Button_Filter_Click" />
                <asp:Button runat="server" ID="Button2" Text="Clear" Width="80px" CssClass="ptButton"
                    OnClick="Button_Clear_Click" />
            </td>
        </tr>
    </table>
</div>
