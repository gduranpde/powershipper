﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using System.Configuration;

namespace PowerShipping.WebSite.Controls
{
    public partial class CustomerFilter : System.Web.UI.UserControl
    {
        public delegate void CustomerFilterHandler(string customQuery);
        public event CustomerFilterHandler OnFilter;

        public delegate void CustomerFilterClearHandler(string customQuery);
        public event CustomerFilterClearHandler OnClear;

        CustomerPresenter presenter = new CustomerPresenter();

        public string DefaultQuery
        {
            get { return "select * from [vw_Customer]"; }
        }

        public string CurrentQuery
        {
            get
            {
                return BuildQuery();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
               
            }
        }

        protected void Button_Filter_Click(object sender, EventArgs e)
        {
            string query = BuildQuery();
            OnFilter(query);
        }

        protected void Button_Clear_Click(object sender, EventArgs e)
        {
            Clear();
            string query = BuildQuery();
            OnClear(query);
        }

        private void Clear()
        {
            RadTextBox_InvitationCode.Text = null;
            RadTextBox_AccountNumber.Text = null;
            RadTextBox_AccountName.Text = null;
            RadTextBox_CompanyName.Text = null;

            RadTextBox_Address.Text = null;
            RadTextBox_Address2.Text = null;
            RadTextBox_City.Text = null;
            RadTextBox_State.Text = null;
            RadTextBox_ZipCode.Text = null;

            RadTextBox_Email1.Text = null;
            RadTextBox_Phone1.Text = null;
            RadTextBox_Email2.Text = null;
            RadTextBox_Phone2.Text = null;

            RadTextBox_ServiceAddress1.Text = null;
            RadTextBox_ServiceAddress2.Text = null;
            RadTextBox_ServiceCity.Text = null;
            RadTextBox_ServiceState.Text = null;
            RadTextBox_ServiceZip.Text = null;

            RadTextBox_WaterHeater.Text = null;
            RadTextBox_OperatingCompany.Text = null;

            RadTextBox_PremiseID.Text = null;
           
           
            RadioButtonList_OptOut.SelectedIndex = 0;
            RadioButtonList_Contact.SelectedIndex = 0;

			RadTextBox_RateCode.Text = null;
        }

        private string BuildQuery()
        {
            StringBuilder sb = new StringBuilder();
            string maxReturnedRows = ConfigurationManager.AppSettings.Get("MaxReturnedRowNumber");
            sb.AppendFormat(String.Format("select TOP {0} * from [vw_Customer] where 1 = 1   ", maxReturnedRows));
            if (!string.IsNullOrEmpty(RadTextBox_InvitationCode.Text.Trim()))
            {
                if (RadTextBox_InvitationCode.Text.Trim() == "null")
                    sb.AppendFormat(" and ([InvitationCode] IS NULL OR [InvitationCode] = '')");
                else
                    sb.AppendFormat(" and ([InvitationCode] LIKE '%'+ '{0}' +'%')   ", RadTextBox_InvitationCode.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_AccountNumber.Text.Trim()))
            {
                if (RadTextBox_AccountNumber.Text.Trim() == "null")
                    sb.AppendFormat(" and ([AccountNumber] IS NULL OR [AccountNumber] = '')");
                else
                    sb.AppendFormat(" and ([AccountNumber] LIKE '%'+ '{0}' +'%')   ", RadTextBox_AccountNumber.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_AccountName.Text.Trim()))
            {
                if (RadTextBox_AccountName.Text.Trim() == "null")
                    sb.AppendFormat(" and ([ContactName] IS NULL OR [ContactName] = '')");
                else
                    sb.AppendFormat(" and ([ContactName] LIKE '%'+ '{0}' +'%')   ", RadTextBox_AccountName.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_CompanyName.Text.Trim()))
            {
                if (RadTextBox_CompanyName.Text.Trim() == "null")
                    sb.AppendFormat(" and ([CompanyName] IS NULL OR [CompanyName] = '')");
                else
                    sb.AppendFormat(" and ([CompanyName] LIKE '%'+ '{0}' +'%')   ", RadTextBox_CompanyName.Text);
            }


            if (!string.IsNullOrEmpty(RadTextBox_Address.Text.Trim()))
            {
                if (RadTextBox_Address.Text.Trim() == "null")
                    sb.AppendFormat(" and ([MailingAddress1] IS NULL OR [MailingAddress1] = '')");
                else
                    sb.AppendFormat(" and ([MailingAddress1] LIKE '%'+ '{0}' +'%')   ", RadTextBox_Address.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_Address2.Text.Trim()))
            {
                if (RadTextBox_Address2.Text.Trim() == "null")
                    sb.AppendFormat(" and ([MailingAddress2] IS NULL OR [MailingAddress2] = '')");
                else
                    sb.AppendFormat(" and ([MailingAddress2] LIKE '%'+ '{0}' +'%')   ", RadTextBox_Address2.Text);
            }

            if (!string.IsNullOrEmpty(RadTextBox_City.Text.Trim()))
            {
                if (RadTextBox_City.Text.Trim() == "null")
                    sb.AppendFormat(" and ([MailingCity] IS NULL OR [MailingCity] = '')");
                else
                    sb.AppendFormat(" and ([MailingCity] LIKE '%'+ '{0}' +'%')   ", RadTextBox_City.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_State.Text.Trim()))
            {
                if (RadTextBox_State.Text.Trim() == "null")
                    sb.AppendFormat(" and ([MailingState] IS NULL OR [MailingState] = '')");
                else
                    sb.AppendFormat(" and ([MailingState] LIKE '%'+ '{0}' +'%')   ", RadTextBox_State.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_ZipCode.Text.Trim()))
            {
                if (RadTextBox_ZipCode.Text.Trim() == "null")
                    sb.AppendFormat(" and ([MailingZip] IS NULL OR [MailingZip] = '')");
                else
                    sb.AppendFormat(" and ([MailingZip] LIKE '%'+ '{0}' +'%')   ", RadTextBox_ZipCode.Text);
            }


            if (!string.IsNullOrEmpty(RadTextBox_Email1.Text.Trim()))
            {
                if (RadTextBox_Email1.Text.Trim() == "null")
                    sb.AppendFormat(" and ([Email1] IS NULL OR [Email1] = '')");
                else
                    sb.AppendFormat(" and ([Email1] LIKE '%'+ '{0}' +'%')   ", RadTextBox_Email1.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_Email2.Text.Trim()))
            {
                if (RadTextBox_Email2.Text.Trim() == "null")
                    sb.AppendFormat(" and ([Email2] IS NULL OR [Email2] = '')");
                else
                    sb.AppendFormat(" and ([Email2] LIKE '%'+ '{0}' +'%')   ", RadTextBox_Email2.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_Phone1.Text.Trim()))
            {
                if (RadTextBox_Phone1.Text.Trim() == "null")
                    sb.AppendFormat(" and ([Phone1] IS NULL OR [Phone1] = '')");
                else
                    sb.AppendFormat(" and ([Phone1] LIKE '%'+ '{0}' +'%')   ", RadTextBox_Phone1.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_Phone2.Text.Trim()))
            {
                if (RadTextBox_Phone2.Text.Trim() == "null")
                    sb.AppendFormat(" and ([Phone2] IS NULL OR [Phone2] = '')");
                else
                    sb.AppendFormat(" and ([Phone2] LIKE '%'+ '{0}' +'%')   ", RadTextBox_Phone2.Text);
            }



            if (!string.IsNullOrEmpty(RadTextBox_ServiceAddress1.Text.Trim()))
            {
                if (RadTextBox_ServiceAddress1.Text.Trim() == "null")
                    sb.AppendFormat(" and ([ServiceAddress1] IS NULL OR [ServiceAddress1] = '')");
                else
                    sb.AppendFormat(" and ([ServiceAddress1] LIKE '%'+ '{0}' +'%')   ", RadTextBox_ServiceAddress1.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_ServiceAddress2.Text.Trim()))
            {
                if (RadTextBox_ServiceAddress2.Text.Trim() == "null")
                    sb.AppendFormat(" and ([ServiceAddress2] IS NULL OR [ServiceAddress2] = '')");
                else
                    sb.AppendFormat(" and ([ServiceAddress2] LIKE '%'+ '{0}' +'%')   ", RadTextBox_ServiceAddress2.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_ServiceCity.Text.Trim()))
            {
                if (RadTextBox_ServiceCity.Text.Trim() == "null")
                    sb.AppendFormat(" and ([ServiceCity] IS NULL OR [ServiceCity] = '')");
                else
                    sb.AppendFormat(" and ([ServiceCity] LIKE '%'+ '{0}' +'%')   ", RadTextBox_ServiceCity.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_ServiceState.Text.Trim()))
            {
                if (RadTextBox_ServiceState.Text.Trim() == "null")
                    sb.AppendFormat(" and ([ServiceState] IS NULL OR [ServiceState] = '')");
                else
                    sb.AppendFormat(" and ([ServiceState] LIKE '%'+ '{0}' +'%')   ", RadTextBox_ServiceState.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_ServiceZip.Text.Trim()))
            {
                if (RadTextBox_ServiceZip.Text.Trim() == "null")
                    sb.AppendFormat(" and ([ServiceZip] IS NULL OR [ServiceZip] = '')");
                else
                    sb.AppendFormat(" and ([ServiceZip] LIKE '%'+ '{0}' +'%')   ", RadTextBox_ServiceZip.Text);
            }

            if (!string.IsNullOrEmpty(RadTextBox_WaterHeater.Text.Trim()))
            {
                if (RadTextBox_WaterHeater.Text.Trim() == "null")
                    sb.AppendFormat(" and ([WaterHeaterType] IS NULL OR [WaterHeaterType] = '')");
                else
                    sb.AppendFormat(" and ([WaterHeaterType] LIKE '%'+ '{0}' +'%')   ", RadTextBox_WaterHeater.Text);
            }

            if (!string.IsNullOrEmpty(RadTextBox_OperatingCompany.Text.Trim()))
            {
                if (RadTextBox_OperatingCompany.Text.Trim() == "null")
                    sb.AppendFormat(" and ([OperatingCompany] IS NULL OR [OperatingCompany] = '')");
                else
                    sb.AppendFormat(" and ([OperatingCompany] LIKE '%'+ '{0}' +'%')   ", RadTextBox_OperatingCompany.Text);
            }

            if (!string.IsNullOrEmpty(RadTextBox_PremiseID.Text.Trim()))
            {
                if (RadTextBox_PremiseID.Text.Trim() == "null")
                    sb.AppendFormat(" and ([PremiseID] IS NULL OR [PremiseID] = '')");
                else
                    sb.AppendFormat(" and ([PremiseID] LIKE '%'+ '{0}' +'%')   ", RadTextBox_PremiseID.Text);
            }

            if (this.RadioButtonList_OptOut.SelectedValue != "-1")
            {
                sb.AppendFormat(" and ([OptOut] = '{0}')   ", this.RadioButtonList_OptOut.SelectedValue);
            }

            if (this.RadioButtonList_Contact.SelectedValue != "-1")
            {
                sb.AppendFormat(" and ([OkToContact] = '{0}')   ", this.RadioButtonList_Contact.SelectedValue);
            }


			if (!string.IsNullOrEmpty(RadTextBox_RateCode.Text.Trim()))
			{
				if (RadTextBox_RateCode.Text.Trim() == "null")
					sb.AppendFormat(" and ([RateCode] IS NULL OR [RateCode] = '')");
				else
					sb.AppendFormat(" and ([RateCode] LIKE '%'+ '{0}' +'%')   ", RadTextBox_RateCode.Text);
			}

            return sb.ToString();

        }
    }
}