﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class CustomerCard : System.Web.UI.UserControl
    {
        private object _dataItem = null;
        private CustomerPresenter presenter = new CustomerPresenter();
        private UserManagementPresenter presenterAdv = new UserManagementPresenter();

        public Guid CustomerId
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_Id.Value))
                           ? new Guid(HiddenField_Id.Value)
                           : Guid.Empty;
            }
            set { HiddenField_Id.Value = value.ToString(); }
        }

        public object DataItem
        {
            get { return _dataItem; }
            set { _dataItem = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!Common.IsCanEdit())
            {
               // btnUpdate.Visible = false;
                //btnInsert.Visible = false;
               // btnDelete.Visible = false;
            }

            base.OnPreRender(e);
        }

        protected override void OnInit(EventArgs e)
        {
            //btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');");

            InitializeComponent();
            base.OnInit(e);
        }
        

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                Customer cr = presenter.GetById(CustomerId);

                presenter.Delete(cr.Id);
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to delete Customer. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                Helper.ErrorInGrid("RadGrid_Customers", Page, l);
            }
        }

        private void InitializeComponent()
        {
            DataBinding += new EventHandler(Init_DataBinding);
        }

        private void Init_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        public void InitData()
        {
            InitDropDowns();

            if ((DataItem != null) & (DataItem.GetType() != typeof(GridInsertionObject)))
            {
                var entity = (Customer)DataItem;

                RadTextBox_InvitationCode.Text = entity.InvitationCode;
                RadTextBox_AccountNumber.Text = entity.AccountNumber;
                RadTextBox_CompanyName.Text = entity.CompanyName;
                RadTextBox_AccountName.Text = entity.ContactName;
                RadTextBox_Email.Text = entity.Email1;
                RadTextBox_Email2.Text = entity.Email2;
                RadTextBox_Address1.Text = entity.MailingAddress1;
                RadTextBox_Address2.Text = entity.MailingAddress2;
                RadTextBox_City.Text = entity.MailingCity;
                RadTextBox_State.Text = entity.MailingState;
                RadTextBox_ZipCode.Text = entity.MailingZip;
                if (entity.OkToContact != null)
                    RadComboBox_OkContact.SelectedValue = entity.OkToContact.ToString();
                RadTextBox_OperatingCompany.Text = entity.OperatingCompany;
                if (entity.OptOut != null)
                    RadComboBox_OptOut.SelectedValue = entity.OptOut.ToString();
                RadMaskedTextBox_Phone1.Text = entity.Phone1;
                RadMaskedTextBox_Phone2.Text = entity.Phone2;
                RadTextBox_PremiseID.Text = entity.PremiseID;
                RadComboBox_Project.SelectedValue = entity.ProjectId.ToString();
                RadTextBox_ServiceAddress1.Text = entity.ServiceAddress1;
                RadTextBox_ServiceAddress2.Text = entity.ServiceAddress2;
                RadTextBox_ServiceCity.Text = entity.ServiceCity;
                RadTextBox_ServiceState.Text = entity.ServiceState;
                RadTextBox_ServiceZip.Text = entity.ServiceZip;
                RadTextBox_WaterHeaterFuel.Text = entity.WaterHeaterType;
				RadTextBox_RateCode.Text = entity.RateCode;
                
                CustomerId = entity.Id;
                SetVisibility(true);
            }
            else
            {
                SetVisibility(false);
            }
        }

        public Customer FillEntity()
        {
            var entity = new Customer();
            if (CustomerId != Guid.Empty)
            {
                entity.Id = CustomerId;
            }
            else
            {
                entity.Id = Guid.NewGuid();
            }
            entity.InvitationCode = RadTextBox_InvitationCode.Text;
            entity.AccountNumber = RadTextBox_AccountNumber.Text;
            entity.CompanyName = RadTextBox_CompanyName.Text;
            entity.ContactName = RadTextBox_AccountName.Text;
            entity.Email1 = RadTextBox_Email.Text;
            entity.Email2 = RadTextBox_Email2.Text;
            entity.MailingAddress1 = RadTextBox_Address1.Text;
            entity.MailingAddress2 = RadTextBox_Address2.Text;
            entity.MailingCity = RadTextBox_City.Text;
            entity.MailingState = RadTextBox_State.Text;
            entity.MailingZip = RadTextBox_ZipCode.Text;

            if (!string.IsNullOrEmpty(RadComboBox_OkContact.SelectedValue))
                entity.OkToContact = Convert.ToBoolean(RadComboBox_OkContact.SelectedValue);
            else
                entity.OkToContact = null;

            entity.OperatingCompany = RadTextBox_OperatingCompany.Text;

            if (!string.IsNullOrEmpty(RadComboBox_OptOut.SelectedValue))
                entity.OptOut = Convert.ToBoolean(RadComboBox_OptOut.SelectedValue);
            else
                entity.OptOut = null;

            entity.Phone1 = RadMaskedTextBox_Phone1.Text;
            entity.Phone2 = RadMaskedTextBox_Phone2.Text;
            entity.PremiseID = RadTextBox_PremiseID.Text;
            entity.ServiceAddress1 = RadTextBox_ServiceAddress1.Text;
            entity.ServiceAddress2 = RadTextBox_ServiceAddress2.Text;
            entity.ServiceCity = RadTextBox_ServiceCity.Text;
            entity.ServiceState = RadTextBox_ServiceState.Text;
            entity.ServiceZip = RadTextBox_ServiceZip.Text;
            entity.WaterHeaterType = RadTextBox_WaterHeaterFuel.Text;
			entity.RateCode = RadTextBox_RateCode.Text;
        
            entity.ProjectId = new Guid(RadComboBox_Project.SelectedValue);

            return entity;
        }

        public void DeleteEntity()
        {
        }

        private void InitDropDowns()
        {
            User u = presenterAdv.GetOne(HttpContext.Current.User.Identity.Name);
            List<Project> list = new List<Project>();

            if (u.Roles.Contains(PDMRoles.ROLE_Customer))
                list = presenterAdv.GetProjectByUser(u.UserID);
            else
                list = presenterAdv.GetProjectByUser(null);

            if (DataItem as OptOut == null)
            {
                list.Insert(0, new Project() { Id = Guid.Empty });
            }

            RadComboBox_Project.DataSource = list;
            RadComboBox_Project.DataBind();
        }

        private void SetVisibility(bool forUpdate)
        {
            RadComboBox_Project.Enabled = false;

            if (forUpdate)
            {
                //RadTextBox_AccountNumber.ReadOnly = false;
                
            }
            else
            {
                //RadTextBox_AccountNumber.ReadOnly = false;
              
            }
        }
    }
}