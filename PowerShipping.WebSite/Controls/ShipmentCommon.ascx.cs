﻿using System;
using System.Collections.Generic;
using System.Linq;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;

namespace PowerShipping.WebSite.Controls
{
    public partial class ShipmentCommon : System.Web.UI.UserControl
    {
        KitTypesPresenter presenter = new KitTypesPresenter();
        UserManagementPresenter presenterAdv = new UserManagementPresenter();
        ProjectPresenter presenterProj = new ProjectPresenter();

        private List<Guid> listOfSelectedItems = new List<Guid>();

        public List<Guid> ListOfSelectedItems
        {
            get { return listOfSelectedItems; }
            set { listOfSelectedItems = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!Page.IsPostBack)
            //    InitData();
            if(Session["NewLabelRequestIds"] == null)
            {
                lbNumOfLabelsText.Visible = false;
                RadTextBox_CountToSelect.Visible = false;
                lbMaxResults.Visible = false;
            }
        }

        public void InitData()
        {
            if (Session["NewLabelRequestIds"] != null)
                lbMaxResults.Text = "(Max = " + ((List<Guid>)Session["NewLabelRequestIds"]).Count.ToString() + ")";

            this.RadDatePicker_DateShipped.SelectedDate = DateTime.Now;
            this.RadDatePicker_DateLabelNeeded.SelectedDate = DateTime.Now;

            if (true)
            {
                
            }
            this.RadComboBox_KitType.DataSource = presenter.GetAll();
            this.RadComboBox_KitType.DataBind();

            RadComboBox_Shipper.DataSource = presenterAdv.GetAllShipers();
            RadComboBox_Shipper.DataBind();
        }


        public Entities.Shipment GetEntity()
        {
            PowerShipping.Entities.Shipment entity = new PowerShipping.Entities.Shipment();
            entity.KitTypeId = new Guid(this.RadComboBox_KitType.SelectedValue);
            entity.DateCustomerWillShip = this.RadDatePicker_DateShipped.SelectedDate.Value;
            entity.PurchaseOrderNumber = this.RadTextBox_PONumber.Text;

			// todo: add DateEdit instead of checkbox here 
            entity.DateLabelsNeeded = this.RadDatePicker_DateLabelNeeded.SelectedDate.Value;

            entity.ShipperId = new Guid(RadComboBox_Shipper.SelectedValue);
            if (RadTextBox_CountToSelect.Visible)
                entity.ShipmentDetailsCount = (int) RadTextBox_CountToSelect.Value;

            return entity;
        }
    }
}