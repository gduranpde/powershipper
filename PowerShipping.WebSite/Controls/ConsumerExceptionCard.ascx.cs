﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;
using System.Linq;

namespace PowerShipping.WebSite.Controls
{
    public partial class ConsumerExceptionCard : UserControl
    {
        private object _dataItem = null;
        private ConsumerExceptionsPresenter presenter = new ConsumerExceptionsPresenter();
        private UserManagementPresenter presenterAdv = new UserManagementPresenter();

        public Guid ConsumerRequestExceptionId
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_ConsumerRequestExceptionId.Value))
                           ? new Guid(HiddenField_ConsumerRequestExceptionId.Value)
                           : Guid.Empty;
            }
            set { HiddenField_ConsumerRequestExceptionId.Value = value.ToString(); }
        }

        public object DataItem
        {
            get { return _dataItem; }
            set { _dataItem = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnInit(EventArgs e)
        {
            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');");

            InitializeComponent();
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!Common.IsCanEdit())
            {
                btnUpdate.Visible = false;
                btnInsert.Visible = false;
                btnDelete.Visible = false;
            }

            base.OnPreRender(e);
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                ConsumerRequestException cr = presenter.GetById(ConsumerRequestExceptionId);
                cr.Status = ConsumerRequestExceptionStatus.Deleted;

                presenter.Save(cr);
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                Label l = new Label();
                l.Text = "Unable to delete ConsumerRequestException. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                //RadGrid grid = (this.Page.Controls[0].Controls[3].Controls[21].FindControl("RadGrid_ConsumerRequests") as RadGrid);
                //grid.Controls.Add(l);
                Helper.ErrorInGrid("RadGrid_ConsumerRequests", Page, l);
            }
            
        }

        private void InitializeComponent()
        {
            DataBinding += new EventHandler(Init_DataBinding);
        }

        private void Init_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        public void InitData()
        {
            InitDropDowns();
            if ((DataItem != null) & (DataItem.GetType() != typeof (GridInsertionObject)))
            {
                var entity = (ConsumerRequestException) DataItem;

                RadTextBox_AccountNumber.Text = entity.AccountNumber;
                RadTextBox_AccountName.Text = entity.AccountName;
                RadTextBox_Address1.Text = entity.Address1;
                RadTextBox_Address2.Text = entity.Address2;
                RadTextBox_City.Text = entity.City;
                RadTextBox_State.Text = entity.State;
                RadTextBox_ZipCode.Text = entity.ZipCode;
                RadTextBox_Email.Text = entity.Email;


                RadTextBox_CompanyName.Text = entity.CompanyName;
                RadTextBox_ServiceAddress1.Text = entity.ServiceAddress1;
                RadTextBox_ServiceAddress2.Text = entity.ServiceAddress2;
                RadTextBox_ServiceCity.Text = entity.ServiceCity;
                RadTextBox_ServiceState.Text = entity.ServiceState;
                RadTextBox_ServiceZip.Text = entity.ServiceZip;


                RadMaskedTextBox_Phone1.Text = EnumUtils.GetPhoneDigits(entity.Phone1);
                RadMaskedTextBox_Phone2.Text = EnumUtils.GetPhoneDigits(entity.Phone2);

                RadTextBox_Method.Text = entity.Method;
                RadTextBox_IsOkayToContact.Text = entity.IsOkayToContact;
                RadTextBox_AnalysisDate.Text = entity.AnalysisDate;
                RadTextBox_WaterHeaterFuel.Text = entity.WaterHeaterFuel;
                RadTextBox_HeaterFuel.Text = entity.HeaterFuel;
                //PG31
                RadTextBox_FacilityType.Text = entity.FacilityType;
                RadTextBox_IncomeQualified.Text = entity.IncomeQualified;
                RadTextBox_RateCode.Text = entity.RateCode;
                //PG31
                RadTextBox_OperatingCompany.Text = entity.OperatingCompany;
                RadTextBox_Notes.Text = entity.Notes;
                RadComboBox_Status.SelectedValue = Convert.ToInt32(entity.Status).ToString();
                chbOutOfState.Checked = entity.OutOfState;
                RadTextBox_BatchID.Text = entity.BatchLabel;
                RadDatePicker_ImportedDate.SelectedDate = entity.ImportedDate;

                RadTextBox_PremiseID.Text = entity.PremiseID;
                RadTextBox_Quantity.Text = entity.Quantity;
                RadTextBox_RequestedKit.Text = entity.RequestedKit;

                if (entity.OverrideDuplicates != null)
                    CheckBox_OverrideDuplicates.Checked = (bool)entity.OverrideDuplicates;
                else
                    CheckBox_OverrideDuplicates.Checked = false;

                RadComboBox_Project.SelectedValue = entity.ProjectId.ToString();

                ConsumerRequestExceptionId = entity.Id;
                SetVisibility(true);
            }
            else
            {
                SetVisibility(false);
            }
        }

        public ConsumerRequestException FillEntity()
        {
            var entity = new ConsumerRequestException();

            if (ConsumerRequestExceptionId != Guid.Empty)
            {
                entity.AccountNumber = RadTextBox_AccountNumber.Text;
                entity.AccountName = RadTextBox_AccountName.Text.ToUpperInvariant();
                entity.Address1 = RadTextBox_Address1.Text.ToUpperInvariant();
                entity.Address2 = RadTextBox_Address2.Text.ToUpperInvariant();
                entity.City = RadTextBox_City.Text.ToUpperInvariant();
                entity.State = RadTextBox_State.Text.ToUpperInvariant();
                entity.ZipCode = RadTextBox_ZipCode.Text;
                entity.Email = RadTextBox_Email.Text;
                entity.Phone1 = EnumUtils.GetPhoneDigits(RadMaskedTextBox_Phone1.Text);
                entity.Phone2 = EnumUtils.GetPhoneDigits(RadMaskedTextBox_Phone2.Text);

                entity.CompanyName = RadTextBox_CompanyName.Text.ToUpperInvariant();
                entity.ServiceAddress1 = RadTextBox_ServiceAddress1.Text.ToUpperInvariant();
                entity.ServiceAddress2 = RadTextBox_ServiceAddress2.Text.ToUpperInvariant();
                entity.ServiceCity = RadTextBox_ServiceCity.Text.ToUpperInvariant();
                entity.ServiceState = RadTextBox_ServiceState.Text.ToUpperInvariant();
                entity.ServiceZip = RadTextBox_ServiceZip.Text;


                entity.Method = RadTextBox_Method.Text;
                entity.IsOkayToContact = RadTextBox_IsOkayToContact.Text;
                entity.AnalysisDate = RadTextBox_AnalysisDate.Text;
                entity.CRImportedDate = RadDatePicker_ImportedDate.SelectedDate;
                entity.WaterHeaterFuel = RadTextBox_WaterHeaterFuel.Text;
                entity.HeaterFuel = RadTextBox_HeaterFuel.Text;
                //PG31
                entity.FacilityType = RadTextBox_FacilityType.Text;
                entity.IncomeQualified = RadTextBox_IncomeQualified.Text;
                entity.RateCode = RadTextBox_RateCode.Text;
                //PG31
                entity.OperatingCompany = RadTextBox_OperatingCompany.Text;
                entity.Notes = RadTextBox_Notes.Text;
                entity.Status = (ConsumerRequestExceptionStatus)Convert.ToInt32(RadComboBox_Status.SelectedValue);
                entity.OutOfState = chbOutOfState.Checked;

                entity.PremiseID = RadTextBox_PremiseID.Text;
                entity.Quantity = RadTextBox_Quantity.Text;
                entity.RequestedKit = RadTextBox_RequestedKit.Text;

                entity.OverrideDuplicates = CheckBox_OverrideDuplicates.Checked;

                entity.Id = ConsumerRequestExceptionId;
            }

            entity.ProjectId = new Guid(RadComboBox_Project.SelectedValue);

            return entity;
        }

        public void DeleteEntity()
        {
            
        }

        private void InitDropDowns()
        {
            RadComboBox_Status.DataSource = EnumUtils.EnumToListItems(typeof(ConsumerRequestExceptionStatus));
            RadComboBox_Status.DataBind();

            User u = presenterAdv.GetOne(HttpContext.Current.User.Identity.Name);
            if (u.Roles.Contains(PDMRoles.ROLE_Customer))
                RadComboBox_Project.DataSource = presenterAdv.GetProjectByUser(u.UserID);
            else
                RadComboBox_Project.DataSource = presenterAdv.GetProjectByUser(null);

            RadComboBox_Project.DataBind();
        }

        private void SetVisibility(bool forUpdate)
        {
            if (forUpdate)
            {
                
            }
            else
            {
              
            }
        }
    }
}