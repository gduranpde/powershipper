﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShipperCard.ascx.cs" Inherits="PowerShipping.WebSite.Controls.ShipperCard" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:HiddenField  ID="HiddenField_Id" runat="server" />

<table> 
    <tr id="tr_ShipperCode" runat="server">
        <td>ShipperCode</td>
        <td><telerik:RadTextBox ID="RadTextBox_ShipperCode" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_ShipperName" runat="server">
        <td >ShipperName</td>
        <td><telerik:RadTextBox ID="RadTextBox_ShipperName" runat="server"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_PackageTrackingUrl" runat="server">
        <td>Package Tracking Url</td>
        <td><telerik:RadTextBox ID="RadTextBox_PackageTrackingUrl" runat="server" Width="500px"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td id="tr_TransactionFileUrl" runat="server">Transaction File Url</td>
        <td><telerik:RadTextBox ID="RadTextBox_TransactionFileUrl" runat="server" Width="500px"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_BatchTrackingCodeModule" runat="server">
        <td>Batch Tracking Code Module</td>
        <td><telerik:RadTextBox ID="RadTextBox_BatchTrackingCodeModule" runat="server"></telerik:RadTextBox></td>
    </tr>
    
    <tr runat="server" id="td_CommandCell">
        <td colspan="2">            
            <asp:LinkButton id="btnUpdate" text="Save" runat="server" CommandName="Update" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton id="btnInsert" text="Save" runat="server" CommandName="PerformInsert" Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnCancel" text="Cancel" runat="server" causesvalidation="False" commandname="Cancel" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnDelete" text="Delete" runat="server" causesvalidation="False" CommandName="Cancel" OnClick="btnDelete_OnClick" Font-Size="Medium"></asp:LinkButton>
         </td>
    </tr>   
</table>