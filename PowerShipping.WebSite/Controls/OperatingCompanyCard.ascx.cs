﻿using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class OperatingCompanyCard : System.Web.UI.UserControl
    {

        private readonly UserManagementPresenter presenter = new UserManagementPresenter();

        public Guid OperatingCompanyID
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_Id.Value))
                           ? new Guid(HiddenField_Id.Value)
                           : Guid.Empty;
            }
            set { HiddenField_Id.Value = value.ToString(); }
        }

        public object DataItem { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        private void InitDropDowns()
        {
            RadComboBox_Companies.DataSource = presenter.GetCompanyByUser(null);
            RadComboBox_Companies.DataBind();

            cmbStates.DataSource = presenter.GetAllStates();
            cmbStates.DataBind();
        }
        public void InitData()
        {
            InitDropDowns();

            if ((DataItem != null) & (DataItem.GetType() != typeof(GridInsertionObject)))
            {
                var entity = (OperatingCompany)DataItem;


                RadTextBox_OperatingCompanyCode.Text = entity.OperatingCompanyCode;
                RadTextBox_OperatingCompanyName.Text = entity.OperatingCompanyName;
                CheckBox_IsActive.Checked = entity.IsActive;
                cmbStates.SelectedValue = entity.ServiceState.ToString();
                OperatingCompanyID = entity.OperatingCompanyID;
            }
        }

        private void Init_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        private void InitializeComponent()
        {
            DataBinding += Init_DataBinding;
        }

        protected override void OnInit(EventArgs e)
        {
            //btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');");

            InitializeComponent();
            base.OnInit(e);
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                OperatingCompany c = presenter.GetOperatingCompanyById(OperatingCompanyID);
                presenter.DeleteOperatingCompany(c);
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to delete Operating Company. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;

                Helper.ErrorInGrid("RadGrid_OperatingCompanies", Page, l);
            }
        }

        public OperatingCompany FillEntity()
        {
            var entity = new OperatingCompany();
            if (OperatingCompanyID != Guid.Empty)
            {
                entity.OperatingCompanyID = OperatingCompanyID;
            }
            else
            {
                entity.OperatingCompanyID = Guid.NewGuid();
            }

            entity.OperatingCompanyCode = RadTextBox_OperatingCompanyCode.Text;
            entity.OperatingCompanyName = RadTextBox_OperatingCompanyName.Text;
            entity.ParentCompanyID = new Guid(RadComboBox_Companies.SelectedValue);
            entity.ServiceState = int.Parse(cmbStates.SelectedValue);
            entity.IsActive = CheckBox_IsActive.Checked;
            return entity;
        }
    }
}