﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ItemSetupCard.ascx.cs" Inherits="PowerShipping.WebSite.Controls.ItemSetupCard" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:HiddenField ID="HiddenField_Id" runat="server" />


<telerik:RadProgressManager ID="Radprogressmanager1" runat="server" />
<table id="MainTable">
    <tr runat="server" id="TrData">
        <td id="firstColumn">
            <table id="secondTable">
                <tr id="tr_ItemName" runat="server">
                    <td>Item Name</td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBox_ItemName" runat="server"></telerik:RadTextBox></td>
                </tr>

                <tr id="tr_ItemDescription" runat="server">
                    <td>Item Description</td>
                    <td>
                        <telerik:RadTextBox Width="160px" ID="RadTextBox_ItemDescription" runat="server"
                            TextMode="MultiLine" Resize="Both">
                        </telerik:RadTextBox></td>
                </tr>

                <tr id="tr_DefaultKwImpact" runat="server">
                    <td>Default Kw Impact</td>
                    <td>
                        <telerik:RadNumericTextBox ID="RadNumericTextBox_Default_Kw_Impact" runat="server"
                            AllowRounding="false" KeepNotRoundedValue="true" NumberFormat-DecimalDigits="6">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>

                <tr id="tr_DefaultKwhImpact" runat="server">
                    <td>Default Kwh Impact</td>
                    <td>
                        <telerik:RadNumericTextBox ID="RadNumericTextBox_Default_Kwh_Impact" runat="server"
                            AllowRounding="false" KeepNotRoundedValue="true" NumberFormat-DecimalDigits="6">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>

                <tr id="tr_Active" runat="server">
                    <td>IsActive</td>
                    <td>
                        <asp:CheckBox ID="CheckBox_IsActive" runat="server"></asp:CheckBox></td>
                </tr>
            </table>
        </td>
        <td id="secondColumn">
            <table id="Table1" style="margin:30px">
                <tr id="tr_CreatedBy" runat="server">
                    <td align="right">Created By:
                    </td>
                    <td>
                        <%#Eval("CreatedBy")%>
                    </td>
                </tr>
                <tr id="tr_CreatedDate" runat="server">
                    <td align="right">Created Date:</td>
                    <td>
                        <%#Eval("CreatedDate")%>
                    </td>
                </tr>

                <tr id="tr_ModifiedBY" runat="server">
                    <td align="right">Last Modified By: </td>
                    <td>
                        <%#Eval("LastModifiedBy")%>
                    </td>
                </tr>

                <tr id="tr_ModifiedDate" runat="server">
                    <td align="right">Last Modified Date: 
                    </td>
                    <td>
                        <%#Eval("LastModifiedDate")%>
                    </td>
                </tr>


            </table>
        </td>
    </tr>

    <tr runat="server" id="td_CommandCell">
        <td colspan="2">
            <asp:LinkButton ID="btnUpdate" Text="Save" runat="server" CommandName="Update" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnInsert" Text="Save" runat="server" CommandName="PerformInsert" Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False" CommandName="Cancel" Font-Size="Medium"></asp:LinkButton>
        </td>
    </tr>
</table>
