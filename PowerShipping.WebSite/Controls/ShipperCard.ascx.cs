﻿using System;
using System.Drawing;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class ShipperCard : System.Web.UI.UserControl
    {
        private readonly UserManagementPresenter presenter = new UserManagementPresenter();

        public Guid ShipperId
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_Id.Value))
                           ? new Guid(HiddenField_Id.Value)
                           : Guid.Empty;
            }
            set { HiddenField_Id.Value = value.ToString(); }
        }

        public object DataItem { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnInit(EventArgs e)
        {
            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');");

            InitializeComponent();
            base.OnInit(e);
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                Shipper s = presenter.GetShipperById(ShipperId);
                presenter.DeleteShipper(s);
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to delete shipper. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                //var grid =
                //    (Page.Controls[0].Controls[3].Controls[13].FindControl("RadGrid_Shippers") as RadGrid);
                //grid.Controls.Add(l);
                Helper.ErrorInGrid("RadGrid_Shippers", Page, l);
            }
        }

        private void InitializeComponent()
        {
            DataBinding += Init_DataBinding;
        }

        private void Init_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        public void InitData()
        {
            InitDropDowns();

            if ((DataItem != null) & (DataItem.GetType() != typeof(GridInsertionObject)))
            {
                var entity = (Shipper)DataItem;

                RadTextBox_ShipperCode.Text = entity.ShipperCode;
                RadTextBox_ShipperName.Text = entity.ShipperName;
                RadTextBox_TransactionFileUrl.Text = entity.TransactionFileUrl;
                RadTextBox_BatchTrackingCodeModule.Text = entity.BatchTrackingCodeModule;
                RadTextBox_PackageTrackingUrl.Text = entity.PackageTrackingUrl;

                ShipperId = entity.Id;
            }
        }

        public Shipper FillEntity()
        {
            var entity = new Shipper();
            if (ShipperId != Guid.Empty)
            {
                entity.Id = ShipperId;
            }
            else
            {
                entity.Id = Guid.NewGuid();
            }

            entity.ShipperCode = RadTextBox_ShipperCode.Text;
            entity.ShipperName = RadTextBox_ShipperName.Text;
            entity.TransactionFileUrl = RadTextBox_TransactionFileUrl.Text;
            entity.BatchTrackingCodeModule = RadTextBox_BatchTrackingCodeModule.Text;
            entity.PackageTrackingUrl = RadTextBox_PackageTrackingUrl.Text.Trim();

            return entity;
        }

        private void InitDropDowns()
        {
        }
    }
}