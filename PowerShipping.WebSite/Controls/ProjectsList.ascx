﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProjectsList.ascx.cs" Inherits="PowerShipping.WebSite.Controls.ProjectsList" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<table>
    <tr>
        <td>
            Select project:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBox_Projects" runat="server" 
                DataValueField="Value" DataTextField="Text" AutoPostBack="true" OnSelectedIndexChanged="RadComboBox_Projects_SelectedIndexChanged"
                Width="400px">
            </telerik:RadComboBox>
        </td>
        <td>
            
        </td>
    </tr>
</table>