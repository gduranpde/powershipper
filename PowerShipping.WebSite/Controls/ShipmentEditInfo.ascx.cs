﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class ShipmentEditInfo : System.Web.UI.UserControl
    {
        ShipmentDetailsPresenter presenter = new ShipmentDetailsPresenter();
        UserManagementPresenter presenterAdv = new UserManagementPresenter();

        public Guid ShipmentId
        {
            get { return (!string.IsNullOrEmpty(HiddenField_ShipmentId.Value)) ? new Guid(HiddenField_ShipmentId.Value) : Guid.Empty; }
            set { HiddenField_ShipmentId.Value = value.ToString(); }
        }

        public Guid ConsumerRequestId
        {
            get { return (!string.IsNullOrEmpty(HiddenField_ConsumerRequestId.Value)) ? new Guid(HiddenField_ConsumerRequestId.Value) : Guid.Empty; }
            set { HiddenField_ConsumerRequestId.Value = value.ToString(); }
        }

        private object _dataItem = null;

        public object DataItem
        {
            get
            {
                return this._dataItem;
            }
            set
            {
                this._dataItem = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!Common.IsCanEdit())
            {
                btnUpdate.Visible = false;
                btnInsert.Visible = false;
                btnDelete.Visible = false;
            }

            base.OnPreRender(e);
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                presenter.Remove(ShipmentId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to delete Shipment Detail. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                //var grid =
                //    (Page.Controls[0].Controls[3].Controls[13].FindControl("RadGrid_ShipmentDetails") as RadGrid);
                //grid.Controls.Add(l);
                Helper.ErrorInGrid("RadGrid_ShipmentDetails", Page, l);
            }
        }

        private void InitializeComponent()
        {
            this.DataBinding += new EventHandler(SubjobZipCode_DataBinding);
        }

        private void SubjobZipCode_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        public void InitData()
        {
            InitDropDowns();
            if ((DataItem != null) & (DataItem.GetType() != typeof(Telerik.Web.UI.GridInsertionObject)))
            {
                ShipmentDetail sd = (ShipmentDetail)DataItem;

                this.RadTextBox_PurchaseOrderNumber.Text = sd.PurchaseOrderNumber;
                this.RadTextBox_FedExTrackingNumber.Text = sd.FedExTrackingNumber;
                this.RadTextBox_FedExStatusCode.Text = sd.FedExStatusCode;
                this.RadTextBox_AccountName.Text = sd.AccountName;

                this.RadTextBox_Address1.Text = sd.ShipmentDetailAddress1;
                this.RadTextBox_Address2.Text = sd.ShipmentDetailAddress2;
                this.RadTextBox_City.Text = sd.ShipmentDetailCity;
                this.RadTextBox_State.Text = sd.ShipmentDetailState;
                this.RadTextBox_ZipCode.Text = sd.ShipmentDetailZip;

                this.RadTextBox_Email.Text = sd.Email;
                try
                {
                    Double myDouble = Convert.ToDouble(sd.Phone1);
                    this.RadMaskedTextBox_Phone1.Text = myDouble.ToString("###-###-####");
                    if (!string.IsNullOrEmpty(sd.Phone2))
                    {
                        myDouble = Convert.ToDouble(sd.Phone2);
                        this.RadMaskedTextBox_Phone2.Text = myDouble.ToString("###-###-####");
                    }
                }
                catch
                {
                    this.RadMaskedTextBox_Phone1.Text = sd.Phone1;
                    this.RadMaskedTextBox_Phone2.Text = sd.Phone2;
                }

                this.RadComboBox_ShppingStatus.SelectedValue = Convert.ToInt32(sd.ShippingStatus).ToString();
                this.RadComboBox_ReshipStatus.SelectedValue = Convert.ToInt32(sd.ReshipStatus).ToString();
                this.RadDatePicker_StatusDate.SelectedDate = sd.ShippingStatusDate;
                this.RadDatePicker_ReturnDate.SelectedDate = sd.ReturnDate;
                RadDatePicker_ShipDate.SelectedDate = sd.ShipDate;
                chbIsReship.Checked = sd.IsReship;
                RadTextBox_Notes.Text = sd.Notes;
                RadTextBox_BatchID.Text = sd.FedExBatchId.ToString();
                //RadTextBox_FedExLastUpdateStatus.Text = sd.FedExLastUpdateStatus;

                RadComboBox_Shipper.SelectedValue = sd.ShipperId.ToString();

                //RadComboBox_Project.SelectedValue = sd.ProjectId.ToString();

                RadTextBox_CompanyName.Text = sd.CompanyName;
                if (sd.ReasonId != null)
                    RadComboBox_Reason.SelectedValue = sd.ReasonId.ToString();

                ShipmentId = sd.Id;
                ConsumerRequestId = sd.ConsumerRequestId;

                //ConsumerRequestsPresenter consumerPresenter = new ConsumerRequestsPresenter();
                //ConsumerRequest cr = consumerPresenter.GetByAccountNumber(sd);
                SetVisibility(true);
            }
            else
            {
                SetVisibility(false);
                this.RadTextBox_ShippingNumber.Text = (presenter.GetMaxShippingNumber() + 1).ToString();
            }
        }

        public PowerShipping.Entities.ShipmentDetail FillEntity()
        {
            PowerShipping.Entities.ShipmentDetail sd = new PowerShipping.Entities.ShipmentDetail();

            if (ShipmentId != Guid.Empty)
            {
                sd.PurchaseOrderNumber = this.RadTextBox_PurchaseOrderNumber.Text;
                sd.FedExTrackingNumber = this.RadTextBox_FedExTrackingNumber.Text;
                sd.FedExStatusCode = this.RadTextBox_FedExStatusCode.Text;
                sd.AccountName = this.RadTextBox_AccountName.Text.ToUpperInvariant();

                sd.ShipmentDetailAddress1 = this.RadTextBox_Address1.Text.ToUpperInvariant();
                sd.ShipmentDetailAddress2 = this.RadTextBox_Address2.Text.ToUpperInvariant();
                sd.ShipmentDetailCity = this.RadTextBox_City.Text.ToUpperInvariant();
                sd.ShipmentDetailState = this.RadTextBox_State.Text.ToUpperInvariant();
                sd.ShipmentDetailZip = this.RadTextBox_ZipCode.Text;
                sd.ReturnDate = this.RadDatePicker_ReturnDate.SelectedDate;
                sd.Email = this.RadTextBox_Email.Text;
                sd.Phone1 = this.RadMaskedTextBox_Phone1.Text;
                sd.Phone2 = this.RadMaskedTextBox_Phone2.Text;
                sd.ShippingStatus = (ShipmentShippingStatus)Convert.ToInt32(this.RadComboBox_ShppingStatus.SelectedValue);
                sd.ReshipStatus = (ShipmentReshipStatus)Convert.ToInt32(this.RadComboBox_ReshipStatus.SelectedValue);
                sd.ShippingStatusDate = (DateTime)RadDatePicker_StatusDate.SelectedDate;

                sd.ShipDate = RadDatePicker_ShipDate.SelectedDate;
                sd.IsReship = chbIsReship.Checked;
                sd.Notes = RadTextBox_Notes.Text;

                sd.CompanyName = RadTextBox_CompanyName.Text;
                if (RadComboBox_Reason.SelectedValue != Guid.Empty.ToString())
                    sd.ReasonId = new Guid(RadComboBox_Reason.SelectedValue);
                else
                    sd.ReasonId = null;

                sd.Id = ShipmentId;
                sd.ConsumerRequestId = ConsumerRequestId;
            }
            else
            {
                sd = new PowerShipping.Entities.ShipmentDetail();
                sd.AccountNumber = this.RadTextBox_AccountNumber.Text;
                sd.PurchaseOrderNumber = this.RadTextBox_PurchaseOrderNumber.Text;
                sd.FedExTrackingNumber = this.RadTextBox_FedExTrackingNumber.Text;
                sd.ShipmentNumber = this.RadTextBox_ShippingNumber.Text;
                sd.ShippingStatus = ShipmentShippingStatus.Pending;
                sd.ReshipStatus = ShipmentReshipStatus.NA;
                sd.KitTypeId = new Guid(this.RadComboBox_KitTypes.SelectedValue);
                sd.ShippingStatusDate = DateTime.Now;
            }

            //sd.FedExLastUpdateStatus = RadTextBox_FedExLastUpdateStatus.Text;
            sd.ShipperId = new Guid(RadComboBox_Shipper.SelectedValue);
            //sd.ProjectId = new Guid(RadComboBox_Project.SelectedValue);

            return sd;
        }

        private void InitDropDowns()
        {
            RadComboBox_ShppingStatus.DataSource = EnumUtils.EnumToListItems(typeof(ShipmentShippingStatus));
            RadComboBox_ShppingStatus.DataBind();

            RadComboBox_ReshipStatus.DataSource = EnumUtils.EnumToListItems(typeof(ShipmentReshipStatus));
            RadComboBox_ReshipStatus.DataBind();

            RadComboBox_KitTypes.DataSource = presenter.GetListOfKitTypes();
            RadComboBox_KitTypes.DataBind();

            RadComboBox_Shipper.DataSource = presenterAdv.GetAllShipers();
            RadComboBox_Shipper.DataBind();

            User u = presenterAdv.GetOne(HttpContext.Current.User.Identity.Name);
            //if (u.Roles.Contains(PDMRoles.ROLE_Customer))
            //    RadComboBox_Project.DataSource = presenterAdv.GetProjectByUser(u.UserID);
            //else
            //    RadComboBox_Project.DataSource = presenterAdv.GetProjectByUser(null);

            //RadComboBox_Project.DataBind();

            List<Reason> listReasons = new List<Reason>();
            listReasons.Add(new Reason() { ReasonCode = "", ReasonDescription = "", ReasonID = Guid.Empty });
            List<Reason> listR = presenter.GetAllReasons();
            foreach (Reason reason in listR)
            {
                listReasons.Add(reason);
            }

            RadComboBox_Reason.DataSource = listReasons;
            RadComboBox_Reason.DataBind();
        }

        private void SetVisibility(bool forUpdate)
        {
            if (forUpdate)
            {
                this.tr_AccountNumber.Visible = false;
                this.tr_KitType.Visible = false;
                this.tr_ShippingNumber.Visible = false;
                this.tr_PurchaseOrderNumber.Visible = true;
                this.tr_FedExTrackingNumber.Visible = true;

                this.tr_AccountName.Visible = true;
                this.tr_Address1.Visible = true;
                this.tr_Address2.Visible = true;
                this.tr_City.Visible = true;
                this.tr_Email.Visible = true;
                this.tr_FedExStatusCode.Visible = true;
                this.tr_Phone1.Visible = true;
                this.tr_Phone2.Visible = true;
                this.tr_ReshipStatus.Visible = true;
                this.tr_ShppingStatus.Visible = true;
                this.tr_State.Visible = true;
                this.tr_StatusDate.Visible = true;
                this.tr_ZipCode.Visible = true;

                tr_ShipDate.Visible = true;
                tr_IsReship.Visible = true;
                tr_Notes.Visible = true;
                tr_BatchID.Visible = true;

                tr_Reason.Visible = true;
                tr_CompanyName.Visible = true;
            }
            else
            {
                this.tr_AccountNumber.Visible = true;
                this.tr_KitType.Visible = true;
                this.tr_ShippingNumber.Visible = true;
                this.tr_PurchaseOrderNumber.Visible = true;
                this.tr_FedExTrackingNumber.Visible = true;

                this.tr_AccountName.Visible = false;
                this.tr_Address1.Visible = false;
                this.tr_Address2.Visible = false;
                this.tr_City.Visible = false;
                this.tr_Email.Visible = false;
                this.tr_FedExStatusCode.Visible = false;
                this.tr_Phone1.Visible = false;
                this.tr_Phone2.Visible = false;
                this.tr_ReshipStatus.Visible = false;
                this.tr_ShppingStatus.Visible = false;
                this.tr_State.Visible = false;
                this.tr_StatusDate.Visible = false;
                this.tr_ZipCode.Visible = false;

                tr_ShipDate.Visible = false;
                tr_IsReship.Visible = false;
                tr_Notes.Visible = false;
                tr_BatchID.Visible = false;

                tr_Reason.Visible = false;
                tr_CompanyName.Visible = false;
            }
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
        }
    }
}