﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerCreateRequestFilter.ascx.cs" Inherits="PowerShipping.WebSite.Controls.CustomerCreateRequestFilter" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<div class="jobInfo" runat="server" id="div_jobInfo">
 <div style=" text-align : center;"><b>Customer Filter</b></div>
 <table>
    <tr>
        <td colspan="2" align="center">           
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <asp:Button runat="server" ID="Button_Filter" Text="Search"  Width="80px"
                CssClass="ptButton" onclick="Button_Filter_Click" />
            <asp:Button runat="server" ID="Button_Clear" Text="Clear"  Width="80px"
                CssClass="ptButton" onclick="Button_Clear_Click" />
        </td>
    </tr>
   <tr>
        <td colspan="2" align="center">           
        </td>
    </tr> 
    
    <tr>
        <td class="jobInfo_td">Project:</td>
        <td class="jobInfo_info">
              <telerik:RadComboBox ID="RadComboBox_Projects" runat="server" DataValueField="Value" DataTextField="Text" Width="100px">
            </telerik:RadComboBox>
        </td>
    </tr>     
    <tr>
        <td class="jobInfo_td">Note:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_Source"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="jobInfo_td">User:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_User"></telerik:RadTextBox></td>
    </tr>
    <tr>
        <td class="jobInfo_td">File Name:</td><td class="jobInfo_info"><telerik:RadTextBox width="100px" runat="server" ID="RadTextBox_File"></telerik:RadTextBox></td>
    </tr>
    
    <tr>
        <td align="center" colspan="2">Upload Date:</td>
    </tr>
    <tr>
        <td align="right" colspan="2">From: <telerik:RadDatePicker ID="RadDatePicker_ImportStartDate" Runat="server" Skin="Telerik" Width="120px">
                <calendar skin="Telerik" usecolumnheadersasselectors="False" 
                    userowheadersasselectors="False" viewselectortext="x">
                </calendar>
                <datepopupbutton hoverimageurl="" imageurl="" />
                <dateinput dateformat="MM/dd/yyyy" displaydateformat="MM/dd/yyyy">
                </dateinput>
            </telerik:RadDatePicker><br />
            To: <telerik:RadDatePicker ID="RadDatePicker_ImportEndDate" Runat="server" Skin="Telerik" Width="120px">
                <calendar skin="Telerik" usecolumnheadersasselectors="False" 
                    userowheadersasselectors="False" viewselectortext="x">
                </calendar>
                <datepopupbutton hoverimageurl="" imageurl="" />
                <dateinput dateformat="MM/dd/yyyy" displaydateformat="MM/dd/yyyy">
                </dateinput>
            </telerik:RadDatePicker>
        </td>
    </tr>
    
    
                
 </table>
</div>