﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;

namespace PowerShipping.WebSite.Controls
{
    public partial class ShipmentDetailsInfo : System.Web.UI.UserControl
    {
        ConsumerRequestsPresenter presenter = new ConsumerRequestsPresenter();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void InitData()
        {
            PowerShipping.Entities.FedExShippingDefault entity = presenter.GetFedExShippingDefault();
            this.RadTextBox_AddressToSendLabels.Text = entity.AddressToSendLabels;
            this.RadTextBox_AnyPackagesToResidentialAddresses.Text = entity.AnyPackagesToResidentialAddresses;
            this.RadTextBox_BulkLabelSupportSpecialHandling.Text = entity.BulkLabelSupportSpecialHandling;
            this.RadTextBox_ConsumerCustomerServicePhoneNumber.Text = entity.ConsumerCustomerServicePhoneNumber;
            this.RadTextBox_DeclaredValueAmount.Text = entity.DeclaredValueAmount;
            this.RadTextBox_DeliveryServiceForLabels.Text = entity.DeliveryServiceForLabels;
            this.RadTextBox_DomesticOrInternational.Text = entity.DomesticOrInternational;
            this.RadTextBox_FedExAccountNumberForBilling.Text = entity.FedExAccountNumberForBilling;
            this.RadTextBox_FedExBulkLabelEmailAddress.Text = entity.FedExBulkLabelEmailAddress;
            this.RadTextBox_FedExServiceType.Text = entity.FedExServiceType;
            this.RadTextBox_MoreThanOnePackageForEachRecipient.Text = entity.MoreThanOnePackageForEachRecipient;
            this.RadTextBox_OtherCCEmailAddresses.Text = entity.OtherCcEmailAddresses;
            this.RadTextBox_PackagePickupLocation.Text = entity.PackagePickupLocation;
            this.RadTextBox_PackageType.Text = entity.PackageType;
            this.RadTextBox_PaymentType.Text = entity.PaymentType;
            this.RadTextBox_PDMShippingExceptionEmailAddress.Text = entity.PdmShippingExceptionEmailAddress;
            this.RadTextBox_ShipperCompanyName.Text = entity.ShipperCompanyName;
            this.RadTextBox_ShipperContactName.Text = entity.ShipperContactName;
            this.RadTextBox_ShipperContactPhone.Text = entity.ShipperContactPhone;
            this.RadTextBox_ShipperFedExAccountNumber.Text = entity.ShipperFedExAccountNumber;
            this.RadTextBox_ShipperPhysicalAddress.Text = entity.ShipperPhysicalAddress;
            this.RadTextBox_SignatureRequired.Text = entity.SignatureRequired;



        }

        public PowerShipping.Entities.FedExShippingDefault GetEntity()
        {
            PowerShipping.Entities.FedExShippingDefault entity = new PowerShipping.Entities.FedExShippingDefault();
            entity.AddressToSendLabels = this.RadTextBox_AddressToSendLabels.Text;
            entity.AnyPackagesToResidentialAddresses = this.RadTextBox_AnyPackagesToResidentialAddresses.Text;
            entity.BulkLabelSupportSpecialHandling = this.RadTextBox_BulkLabelSupportSpecialHandling.Text;
            entity.ConsumerCustomerServicePhoneNumber = this.RadTextBox_ConsumerCustomerServicePhoneNumber.Text;
            entity.DeclaredValueAmount = this.RadTextBox_DeclaredValueAmount.Text;
            entity.DeliveryServiceForLabels = this.RadTextBox_DeliveryServiceForLabels.Text;
            entity.DomesticOrInternational = this.RadTextBox_DomesticOrInternational.Text;
            entity.FedExAccountNumberForBilling = this.RadTextBox_FedExAccountNumberForBilling.Text;
            entity.FedExBulkLabelEmailAddress = this.RadTextBox_FedExBulkLabelEmailAddress.Text;
            entity.FedExServiceType = this.RadTextBox_FedExServiceType.Text;
            entity.MoreThanOnePackageForEachRecipient = this.RadTextBox_MoreThanOnePackageForEachRecipient.Text;
            entity.OtherCcEmailAddresses = this.RadTextBox_OtherCCEmailAddresses.Text;
            entity.PackagePickupLocation = this.RadTextBox_PackagePickupLocation.Text;
            entity.PackageType = this.RadTextBox_PackageType.Text;
            entity.PaymentType = this.RadTextBox_PaymentType.Text;
            entity.PdmShippingExceptionEmailAddress = this.RadTextBox_PDMShippingExceptionEmailAddress.Text;
            entity.ShipperCompanyName = this.RadTextBox_ShipperCompanyName.Text;
            entity.ShipperContactName = this.RadTextBox_ShipperContactName.Text;
            entity.ShipperContactPhone = this.RadTextBox_ShipperContactPhone.Text;
            entity.ShipperFedExAccountNumber = this.RadTextBox_ShipperFedExAccountNumber.Text;
            entity.ShipperPhysicalAddress = this.RadTextBox_ShipperPhysicalAddress.Text;
            entity.SignatureRequired = this.RadTextBox_SignatureRequired.Text;

            return entity;
        }
    }
}