﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConsumerRequestList.ascx.cs" Inherits="PowerShipping.WebSite.Controls.ConsumerRequestList" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register src="../Controls/ProjectsList.ascx" tagname="ProjectsList" tagprefix="uc1" %>

<table>
       <tr>
            <td>              
               <h3>Select Consumer Requests</h3>
            </td>
            <td align="center">
                 <uc1:ProjectsList ID="ProjectsList1" runat="server" />
            </td> 
        </tr>
        <tr>
            <td>              
               
            </td>
            <td align="center">
                 <asp:Label ID="lbErr" runat="server"></asp:Label>
            </td> 
        </tr>  
 </table>
<table>
    <tr>
    <td>
    <table>
    <tr>
        
     <td>Number to Select</td><td>
     <telerik:RadNumericTextBox ID="RadTextBox_CountToSelect" runat="server" DataType="System.Int32"> 
     <NumberFormat DecimalDigits="0" />
     </telerik:RadNumericTextBox> 
     </td>
     <td>
        <asp:Label ID="lbMaxResults" runat="server"></asp:Label>
     </td>
           <td>
                <asp:Button runat="server" ID="Button_Filter" Text="Next" onclick="Button_Filter_Click" CssClass="ptButton" />
            </td>
          <%--   <td>
                <asp:Button ID="btnClearFilter" runat="server" OnClick="btnClearFilter_Click" Text="Clear" CssClass="ptButton"/>
            </td>--%>
   <%--  
        <td>Prior</td>
        <td>
            <telerik:RadComboBox runat="server" ID="RadComboBox_Priors">
                <Items>
                    <telerik:RadComboBoxItem Text="Original" Value="false" />
                    <telerik:RadComboBoxItem Text="Reship" Value="true" />
                </Items>
            </telerik:RadComboBox>
        </td>--%>
    </tr>
    </table>      
    </td>    
    </tr>
    <tr>
    <td>
 <table>
    <tr>
    <%--<td>
                Account Number 
                <asp:TextBox ID="tbAccountNumberFilter" runat="server"></asp:TextBox>         
            </td>
            <td>
                Account Name
                <asp:TextBox ID="tbAccountNameFilter" runat="server"></asp:TextBox>
            </td>
            <td>
                Phone Number
                <asp:TextBox ID="tbPhoneFilter" runat="server"></asp:TextBox>
            </td>
            <td>
                Address
                <asp:TextBox ID="tbAddressFilter" runat="server"></asp:TextBox>
            </td>
            <td>Filter</td>--%>
            
    </tr>
    </table>
    </td>    
    </tr>
</table>

<telerik:RadGrid ID="RadGrid_ConsumerRequests" runat="server" 
    AutoGenerateColumns="False" GridLines="None" 
    Skin="Vista" AllowPaging="True" AllowSorting="True"
    onneeddatasource="RadGrid_ConsumerRequests_NeedDataSource" 
    ondatabound="RadGrid_ConsumerRequests_DataBound" EnableLinqExpressions="false">
    <GroupingSettings CaseSensitive="false" />
    <MasterTableView DataKeyNames="Id" AllowMultiColumnSorting="True" AllowSorting="true" AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true"
    >    
        <Columns>
            <telerik:GridTemplateColumn HeaderStyle-Width="10" AllowFiltering="false">                
                <HeaderTemplate>#</HeaderTemplate>
                    <ItemTemplate><%# (Container.ItemIndex + 1).ToString() %></ItemTemplate>            
                <HeaderStyle Width="30px"></HeaderStyle>                
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderStyle-Width="10" AllowFiltering="false" Visible="false">
                <HeaderTemplate>#</HeaderTemplate>
                    <ItemTemplate><asp:CheckBox id="CheckBox_Choosed" runat="server" /></ItemTemplate>            
                <HeaderStyle Width="30px"></HeaderStyle>
            </telerik:GridTemplateColumn>            
            <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="AccountNumber" SortExpression="AccountNumber"
                UniqueName="AccountNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                EditFormColumnIndex="0">                
                </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="AccountName" HeaderText="Name" SortExpression="AccountName" FilterControlWidth="150"
                        UniqueName="AccountName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
            <telerik:GridCheckBoxColumn DataField="IsReship" HeaderText="Reship?" SortExpression="IsReship"
                        UniqueName="IsReship" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true">
            </telerik:GridCheckBoxColumn>
              <telerik:GridCheckBoxColumn DataField="DoNotShip" HeaderText="Opt Out" SortExpression="DoNotShip" AllowFiltering="false"
                        UniqueName="DoNotShip" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true">
                        </telerik:GridCheckBoxColumn>
            <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email" Visible="false"
                UniqueName="Email" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
            </telerik:GridBoundColumn> 
            <telerik:GridBoundColumn DataField="Address1" HeaderText="Address1" SortExpression="Address1" AllowFiltering="false"
                UniqueName="Address1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterControlWidth="200">               
                </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Address2" HeaderText="Address2" SortExpression="Address2" AllowFiltering="false"
                UniqueName="Address2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" FilterControlWidth="200">               
                </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="City" HeaderText="City" SortExpression="City" FilterControlWidth="100"
                UniqueName="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="State" HeaderText="State" SortExpression="State"
                UniqueName="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="ZipCode" HeaderText="ZIP" SortExpression="ZipCode"
                UniqueName="ZipCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
            </telerik:GridBoundColumn>
             <telerik:GridDateTimeColumn DataField="AnalysisDate" HeaderText="AnalysisDate" SortExpression="AnalysisDate" HeaderStyle-Width="130" 
                        UniqueName="AnalysisDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                        DataFormatString="{0:MM/dd/yyyy}">
                        </telerik:GridDateTimeColumn>
            <telerik:GridDateTimeColumn DataField="ImportedDate" HeaderText="Import Date" SortExpression="ImportedDate" HeaderStyle-Width="130" AllowFiltering="false"
                        UniqueName="ImportedDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true" DataFormatString="{0:MM/dd/yyyy}">
                        </telerik:GridDateTimeColumn>
        </Columns>
        <ExpandCollapseColumn>
            <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>
    </MasterTableView>
</telerik:RadGrid>
    