﻿using System;
using System.Drawing;
using System.Web;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class OptOutExceptionCard : UserControl
    {
        private object _dataItem = null;
        private OptOutExceptionPresenter presenter = new OptOutExceptionPresenter();
        private UserManagementPresenter presenterAdv = new UserManagementPresenter();

        public Guid OptOutExId
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_Id.Value))
                           ? new Guid(HiddenField_Id.Value)
                           : Guid.Empty;
            }
            set { HiddenField_Id.Value = value.ToString(); }
        }

        public object DataItem
        {
            get { return _dataItem; }
            set { _dataItem = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnInit(EventArgs e)
        {
            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');");

            InitializeComponent();
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!Common.IsCanEdit())
            {
                btnUpdate.Visible = false;
                btnDelete.Visible = false;
            }

            base.OnPreRender(e);
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                //OptOutException ooEx = presenter.GetById(OptOutExId);
                //ooEx.Status = OptOutExceptionStatus.Deleted;
                //presenter.Save(ooEx);
                presenter.Delete(OptOutExId);
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to delete Opt Out Exception. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                //var grid = (Page.Controls[0].Controls[3].Controls[13].FindControl("RadGrid_OptOutsEx") as RadGrid);
                //grid.Controls.Add(l);
                Helper.ErrorInGrid("RadGrid_OptOutsEx", Page, l);
            }
        }

        private void InitializeComponent()
        {
            DataBinding += new EventHandler(Init_DataBinding);
        }

        private void Init_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        public void InitData()
        {
            InitDropDowns();

            if ((DataItem != null) & (DataItem.GetType() != typeof (GridInsertionObject)))
            {
                var entity = (OptOutException) DataItem;

                RadTextBox_OptOutDate.Text = entity.OptOutDate;
                RadTextBox_AccountNumber.Text = entity.AccountNumber;
                RadTextBox_AccountName.Text = entity.AccountName;
                RadTextBox_Address1.Text = entity.Address;
                RadTextBox_City.Text = entity.City;
                RadTextBox_State.Text = entity.State;
                RadTextBox_ZipCode.Text = entity.ZipCode;
                RadTextBox_Email.Text = entity.Email;
                RadMaskedTextBox_Phone1.Text = EnumUtils.GetPhoneDigits(entity.Phone1);
                RadMaskedTextBox_Phone2.Text = EnumUtils.GetPhoneDigits(entity.Phone2);

                //RadTextBox_IsEnergyProgram.Text = entity.IsEnergyProgram;
                //RadTextBox_IsTotalDesignation.Text = entity.IsTotalDesignation;
                if (string.IsNullOrEmpty(entity.IsEnergyProgram))
                    CheckBox_IsEnergyProgram.Checked = false;
                else
                {
                    try
                    {
                        CheckBox_IsEnergyProgram.Checked = Convert.ToBoolean(entity.IsEnergyProgram);
                    }
                    catch
                    {
                    }
                }

                if (string.IsNullOrEmpty(entity.IsTotalDesignation))
                    CheckBox_IsTotalDesignation.Checked = false;
                else
                {
                    try
                    {
                        CheckBox_IsTotalDesignation.Checked = Convert.ToBoolean(entity.IsTotalDesignation);
                    }
                    catch
                    {
                    }
                }


                RadTextBox_Notes.Text = entity.Notes;

                RadComboBox_Project.SelectedValue = entity.ProjectId.ToString();

                //RadComboBox_Status.SelectedValue = Convert.ToInt32(entity.Status).ToString();

                OptOutExId = entity.Id;
                SetVisibility(true);
            }
            else
            {
                SetVisibility(false);
            }
        }

        public OptOutException FillEntity()
        {
            var entity = new OptOutException();

            if (OptOutExId != Guid.Empty)
            {
                entity.OptOutDate = RadTextBox_OptOutDate.Text;

                entity.AccountNumber = RadTextBox_AccountNumber.Text;
                entity.AccountName = RadTextBox_AccountName.Text.ToUpperInvariant();
                entity.Address = RadTextBox_Address1.Text.ToUpperInvariant();
                entity.City = RadTextBox_City.Text.ToUpperInvariant();
                entity.State = RadTextBox_State.Text.ToUpperInvariant();
                entity.ZipCode = RadTextBox_ZipCode.Text;
                entity.Email = RadTextBox_Email.Text;
                entity.Phone1 = EnumUtils.GetPhoneDigits(RadMaskedTextBox_Phone1.Text);
                entity.Phone2 = EnumUtils.GetPhoneDigits(RadMaskedTextBox_Phone2.Text);

                //entity.IsEnergyProgram = RadTextBox_IsEnergyProgram.Text;
                //entity.IsTotalDesignation = RadTextBox_IsTotalDesignation.Text;

                entity.IsEnergyProgram = CheckBox_IsEnergyProgram.Checked.ToString();
                entity.IsTotalDesignation = CheckBox_IsTotalDesignation.Checked.ToString();

                entity.Notes = RadTextBox_Notes.Text;

                //entity.Status = (OptOutExceptionStatus)Convert.ToInt32(RadComboBox_Status.SelectedValue);

                entity.Id = OptOutExId;
            }

            entity.ProjectId = new Guid(RadComboBox_Project.SelectedValue);


            return entity;
        }

        public void DeleteEntity()
        {
        }

        private void InitDropDowns()
        {
            //RadComboBox_Status.DataSource = Utils.EnumToListItems(typeof(OptOutExceptionStatus));
            //RadComboBox_Status.DataBind();

            User u = presenterAdv.GetOne(HttpContext.Current.User.Identity.Name);
            if (u.Roles.Contains(PDMRoles.ROLE_Customer))
                RadComboBox_Project.DataSource = presenterAdv.GetProjectByUser(u.UserID);
            else
                RadComboBox_Project.DataSource = presenterAdv.GetProjectByUser(null);

            RadComboBox_Project.DataBind();
        }

        private void SetVisibility(bool forUpdate)
        {
            if (forUpdate)
            {
            }
            else
            {
            }
        }
    }
}