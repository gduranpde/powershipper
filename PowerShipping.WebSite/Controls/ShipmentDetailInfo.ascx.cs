﻿using System;
using System.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class ShipmentDetailInfo : UserControl
    {
        private object _dataItem = null;
        public object DataItem
        {
            get
            {
                return this._dataItem;
            }
            set
            {
                this._dataItem = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {

            this.DataBinding += new EventHandler(SubjobZipCode_DataBinding);

        }

        void SubjobZipCode_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        public void InitData()
        {
            if ((DataItem != null) & (DataItem.GetType() != typeof(Telerik.Web.UI.GridInsertionObject)))
            {
                //Fill Controls
                //PowerTrakker.Entities.ScheduledDailyActivityInfoZipCode scheduledInfoZipCode = (PowerTrakker.Entities.ScheduledDailyActivityInfoZipCode)DataItem;

                //this.Label_ZipCodeValue.Text = scheduledInfoZipCode.ZipCodeValue.ToString();
                //this.Label_EstimatedDelivery.Text = scheduledInfoZipCode.TotalDeliveryHouseHolds.ToString();
                //this.Label_TotalEstimated.Text = scheduledInfoZipCode.OverallEstimatedDelivery.ToString();

                //this.RadTexBox_AmoutDelivered.Text = scheduledInfoZipCode.Quantity.ToString();

                //ScheduledDailyActivityInfoZipCodeId = scheduledInfoZipCode.ScheduledDailyActivityInfoZipCodeId;


            }
            else
            {
                
            }


        }

        public object GetEntity()
        {
            //PowerTrakker.Entities.ScheduledDailyActivityInfoZipCode entity = new PowerTrakker.Entities.ScheduledDailyActivityInfoZipCode();

            //entity.Quantity = entity.AmoutDelivered = Convert.ToInt32(this.RadTexBox_AmoutDelivered.Text);
            //entity.ScheduledDailyActivityInfoZipCodeId = ScheduledDailyActivityInfoZipCodeId;


            //return entity;
            return null;
        }
    }
}