﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LabelRequestCard.ascx.cs"
    Inherits="PowerShipping.WebSite.Controls.LabelRequestCard" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:HiddenField ID="HiddenField_Id" runat="server" />
<table>
    <tr id="tr_PONumber" runat="server">
        <td>
            PO Number
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_PONumber" runat="server">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr runat="server" id="tr_Shipper" visible="true">
        <td>
            Shipper
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBox_Shipper" runat="server" DataValueField="Id"
                DataTextField="FullName">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr runat="server" id="tr_Kit" visible="true">
        <td>
            Kit
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBox_Kit" runat="server" DataValueField="Id" DataTextField="KitName">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr runat="server" id="td_CommandCell">
        <td colspan="2">
            <asp:LinkButton ID="btnUpdate" Text="Save" runat="server" CommandName="Update" ValidationGroup="Card"
                Font-Size="Medium" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>'></asp:LinkButton>
            <asp:LinkButton ID="btnInsert" Text="Save" runat="server" CommandName="PerformInsert"
                ValidationGroup="Card" Font-Size="Medium" Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>'></asp:LinkButton>
            <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False"
                CommandName="Cancel" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnDelete" Text="Delete" runat="server" CommandName="Cancel"
                CausesValidation="False" Font-Size="Medium" OnClick="btnDelete_Click"></asp:LinkButton>
        </td>
    </tr>
</table>