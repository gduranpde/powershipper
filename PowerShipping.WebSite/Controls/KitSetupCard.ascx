﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="KitSetupCard.ascx.cs" Inherits="PowerShipping.WebSite.Controls.KitSetupCard" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:HiddenField ID="HiddenField_Id" runat="server" />
<table>
    <tr>
        <td id="tr_KitName" runat="server">Kit Name</td>
        <td>
            <telerik:RadTextBox ID="RadTextBox_KitName" runat="server"></telerik:RadTextBox></td>
    </tr>

    <tr id="tr_IsActive" runat="server">
        <td>Is Active</td>
        <td>
            <asp:CheckBox ID="chkEditIsActive" runat="server" ></asp:CheckBox>
        </td>
    </tr>

    <tr id="tr_KitContents" runat="server">
        <td>Kit Contents</td>
        <td>
            <telerik:RadTextBox ID="txtKitContents" runat="server" Enabled="false"></telerik:RadTextBox>
        </td>
    </tr>

    <tr id="tr_Width" runat="server">
        <td>Width</td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBox_Width" runat="server" NumberFormat-DecimalDigits="2"></telerik:RadNumericTextBox></td>
    </tr>
    <tr id="tr_Height" runat="server">
        <td>Height</td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBox_Height" runat="server" NumberFormat-DecimalDigits="2"></telerik:RadNumericTextBox></td>
    </tr>
    <tr id="tr_Length" runat="server">
        <td>Length</td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBox_Length" runat="server" NumberFormat-DecimalDigits="2"></telerik:RadNumericTextBox></td>
    </tr>
    <tr id="tr_Weight" runat="server">
        <td>Weight</td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBox_Weight" runat="server" NumberFormat-DecimalDigits="2"></telerik:RadNumericTextBox></td>
    </tr>
    <tr id="tr_Items" runat="server">
        <td>Kit Items</td>
        <td>
            <telerik:RadListBox ID="RadListBox_Items" runat="server" 
                DataValueField="ItemID" DataTextField="ItemName">
            </telerik:RadListBox>
        </td>
    </tr>
    <tr id="tr1" runat="server">
        <td></td>
        <td>
            <asp:LinkButton ID="lnkBtnItems" runat="server" Text="Edit Items" OnClick="lnkBtnItems_OnClick"></asp:LinkButton></td>
    </tr>
    <tr id="tr2" runat="server">
        <td></td>
        <td></td>
    </tr>
    <tr runat="server" id="td_CommandCell">
        <td colspan="2">
            <asp:LinkButton ID="btnUpdate" Text="Save" runat="server" CommandName="Update" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnInsert" Text="Save" runat="server" CommandName="PerformInsert" Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False" CommandName="Cancel" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnDelete" Text="Delete" runat="server" CausesValidation="False" CommandName="Cancel" OnClick="btnDelete_OnClick" Font-Size="Medium"></asp:LinkButton>
        </td>
    </tr>
</table>
