﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;

namespace PowerShipping.WebSite.Controls
{
    public partial class StateList : UserControl
    {
        public delegate void ProjectsListHandler(string projectId);

        private readonly UserManagementPresenter presenter = new UserManagementPresenter();

        public string StateName
        {
            get
            {
                if (RadComboBox_States.SelectedValue != null)

                    return RadComboBox_States.Text;

                return string.Empty;
            }
        }

        public string StateID
        {
            get
            {
                if (RadComboBox_States.SelectedValue != null)

                    return RadComboBox_States.SelectedItem.Value;

                return string.Empty;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected override void OnInit(EventArgs e)
        {
            InitControls();

            base.OnInit(e);
        }
        private void InitControls()
        {

            List<ListItem> list = new List<ListItem>();
            list.Add(new ListItem("----", Guid.Empty.ToString()));

            List<State> listP = presenter.GetAllStates();

            foreach (State project in listP)
                {
                    list.Add(new ListItem(project.Name, project.Id.ToString()));
                }

            RadComboBox_States.DataSource = list;
            RadComboBox_States.DataBind();
           
        }
   
    }
}