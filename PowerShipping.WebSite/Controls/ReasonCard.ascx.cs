﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using Telerik.Web.UI;

namespace PowerShipping.WebSite.Controls
{
    public partial class ReasonCard : System.Web.UI.UserControl
    {
        private readonly UserManagementPresenter presenter = new UserManagementPresenter();

        public Guid ReasonId
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_Id.Value))
                           ? new Guid(HiddenField_Id.Value)
                           : Guid.Empty;
            }
            set { HiddenField_Id.Value = value.ToString(); }
        }

        public object DataItem { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnInit(EventArgs e)
        {
            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');");

            InitializeComponent();
            base.OnInit(e);
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                Reason r = presenter.GetReasonById(ReasonId);
                presenter.DeleteReason(r);
            }
            catch (Exception ex)
            {
                Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to delete reason. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                Helper.ErrorInGrid("RadGrid_Reasons", Page, l);
            }
        }

        private void InitializeComponent()
        {
            DataBinding += Init_DataBinding;
        }

        private void Init_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        public void InitData()
        {
            InitDropDowns();

            if ((DataItem != null) & (DataItem.GetType() != typeof(GridInsertionObject)))
            {
                var entity = (Reason)DataItem;

                RadTextBox_Code.Text = entity.ReasonCode;
                RadTextBox_Description.Text = entity.ReasonDescription;

                ReasonId = entity.ReasonID;
            }
        }

        public Reason FillEntity()
        {
            var entity = new Reason();
            if (ReasonId != Guid.Empty)
            {
                entity.ReasonID = ReasonId;
            }
            else
            {
                entity.ReasonID = Guid.NewGuid();
            }

            entity.ReasonCode = RadTextBox_Code.Text;
            entity.ReasonDescription = RadTextBox_Description.Text;

            return entity;
        }

        private void InitDropDowns()
        {
        }
    }
}