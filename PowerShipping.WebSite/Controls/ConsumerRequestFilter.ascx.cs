﻿using System;
using System.Text;
using System.Web.UI.WebControls;
using PowerShipping.Business.Presenters;
using PowerShipping.Entities;
using System.Configuration;

namespace PowerShipping.WebSite.Controls
{
    public partial class ConsumerRequestFilter : System.Web.UI.UserControl
    {
        public delegate void ConsumerRequestFilterHandler(string customQuery);
        public event ConsumerRequestFilterHandler OnFilter;

        public delegate void ConsumerRequestFilterClearHandler(string customQuery);
        public event ConsumerRequestFilterClearHandler OnClear;

        ConsumerRequestsPresenter presenter = new ConsumerRequestsPresenter();

        public bool FillView
        {
            get
            {
                return chbFullView.Checked;
            }
        }

        public bool ForLabelRequest { get; set; }

        public string DefaultQuery
        {
            get { return "select * from [vw_ConsumerRequest] where ([Status] <> 7)"; }
        }

        public string CurrentQuery
        {
            get
            {
                return BuildQuery();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                FillWithConsumerRequestStatuses();
                if (ForLabelRequest)
                {
                    ListBox_Status.SelectedValue = ConsumerRequestStatus.RequestReceived.ToString();
                    Button_Filter_Click(null, null);
                }
            }
        }

        protected void Button_Filter_Click(object sender, EventArgs e)
        {
            string query = BuildQuery();
            OnFilter(query);
        }

        protected void Button_Clear_Click(object sender, EventArgs e)
        {
            Clear();
            string query = BuildQuery();
            OnClear(query);
        }

        private void Clear()
        {
            RadTextBox_AccountNumber.Text = null;
            RadTextBox_AccountName.Text = null;
            RadTextBox_PremiseID.Text = null;
            RadTextBox_Address.Text = null;
            RadTextBox_Address2.Text = null;
            RadTextBox_City.Text = null;
            RadTextBox_State.Text = null;
            RadTextBox_ZipCode.Text = null;
            RadTextBox_Email.Text = null;
            RadTextBox_Phone1.Text = null;
            RadTextBox_Method.Text = null;
            RadioButtonList_Reship.SelectedIndex = 0;
            RadioButtonList_OptOut.SelectedIndex = 0;
            RadioButtonList_Contact.SelectedIndex = 0;
            RadDatePicker_AnalysisStartDate.SelectedDate = null;
            RadDatePicker_AnalysisEndDate.SelectedDate = null;
            RadDatePicker_RecipeStartDate.SelectedDate = null;
            RadDatePicker_RecipeEndDate.SelectedDate = null;
            RadDatePicker_ImportStartDate.SelectedDate = null;
            RadDatePicker_ImportEndDate.SelectedDate = null;
            RadDatePicker_AuditFailureStartDate.SelectedDate = null;
            RadDatePicker_AuditFailureEndDate.SelectedDate = null;
            RadTextBox_WaterHeater.Text = null;
            RadTextBox_HeaterFuel.Text = null;
            RadTextBox_OperatingCompany.Text = null;
            RadTextBox_BatchId.Text = null;
            RadTextBox_Notes.Text = null;
            ListBox_Status.SelectedValue = null;
            RadTextBox_CompanyName.Text = null;
            RadTextBox_ServiceAddress1.Text = null;
            RadTextBox_AccountNumberOld.Text = null;
            //PG31
            TxtFacilityType.Text = null;
            RadTextBox_RequestedKit.Text = null;

        }

        private string BuildQuery()
        {
            StringBuilder sb = new StringBuilder();
            string maxReturnedRows = ConfigurationManager.AppSettings.Get("MaxReturnedRowNumber");
            sb.AppendFormat(String.Format("select TOP {0} * from [vw_ConsumerRequest] where ([Status] <> 7)   ", maxReturnedRows));
            if (!string.IsNullOrEmpty(RadTextBox_AccountNumber.Text.Trim()))
            {
                if (RadTextBox_AccountNumber.Text.Trim() == "null")
                    sb.AppendFormat(" and ([AccountNumber] IS NULL OR [AccountNumber] = '')");
                else
                    sb.AppendFormat(" and ([AccountNumber] LIKE '%'+ '{0}' +'%')   ", RadTextBox_AccountNumber.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_AccountName.Text.Trim()))
            {
                if (RadTextBox_AccountName.Text.Trim() == "null")
                    sb.AppendFormat(" and ([AccountName] IS NULL OR [AccountName] = '')");
                else
                    sb.AppendFormat(" and ([AccountName] LIKE '%'+ '{0}' +'%')   ", RadTextBox_AccountName.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_CompanyName.Text.Trim()))
            {
                if (RadTextBox_CompanyName.Text.Trim() == "null")
                    sb.AppendFormat(" and ([CompanyName] IS NULL OR [CompanyName] = '')");
                else
                    sb.AppendFormat(" and ([CompanyName] LIKE '%'+ '{0}' +'%')   ", RadTextBox_CompanyName.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_PremiseID.Text.Trim()))
            {
                if (RadTextBox_PremiseID.Text.Trim() == "null")
                    sb.AppendFormat(" and ([PremiseID] IS NULL OR [PremiseID] = '')");
                else
                    sb.AppendFormat(" and ([PremiseID] LIKE '%'+ '{0}' +'%')   ", RadTextBox_PremiseID.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_Address.Text.Trim()))
            {
                if (RadTextBox_Address.Text.Trim() == "null")
                    sb.AppendFormat(" and ([Address1] IS NULL OR [Address1] = '')");
                else
                    sb.AppendFormat(" and ([Address1] LIKE '%'+ '{0}' +'%')   ", RadTextBox_Address.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_ServiceAddress1.Text.Trim()))
            {
                if (RadTextBox_ServiceAddress1.Text.Trim() == "null")
                    sb.AppendFormat(" and ([ServiceAddress1] IS NULL OR [ServiceAddress1] = '')");
                else
                    sb.AppendFormat(" and ([ServiceAddress1] LIKE '%'+ '{0}' +'%')   ", RadTextBox_ServiceAddress1.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_Address2.Text.Trim()))
            {
                if (RadTextBox_Address2.Text.Trim() == "null")
                    sb.AppendFormat(" and ([Address2] IS NULL OR [Address2] = '')");
                else
                    sb.AppendFormat(" and ([Address2] LIKE '%'+ '{0}' +'%')   ", RadTextBox_Address2.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_BatchId.Text.Trim()))
            {
                if (RadTextBox_BatchId.Text.Trim() == "null")
                    sb.AppendFormat(" and ([BatchLabel] IS NULL OR [BatchLabel] = '')");
                else
                    sb.AppendFormat(" and ([BatchLabel] LIKE '%'+ '{0}' +'%')   ", RadTextBox_BatchId.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_City.Text.Trim()))
            {
                if (RadTextBox_City.Text.Trim() == "null")
                    sb.AppendFormat(" and ([City] IS NULL OR [City] = '')");
                else
                    sb.AppendFormat(" and ([City] LIKE '%'+ '{0}' +'%')   ", RadTextBox_City.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_Email.Text.Trim()))
            {
                if (RadTextBox_Email.Text.Trim() == "null")
                    sb.AppendFormat(" and ([Email] IS NULL OR [Email] = '')");
                else
                    sb.AppendFormat(" and ([Email] LIKE '%'+ '{0}' +'%')   ", RadTextBox_Email.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_Method.Text.Trim()))
            {
                if (RadTextBox_Method.Text.Trim() == "null")
                    sb.AppendFormat(" and ([Method] IS NULL OR [Method] = '')");
                else
                    sb.AppendFormat(" and ([Method] LIKE '%'+ '{0}' +'%')   ", RadTextBox_Method.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_Notes.Text.Trim()))
            {
                if (RadTextBox_Notes.Text.Trim() == "null")
                    sb.AppendFormat(" and ([Notes] IS NULL OR [Notes] = '')");
                else
                    sb.AppendFormat(" and ([Notes] LIKE '%'+ '{0}' +'%')   ", RadTextBox_Notes.Text);
            }

            //---------   AccountNumberOld ----//

            if (!string.IsNullOrEmpty(RadTextBox_AccountNumberOld.Text.Trim()))
            {
                if (RadTextBox_AccountNumberOld.Text.Trim() == "null")
                    sb.AppendFormat(" and ([AccountNumberOld] IS NULL OR [AccountNumberOld] = '')");
                else
                    sb.AppendFormat(" and ([AccountNumberOld] LIKE '%'+ '{0}' +'%')   ", RadTextBox_AccountNumberOld.Text);
            }

            if (!string.IsNullOrEmpty(RadTextBox_Phone1.Text.Trim()))
            {
                if (RadTextBox_Phone1.Text.Trim() == "null")
                    sb.AppendFormat(" and ([Phone1] IS NULL OR [Phone1] = '')");
                else
                    sb.AppendFormat(" and ([Phone1] LIKE '%'+ '{0}' +'%')   ", RadTextBox_Phone1.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_State.Text.Trim()))
            {
                if (RadTextBox_State.Text.Trim() == "null")
                    sb.AppendFormat(" and ([State] IS NULL OR [State] = '')");
                else
                    sb.AppendFormat(" and ([State] LIKE '%'+ '{0}' +'%')   ", RadTextBox_State.Text);
            }
            if (!string.IsNullOrEmpty(RadTextBox_WaterHeater.Text.Trim()))
            {
                if (RadTextBox_WaterHeater.Text.Trim() == "null")
                    sb.AppendFormat(" and ([WaterHeaterFuel] IS NULL OR [WaterHeaterFuel] = '')");
                else
                    sb.AppendFormat(" and ([WaterHeaterFuel] LIKE '%'+ '{0}' +'%')   ", RadTextBox_WaterHeater.Text);
            }

            if (!string.IsNullOrEmpty(RadTextBox_HeaterFuel.Text.Trim()))
            {
                if (RadTextBox_HeaterFuel.Text.Trim() == "null")
                    sb.AppendFormat(" and ([HeaterFuel] IS NULL OR [HeaterFuel] = '')");
                else
                    sb.AppendFormat(" and ([HeaterFuel] LIKE '%'+ '{0}' +'%')   ", RadTextBox_HeaterFuel.Text);
            }

            if (!string.IsNullOrEmpty(RadTextBox_OperatingCompany.Text.Trim()))
            {
                if (RadTextBox_OperatingCompany.Text.Trim() == "null")
                    sb.AppendFormat(" and ([OperatingCompany] IS NULL OR [OperatingCompany] = '')");
                else
                    sb.AppendFormat(" and ([OperatingCompany] LIKE '%'+ '{0}' +'%')   ", RadTextBox_OperatingCompany.Text);
            }
            //PG31
            if (!string.IsNullOrEmpty(TxtFacilityType.Text.Trim()))
            {
                if (RadTextBox_WaterHeater.Text.Trim() == "null")
                    sb.AppendFormat(" and ([FacilityType] IS NULL OR [FacilityType] = '')");
                else
                    sb.AppendFormat(" and ([FacilityType] LIKE '%'+ '{0}' +'%')   ", TxtFacilityType.Text);
            }

            if (!string.IsNullOrEmpty(RadTextBox_RequestedKit.Text.Trim()))
            {
                if (RadTextBox_RequestedKit.Text.Trim() == "null")
                    sb.AppendFormat(" and ([RequestedKit] IS NULL OR [RequestedKit] = '')");
                else
                    sb.AppendFormat(" and ([RequestedKit] LIKE '%'+ '{0}' +'%')   ", RadTextBox_RequestedKit.Text);
            }
            //PG31
            if (!string.IsNullOrEmpty(RadTextBox_ZipCode.Text.Trim()))
            {
                if (RadTextBox_ZipCode.Text.Trim() == "null")
                    sb.AppendFormat(" and ([ZipCode] IS NULL OR [ZipCode] = '')");
                else
                    sb.AppendFormat(" and ([ZipCode] LIKE '%'+ '{0}' +'%')   ", RadTextBox_ZipCode.Text);
            }

            if (RadioButtonList_Reship.SelectedValue != "-1")
            {
                sb.AppendFormat(" and ([IsReship] = '{0}')   ", RadioButtonList_Reship.SelectedValue);
            }

            if (this.RadioButtonList_OptOut.SelectedValue != "-1")
            {
                sb.AppendFormat(" and ([DoNotShip] = '{0}')   ", this.RadioButtonList_OptOut.SelectedValue);
            }

            if (this.RadioButtonList_Contact.SelectedValue != "-1")
            {
                sb.AppendFormat(" and ([IsOkayToContact] = '{0}')   ", this.RadioButtonList_Contact.SelectedValue);
            }

            if (this.RadDatePicker_AuditFailureEndDate.SelectedDate.HasValue || this.RadDatePicker_AuditFailureStartDate.SelectedDate.HasValue)
            {
                sb.AppendFormat(" and ( ");
                if (this.RadDatePicker_AuditFailureStartDate.SelectedDate.HasValue)
                    sb.AppendFormat("[AuditFailureDate] >= '{0}'", this.RadDatePicker_AuditFailureStartDate.SelectedDate.Value.ToString("MM/dd/yyyy"));
                if (this.RadDatePicker_AuditFailureEndDate.SelectedDate.HasValue & this.RadDatePicker_AuditFailureStartDate.SelectedDate.HasValue)
                    sb.AppendFormat(" and ");
                if (this.RadDatePicker_AuditFailureEndDate.SelectedDate.HasValue)
                    sb.AppendFormat("[AuditFailureDate] < '{0}'", this.RadDatePicker_AuditFailureEndDate.SelectedDate.Value.AddDays(1).ToString("MM/dd/yyyy"));
                sb.AppendFormat(" )   ");
            }
            if (cbAuditFailureDate.Checked)
            {
                sb.AppendFormat(" and ([AuditFailureDate] IS NULL) ");
            }

            if (this.RadDatePicker_AnalysisEndDate.SelectedDate.HasValue || this.RadDatePicker_AnalysisStartDate.SelectedDate.HasValue)
            {
                sb.AppendFormat(" and ( ");
                if (this.RadDatePicker_AnalysisStartDate.SelectedDate.HasValue)
                    sb.AppendFormat("[AnalysisDate] >= '{0}'", this.RadDatePicker_AnalysisStartDate.SelectedDate.Value.ToString("MM/dd/yyyy"));
                if (this.RadDatePicker_AnalysisEndDate.SelectedDate.HasValue & this.RadDatePicker_AnalysisStartDate.SelectedDate.HasValue)
                    sb.AppendFormat(" and ");
                if (this.RadDatePicker_AnalysisEndDate.SelectedDate.HasValue)
                    sb.AppendFormat("[AnalysisDate] < '{0}'", this.RadDatePicker_AnalysisEndDate.SelectedDate.Value.AddDays(1).ToString("MM/dd/yyyy"));
                sb.AppendFormat(" )   ");
            }
            if (cbAnalysisDate.Checked)
            {
                sb.AppendFormat(" and ([AnalysisDate] IS NULL) ");
            }

            if (this.RadDatePicker_ImportEndDate.SelectedDate.HasValue || this.RadDatePicker_ImportStartDate.SelectedDate.HasValue)
            {
                sb.AppendFormat(" and ( ");
                if (this.RadDatePicker_ImportStartDate.SelectedDate.HasValue)
                    sb.AppendFormat("[CRImportedDate] >= '{0}'", this.RadDatePicker_ImportStartDate.SelectedDate.Value.ToString("MM/dd/yyyy"));
                if (this.RadDatePicker_ImportEndDate.SelectedDate.HasValue & this.RadDatePicker_ImportStartDate.SelectedDate.HasValue)
                    sb.AppendFormat(" and ");
                if (this.RadDatePicker_ImportEndDate.SelectedDate.HasValue)
                    sb.AppendFormat("[CRImportedDate] < '{0}'", this.RadDatePicker_ImportEndDate.SelectedDate.Value.AddDays(1).ToString("MM/dd/yyyy"));
                sb.AppendFormat(" )   ");
            }
            if (cbImportDate.Checked)
            {
                sb.AppendFormat(" and ([CRImportedDate] IS NULL) ");
            }

            if (this.RadDatePicker_RecipeEndDate.SelectedDate.HasValue || this.RadDatePicker_RecipeStartDate.SelectedDate.HasValue)
            {
                sb.AppendFormat(" and ( ");
                if (this.RadDatePicker_RecipeStartDate.SelectedDate.HasValue)
                    sb.AppendFormat("[ReceiptDate] >= '{0}'", this.RadDatePicker_RecipeStartDate.SelectedDate.Value.ToString("MM/dd/yyyy"));
                if (this.RadDatePicker_RecipeEndDate.SelectedDate.HasValue & this.RadDatePicker_RecipeStartDate.SelectedDate.HasValue)
                    sb.AppendFormat(" and ");
                if (this.RadDatePicker_RecipeEndDate.SelectedDate.HasValue)
                    sb.AppendFormat("[ReceiptDate] < '{0}'", this.RadDatePicker_RecipeEndDate.SelectedDate.Value.AddDays(1).ToString("MM/dd/yyyy"));
                sb.AppendFormat(" )   ");
            }
            if (cbReceiptDate.Checked)
            {
                sb.AppendFormat(" and ([ReceiptDate] IS NULL) ");
            }

            string selectedRange = string.Empty;
            foreach (ListItem item in ListBox_Status.Items)
            {
                if (item.Selected)
                {
                    selectedRange += string.Format("'{0}',", (byte)(PowerShipping.Entities.ConsumerRequestStatus)presenter.GetValueByName(item.Text));
                }
            }
            if (!string.IsNullOrEmpty(selectedRange))
            {
                selectedRange = selectedRange.TrimEnd(',');
                sb.AppendFormat(" and ([Status] in ({0}))   ", selectedRange);
            }

            return sb.ToString();
        }

        private void FillWithConsumerRequestStatuses()
        {
            this.ListBox_Status.DataSource = presenter.GetStatuses();
            this.ListBox_Status.DataBind();
        }
    }
}