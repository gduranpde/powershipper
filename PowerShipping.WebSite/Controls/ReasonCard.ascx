﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReasonCard.ascx.cs" Inherits="PowerShipping.WebSite.Controls.ReasonCard" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:HiddenField  ID="HiddenField_Id" runat="server" />

<table> 
    <tr id="tr_Code" runat="server">
        <td>Code</td>
        <td><telerik:RadTextBox ID="RadTextBox_Code" runat="server" MaxLength="3"></telerik:RadTextBox></td>
    </tr>
    <tr id="tr_Description" runat="server">
        <td >Description</td>
        <td><telerik:RadTextBox ID="RadTextBox_Description" runat="server" MaxLength="20"></telerik:RadTextBox></td>
    </tr>   
    
    <tr runat="server" id="td_CommandCell">
        <td colspan="2">            
            <asp:LinkButton id="btnUpdate" text="Save" runat="server" CommandName="Update" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton id="btnInsert" text="Save" runat="server" CommandName="PerformInsert" Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnCancel" text="Cancel" runat="server" causesvalidation="False" commandname="Cancel" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnDelete" text="Delete" runat="server" causesvalidation="False" CommandName="Cancel" OnClick="btnDelete_OnClick" Font-Size="Medium"></asp:LinkButton>
         </td>
    </tr>   
</table>