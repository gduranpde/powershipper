﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProjectCard.ascx.cs"
    Inherits="PowerShipping.WebSite.Controls.ProjectCard" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:HiddenField ID="HiddenField_Id" runat="server" />
<telerik:radprogressmanager id="Radprogressmanager1" runat="server" />
<table>
    <tr>
        <td id="tr_ProjectName" runat="server">Project Name
        </td>
        <td>
            <telerik:radtextbox id="RadTextBox_ProjectName" runat="server" width="250px">
            </telerik:radtextbox>
        </td>
    </tr>
    <tr id="tr_ProjectCode" runat="server">
        <td>Project Code
        </td>
        <td>
            <telerik:radtextbox id="RadTextBox_ProjectCode" runat="server" width="250px">
            </telerik:radtextbox>
        </td>
    </tr>
    <tr id="tr_ProgramID" runat="server">
        <td>Program ID
        </td>
        <td>
            <telerik:radtextbox id="RadTextBox_ProgramID" runat="server" width="250px">
            </telerik:radtextbox>
        </td>
    </tr>
     <tr id="tr_ProjectState" runat="server">
        <td>Service State</td>
        <td>
            <telerik:RadCombobox id="cmbStates" runat="server" EmptyMessage="Select a state..."
                DataValueField="Id" DataTextField="Name" MaxHeight="350">
            </telerik:RadCombobox>
        </td>
    </tr>
    <tr id="tr_Company" runat="server">
        <td>Company
        </td>
        <td>
            <telerik:radcombobox id="RadComboBox_Companies" runat="server" datavaluefield="Id"
                datatextfield="CompanyName">
            </telerik:radcombobox>
        </td>
    </tr>
    <tr id="tr_Active" runat="server">
        <td>Is Active
        </td>
        <td>
            <asp:CheckBox ID="CheckBox_IsActive" runat="server"></asp:CheckBox>
        </td>
    </tr>
    <tr id="tr1" runat="server">
        <td>Returns Allowed
        </td>
        <td>
            <asp:CheckBox ID="CheckBox_ReturnsAllowed" runat="server" Checked="True"></asp:CheckBox>
        </td>
    </tr>
    <tr id="tr_Duplicates" runat="server">
        <td>Is AllowDuplicates
        </td>
        <td>
            <asp:CheckBox ID="CheckBox_IsAllowDuplicates" runat="server" 
                AutoPostBack="true" OnCheckedChanged="chb_IsAllowDuplicates_CheckedChanged">
            </asp:CheckBox>
        </td>
    </tr>
    <tr id="tr_DuplicateExceptions" runat="server">
        <td>Duplicate Exceptions
        </td>
        <td>
            <asp:CheckBox ID="CheckBox_DuplicateExceptions" runat="server" Enabled="false"></asp:CheckBox>
        </td>
    </tr>
    <tr id="tr_Projects" runat="server">
        <td>Projects</td>
        <td>
            <telerik:RadListBox id="RadListBox_Projects" runat="server" checkboxes="false" selectionmode="Multiple"
                datavaluefield="Id" datatextfield="ProjectName" visible="false" width="160px">
            </telerik:RadListBox>
        </td>
    </tr>
    <tr id="tr_RemoveProject" runat="server">
        <td></td>
        <td>
            <asp:Button ID="Button_RemoveProject" runat="server" Text="Remove" Visible="false"
                OnClick="btnRemoveProject_OnClick"></asp:Button>
        </td>
    </tr>
     <tr id="tr_ComboBoxProjects" runat="server">
        <td>Available Exception Projects</td>
        <td>
            <telerik:RadCombobox id="RadComboBox_Projects" runat="server" DataValueField="Id" Width="300"
                DataTextField="ProjectName" Visible="false">
            </telerik:RadCombobox>
        </td>
    </tr>
    <tr id="tr_AddProject" runat="server">
        <td></td>
        <td>
            <asp:Button ID="Button_AddProject" runat="server" Text="Add" Visible="true"
                OnClick="btnAddProject_OnClick" Enabled="false"></asp:Button>
        </td>
    </tr>
    <tr id="tr_IsAllowDupsInProject" runat="server">
        <td>Allow Duplicates in Same Project
        </td>
        <td>
            <asp:CheckBox ID="CheckBox_IsAllowDupsInProject" runat="server"></asp:CheckBox>
        </td>
    </tr>

    <%--<tr id="tr_States" runat="server">
        <td>Ship to States
        </td>
        <td>
            <asp:CheckBox ID="chb_StatesAll" runat="server" Text="All States" AutoPostBack="true"
                OnCheckedChanged="chb_StatesAll_CheckedChanged" />
            <br />
            <telerik:radlistbox id="RadListBox_States" runat="server" checkboxes="true" selectionmode="Multiple"
                datavaluefield="Id" datatextfield="Code">
            </telerik:radlistbox>
        </td>
    </tr>--%>

    <tr id="tr_SelectedStates" runat="server">
        <td>Selected States</td>
        <td>
            <telerik:RadListBox id="selectedStates" runat="server" selectionmode="Multiple"
                datavaluefield="Id" datatextfield="Name" visible="false" width="160px">
            </telerik:RadListBox>
        </td>
    </tr>
    <tr id="tr_RemoveStates" runat="server">
        <td></td>
        <td>
            <asp:Button ID="Button_RemoveState" runat="server" Text="Remove" Visible="false"
                OnClick="btnRemoveState_OnClick"></asp:Button>
        </td>
    </tr>
    <tr id="tr_AvailableStates" runat="server">
        <td>Ship to States</td>
        <td>
            <asp:CheckBox ID="chb_StatesAll" runat="server" Text="All States" AutoPostBack="true"
                OnCheckedChanged="chb_StatesAll_CheckedChanged" />
            <br />
            <telerik:RadCombobox id="cmbAvailableStates" runat="server" DataValueField="Id" DataTextField="Name" MaxHeight="350" >
            </telerik:RadCombobox>
        </td>
    </tr>    
    <tr id="tr_AddState" runat="server">
        <td></td>
        <td>
            <asp:Button ID="Button_AddState" runat="server" Text="Add" Visible="true"
                OnClick="btnAddState_OnClick" Enabled="true"></asp:Button>
        </td>
    </tr>

    <tr runat="server" id="td_CommandCell">
        <td colspan="2">
            <asp:LinkButton ID="btnUpdate" Text="Save" runat="server" CommandName="Update" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>'
                ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnInsert" Text="Save" runat="server" CommandName="PerformInsert"
                Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>' ValidationGroup="Card"
                Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False"
                CommandName="Cancel" OnClick="btnCancel_OnClick" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnDelete" Text="Delete" runat="server" CausesValidation="False"
                CommandName="Cancel" OnClick="btnDelete_OnClick" Font-Size="Medium"></asp:LinkButton>
        </td>
    </tr>
</table>
