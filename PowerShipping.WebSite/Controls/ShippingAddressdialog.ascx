﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShippingAddressdialog.ascx.cs"
    Inherits="PowerShipping.WebSite.Controls.ShippingAddressdialog" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<script type="text/javascript">
    function HidePopup() {
        document.getElementById("ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_ShippingAddressdialog_pnlPopUp2").style.display = "none";
        document.getElementById("ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_backgroundElement1").style.display = "none";

        return false;
    }
    //    function ShowPopup1() {
    //        alert("philip");
    //        document.getElementById("ctl00_ContentPlaceHolder1_pnlList").style.display = "block";
    //        alert("0");
    //        document.getElementById("ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_ShippingAddressdialog_pnlPopUp2").style.display = "none";
    //        alert("1");
    //        document.getElementById("ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_backgroundElement1").style.display = "none";
    //        alert("2");
    //    }

</script>

<asp:Panel ID="pnl2" runat="server">
</asp:Panel>
<asp:Panel ID="pnlPopUp2" runat="server" CssClass="modalPopupError2">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <asp:Label ID="LblAccount" runat="server" Text="Account #:"></asp:Label>
            </td>
            <td style="height: 25px;">
                <asp:Label ID="LblAccountNo" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LblName" runat="server" Text="Name:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="LblFullName" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LblAddress1" runat="server" Text="Address 1:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TxtAddress1" Width="250px" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LblAddress2" runat="server" Text="Address 2:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TxtAddress2" Width="250px" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LblCity" runat="server" Text="City:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TxtCity" Width="150px" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LblState" runat="server" Text="State:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DDLState" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LblZip" runat="server" Text="ZIP:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TxtZip" Width="80px" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" id="BtnSaveReturn" value="Save & Return" onclick="return SavePopup();" />
            </td>
            <td align="right">
                <asp:Button ID="BtnCancel2" runat="server" Text="Cancel" OnClientClick="return HidePopup();" />
            </td>
        </tr>
        <tr style="display: none">
            <asp:Label ID="lblGetID" Style="display: none" runat="server">
            </asp:Label>
        </tr>
    </table>
</asp:Panel>
<%--<ajaxToolkit:ModalPopupExtender ID="mpe2" runat="server" PopupControlID="pnlPopUp2"
    TargetControlID="pnl2" OkControlID="BtnCancel" BackgroundCssClass="modalBackground">
</ajaxToolkit:ModalPopupExtender>--%>
<%--<ajaxToolkit:ModalPopupExtender ID="mpe2" runat="server" PopupControlID="pnlPopUp2"
    TargetControlID="pnl2" OkControlID="BtnCancel2"
    BackgroundCssClass="modalBackground" Enabled="True">
</ajaxToolkit:ModalPopupExtender>
--%>