﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReshipSpecialProcessingCard.ascx.cs"
    Inherits="PowerShipping.WebSite.Controls.ReshipSpecialProcessingCard" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/ShippingAddressdialog.ascx" TagName="ShippingAddressdialog"
    TagPrefix="uc1" %>

<script type="text/javascript">
    function ShowPopup() {
        //        alert("reached popup");

        document.getElementById("ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_ShippingAddressdialog_pnlPopUp2").style.display = "block";
        document.getElementById("ShippingAddressDialog").style.zIndex = "900003";
        document.getElementById("ctl00_ContentPlaceHolder1_reshipSpecialProcessingCard_backgroundElement1").style.display = "block";
        document.getElementById("ctl00_ContentPlaceHolder1_pnlList").className = "modalPopupList retainPopup"
        return false;
    }
</script>

<table cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <asp:Repeater ID="rp" runat="server" OnItemDataBound="rp_OnItemDataBound" OnItemCommand="rp_ItemCommand">
                <HeaderTemplate>
                    <table class="grid" cellpadding="0" cellspacing="0">
                        <tr>
                            <th>
                            </th>
                            <th>
                                Account #
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Address
                            </th>
                            <th>
                                City
                            </th>
                            <th>
                                State
                            </th>
                            <th>
                                Zip
                            </th>
                            <th>
                                Kit
                            </th>
                            <th>
                                PO #
                            </th>
                            <th>
                                Tracking #
                            </th>
                            <th>
                                Shipper
                            </th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:LinkButton ID="LnkBtnEdit" CommandName="linkedit" runat="server">Edit</asp:LinkButton>
                        </td>
                        <td>
                            <asp:HiddenField ID="hfId" runat="server" />
                            <asp:Label ID="lbAccNum" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbName" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbAddress" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbCity" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbState" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbZip" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbKitType" runat="server"></asp:Label>
                            <%--<telerik:RadComboBox ID="RadComboBox_KitType" runat="server" DataValueField="Id" DataTextField="KitName" Width="100px">
                            </telerik:RadComboBox>--%>
                        </td>
                        <td>
                            <asp:TextBox ID="tbPONum" runat="server" Width="80px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="tbTrackNum" runat="server" Width="100px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList ID="DDL_Shipper" runat="server" DataTextField="ShipperName" DataValueField="Id"
                                Width="80px">
                            </asp:DropDownList>
                            <%--<telerik:RadComboBox ID="RadComboBox_Shipper" runat="server" DataValueField="Id"
                                DataTextField="ShipperName" Width="80px">
                            </telerik:RadComboBox>--%>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 5px;">
            <asp:Label ID="lbError" runat="server" ForeColor="Red"></asp:Label>
        </td>
    </tr>
    <%-- <tr>
        <td>
            <asp:Button ID="btnOk" runat="server" Text="Ok" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
        </td>
    </tr>--%>
</table>
<div id="ShippingAddressDialog">
    <uc1:ShippingAddressdialog ID="ShippingAddressdialog" runat="server" />
    <%--<a href="~/Controls/ShippingAddressdialog.ascx">~/Controls/ShippingAddressdialog.ascx</a>--%>
</div>
<div id="backgroundElement1" runat="server" class="modalBackground" style="position: fixed;
    display: none; left: 0px; top: 0px; width: 1530px; height: 1601px; z-index: 10003;">
</div>