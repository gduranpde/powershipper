﻿using System;
using System.Collections.Generic;
using System.IO;
using PowerShipping.Data;
using PowerShipping.Entities;
using PowerShipping.ImportExport;
using System.Data;

namespace PowerShipping.Business.Presenters
{
    public class BatchesPresenter
    {
        public List<ConsumerRequestBatch> GetConsumerRequestBatches(DateTime? startDate, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return DataProvider.Current.ConsumerRequestBatch.GetManyFiltered(startDate, userId, projectId, companyId);
        }

        public ConsumerRequestBatch GetByIdConsumerRequestBatch(Guid id)
        {
            return DataProvider.Current.ConsumerRequestBatch.GetOne(id);
        }

        public void DeleteConsumerRequestBatch(Guid id)
        {
            List<ConsumerRequest> crList = DataProvider.Current.ConsumerRequest.GetByBatch(id);

            bool allowDelete = true;
            foreach (var cr in crList)
            {
                if (cr.Status != ConsumerRequestStatus.RequestReceived)
                {
                    allowDelete = false;
                    break;
                }
            }
            if (allowDelete)
            {
                //delete CR, CREx, CRBatch
                DataProvider.Current.ConsumerRequestBatch.DeleteBatch(id);
            }
            else
            {
                throw new Exception("Cann't delete, because Batch Consumer Requests have Shipments.");
            }
        }

        public List<OptOutBatch> GetOptOutBatches(DateTime? startDate, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return DataProvider.Current.OptOutBatch.GetManyFiltered(startDate, userId, projectId, companyId);
        }

        public void DeleteOptOutBatch(Guid id)
        {
            //delete OO, OOEx, OOBatch
            DataProvider.Current.OptOutBatch.Remove(id);
        }

        public List<TrackingNumberBatch> GetTrackingNumberBatches(DateTime? startDate)
        {
            return DataProvider.Current.TrackingNumberBatch.GetManyFiltered(startDate);
        }

        public List<ShipmentDetail> DeleteTrackingNumberBatch(Guid id)
        {
            return DeleteTrackingNumberBatch(id, true);
        }

        public List<ShipmentDetail> DeleteTrackingNumberBatch(Guid id, bool check)
        {
            List<ShipmentDetail> sdInUse = new List<ShipmentDetail>();
            if (check)
            {
                List<ShipmentDetail> sdList = DataProvider.Current.ShipmentDetail.GetByFedExBatch(id);

                bool allowDelete = true;
                foreach (var sd in sdList)
                {
                    if (sd.ShippingStatus != ShipmentShippingStatus.Pending)
                    {
                        allowDelete = false;
                        //break;
                        sdInUse.Add(sd);
                    }
                }
                if (allowDelete)
                {
                    // undo changes in ShipmentDetails, delete TrNum, TrNumEx, TrNumBatch
                    DataProvider.Current.TrackingNumberBatch.Remove(id);
                }
                //else
                //{
                //    throw new Exception("Cann't delete, because Shipments in use.");
                //}
            }
            else
            {
                DataProvider.Current.TrackingNumberBatch.Remove(id);
            }
            return sdInUse;
        }

        public List<ShipmentDetail> DeleteReshipImportBatch(Guid id, string filePath)
        {
            return DeleteReshipImportBatch(id, true, filePath);
        }

        public List<ShipmentDetail> DeleteReshipImportBatch(Guid id, bool check, string filePath)
        {
            List<ShipmentDetail> sdInUse = new List<ShipmentDetail>();
            if (check)
            {
                List<ShipmentDetail> sdList = DataProvider.Current.ShipmentDetail.GetByFedExBatch(id);

                bool allowDelete = true;
                foreach (var sd in sdList)
                {
                    if (sd.ShippingStatus != ShipmentShippingStatus.Pending)
                    {
                        allowDelete = false;
                        //break;
                        sdInUse.Add(sd);
                    }
                }
                if (allowDelete)
                {
                    With.Transaction(() =>
                        {
                            // undo changes in ShipmentDetails, delete ReshipBatch, Delete ReshipExcep
                            DataProvider.Current.ReshipImportBatch.Remove(id);

                            var lines = ReadLines(filePath);

                            lines.ForEach(l =>
                                {
                                    string shipmentNumber = l[1];
                                    ShipmentDetail SD = DataProvider.Current.ShipmentDetail.GetByShipmentNumber(shipmentNumber);

                                    SD.ReshipStatus = ShipmentReshipStatus.ReadyForReship;
                                    DataProvider.Current.ShipmentDetail.Save(SD);
                                }
                                );
                        });
                }
                else
                {
                    throw new Exception("Cann't delete, because Shipments in use.");
                }
            }
            else
            {
                //DataProvider.Current.TrackingNumberBatch.Remove(id);
            }
            return sdInUse;
        }

        private List<string[]> ReadLines(string filePath)
        {
            var rows = new List<string[]>();

            var content = File.ReadAllText(filePath);

            var lines = content.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            for (var index = 1; index < lines.Length; index++) //Skip first line (index = 0)
            {
                string[] stringArray = lines[index].Split(',');

                rows.Add(stringArray);
            }

            return rows;
        }
        //PG31
        public bool ConsumerRequestStatusCheck(Guid id)
        {
            List<ConsumerRequest> crList = DataProvider.Current.ConsumerRequest.GetByBatch(id);

            bool RequestReceivedPresent = true;
            foreach (var cr in crList)
            {
                if (cr.Status != ConsumerRequestStatus.RequestReceived)
                {
                    RequestReceivedPresent = false;
                    break;
                }
            }
            return RequestReceivedPresent;
        }
        public DataSet ProcessImportData(Guid id)
        {
            return DataProvider.Current.ConsumerRequestBatch.ProcessImportData(id);
        }
        //PG31
    }
}