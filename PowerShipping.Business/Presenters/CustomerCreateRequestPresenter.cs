﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.Business.Presenters
{
    public class CustomerCreateRequestPresenter
    {
        public List<CustomerCreateRequestHistory> GetFiltered(string sql)
        {
            return DataProvider.Current.CustomerCreateRequestHistory.GetCustom(sql);
        }

        public List<Customer> GetCustomersMany(string accNum, string invCode, Guid projectId)
        {
            return DataProvider.Current.Customer.GetMany(accNum, invCode, projectId);
        }
    }
}
