﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using PowerShipping.Data;

namespace PowerShipping.Business.Presenters
{
    public class CustomerImportHistoryPresenter
    {
        public DataSet GetAll()
        {
            return DataProvider.Current.CustomerImportHistory.GetAll();
        }
    }
}
