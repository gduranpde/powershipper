﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using PowerShipping.Data;
using PowerShipping.Entities;
using PowerShipping.ImportExport;
using System.Data;

namespace PowerShipping.Business.Presenters
{
    public class ConsumerRequestsPresenter
    {
        public ConsumerRequestsPresenter()
        {
        }

        public void Delete(Guid id)
        {
            DataProvider.Current.ConsumerRequest.Remove(id);
        }

        public List<ConsumerRequest> GetAll(string accNum, string accName, string phone, string address)
        {
            return DataProvider.Current.ConsumerRequest.GetManyFiltered(false, accNum, accName, phone, address);
        }

        public List<ConsumerRequest> GetFiltered(string customQuery)
        {
            return DataProvider.Current.ConsumerRequest.GetCustom(customQuery);
        }
        //PG31
        public List<ConsumerRequest> GetByBatch(Guid batchId)
        {
            return DataProvider.Current.ConsumerRequest.GetByBatch(batchId);
        }
        public DataSet GetEquipmentAttributes(Guid ConsumerRequestID)
        {
            return DataProvider.Current.ConsumerRequest.GetEquipmentAttributes(ConsumerRequestID);
        }
        //PG31
        public List<ConsumerRequest> GetAllByQuantity(int quantity, bool? isReship, string accNum, string accName, string phone, string address,
            ref int total, string orderClause, Guid? projectId, string filerExpr)
        {
            int? status = Convert.ToInt32(ConsumerRequestStatus.RequestReceived);

            List<ConsumerRequest> list = DataProvider.Current.ConsumerRequest.GetManyFiltered(false, accNum, accName, phone, address, status, orderClause, projectId, filerExpr);

            total = list.Count;
            if (quantity == 0)
                quantity = list.Count;

            //if (isReship)
            //{
            //    list = (from entry in list
            //            orderby entry.IsReship descending
            //            select entry).Take(quantity).ToList();
            //}
            //else
            //{
            //    list = (from entry in list
            //            orderby entry.IsReship ascending
            //            select entry).Take(quantity).ToList();
            //}

            return list.Take(quantity).ToList();
        }

        public FedExShippingDefault GetFedExShippingDefault()
        {
            return DataProvider.Current.FedExShippingDefault.GetOne();
        }

        public void SaveFedExShippingDefault(FedExShippingDefault entity)
        {
            DataProvider.Current.FedExShippingDefault.Save(entity);
        }

        public ExportResult GenerateExportDate(List<Guid> listOfSelected, Shipment shipment, string exportedBy)
        {
            shipment.Id = Guid.NewGuid();
            shipment.CreatedBy = exportedBy;

            With.Transaction(() =>
            {
                DataProvider.Current.Shipment.Add(shipment);

                var sortOrder = 0;
                foreach (var consumerId in listOfSelected)
                {
                    var request = DataProvider.Current.ConsumerRequest.GetOne(consumerId);

                    DataProvider.Current.ShipmentDetail.Save(new ShipmentDetail
                    {
                        Id = Guid.NewGuid(),
                        ShipmentId = shipment.Id,
                        ConsumerRequestId = request.Id,
                        SortOrder = ++sortOrder,
                        HasException = false,
                        IsReship = request.IsReship,
                        ShipDate = null,
                        ShippingStatusDate = DateTime.Now,
                        ShippingStatus = ShipmentShippingStatus.Pending,
                        ReshipStatus = ShipmentReshipStatus.NA,
                        FedExBatchId = null,
                        FedExStatusDate = null,
                        FedExStatusCode = null,
                        FedExStatusHistory = null,
                        FedExTrackingNumber = null
                    });

                    request.Status = ConsumerRequestStatus.PendingShipment;
                    DataProvider.Current.ConsumerRequest.Save(request);
                }
            });

            return ExportShipments(shipment);
        }

        public ExportResult GenerateExportDate(Guid shipmentId)
        {
            return DataExchange.Current.ExportFedExLabels(shipmentId, ConfigurationManager.AppSettings.Get("UploadFolder"));
        }

        public Guid SaveExportDate(List<Guid> listOfSelected, Shipment shipment, string exportedBy, Guid projectId)
        {
            shipment.Id = Guid.NewGuid();
            shipment.CreatedBy = exportedBy;
            shipment.ProjectId = projectId;

            With.Transaction(() =>
            {
                DataProvider.Current.Shipment.Add(shipment);

                var sortOrder = 0;
                foreach (var consumerId in listOfSelected)
                {
                    var request = DataProvider.Current.ConsumerRequest.GetOne(consumerId);

                    DataProvider.Current.ShipmentDetail.Save(new ShipmentDetail
                    {
                        Id = Guid.NewGuid(),
                        ShipmentId = shipment.Id,
                        ConsumerRequestId = request.Id,
                        SortOrder = ++sortOrder,
                        HasException = false,
                        IsReship = request.IsReship,
                        ShipDate = null,
                        ShippingStatusDate = DateTime.Now,
                        ShippingStatus = ShipmentShippingStatus.Pending,
                        ReshipStatus = ShipmentReshipStatus.NA,
                        FedExBatchId = null,
                        FedExStatusDate = null,
                        FedExStatusCode = null,
                        FedExStatusHistory = null,
                        FedExTrackingNumber = null,
                        ShipperId = shipment.ShipperId,
                        ProjectId = shipment.ProjectId,

                        ShipmentDetailAddress1 = request.Address1,
                        ShipmentDetailAddress2 = request.Address2,
                        ShipmentDetailCity = request.City,
                        ShipmentDetailState = request.State,
                        ShipmentDetailZip = request.ZipCode
                    });

                    request.Status = ConsumerRequestStatus.PendingShipment;
                    DataProvider.Current.ConsumerRequest.Save(request);
                }
            });

            return shipment.Id;
        }

        public ExportResult ExportShipments(Shipment shipment)
        {
            return DataExchange.Current.ExportFedExLabels(shipment.Id, ConfigurationManager.AppSettings.Get("UploadFolder"));
        }

        public void ClearAllInfo()
        {
            DataProvider.Current.ConsumerRequest.ClearAll();
        }

        public void Save(ConsumerRequest cr)
        {
            if (cr.Id == Guid.Empty)
                cr.Id = Guid.NewGuid();

            try
            {
                DataProvider.Current.ConsumerRequest.Save(cr);
            }
            catch
            { }
        }

        public ConsumerRequest GetById(Guid id)
        {
            return DataProvider.Current.ConsumerRequest.GetOne(id);
        }

        public ConsumerRequest GetByAccountNumber(string accNum, Guid projectId)
        {
            return DataProvider.Current.ConsumerRequest.GetByAccountNumber(accNum, projectId);
        }

        public Array GetStatuses()
        {
            return Enum.GetValues(typeof(ConsumerRequestStatus));
        }

        public ConsumerRequestStatus GetValueByName(string name)
        {
            return (ConsumerRequestStatus)Enum.Parse(typeof(ConsumerRequestStatus), name);
        }

        public string GetNameByValue(byte value)
        {
            return Enum.GetName(typeof(ConsumerRequestStatus), value);
        }

        public DataSet GetEquipmentAttributesV3(Guid ConsumerRequestID)
        {
            return DataProvider.Current.ConsumerRequest.GetEquipmentAttributesV3(ConsumerRequestID);
        }
    }
}