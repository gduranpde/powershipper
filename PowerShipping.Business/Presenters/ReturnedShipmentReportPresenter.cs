﻿using System;
using System.Data;
using PowerShipping.Data;

namespace PowerShipping.Business.Presenters
{
    public class ReturnedShipmentReportPresenter
    {
        public DataSet GetAll(DateTime? start, DateTime? end, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return DataProvider.Current.ShipmentDetail.ReturnedShipmentReport(start, end, userId, projectId, companyId);
        }
    }
}