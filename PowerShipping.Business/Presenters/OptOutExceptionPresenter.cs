﻿using System;
using System.Collections.Generic;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.Business.Presenters
{
    public class OptOutExceptionPresenter
    {
        public List<OptOutException> GetAll(string accNum, string accName, string phone, string address, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return DataProvider.Current.OptOutException.GetMany(false, accNum, accName, phone, address, userId, projectId, companyId);
        }

        public void Save(OptOutException oo)
        {
            DataProvider.Current.OptOutException.Save(oo);
        }

        public OptOutException GetById(Guid id)
        {
            return DataProvider.Current.OptOutException.GetOne(id);
        }

        public void Delete(Guid id)
        {
            DataProvider.Current.OptOutException.Remove(id);
        }
    }
}
