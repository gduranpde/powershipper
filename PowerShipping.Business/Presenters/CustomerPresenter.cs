﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.Business.Presenters
{
    public class CustomerPresenter
    {
        public void Delete(Guid id)
        {
            DataProvider.Current.Customer.Remove(id);
        }

        public List<Customer> GetFiltered(string customQuery)
        {
            return DataProvider.Current.Customer.GetCustom(customQuery);
        }

        public void Update(Customer cr)
        {
            DataProvider.Current.Customer.Save(cr);
        }

        public void Add(Customer cr)
        {
            if (cr.Id == Guid.Empty)
                cr.Id = Guid.NewGuid();
            DataProvider.Current.Customer.Add(cr);
        }

        public Customer GetById(Guid id)
        {
            return DataProvider.Current.Customer.GetOne(id);
        }
    }
}
