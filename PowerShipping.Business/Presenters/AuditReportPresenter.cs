﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.Business.Presenters
{
    public class AuditReportPresenter
    {
        public DataSet GetAuditReportAccounts(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return DataProvider.Current.ConsumerRequest.GetAuditReportAccounts(poNum, start, end, kitType, userId, projectId, companyId);
        }

        public DataSet GetAuditReportExceptions(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return DataProvider.Current.ConsumerRequest.GetAuditReportExceptions(poNum, start, end, kitType, userId, projectId, companyId);
        }

        public List<KitType> GetAllKitTypes()
        {
            return DataProvider.Current.KitType.GetAll();
        }


        public DataSet GetAEGReport(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return DataProvider.Current.ConsumerRequest.GetAEGReport(poNum, start, end, kitType, userId, projectId, companyId);
        }
        public DataSet GetAEGReportPhase2(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return DataProvider.Current.ConsumerRequest.GetAEGReportPhase2(poNum, start, end, kitType, userId, projectId, companyId);
        }
        public DataSet GetAEGXMLReport(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return DataProvider.Current.ConsumerRequest.GetAEGXMLReport(poNum, start, end, kitType, userId, projectId, companyId);
        }
        public DataSet GetAEGReportExceptions(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return DataProvider.Current.ConsumerRequest.GetAEGReportExceptions(poNum, start, end, kitType, userId, projectId, companyId);
        }
        public DataSet GetAEGReportExceptionsPhase2(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return DataProvider.Current.ConsumerRequest.GetAEGReportExceptionsPhase2(poNum, start, end, kitType, userId, projectId, companyId);
        }
        
        public DataSet GetAEGReportExceptionsPhase2(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId, DateTime? ReceptionStartDate, DateTime? ReceptionEndDate)
        {
            return DataProvider.Current.ConsumerRequest.GetAEGReportExceptionsPhase2(poNum, start, end, kitType, userId, projectId, companyId, ReceptionStartDate, ReceptionEndDate);
        }

        public DataSet GetAEGReportExceptions(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId, DateTime? ReceptionStartDate, DateTime? ReceptionEndDate)
        {
            return DataProvider.Current.ConsumerRequest.GetAEGReportExceptions(poNum, start, end, kitType, userId, projectId, companyId, ReceptionStartDate, ReceptionEndDate);
        }
        public DataSet GetAEGXMLReportExceptions(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId, DateTime? ReceptionStartDate, DateTime? ReceptionEndDate)
        {
            return DataProvider.Current.ConsumerRequest.GetAEGXMLReportExceptions(poNum, start, end, kitType, userId, projectId, companyId, ReceptionStartDate, ReceptionEndDate);
        }       

        /// <summary>
        /// AEG Exception Report V3
        /// </summary>
        /// <param name="poNum"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="kitType"></param>
        /// <param name="userId"></param>
        /// <param name="projectId"></param>
        /// <param name="companyId"></param>
        /// <param name="ReceptionStartDate"></param>
        /// <param name="ReceptionEndDate"></param>
        /// <returns></returns>
        public DataSet GetAEGReportExceptionsV3(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId, DateTime? ReceptionStartDate, DateTime? ReceptionEndDate)
        {
            return DataProvider.Current.ConsumerRequest.GetAEGReportExceptionsV3(poNum, start, end, kitType, userId, projectId, companyId, ReceptionStartDate, ReceptionEndDate);
        }

        /// <summary>
        /// AEG XML Exceptions V3
        /// </summary>
        /// <param name="poNum"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="kitType"></param>
        /// <param name="userId"></param>
        /// <param name="projectId"></param>
        /// <param name="companyId"></param>
        /// <param name="ReceptionStartDate"></param>
        /// <param name="ReceptionEndDate"></param>
        /// <returns></returns>
        public DataSet GetAEGXMLReportExceptionsV3(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId, DateTime? ReceptionStartDate, DateTime? ReceptionEndDate)
        {
            return DataProvider.Current.ConsumerRequest.GetAEGXMLReportExceptionsV3(poNum, start, end, kitType, userId, projectId, companyId, ReceptionStartDate, ReceptionEndDate);
        }

        /// <summary>
        /// AEG Delivery V3
        /// </summary>
        /// <param name="poNum"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="kitType"></param>
        /// <param name="userId"></param>
        /// <param name="projectId"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public DataSet GetAEGDeliveryReportV3(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return DataProvider.Current.ConsumerRequest.GetAEGDeliveryReportV3(poNum, start, end, kitType, userId, projectId, companyId);
        }

        /// <summary>
        /// AEG XML Delivery V3
        /// </summary>
        /// <param name="poNum"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="kitType"></param>
        /// <param name="userId"></param>
        /// <param name="projectId"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public DataSet GetAEGXMLDeliveryReportV3(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return DataProvider.Current.ConsumerRequest.GetAEGXMLDeliveryReportV3(poNum, start, end, kitType, userId, projectId, companyId);
        }        
    }
}