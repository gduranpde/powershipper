﻿using System;
using System.Collections.Generic;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.Business.Presenters
{
    public class ShipmentDetailsPresenter
    {
        public List<ShipmentDetail> GetFiltered(string customQuery)
        {
            return DataProvider.Current.ShipmentDetail.GetCustom(customQuery);
        }

        public List<ShipmentDetail> GetAll(string accNum, string accName, string phone, string address, Guid? fedExBatchId)
        {
            return DataProvider.Current.ShipmentDetail.GetManyFiltered(accNum, accName, phone, address, fedExBatchId);
        }

        public void Save(ShipmentDetail sd)
        {
            if (sd.ShipmentId == Guid.Empty)
            {
                Shipment s = new Shipment();
                s.Id = Guid.NewGuid();
                s.KitTypeId = sd.KitTypeId;
                s.PurchaseOrderNumber = sd.PurchaseOrderNumber;
                s.DateCustomerWillShip = DateTime.Now.Date;
                s.DateLabelsNeeded = DateTime.Now.Date;
                s.CreatedBy = System.Web.Security.Membership.GetUser().UserName;
                s.ProjectId = sd.ProjectId;
                s.ShipperId = sd.ShipperId;

                DataProvider.Current.Shipment.Save(s);
                sd.ShipmentId = s.Id;
            }

            if (sd.Id == Guid.Empty)
            {
                sd.Id = Guid.NewGuid();
            }

            DataProvider.Current.ShipmentDetail.Save(sd);
        }

        public ShipmentDetail GetById(Guid id)
        {
            return DataProvider.Current.ShipmentDetail.GetOne(id);
        }

        public void Save(ShipmentDetail sd, ConsumerRequest cr)
        {
            if (sd.ReshipStatus == ShipmentReshipStatus.Reshipped)
                cr.IsReship = true;

            DataProvider.Current.ShipmentDetail.Save(sd);
            DataProvider.Current.ConsumerRequest.Save(cr);
        }

        public void UpdatePurchaseOrderNumber(Guid shipmentId, string newOrderNumber)
        {
            Shipment s = DataProvider.Current.Shipment.GetOne(shipmentId);
            s.PurchaseOrderNumber = newOrderNumber;
            DataProvider.Current.Shipment.Save(s);
        }

        public void UpdateKitType(Guid shipmentId, Guid kitTypeId)
        {
            Shipment s = DataProvider.Current.Shipment.GetOne(shipmentId);
            s.KitTypeId = kitTypeId;
            DataProvider.Current.Shipment.Save(s);
        }

        public List<FedExLabel> GetByShipmentId(Guid id)
        {
            return DataProvider.Current.ShipmentDetail.GetFedExLabels(id);
        }

        public List<ShipmentDetail> GetByAccountNumber(string accNum)
        {
            return DataProvider.Current.ShipmentDetail.GetByAccountNumber(accNum);
        }

        public List<KitType> GetListOfKitTypes()
        {
            return DataProvider.Current.KitType.GetAll();
        }

        public List<KitType> GetAllKitTypes()
        {
            return DataProvider.Current.KitType.GetAllKitTypes();
        }

        public int GetMaxShippingNumber()
        {
            return DataProvider.Current.ShipmentDetail.GetMaxShippingNumber();
        }

        public bool IsShipmentNumberAlreadyExists(string shipmentNumber)
        {
            ShipmentDetail sd = DataProvider.Current.ShipmentDetail.GetByShipmentNumber(shipmentNumber);

            return !(sd == null);
        }

        public void Remove(Guid id)
        {
            DataProvider.Current.ShipmentDetail.Remove(id);
        }

        public List<ShipmentDetail> GetMissingShipmentDetails(string poNum)
        {
            return DataProvider.Current.ShipmentDetail.GetMissingShipmentDetails(poNum);
        }

        public List<Reason> GetAllReasons()
        {
            return DataProvider.Current.Reasons.GetAll();
        }

        public List<ShipmentDetail> GetShipmentDetailByShipmentID(Guid ShipmentId)
        {
            return DataProvider.Current.ShipmentDetail.GetByShipment(ShipmentId);
        }
    }
}