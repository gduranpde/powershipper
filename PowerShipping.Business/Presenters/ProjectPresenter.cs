﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PowerShipping.Data;
using PowerShipping.Entities;
namespace PowerShipping.Business.Presenters
{
    public class ProjectPresenter
    {
        public string GetProgramID(Guid projectId)
        {
           return DataProvider.Current.Project.GetProgramID(projectId);
        }
    }
}
