﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.Business.Presenters
{
    public class ShipmentDetailReportPresenter
    {
        //public DataSet GetAll(DateTime? start, DateTime? end, int? shippingStatus)
        //{
        //    return DataProvider.Current.ShipmentDetail.GetManyFilteredDataSet(start, end, shippingStatus);
        //}

        public List<ShipmentDetail> GetAll(DateTime? start, DateTime? end, int? shippingStatus, Guid? userId, Guid? projectId, Guid? companyId)
        {
            List<ShipmentDetail> list = DataProvider.Current.ShipmentDetail.GetManyFiltered(start, end, shippingStatus, userId, projectId, companyId);
            list = (from entry in list orderby entry.ShipDate ascending select entry).ToList();
            return list;
        }
    }
}