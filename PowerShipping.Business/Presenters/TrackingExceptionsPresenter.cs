﻿using System;
using System.Collections.Generic;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.Business.Presenters
{
    public class TrackingExceptionsPresenter
    {
        public List<TrackingNumberException> GetAll(string accNum, string trNum, string poNum, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return DataProvider.Current.TrackingNumberException.GetAll(false, accNum, trNum, poNum, userId, projectId, companyId);
        }

        public void Save(TrackingNumberException tr)
        {
            DataProvider.Current.TrackingNumberException.Save(tr);
        }

        public TrackingNumberException GetById(Guid id)
        {
            return DataProvider.Current.TrackingNumberException.GetOne(id);
        }

        public ShipmentDetail GetShipmentByShipmentNumber(string shipNum)
        {
            return DataProvider.Current.ShipmentDetail.GetByShipmentNumber(shipNum);
        }

        public void SaveShipmentDetail(ShipmentDetail sd)
        {
            DataProvider.Current.ShipmentDetail.Save(sd);
        }
    }
}
