﻿using PowerShipping.Data;
using PowerShipping.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Business.Presenters
{
    public class ItemCatalogPresenter
    {
        public void SaveItemCatalog(ItemCatalog itemCatalog)
        {
            DataProvider.Current.ItemCatalog.Save(itemCatalog);
        }

    }
}
