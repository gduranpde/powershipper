﻿using PowerShipping.Data;
using PowerShipping.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Business.Presenters
{
   public  class ItemsSetupPresenter
    {
       public List<Item> GetAllItems()
       {
           return DataProvider.Current.Item.GetAll();
       }

       public void SaveItem(Item item)
       {
           DataProvider.Current.Item.Save(item);
       }

       public void DeleteItem(Item item)
       {
           DataProvider.Current.Item.Remove(item);
       }

       public Item GetItemById(Guid itemID)
       {
           return DataProvider.Current.Item.GetOne(itemID);
       }

    }
}
