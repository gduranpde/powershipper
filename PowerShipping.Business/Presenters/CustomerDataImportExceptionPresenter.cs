﻿using System;
using System.Collections.Generic;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.Business.Presenters
{
    public class CustomerDataImportExceptionPresenter
    {
		public List<CustomerDataImportException> GetByCustomerDataImportId(Guid id)
        {
			return DataProvider.Current.CustomerDataImportException.GetByCustomerDataImportId(id);
        }
    }
}