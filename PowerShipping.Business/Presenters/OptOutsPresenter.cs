﻿using System;
using System.Collections.Generic;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.Business.Presenters
{
    public class OptOutsPresenter
    {
        public List<OptOut> GetAll(DateTime? start, DateTime? end, string accNum, string accName, string phone, string address, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return DataProvider.Current.OptOut.GetManyFiltered(start, end, accNum, accName, phone, address, userId, projectId, companyId);
        }

        public void Add(OptOut oo)
        {
            DataProvider.Current.OptOut.Add(oo);
        }

        public void Save(OptOut oo)
        {
            DataProvider.Current.OptOut.Save(oo);
        }

        public OptOut GetById(Guid id)
        {
            return DataProvider.Current.OptOut.GetOne(id);
        }

        public OptOut GetByAccNumber(string accNum, Guid projectId)
        {
            return DataProvider.Current.OptOut.GetByAccountNumber(accNum, projectId);
        }

        public void RemoveOptOut(Guid id)
        {
            DataProvider.Current.OptOut.Remove(id);
        }
    }
}