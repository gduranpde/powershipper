﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PowerShipping.Entities;
using PowerShipping.Data;
using System.Globalization;

namespace PowerShipping.Business.Presenters
{
    public class KitTypesPresenter
    {
        public KitTypesPresenter()
        {
        }

        public List<KitType> GetAll()
        {
            CultureInfo ci = new CultureInfo("en-US");
            List<KitType> list = DataProvider.Current.KitType.GetAll();
            foreach (KitType kit in list)
            {
                kit.KitItems = GetKitItemsByKitType(kit.Id);
            }

            return list;
        }

        public List<KitType> GetAllUpdated()
        {
            List<KitType> list = DataProvider.Current.KitType.GetAll();
            List<KitType> newList = new List<KitType>();
            foreach (KitType kit in list)
            {
                List<KitContent> kitContents = GetKitContentByKitType(kit.Id);
                string kitContentsField = string.Empty;
                if (kitContents.Count == 0)
                {
                    kit.KitContents = kitContentsField;
                }
                else
                {
                    foreach (KitContent kitContent in kitContents)
                    {
                        kitContentsField = kitContentsField + kitContent.Quantity + "-" + kitContent.ItemName + ", ";
                    }
                    if (!string.IsNullOrEmpty(kitContentsField))
                    {
                        kit.KitContents = kitContentsField.Remove(kitContentsField.Length - 2);
                    }
                }
                newList.Add(kit);
            }
            return newList;

        }

        public List<KitContent> GetKitContentByKitType(Guid kitTypeId)
        {
            return DataProvider.Current.KitContents.GetFromKitContentsByKitType(kitTypeId);
        }


        public KitType GetOne(Guid kitTypeId)
        {
            return DataProvider.Current.KitType.GetOne(kitTypeId);
        }

        public void AddKitType(KitType kitType)
        {
            DataProvider.Current.KitType.Add(kitType);
        }

        public void SaveKitType(KitType kitType)
        {
            DataProvider.Current.KitType.Save(kitType);
        }


        public void DeleteKitType(Guid kitTypeId)
        {
            DataProvider.Current.KitType.Remove(kitTypeId);
        }

        public List<KitItem> GetKitItemsByKitType(Guid? id)
        {
            return DataProvider.Current.KitItem.GetByKitType(id);
        }

        public List<KitType> GetKitTypeByKitName(string kitName)
        {
            return DataProvider.Current.KitType.GetByKitName(kitName);
        }

        public List<KitContent> GetFromKitContentsByKitType(Guid? id)
        {
            return DataProvider.Current.KitContents.GetFromKitContentsByKitType(id);
        }


        public List<Item> GetAllItemNames()
        {
            return DataProvider.Current.Item.GetAllNames();
        }

        public KitContent GetKitContentById(Guid id)
        {
            return DataProvider.Current.KitContents.GetOne(id);
        }

        public void SaveKitContent(KitContent k)
        {
            DataProvider.Current.KitContents.Save(k);

        }

        public void SaveKitTypeItems(Guid itemID, Guid kitTypeID)
        {
            DataProvider.Current.KitContents.SaveKitTypeItem(itemID, kitTypeID);
        }

        public void DeleteKitContentItemById(Guid kitContentID, Guid kitID, Guid itemID)
        {
            DataProvider.Current.KitContents.DeleteKitContentItemById(kitContentID, kitID, itemID);
        }

        public void UpdateKitTypeItem(Guid kitID, Guid oldItemID, Guid newItemID)
        {
            DataProvider.Current.KitContents.UpdateKitTypeItem(kitID, oldItemID, newItemID);
        }
    }
}
