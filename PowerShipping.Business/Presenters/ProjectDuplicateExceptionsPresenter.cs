﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PowerShipping.Entities;
using PowerShipping.Data;
using System.Globalization;

namespace PowerShipping.Business.Presenters
{
    public class ProjectDuplicateExceptionsPresenter
    {
        public ProjectDuplicateExceptionsPresenter()
        {
        }

        public void AddProjectDuplicateException(ProjectDuplicateExceptions projectDuplicateException)
        {
            DataProvider.Current.ProjectDuplicateExceptions.Add(projectDuplicateException);
        }
    }
}
