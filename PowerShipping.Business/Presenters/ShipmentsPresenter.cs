﻿using System;
using System.Collections.Generic;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.Business.Presenters
{
    public class ShipmentsPresenter
    {
        public List<Shipment> GetAll(DateTime? start, DateTime? end, string poNum, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return DataProvider.Current.Shipment.GetManyFiltered(start, end, poNum, userId, projectId, companyId);
        }

        public void Delete(Guid id)
        {
            DataProvider.Current.Shipment.Remove(id);
        }

        public Shipment GetById(Guid id)
        {
            return DataProvider.Current.Shipment.GetOne(id);
        }

        public void Save(Shipment shipment)
        {
            DataProvider.Current.Shipment.Save(shipment);
        }
    }
}