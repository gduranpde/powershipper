﻿using System;
using System.Collections.Generic;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.Business.Presenters
{
    public class ConsumerExceptionsPresenter
    {
        public List<ConsumerRequestException> GetAll(string accNum, string accName, string phone, string address, Guid? batchId)
        {
            return DataProvider.Current.ConsumerRequestException.GetMany(false, accNum, accName, phone, address, batchId);
        }

        public List<ConsumerRequestException> GetAll(string accNum, string accName, string batchLabel, DateTime? date, Guid? batchId, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return DataProvider.Current.ConsumerRequestException.GetMany(false, accNum, accName, batchLabel, date, batchId, userId, projectId, companyId);
        }


        public void Save(ConsumerRequestException cr)
        {
            ConsumerRequestsPresenter presenterRequest = new ConsumerRequestsPresenter();
            ConsumerRequest request = new ConsumerRequest();
            PowerShipping.ImportExport.ValidationResult result;

            switch (cr.Status)
            {
                case ConsumerRequestExceptionStatus.Exception :
                    result = PowerShipping.ImportExport.EntityValidation.MapAndValidate(cr, request, true, true);

                    if (result.Success)
                    {
                        cr.Status = ConsumerRequestExceptionStatus.Deleted;

                        request.Id = Guid.NewGuid();
                        request.ChangedBy = System.Web.Security.Membership.GetUser().UserName;
                        request.Status = ConsumerRequestStatus.Exception;

                        presenterRequest.Save(request);

                    }
                    break;
                case ConsumerRequestExceptionStatus.OptOut :

                    result = PowerShipping.ImportExport.EntityValidation.MapAndValidate(cr, request, true, true);

                    if (result.Success)
                    {
                        cr.Status = ConsumerRequestExceptionStatus.Deleted;

                        request.Id = Guid.NewGuid();
                        request.ChangedBy = System.Web.Security.Membership.GetUser().UserName;
                        request.Status = ConsumerRequestStatus.OptOut;
                        request.DoNotShip = true;

                        presenterRequest.Save(request);

                    }
                    break;



            }


            DataProvider.Current.ConsumerRequestException.Save(cr);
        }

        public ConsumerRequestException GetById(Guid id)
        {
            return DataProvider.Current.ConsumerRequestException.GetOne(id);
        }
    }
}