﻿using PowerShipping.Data;
using PowerShipping.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Business.Presenters
{
    public class ItemSavingsPresenter
    {
        public List<ItemSavings> GetProjectAndOperatingCompaniesByState(int stateID, Guid itemId, bool onlyActiveProjects)
        {
            return DataProvider.Current.ItemSaving.GetProjectAndOperatingCompaniesByState(stateID, itemId, onlyActiveProjects);
        }

        public void UpdateItemSavings(Guid ItemID, string projectID, string operatingCompanyID, KwType type, double value, string identity, string catalogID)
        {
            DataProvider.Current.ItemSaving.UpdateItemSavings(ItemID, projectID, operatingCompanyID, type.ToString(), value, identity, catalogID);
        }

        public void UpdateItemCatalog(Guid ItemID, string projectID, string catalogID)
        {
            DataProvider.Current.ItemSaving.UpdateItemCatalog(ItemID, projectID, catalogID);            
        }
    }

}
