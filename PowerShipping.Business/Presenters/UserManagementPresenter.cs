﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.Business.Presenters
{
    public class UserManagementPresenter
    {
        public List<User> GetMany()
        {
            var coll = Membership.FindUsersByName("%%");

            var ret = User.GetUsersList(coll);

            return ret;
        }

        public User GetOne(string userName)
        {
            return new User(Membership.GetUser(userName));
        }

        public User GetById(Guid id)
        {
            return new User(Membership.GetUser(id));
        }

        public void Add(User obj)
        {
            if (obj.UserID == Guid.Empty)
            {
                obj.UserID = Guid.NewGuid();
            }
            Membership.CreateUser(obj.UserName, obj.Password);
            obj.SetRoles();
        }

        public void Save(User obj)
        {
            Membership.UpdateUser(obj);

            if (string.IsNullOrEmpty(obj.Password) == false)
            {
                obj.SetPassword(obj.Password);
            }

            obj.SetRoles();
        }

        public void Remove(params object[] obj)
        {
            if (obj[0] is User)
            {
                User u = (User)obj[0];
                Membership.DeleteUser(u.UserName, true);
            }

            if (obj[0] is string)
            {
                string name = (string)obj[0];
                Membership.DeleteUser(name);
            }

        }



        public string[] RolesList
        {
            get
            {
                return PDMRoles.Roles;
            }
        }

        public Company GetCompanyById(Guid companyId)
        {
            return DataProvider.Current.Company.GetOne(companyId);
        }

        public OperatingCompany GetOperatingCompanyById(Guid operatingCompanyID)
        {
            return DataProvider.Current.OperatingCompany.GetOne(operatingCompanyID);
        }

        public List<Company> GetCompanyByUser(Guid? userId)
        {
            return DataProvider.Current.Company.GetByUser(userId);
        }

        public List<OperatingCompany> GetAllOperatingCompanies()
        {
            return DataProvider.Current.OperatingCompany.GetAll();
        }

        public Project GetProjectById(Guid projectId)
        {
            return DataProvider.Current.Project.GetOne(projectId);
        }

        public List<Project> GetProjectByUser(Guid? userId)
        {
            return DataProvider.Current.Project.GetByUser(userId);
        }

        public List<Project> GetProjectByCompany(Guid? companyId)
        {
            return DataProvider.Current.Project.GetByCompany(companyId);
        }

        public List<OperatingCompany> GetOperatingCompanyByCompany(Guid? companyId)
        {
            return DataProvider.Current.OperatingCompany.GetByCompany(companyId);
        }

        private void DeleteUserCompany(User u)
        {
            DataProvider.Current.Company.DeleteByUser(new Guid(u.ProviderUserKey.ToString()));
        }

        public void SaveUserCompany(User u)
        {
            DeleteUserCompany(u);

            if (u.Company != null)
                DataProvider.Current.Company.SaveUserCompany(new Guid(u.ProviderUserKey.ToString()), u.Company.Id);
        }

        private void DeleteUserProjectByUser(User u)
        {
            DataProvider.Current.Project.DeleteUserProjectByUser(new Guid(u.ProviderUserKey.ToString()));
        }

        public void SaveUserProject(User u)
        {
            DeleteUserProjectByUser(u);

            if (u.Projects == null)
                return;

            foreach (Project p in u.Projects)
            {
                DataProvider.Current.Project.SaveUserProject(new Guid(u.ProviderUserKey.ToString()), p.Id);
            }
        }

        public void DeleteProject(Project p)
        {
            DeleteProjectStateByProject(p.Id);
            DataProvider.Current.Project.Remove(p);
        }

        public void DeleteCompany(Company c)
        {
            DataProvider.Current.Company.Remove(c);
        }

        public void DeleteOperatingCompany(OperatingCompany c)
        {
            DataProvider.Current.OperatingCompany.Remove(c);
        }

        public void SaveCompany(Company c)
        {
            DataProvider.Current.Company.Save(c);
        }

        public void SaveOperatingCompany(OperatingCompany c)
        {
            DataProvider.Current.OperatingCompany.Save(c);
        }


        public void SaveProject(Project p)
        {
            DataProvider.Current.Project.Save(p);
        }

        public List<State> GetAllStates()
        {
            return DataProvider.Current.State.GetAllStates();
        }

        public void RemoveProjectFromProjectDuplicateExceptions(Guid selectedId, Guid ProjectId)
        {
            DataProvider.Current.ProjectDuplicateExceptions.ProjectRemoveExceptionProject(selectedId, ProjectId);
        }

        public List<Project> GetAllSelectedProjects(Guid id)
        {
            return DataProvider.Current.ProjectDuplicateExceptions.GetAllSelectedProjects(id);
        }

        public void AddProjectToProjectDuplicateExceptions(Guid projectDuplicateExceptionId, Guid ProjectId, Guid selectedId)
        {
            DataProvider.Current.ProjectDuplicateExceptions.AddProjectAsDuplicateException(projectDuplicateExceptionId, ProjectId, selectedId);
        }

        public List<ConsumerRequestException> GetAllConsumerRequestExceptionsByProjectAndReason(Guid projectId)
        {
            return DataProvider.Current.ConsumerRequestException.GetConsumerRequestExceptionByProjIdAndReason(projectId);
        }

        public List<Project> GetUnselectedProjects(Guid id)
        {
            return DataProvider.Current.ProjectDuplicateExceptions.GetUnselectedProjects(id);
        }

        public List<State> GetStatesByProject(Guid id)
        {
            return DataProvider.Current.State.GetByProject(id);
        }

        public State GetStateById(int id)
        {
            return DataProvider.Current.State.GetById(id);
        }

        private void DeleteProjectStateByProject(Guid id)
        {
            DataProvider.Current.State.DeleteProjectStateByProject(id);
        }

        public void SaveProjectState(Project p)
        {
            DeleteProjectStateByProject(p.Id);

            foreach (State s in p.States)
            {
                DataProvider.Current.State.SaveProjectState(s.Id, p.Id);
            }
        }


        public List<Shipper> GetAllShipers()
        {
            return DataProvider.Current.Shipper.GetAllShippers();
        }

        public Shipper GetShipperById(Guid id)
        {
            return DataProvider.Current.Shipper.GetOne(id);
        }

        public void DeleteShipper(Shipper s)
        {
            DataProvider.Current.Shipper.Remove(s);
        }

        public void SaveShipper(Shipper s)
        {
            DataProvider.Current.Shipper.Save(s);
        }

        public List<Reason> GetAllReasons()
        {
            return DataProvider.Current.Reasons.GetAll();
        }

        public void SaveReasons(Reason r)
        {
            DataProvider.Current.Reasons.Save(r);
        }

        public void AddReasons(Reason r)
        {
            DataProvider.Current.Reasons.Add(r);
        }

        public Reason GetReasonById(Guid id)
        {
            return DataProvider.Current.Reasons.GetOne(id);
        }

        public void DeleteReason(Reason r)
        {
            DataProvider.Current.Reasons.Remove(r);
        }

        public KitItem GetKitItemById(Guid id)
        {
            return DataProvider.Current.KitItem.GetOne(id);
        }

        public List<KitItem> GetKitItemsByKitType(Guid? id)
        {
            return DataProvider.Current.KitItem.GetByKitType(id);
        }

        public List<Item> GetItemsByKitType(Guid? id)
        {
            return DataProvider.Current.KitType.GetItemsByKitTypeId(id);

        }

        public List<KitContent> GetItemsAndQuantitesByKitTypeId(Guid kitTypeId)
        {
            return DataProvider.Current.KitContents.GetItemsAndQuantitesByKitTypeId(kitTypeId);
        }

        public void DeleteKitItem(KitItem k)
        {
            DataProvider.Current.KitItem.Remove(k);
        }

        public void SaveKitItem(KitItem k)
        {
            DataProvider.Current.KitItem.Save(k);
        }

        public void DeleteKitType(Guid kitTypeId)
        {
            DeleteKitTypeKitItemByKitType(kitTypeId);

            DataProvider.Current.KitType.Remove(kitTypeId);
        }

        public void DeleteKitTypeNew(Guid kitTypeId)
        {            
            DataProvider.Current.KitType.Remove(kitTypeId);
        }

        public KitType GetKitTypeById(Guid kitTypeId)
        {
            return DataProvider.Current.KitType.GetOne(kitTypeId);
        }

        private void DeleteKitTypeKitItemByKitType(Guid id)
        {
            DataProvider.Current.KitItem.DeleteKitTypeKitItemByKitType(id);
        }

        private void DeleteKitTypeItemByKitType(Guid id)
        {
            DataProvider.Current.KitContents.DeleteKitTypeItemByKitType(id);
        }


        public void SaveKitTypeKitItems(KitType k, bool delete)
        {
            if (delete)
                DeleteKitTypeKitItemByKitType(k.Id);

            foreach (KitItem ki in k.KitItems)
            {
                DataProvider.Current.KitItem.SaveKitTypeKitItem(ki.Id, k.Id);
            }
        }

        public void SaveKitTypeItems(KitType k, bool delete)
        {
            //if (delete)
            //    DeleteKitTypeItemByKitType(k.Id);

            foreach (Item item in k.Items)
            {
                DataProvider.Current.KitContents.SaveKitTypeItem(item.ItemID, k.Id);
            }
        }

        

        public List<string> GetReasonByID(Guid id)
        {
            return DataProvider.Current.ConsumerRequestException.GetReason(id);
        }

        public Company UserCompany
        {
            get
            {
                if (HttpContext.Current.Session["ComapanyForLogo"] != null)
                    return (Company)HttpContext.Current.Session["ComapanyForLogo"];

                if (HttpContext.Current.User == null)
                    return null;

                if (string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name))
                    return null;

                User u = GetOne(HttpContext.Current.User.Identity.Name);

                List<Company> list = GetCompanyByUser(u.UserID);

                if (list == null)
                    return null;
                if (list.Count == 0)
                    return null;

                HttpContext.Current.Session.Add("ComapanyForLogo", list[0]);

                return list[0];
            }
        }


    }
}
