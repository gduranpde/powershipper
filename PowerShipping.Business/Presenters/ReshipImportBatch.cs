﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.Business.Presenters
{
    public class ReshipImportBatch
    {
        public List<PowerShipping.Entities.ReshipImportBatch> GetReshipImportBatches(String sBatchName, String sImportedBy, Guid? sShipperID, DateTime? FromDate, DateTime? ToDate)
        {
            return DataProvider.Current.ReshipImportBatch.GetManyFiltered(sBatchName, sImportedBy, sShipperID, FromDate, ToDate);
        }
    }
}