﻿using System;
using System.Collections.Generic;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.Business.Presenters
{
    public class ReportPresenter
    {
        public List<Report> GetAll()
        {
            return DataProvider.Current.Report.GetAll();
        }


    }
}
