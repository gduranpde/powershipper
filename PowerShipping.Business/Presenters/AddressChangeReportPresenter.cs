﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using PowerShipping.Data;

namespace PowerShipping.Business.Presenters
{
    public class AddressChangeReportPresenter
    {
        public DataSet GetAll(DateTime? start, DateTime? end, string accNum, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return DataProvider.Current.ConsumerRequest.GetAddressChange(start, end, accNum, userId, projectId, companyId);
        }
    }
}
