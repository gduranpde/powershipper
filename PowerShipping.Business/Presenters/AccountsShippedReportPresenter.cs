﻿using System;
using System.Data;
using PowerShipping.Data;

namespace PowerShipping.Business.Presenters
{
    public class AccountsShippedReportPresenter
    {
        public DataSet GetAll(DateTime? start, DateTime? end, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return DataProvider.Current.ConsumerRequest.GetAccountsShipped(start, end, userId, projectId, companyId);
        }
    }
}