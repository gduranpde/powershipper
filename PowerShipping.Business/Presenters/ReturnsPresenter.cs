﻿using System;
using System.Collections.Generic;
using System.Data;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.Business.Presenters
{
    public class ReturnsPresenter
    {
        //public List<ShipmentDetail> GetAll(int? status, DateTime? start, DateTime? end)
        //{
        //    int? shippingStatus = (int?)ShipmentShippingStatus.Returned;
        //    return DataProvider.Current.ShipmentDetail.GetManyFiltered(status, start, end, shippingStatus);
        //}

        public List<ShipmentDetail> GetAll(int? status, string shipNum, string accNum, string fedNum, Guid? userId, Guid? projectId, Guid? companyId, string accountName, string PONumber, Guid? kitTypeID, Guid? shipperID, string OpCo, Guid? reasonID, DateTime? returnStartDate, DateTime? returnEndDate)
        {
            int? shippingStatus = (int?)ShipmentShippingStatus.Returned;
            return DataProvider.Current.ShipmentDetail.GetManyFiltered(status, shipNum, accNum, fedNum, shippingStatus, userId, projectId, companyId, accountName, PONumber, kitTypeID, shipperID, OpCo, reasonID, returnStartDate, returnEndDate);
        }

        public ShipmentDetail GetById(Guid id)
        {
            return DataProvider.Current.ShipmentDetail.GetOne(id);
        }

        public void Save(ShipmentDetail sd, ConsumerRequest cr)
        {
            DataProvider.Current.ShipmentDetail.Save(sd);
            DataProvider.Current.ConsumerRequest.Save(cr);
        }

        public void Save(ShipmentDetail sd)
        {
            DataProvider.Current.ShipmentDetail.Save(sd);
        }

        public DataTable GetKitType_ProjectByShipmentNumber(int shipmentNumber)
        {
            return DataProvider.Current.ReshipImportBatch.GetKitType_ProjectByShipmentNumber(shipmentNumber);
        }
    }
}