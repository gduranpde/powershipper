﻿using System;
using System.Collections.Generic;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.Business.Presenters
{
    public class RecieveReturnsPresenter
    {
        public List<ShipmentDetail> GetByTrackingNumber(string shipmentNum, string trackingNumber, string accNum, string accName, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return DataProvider.Current.ShipmentDetail.GetByTrackingNumber(shipmentNum, trackingNumber, accNum, accName, userId, projectId, companyId);
        }

        public void Save(ShipmentDetail sd)
        {
            DataProvider.Current.ShipmentDetail.Save(sd);
        }

        public ShipmentDetail GetById(Guid id)
        {
            return DataProvider.Current.ShipmentDetail.GetOne(id);
        }

        public List<Reason> GetReasonsAll()
        {
            return DataProvider.Current.Reasons.GetAll();
        }
    }
}
