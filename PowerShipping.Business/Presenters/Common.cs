﻿using PowerShipping.Entities;
using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace PowerShipping.Business.Presenters
{
    public class Common
    {
        public static string GetHelp(Type type, string severPath)
        {
            System.IO.StreamReader sw;
            string help = string.Empty;
            if (type == typeof(ConsumerRequest))
            {
                sw = new System.IO.StreamReader(string.Format("{0}/Help/{1}", severPath, ConfigurationManager.AppSettings.Get("Help_ConsumerRequests")));
                help = sw.ReadToEnd();
                sw.Close();
            }

            if (type == typeof(OptOut))
            {
                sw = new System.IO.StreamReader(string.Format("{0}/Help/{1}", severPath, ConfigurationManager.AppSettings.Get("Help_OptOuts")));
                return sw.ReadToEnd();
                sw.Close();
            }
               

            if (type == typeof(ShipmentDetail))
            {
                sw = new System.IO.StreamReader(string.Format("{0}/Help/{1}", severPath, ConfigurationManager.AppSettings.Get("Help_FedExTracking")));
                help = sw.ReadToEnd();
                sw.Close();
            }
            return help;
        }

        public static string GetHelp(string page, string severPath)
        {
            string filename = "";
            if (page.Split('^').Length > 1)
                filename = page.Split('^')[0];
            else
                filename = page;

            System.IO.StreamReader sw;
            string help = string.Empty;

            sw = new System.IO.StreamReader(string.Format("{0}/Help/{1}", severPath, filename + ".txt"));
                help = sw.ReadToEnd();
                sw.Close();
            
            return help;
        }

        public static string GetTitle(string page)
        {
            if (page.Split('^').Length > 1)
            {
                
                return page.Split('^')[1].ToUpper();
            }

            return "";
        }

        public static string MakeHelpLink(Page page, string text, string title)
        {
            int start = page.AppRelativeVirtualPath.LastIndexOf('/');
            string pageFile = page.AppRelativeVirtualPath.Substring(start + 1);
            pageFile = pageFile.Replace(".aspx", "");
            return string.Format("<a href='../Help.aspx?type={0}^{1}' target='_blank'\">" + text + "</a>", pageFile, title);
        }

        public static bool IsCanEdit()
        {
            if (HttpContext.Current.Session["IsCanEdit"] == null)
            {
                UserManagementPresenter userManagementPresenter = new UserManagementPresenter();
                User u = userManagementPresenter.GetOne(HttpContext.Current.User.Identity.Name);

                if (u.Roles.Contains(PDMRoles.ROLE_Customer))
                    HttpContext.Current.Session.Add("IsCanEdit", false);
                else
                    HttpContext.Current.Session.Add("IsCanEdit", true);
            }

            return (bool)HttpContext.Current.Session["IsCanEdit"];
        }
    }

    public enum Errors
    {
        HaventRights = 0
    }

    public enum KwType
    {
        Kw,
        Kwh
    }
}
