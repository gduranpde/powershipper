﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_Shipper_GetById : StoredProcedure
    {
        [InParameter] public Guid Id;
    }
}