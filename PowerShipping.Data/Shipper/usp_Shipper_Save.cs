﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_Shipper_Save : StoredProcedure
    {
        [InParameter] public string BatchTrackingCodeModule;
        [InParameter] public Guid Id;
        [InParameter] public string PackageTrackingUrl;
        [InParameter] public string ShipperCode;
        [InParameter] public string ShipperName;
        [InParameter] public string TransactionFileUrl;
    }
}