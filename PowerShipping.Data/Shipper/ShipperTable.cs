﻿using System.Collections.Generic;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
    [spAdd(typeof (usp_Shipper_Save))]
    [spSave(typeof (usp_Shipper_Save))]
    [spGetOne(typeof (usp_Shipper_GetById))]
    [spRemove(typeof (usp_Shipper_Delete))]
    public class ShipperTable : Table<Shipper>
    {
        public List<Shipper> GetAllShippers()
        {
            return db.Execute<Shipper>(new usp_Shipper_List {});
        }
    }
}