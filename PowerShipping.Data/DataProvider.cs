﻿using System;

namespace PowerShipping.Data
{
	public class DataProvider
	{
		[ThreadStatic]
		static PowerShippingDb _Current;

		public static PowerShippingDb Current
		{
			get { return _Current ?? (_Current = new PowerShippingDb()); }
		}
	}
}
