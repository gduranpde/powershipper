﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_Company_GetByUser : StoredProcedure
    {
        [InParameter] public Guid? UserId;
    }
}