﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_Company_Save : StoredProcedure
    {
        [InParameter]
        public Guid Id;
        [InParameter]
        public string CompanyName;
        [InParameter]
        public string CompanyCode;
        [InParameter]
        public string CompanyLogo;
        [InParameter] 
        public bool IsActive;
    }
}