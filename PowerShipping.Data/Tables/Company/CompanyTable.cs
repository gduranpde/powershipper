﻿using System;
using System.Collections.Generic;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
    [spAdd(typeof (usp_Company_Save))]
    [spSave(typeof (usp_Company_Save))]
    [spGetOne(typeof (usp_Company_GetById))]
    [spRemove(typeof (usp_Company_Delete))]
    public class CompanyTable : Table<Company>
    {
        public List<Company> GetByUser(Guid? userId)
        {
            return db.Execute<Company>(new usp_Company_GetByUser { UserId = userId });
        }

        public void SaveUserCompany(Guid userId, Guid? companyId)
        {
            db.Execute(new usp_UserCompany_Save { UserId = userId, CompanyId = companyId });
        }

        public void DeleteByUser(Guid userId)
        {
            db.Execute(new usp_UserCompany_DeleteByUser { UserId = userId });
        }
    }
}