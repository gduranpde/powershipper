﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_UserCompany_DeleteByUser : StoredProcedure
    {
        [InParameter] public Guid UserId;
    }
}