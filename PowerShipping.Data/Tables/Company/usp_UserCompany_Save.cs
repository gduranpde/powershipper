﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_UserCompany_Save : StoredProcedure
    {
        //[InParameter]
        //public Guid UserCompanyId;
        [InParameter]
        public Guid UserId;
        [InParameter]
        public Guid? CompanyId;
    }
}
