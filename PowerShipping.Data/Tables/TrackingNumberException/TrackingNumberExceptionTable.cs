﻿using System;
using System.Collections.Generic;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
	[spAdd(typeof(usp_TrackingNumberException_Save))]
	[spSave(typeof(usp_TrackingNumberException_Save))]
	[spGetOne(typeof(usp_TrackingNumberException_GetById))]

	public class TrackingNumberExceptionTable : Table<TrackingNumberException>
	{
        public List<TrackingNumberException> GetAll(bool showDeleted, string accNum, string trNum, string poNum, Guid? userId, Guid? projectId, Guid? companyId)
		{
			return db.Execute<TrackingNumberException>(new usp_TrackingNumberException_GetAll {ShowDeleted = showDeleted, TrackingNumber = trNum, 
                AccountNumber = accNum, PurchaseOrderNumber = poNum, UserId = userId, ProjectId = projectId, CompanyId = companyId});
		}
	}
}
