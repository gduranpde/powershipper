using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_TrackingNumberException_GetById : StoredProcedure
	{
		[InParameter] public Guid Id;
	}
}