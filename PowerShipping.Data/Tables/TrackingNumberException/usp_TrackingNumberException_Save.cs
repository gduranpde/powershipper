using System;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
	class usp_TrackingNumberException_Save : StoredProcedure
	{
		[InParameter] public Guid Id;

		[InParameter] public string TrackingNumber;
		[InParameter] public string AccountNumber;
		[InParameter] public string PurchaseOrderNumber;
		[InParameter] public string AccountName;
		[InParameter] public string Address1;
		[InParameter] public string Address2;
		[InParameter] public string City;
		[InParameter] public string State;
		[InParameter] public string ZipCode;
		[InParameter] public string Reference;

		[InParameter] public Guid? BatchId;
		[InParameter] public string Reason;
		[InParameter] public TrackingNumberExceptionStatus Status;
        [InParameter]
        public Guid? ProjectId;
        [InParameter]
        public Guid? ShipperId;
	}
}