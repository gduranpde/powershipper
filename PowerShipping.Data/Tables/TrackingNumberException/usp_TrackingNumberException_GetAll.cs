using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_TrackingNumberException_GetAll : StoredProcedure
	{
        [InParameter]
        public string TrackingNumber;
        [InParameter]
        public string AccountNumber;
        [InParameter]
        public string PurchaseOrderNumber;

		[InParameter] public bool ShowDeleted;

        [InParameter]
        public Guid? UserId;

        [InParameter]
        public Guid? ProjectId;

        [InParameter]
        public Guid? CompanyId;
	}
}