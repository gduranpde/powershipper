using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_OptOut_GetMany : StoredProcedure
	{
        [InParameter]
        public DateTime? StartDate;

        [InParameter]
        public DateTime? EndDate;

        [InParameter]
        public string AccountNumber;
        [InParameter]
        public string AccountName;
        [InParameter]
        public string Address;
        [InParameter]
        public string Phone;

        [InParameter]
        public Guid? UserId;

        [InParameter]
        public Guid? ProjectId;

        [InParameter]
        public Guid? CompanyId;
	}
}