using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_OptOut_GetByAccountNumber : StoredProcedure
	{
		[InParameter] public string AccountNumber;
        [InParameter]
        public Guid ProjectId;
	}
}