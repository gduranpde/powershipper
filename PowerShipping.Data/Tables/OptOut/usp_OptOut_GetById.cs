using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_OptOut_GetById : StoredProcedure
	{
		[InParameter] public Guid Id;
	}
}