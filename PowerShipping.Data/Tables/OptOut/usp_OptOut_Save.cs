using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_OptOut_Save : StoredProcedure
	{
		[InParameter] public Guid Id;

		[InParameter] public DateTime OptOutDate;
		[InParameter] public string AccountNumber;
		[InParameter] public string AccountName;
		[InParameter] public string Address;
		[InParameter] public string City;
		[InParameter] public string State;
		[InParameter] public string ZipCode;
		[InParameter] public string Email;
		[InParameter] public string Phone1;
		[InParameter] public string Phone2;

		[InParameter] public bool IsEnergyProgram;
		[InParameter] public bool IsTotalDesignation;

		[InParameter] public Guid? BatchId;
		[InParameter] public string Notes;
        [InParameter]
        public Guid ProjectId;
	}
}