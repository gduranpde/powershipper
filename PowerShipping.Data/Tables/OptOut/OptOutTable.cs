﻿using System;
using System.Collections.Generic;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
	[spAdd(typeof(usp_OptOut_Save))]
	[spSave(typeof(usp_OptOut_Save))]
	[spGetOne(typeof(usp_OptOut_GetById))]
    [spRemove(typeof(usp_OptOut_Remove))]

	public class OptOutTable : Table<OptOut>
	{
		public List<OptOut> GetMany()
		{
			return db.Execute<OptOut>(new usp_OptOut_GetMany());
		}

		public OptOut GetByAccountNumber(string accountNumber, Guid projectId)
		{
			return db.ExecuteOne<OptOut>(new usp_OptOut_GetByAccountNumber {AccountNumber = accountNumber, ProjectId = projectId});
		}

        public List<OptOut> GetManyFiltered(DateTime? start, DateTime? end, string accNum, string accName, string phone, string address, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return db.Execute<OptOut>(new usp_OptOut_GetMany() { StartDate = start, EndDate = end, AccountName = accName, AccountNumber = accNum, Address = address, Phone = phone, UserId = userId, ProjectId = projectId, CompanyId = companyId});
        }
	}
}
