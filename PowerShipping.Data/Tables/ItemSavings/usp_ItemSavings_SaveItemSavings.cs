﻿using Dims.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Data.Tables.ItemSavings
{
    class usp_ItemSavings_SaveItemSavings : StoredProcedure
    {
        [InParameter]
        public Guid OperatingCompanyID;
        [InParameter]
        public Guid ProjectID;
        [InParameter]
        public Guid ItemID;
        [InParameter]
        public string KwType;
        [InParameter]
        public double Value;
        [InParameter]
        public string CreatedBy;
        [InParameter]
        public string LastModifiedBy;
        [InParameter]
        public string CatalogID;
    }
}
