﻿using Dims.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Data.Tables.ItemSavings
{
    class usp_ItemCatalog_SaveItemCatalog : StoredProcedure
    {
        [InParameter]
        public Guid ProjectID;
        [InParameter]
        public Guid ItemID;       
        [InParameter]
        public string CatalogID;
    }
}
