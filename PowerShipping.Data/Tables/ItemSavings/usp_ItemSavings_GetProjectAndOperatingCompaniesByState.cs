﻿using Dims.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Data.Tables.ItemSavings
{
    internal class usp_ItemSavings_GetProjectAndOperatingCompaniesByState: StoredProcedure
    {
        [InParameter]
        public int StateId;
        [InParameter]
        public Guid ItemId;
        [InParameter]
        public bool OnlyActiveProjects;
    }
}
