﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PowerShipping.Entities;
using Dims.DAL;
using PowerShipping.Data.Tables.ItemSavings;

namespace PowerShipping.Data
{

    //[spAdd(typeof(usp_OperatingCompany_Save))]
    //[spSave(typeof(usp_OperatingCompany_Save))]
    //[spGetOne(typeof(usp_OperatingCompany_GetById))]
    //[spRemove(typeof(usp_OperatingCompany_Delete))]
    //[spGetAll(typeof(usp_OperatingCompany_GetAll))]

    public class ItemSavingsTable: Table<ItemSavings>
    {
        public List<ItemSavings> GetProjectAndOperatingCompaniesByState(int stateID, Guid itemId, bool onlyActiveProjects)
        {
            return db.Execute<ItemSavings>(new usp_ItemSavings_GetProjectAndOperatingCompaniesByState { StateId = stateID, ItemId = itemId, OnlyActiveProjects = onlyActiveProjects });
        }

        public void UpdateItemSavings(Guid itemID, string projectID, string operatingCompanyID, string kwType, double value, string identity, string catalogID)
        {
            db.Execute(new usp_ItemSavings_SaveItemSavings 
            { 
                ItemID = itemID, 
                ProjectID = new Guid(projectID), 
                OperatingCompanyID = new Guid(operatingCompanyID),
                KwType = kwType,
                Value = value,
                CreatedBy = identity,
                LastModifiedBy = identity,
                CatalogID = catalogID
            });
        }

        public void UpdateItemCatalog(Guid itemID, string projectID, string catalogID)
        {
            db.Execute(new usp_ItemCatalog_SaveItemCatalog 
            { 
                ItemID = itemID, 
                ProjectID = new Guid(projectID),                 
                CatalogID = catalogID
            });
        }
    }
}
