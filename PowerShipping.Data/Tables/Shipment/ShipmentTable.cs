﻿using System;
using System.Collections.Generic;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
    [spAdd(typeof(usp_Shipment_Save))]
	[spSave(typeof(usp_Shipment_Save))]
	[spRemove(typeof(usp_Shipment_Delete))]
	[spGetOne(typeof(usp_Shipment_GetById))]
	
	public class ShipmentTable : Table<Shipment>
	{
        public List<Shipment> GetManyFiltered(DateTime? start, DateTime? end, string poNum, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return db.Execute<Shipment>(new usp_Shipment_GetMany() { StartDate = start, EndDate = end, PONumber = poNum, UserId = userId, ProjectId = projectId, CompanyId = companyId});
        }
	}
}
