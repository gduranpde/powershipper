using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_Shipment_Save : StoredProcedure
	{
		[InParameter] public Guid Id;
		[InParameter] public Guid KitTypeId;

		[InParameter] public string PurchaseOrderNumber;

		[InParameter] public DateTime DateLabelsNeeded;
		[InParameter] public DateTime DateCustomerWillShip;
	
		[InParameter] public string CreatedBy;
        [InParameter]
        public Guid ProjectId;
        [InParameter]
        public Guid ShipperId;
	}
}