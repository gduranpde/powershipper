using System;
using Dims.DAL;


namespace  PowerShipping.Data
{
    
    internal class usp_Reason_Add:StoredProcedure
    {
          
        [InParameter]
        public Guid ReasonID;
      

        [InParameter]
        public string ReasonCode;
      

        [InParameter]
        public string ReasonDescription;
      

    }
}