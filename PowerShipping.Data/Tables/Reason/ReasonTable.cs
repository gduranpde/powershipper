using Dims.DAL;
using PowerShipping.Data;
using PowerShipping.Entities;


namespace  PowerShippingConnectionString.Data
{

    //[spGetMany(typeof(usp_Reason_GetMany))]
    [spGetAll(typeof(usp_Reason_GetAll))]
    [spGetOne(typeof(usp_Reason_GetOne))]
    [spAdd(typeof(usp_Reason_Add))]
    [spSave(typeof(usp_Reason_Save))]
    [spRemove(typeof(usp_Reason_Remove))]
    public class ReasonTable:Table<Reason>
    {
    }
}
