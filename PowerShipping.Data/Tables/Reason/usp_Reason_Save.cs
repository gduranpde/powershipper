using System;
using System.Text;
using System.Collections.Generic;

using Dims.DAL;


namespace  PowerShipping.Data
{
    
    internal class usp_Reason_Save:StoredProcedure
    {
          
        [InParameter]
        public Guid ReasonID;
      

        [InParameter]
        public string ReasonCode;
      

        [InParameter]
        public string ReasonDescription;
      

    }
}