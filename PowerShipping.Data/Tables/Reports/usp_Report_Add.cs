using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_Report_Add : StoredProcedure
	{
        [InParameter]
        public string ReportName;
        [InParameter]
        public int Status;
        [InParameter]
        public DateTime StartedOn;
        [InParameter]
        public DateTime? CompletedOn;
        [InParameter]
        public string Params;
        [InParameter]
        public string FilePath;
        [InParameter]
        public Guid? UserID;
	}
}