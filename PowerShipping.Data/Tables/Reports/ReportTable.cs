﻿using System;
using System.Collections.Generic;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
    public class ReportTable : Table<Report>
    {
        public List<Report> GetAll()
        {
            return db.Execute<Report>(new usp_Report_GetAll
            {
            });
        }


        public int AddReport(string reportName, string param, Guid? userID)
        {
            return db.ExecuteScalar<int>(new usp_Report_Add { ReportName = reportName, Status = 0, StartedOn = DateTime.Now, CompletedOn = null, Params = param, FilePath = null, UserID = userID });
        }

        public void UpdateReport(int IDCompleted, int status, string filePath)
        {
            db.Execute(new usp_Report_Update { ID = IDCompleted, Status = status, CompletedOn = DateTime.Now, FilePath = filePath });
        }

        public List<ReconciliationReport> ReconciliationReport_GetDelivery(int stateID, DateTime ds, DateTime de)
        {
            
            return db.Execute < ReconciliationReport>(new usp_ReconciliationReport_GetDelivery { StateID = stateID, DateStart = ds, DateEnd = de, CommandTimeout = 300 });
        }

        public List<ReconciliationReport> ReconciliationReport_GetExceptions(int stateID, DateTime ds, DateTime de)
        {
            return db.Execute<ReconciliationReport>(new usp_ReconciliationReport_GetException { StateID = stateID, DateStart = ds, DateEnd = de, CommandTimeout = 300 });
        }

        public List<ReconciliationReport> ReconciliationReport_GetExceptionSummary(int stateID, DateTime ds, DateTime de)
        {
            return db.Execute<ReconciliationReport>(new usp_ReconciliationReport_GetExceptionSummary { StateID = stateID, DateStart = ds, DateEnd = de, CommandTimeout = 300 });
        }
    }
}