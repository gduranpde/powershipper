using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_Report_Update : StoredProcedure
	{
       
        [InParameter]
        public int ID;
        [InParameter]
        public int Status;
        [InParameter]
        public DateTime? CompletedOn;
        [InParameter]
        public string FilePath;
        
	}
}