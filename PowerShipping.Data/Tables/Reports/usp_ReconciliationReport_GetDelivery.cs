using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    class usp_ReconciliationReport_GetDelivery : StoredProcedure
    {
        [InParameter]
        public int StateID;
        [InParameter]
        public DateTime DateStart;
        [InParameter]
        public DateTime DateEnd;
    }
}