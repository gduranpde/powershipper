﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_ProjectState_GetByProject : StoredProcedure
    {
        [InParameter] public Guid ProjectId;
    }
}