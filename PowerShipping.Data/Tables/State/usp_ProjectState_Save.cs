﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_ProjectState_Save : StoredProcedure
    {
        [InParameter] public Guid ProjectId;
        [InParameter] public int StateId;
    }
}