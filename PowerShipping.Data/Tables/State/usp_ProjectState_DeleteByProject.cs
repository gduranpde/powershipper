﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_ProjectState_DeleteByProject : StoredProcedure
    {
        [InParameter] public Guid ProjectId;
    }
}