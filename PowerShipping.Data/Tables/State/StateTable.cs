﻿using System;
using System.Collections.Generic;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
    [spGetOne(typeof(usp_State_GetById))]
    public class StateTable : Table<State>
    {
        public void SaveProjectState(int stateId, Guid projectId)
        {
            db.Execute(new usp_ProjectState_Save { StateId = stateId, ProjectId = projectId });
        }

        public void DeleteProjectStateByProject(Guid projectId)
        {
            db.Execute(new usp_ProjectState_DeleteByProject { ProjectId = projectId });
        }

        public List<State> GetByProject(Guid projectId)
        {
            return db.Execute<State>(new usp_ProjectState_GetByProject { ProjectId = projectId });
        }

        public List<State> GetAllStates()
        {
            return db.Execute<State>(new usp_State_List{});
        }

        public State GetById(int id)
        {
            return db.ExecuteOne<State>(new usp_State_GetById {Id = id});
        }
    }
}