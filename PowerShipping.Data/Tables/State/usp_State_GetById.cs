﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_State_GetById : StoredProcedure
    {
        [InParameter] public int Id;
    }
}