﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_ConsumerRequest_GetCustom : StoredProcedure
	{
        [InParameter]
        public string CustomQuery;
	}
}