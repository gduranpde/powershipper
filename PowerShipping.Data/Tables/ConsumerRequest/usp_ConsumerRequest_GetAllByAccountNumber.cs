using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_ConsumerRequest_GetAllByAccountNumber : StoredProcedure
	{
		[InParameter] 
        public String AccountNumber;
	}
}