﻿using System;
using System.Collections.Generic;
using System.Data;
using Dims.DAL;
using PowerShipping.Entities;
using PowerShipping.Data.Tables.ConsumerRequest;

namespace PowerShipping.Data
{
    [spAdd(typeof(usp_ConsumerRequest_Save))]
    [spSave(typeof(usp_ConsumerRequest_Save))]
    [spGetOne(typeof(usp_ConsumerRequest_GetById))]
    [spRemove(typeof(usp_ConsumerRequest_Delete))]

    public class ConsumerRequestTable : Table<ConsumerRequest>
    {
        public ConsumerRequest GetByAccountNumber(string accountNumber, Guid projectId)
        {
            return db.ExecuteOne<ConsumerRequest>(new usp_ConsumerRequest_GetByAccountNumber { AccountNumber = accountNumber, ProjectId = projectId });
        }

        public List<ConsumerRequest> GetAllByAccountNumber(string accountNumber)
        {
            return db.Execute<ConsumerRequest>(new usp_ConsumerRequest_GetAllByAccountNumber { AccountNumber = accountNumber });
        }

        public List<ConsumerRequest> GetAll(bool showDeleted)
        {
            return db.Execute<ConsumerRequest>(new usp_ConsumerRequest_GetAll { ShowDeleted = showDeleted });
        }

        public List<ConsumerRequest> GetMany()
        {
            return db.Execute<ConsumerRequest>(new usp_ConsumerRequest_GetMany());
        }

        public List<ConsumerRequest> GetManyFiltered(bool showDeleted, string accNum, string accName, string phone, string address)
        {
            return db.Execute<ConsumerRequest>(new usp_ConsumerRequest_GetAll { ShowDeleted = showDeleted, AccountNumber = accNum, AccountName = accName, Phone = phone, Address = address });
        }

        public List<ConsumerRequest> GetManyFiltered(bool showDeleted, string accNum, string accName, string phone, string address, int? status, string orderClause, Guid? projectId, string filerExpr)
        {
            return db.Execute<ConsumerRequest>(new usp_ConsumerRequest_GetAll
            {
                ShowDeleted = showDeleted,
                AccountNumber = accNum,
                AccountName = accName,
                Phone = phone,
                Address = address,
                Status = status,
                OrderClause = orderClause,
                ProjectId = projectId,
                FilterExpr = filerExpr
            });
        }

        public List<ConsumerRequest> GetByBatch(Guid batchId)
        {
            //return db.Execute<ConsumerRequest>(new usp_ConsumerRequest_GetAll { ShowDeleted = false, BatchId = batchId, CommandTimeout = 300});
            return db.Execute<ConsumerRequest>(new usp_ConsumerRequest_GetByBatchId() { BatchId = batchId, CommandTimeout = 300 });
        }

        public List<ConsumerRequest> GetCustom(string customQuery)
        {
            //return db.Execute<ConsumerRequest>(new usp_ConsumerRequest_GetCustom { CustomQuery = "select 1" });
            return db.Execute<ConsumerRequest>(new usp_ConsumerRequest_GetCustom { CustomQuery = customQuery });
        }

        public void ClearAll()
        {
            // !!! temporary !!!
            // Cleanup database
            db.Execute(new usp_ClearAll());
        }

        public DataSet GetAuditReportAccounts(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return db.ExecuteDataSet(new usp_AuditReport_Accounts
            {
                PONumber = poNum,
                StartDate = start,
                EndDate = end,
                KitType = kitType,
                UserId = userId,
                ProjectId = projectId,
                CompanyId = companyId,
                CommandTimeout = 300
            });
        }
        public DataSet GetAuditReportExceptions(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return db.ExecuteDataSet(new usp_AuditReport_Exceptions
            {
                PONumber = poNum,
                StartDate = start,
                EndDate = end,
                KitType = kitType,
                UserId = userId,
                ProjectId = projectId,
                CompanyId = companyId,
                CommandTimeout = 300
            });
        }
        //PG31
        public DataSet GetEquipmentAttributes(Guid ConsumerRequestID)
        {
            return db.ExecuteDataSet(new usp_AEG_XML_EquipmentAttributes
            {
                ConsumerRequestId = ConsumerRequestID,
                CommandTimeout = 300
            });
        }
        public DataSet GetAddressChange(DateTime? start, DateTime? end, string accNum, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return db.ExecuteDataSet(new usp_ConsumerRequestChange_GetAll { AccountNumber = accNum, StartDate = start, EndDate = end, UserId = userId, ProjectId = projectId, CompanyId = companyId });
        }
        public DataSet GetAEGReport(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return db.ExecuteDataSet(new usp_AEG_Delivery_Report
            {
                PONumber = poNum,
                StartDate = start,
                EndDate = end,
                KitType = kitType,
                UserId = userId,
                ProjectId = projectId,
                CompanyId = companyId,
                CommandTimeout = 1000
            });
        }
        public DataSet GetAEGReportPhase2(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return db.ExecuteDataSet(new usp_AEG_Delivery_ReportPhase2
            {
                PONumber = poNum,
                StartDate = start,
                EndDate = end,
                KitType = kitType,
                UserId = userId,
                ProjectId = projectId,
                CompanyId = companyId,
                CommandTimeout = 1000
            });
        }
        public DataSet GetAEGXMLReport(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return db.ExecuteDataSet(new usp_AEG_XML_Delivery_Report
            {
                PONumber = poNum,
                StartDate = start,
                EndDate = end,
                KitType = kitType,
                UserId = userId,
                ProjectId = projectId,
                CompanyId = companyId,
                CommandTimeout = 300
            });
        }
        public DataSet GetAEGReportExceptions(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return db.ExecuteDataSet(new usp_AEG_Exception_Report
            {
                PONumber = poNum,
                StartDate = start,
                EndDate = end,
                KitType = kitType,
                UserId = userId,
                ProjectId = projectId,
                CompanyId = companyId,
                CommandTimeout = 300
            });
        }
        public DataSet GetAEGReportExceptionsPhase2(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return db.ExecuteDataSet(new usp_AEG_Exception_ReportPhase2
            {
                PONumber = poNum,
                StartDate = start,
                EndDate = end,
                KitType = kitType,
                UserId = userId,
                ProjectId = projectId,
                CompanyId = companyId,
                CommandTimeout = 300
            });
        }
        public DataSet GetAEGReportExceptions(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId, DateTime? ReceptionStartDate, DateTime? ReceptionEndDate)
        {
            return db.ExecuteDataSet(new usp_AEG_Exception_Report
            {
                PONumber = poNum,
                StartDate = start,
                EndDate = end,
                KitType = kitType,
                UserId = userId,
                ProjectId = projectId,
                CompanyId = companyId,
                ReceptionStartdate = ReceptionStartDate,
                ReceptionEnddate = ReceptionEndDate,
                CommandTimeout = 300
            });
        }
        public DataSet GetAEGReportExceptionsPhase2(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId, DateTime? ReceptionStartDate, DateTime? ReceptionEndDate)
        {
            return db.ExecuteDataSet(new usp_AEG_Exception_ReportPhase2
            {
                PONumber = poNum,
                StartDate = start,
                EndDate = end,
                KitType = kitType,
                UserId = userId,
                ProjectId = projectId,
                CompanyId = companyId,
                ReceptionStartdate = ReceptionStartDate,
                ReceptionEnddate = ReceptionEndDate,
                CommandTimeout = 300
            });
        }
        public DataSet GetAEGXMLReportExceptions(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId, DateTime? ReceptionStartDate, DateTime? ReceptionEndDate)
        {
            return db.ExecuteDataSet(new usp_AEG_XML_Exception_Report
            {
                PONumber = poNum,
                StartDate = start,
                EndDate = end,
                KitType = kitType,
                UserId = userId,
                ProjectId = projectId,
                CompanyId = companyId,
                ReceptionStartdate = ReceptionStartDate,
                ReceptionEnddate = ReceptionEndDate,
                CommandTimeout = 300
            });
        }
        public DataSet GetAccountsShipped(DateTime? start, DateTime? end, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return db.ExecuteDataSet(new usp_Accounts_Shipped_Report
            {
                StartDate = start,
                EndDate = end,
                UserId = userId,
                ProjectId = projectId,
                CompanyId = companyId,
                CommandTimeout = 300
            });
        }
        public List<string> GetAccountNumberById(Guid projectId)
        {
            return db.Execute<string>(new usp_ConsumerRequest_GetAccountNumberById { ProjectId = projectId });
        }
        public int CheckExistsAccountNumberInProject(Guid projectId, string accountNumber)
        {
            return db.ExecuteScalar<int>(new usp_ConsumerRequest_CheckAccountNumberExistsInProject { ProjectId = projectId, AccountNumber = accountNumber });
        }
        public int CheckExistsAccountNumberInAllProjects(string accountNumber)
        {
            return db.ExecuteScalar<int>(new usp_ConsumerRequest_CheckAccountNumberExistsInAllProjects { AccountNumber = accountNumber });
        }
        public void InsertConsumerRequest(ConsumerRequest consumer)
        {
            db.Execute(new usp_ConsumerRequest_Insert
            {
                Id = consumer.Id,
                AccountNumber = consumer.AccountNumber,
                AccountName = consumer.AccountName,
                Address1 = consumer.Address1,
                Address2 = consumer.Address2,
                City = consumer.City,
                State = consumer.State,
                ZipCode = consumer.ZipCode,
                Email = consumer.Email,
                Phone1 = consumer.Phone1,
                Phone2 = consumer.Phone2,
                Method = consumer.Method,
                AnalysisDate = consumer.AnalysisDate,
                IsOkayToContact = consumer.IsOkayToContact,
                OperatingCompany = consumer.OperatingCompany,
                WaterHeaterFuel = consumer.WaterHeaterFuel,
                HeaterFuel = consumer.HeaterFuel,
                FacilityType = consumer.FacilityType,
                RateCode = consumer.RateCode,
                IncomeQualified = consumer.IncomeQualified,
                IsReship = consumer.IsReship,
                DoNotShip = consumer.DoNotShip,
                ReceiptDate = consumer.ReceiptDate,
                AuditFailureDate = consumer.AuditFailureDate,
                BatchId = consumer.BatchId,
                Notes = consumer.Notes,
                Status = consumer.Status,
                OutOfState = consumer.OutOfState,
                ProjectId = consumer.ProjectId,
                Quantity = consumer.Quantity,
                PremiseID = consumer.PremiseID,
                CompanyName = consumer.CompanyName,
                ServiceAddress1 = consumer.ServiceAddress1,
                ServiceAddress2 = consumer.ServiceAddress2,
                ServiceCity = consumer.ServiceCity,
                ServiceState = consumer.ServiceState,
                ServiceZip = consumer.ServiceZip,
                CRImportedDate = consumer.CRImportedDate,
                AccountNumberOld = consumer.AccountNumberOld,
                RequestedKit = consumer.RequestedKit
            });
        }
        
        public DataSet GetAEGDeliveryReportV3(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return db.ExecuteDataSet(new usp_AEG_Delivery_ReportV3
            {
                PONumber = poNum,
                StartDate = start,
                EndDate = end,
                KitType = kitType,
                UserId = userId,
                ProjectId = projectId,
                CompanyId = companyId,
                CommandTimeout = 1000
            });
        }

        public DataSet GetAEGXMLDeliveryReportV3(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return db.ExecuteDataSet(new usp_AEG_XML_Delivery_ReportV3
            {
                PONumber = poNum,
                StartDate = start,
                EndDate = end,
                KitType = kitType,
                UserId = userId,
                ProjectId = projectId,
                CompanyId = companyId,
                CommandTimeout = 1000
            });
        }

        public DataSet GetEquipmentAttributesV3(Guid ConsumerRequestID)
        {
            return db.ExecuteDataSet(new usp_AEG_XML_EquipmentAttributesV3
            {
                ConsumerRequestId = ConsumerRequestID,
                CommandTimeout = 300
            });
        }

        public DataSet GetAEGReportExceptionsV3(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId, DateTime? ReceptionStartDate, DateTime? ReceptionEndDate)
        {
            return db.ExecuteDataSet(new usp_AEG_Exception_ReportV3
            {
                PONumber = poNum,
                StartDate = start,
                EndDate = end,
                KitType = kitType,
                UserId = userId,
                ProjectId = projectId,
                CompanyId = companyId,
                ReceptionStartdate = ReceptionStartDate,
                ReceptionEnddate = ReceptionEndDate,
                CommandTimeout = 300
            });
        }

        public DataSet GetAEGXMLReportExceptionsV3(string poNum, DateTime? start, DateTime? end, Guid? kitType, Guid? userId, Guid? projectId, Guid? companyId, DateTime? ReceptionStartDate, DateTime? ReceptionEndDate)
        {
            return db.ExecuteDataSet(new usp_AEG_XML_Exception_ReportV3
            {
                PONumber = poNum,
                StartDate = start,
                EndDate = end,
                KitType = kitType,
                UserId = userId,
                ProjectId = projectId,
                CompanyId = companyId,
                ReceptionStartdate = ReceptionStartDate,
                ReceptionEnddate = ReceptionEndDate,
                CommandTimeout = 300
            });
        }
    }
}