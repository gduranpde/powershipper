using System;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
	class usp_ConsumerRequest_GetAll : StoredProcedure
	{
		[InParameter] 
        public bool ShowDeleted;
        [InParameter]
        public string AccountNumber;
        [InParameter]
        public string AccountName;
        [InParameter]
        public string Address;
        [InParameter]
        public string Phone;
        [InParameter]
        public int? Status;
        [InParameter]
        public Guid? BatchId;
        [InParameter]
        public string OrderClause;

        [InParameter]
        public Guid? ProjectId;

        [InParameter]
        public string FilterExpr;
	}
}