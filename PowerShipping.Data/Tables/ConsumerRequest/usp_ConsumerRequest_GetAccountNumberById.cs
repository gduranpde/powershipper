﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_ConsumerRequest_GetAccountNumberById : StoredProcedure
    {
        [InParameter] 
        public Guid ProjectId;
    }
}