﻿using Dims.DAL;
using System;

namespace PowerShipping.Data
{
    internal class usp_ConsumerRequest_CheckAccountNumberExistsInProject : StoredProcedure
    {
        [InParameter]
        public Guid ProjectId;
        [InParameter]
        public string AccountNumber;
    }
}
