﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dims.DAL;

namespace PowerShipping.Data
{
    class usp_AEG_XML_Delivery_Report: StoredProcedure
    {
         [InParameter] public DateTime? EndDate;
        [InParameter] public string PONumber;
        [InParameter] public DateTime? StartDate;
        [InParameter]
        public Guid? KitType;
        [InParameter]
        public Guid? UserId;

        [InParameter]
        public Guid? ProjectId;

        [InParameter]
        public Guid? CompanyId;
    }
}
