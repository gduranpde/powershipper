﻿using Dims.DAL;
using PowerShipping.Data.Tables.ItemCatalog;
using PowerShipping.Data.Tables.ItemSetup;
using PowerShipping.Data.Tables.KitContents;
using PowerShippingConnectionString.Data;

namespace PowerShipping.Data
{
    [ConnectionName("PowerShippingConnectionString")]

    public class PowerShippingDb : DataBase
    {
        public CustomerTable Customer = new CustomerTable();
        public CustomerImportHistoryTable CustomerImportHistory = new CustomerImportHistoryTable();
        public CustomerCreateRequestHistoryTable CustomerCreateRequestHistory = new CustomerCreateRequestHistoryTable();
		public CustomerDataImportExceptionTable CustomerDataImportException = new CustomerDataImportExceptionTable();

        public ConsumerRequestTable ConsumerRequest = new ConsumerRequestTable();
        public ConsumerRequestBatchTable ConsumerRequestBatch = new ConsumerRequestBatchTable();
        public ConsumerRequestExceptionTable ConsumerRequestException = new ConsumerRequestExceptionTable();

        public OptOutTable OptOut = new OptOutTable();
        public OptOutBatchTable OptOutBatch = new OptOutBatchTable();
        public OptOutExceptionTable OptOutException = new OptOutExceptionTable();

        public ShipmentTable Shipment = new ShipmentTable();
        public ShipmentDetailTable ShipmentDetail = new ShipmentDetailTable();

        public ItemSetupTable Item = new ItemSetupTable();
        public ItemSavingsTable ItemSaving = new ItemSavingsTable();
        public ItemCatalogTable ItemCatalog = new ItemCatalogTable();
        public KitTypeTable KitType = new KitTypeTable();
        public KitItemTable KitItem = new KitItemTable();
        public KitContentsTable KitContents = new KitContentsTable();
        public FedExShippingDefaultTable FedExShippingDefault = new FedExShippingDefaultTable();

        public TrackingNumberBatchTable TrackingNumberBatch = new TrackingNumberBatchTable();
        public TrackingNumberExceptionTable TrackingNumberException = new TrackingNumberExceptionTable();

        public CompanyTable Company = new CompanyTable();
        public OperatingCompanyTable OperatingCompany = new OperatingCompanyTable();
        public ProjectTable Project = new ProjectTable();
        public ProjectDuplicateExceptionsTable ProjectDuplicateExceptions = new ProjectDuplicateExceptionsTable();
        public StateTable State = new StateTable();
        public ShipperTable Shipper = new ShipperTable();

        public ReportTable Report = new ReportTable();

        public ReasonTable Reasons = new ReasonTable();

        public ReshipImportBatchTable ReshipImportBatch = new ReshipImportBatchTable();
        public ReshipImportExceptionTable ReshipImportException = new ReshipImportExceptionTable();
    }
}