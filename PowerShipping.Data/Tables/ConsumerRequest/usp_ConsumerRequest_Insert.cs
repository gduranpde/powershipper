using System;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
    internal class usp_ConsumerRequest_Insert : StoredProcedure
    {
        [InParameter]
        public Guid Id;

        [InParameter]
        public string AccountNumber;
        [InParameter]
        public string AccountName;
        [InParameter]
        public string Address1;
        [InParameter]
        public string Address2;
        [InParameter]
        public string City;
        [InParameter]
        public string State;
        [InParameter]
        public string ZipCode;
        [InParameter]
        public string Email;
        [InParameter]
        public string Phone1;
        [InParameter]
        public string Phone2;

        [InParameter]
        public string Method;
        [InParameter]
        public DateTime? AnalysisDate;

        [InParameter]
        public bool? IsOkayToContact;
        [InParameter]
        public string OperatingCompany;
        [InParameter]
        public string WaterHeaterFuel;
        [InParameter]
        public string HeaterFuel;
        //PG31
        [InParameter]
        public string FacilityType;
        [InParameter]
        public string RateCode;
        [InParameter]
        public string IncomeQualified;
        //PG31
        [InParameter]
        public bool IsReship;
        [InParameter]
        public bool DoNotShip;

        [InParameter]
        public DateTime? ReceiptDate;
        [InParameter]
        public DateTime? AuditFailureDate;

        [InParameter]
        public Guid? BatchId;

        [InParameter]
        public string Notes;
        [InParameter]
        public ConsumerRequestStatus Status;

        [InParameter]
        public bool OutOfState;

        [InParameter]
        public Guid ProjectId;
        [InParameter]
        public int Quantity;
        [InParameter]
        public string PremiseID;

        [InParameter]
        public string CompanyName;
        [InParameter]
        public string ServiceAddress1;
        [InParameter]
        public string ServiceAddress2;
        [InParameter]
        public string ServiceCity;
        [InParameter]
        public string ServiceState;
        [InParameter]
        public string ServiceZip;

        [InParameter]
        public DateTime? CRImportedDate;

        [InParameter]
        public string AccountNumberOld;

        [InParameter]
        public string RequestedKit;
    }
}