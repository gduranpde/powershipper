﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    class usp_AEG_Exception_ReportPhase2: StoredProcedure
    {
        [InParameter] public DateTime? EndDate;

        [InParameter] public string PONumber;

        [InParameter] public DateTime? StartDate;

        [InParameter] public Guid? KitType;

        [InParameter] public Guid? UserId;

        [InParameter] public Guid? ProjectId;

        [InParameter] public Guid? CompanyId;

        [InParameter] public DateTime? ReceptionStartdate;

        [InParameter] public DateTime? ReceptionEnddate;
       
    }
}
