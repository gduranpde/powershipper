using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_ConsumerRequest_GetById : StoredProcedure
	{
		[InParameter] public Guid Id;
	}
}