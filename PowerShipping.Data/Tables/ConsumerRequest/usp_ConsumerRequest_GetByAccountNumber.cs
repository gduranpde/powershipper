using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_ConsumerRequest_GetByAccountNumber : StoredProcedure
	{
		[InParameter] 
        public String AccountNumber;
        [InParameter]
        public Guid ProjectId;
	}
}