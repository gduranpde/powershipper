﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    class usp_ConsumerRequestChange_GetAll : StoredProcedure
    {
        [InParameter]
        public string AccountNumber;
        [InParameter]
        public DateTime? EndDate;
        [InParameter]
        public DateTime? StartDate;
        [InParameter]
        public Guid? UserId;

        [InParameter]
        public Guid? ProjectId;

        [InParameter]
        public Guid? CompanyId;
    }
}
