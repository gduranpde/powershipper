﻿using Dims.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Data.Tables.ConsumerRequest
{
    class usp_AEG_XML_EquipmentAttributesV3 : StoredProcedure
    {
        [InParameter]
        public Guid ConsumerRequestId;
    }
}
