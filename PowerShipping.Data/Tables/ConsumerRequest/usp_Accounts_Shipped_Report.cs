﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_Accounts_Shipped_Report : StoredProcedure
    {
        [InParameter] public DateTime? EndDate;
        [InParameter] public DateTime? StartDate;
        [InParameter]
        public Guid? UserId;

        [InParameter]
        public Guid? ProjectId;

        [InParameter]
        public Guid? CompanyId;
    }
}