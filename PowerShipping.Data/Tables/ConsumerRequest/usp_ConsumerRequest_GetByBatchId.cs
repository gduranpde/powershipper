﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dims.DAL;

namespace PowerShipping.Data
{
    class usp_ConsumerRequest_GetByBatchId : StoredProcedure
    {
        [InParameter]
        public Guid BatchId;
    }
}
