﻿using Dims.DAL;
using System;

namespace PowerShipping.Data
{
    internal class usp_ConsumerRequest_CheckAccountNumberExistsInAllProjects : StoredProcedure
    {
        [InParameter]
        public string AccountNumber;
    }
}
