﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_AuditReport_Exceptions : StoredProcedure
    {
        [InParameter] public DateTime? EndDate;
        [InParameter] public string PONumber;
        [InParameter] public DateTime? StartDate;
        [InParameter]
        public Guid? KitType;
        [InParameter]
        public Guid? UserId;

        [InParameter]
        public Guid? ProjectId;

        [InParameter]
        public Guid? CompanyId;
    }
}