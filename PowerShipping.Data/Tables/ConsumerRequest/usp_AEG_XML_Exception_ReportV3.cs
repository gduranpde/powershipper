﻿using Dims.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Data.Tables.ConsumerRequest
{
    class usp_AEG_XML_Exception_ReportV3 : StoredProcedure
    {
        [InParameter]
        public DateTime? EndDate;

        [InParameter]
        public string PONumber;

        [InParameter]
        public DateTime? StartDate;

        [InParameter]
        public Guid? KitType;

        [InParameter]
        public Guid? UserId;

        [InParameter]
        public Guid? ProjectId;

        [InParameter]
        public Guid? CompanyId;

        [InParameter]
        public DateTime? ReceptionStartdate;

        [InParameter]
        public DateTime? ReceptionEnddate;
    }
}
