﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    class usp_KitItem_Save : StoredProcedure
    {
        [InParameter]
        public Guid Id;
        [InParameter]
        public string Name;
        [InParameter]
        public string CatalogID;
        [InParameter]
        public int Quantity;
        [InParameter]
        public decimal Kw_Impact;
        [InParameter]
        public decimal Kwh_Impact;
        
    }
}
