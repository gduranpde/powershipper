﻿using System;
using System.Collections.Generic;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
    [spAdd(typeof(usp_KitItem_Save))]
    [spSave(typeof(usp_KitItem_Save))]
    [spGetOne(typeof(usp_KitItem_GetById))]
    [spRemove(typeof(usp_KitItem_Delete))]
    public class KitItemTable : Table<KitItem>
    {
        public List<KitItem> GetByKitType(Guid? kitTypeId)
        {
            return db.Execute<KitItem>(new usp_KitItem_GetByKitType { KitTypeId = kitTypeId });
        }

        public void SaveKitTypeKitItem(Guid itemId, Guid kitId)
        {
            db.Execute(new usp_KitTypeKitItem_Save { KitItemId = itemId, KitTypeId = kitId });
        }

        public void DeleteKitTypeKitItemByKitType(Guid kitId)
        {
            db.Execute(new usp_KitTypeKitItem_DeleteByKitType { KitTypeId = kitId });
        }

    }
}