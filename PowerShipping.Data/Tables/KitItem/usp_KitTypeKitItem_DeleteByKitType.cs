﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_KitTypeKitItem_DeleteByKitType : StoredProcedure
    {
        [InParameter]
        public Guid KitTypeId;
    }
}