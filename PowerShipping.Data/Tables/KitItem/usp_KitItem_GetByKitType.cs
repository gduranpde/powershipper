﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_KitItem_GetByKitType : StoredProcedure
    {
        [InParameter] public Guid? KitTypeId;
    }
}