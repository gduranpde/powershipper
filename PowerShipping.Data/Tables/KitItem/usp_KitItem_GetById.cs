﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_KitItem_GetById : StoredProcedure
    {
        [InParameter] public Guid Id;
    }
}