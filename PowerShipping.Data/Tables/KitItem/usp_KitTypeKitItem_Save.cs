﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_KitTypeKitItem_Save : StoredProcedure
    {
        [InParameter]
        public Guid KitItemId;
        [InParameter]
        public Guid KitTypeId;
    }
}