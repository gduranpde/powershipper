﻿using Dims.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Data.Tables.ItemCatalog
{
    internal class usp_ItemCatalog_Save : StoredProcedure
    {
        [InParameter]
        public Guid ItemCatalogID;
        [InParameter]
        public Guid ItemID;
        [InParameter]
        public Guid ProjectID;
        [InParameter]
        public string CatalogID;

    }
}
