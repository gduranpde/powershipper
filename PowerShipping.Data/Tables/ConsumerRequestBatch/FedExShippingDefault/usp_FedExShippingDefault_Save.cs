using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_FedExShippingDefault_Save : StoredProcedure
	{
		[InParameter] public string ShipperFedExAccountNumber;
		[InParameter] public string ShipperCompanyName;
		[InParameter] public string ShipperPhysicalAddress;
		[InParameter] public string ShipperContactName;
		[InParameter] public string ShipperContactPhone;
		[InParameter] public string PackagePickupLocation;
		[InParameter] public string MoreThanOnePackageForEachRecipient;
		[InParameter] public string FedExServiceType;
		[InParameter] public string DomesticOrInternational;
		[InParameter] public string AnyPackagesToResidentialAddresses;
		[InParameter] public string SignatureRequired;
		[InParameter] public string BulkLabelSupportSpecialHandling;
		[InParameter] public string PackageType;
		[InParameter] public string DeclaredValueAmount;
		[InParameter] public string PaymentType;
		[InParameter] public string FedExAccountNumberForBilling;
		[InParameter] public string DeliveryServiceForLabels;
		[InParameter] public string AddressToSendLabels;
		[InParameter] public string FedExBulkLabelEmailAddress;
		[InParameter] public string OtherCcEmailAddresses;
		[InParameter] public string ConsumerCustomerServicePhoneNumber;
		[InParameter] public string PdmShippingExceptionEmailAddress;
	}
}