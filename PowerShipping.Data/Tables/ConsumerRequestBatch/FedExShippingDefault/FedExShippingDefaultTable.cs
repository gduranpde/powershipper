﻿using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
	[spSave(typeof(usp_FedExShippingDefault_Save))]
	[spGetOne(typeof(usp_FedExShippingDefault_Get))]
	public class FedExShippingDefaultTable : Table<FedExShippingDefault>
	{
       
	}
}
