﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_ConsumerRequestBatch_GetAll : StoredProcedure
    {
        [InParameter] public DateTime? StartDate;

        [InParameter]
        public Guid? UserId;

        [InParameter]
        public Guid? ProjectId;

        [InParameter]
        public Guid? CompanyId;
    }
}