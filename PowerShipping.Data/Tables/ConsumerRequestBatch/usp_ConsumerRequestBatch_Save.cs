using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_ConsumerRequestBatch_Save : StoredProcedure
    {
        [InParameter]
        public Guid Id;
        [InParameter]
        public string FileName;
        [InParameter]
        public string ImportedBy;
        [InParameter]
        public DateTime ImportedDate;
        [InParameter]
        public int RequestsInBatch;
        [InParameter]
        public int ImportedSuccessfully;
        [InParameter]
        public int ImportedWithException;
        [InParameter]
        public string BatchLabel;
        [InParameter]
        public Guid ProjectId;
    }
}