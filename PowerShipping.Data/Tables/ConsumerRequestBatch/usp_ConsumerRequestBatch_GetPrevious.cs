﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_ConsumerRequestBatch_GetPrevious : StoredProcedure
    {
        [InParameter] public DateTime Today;
    }
}