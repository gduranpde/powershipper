﻿using System;
using System.Collections.Generic;
using Dims.DAL;
using PowerShipping.Entities;
using System.Data;

namespace PowerShipping.Data
{
    [spAdd(typeof(usp_ConsumerRequestBatch_Save))]
    [spSave(typeof(usp_ConsumerRequestBatch_Save))]
    [spGetOne(typeof(usp_ConsumerRequestBatch_GetById))]
    [spRemove(typeof(usp_ConsumerRequestBatch_Remove))]

    public class ConsumerRequestBatchTable : Table<ConsumerRequestBatch>
    {
        public string GetPreviousBatchLabel(DateTime today)
        {
            return db.ExecuteScalar<string>(new usp_ConsumerRequestBatch_GetPrevious { Today = today });
        }

        public List<ConsumerRequestBatch> GetManyFiltered(DateTime? startDate, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return db.Execute<ConsumerRequestBatch>(new usp_ConsumerRequestBatch_GetAll { StartDate = startDate, UserId = userId, ProjectId = projectId, CompanyId = companyId });
        }

        public void DeleteBatch(Guid id)
        {
            db.Execute(new usp_ConsumerRequestBatch_Remove() { Id = id, CommandTimeout = 300 });
        }
        //PG31
        public DataSet ProcessImportData(Guid id)
        {
            return db.ExecuteDataSet(new usp_ConsumerRequestBatch_Association_ShipmentDetail() { BatchId = id });
        }
        //PG31
    }
}