﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_ProjectDuplicateExceptions_AddProject : StoredProcedure
    {
        [InParameter]
        public Guid ProjectDuplicateExceptionID;

        [InParameter]
        public Guid MainProjectID;

        [InParameter]
        public Guid ExceptionProjectID;
    }
}