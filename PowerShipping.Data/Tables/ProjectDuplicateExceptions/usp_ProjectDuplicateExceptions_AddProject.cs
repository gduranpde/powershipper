﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_ProjectDuplicateExceptions_GetAllExById : StoredProcedure
    {
        [InParameter]
        public Guid MainProjectID;
    }
}