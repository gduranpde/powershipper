﻿using Dims.DAL;
using PowerShipping.Entities;
using System;
using System.Collections.Generic;

namespace PowerShipping.Data
{

	public class ProjectDuplicateExceptionsTable : Table<ProjectDuplicateExceptions>
	{
        public void ProjectRemoveExceptionProject(Guid selectedId, Guid ProjectId)
        {
            db.Execute(new usp_ProjectDuplicateExceptions_Remove { MainProjectID = selectedId, ExceptionProjectID = ProjectId });
        }

        public List<Project> GetAllSelectedProjects(Guid projectId)
        {
            return db.Execute<Project>(new usp_SelectedProject_List { Id = projectId });
        }

        public List<Project> GetUnselectedProjects(Guid projectId)
        {
            return db.Execute<Project>(new usp_UnselectedProject_List { Id = projectId });
        }

        public void AddProjectAsDuplicateException(Guid projectDuplicateExceptionId, Guid ProjectId, Guid selectedId)
        {
            db.Execute<Project>(new usp_ProjectDuplicateExceptions_AddProject { ProjectDuplicateExceptionID = projectDuplicateExceptionId, MainProjectID = ProjectId, ExceptionProjectID = selectedId });
        }

        public List<Guid> GetAllExceptionsByProjectId(Guid ProjectId)
        {
            return db.Execute<Guid>(new usp_ProjectDuplicateExceptions_GetAllExById { MainProjectID = ProjectId});
        }

      
	}
}
