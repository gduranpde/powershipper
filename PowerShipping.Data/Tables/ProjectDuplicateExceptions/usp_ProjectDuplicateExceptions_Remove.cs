﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_ProjectDuplicateExceptions_Remove : StoredProcedure
    {
        [InParameter]
        public Guid MainProjectID;

        [InParameter]
        public Guid ExceptionProjectID;
    }
}