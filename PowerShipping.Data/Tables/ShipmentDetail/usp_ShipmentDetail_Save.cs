using System;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
	class usp_ShipmentDetail_Save : StoredProcedure
	{
		[InParameter] public Guid Id;

		[InParameter] public Guid ShipmentId;
		[InParameter] public Guid ConsumerRequestId;

		[InParameter] public int SortOrder;

		[InParameter] public bool HasException;
		[InParameter] public bool IsReship;
		[InParameter] public DateTime? ShipDate;
		[InParameter] public ShipmentReshipStatus ReshipStatus;
        [InParameter] public ShipmentShippingStatus ShippingStatus;
		[InParameter] public DateTime ShippingStatusDate;

		[InParameter] public Guid? FedExBatchId;
		[InParameter] public string FedExTrackingNumber;
		[InParameter] public string FedExStatusCode;
		[InParameter] public DateTime? FedExStatusDate;
		[InParameter] public string FedExStatusHistory;
		[InParameter] public string FedExLastUpdateStatus;

		[InParameter] public string Notes;

        [InParameter]
        public Guid ProjectId;
        [InParameter]
        public Guid ShipperId;


        [InParameter]
        public Guid? ReasonId;

        [InParameter]
        public string ShipmentDetailAddress1;

        [InParameter]
        public string ShipmentDetailAddress2;

        [InParameter]
        public string ShipmentDetailCity;

        [InParameter]
        public string ShipmentDetailState;

        [InParameter]
        public string ShipmentDetailZip;
        [InParameter]
        public DateTime? ReturnDate;


	}
}