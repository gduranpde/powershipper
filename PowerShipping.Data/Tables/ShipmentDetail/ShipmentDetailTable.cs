﻿using System;
using System.Collections.Generic;
using System.Data;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
    [spAdd(typeof(usp_ShipmentDetail_Save))]
    [spSave(typeof(usp_ShipmentDetail_Save))]
    [spGetOne(typeof(usp_ShipmentDetail_GetById))]
    [spGetAll(typeof(usp_ShipmentDetail_GetMany))]
    [spRemove(typeof(usp_ShipmentDetail_Remove))]
    public class ShipmentDetailTable : Table<ShipmentDetail>
    {
        public List<ShipmentDetail> GetByShipment(Guid shipmentId)
        {
            return db.Execute<ShipmentDetail>(new usp_ShipmentDetail_GetByShipment { ShipmentId = shipmentId });
        }

        public List<ShipmentDetail> GetByAccountNumber(string accoutNumber)
        {
            return db.Execute<ShipmentDetail>(new usp_ShipmentDetail_GetByAccountNumber { AccountNumber = accoutNumber });
        }

        public List<FedExLabel> GetFedExLabels(Guid shipmentId)
        {
            return db.Execute<FedExLabel>(new usp_ShipmentDetail_GetFedExLabels { ShipmentId = shipmentId });
        }

        public List<ShipmentDetail> GetByTrackingNumber(string shipmentNum, string trackingNumber, string accNum, string accName, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return db.Execute<ShipmentDetail>(new usp_ShipmentDetail_GetByTrackingNumber
            {
                ShipmentNumber = shipmentNum,
                FedExTrackingNumber = trackingNumber,
                AccountName = accName,
                AccountNumber = accNum,
                UserId = userId,
                ProjectId = projectId,
                CompanyId = companyId,
                CommandTimeout = 300
            });
        }

        public ShipmentDetail GetByShipmentNumber(string shipmentNumber)
        {
            return db.ExecuteOne<ShipmentDetail>(new usp_ShipmentDetail_GetByShipmentNumber { ShipmentNumber = shipmentNumber });
        }

        public List<ShipmentDetail> GetByShipmentNumbersList(string shipmentNumbersList)
        {
            return db.Execute<ShipmentDetail>(new usp_ShipmentDetail_GetByShipmentNumbersList() { ShipmentNumbersList = shipmentNumbersList });
        }

        public List<ShipmentDetail> GetManyForTracking(Guid shipperId)
        {
            return db.Execute<ShipmentDetail>(new usp_ShipmentDetail_GetManyForTracking() { ShipperId = shipperId });
        }

        public List<ShipmentDetail> GetManyFiltered(int? status, DateTime? start, DateTime? end, int? shippingStatus)
        {
            return db.Execute<ShipmentDetail>(new usp_ShipmentDetail_GetMany { StatusReship = status, StartDate = start, EndDate = end, ShippingStatus = shippingStatus });
        }

        public List<ShipmentDetail> GetManyFiltered(int? status, string shipNum, string accNum, string fedNum, int? shippingStatus, Guid? userId, Guid? projectId, Guid? companyId, string accountName, string PONumber, Guid? KitTypeID, Guid? shipperID, string OpCo, Guid? reasonID, DateTime? returnStartDate, DateTime? returnEndDate)
        {
            return db.Execute<ShipmentDetail>(new usp_ShipmentDetail_GetMany
            {
                StatusReship = status,
                ShipmentNumber = shipNum,
                AccountNumber = accNum,
                FedExTrackingNumber = fedNum,
                ShippingStatus = shippingStatus,
                UserId = userId,
                ProjectId = projectId,
                CompanyId = companyId,
                AccountName = accountName,
                PurchaseOrderNumber = PONumber,
                KitTypeID = KitTypeID,
                ShipperID = shipperID,
                OpCo = OpCo,
                ReasonID = reasonID,
                ReturnStartDate = returnStartDate,
                ReturnEndDate = returnEndDate,

                CommandTimeout = 300
            });
        }

        public List<ShipmentDetail> GetManyFiltered(string accNum, string accName, string phone, string address, Guid? fedExBatchId)
        {
            return db.Execute<ShipmentDetail>(new usp_ShipmentDetail_GetMany { AccountNumber = accNum, AccountName = accName, Phone = phone, Address = address, FedExBatchId = fedExBatchId });
        }

        //public DataSet GetManyFilteredDataSet(DateTime? start, DateTime? end, int? shippingStatus)
        //{
        //    return db.ExecuteDataSet(new usp_ShipmentDetail_GetMany { StartShipDate = start, EndShipDate = end, ShippingStatus = shippingStatus });
        //}

        public List<ShipmentDetail> GetManyFiltered(DateTime? start, DateTime? end, int? shippingStatus, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return db.Execute<ShipmentDetail>(new usp_ShipmentDetail_GetMany { StartShipDate = start, EndShipDate = end, ShippingStatus = shippingStatus, UserId = userId, ProjectId = projectId, CompanyId = companyId });
        }

        public List<ShipmentDetail> GetByFedExBatch(Guid fedExBatchId)
        {
            return db.Execute<ShipmentDetail>(new usp_ShipmentDetail_GetMany { FedExBatchId = fedExBatchId });
        }

        public int GetMaxShippingNumber()
        {
            return db.ExecuteOne<int>(new usp_ShipmentDetail_GetMaxShipmentNumber());
        }

        public DataSet ReturnedShipmentReport(DateTime? start, DateTime? end, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return db.ExecuteDataSet(new usp_Returned_Shipment_Report { StartDate = start, EndDate = end, UserId = userId, ProjectId = projectId, CompanyId = companyId });
        }

        public List<ShipmentDetail> GetMissingShipmentDetails(string poNum)
        {
            return db.Execute<ShipmentDetail>(new usp_ShipmentDetail_GetMissing { PONumber = poNum });
        }

        public List<ShipmentDetail> GetCustom(string customQuery)
        {
            //return db.Execute<ConsumerRequest>(new usp_ConsumerRequest_GetCustom { CustomQuery = "select 1" });
            return db.Execute<ShipmentDetail>(new usp_ShipmentDetail_GetCustom { CustomQuery = customQuery });
        }

        public List<ShipmentDetail> FixingFedEx()
        {
            return db.Execute<ShipmentDetail>(new usp_ShipmentDetailFixingFedEx { });
        }
    }
}