﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_ShipmentDetail_GetByTrackingNumber : StoredProcedure
    {
        [InParameter] public string ShipmentNumber;
        [InParameter] public string AccountName;
        [InParameter] public string AccountNumber;
        [InParameter] public string FedExTrackingNumber;
        [InParameter]
        public Guid? UserId;

        [InParameter]
        public Guid? ProjectId;

        [InParameter]
        public Guid? CompanyId;
    }
}