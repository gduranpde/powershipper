﻿using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_ShipmentDetail_GetCustom : StoredProcedure
    {
        [InParameter] public string CustomQuery;
    }
}