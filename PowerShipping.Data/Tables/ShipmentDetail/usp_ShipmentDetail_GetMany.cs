﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_ShipmentDetail_GetMany : StoredProcedure
    {
        [InParameter]
        public int? StatusReship;

        [InParameter]
        public DateTime? StartDate;

        [InParameter]
        public DateTime? EndDate;

        [InParameter]
        public string ShipmentNumber;

        [InParameter]
        public string AccountNumber;
        [InParameter]
        public string AccountName;
        [InParameter]
        public string Address;
        [InParameter]
        public string Phone;
        [InParameter]
        public int? ShippingStatus;

        [InParameter]
        public DateTime? StartShipDate;
        [InParameter]
        public DateTime? EndShipDate;

        [InParameter]
        public string FedExTrackingNumber;

        [InParameter]
        public Guid? FedExBatchId;

        [InParameter]
        public Guid? UserId;

        [InParameter]
        public Guid? ProjectId;

        [InParameter]
        public Guid? CompanyId;

        [InParameter]
        public string PurchaseOrderNumber;

        [InParameter]
        public Guid? KitTypeID;

        [InParameter]
        public Guid? ShipperID;

        [InParameter]
        public string OpCo;

        [InParameter]
        public Guid? ReasonID;

        [InParameter]
        public DateTime? ReturnStartDate;
        [InParameter]
        public DateTime? ReturnEndDate;
    }
}