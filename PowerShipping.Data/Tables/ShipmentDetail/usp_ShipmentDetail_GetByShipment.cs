using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_ShipmentDetail_GetByShipment : StoredProcedure
	{
		[InParameter] public Guid ShipmentId;
	}
}