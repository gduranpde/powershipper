﻿using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_ShipmentDetail_GetMissing : StoredProcedure
    {
        [InParameter] public string PONumber;
    }
}