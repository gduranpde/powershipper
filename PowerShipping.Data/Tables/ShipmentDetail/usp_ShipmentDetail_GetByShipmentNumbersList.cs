﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dims.DAL;

namespace PowerShipping.Data
{
    class usp_ShipmentDetail_GetByShipmentNumbersList : StoredProcedure
    {
        [InParameter]
        public string ShipmentNumbersList;
    }
}
