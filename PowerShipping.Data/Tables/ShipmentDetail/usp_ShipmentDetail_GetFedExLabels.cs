using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_ShipmentDetail_GetFedExLabels : StoredProcedure
	{
		[InParameter] public Guid ShipmentId;
	}
}