﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_ShipmentDetail_Remove : StoredProcedure
	{
		[InParameter] public Guid Id;
	}
}