﻿using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_ShipmentDetail_GetByShipmentNumber : StoredProcedure
	{
		[InParameter] public string ShipmentNumber;
	}
}
