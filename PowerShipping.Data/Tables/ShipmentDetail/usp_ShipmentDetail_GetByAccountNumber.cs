﻿using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_ShipmentDetail_GetByAccountNumber : StoredProcedure
	{
		[InParameter] public string AccountNumber;
	}
}
