using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_ShipmentDetail_GetManyForTracking : StoredProcedure
	{
        [InParameter]
        public Guid ShipperId;
	}
}