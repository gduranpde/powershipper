﻿using Dims.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Data.Tables.OperatingCompany
{
    class usp_OperatingCompany_GetByProjectServiceState : StoredProcedure
    {
        [InParameter]
        public Guid ProjectID;
    }
}
