﻿using System;
using System.Collections.Generic;
using Dims.DAL;
using PowerShipping.Entities;
using PowerShipping.Data.Tables.OperatingCompany;

namespace PowerShipping.Data
{
    [spAdd(typeof(usp_OperatingCompany_Save))]
    [spSave(typeof(usp_OperatingCompany_Save))]
    [spGetOne(typeof(usp_OperatingCompany_GetById))]
    [spRemove(typeof(usp_OperatingCompany_Delete))]
    [spGetAll(typeof(usp_OperatingCompany_GetAll))]
    public class OperatingCompanyTable : Table<OperatingCompany>
    {          
        public List<OperatingCompany> GetByCompany(Guid? companyId)
        {
            return db.Execute<OperatingCompany>(new usp_OperatingCompany_GetByCompany { CompanyId = companyId });
        } 

        public List<OperatingCompany> GetByProjectServiceState(Guid projectID)
        {
            return db.Execute<OperatingCompany>(new usp_OperatingCompany_GetByProjectServiceState { ProjectID = projectID });
        }
        public List<OperatingCompany> GetActiveByState(int stateId)
        {
            return db.Execute<OperatingCompany>(new usp_OperatingCompany_GetByState { StateID = stateId });
        }
   
    }
}
