﻿using Dims.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Data
{
    internal class usp_OperatingCompany_Delete : StoredProcedure
    {
        [InParameter]
        public Guid OperatingCompanyID;
    }
}
