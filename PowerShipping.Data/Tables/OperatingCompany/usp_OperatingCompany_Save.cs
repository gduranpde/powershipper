﻿using Dims.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Data
{
    class usp_OperatingCompany_Save : StoredProcedure
    {
        [InParameter]
        public Guid OperatingCompanyID;
        [InParameter]
        public string OperatingCompanyCode;
        [InParameter]
        public string OperatingCompanyName;
        [InParameter]
        public Guid ParentCompanyID;
        [InParameter]
        public int ServiceState;
        [InParameter]
        public bool IsActive;

        [InParameter]
        public string CreatedBy;
        [InParameter]
        public DateTime CreatedDate;
        [InParameter]
        public string LastModifiedBy;
        [InParameter]
        public DateTime LastModifiedDate;

       
    }
}
