﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_ConsumerRequestException_GetMany : StoredProcedure
    {
        [InParameter]
        public bool ShowDeleted;

        [InParameter]
        public string AccountNumber;
        [InParameter]
        public string AccountName;
        [InParameter]
        public string Address;
        [InParameter]
        public string Phone;

        [InParameter]
        public Guid? BatchId;

        [InParameter]
        public string BatchLabel;
        [InParameter]
        public DateTime? StartDate;


        [InParameter]
        public Guid? UserId;

        [InParameter]
        public Guid? ProjectId;

        [InParameter]
        public Guid? CompanyId;
    }
}