﻿using System;
using System.Collections.Generic;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
    [spAdd(typeof(usp_ConsumerRequestException_Save))]
    [spSave(typeof(usp_ConsumerRequestException_Save))]
    [spGetOne(typeof(usp_ConsumerRequestException_GetById))]
    public class ConsumerRequestExceptionTable : Table<ConsumerRequestException>
    {
        public List<ConsumerRequestException> GetMany(bool showDeleted, string accNum, string accName, string phone, string address, Guid? batchId)
        {
            return db.Execute<ConsumerRequestException>(new usp_ConsumerRequestException_GetMany() { ShowDeleted = showDeleted, AccountNumber = accNum, AccountName = accName, Phone = phone, Address = address, BatchId = batchId });
        }

        public List<ConsumerRequestException> GetMany(bool showDeleted, string accNum, string accName, string batchLabel, DateTime? date, Guid? batchId, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return db.Execute<ConsumerRequestException>(new usp_ConsumerRequestException_GetMany()
            {
                ShowDeleted = showDeleted,
                AccountNumber = accNum,
                AccountName = accName,
                BatchLabel = batchLabel,
                StartDate = date,
                BatchId = batchId,
                UserId = userId,
                ProjectId = projectId,
                CompanyId = companyId
            });
        }

        public List<ConsumerRequestException> GetConsumerRequestExceptionByProjIdAndReason(Guid projectId)
        {
            return db.Execute<ConsumerRequestException>(new usp_ConsumerRequestException_GetByProjIdAndReason() { projectId = projectId });
        }

        public List<string> GetReason(Guid id)
        {
            return db.Execute<string>(new usp_ConsumerRequestException_GetReasonById() {Id = id});
        }

        public void DeleteConsumerRequestException(Guid id) 
        {
            db.Execute(new usp_ConsumerRequestException_DeleteById { Id = id });
        }
    }
}