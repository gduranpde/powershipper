using System;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
    internal class usp_ConsumerRequestException_Save : StoredProcedure
    {
        [InParameter]
        public Guid Id;

        [InParameter]
        public string AccountNumber;
        [InParameter]
        public string AccountName;
        [InParameter]
        public string Address1;
        [InParameter]
        public string Address2;
        [InParameter]
        public string City;
        [InParameter]
        public string State;
        [InParameter]
        public string ZipCode;
        [InParameter]
        public string Email;
        [InParameter]
        public string Phone1;
        [InParameter]
        public string Phone2;

        [InParameter]
        public string Method;
        [InParameter]
        public string AnalysisDate;

        [InParameter]
        public string IsOkayToContact;
        [InParameter]
        public string OperatingCompany;
        [InParameter]
        public string WaterHeaterFuel;
        [InParameter]
        public string HeaterFuel;
        //PG31
        [InParameter]
        public string FacilityType;
        [InParameter]
        public string RateCode;
        [InParameter]
        public string IncomeQualified;
        //PG31
        [InParameter]
        public Guid? BatchId;

        [InParameter]
        public string Reason;
        [InParameter]
        public ConsumerRequestExceptionStatus Status;
        [InParameter]
        public string Notes;
        [InParameter]
        public bool OutOfState;
        [InParameter]
        public Guid ProjectId;
        [InParameter]
        public string Quantity;
        [InParameter]
        public string PremiseID;
        [InParameter]
        public bool OverrideDuplicates;

        [InParameter]
        public string CompanyName;
        [InParameter]
        public string ServiceAddress1;
        [InParameter]
        public string ServiceAddress2;
        [InParameter]
        public string ServiceCity;
        [InParameter]
        public string ServiceState;
        [InParameter]
        public string ServiceZip;
        [InParameter]
        public string AccountNumberOld;
        [InParameter]
        public string RequestedKit;
    }
}