using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    class usp_ConsumerRequestException_GetByProjIdAndReason : StoredProcedure
	{
		[InParameter] 
        public Guid projectId;
	}
}