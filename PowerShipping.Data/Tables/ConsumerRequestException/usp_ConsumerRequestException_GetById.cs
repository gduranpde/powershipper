using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_ConsumerRequestException_GetById : StoredProcedure
	{
		[InParameter] public Guid Id;
	}
}