﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_CustomerImport_AppendImport : StoredProcedure
    {
        [InParameter]
        public string FileName;
        [InParameter]
        public Guid ProjectId;
    }
}
