
using System;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Collections.Generic;

using Dims.DAL;

using PowerShipping.Entities;


namespace  PowerShipping.Data
{

    //[spGetMany(typeof(usp_CustomerImportHistoryTable_GetMany))]
    [spGetAll(typeof(usp_CustomerImportHistory_GetAll))]
    [spGetOne(typeof(usp_CustomerImportHistory_GetOne))]
    [spAdd(typeof(usp_CustomerImportHistory_Add))]
    [spSave(typeof(usp_CustomerImportHistory_Save))]
    [spRemove(typeof(usp_CustomerImportHistory_Remove))]
    public class CustomerImportHistoryTable:Table<CustomerImportHistory>
    {
        public int ImportBulk(string fileName, Guid projectId)
        {
            return db.ExecuteScalar<int>(new usp_CustomerImport_Import { ProjectId = projectId, FileName = fileName.Replace("\\",@"\"),CommandTimeout = 600});
        }

        public int AppendImportBulk(string fileName, Guid projectId)
        {
            return db.ExecuteScalar<int>(new usp_CustomerImport_AppendImport { ProjectId = projectId, FileName = fileName.Replace("\\", @"\"), CommandTimeout = 600 });
        }

        public void CopyTable(string filename, string sql, string tableName, bool IsFirstRowHeader, string connectionString)
        {
            string header = "No";
            string pathOnly = string.Empty;
            string fileName = string.Empty;

            pathOnly = Path.GetDirectoryName(filename);
            fileName = Path.GetFileName(filename);

            if (IsFirstRowHeader)
            {
                header = "Yes";
            }

            // Connection String to Excel Workbook
            //string excelConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filename + ";Extended Properties='Excel 8.0; HDR=YES; IMEX=1'";

            //using (OleDbConnection con = new OleDbConnection(excelConnectionString))
            //using (OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;;Data Source=" + pathOnly + ";Extended Properties=\"Text;HDR=" + header + "\""))
            using (OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + pathOnly +";Extended Properties=\"Text;HDR=" + header + "\""))
            {
                OleDbCommand sqlcom = new OleDbCommand(sql + " FROM [Sheet1$]", connection);
                connection.Open();

                // Create DbDataReader to Data Worksheet
                using (DbDataReader dr = sqlcom.ExecuteReader())
                {
                    string M_str_sqlcon = connectionString;

                    //Bulk Copy to SQL Server 
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(M_str_sqlcon))
                    {
                        bulkCopy.DestinationTableName = tableName;
                        bulkCopy.WriteToServer(dr);

                    }
                }
            }
        }

        public DataSet GetAll()
        {
            return db.ExecuteDataSet(new usp_CustomerImportHistory_GetAll());
        }

        public void UpdateHistory(string historyId, int exceptionCount, int countRecords)
        {
            db.Execute(new usp_Customer_UpdateHistory { Id = historyId, ExceptionCount = exceptionCount, CountRecords = countRecords });
        }
    }
}
