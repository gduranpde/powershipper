using System;
using System.Text;
using System.Collections.Generic;

using Dims.DAL;


namespace  PowerShipping.Data
{
    
    internal class usp_CustomerImportHistory_Add:StoredProcedure
    {
          
        [InParameter]
        public Guid Id;
      

        [InParameter]
        public Guid ProjectId;
      

        [InParameter]
        public string FileName;
      

        [InParameter]
        public DateTime ImportDate;
      

        [InParameter]
        public int CountRecords;
      

        [InParameter]
        public Guid UserId;

        [InParameter]
        public string UploadType;

        [InParameter]
        public int ExceptionCount;
      

    }
}