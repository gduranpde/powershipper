﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_Customer_UpdateHistory : StoredProcedure
    {
        [InParameter]
        public string Id;

        [InParameter]
        public int ExceptionCount;

        [InParameter]
        public int CountRecords;
    }
}
