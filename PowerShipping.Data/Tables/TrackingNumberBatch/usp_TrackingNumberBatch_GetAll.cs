﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_TrackingNumberBatch_GetAll : StoredProcedure
    {
        [InParameter] public DateTime? StartDate;
    }
}