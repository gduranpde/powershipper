using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_TrackingNumberBatch_Save : StoredProcedure
	{
		[InParameter] public Guid Id;

		[InParameter] public string FileName;
		[InParameter] public string ImportedBy;
		[InParameter] public DateTime ImportedTime;

		[InParameter] public int NumbersInBatch;
		[InParameter] public int ImportedSuccessfully;
		[InParameter] public int ImportedWithException;
	}
}