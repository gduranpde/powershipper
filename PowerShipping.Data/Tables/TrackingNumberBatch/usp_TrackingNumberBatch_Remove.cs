﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_TrackingNumberBatch_Remove : StoredProcedure
    {
        [InParameter] public Guid Id;
    }
}