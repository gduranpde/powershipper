﻿using System;
using System.Collections.Generic;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
	[spAdd(typeof(usp_TrackingNumberBatch_Save))]
	[spSave(typeof(usp_TrackingNumberBatch_Save))]
    [spRemove(typeof(usp_TrackingNumberBatch_Remove))]

	public class TrackingNumberBatchTable : Table<TrackingNumberBatch>
	{
        public List<TrackingNumberBatch> GetManyFiltered(DateTime? startDate)
        {
            return db.Execute<TrackingNumberBatch>(new usp_TrackingNumberBatch_GetAll { StartDate = startDate });
        }
	}
}
