﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_UserProject_Save : StoredProcedure
    {
        //[InParameter]
        //public Guid UserProjectId;
        [InParameter] public Guid? ProjectId;
        [InParameter] public Guid UserId;
    }
}