﻿using System;
using System.Collections.Generic;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
    [spAdd(typeof(usp_Project_Save))]
    [spSave(typeof(usp_Project_Save))]
    [spGetOne(typeof(usp_Project_GetById))]
    [spRemove(typeof(usp_Project_Delete))]
    public class ProjectTable : Table<Project>
    {
        public List<Project> GetByCompany(Guid? companyId)
        {
            return db.Execute<Project>(new usp_Project_GetByCompany { CompanyId = companyId });
        }

        public void SaveUserProject(Guid userId, Guid? projectId)
        {
            db.Execute(new usp_UserProject_Save { UserId = userId, ProjectId = projectId });
        }

        public List<Project> GetByUser(Guid? userId)
        {
            return db.Execute<Project>(new usp_Project_GetByUser { UserId = userId });
        }

        public void DeleteUserProjectByUser(Guid userId)
        {
            db.Execute(new usp_UserProject_DeleteByUser { UserId = userId });
        }
        //PG31
        public string GetProgramID(Guid projectId)
        {
            return db.ExecuteScalar<string>(new usp_Project_GetProgramID { Id = projectId });
        }
        public List<Project> GetActiveByState(int stateId)
        {
            return db.Execute<Project>(new usp_Project_GetActiveByState { StateID = stateId });
        }


    }
}