﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_Project_Save : StoredProcedure
    {
        [InParameter]
        public Guid CompanyId;
        [InParameter]
        public Guid Id;
        [InParameter]
        public bool IsActive;
        [InParameter]
        public bool IsAllowDuplicates;
        [InParameter]
        public string ProjectCode;
        [InParameter]
        public string ProjectName;
        [InParameter]
        public bool IsAllowDupsInProject;
        [InParameter]
        public bool ReturnsAllowed;
        [InParameter]
        public string ProgramID;
        [InParameter]
        public int StateId;
    }
}