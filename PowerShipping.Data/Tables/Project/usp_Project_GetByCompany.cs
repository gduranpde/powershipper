﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_Project_GetByCompany : StoredProcedure
    {
        [InParameter] public Guid? CompanyId;
    }
}