﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_Project_GetActiveByState : StoredProcedure
    {
        [InParameter]
        public int StateID;
    }
}
