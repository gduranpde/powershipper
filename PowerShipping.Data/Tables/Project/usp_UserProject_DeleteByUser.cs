﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_UserProject_DeleteByUser : StoredProcedure
    {
        [InParameter] public Guid UserId;
    }
}