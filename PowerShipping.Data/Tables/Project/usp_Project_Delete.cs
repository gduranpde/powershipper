﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_Project_Delete : StoredProcedure
    {
        [InParameter] public Guid Id;
    }
}