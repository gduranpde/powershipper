﻿using Dims.DAL;
using PowerShipping.Data.Tables.ItemSetup;
using PowerShipping.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Data
{
    [spAdd(typeof(usp_ItemSetup_Save))]
    [spSave(typeof(usp_ItemSetup_Save))]
    [spGetOne(typeof(usp_ItemSetup_GetById))]
    [spRemove(typeof(usp_ItemSetup_Delete))]
    [spGetAll(typeof(usp_ItemSetup_GetAll))]

    public class ItemSetupTable : Table<Item>
    {
        public List<Item> GetAllNames()
        {
            return db.Execute<Item>(new usp_Item_List { });
        }
    }
}