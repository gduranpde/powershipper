﻿using Dims.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Data.Tables.ItemSetup
{
    internal class usp_ItemSetup_Save : StoredProcedure
    {

        [InParameter]
        public Guid ItemID;
        [InParameter]
        public string ItemName;
        [InParameter]
        public string ItemDescription;
        [InParameter]
        public decimal DefaultKwImpact;
        [InParameter]
        public decimal DefaultKwhImpact;
        [InParameter]
        public bool IsActive;

        [InParameter]
        public string CreatedBy;
        [InParameter]
        public DateTime CreatedDate;
        [InParameter]
        public string LastModifiedBy;
        [InParameter]
        public DateTime LastModifiedDate;

    }
}
