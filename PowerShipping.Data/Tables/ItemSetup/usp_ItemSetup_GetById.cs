﻿using Dims.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Data
{
    internal class usp_ItemSetup_GetById : StoredProcedure
    {
        [InParameter]
        public Guid ItemID;
    }
}
