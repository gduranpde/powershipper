﻿using Dims.DAL;
using PowerShipping.Data.Tables.KitType;
using PowerShipping.Entities;
using System;
using System.Collections.Generic;

namespace PowerShipping.Data
{
	[spAdd(typeof(usp_KitType_Save))]
	[spSave(typeof(usp_KitType_Save))]
	[spGetAll(typeof(usp_KitType_GetAll))]
	[spGetOne(typeof(usp_KitType_GetById))]
    [spRemove(typeof(usp_KitType_Remove))]
	public class KitTypeTable : Table<KitType>
	{

        public List<KitType> GetByKitName(string kitName)
        {
            return db.Execute<KitType>(new usp_KitType_GetByKitName { KitName = kitName });
        }

        public List<Item> GetItemsByKitTypeId(Guid? kitTypeId)
        {
            return db.Execute<Item>(new usp_KitType_GetItemsByKitTypeId { KitTypeId = kitTypeId });
        }

        public List<KitType> GetAllKitTypes()
        {
            return db.Execute<KitType>(new usp_KitType_GetAllKitTypes {});
        }
	}
}
