using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_KitType_GetById : StoredProcedure
	{
		[InParameter] public Guid Id;
	}
}