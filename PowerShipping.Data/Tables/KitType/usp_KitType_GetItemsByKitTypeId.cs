﻿using Dims.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Data.Tables.KitType
{
    internal class usp_KitType_GetItemsByKitTypeId: StoredProcedure 
    {
        [InParameter]
        public Guid? KitTypeId;

    }
}
