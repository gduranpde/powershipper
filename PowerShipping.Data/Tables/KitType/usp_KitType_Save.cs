using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	internal class usp_KitType_Save : StoredProcedure
	{
        [InParameter]
        public Guid Id;

        [InParameter]
        public double Width;
        //[InParameter]
        //public double Kw_Savings;
        //[InParameter]
        //public double Kwh_Savings;
		[InParameter] public double Height;
		[InParameter] public double Length;
		[InParameter] public double Weight;

		[InParameter] public string KitName;
		[InParameter] public string KitContents;
        [InParameter] public bool IsActive;
	}
}