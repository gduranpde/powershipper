﻿using Dims.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Data.Tables.KitContents
{
    internal class usp_KitContents_GetById: StoredProcedure 
    {
        [InParameter]
        public Guid KitContentID;
    }
}
