﻿using Dims.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Data.Tables.KitContents
{
    internal class ups_KitContents_GetItemsAndQuantitesByKitTypeId: StoredProcedure 
    {
        [InParameter]
        public Guid? KitTypeId;
    }
}
