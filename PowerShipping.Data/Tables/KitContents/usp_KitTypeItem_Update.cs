﻿using Dims.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerShipping.Data.Tables.KitContents
{
    class usp_KitTypeItem_Update : StoredProcedure
    {
        [InParameter]
        public Guid KitID;
        [InParameter]
        public Guid OldItemID;
        [InParameter]
        public Guid NewItemID;
    }
}
