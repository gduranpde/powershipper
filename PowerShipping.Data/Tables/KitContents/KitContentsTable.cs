﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PowerShipping.Entities;
using Dims.DAL;

namespace PowerShipping.Data.Tables.KitContents
{


    [spAdd(typeof(usp_KitContents_Save))]
    [spSave(typeof(usp_KitContents_Save))]
    [spGetOne(typeof(usp_KitContents_GetById))]
    [spRemove(typeof(usp_KitContents_Delete))]

    public class KitContentsTable : Table<KitContent>
    {

        public List<KitContent> GetFromKitContentsByKitType(Guid? kitTypeId)
        {
            return db.Execute<KitContent>(new usp_KitContents_GetByKitType { KitID = kitTypeId });
        }

        public void SaveKitTypeItem(Guid itemId, Guid kitId)
        {
            db.Execute(new usp_KitTypeItem_Save { ItemId = itemId, KitTypeId = kitId });
        }

        public void DeleteKitTypeItemByKitType(Guid kitId)
        {
            db.Execute(new usp_KitTypeItem_DeleteByKitType { KitTypeId = kitId });
        }

        public List<KitContent> GetItemsAndQuantitesByKitTypeId(Guid? kitTypeId)
        {
            return db.Execute<KitContent>(new ups_KitContents_GetItemsAndQuantitesByKitTypeId { KitTypeId = kitTypeId });
        }


        public void DeleteKitContentItemById(Guid kitContentID, Guid kitID, Guid itemID)
        {
            db.Execute(new usp_KitContents_Delete { KitContentID = kitContentID, KitID = kitID, ItemID = itemID });
        }

        public void UpdateKitTypeItem(Guid kitID, Guid oldItemID, Guid newItemID)
        {
            db.Execute(new usp_KitTypeItem_Update { KitID = kitID, OldItemID = oldItemID, NewItemID = newItemID });
        }
    }
}
