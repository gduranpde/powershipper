﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{

	internal class usp_Customer_DeleteAllByProjectId : StoredProcedure
	{
		[InParameter] public Guid ProjectId;
	}
}