﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dims.DAL;

namespace PowerShipping.Data
{
    class usp_Customer_GetMany : StoredProcedure
    {
        [InParameter]
        public Guid ProjectId;

        [InParameter]
        public string AccountNumber;

        [InParameter]
        public string InvitationCode;
    }
}
