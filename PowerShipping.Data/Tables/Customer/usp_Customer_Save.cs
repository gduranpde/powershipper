using System;
using System.Text;
using System.Collections.Generic;

using Dims.DAL;


namespace  PowerShipping.Data
{
    
    internal class usp_Customer_Save:StoredProcedure
    {
          
        [InParameter]
        public Guid Id;
      
      
        [InParameter]
        public Guid ProjectId;
      

        [InParameter]
        public string AccountNumber;
      

        [InParameter]
        public string CompanyName;
      

        [InParameter]
        public string ContactName;
      

        [InParameter]
        public string Email1;
      

        [InParameter]
        public string Email2;
      

        [InParameter]
        public string Phone1;
      

        [InParameter]
        public string Phone2;
      

        [InParameter]
        public string MailingAddress1;
      

        [InParameter]
        public string MailingAddress2;
      

        [InParameter]
        public string MailingCity;
      

        [InParameter]
        public string MailingState;
      

        [InParameter]
        public string MailingZip;
      

        [InParameter]
        public string ServiceAddress1;
      

        [InParameter]
        public string ServiceAddress2;
      

        [InParameter]
        public string ServiceCity;
      

        [InParameter]
        public string ServiceState;
      

        [InParameter]
        public string ServiceZip;
      

        [InParameter]
        public string OperatingCompany;
      

        [InParameter]
        public string WaterHeaterType;
      

        [InParameter]
        public string PremiseID;
      

        [InParameter]
        public bool? OkToContact;
      

        [InParameter]
        public bool? OptOut;
      

        [InParameter]
        public string InvitationCode;
      

    }
}