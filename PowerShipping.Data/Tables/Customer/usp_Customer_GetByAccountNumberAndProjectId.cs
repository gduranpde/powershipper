﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_Customer_GetByAccountNumberAndProjectId : StoredProcedure
	{
		[InParameter] public string AccountNumber;
		[InParameter] public Guid ProjectId;
	}
}