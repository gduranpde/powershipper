using System;
using System.Collections.Generic;
using Dims.DAL;
using PowerShipping.Entities;

namespace  PowerShipping.Data
{

    //[spGetMany(typeof(usp_CustomerTable_GetMany))]
    [spGetAll(typeof(usp_Customer_GetAll))]
    [spGetOne(typeof(usp_Customer_GetOne))]
    [spAdd(typeof(usp_Customer_Add))]
    [spSave(typeof(usp_Customer_Save))]
    [spRemove(typeof(usp_Customer_Remove))]

    public class CustomerTable:Table<Customer>
    {
        public List<Customer> GetCustom(string customQuery)
        {
            return db.Execute<Customer>(new usp_Customer_GetCustom { CustomQuery = customQuery });
        }

        public List<Customer> GetMany(string accNum, string invCode, Guid projectId)
        {
            return db.Execute<Customer>(new usp_Customer_GetMany { ProjectId = projectId, AccountNumber = accNum, InvitationCode = invCode});
        }

        public List<Customer> GetManyByEnrollments(Guid projectId, string enrollmentsXml)
        {
            return db.Execute<Customer>(new usp_Customer_GetManyByEnrollments { ProjectId = projectId, EnrollmentsXml = enrollmentsXml });
        }

        public int CountRecords(string projectId) 
        {
            var result = db.ExecuteScalar<int>(new usp_Customer_CountRecords { ProjectId = projectId});
            return result;
        }

		public Customer GetByAccountNumberAndProjectId(string accountNumber, Guid projectId)
		{
			return db.ExecuteOne<Customer>(new usp_Customer_GetByAccountNumberAndProjectId { AccountNumber = accountNumber, ProjectId = projectId });
		}

		public void DeleteAllByProjectId(Guid projectId)
		{
			db.Execute(new usp_Customer_DeleteAllByProjectId { ProjectId = projectId });
		}
	}
}
