﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dims.DAL;

namespace PowerShipping.Data
{
    public class usp_Customer_GetManyByEnrollments : StoredProcedure
    {
        [InParameter]
        public Guid ProjectId;

        [InParameter]
        public string EnrollmentsXml;        
    }
}