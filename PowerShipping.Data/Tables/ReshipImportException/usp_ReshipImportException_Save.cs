﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_ReshipImportException_Save : StoredProcedure
    {
        [InParameter]
        public Guid ReshipExceptionImportID;
        [InParameter]
        public Guid ReshipImportBatchID;
        [InParameter]
        public string AccountNumber;
        [InParameter]
        public Int64 ShipmentNumber;
        [InParameter]
        public string TrackingNumber;
        [InParameter]
        public string ReasonForException;
    }
}