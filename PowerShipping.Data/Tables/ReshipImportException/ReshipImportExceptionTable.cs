﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
    [spAdd(typeof(usp_ReshipImportException_Save))]
    [spSave(typeof(usp_ReshipImportException_Save))]
    public class ReshipImportExceptionTable : Table<ReshipImportException>
    {
        public List<PowerShipping.Entities.ReshipImportException> GetManyFiltered(String sBatchName, String sImportedBy, Guid? sShipperID, DateTime? FromDate, DateTime? ToDate)
        {
            return db.Execute<PowerShipping.Entities.ReshipImportException>(new usp_ReshipImportBatchExceptions_GetAll { sBatchName = sBatchName, sImportedBy = sImportedBy, sShipperID = sShipperID, FromDate = FromDate, ToDate = ToDate });
        }
    }
}