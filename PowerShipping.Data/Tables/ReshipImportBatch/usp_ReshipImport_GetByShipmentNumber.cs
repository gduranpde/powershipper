﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_ReshipImport_GetByShipmentNumber : StoredProcedure
    {
        [InParameter]
        public int ShipmentNumber;
    }
}