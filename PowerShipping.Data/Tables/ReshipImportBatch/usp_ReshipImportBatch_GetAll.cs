﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_ReshipImportBatch_GetAll : StoredProcedure
    {
        [InParameter]
        public Guid? sShipperID;

        [InParameter]
        public String sImportedBy;

        [InParameter]
        public String sBatchName;

        [InParameter]
        public DateTime? FromDate;

        [InParameter]
        public DateTime? ToDate;
    }
}