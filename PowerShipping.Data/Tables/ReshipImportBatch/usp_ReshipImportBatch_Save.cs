﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_ReshipImportBatch_Save : StoredProcedure
    {
        [InParameter]
        public Guid ReshipImportBatchID;
        [InParameter]
        public int ReshipImportBatchName;
        [InParameter]
        public Guid ShipperID;
        [InParameter]
        public int NumberOfRecords;
        [InParameter]
        public int ImportedSuccessfully;
        [InParameter]
        public int NumberWithExceptions;
        [InParameter]
        public string SourceFileName;
        [InParameter]
        public string SourceFilePath;
        [InParameter]
        public string ImportedBy;
        [InParameter]
        public DateTime ImportedTime;
    }
}