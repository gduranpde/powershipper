﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
    [spAdd(typeof(usp_ReshipImportBatch_Save))]
    [spSave(typeof(usp_ReshipImportBatch_Save))]
    [spRemove(typeof(usp_ReshipImportBatch_Remove))]
    [spGetOne(typeof(usp_ReshipImportBatch_GetById))]
    public class ReshipImportBatchTable : Table<ReshipImportBatch>
    {
        public DataTable GetKitType_ProjectByShipmentNumber(int shipmentNumber)
        {
            return db.ExecuteDataSet(new usp_ReshipImport_GetByShipmentNumber { ShipmentNumber = shipmentNumber }).Tables[0];
        }

        public DataTable GetReshipImportMaxShipmentNumber()
        {
            return db.ExecuteDataSet(new usp_Reship_GetMAXShipmentNumber { }).Tables[0];
        }

        public List<ReshipImportBatch> GetManyFiltered(String sBatchName, String sImportedBy, Guid? sShipperID, DateTime? FromDate, DateTime? ToDate)
        {
            return db.Execute<ReshipImportBatch>(new usp_ReshipImportBatch_GetAll { sBatchName = sBatchName, sImportedBy = sImportedBy, sShipperID = sShipperID, FromDate = FromDate, ToDate = ToDate });
        }
    }
}