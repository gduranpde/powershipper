﻿using System;
using Dims.DAL;

namespace  PowerShipping.Data
{
    
    internal class usp_CustomerDataImportException_Add : StoredProcedure
    {
	    [InParameter] public Guid CustomerDataImportExceptionId;
	    [InParameter] public Guid CustomerDataImportId;
	    [InParameter] public string RecordNumber;
	    [InParameter] public string Reason;
	    [InParameter] public string RecordData;
    }
}
