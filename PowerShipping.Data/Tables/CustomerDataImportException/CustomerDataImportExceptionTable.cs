﻿using System;
using System.Collections.Generic;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
	[spAdd(typeof(usp_CustomerDataImportException_Add))]
    public class CustomerDataImportExceptionTable:Table<CustomerDataImportException>
    {
		public List<CustomerDataImportException> GetByCustomerDataImportId(Guid id)
		{
			return db.Execute<CustomerDataImportException>(new usp_CustomerDataImportException_GetByCustomerDataImportID { CustomerDataImportId = id });
		}
    }
}
