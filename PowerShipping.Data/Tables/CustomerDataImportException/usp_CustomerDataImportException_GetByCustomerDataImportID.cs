﻿using System;
using Dims.DAL;

namespace PowerShipping.Data
{
    internal class usp_CustomerDataImportException_GetByCustomerDataImportID : StoredProcedure
    {
	    [InParameter] public Guid CustomerDataImportId;
    }
}
