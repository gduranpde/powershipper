
using System;
using System.Text;
using System.Collections.Generic;

using Dims.DAL;

using PowerShipping.Entities;


namespace  PowerShipping.Data
{

    //[spGetMany(typeof(usp_CustomerCreateRequestHistory_GetMany))]
    [spGetAll(typeof(usp_CustomerCreateRequestHistory_GetAll))]
    [spGetOne(typeof(usp_CustomerCreateRequestHistory_GetOne))]
    [spAdd(typeof(usp_CustomerCreateRequestHistory_Add))]
    [spSave(typeof(usp_CustomerCreateRequestHistory_Save))]
    [spRemove(typeof(usp_CustomerCreateRequestHistory_Remove))]
    public class CustomerCreateRequestHistoryTable:Table<CustomerCreateRequestHistory>
    {
        public List<CustomerCreateRequestHistory> GetCustom(string customQuery)
        {
            return db.Execute<CustomerCreateRequestHistory>(new usp_CustomerCreateRequestHistory_GetCustom { CustomQuery = customQuery });
        }
    }
}
