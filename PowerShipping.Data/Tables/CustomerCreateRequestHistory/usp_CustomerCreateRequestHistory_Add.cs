using System;
using System.Text;
using System.Collections.Generic;

using Dims.DAL;


namespace  PowerShipping.Data
{
    
    internal class usp_CustomerCreateRequestHistory_Add:StoredProcedure
    {
          
        [InParameter]
        public Guid Id;
      

        [InParameter]
        public Guid ProjectId;
      

        [InParameter]
        public DateTime UploadTime;
      

        [InParameter]
        public string FileName;
      

        [InParameter]
        public Guid UserId;
      

        [InParameter]
        public int TotalRecords;
      

        [InParameter]
        public int SuccessfullyUploaded;
      

        [InParameter]
        public int UploadedWithExceptions;


        [InParameter]
        public string Source;

    }
}