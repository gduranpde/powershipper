﻿using System;
using System.Collections.Generic;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
	[spAdd(typeof(usp_OptOutBatch_Save))]
	[spSave(typeof(usp_OptOutBatch_Save))]
    [spRemove(typeof(usp_OptOutBatch_Remove))]

	public class OptOutBatchTable : Table<OptOutBatch>
	{
        public List<OptOutBatch> GetManyFiltered(DateTime? startDate, Guid? userId, Guid? projectId, Guid? companyId)
        {
            return db.Execute<OptOutBatch>(new usp_OptOutBatch_GetAll { StartDate = startDate, UserId = userId, ProjectId = projectId, CompanyId = companyId});
        }
	}
}
