using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_OptOutBatch_Save : StoredProcedure
	{
		[InParameter] public Guid Id;

		[InParameter] public string FileName;
		[InParameter] public string ImportedBy;
		[InParameter] public DateTime ImportedDate;

		[InParameter] public int OptOutsInBatch;
		[InParameter] public int ImportedSuccessfully;
		[InParameter] public int ImportedWithException;
        [InParameter]
        public Guid ProjectId;
	}
}