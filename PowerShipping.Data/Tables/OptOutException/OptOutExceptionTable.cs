﻿using System;
using System.Collections.Generic;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
	[spAdd(typeof(usp_OptOutException_Save))]
	[spSave(typeof(usp_OptOutException_Save))]
	[spGetOne(typeof(usp_OptOutException_GetById))]
    [spRemove(typeof(usp_OptOutException_Remove))]

	public class OptOutExceptionTable : Table<OptOutException>
	{
        public List<OptOutException> GetMany(bool showDeleted, string accNum, string accName, string phone, string address, Guid? userId, Guid? projectId, Guid? companyId)
		{
            return db.Execute<OptOutException>(new usp_OptOutException_GetMany { ShowDeleted = showDeleted, AccountNumber = accNum, AccountName = accName, Address = address, Phone = phone, UserId = userId, ProjectId = projectId, CompanyId = companyId});
		}
	}
}
