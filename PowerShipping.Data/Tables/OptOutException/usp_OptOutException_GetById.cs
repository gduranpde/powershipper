using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_OptOutException_GetById : StoredProcedure
	{
		[InParameter] public Guid Id;
	}
}