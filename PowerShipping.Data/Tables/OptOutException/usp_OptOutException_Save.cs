using System;
using Dims.DAL;
using PowerShipping.Entities;

namespace PowerShipping.Data
{
	class usp_OptOutException_Save : StoredProcedure
	{
		[InParameter] public Guid Id;

		[InParameter] public string OptOutDate;
		[InParameter] public string AccountNumber;
		[InParameter] public string AccountName;
		[InParameter] public string Address;
		[InParameter] public string City;
		[InParameter] public string State;
		[InParameter] public string ZipCode;
		[InParameter] public string Email;
		[InParameter] public string Phone1;
		[InParameter] public string Phone2;
		[InParameter] public string Notes;

		[InParameter] public string IsEnergyProgram;
		[InParameter] public string IsTotalDesignation;

		[InParameter] public Guid? BatchId;
		[InParameter] public string Reason;
		[InParameter] public OptOutExceptionStatus Status;
        [InParameter]
        public Guid ProjectId;
	}
}