using System;
using Dims.DAL;

namespace PowerShipping.Data
{
	class usp_OptOutException_GetMany : StoredProcedure
	{
        [InParameter]
        public bool ShowDeleted;
        [InParameter]
        public string AccountNumber;
        [InParameter]
        public string AccountName;
        [InParameter]
        public string Address;
        [InParameter]
        public string Phone;

        [InParameter]
        public Guid? UserId;

        [InParameter]
        public Guid? ProjectId;
        [InParameter]
        public Guid? CompanyId;
	}
}