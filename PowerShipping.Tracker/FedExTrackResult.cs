﻿using System;
using System.Collections.Generic;
using PowerShipping.Tracker.TrackServiceWebReference;
using System.IO;
using System.Configuration;

namespace PowerShipping.Tracker
{
	class FedExTrackResult
	{
		public bool Success
		{
			get { return Errors.Count == 0; }
		}

		public readonly List<string> Errors = new List<string>();

		public string StatusCode { get; private set; }
        public string Status { get; private set; }
		public DateTime? ShipDate { get; private set; }
		public DateTime? StatusDate { get; private set; }
		public DateTime? DeliveryDate { get; private set; }
		
		public void ParseReply(TrackReply reply)
		{
			if (reply.HighestSeverity == NotificationSeverityType.NOTE ||
				reply.HighestSeverity == NotificationSeverityType.WARNING ||
				reply.HighestSeverity == NotificationSeverityType.SUCCESS)
			{
				
				foreach (var trackDetail in reply.TrackDetails)
				{
					if (trackDetail.Events != null)
					{
						// events sorted by Timestamp in reverse order
						for (var i = trackDetail.Events.Length - 1; i >= 0; i--)
						{
							var trackEvent = trackDetail.Events[i];

							if (!ShipDate.HasValue && ContainsShipDate(trackEvent))
								ShipDate = trackEvent.Timestamp;

							if (!DeliveryDate.HasValue && ContainsDeliveryDate(trackEvent))
								DeliveryDate = trackEvent.Timestamp;

							if (i == 0) // latest event
							{
								StatusCode = trackEvent.EventType;
                                Status = trackEvent.EventDescription;

								if (!StatusDate.HasValue && trackEvent.TimestampSpecified)
									StatusDate = trackEvent.Timestamp;
							}
						}
					}
				}
			}
			else
			{
				AppendError(reply.Notifications[0].Message);
			}
		}

		private static bool ContainsShipDate(TrackEvent trackEvent)
		{
            return (trackEvent.EventType == "AR" || trackEvent.EventType == "IT") && trackEvent.TimestampSpecified;
		}

        private static string allowedLocations = string.Empty;

        private static string AllowedLocations
        {
            get {
                if (string.IsNullOrEmpty(allowedLocations))
                {
                    try
                    {
                        StreamReader sr = new StreamReader(ConfigurationManager.AppSettings["AllowedLocationFilePath"]);
                        allowedLocations = sr.ReadToEnd();
                        sr.Close();

                    }
                    catch
                    {
                        allowedLocations = "At U.S. Postal Service facility At USPS facility";
                    }

                }
                
                return FedExTrackResult.allowedLocations; }
        }

        private static bool ContainsDeliveryDate(TrackEvent trackEvent)
        {
            return (trackEvent.EventType == "AR" || trackEvent.EventType == "DL")
                   && trackEvent.TimestampSpecified
                   && AllowedLocations.Contains(trackEvent.EventDescription);// == "Arrived at local Post Office";


            //     return trackEvent.EventType == "AR"
            //&& trackEvent.TimestampSpecified
            //&& trackEvent.EventDescription == "At USPS facility";


            //trackEvent.EventType == "DL"
            //&& trackEvent.TimestampSpecified;

            //&& trackEvent.ArrivalLocationSpecified
            //&& trackEvent.ArrivalLocation == ArrivalLocationType.NON_FEDEX_FACILITY;
        }

		public void AppendError(string message, params object[] args)
		{
			Errors.Add(String.Format(message, args));
		}
	}
}
