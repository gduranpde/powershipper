﻿using System;
using System.Configuration;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.Tracker
{
    internal class FedExTrackStatusUpdate
    {
        public static void UpdateAll()
        {
            new FedExTrackStatusUpdate().DoUpdate();
        }

        private readonly FedExTrackService Service = new FedExTrackService();

        private void DoUpdate()
        {
            try
            {
                Log.Info("Process FedExTrackStatusUpdate started");

                string shipperId = ConfigurationManager.AppSettings["FedExID"];
                if (string.IsNullOrEmpty(shipperId))
                {
                    Log.Error("No FedExID in configuration file.");
                    return;
                }

                var details = DataProvider.Current.ShipmentDetail.GetManyForTracking(new Guid(shipperId));
                //var details = DataProvider.Current.ShipmentDetail.FixingFedEx();
                //var details = DataProvider.Current.ShipmentDetail.GetByTrackingNumber(null, "02927003094308059218", null, null, null, null, null);

                foreach (var detail in details)
                {
                    try
                    {
                        var result = Service.CheckTrackingNumber(detail.FedExTrackingNumber, detail.AccountNumber);

                        if (result.Success)
                        {
                            if (result.ShipDate.HasValue && !detail.ShipDate.HasValue)
                                detail.ShipDate = result.ShipDate;

                            if (!String.IsNullOrEmpty(result.StatusCode))
                            {
                                detail.FedExStatusCode = result.StatusCode;
                                detail.FedExLastUpdateStatus = result.Status;

                                if (result.StatusDate.HasValue)
                                    detail.FedExStatusDate = result.StatusDate;
                            }
                            //100007272964
                            if (detail.ShipDate.HasValue)
                            {
                                if (detail.ShippingStatus != ShipmentShippingStatus.InTransit)
                                {
                                    detail.ShippingStatus = ShipmentShippingStatus.InTransit;
                                    detail.ShippingStatusDate = DateTime.Now;
                                }
                            }

                            if (result.DeliveryDate.HasValue)
                            {
                                if (detail.ShippingStatus != ShipmentShippingStatus.Delivered)
                                {
                                    detail.ShippingStatus = ShipmentShippingStatus.Delivered;
                                    detail.ShippingStatusDate = result.DeliveryDate.Value;
                                }
                            }

                            var request = DataProvider.Current.ConsumerRequest.GetOne(detail.ConsumerRequestId);

                            if (request != null)
                            {
                                switch (detail.ShippingStatus)
                                {
                                    case ShipmentShippingStatus.InTransit:
                                        request.Status = ConsumerRequestStatus.InTransit;
                                        break;
                                    //case ShipmentShippingStatus.Delivered:
                                    //    request.Status = ConsumerRequestStatus.Delivered;
                                    //    if (detail.FedExStatusDate.HasValue && !request.ReceiptDate.HasValue)
                                    //        request.ReceiptDate = detail.FedExStatusDate;
                                    //    break;
                                    case ShipmentShippingStatus.Delivered:
                                        request.Status = ConsumerRequestStatus.Delivered;
                                        if (!request.ReceiptDate.HasValue)
                                            request.ReceiptDate = detail.ShippingStatusDate;
                                        break;
                                }

                                DataProvider.Current.ConsumerRequest.Save(request);
                            }

                            detail.FedExLastUpdateStatus = "Success";

                            DataProvider.Current.ShipmentDetail.Save(detail);
                        }
                        else
                        {
                            var shipmentDetail = detail;

                            result.Errors.ForEach(e => Log.Error("Tracking Number [{0}] - {1}", shipmentDetail.FedExTrackingNumber, e));

                            shipmentDetail.FedExLastUpdateStatus = String.Join("|", result.Errors.ToArray());
                            DataProvider.Current.ShipmentDetail.Save(detail);
                        }
                    }

                    catch (Exception ex)
                    {
                        Log.Error(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

            Log.Info("Process FedExTrackStatusUpdate successfull.");
        }
    }
}