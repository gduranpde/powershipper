﻿using System;
using System.Web.Services.Protocols;
using PowerShipping.Tracker.TrackServiceWebReference;

namespace PowerShipping.Tracker
{
	class FedExTrackService
	{
		private readonly TrackService Service = new TrackService();

		public FedExTrackResult CheckTrackingNumber(string trackingNumber, string accountNumber)
		{
				var result = new FedExTrackResult();

				try
				{

                    //System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(typeof(TrackRequest));
                    //System.IO.TextWriter w = new System.IO.StreamWriter(@"d:\list.xml");
                    //s.Serialize(w, CreateTrackRequest(trackingNumber, accountNumber));
                    //w.Close();

					result.ParseReply(Service.track(CreateTrackRequest(trackingNumber, accountNumber)));

                    


					//use this to save reply to xml file 
					//var reply = Service.track(CreateTrackRequest(trackingNumber, accountNumber));
					//new XmlSerializer(typeof (TrackReply)).Serialize(new StreamWriter(String.Format("C:\\Reply_{0}.xml", trackingNumber)), reply);
				}
				catch (SoapException e)
				{
					result.AppendError(e.Detail.InnerText);
				}
				catch (Exception e)
				{
					result.AppendError(e.Message);
				}

			return result;
		}

		private static TrackRequest CreateTrackRequest(string trackingNumber, string accountNumber)
		{
			return new TrackRequest
			{
				WebAuthenticationDetail = new WebAuthenticationDetail
				{
					UserCredential = new WebAuthenticationCredential
					{
						Key = "xYt4B39csbYiV4AL",
						Password = "WEY7mcQ6P9kWQDPJAgVcH9Kb3"
					}
				},

				ClientDetail = new ClientDetail
				{
                    //AccountNumber = accountNumber, 101846429 is 120923919.
                    AccountNumber = "120923919",
					MeterNumber = "101846429"
				},

				TransactionDetail = new TransactionDetail
				{
					CustomerTransactionId = "*** PDM Shipment Track ***"
				},

				Version = new VersionId(),

				PackageIdentifier = new TrackPackageIdentifier
				{
					Value = trackingNumber,
					Type = TrackIdentifierType.TRACKING_NUMBER_OR_DOORTAG
				},

				ShipDateRangeBeginSpecified = false,
				ShipDateRangeEndSpecified = false,

				IncludeDetailedScans = true,
				IncludeDetailedScansSpecified = true
			};
		}


		private static void ShowTrackReply(TrackReply reply)
		{
			Console.WriteLine("Tracking details:");
			Console.WriteLine("**************************************");
			foreach (var trackDetail in reply.TrackDetails) // Track details for each package
			{
				Console.WriteLine("Tracking number : {0}", trackDetail.TrackingNumber);
				Console.WriteLine("Tracking number unique identifier : {0}", trackDetail.TrackingNumberUniqueIdentifier);
				Console.WriteLine("Track Status : {0} ({1})", trackDetail.StatusDescription, trackDetail.StatusCode);
				Console.WriteLine("Carrier code : {0}", trackDetail.CarrierCode);

				if (trackDetail.OtherIdentifiers != null)
				{
					foreach (var identifier in trackDetail.OtherIdentifiers)
					{
						Console.WriteLine("Other Identifier : {0} {1}", identifier.Type, identifier.Value);
					}
					Console.WriteLine();
				}
				if (trackDetail.ServiceInfo != null)
				{
					Console.WriteLine("ServiceInfo : {0}", trackDetail.ServiceInfo);
				}
				if (trackDetail.PackageWeight != null)
				{
					Console.WriteLine("Package weight : {0} {1}", trackDetail.PackageWeight.Value, trackDetail.PackageWeight.Units);
				}
				if (trackDetail.ShipmentWeight != null)
				{
					Console.WriteLine("Shipment weight : {0} {1}", trackDetail.ShipmentWeight.Value, trackDetail.ShipmentWeight.Units);
				}
				if (trackDetail.Packaging != null)
				{
					Console.WriteLine("Packaging : {0}", trackDetail.Packaging);
				}
				Console.WriteLine("Package Sequence Number : {0}", trackDetail.PackageSequenceNumber);
				Console.WriteLine("Package Count : {0} ", trackDetail.PackageCount);
				if (trackDetail.ShipTimestampSpecified)
				{
					Console.WriteLine("Ship timestamp : {0}", trackDetail.ShipTimestamp);
				}
				if (trackDetail.DestinationAddress != null)
				{
					Console.WriteLine("Destination : {0} {1}", trackDetail.DestinationAddress.City, trackDetail.DestinationAddress.StateOrProvinceCode);
				}
				if (trackDetail.ActualDeliveryTimestampSpecified)
				{
					Console.WriteLine("Actual delivery timestamp : {0}", trackDetail.ActualDeliveryTimestamp);
				}
				if (trackDetail.SignatureProofOfDeliveryAvailableSpecified)
				{
					Console.WriteLine("SPOD availability : {0}", trackDetail.SignatureProofOfDeliveryAvailable);
				}
				if (trackDetail.ProofOfDeliveryNotificationsAvailableSpecified)
				{
					Console.WriteLine("Proof Of Delivery Notifications availability : {0}", trackDetail.ProofOfDeliveryNotificationsAvailable);
				}
				if (trackDetail.ExceptionNotificationsAvailableSpecified)
				{
					Console.WriteLine("Exception Notifications availability : {0}", trackDetail.ExceptionNotificationsAvailable);
				}
				//Events
				if (trackDetail.Events != null)
				{
					Console.WriteLine("\nTrack Events:");
					foreach (var trackevent in trackDetail.Events)
					{
						if (trackevent.TimestampSpecified)
						{
							Console.WriteLine("Timestamp : {0}", trackevent.Timestamp);
						}
						Console.WriteLine("Event Type : {0} ({1})", trackevent.EventDescription, trackevent.EventType);
						Console.WriteLine("***");
					}
					Console.WriteLine();
				}
				Console.WriteLine("**************************************");
			}
		}
	}
}
