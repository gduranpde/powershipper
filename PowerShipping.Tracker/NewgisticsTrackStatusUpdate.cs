﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using PowerShipping.Data;
using PowerShipping.Entities;
using PowerShipping.ImportExport;

namespace PowerShipping.Tracker
{
    internal class NewgisticsTrackStatusUpdate
    {
        private const int FieldCount = 17;

        private const string Pattern =
            "^(\"(?<value>[^\"]*)\"|(?<value>[^,\"]*))?(,(\"(?<value>[^\"]*)\"|(?<value>[^,\"]*)))*$";

        public readonly ErrorCollection UpdateErrors = new ErrorCollection();
        private readonly Regex Matcher = new Regex(Pattern, RegexOptions.Compiled);

        public static void UpdateAll()
        {
            new NewgisticsTrackStatusUpdate().DoImportTrackingNumbers();
        }

        private void DoImportTrackingNumbers()
        {
            try
            {
                if (!CheckFirstProcess())
                {
                    return;
                }

                Log.Info("Process NewgisticsTrackStatusUpdate started");

                var ftpManager = new FtpManager();

                ftpManager.Host = ConfigurationManager.AppSettings["NewgisticsFTPUrl"];
                ftpManager.Username = ConfigurationManager.AppSettings["NewgisticsFTPLogin"];
                ftpManager.Password = ConfigurationManager.AppSettings["NewgisticsFTPPass"];

                string[] files = ftpManager.GetFileList("read/Tracking/");

                if (files == null)
                {
                    Log.Info("Some errors when getting list of files.");
                    return;
                }

                if (Environment.GetCommandLineArgs().Length == 1)
                    ProcessForToday(ftpManager, files);
                else
                {
                    for (int i = 1; i < Environment.GetCommandLineArgs().Length; i++)
                    {
                        if (Environment.GetCommandLineArgs()[i].StartsWith("all"))
                        {
                            Log.Info("Process all files");
                            ProcessAllFiles(ftpManager, files);
                        }
                        if (Environment.GetCommandLineArgs()[i].StartsWith("d"))
                        {
                            string date = Environment.GetCommandLineArgs()[i].Substring(1);
                            Log.Info("Process for date: " + date);
                            ProcessForDate(ftpManager, files, date);
                        }
                        if (Environment.GetCommandLineArgs()[i].StartsWith("from")) // from date to nowdate
                        {
                            string date = Environment.GetCommandLineArgs()[i].Substring(4);
                            Log.Info("Process from date: " + date);

                            int year = Convert.ToInt32(date.Substring(0, 4));
                            int month = Convert.ToInt32(date.Substring(4, 2));
                            int day = Convert.ToInt32(date.Substring(6, 2));

                            int dateInt = Convert.ToInt32(date);

                            for (int y = year; y <= DateTime.Now.Year; y++)
                            {
                                for (int m = 1; m <= 12; m++)
                                {
                                    string mm = m.ToString();
                                    if (mm.Length == 1)
                                        mm = "0" + mm;

                                    for (int d = 1; d <= 31; d++)
                                    {
                                        string dd = d.ToString();
                                        if (dd.Length == 1)
                                            dd = "0" + dd;

                                        string dateRes = year.ToString() + mm + dd;

                                        if (Convert.ToInt32(dateRes) >= dateInt)
                                            ProcessForDate(ftpManager, files, dateRes);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

            Log.Info("Process NewgisticsTrackStatusUpdate successfull.");
        }

        private void ProcessForToday(FtpManager ftpManager, string[] files)
        {
            string date = DateTime.Now.ToString("yyyy:MM:dd", new CultureInfo("en-US"));
            date = date.Replace(":", "");

            string file = "";
            foreach (string f in files)
            {
                if (f.Contains("_" + date))
                {
                    ftpManager.DownloadFile("read/Tracking/" + f, "Newgistics.txt");
                    file = f;
                    break;
                }
            }

            ReadFile("Newgistics.txt");
            Log.Info("Processed file: " + file);
            Console.WriteLine("Processed file: " + file);
        }

        private void ProcessAllFiles(FtpManager ftpManager, string[] files)
        {
            foreach (string f in files)
            {
                ftpManager.DownloadFile("read/Tracking/" + f, "Newgistics.txt");
                ReadFile("Newgistics.txt");
                Log.Info("Processed file: " + f);
                Console.WriteLine("Processed file: " + f);
            }
        }

        private void ProcessForDate(FtpManager ftpManager, string[] files, string date)
        {
            string file = "";
            foreach (string f in files)
            {
                if (f.Contains("_" + date))
                {
                    ftpManager.DownloadFile("read/Tracking/" + f, "Newgistics.txt");
                    file = f;
                    break;
                }
            }

            ReadFile("Newgistics.txt");
            Log.Info("Processed file: " + file);
            Console.WriteLine("Processed file: " + file);
        }

        private bool CheckFirstProcess()
        {
            if (Environment.GetCommandLineArgs().Length != 1) //for manual execution
                return true;

            FileInfo fi = new FileInfo("Newgistics.txt");

            if (!fi.Exists)
                return true;

            if (fi.LastWriteTime.ToShortDateString() != DateTime.Now.ToShortDateString())
                return true;

            return false;
        }

        public void ReadFile(string file)
        {
            List<string[]> lines = ReadLines(file);

            if (UpdateErrors.IsEmpty)
            {
                try
                {
                    lines.ForEach(l => { UpdateShipmentDetail(l); });
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
            }
            else
            {
                Log.Error("Some errors when reading file : {0}", UpdateErrors.ToString("; "));
            }
        }

        private List<string[]> ReadLines(string file)
        {
            var rows = new List<string[]>();

            try
            {
                string content = File.ReadAllText(file);
                string[] lines = content.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                for (int index = 0; index < lines.Length; index++) //Skip first line (index = 0)
                {
                    Match match = Matcher.Match(lines[index]);

                    if (!match.Success)
                    {
                        UpdateErrors.Add("Line {0}. Unexpected file format.", index);
                        continue;
                    }

                    string[] row = match.Groups["value"].Captures.Cast<Capture>().Select(c => c.Value).ToArray();

                    if (row.Length < FieldCount)
                    {
                        UpdateErrors.Add("Line {0}. Expected at least {1} fields in row but was {2}", index, FieldCount,
                                         row.Length);
                        continue;
                    }

                    // skip row with all empty values
                    if (Array.TrueForAll(row, field => field == String.Empty))
                        continue;

                    rows.Add(row);
                }
            }
            catch (Exception ex)
            {
                UpdateErrors.Add(ex.Message);
            }
            finally
            {
            }

            return rows;
        }

        private void UpdateShipmentDetail(string[] values)
        {
            string trNum = values[0].Trim();
            string eventCode = values[9].Trim();
            string eventDescr = values[10].Trim();
            string shipmentNumber = values[13].Trim();
            if (string.IsNullOrEmpty(shipmentNumber))
                if (values[14].Trim().StartsWith("10")) // if(values[14].Trim().StartsWith("10002"))
                    shipmentNumber = values[14].Trim();

            //string statusDate = values[11].Trim();
            string year = values[11].Trim().Substring(0, 4);
            string month = values[11].Trim().Substring(4, 2);
            string day = values[11].Trim().Substring(6, 2);

            string statusDate = month + "/" + day + "/" + year;

            ShipmentDetail existingDetail = DataProvider.Current.ShipmentDetail.GetByShipmentNumber(shipmentNumber);

            if (existingDetail != null)
            {
                ConsumerRequest request = DataProvider.Current.ConsumerRequest.GetOne(existingDetail.ConsumerRequestId);

                if (existingDetail.FedExTrackingNumber != trNum) //1b
                {
                    existingDetail.FedExTrackingNumber = trNum;
                    existingDetail.ShippingStatus = ShipmentShippingStatus.InTransit;
                    if (!string.IsNullOrEmpty(statusDate))
                    {
                        existingDetail.FedExStatusDate = Convert.ToDateTime(statusDate);
                        existingDetail.ShipDate = Convert.ToDateTime(statusDate);
                    }

                    if (request != null)
                    {
                        request.Status = ConsumerRequestStatus.InTransit;
                        if (existingDetail.FedExStatusDate.HasValue && !request.ReceiptDate.HasValue)
                            request.ReceiptDate = existingDetail.FedExStatusDate;
                    }
                }
                else //2
                {
                    //if (existingDetail.ShipDate == null)//only for update old shipments
                    //{
                    //    existingDetail.ShipDate = Convert.ToDateTime(statusDate);
                    //}

                    if (existingDetail.ShippingStatus != ShipmentShippingStatus.Delivered)
                    {
                        if (eventCode == "AUSPS" || eventCode == "PTS03" || eventCode == "PTS07" || eventCode == "PTS19" || eventCode == "PTSTM" || eventCode == "PTS01")//2b
                        {
                            SetDelivered(existingDetail, statusDate, request);
                        }
                        else if (string.IsNullOrEmpty(eventCode) && eventDescr == "Accepted by USPS")//new rule
                        {
                            SetDelivered(existingDetail, statusDate, request);
                        }
                        else if (existingDetail.ShippingStatus == ShipmentShippingStatus.Pending)//2c
                        {
                            existingDetail.ShippingStatus = ShipmentShippingStatus.InTransit;
                            if (!string.IsNullOrEmpty(statusDate))
                                existingDetail.FedExStatusDate = Convert.ToDateTime(statusDate);

                            existingDetail.FedExStatusCode = eventCode;
                            existingDetail.FedExLastUpdateStatus = eventDescr;

                            if (request != null)
                                request.Status = ConsumerRequestStatus.InTransit;
                        }
                        else if (existingDetail.ShippingStatus == ShipmentShippingStatus.InTransit)//2d
                        {
                            existingDetail.FedExStatusCode = eventCode;
                            existingDetail.FedExLastUpdateStatus = eventDescr;
                        }
                    }
                    else // need to update not filled Receipd Date in CR - then need remove this code
                    {
                        if (eventCode == "AUSPS" || eventCode == "PTS03" || eventCode == "PTS07" || eventCode == "PTS19" || eventCode == "PTSTM" || eventCode == "PTS01")//2b
                        {
                            if (request.ReceiptDate == null)
                            {
                                if (!string.IsNullOrEmpty(statusDate))
                                    request.ReceiptDate = Convert.ToDateTime(statusDate);
                            }
                        }
                        else if (string.IsNullOrEmpty(eventCode) && eventDescr == "Accepted by USPS")//new rule
                        {
                            if (request.ReceiptDate == null)
                            {
                                if (!string.IsNullOrEmpty(statusDate))
                                    request.ReceiptDate = Convert.ToDateTime(statusDate);
                            }
                        }
                    }
                }

                DataProvider.Current.ConsumerRequest.Save(request);

                DataProvider.Current.ShipmentDetail.Save(existingDetail);

                Console.WriteLine(existingDetail.ShipmentNumber);
            }
            else
            {
                UpdateErrors.Add("Not found shipment detail with Shipment# : {0}", shipmentNumber);
                Console.WriteLine("Not found shipment detail with Shipment# : {0}", shipmentNumber);
            }
        }

        private void SetDelivered(ShipmentDetail existingDetail, string statusDate, ConsumerRequest request)
        {
            existingDetail.ShippingStatus = ShipmentShippingStatus.Delivered;
            existingDetail.ShippingStatusDate = DateTime.Now;

            if (!string.IsNullOrEmpty(statusDate))
                existingDetail.FedExStatusDate = Convert.ToDateTime(statusDate);

            if (request != null)
            {
                request.Status = ConsumerRequestStatus.Delivered;

                if (request.ReceiptDate == null)
                {
                    if (!string.IsNullOrEmpty(statusDate))
                        request.ReceiptDate = Convert.ToDateTime(statusDate);
                }
            }
        }
    }
}