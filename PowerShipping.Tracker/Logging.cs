﻿using System;
using System.Diagnostics;

namespace PowerShipping.Tracker
{
	static class Log
	{
		public static void Blank()
		{
			Trace.WriteLine(String.Empty);
		}

		public static void Error(Exception exception)
		{
			var message = String.Empty;

			while (exception != null)
			{
				message += exception.Message + " ";
				exception = exception.InnerException;
			}

			Error(message);
		}

		public static void Error(string message, params object[] args)
		{
			Trace.WriteLine(String.Format(DateTime.Now.ToString("yyy-MM-dd HH:mm:ss") + "\tERROR\t" + message, args));
		}

		public static void Info(string message, params object[] args)
		{
			Trace.WriteLine(String.Format(DateTime.Now.ToString("yyy-MM-dd HH:mm:ss") + "\tINFO\t" + message, args));
		}
	}
}
