﻿using System;

namespace PowerShipping.Tracker
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				Log.Blank();

				Log.Info("Process started");

                NewgisticsTrackStatusUpdate.UpdateAll();
			    FedExTrackStatusUpdate.UpdateAll();
			}
			catch (Exception ex)
			{
				Log.Error(ex);
			}
			finally
			{
				Log.Info("Process completed");
			}
		}
	}
}
