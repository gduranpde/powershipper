namespace PowerShipping.ImportExport
{
	interface MappingMethod<TSource, TTarget>
	{
		MappingResult Map(TSource source, TTarget target);
	}
}