using System;
using System.Collections.Generic;

namespace PowerShipping.ImportExport
{
    public class EntityMapping
    {
        static readonly List<object> MappingMethods = new List<object>();

        static EntityMapping()
        {
            MappingMethods.Add(new MapStringArrayToConsumerRequestException());
            MappingMethods.Add(new MapConsumerRequestExceptionToConsumerRequest());

            MappingMethods.Add(new MapStringArrayToOptOutException());
            MappingMethods.Add(new MapOptOutExceptionToOptOut());

            MappingMethods.Add(new MapStringArrayToTrackingNumberException());
            MappingMethods.Add(new MapTrackingNumberExceptionToShipmentDetail());

            MappingMethods.Add(new MapStringArrayToReshipImportException());
            MappingMethods.Add(new MapReshipImportExceptionToShipmentDetails());

			MappingMethods.Add(new MapStringArrayToCustomerException());
			MappingMethods.Add(new MapCustomerExceptionToCustomer());
        }

        public static MappingResult Map<TSource, TTarget>(TSource source, TTarget target)
        {
            foreach (var method in MappingMethods)
            {
                if (method is MappingMethod<TSource, TTarget>)
                    return (method as MappingMethod<TSource, TTarget>).Map(source, target);
            }

            throw new InvalidOperationException(
                String.Format("No mapping defined for source type '{0}' and target type '{1}'",
                    typeof(TSource).Name, typeof(TTarget).Name));
        }
    }
}