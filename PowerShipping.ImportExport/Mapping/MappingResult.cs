﻿namespace PowerShipping.ImportExport
{
	public class MappingResult
	{
		public bool Success
		{
			get { return Errors.Count == 0; }
		}

		public readonly ErrorCollection Errors = new ErrorCollection();
	}
}
