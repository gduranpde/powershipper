﻿namespace PowerShipping.ImportExport
{
	abstract class StringArrayMappingMethod<TTarget> : BaseMappingMethod<string[], TTarget>
	{
		protected abstract int ExpectedNumberOfValues { get; }

		sealed protected override void MapEntities(string[] values, TTarget target, ErrorCollection errors)
		{
			if (values.Length < ExpectedNumberOfValues)
			{
				errors.Add("Unable to map entity. Expected {0} values in array but was {1}", ExpectedNumberOfValues, values.Length);
				return;
			}

			//MapStringArrayToEntity(values, target, errors);
            MapStringArrayToEntityForCustomersRequestImport(values, target, errors);
		}

		protected abstract void MapStringArrayToEntity(string[] values, TTarget target, ErrorCollection errors);
        protected abstract void MapStringArrayToEntityForCustomersRequestImport(string[] values, TTarget target, ErrorCollection errors);
	}
}
