using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using PowerShipping.Entities;

namespace PowerShipping.ImportExport
{
    internal class MapConsumerRequestExceptionToConsumerRequest :
        BaseMappingMethod<ConsumerRequestException, ConsumerRequest>
    {
        protected override void MapEntities(ConsumerRequestException exception, ConsumerRequest request, ErrorCollection errors)
        {
            request.Method = ReplaceNullAndTrim(exception.Method);
            CultureInfo ci = new CultureInfo("en-US");

            DateTime analysisDate;
            var analysisDateAsString = ReplaceNullAndTrim(exception.AnalysisDate);
            if (!string.IsNullOrEmpty(analysisDateAsString))
            {
                if (DateTime.TryParse(analysisDateAsString, ci, DateTimeStyles.None, out analysisDate))
                    request.AnalysisDate = analysisDate;
                else
                    errors.Add("Analysis Date is in unexpected format. Current value: [{0}]", analysisDateAsString);
            }
            else
                request.AnalysisDate = null;

            // Skipped values[2] - User ID

            var accountNumberAsString = ReplaceNullAndTrim(exception.AccountNumber);
            if (String.IsNullOrEmpty(accountNumberAsString))
            {
                if (exception.SkipRules)
                    request.AccountNumber = string.Empty;
                else
                    errors.Add("Account Number should not be empty.");
            }
            else
                request.AccountNumber = accountNumberAsString;

            var accountNameAsUpperString = ReplaceNullAndTrim(exception.AccountName).ToUpper();
            if (String.IsNullOrEmpty(accountNameAsUpperString))
            {
                if (exception.SkipRules)
                    request.AccountName = string.Empty;
                else
                    errors.Add("Account Name should not be empty.");
            }
            else
                request.AccountName = accountNameAsUpperString;

            var address1AsUpperString = ReplaceNullAndTrim(exception.Address1).ToUpper();
            if (String.IsNullOrEmpty(address1AsUpperString))
            {
                if (exception.SkipRules)
                    request.Address1 = string.Empty;
                else
                    errors.Add("Address1 should not be empty.");
            }
            else
            {
                if (address1AsUpperString.Length > 35)
                    errors.Add("Address1 should not be more than 35 symbols.");
                else
                    request.Address1 = address1AsUpperString;
            }

            //request.Address2 = ReplaceNullAndTrim(exception.Address2).ToUpper();
            var address2AsUpperString = ReplaceNullAndTrim(exception.Address2).ToUpper();
            if (address2AsUpperString.Length > 35)
                errors.Add("Address2 should not be more than 35 symbols.");
            else
                request.Address2 = address2AsUpperString;

            var cityAsUpperString = ReplaceNullAndTrim(exception.City).ToUpper();
            if (String.IsNullOrEmpty(cityAsUpperString))
            {
                if (exception.SkipRules)
                    request.City = string.Empty;
                else
                    errors.Add("City should not be empty.");
            }
            else
                request.City = cityAsUpperString;

            var stateAsUpperString = ReplaceNullAndTrim(exception.State).ToUpper();

            bool contains = false;

            if (exception.States != null) // NEW loop
            {
                foreach (State s in exception.States)
                {
                    if (s.Code == stateAsUpperString)
                    {
                        contains = true;
                        break;
                    }
                }
            }

            if (String.IsNullOrEmpty(stateAsUpperString))
                errors.Add("State should not be empty.");
            //else if (stateAsUpperString != "PA")
            //{
            //    if (exception.SkipRules)
            //        request.State = stateAsUpperString;
            //    else if (exception.OutOfState)
            //        request.State = stateAsUpperString;
            //    else
            //        errors.Add("State is not 'PA'. Current value: [{0}]", stateAsUpperString);
            //}
            else if (!contains)
            {
                if (exception.SkipRules)
                    request.State = stateAsUpperString;
                else if (exception.OutOfState)
                    request.State = stateAsUpperString;
                else
                    errors.Add("State is not in available States for this Project'. Current value: [{0}]", stateAsUpperString);
            }
            else
                request.State = stateAsUpperString;

            var zipCodeAsString = ReplaceNullAndTrim(exception.ZipCode);
            if (String.IsNullOrEmpty(zipCodeAsString))
            {
                if (exception.SkipRules)
                    request.ZipCode = string.Empty;
                else
                    errors.Add("Zip Code should not be empty.");
            }
            else
                request.ZipCode = zipCodeAsString.Substring(0, Math.Min(zipCodeAsString.Length, 10));

            request.Email = ReplaceNullAndTrim(exception.Email);

            var phone1 = new String(ReplaceNullAndTrim(exception.Phone1).Where(Char.IsDigit).ToArray());
            if (phone1.Length == 0 || phone1.Length == 10)
                request.Phone1 = phone1;
            else
                errors.Add("Phone1 should either be empty or contain 10 digits");

            var phone2 = new String(ReplaceNullAndTrim(exception.Phone2).Where(Char.IsDigit).ToArray());
            if (phone2.Length == 0 || phone2.Length == 10)
                request.Phone2 = phone2;
            else
                errors.Add("Phone2 should either be empty or contain 10 digits");

            var isOkToContactAsUpperString = ReplaceNullAndTrim(exception.IsOkayToContact).ToUpper();
            switch (isOkToContactAsUpperString)
            {
                case "":
                    request.IsOkayToContact = null;
                    break;
                case "YES":
                    request.IsOkayToContact = true;
                    break;
                case "NO":
                    request.IsOkayToContact = false;
                    break;
                default:
                    errors.Add("Is Okay To Contact should be 'YES', 'NO' or empty. Current value: [{0}]", isOkToContactAsUpperString);
                    break;
            }

            request.OperatingCompany = ReplaceNullAndTrim(exception.OperatingCompany);
            request.WaterHeaterFuel = ReplaceNullAndTrim(exception.WaterHeaterFuel);
            request.HeaterFuel = ReplaceNullAndTrim(exception.HeaterFuel);
            //PG31
            request.FacilityType = ReplaceNullAndTrim(exception.FacilityType);
            request.IncomeQualified = ReplaceNullAndTrim(exception.IncomeQualified);
            request.RateCode = ReplaceNullAndTrim(exception.RateCode);
            //PG31
            request.PremiseID = ReplaceNullAndTrim(exception.PremiseID);

            int quantity;
            var quantityAsString = ReplaceNullAndTrim(exception.Quantity);
            if (Int32.TryParse(quantityAsString, out quantity))
                request.Quantity = quantity;
            else
                errors.Add("Quantity is in unexpected format. Current value: [{0}]", quantityAsString);

            request.ProjectId = exception.ProjectId;

            request.IsReship = false;
            request.DoNotShip = false;

            request.ReceiptDate = null;
            request.AuditFailureDate = null;

            request.BatchId = exception.BatchId;

            request.Status = ConsumerRequestStatus.RequestReceived;

            request.Notes = ReplaceNullAndTrim(exception.Notes).ToUpper();

            request.AccountNumberOld = ReplaceNullAndTrim(exception.AccountNumberOld).ToUpper();

            request.OutOfState = exception.OutOfState;

            if (exception.OverrideDuplicates != null)
                request.OverrideDuplicates = (bool)exception.OverrideDuplicates;
            else
                request.OverrideDuplicates = false;

            if (exception.CompanyName != null)
                request.CompanyName = ReplaceNullAndTrim(exception.CompanyName).ToUpper();
            //if (exception.ServiceAddress1 != null)
            //    request.ServiceAddress1 = ReplaceNullAndTrim(exception.ServiceAddress1).ToUpper();

            if (string.IsNullOrEmpty(exception.ServiceAddress1) && string.IsNullOrEmpty(exception.ServiceAddress2) && string.IsNullOrEmpty(exception.ServiceCity)
                && string.IsNullOrEmpty(exception.ServiceState) && string.IsNullOrEmpty(exception.ServiceZip))
            {
                exception.ServiceAddress1 = exception.Address1;
                exception.ServiceAddress2 = exception.Address2;
                exception.ServiceCity = exception.City;
                exception.ServiceState = exception.State;
                exception.ServiceZip = exception.ZipCode;
            }

            var serviceaddress1AsUpperString = ReplaceNullAndTrim(exception.ServiceAddress1).ToUpper();
            if (!String.IsNullOrEmpty(serviceaddress1AsUpperString))
            {
                if (serviceaddress1AsUpperString.Length > 35)
                    errors.Add("ServiceAddress1 should not be more than 35 symbols.");
                else
                    request.ServiceAddress1 = serviceaddress1AsUpperString;
            }

            if (!String.IsNullOrEmpty(exception.ServiceAddress2))
                request.ServiceAddress2 = ReplaceNullAndTrim(exception.ServiceAddress2).ToUpper();
            if (!String.IsNullOrEmpty(exception.ServiceCity))
                request.ServiceCity = ReplaceNullAndTrim(exception.ServiceCity).ToUpper();
            //if (exception.ServiceState != null)
            //    request.ServiceState = ReplaceNullAndTrim(exception.ServiceState).ToUpper();

            var servicestateAsUpperString = ReplaceNullAndTrim(exception.ServiceState).ToUpper();
            bool containsS = false;

            if (exception.States != null) // NEW if loop
            {
                foreach (State s in exception.States)
                {
                    if (s.Code == servicestateAsUpperString)
                    {
                        containsS = true;
                        break;
                    }
                }
            }

            if (String.IsNullOrEmpty(servicestateAsUpperString))
            {
            }
            else if (!containsS)
            {
                if (exception.SkipRules)
                    request.ServiceState = servicestateAsUpperString;
                else if (exception.OutOfState)
                    request.ServiceState = servicestateAsUpperString;
                else
                    errors.Add("ServiceState is not in available States for this Project'. Current value: [{0}]", servicestateAsUpperString);
            }
            else
                request.ServiceState = servicestateAsUpperString;

            if (!String.IsNullOrEmpty(exception.ServiceZip))
                request.ServiceZip = exception.ServiceZip;

            request.RequestedKit = ReplaceNullAndTrim(exception.RequestedKit);

            //Below lines commented SS Oct 15 //
            /* sc 14-09-2011 */
            //DateTime ImportedDate;
            //var importedDateAsString = ReplaceNullAndTrim(exception.ImportedDate.ToString());
            //if (!string.IsNullOrEmpty(importedDateAsString))
            //{
            //    if (DateTime.TryParse(importedDateAsString, ci, DateTimeStyles.None, out ImportedDate))
            //        request.CRImportedDate = ImportedDate;
            //    else
            //        errors.Add("Imported Date is in unexpected format. Current value: [{0}]", importedDateAsString);
            //}
            //else
            //    request.CRImportedDate = null;
            /* END */
        }
    }
}