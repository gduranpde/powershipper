﻿using PowerShipping.Entities;

namespace PowerShipping.ImportExport
{
	internal class MapStringArrayToCustomerException : StringArrayMappingMethod<CustomerException>
    {
        protected override int ExpectedNumberOfValues
        {
            get { return 26; }
        }

		protected override void MapStringArrayToEntityForCustomersRequestImport(string[] values, CustomerException exception, ErrorCollection errors)
        {
			exception.AccountNumber = ReplaceNullAndTrim(values[0]);
			exception.CompanyName = ReplaceNullAndTrim(values[1]);
			exception.ContactName = ReplaceNullAndTrim(values[2]);
			exception.Email1 = ReplaceNullAndTrim(values[3]);
			exception.Email2 = ReplaceNullAndTrim(values[4]);
			exception.Phone1 = ReplaceNullAndTrim(values[5]);
			exception.Phone2 = ReplaceNullAndTrim(values[6]);
			exception.MailingAddress1 = ReplaceNullAndTrim(values[7]);
			exception.MailingAddress2 = ReplaceNullAndTrim(values[8]);
			exception.MailingCity = ReplaceNullAndTrim(values[9]);
			exception.MailingState = ReplaceNullAndTrim(values[10]);
			exception.MailingZip = ReplaceNullAndTrim(values[11]);
			exception.ServiceAddress1 = ReplaceNullAndTrim(values[12]).ToUpper();
			exception.ServiceAddress2 = ReplaceNullAndTrim(values[13]).ToUpper();
			exception.ServiceCity = ReplaceNullAndTrim(values[14]).ToUpper();
			exception.ServiceState = ReplaceNullAndTrim(values[15]).ToUpper();
			exception.ServiceZip = ReplaceNullAndTrim(values[16]);
			exception.OperatingCompany = ReplaceNullAndTrim(values[17]);
			exception.WaterHeaterType = ReplaceNullAndTrim(values[18]);
			exception.PremiseId = ReplaceNullAndTrim(values[19]);
			exception.OkToContact = ReplaceNullAndTrim(values[20]);
			exception.OptOut = ReplaceNullAndTrim(values[21]);
			exception.InvitationCode = ReplaceNullAndTrim(values[22]);
			exception.FacilityType = ReplaceNullAndTrim(values[23]);
			exception.RateCode = ReplaceNullAndTrim(values[24]);
			exception.IncomeQualified = ReplaceNullAndTrim(values[25]);
        }

		protected override void MapStringArrayToEntity(string[] values, CustomerException exception, ErrorCollection errors)
        {
			exception.AccountNumber = ReplaceNullAndTrim(values[0]);
			exception.CompanyName = ReplaceNullAndTrim(values[1]);
			exception.ContactName = ReplaceNullAndTrim(values[2]);
			exception.Email1 = ReplaceNullAndTrim(values[3]).Substring(0, 50);
			exception.Email2 = ReplaceNullAndTrim(values[4]).Substring(0, 50);
			exception.Phone1 = ReplaceNullAndTrim(values[5]);
			exception.Phone2 = ReplaceNullAndTrim(values[6]);
			exception.MailingAddress1 = ReplaceNullAndTrim(values[7]);
			exception.MailingAddress2 = ReplaceNullAndTrim(values[8]);
			exception.MailingCity = ReplaceNullAndTrim(values[9]);
			exception.MailingState = ReplaceNullAndTrim(values[10]);
			exception.MailingZip = ReplaceNullAndTrim(values[11]).Substring(0, 10);
			exception.ServiceAddress1 = ReplaceNullAndTrim(values[12]).ToUpper();
			exception.ServiceAddress2 = ReplaceNullAndTrim(values[13]).ToUpper();
			exception.ServiceCity = ReplaceNullAndTrim(values[14]).ToUpper();
			exception.ServiceState = ReplaceNullAndTrim(values[15]).ToUpper();
			exception.ServiceZip = ReplaceNullAndTrim(values[16]);
			exception.OperatingCompany = ReplaceNullAndTrim(values[17]);
			exception.WaterHeaterType = ReplaceNullAndTrim(values[18]);
			exception.PremiseId = ReplaceNullAndTrim(values[19]);
			exception.OkToContact = ReplaceNullAndTrim(values[20]);
			exception.OptOut = ReplaceNullAndTrim(values[21]);
			exception.InvitationCode = ReplaceNullAndTrim(values[22]).Substring(0, 50);
			exception.FacilityType = ReplaceNullAndTrim(values[23]);
			exception.RateCode = ReplaceNullAndTrim(values[24]);
			exception.IncomeQualified = ReplaceNullAndTrim(values[25]);
        }
    }
}