﻿using PowerShipping.Entities;

namespace PowerShipping.ImportExport
{
	class MapStringArrayToTrackingNumberException : StringArrayMappingMethod<TrackingNumberException>
	{
		protected override int ExpectedNumberOfValues
		{
			//get { return 10; }
            get { return 3; }
		}

		protected override void MapStringArrayToEntity(string[] values, TrackingNumberException exception, ErrorCollection errors)
		{
			exception.TrackingNumber = ReplaceNullAndTrim(values[0]);
			exception.AccountNumber = ReplaceNullAndTrim(values[1]);
			exception.Reference = ReplaceNullAndTrim(values[2]); //ShipmentID ???
            if (values.Length > 3)
			    exception.PurchaseOrderNumber = ReplaceNullAndTrim(values[3]);
            if (values.Length > 4)
			    exception.AccountName = ReplaceNullAndTrim(values[4]).ToUpper();
            if (values.Length > 5)
			    exception.Address1 = ReplaceNullAndTrim(values[5]).ToUpper();
            if (values.Length > 6)
			    exception.Address2 = ReplaceNullAndTrim(values[6]).ToUpper();
            if (values.Length > 7)
			    exception.City = ReplaceNullAndTrim(values[7]).ToUpper();
            if (values.Length > 8)
			    exception.State = ReplaceNullAndTrim(values[8]).ToUpper();
            if (values.Length > 9)
			    exception.ZipCode = ReplaceNullAndTrim(values[9]);

			exception.Status = TrackingNumberExceptionStatus.Normal;
		}
        protected override void MapStringArrayToEntityForCustomersRequestImport(string[] values, TrackingNumberException exception, ErrorCollection errors)
        {
            exception.TrackingNumber = ReplaceNullAndTrim(values[0]);
            exception.AccountNumber = ReplaceNullAndTrim(values[1]);
            exception.Reference = ReplaceNullAndTrim(values[2]); //ShipmentID ???
            if (values.Length > 3)
                exception.PurchaseOrderNumber = ReplaceNullAndTrim(values[3]);
            if (values.Length > 4)
                exception.AccountName = ReplaceNullAndTrim(values[4]).ToUpper();
            if (values.Length > 5)
                exception.Address1 = ReplaceNullAndTrim(values[5]).ToUpper();
            if (values.Length > 6)
                exception.Address2 = ReplaceNullAndTrim(values[6]).ToUpper();
            if (values.Length > 7)
                exception.City = ReplaceNullAndTrim(values[7]).ToUpper();
            if (values.Length > 8)
                exception.State = ReplaceNullAndTrim(values[8]).ToUpper();
            if (values.Length > 9)
                exception.ZipCode = ReplaceNullAndTrim(values[9]);

            exception.Status = TrackingNumberExceptionStatus.Normal;
        }
	}
}
