﻿using PowerShipping.Entities;

namespace PowerShipping.ImportExport
{
    internal class MapCustomerExceptionToCustomer :
		BaseMappingMethod<CustomerException, Customer>
    {
		protected override void MapEntities(CustomerException exception, Customer customer, ErrorCollection errors)
        {
			customer.ProjectId = exception.ProjectId;
			
			var accountNumberAsString = ReplaceNullAndTrim(exception.AccountNumber);
			if (accountNumberAsString.Length > 50)
			{
				errors.Add("Account Number should not be more than 50 symbols.");
			}
			else
				customer.AccountNumber = accountNumberAsString;

			var companyNameAsString = ReplaceNullAndTrim(exception.CompanyName);
			if (companyNameAsString.Length > 200)
			{
				errors.Add("Company Name should not be more than 200 symbols.");
			}
			else
				customer.CompanyName = companyNameAsString;

			var contactNameAsString = ReplaceNullAndTrim(exception.ContactName);
			if (contactNameAsString.Length > 100)
			{
				errors.Add("Contact Name should not be more than 100 symbols.");
			}
			else
				customer.ContactName = contactNameAsString;
			
			var email1AsString = ReplaceNullAndTrim(exception.Email1);
			if (email1AsString.Length > 50)
				errors.Add("Email1 should not be more than 50 symbols.");
			else
				customer.Email1 = email1AsString;

			var email2AsString = ReplaceNullAndTrim(exception.Email2);
			if (email2AsString.Length > 50)
				errors.Add("Email2 should not be more than 50 symbols.");
			else
				customer.Email2 = email2AsString;

			var phone1 = ReplaceNullAndTrim(exception.Phone1);
			if (phone1.Length > 50)
				errors.Add("Phone1 should not be more than 50 symbols.");
			else
				customer.Phone1 = phone1;

			var phone2 = ReplaceNullAndTrim(exception.Phone2);
			if (phone2.Length > 50)
				errors.Add("Phone2 should not be more than 50 symbols.");
			else
				customer.Phone2 = phone2;

			var mailingAddress1AsUpperString = ReplaceNullAndTrim(exception.MailingAddress1).ToUpper();
			if (mailingAddress1AsUpperString.Length > 50)
				errors.Add("Mailing Address1 should not be more than 50 symbols.");
			else
				customer.MailingAddress1 = mailingAddress1AsUpperString;

			var mailingAddress2AsUpperString = ReplaceNullAndTrim(exception.MailingAddress2).ToUpper();
			if (mailingAddress2AsUpperString.Length > 50)
				errors.Add("Mailing Address2 should not be more than 50 symbols.");
			else
				customer.MailingAddress2 = mailingAddress2AsUpperString;

			var mailingCityAsUpperString = ReplaceNullAndTrim(exception.MailingCity).ToUpper();
			if (mailingCityAsUpperString.Length > 50)
				errors.Add("Mailing City should not be more than 50 symbols.");
			else
				customer.MailingCity = mailingCityAsUpperString;

			var mailingStateAsUpperString = ReplaceNullAndTrim(exception.MailingState).ToUpper();
			if (mailingStateAsUpperString.Length > 50)
				errors.Add("Mailing State should not be more than 50 symbols.");
			else
				customer.MailingState = mailingStateAsUpperString;

			var mailingZipAsString = ReplaceNullAndTrim(exception.MailingZip);
			if (mailingZipAsString.Length > 10)
				errors.Add("Mailing Zip should not be more than 10 symbols.");
			else
				customer.MailingZip = mailingZipAsString;

			var serviceAddress1AsUpperString = ReplaceNullAndTrim(exception.ServiceAddress1).ToUpper();
			if (serviceAddress1AsUpperString.Length > 200)
				errors.Add("Service Address1 should not be more than 200 symbols.");
			else
				customer.ServiceAddress1 = serviceAddress1AsUpperString;

			var serviceAddress2AsUpperString = ReplaceNullAndTrim(exception.ServiceAddress2).ToUpper();
			if (serviceAddress2AsUpperString.Length > 200)
				errors.Add("Service Address2 should not be more than 200 symbols.");
			else
				customer.ServiceAddress2 = serviceAddress2AsUpperString;

			var serviceCityAsUpperString = ReplaceNullAndTrim(exception.ServiceCity).ToUpper();
			if (serviceCityAsUpperString.Length > 50)
				errors.Add("Service City should not be more than 50 symbols.");
			else
				customer.ServiceCity = serviceCityAsUpperString;

			var serviceStateAsUpperString = ReplaceNullAndTrim(exception.ServiceState).ToUpper();
			if (serviceStateAsUpperString.Length > 50)
				errors.Add("Service State should not be more than 50 symbols.");
			else
				customer.ServiceState = serviceStateAsUpperString;

			var serviceZipAsString = ReplaceNullAndTrim(exception.ServiceZip);
			if (serviceZipAsString.Length > 10)
				errors.Add("Service Zip should not be more than 10 symbols.");
			else
				customer.ServiceZip = serviceZipAsString;

			var operatingCompany = ReplaceNullAndTrim(exception.OperatingCompany);
			if (operatingCompany.Length > 30)
				errors.Add("Operating Company should not be more than 30 symbols.");
			else
				customer.OperatingCompany = operatingCompany;

			var waterHeaterType = ReplaceNullAndTrim(exception.WaterHeaterType);
			if (waterHeaterType.Length > 30)
				errors.Add("Water Heater Type should not be more than 30 symbols.");
			else
				customer.WaterHeaterType = waterHeaterType;

			var premiseId = ReplaceNullAndTrim(exception.PremiseId);
			if (premiseId.Length > 100)
				errors.Add("Premise ID should not be more than 100 symbols.");
			else
				customer.PremiseID = premiseId;

			var isOkToContactAsUpperString = ReplaceNullAndTrim(exception.OkToContact).ToUpper();
			switch (isOkToContactAsUpperString)
			{
				case "":
					customer.OkToContact = null;
					break;
				case "YES":
					customer.OkToContact = true;
					break;
				case "NO":
					customer.OkToContact = false;
					break;
				default:
					errors.Add("Is Okay To Contact should be 'YES', 'NO' or empty. Current value: [{0}]", isOkToContactAsUpperString);
					break;
			}

			var isOptOutAsUpperString = ReplaceNullAndTrim(exception.OptOut).ToUpper();
			switch (isOptOutAsUpperString)
			{
				case "":
					customer.OptOut = null;
					break;
				case "YES":
					customer.OptOut = true;
					break;
				case "NO":
					customer.OptOut = false;
					break;
				default:
					errors.Add("Opt Out should be 'YES', 'NO' or empty. Current value: [{0}]", isOkToContactAsUpperString);
					break;
			}

			var invitationCode = ReplaceNullAndTrim(exception.InvitationCode);
			if (invitationCode.Length > 50)
				errors.Add("Invitation Code should not be more than 50 symbols.");
			else
				customer.InvitationCode = invitationCode;

			var facilityType = ReplaceNullAndTrim(exception.FacilityType);
			if (facilityType.Length > 50)
				errors.Add("Facility Type should not be more than 50 symbols.");
			else
				customer.FacilityType = facilityType;

			var rateCode = ReplaceNullAndTrim(exception.RateCode);
			if (rateCode.Length > 20)
				errors.Add("Rate Code should not be more than 20 symbols.");
			else
				customer.RateCode = rateCode;

			var incomeQualified = ReplaceNullAndTrim(exception.IncomeQualified);
			if (incomeQualified.Length > 20)
				errors.Add("Income Qualified should not be more than 20 symbols.");
			else
				customer.IncomeQualified = incomeQualified;
        }
    }
}