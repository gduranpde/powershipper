using System;

namespace PowerShipping.ImportExport
{
	abstract class BaseMappingMethod<TSource, TTarget> : MappingMethod<TSource, TTarget>
	{
		public MappingResult Map(TSource source, TTarget target)
		{
			var result = new MappingResult();

			try
			{
				MapEntities(source, target, result.Errors);
			}
			catch (Exception ex)
			{
				result.Errors.Add("Unexpected mapping error : ", ex.Message);
			}
			
			return result;
		}


		protected string ReplaceNullAndTrim(string original)
		{
			if (String.IsNullOrEmpty(original))
				return String.Empty;

			return original.Trim();
		}

		protected abstract void MapEntities(TSource source, TTarget target, ErrorCollection errors);
	}
}