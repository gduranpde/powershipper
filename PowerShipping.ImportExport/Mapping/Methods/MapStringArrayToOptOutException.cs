﻿using PowerShipping.Entities;

namespace PowerShipping.ImportExport
{
	class MapStringArrayToOptOutException : StringArrayMappingMethod<OptOutException>
	{
		protected override int ExpectedNumberOfValues
		{
			get { return 12; }
		}

		protected override void MapStringArrayToEntity(string[] values, OptOutException exception, ErrorCollection errors)
		{
			exception.OptOutDate = ReplaceNullAndTrim(values[0]);
			exception.AccountNumber = ReplaceNullAndTrim(values[1]);
			exception.AccountName = ReplaceNullAndTrim(values[2]).ToUpper();
			exception.Address = ReplaceNullAndTrim(values[3]).ToUpper();
			exception.City = ReplaceNullAndTrim(values[4]).ToUpper();
			exception.State = ReplaceNullAndTrim(values[5]).ToUpper();
			exception.ZipCode = ReplaceNullAndTrim(values[6]);
			exception.Phone1 = ReplaceNullAndTrim(values[7]);
			exception.Phone2 = ReplaceNullAndTrim(values[8]);
			exception.Email = ReplaceNullAndTrim(values[9]);
			// Skipped values[10] - TotalOptOut
			exception.Notes = ReplaceNullAndTrim(values[11]);
			exception.Status = OptOutExceptionStatus.Normal;
		}
        protected override void MapStringArrayToEntityForCustomersRequestImport(string[] values, OptOutException exception, ErrorCollection errors)
        {
            exception.OptOutDate = ReplaceNullAndTrim(values[0]);
            exception.AccountNumber = ReplaceNullAndTrim(values[1]);
            exception.AccountName = ReplaceNullAndTrim(values[2]).ToUpper();
            exception.Address = ReplaceNullAndTrim(values[3]).ToUpper();
            exception.City = ReplaceNullAndTrim(values[4]).ToUpper();
            exception.State = ReplaceNullAndTrim(values[5]).ToUpper();
            exception.ZipCode = ReplaceNullAndTrim(values[6]);
            exception.Phone1 = ReplaceNullAndTrim(values[7]);
            exception.Phone2 = ReplaceNullAndTrim(values[8]);
            exception.Email = ReplaceNullAndTrim(values[9]);
            // Skipped values[10] - TotalOptOut
            exception.Notes = ReplaceNullAndTrim(values[11]);
            exception.Status = OptOutExceptionStatus.Normal;
        }
	}
}
