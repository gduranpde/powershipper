﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PowerShipping.Entities;

namespace PowerShipping.ImportExport
{
    internal class MapReshipImportExceptionToShipmentDetails : BaseMappingMethod<ReshipImportException, ShipmentDetail>
    {
        protected override void MapEntities(ReshipImportException exception, ShipmentDetail request, ErrorCollection errors)
        {
            var trackingNumberAsString = ReplaceNullAndTrim(exception.TrackingNumber);
            if (String.IsNullOrEmpty(trackingNumberAsString) || trackingNumberAsString.ToLower() == "null")
                errors.Add("Tracking Number should not be empty");
            else
                request.FedExTrackingNumber = trackingNumberAsString;

            var accountNumberAsString = ReplaceNullAndTrim(exception.AccountNumber);
            if (String.IsNullOrEmpty(accountNumberAsString) || accountNumberAsString.ToLower() == "null")
                errors.Add("Account Number should not be empty");
            else
                request.AccountNumber = accountNumberAsString;

            var shipmentNumberAsString = ReplaceNullAndTrim(Convert.ToString(exception.ShipmentNumber));
            if (String.IsNullOrEmpty(shipmentNumberAsString) || shipmentNumberAsString.ToLower() == "null")
                errors.Add("Shipment Number should not be empty");
            else
                request.ShipmentNumber = shipmentNumberAsString;

            var KitTypeIDAsString = ReplaceNullAndTrim(Convert.ToString(exception.KitTypeID));
            if (String.IsNullOrEmpty(KitTypeIDAsString))
                errors.Add("KiTypeID should not be empty");
            else
                request.KitTypeId = new Guid(KitTypeIDAsString);

            var ProjectIDAsString = ReplaceNullAndTrim(Convert.ToString(exception.ProjectID));
            if (String.IsNullOrEmpty(ProjectIDAsString))
                errors.Add("ProjectID should not be empty");
            else
                request.ProjectId = new Guid(ProjectIDAsString);

            var ShipmentIDAsString = ReplaceNullAndTrim(Convert.ToString(exception.ShipmentID));
            if (String.IsNullOrEmpty(ShipmentIDAsString))
                errors.Add("ShipmentID should not be empty");
            else
                request.ShipmentId = new Guid(ShipmentIDAsString);
        }
    }
}