﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PowerShipping.Entities;

namespace PowerShipping.ImportExport
{
    internal class MapStringArrayToReshipImportException : StringArrayMappingMethod<ReshipImportException>
    {
        protected override int ExpectedNumberOfValues
        {
            get { return 3; }
        }

        protected override void MapStringArrayToEntity(string[] values, ReshipImportException exception, ErrorCollection errors)
        {
            exception.AccountNumber = ReplaceNullAndTrim(values[0]);
            exception.ShipmentNumber = Convert.ToInt32(ReplaceNullAndTrim(values[1]));
            exception.TrackingNumber = ReplaceNullAndTrim(values[2]);

            exception.KitTypeID = new Guid(ReplaceNullAndTrim(values[3]));
            exception.ProjectID = new Guid(ReplaceNullAndTrim(values[4]));
            exception.ShipmentID = new Guid(ReplaceNullAndTrim(values[5]));
        }
        protected override void MapStringArrayToEntityForCustomersRequestImport(string[] values, ReshipImportException exception, ErrorCollection errors)
        {
            exception.AccountNumber = ReplaceNullAndTrim(values[0]);
            exception.ShipmentNumber = Convert.ToInt32(ReplaceNullAndTrim(values[1]));
            exception.TrackingNumber = ReplaceNullAndTrim(values[2]);

            exception.KitTypeID = new Guid(ReplaceNullAndTrim(values[3]));
            exception.ProjectID = new Guid(ReplaceNullAndTrim(values[4]));
            exception.ShipmentID = new Guid(ReplaceNullAndTrim(values[5]));
        }
    }
}