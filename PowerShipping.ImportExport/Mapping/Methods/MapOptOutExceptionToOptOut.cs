﻿using System;
using System.Linq;
using PowerShipping.Entities;

namespace PowerShipping.ImportExport
{
	class MapOptOutExceptionToOptOut : BaseMappingMethod<OptOutException, OptOut>
	{
		protected override void MapEntities(OptOutException exception, OptOut optout, ErrorCollection errors)
		{
			var optOutDateAsString = ReplaceNullAndTrim(exception.OptOutDate);
			if (String.IsNullOrEmpty(optOutDateAsString))
				optout.OptOutDate = DateTime.Now;
			else
			{
				DateTime oupOutDate;
				if (DateTime.TryParse(optOutDateAsString, out oupOutDate))
					optout.OptOutDate = oupOutDate;
				else
					errors.Add("Opt Out Date is in unexpected format. Current value: [{0}]", optOutDateAsString);
			}

			var accountNumberAsString = ReplaceNullAndTrim(exception.AccountNumber);
			if (String.IsNullOrEmpty(accountNumberAsString))
				errors.Add("Account Number should not be empty.");
			else
				optout.AccountNumber = accountNumberAsString;

			var accountNameAsUpperString = ReplaceNullAndTrim(exception.AccountName).ToUpper();
			if (String.IsNullOrEmpty(accountNameAsUpperString))
				errors.Add("Account Name should not be empty.");
			else
				optout.AccountName = accountNameAsUpperString;

			var addressAsUpperString = ReplaceNullAndTrim(exception.Address).ToUpper();
			if (String.IsNullOrEmpty(addressAsUpperString))
				errors.Add("Address should not be empty.");
			else
				optout.Address = addressAsUpperString;

			var cityAsUpperString = ReplaceNullAndTrim(exception.City).ToUpper();
			if (String.IsNullOrEmpty(cityAsUpperString))
				errors.Add("City should not be empty.");
			else
				optout.City = cityAsUpperString;

			var stateAsUpperString = ReplaceNullAndTrim(exception.State).ToUpper();
			if (String.IsNullOrEmpty(stateAsUpperString))
				errors.Add("State should not be empty.");
            //else if (stateAsUpperString != "PA")
            //    errors.Add("State is not 'PA'. Current value: [{0}]", stateAsUpperString);
            else
                optout.State = stateAsUpperString;

			var zipCodeAsString = ReplaceNullAndTrim(exception.ZipCode);
			if (String.IsNullOrEmpty(zipCodeAsString))
				errors.Add("Zip Code should not be empty.");
			else
				optout.ZipCode = zipCodeAsString;

			var phone1 = new String(ReplaceNullAndTrim(exception.Phone1).Where(Char.IsDigit).ToArray());
			if (phone1.Length == 0 || phone1.Length == 10)
				optout.Phone1 = phone1;
			else
				errors.Add("Phone1 should either be empty or contain 10 digits");

			var phone2 = new String(ReplaceNullAndTrim(exception.Phone2).Where(Char.IsDigit).ToArray());
			if (phone2.Length == 0 || phone2.Length == 10)
				optout.Phone2 = phone2;
			else
				errors.Add("Phone2 should either be empty or contain 10 digits");

			optout.Email = ReplaceNullAndTrim(exception.Email);
			optout.Notes = ReplaceNullAndTrim(exception.Notes);

			optout.IsEnergyProgram = true;
			optout.IsTotalDesignation = false;

            optout.ProjectId = exception.ProjectId;
		}
	}
}
