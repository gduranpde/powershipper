﻿using PowerShipping.Entities;

namespace PowerShipping.ImportExport
{
    internal class MapStringArrayToConsumerRequestException : StringArrayMappingMethod<ConsumerRequestException>
    {
        protected override int ExpectedNumberOfValues
        {
            get { return 18; }
        }

        protected override void MapStringArrayToEntityForCustomersRequestImport(string[] values, ConsumerRequestException exception, ErrorCollection errors)
        {
            exception.Method = ReplaceNullAndTrim(values[0]);
            exception.AnalysisDate = ReplaceNullAndTrim(values[1]);
            // Skipped values[2] - User ID
            exception.AccountNumber = ReplaceNullAndTrim(values[3]);
            exception.AccountName = ReplaceNullAndTrim(values[4]).ToUpper();

            exception.Address1 = ReplaceNullAndTrim(values[5]).ToUpper();
            exception.Address2 = ReplaceNullAndTrim(values[6]).ToUpper();

            exception.City = ReplaceNullAndTrim(values[7]).ToUpper();
            exception.State = ReplaceNullAndTrim(values[8]).ToUpper();
            exception.ZipCode = ReplaceNullAndTrim(values[9]);
            exception.Email = ReplaceNullAndTrim(values[10]);
            exception.Phone1 = ReplaceNullAndTrim(values[11]);
            exception.Phone2 = ReplaceNullAndTrim(values[12]);
            exception.IsOkayToContact = ReplaceNullAndTrim(values[13]);
            exception.OperatingCompany = ReplaceNullAndTrim(values[14]);

            //if (values.Length > 14)
            exception.WaterHeaterFuel = ReplaceNullAndTrim(values[15]);
           
            exception.Quantity = ReplaceNullAndTrim(values[16]);
            if (string.IsNullOrEmpty(exception.Quantity))
                exception.Quantity = "1";

            exception.PremiseID = ReplaceNullAndTrim(values[17]);

            exception.Status = ConsumerRequestExceptionStatus.RequestReceived;
            if (values.Length > 18)
            {
                exception.CompanyName = ReplaceNullAndTrim(values[18]).ToUpper();
                exception.ServiceAddress1 = ReplaceNullAndTrim(values[19]).ToUpper();
                exception.ServiceAddress2 = ReplaceNullAndTrim(values[20]).ToUpper();

                exception.ServiceCity = ReplaceNullAndTrim(values[21]).ToUpper();
                exception.ServiceState = ReplaceNullAndTrim(values[22]).ToUpper();
                exception.ServiceZip = ReplaceNullAndTrim(values[23]);
            }
            if (values.Length > 24)
            {
                exception.AccountNumberOld = ReplaceNullAndTrim(values[24]);
            }
            //PG31
            if (values.Length > 25)
            {
                exception.FacilityType = ReplaceNullAndTrim(values[25]);
                exception.RateCode = ReplaceNullAndTrim(values[26]);//PG31
                exception.IncomeQualified = ReplaceNullAndTrim(values[27]);
                exception.HeaterFuel = ReplaceNullAndTrim(values[28]);
                exception.RequestedKit = ReplaceNullAndTrim(values[29]);
            }
            //PG31
        }

        protected override void MapStringArrayToEntity(string[] values, ConsumerRequestException exception, ErrorCollection errors)
        {
            exception.Method = ReplaceNullAndTrim(values[11]);
            exception.AnalysisDate = ReplaceNullAndTrim(values[15]);
            // Skipped values[2] - User ID
            exception.AccountNumber = ReplaceNullAndTrim(values[1]);
            exception.AccountName = ReplaceNullAndTrim(values[2]).ToUpper();

            var addressAsUpperString = ReplaceNullAndTrim(values[3]).ToUpper();

            //if (addressAsUpperString.Length <= 35)
            //    exception.Address1 = addressAsUpperString;
            //else
            //{
            //    exception.Address1 = addressAsUpperString.Substring(0, 35);
            //    exception.Address2 = addressAsUpperString.Substring(35, addressAsUpperString.Length - 35);
            //}
            exception.Address1 = addressAsUpperString;
            exception.Address2 = ReplaceNullAndTrim(values[4]).ToUpper();

            exception.City = ReplaceNullAndTrim(values[5]).ToUpper();
            exception.State = ReplaceNullAndTrim(values[6]).ToUpper();
            exception.ZipCode = ReplaceNullAndTrim(values[7]);
            exception.Email = ReplaceNullAndTrim(values[8]);
            exception.Phone1 = ReplaceNullAndTrim(values[9]);
            exception.Phone2 = ReplaceNullAndTrim(values[10]);
            exception.IsOkayToContact = ReplaceNullAndTrim(values[14]);
            exception.OperatingCompany = ReplaceNullAndTrim(values[21]);

            //if (values.Length > 14)
            exception.WaterHeaterFuel = ReplaceNullAndTrim(values[22]);
            exception.HeaterFuel = ReplaceNullAndTrim(values[38]);

            exception.Quantity = ReplaceNullAndTrim(values[25]);
            if (string.IsNullOrEmpty(exception.Quantity))
                exception.Quantity = "1";

            exception.PremiseID = ReplaceNullAndTrim(values[26]);

            exception.Status = ConsumerRequestExceptionStatus.RequestReceived;
            if (values.Length > 18)
            {
                exception.CompanyName = ReplaceNullAndTrim(values[27]).ToUpper();
                exception.ServiceAddress1 = ReplaceNullAndTrim(values[28]).ToUpper();
                exception.ServiceAddress2 = ReplaceNullAndTrim(values[29]).ToUpper();

                exception.ServiceCity = ReplaceNullAndTrim(values[30]).ToUpper();
                exception.ServiceState = ReplaceNullAndTrim(values[31]).ToUpper();
                exception.ServiceZip = ReplaceNullAndTrim(values[32]);
            }
            if (values.Length > 24)
            {
                exception.AccountNumberOld = ReplaceNullAndTrim(values[34]);
            }
            //PG31
            if (values.Length > 25)
            {
                exception.FacilityType = ReplaceNullAndTrim(values[35]);
                exception.RateCode = ReplaceNullAndTrim(values[36]);//PG31
                exception.IncomeQualified = ReplaceNullAndTrim(values[37]);

            }
            //PG31
        }
    }
}