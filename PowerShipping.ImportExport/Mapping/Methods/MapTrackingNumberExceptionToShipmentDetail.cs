﻿using System;
using PowerShipping.Entities;

namespace PowerShipping.ImportExport
{
	class MapTrackingNumberExceptionToShipmentDetail : BaseMappingMethod<TrackingNumberException, ShipmentDetail>
	{
		protected override void MapEntities(TrackingNumberException exception, ShipmentDetail detail, ErrorCollection errors)
		{
			var trackingNumberAsString = ReplaceNullAndTrim(exception.TrackingNumber);
			if (String.IsNullOrEmpty(trackingNumberAsString))
				errors.Add("Tracking Number should not be empty");

			var accountNumberAsString = ReplaceNullAndTrim(exception.AccountNumber);
			if (String.IsNullOrEmpty(accountNumberAsString))
				errors.Add("Account Number should not be empty");

			var shipmentNumberAsString = ReplaceNullAndTrim(exception.Reference);
			if (String.IsNullOrEmpty(shipmentNumberAsString))
				errors.Add("Reference (Shipment Number) should not be empty");

			if (errors.IsEmpty)
				detail.FedExTrackingNumber = trackingNumberAsString;
		}
	}
}
