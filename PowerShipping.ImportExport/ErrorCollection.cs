﻿using System;
using System.Collections.Generic;

namespace PowerShipping.ImportExport
{
	public class ErrorCollection
	{
		readonly List<string> Errors = new List<string>();

		public int Count
		{
			get { return Errors.Count; }
		}

		public bool IsEmpty
		{
			get { return Errors.Count == 0; }
		}

		public void Add(string error)
		{
			Errors.Add(error);
		}

		public void Add(string format, params object[] args)
		{
			Errors.Add(String.Format(format, args));
		}

		public void ForEach(Action<string> action)
		{
			Errors.ForEach(action);
		}

		public new string ToString()
		{
			return String.Join("<br />", Errors.ToArray());
		}

        public string ToString(string delim)
        {
            return String.Join(delim, Errors.ToArray());
        }
	}
}
