﻿using System;
using System.Data;
using PowerShipping.Data;
using PowerShipping.Logging;

namespace PowerShipping.ImportExport
{
	public static partial class With
	{
		public static void Transaction(IsolationLevel level, Action transactional)
		{
			DataProvider.Current.Open();
			try
			{
				DataProvider.Current.TransactionBegin(level);
				try
				{
					transactional();
					DataProvider.Current.TransactionCommit();
				}
				catch(Exception ex)
				{
                    CurrentLogger.Log.Error(ex);
					DataProvider.Current.TransactionRollback();
					throw;
				}
			}
			finally
			{
				DataProvider.Current.Close();
			}
		}

		public static void Transaction(Action transactional)
		{
			Transaction(IsolationLevel.ReadCommitted, transactional);
		}
	}

}
