﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.ImportExport
{
	public class OptOutImporter
	{
		private const int FieldCount = 12;

		private readonly string FilePath;
		private readonly string ImportedBy;
        private readonly Guid ProjectId;
		private readonly OptOutBatch Batch;
		
		public readonly ErrorCollection ImportErrors = new ErrorCollection();

		public event EventHandler<ImportExportProgressEventArgs> ImportProgress;

        public OptOutImporter(string filePath, string importedBy, Guid projectId)
		{
			FilePath = filePath;
			ImportedBy = importedBy;
            ProjectId = projectId;

			Batch = new OptOutBatch
			{
				Id = Guid.NewGuid(),
				ImportedBy = ImportedBy,
				ImportedDate = DateTime.Now,
				FileName = Path.GetFileName(FilePath),
				OptOutsInBatch = 0,
				ImportedSuccessfully = 0,
				ImportedWithException = 0,
                ProjectId = ProjectId
			};
		}

		public OptOutBatch Import()
		{
			ReportProgress("Loading file...", 100, 50);
			var lines = ReadLines();
			ReportProgress("File loaded.", 100, 100);

			if (ImportErrors.IsEmpty)
			{
				ReportProgress("Importing Opt Outs...", Batch.OptOutsInBatch, 0);

				try
				{
					With.Transaction(() =>
					{
						DataProvider.Current.OptOutBatch.Add(Batch);

						lines.ForEach(l =>
						{
							ImportLine(l);

							ReportProgress("Importing Opt Outs...",
								Batch.OptOutsInBatch, Batch.ImportedSuccessfully + Batch.ImportedWithException);
						});

						DataProvider.Current.OptOutBatch.Save(Batch);
					});

					ReportProgress("Opt Outs imported successfully.",
							Batch.OptOutsInBatch, Batch.ImportedSuccessfully + Batch.ImportedWithException);

				}
				catch (Exception ex)
				{
					ImportErrors.Add(ex.Message);
					ReportProgress("Opt Outs import failed.",
						Batch.OptOutsInBatch, Batch.OptOutsInBatch);
				}
			}

			return Batch;
		}

		List<string[]> ReadLines()
		{
			Batch.OptOutsInBatch = 0;
			var rows = new List<string[]>();

			try
			{
				var content = File.ReadAllText(FilePath);
				var lines = content.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

				for (var index = 1; index < lines.Length; index++) //Skip first line (index = 0)
				{
					var match = Matcher.Match(lines[index]);

					if (!match.Success)
					{
						ImportErrors.Add("Line {0}. Unexpected file format.", index);
						continue;
					}

					var row = match.Groups["value"].Captures.Cast<Capture>().Select(c => c.Value).ToArray();

					if (row.Length < FieldCount)
					{
						ImportErrors.Add("Line {0}. Expected at least {1} fields in row but was {2}", index, FieldCount, row.Length);
						continue;
					}

					// skip row with all empty values
					if (Array.TrueForAll(row, field => field == String.Empty))
						continue;
					
					rows.Add(row);
				}
			}
			catch (Exception ex)
			{
				ImportErrors.Add(ex.Message);
			}
			finally
			{
				Batch.OptOutsInBatch = rows.Count;
			}

			return rows;
		}

		private void ImportLine(string[] columns)
		{
			var exception = NewOptOutException();
			var mapping = EntityMapping.Map(columns, exception);

			if (mapping.Success)
			{
				var optout = NewOptOut();
				var validation = EntityValidation.MapAndValidate(exception, optout);

				if (validation.Success)
				{
					DataProvider.Current.OptOut.Save(optout);

					var existingRequest = DataProvider.Current.ConsumerRequest.GetByAccountNumber(optout.AccountNumber, ProjectId);
					if (existingRequest != null)
					{
						existingRequest.Status = ConsumerRequestStatus.OptOut;
						DataProvider.Current.ConsumerRequest.Save(existingRequest);
					}

					Batch.ImportedSuccessfully++;
					return;
				}

				DataProvider.Current.OptOutException.Add(exception);
				Batch.ImportedWithException++;
			}
		}

		private OptOut NewOptOut()
		{
			return new OptOut
	       	{
	       		Id = Guid.NewGuid(),
	       		BatchId = Batch.Id,
                ProjectId = ProjectId
	       	};
		}

		private OptOutException NewOptOutException()
		{
			return new OptOutException
	       	{
	       		Id = Guid.NewGuid(),
				BatchId = Batch.Id,
                ProjectId = ProjectId
	       	};
		}

		private void ReportProgress(string message, int total, int value)
		{
			var handler = ImportProgress;

			if (handler != null)
				handler(this, new ImportExportProgressEventArgs(message, total, value));
		}

		private readonly Regex Matcher = new Regex(Pattern, RegexOptions.Compiled);

		private const string Pattern = "^(\"(?<value>[^\"]*)\"|(?<value>[^,\"]*))?(,(\"(?<value>[^\"]*)\"|(?<value>[^,\"]*)))*$";

	}
}
