﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using PowerShipping.Data;
using PowerShipping.Entities;
using System.Diagnostics;
using PowerShipping.Logging;
using System.Xml.Linq;

namespace PowerShipping.ImportExport
{
    public class CreatorLabelRequestFile
    {
        private const int FieldCount = 15;

        private readonly string _Directory;
        private readonly string FilePath;
        private readonly Guid UserId;
        private readonly Guid ProjectId;


        private readonly CustomerCreateRequestHistory _history;

        public readonly ErrorCollection ImportErrors = new ErrorCollection();
        public event EventHandler<ImportExportProgressEventArgs> ImportProgress;

        public CreatorLabelRequestFile(string filePath, string dir, string userId, Guid projectId, string source)
        {
            FilePath = filePath;
            UserId = new Guid(userId);
            ProjectId = projectId;
            _Directory = dir;

            _history = new CustomerCreateRequestHistory()
                           {
                               Id = Guid.NewGuid(),
                               TotalRecords = 0,
                               FileName = Path.GetFileName(FilePath),
                               UploadTime = DateTime.Now,
                               ProjectId = ProjectId,
                               UserId = UserId,
                               Source = source
                           };
        }


        public CustomerCreateRequestHistory Create()
        {
            ReportProgress("Loading file...", 100, 50);
            var lines = ReadLines();
            ReportProgress("File loaded.", 100, 100);

            if (ImportErrors.IsEmpty)
            {
                ReportProgress("Importing Enrollments ...", _history.TotalRecords, 0);

                try
                {
                    With.Transaction(() =>
                                         {
                                             Stopwatch sw = Stopwatch.StartNew();
                                             List<Enrollment> enrollments = new List<Enrollment>();
                                             lines.ForEach(l =>
                                                               {
                                                                   Enrollment e = ConvertToEnrollment(l);
                                                                   if (e != null)
                                                                       enrollments.Add(e);
                                                               });
                                             sw.Stop();
                                             CurrentLogger.Log.Info("Finished converting enrollments in:" + sw.Elapsed.TotalSeconds + " seconds");
                                             sw.Reset();
                                             sw.Start();
                                             ExportFile(enrollments);
                                             sw.Stop();
                                             CurrentLogger.Log.Info("Finished exporting in:" + sw.Elapsed.TotalSeconds + " seconds");
                                             DataProvider.Current.CustomerCreateRequestHistory.Add(_history);
                                         });

                    ReportProgress("Enrollments imported successfully.",
                            _history.TotalRecords, _history.SuccessfullyUploaded + _history.UploadedWithExceptions);
                }
                catch (Exception ex)
                {
                    ImportErrors.Add(ex.Message);
                    ReportProgress("Enrollment import failed.", _history.TotalRecords, _history.SuccessfullyUploaded + _history.UploadedWithExceptions);
                }
            }
            return _history;
        }


        List<string[]> ReadLines()
        {
            _history.TotalRecords = 0;
            var rows = new List<string[]>();

            try
            {
                var content = File.ReadAllText(FilePath);

                var lines = content.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                for (var index = 1; index < lines.Length; index++) //  Skip first line (index = 1)
                {
                    lines[index] = lines[index].Replace("\"", "");
                    var row = lines[index].Split(',');
                    if (row.Length < FieldCount)
                    {
                        ImportErrors.Add("Line {0}. Expected at least {1} values in row but was {2}", index, FieldCount, row.Length);
                        continue;
                    }
                    rows.Add(row);
                }
            }
            catch (Exception ex)
            {
                ImportErrors.Add(ex.Message);
            }
            finally
            {
                _history.TotalRecords = rows.Count;
            }

            return rows;
        }

        private Enrollment ConvertToEnrollment(string[] columns)
        {
            try
            {
                Enrollment e = new Enrollment();
                e.Method = columns[0];
                Int64 id = Convert.ToInt64(columns[1]);
                e.EnrollmentID = id;
                e.SubmittedDate = columns[2];
                e.OperatingCompany = columns[3];
                e.InvitationCode = columns[4];
                e.AccountNumber = columns[5];
                e.AccountNumber12 = columns[6];
                e.ContactFirstName = columns[7];
                e.ContactLastName = columns[8];
                e.ContactEmail = columns[9];
                e.ContactPhone = columns[10];
                e.Phone1 = columns[10];
                e.ContactZipCode = columns[11];
                e.ShipToAddress = columns[12];
                e.Address1 = columns[13];
                e.Address2 = columns[14];
                e.City = columns[15];
                e.State = columns[16];
                e.Zip = columns[17];
                e.ReferralSource = columns[18];
                e.WaterHeater = columns[19];
                e.HeaterFuel = columns[20];
                e.MoreInfo = columns[21];
                e.RequestedKit = columns[22];
                e.FacilityType = columns[23];
                e.IncomeQualified = columns[24];

                return e;
            }
            catch (Exception ex)
            {
                //first row
                return null;
            }
        }

        private void ExportFile(List<Enrollment> enrollments)
        {
            CurrentLogger.Log.Info("Started export file");
            Stopwatch swAll = Stopwatch.StartNew();
            string fileRes = _history.Id + "_Result.csv";
            string fileEx = _history.Id + "_Exception.csv";
            string fileUploaded = _history.Id + "_Uploaded.csv";

            List<Enrollment> good = new List<Enrollment>();
            List<Enrollment> exceptions = new List<Enrollment>();


            //TODO: check good or exception
            StringBuilder xml = new StringBuilder("<root>");
            foreach (var e in enrollments)
            {
                xml.AppendLine("<enrollment>");
                xml.AppendLine("<account>" + e.AccountNumber + "</account>");
                xml.AppendLine("<account12>" + e.AccountNumber12 + "</account12>");
                xml.AppendLine("<invitation>" + e.InvitationCode + "</invitation>");
                xml.AppendLine("</enrollment>");
            }
            xml.Append("</root>");
                                    
            List<Customer> customers = DataProvider.Current.Customer.GetManyByEnrollments(ProjectId, xml.ToString());
            CurrentLogger.Log.Info("Total customers:" + customers.Count);
            CurrentLogger.Log.Info("ExportFile - Started foreach");
            Stopwatch sw = Stopwatch.StartNew();
            foreach (Enrollment enroll in enrollments)
            {
                if (!string.IsNullOrEmpty(enroll.AccountNumber) || 
                    !string.IsNullOrEmpty(enroll.AccountNumber12) ||
                    !string.IsNullOrEmpty(enroll.InvitationCode))
                {
                    Customer cust = customers.Find(c => c.AccountNumber == enroll.AccountNumber || 
                                                        c.AccountNumber == enroll.AccountNumber12 ||
                                                        c.InvitationCode == enroll.InvitationCode);
                    if (cust == null)
                        exceptions.Add(enroll);
                    else
                    {                       
                        FillEnrollment(enroll, cust);                        
                        good.Add(enroll);
                    }
                }                
                else
                {
                    exceptions.Add(enroll);
                }
            }
            sw.Stop();
            CurrentLogger.Log.Info("ExportFile - Finished foreach in:" + sw.Elapsed.TotalSeconds + " seconds");

            CurrentLogger.Log.Info("ExportFile - Started saving files");
            sw = Stopwatch.StartNew();
            _history.UploadedWithExceptions = exceptions.Count;
            _history.SuccessfullyUploaded = good.Count;

            using (var file = new StreamWriter(Path.Combine(_Directory, fileRes), false))
            {
                file.WriteLine(ColumnHeaders());
                good.ForEach(enrollment => file.WriteLine(Formatted(enrollment)));
            }
            using (var file = new StreamWriter(Path.Combine(_Directory, fileEx), false))
            {
                file.WriteLine(ColumnExceptionHeaders());
                exceptions.ForEach(enrollment => file.WriteLine(FormattedException(enrollment)));
            }
            using (var file = new StreamWriter(Path.Combine(_Directory, fileUploaded), false))
            {
                file.WriteLine(ColumnExceptionHeaders());
                enrollments.ForEach(enrollment => file.WriteLine(FormattedException(enrollment)));
            }
            sw.Stop();
            CurrentLogger.Log.Info("ExportFile - Finished saving files in:" + sw.Elapsed.TotalSeconds + " seconds");
            swAll.Stop();
            CurrentLogger.Log.Info("Finished ExportFile method in:" + swAll.Elapsed.TotalSeconds + " seconds");
        }

        /// <summary>
        /// Fills up an enrollment
        /// </summary>
        /// <param name="enroll"></param>
        /// <param name="cust"></param>
        private void FillEnrollment(Enrollment enroll, Customer cust)
        {
            enroll.OldAccountNumber = enroll.AccountNumber;
            enroll.AccountNumberResult = enroll.AccountNumber12;
            //if (!string.IsNullOrEmpty(enroll.AccountNumber))
            //{
            //    enroll.AccountNumber12 = enroll.AccountNumber12;
            //}
            
            if (enroll.ShipToAddress == "M")
            {
                enroll.Address1 = cust.MailingAddress1;
                enroll.Address2 = cust.MailingAddress2;
                enroll.City = cust.MailingCity;
                enroll.State = cust.MailingState;
                enroll.Zip = cust.MailingZip;
            }
            else
            {
                if (enroll.ShipToAddress == "S")
                {
                    enroll.Address1 = cust.ServiceAddress1;
                    enroll.Address2 = cust.ServiceAddress2;
                    enroll.City = cust.ServiceCity;
                    enroll.State = cust.ServiceState;
                    enroll.Zip = cust.ServiceZip;
                }
                else
                {
                    if (enroll.ShipToAddress == "O")
                    {
                        enroll.Address1 = enroll.Address1;
                        enroll.Address2 = enroll.Address2;
                        enroll.City = enroll.City;
                        enroll.State = enroll.State;
                        enroll.Zip = enroll.Zip;
                    }
                }
            }
            if (string.IsNullOrEmpty(enroll.ContactEmail))
            {
                enroll.ContactEmail = cust.Email1;
            }
            if (string.IsNullOrEmpty(enroll.MoreInfo))
            {
                enroll.OkToContact = null;
            }
            if (string.IsNullOrEmpty(enroll.FacilityType))
            {
                enroll.FacilityType = cust.FacilityType;
            }
            if (string.IsNullOrEmpty(enroll.IncomeQualified))
            {
                enroll.IncomeQualified = cust.IncomeQualified;
            }
            enroll.Method = enroll.Method;
            enroll.SubmittedDate = enroll.SubmittedDate;
            enroll.ContactEmail = enroll.ContactEmail;
            enroll.ContactName = cust.ContactName;            
            enroll.OkToContact = enroll.MoreInfo;
            enroll.OpCo = cust.OperatingCompany;
            enroll.WaterHeater = enroll.WaterHeater;
            enroll.Quantity = "1";
            enroll.PremiseID = cust.PremiseID;
            enroll.CompanyName = cust.CompanyName;
            enroll.ServiceAddress1 = cust.ServiceAddress1;
            enroll.ServiceAddress2 = cust.ServiceAddress2;
            enroll.ServiceCity = cust.ServiceCity;
            enroll.ServiceState = cust.ServiceState;
            enroll.ServiceZip = cust.ServiceZip;
            enroll.Phone2 = cust.Phone1;
            enroll.RateCode = cust.RateCode;
            enroll.HeaterFuel = enroll.HeaterFuel;
            enroll.RequestedKit = enroll.RequestedKit;
            enroll.FacilityType = enroll.FacilityType;
            enroll.IncomeQualified = enroll.IncomeQualified;
        }

        private string Formatted(Enrollment enrollment)
        {
            switch (enrollment.OkToContact)
            {
                case "TRUE":
                    enrollment.OkToContact = "YES";
                    break;
                case "FALSE":
                    enrollment.OkToContact = "NO";
                    break;
                default: enrollment.OkToContact = "";
                    break;
            }

            var values = new List<string>
                             {
                                 FormattedValue(enrollment.Method),
                                 FormattedValue(enrollment.SubmittedDate.ToString()),
                                 FormattedValue(enrollment.EnrollmentID.ToString()),
                                 FormattedValue(enrollment.AccountNumberResult),
                                 FormattedValue(enrollment.ContactName),
                                 FormattedValue(enrollment.Address1),
                                 FormattedValue(enrollment.Address2),
                                 FormattedValue(enrollment.City),
                                 FormattedValue(enrollment.State),
                                 FormattedValue(enrollment.Zip),
                                 FormattedValue(enrollment.ContactEmail),
                                 FormattedValue(enrollment.Phone1),
                                 FormattedValue(enrollment.Phone2),
                                 FormattedValue(enrollment.OkToContact),
                                 FormattedValue(enrollment.OpCo),
                                 FormattedValue(enrollment.WaterHeater),
                                 FormattedValue("1"),
                                 FormattedValue(enrollment.PremiseID),
                                 FormattedValue(enrollment.CompanyName),
                                 FormattedValue(enrollment.ServiceAddress1),
                                 FormattedValue(enrollment.ServiceAddress2),
                                 FormattedValue(enrollment.ServiceCity),
                                 FormattedValue(enrollment.ServiceState),
                                 FormattedValue(enrollment.ServiceZip),
                                 FormattedValue(enrollment.OldAccountNumber),
                                 FormattedValue(enrollment.FacilityType),
                                 FormattedValue(enrollment.RateCode),
                                 FormattedValue(enrollment.IncomeQualified),
                                 FormattedValue(enrollment.HeaterFuel),
                                 FormattedValue(enrollment.RequestedKit),
                            };

            return String.Join(",", values.ToArray());
        }

        private static string ColumnHeaders()
        {
            var values = new List<string>
                             {
                                 "Method",
                                 "Analysis Date",
                                 "User ID",
                                 "Account Number",
                                 "Name",
                                 "Address 1",
                                 "Address 2",
                                 "City",
                                 "State",
                                 "Zip",
                                 "Email Address",
                                 "Phone 1",
                                 "Phone 2",
                                 "OK to Contact?",
                                 "Operating Company",
                                 "Water Heater Type",
                                 "Quantity",
                                 "Premise ID",
                                 "Company Name",
                                 "Service Address 1",
                                 "Service Address 2",
                                 "Service City",
                                 "Service State",
                                 "Service ZIP",
                                 "Old Account Number",
                                 "Facility Type",
                                 "Rate Code",
                                 "Income Qualified",
                                 "Heater Fuel",
                                 "Requested Kit",
                             };

            return String.Join(",", values.ToArray());
        }

        private static string ColumnExceptionHeaders()
        {
            var values = new List<string>
                             {
                                 "Method",
                                 "Enrollment ID",
                                 "Submitted Date",
                                 "Operating Company",
                                 "Invitation Code",
                                 "Account Number",
                                 "Account Number12",
                                 "Contact First Name",
                                 "Contact Last Name",
                                 "Contact Email",
                                 "Contact Phone",
                                 "Contact ZipCode",
                                 "Ship To Address",
                                 "Address1",
                                 "Address2",
                                 "City",
                                 "State",
                                 "Zip",
                                 "Refferal Source",
                                 "Water Heater",
                                 "Heater Fuel",
                                 "More Info",
                                 "Requested Kit",
                                 "Facility Type",
                                 "Income Qualified"
                             };

            return String.Join(",", values.ToArray());
        }

        private string FormattedException(Enrollment enrollment)
        {
            var values = new List<string>
                             {
                                 FormattedValue(enrollment.Method),
                                 FormattedValue(enrollment.EnrollmentID.ToString()),
                                 FormattedValue(enrollment.SubmittedDate),
                                 FormattedValue(enrollment.OperatingCompany),
                                 FormattedValue(enrollment.InvitationCode),
                                 FormattedValue(enrollment.AccountNumber),
                                 FormattedValue(enrollment.AccountNumber12),
                                 FormattedValue(enrollment.ContactFirstName),
                                 FormattedValue(enrollment.ContactLastName),
                                 FormattedValue(enrollment.ContactEmail),
                                 FormattedValue(enrollment.ContactPhone),
                                 FormattedValue(enrollment.ContactZipCode),
                                 FormattedValue(enrollment.ShipToAddress),
                                 FormattedValue(enrollment.Address1),
                                 FormattedValue(enrollment.Address2),
                                 FormattedValue(enrollment.City),
                                 FormattedValue(enrollment.State),
                                 FormattedValue(enrollment.Zip),
                                 FormattedValue(enrollment.ReferralSource),
                                 FormattedValue(enrollment.WaterHeater),
                                 FormattedValue(enrollment.HeaterFuel),
                                 FormattedValue(enrollment.MoreInfo),
                                 FormattedValue(enrollment.RequestedKit),
                                 FormattedValue(enrollment.FacilityType),
                                 FormattedValue(enrollment.IncomeQualified)
                             };

            return String.Join(",", values.ToArray());
        }

        private static string FormattedValue(string fieldValue)
        {
            if (fieldValue != null)
                return String.Format("\"{0}\"", fieldValue.Replace("\n", " ").Replace("\"", " "));

            return String.Format("\"{0}\"", string.Empty);
        }

        private static string FormattedValue(string fieldValue, int maxLength)
        {
            if (fieldValue != null)
                return String.Format("\"{0}\"",
                                     fieldValue.Replace("\n", " ").Replace("\"", " ").Substring(0,
                                                                                                Math.Min(maxLength,
                                                                                                         fieldValue.
                                                                                                             Length)));

            return String.Format("\"{0}\"", string.Empty);
        }

        private void ReportProgress(string message, int total, int value)
        {
            var handler = ImportProgress;

            if (handler != null)
                handler(this, new ImportExportProgressEventArgs(message, total, value));
        }

        private readonly Regex Matcher = new Regex(Pattern, RegexOptions.Compiled);

        private const string Pattern = "^(\"(?<value>[^\"]*)\"|(?<value>[^,\"]*))?(,(\"(?<value>[^\"]*)\"|(?<value>[^,\"]*)))*$";
    }
}
