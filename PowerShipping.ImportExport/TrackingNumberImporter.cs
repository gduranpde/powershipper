﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.ImportExport
{
	public class TrackingNumberImporter
	{
		//private const int FieldCount = 10;
        private const int FieldCount = 3;

		private readonly string FilePath;
		private readonly string ImportedBy;

		private readonly TrackingNumberBatch Batch;

		public readonly ErrorCollection ImportErrors = new ErrorCollection();

        public event EventHandler<ImportExportProgressEventArgs> ImportProgress;
        
		public TrackingNumberImporter(string filePath, string importedBy)
		{
			FilePath = filePath;
			ImportedBy = importedBy;

			Batch = new TrackingNumberBatch
    		{
    			Id = Guid.NewGuid(),
    			ImportedBy = ImportedBy,
    			ImportedTime = DateTime.Now,
				FileName = Path.GetFileName(FilePath),
				NumbersInBatch = 0,
    			ImportedSuccessfully = 0,
    			ImportedWithException = 0
    		};
		}

		public TrackingNumberBatch Import()
		{
			ReportProgress("Loading file...", 100, 50);
			var lines = ReadLines();
			ReportProgress("File loaded.", 100, 100);

			if (ImportErrors.IsEmpty)
			{
				ReportProgress("Importing Tracking Numbers...", Batch.NumbersInBatch, 0);

				try
				{
					With.Transaction(() => {

						DataProvider.Current.TrackingNumberBatch.Add(Batch);

                        List<TrackingNumberException> exList = new List<TrackingNumberException>();
                       string references = "";
                        // get all ids from file
					    lines.ForEach(l =>
					                      {
					                          var exception = NewTrackingNumberException();
					                          var mapping = EntityMapping.Map(l, exception);
                                              if (mapping.Success)
                                              {
                                                  exList.Add(exception);
                                                  references = references + exception.Reference +",";
                                              }
					                      });

                        if (references.Length > 0)
                            references = references.Remove(references.Length - 1);

					    List<ShipmentDetail> existingList =
					                               DataProvider.Current.ShipmentDetail.GetByShipmentNumbersList(references);

                        exList.ForEach(exTr =>
       	              	{
                            ImportLine(exTr, existingList);

       	              		ReportProgress("Importing Tracking Numbers...",
       	              		               Batch.NumbersInBatch, Batch.ImportedSuccessfully + Batch.ImportedWithException);
       	              	});

						DataProvider.Current.TrackingNumberBatch.Save(Batch);
					    
					});

					ReportProgress("Tracking Numbers imported successfully.",
							Batch.NumbersInBatch, Batch.ImportedSuccessfully + Batch.ImportedWithException);

				}
				catch (Exception ex)
				{
					ImportErrors.Add(ex.Message);
					ReportProgress("Tracking Numbers import failed.",
						Batch.NumbersInBatch, Batch.NumbersInBatch);
				}
			}

			return Batch;
		}

        private void ImportLine(TrackingNumberException exception, List<ShipmentDetail> existingList)
        {
            //var validation = EntityValidation.Validate(exception);

            //ShipmentDetail existingDetail = DataProvider.Current.ShipmentDetail.GetByShipmentNumber(exception.Reference);
            ShipmentDetail existingDetail = existingList.Find(o => o.ShipmentNumber == exception.Reference);

            //if (validation.Success)
            if (existingDetail != null)
            {
                if (string.IsNullOrEmpty(existingDetail.FedExTrackingNumber))
                {
                    existingDetail.FedExBatchId = Batch.Id;
                    existingDetail.FedExTrackingNumber = exception.TrackingNumber;
                    DataProvider.Current.ShipmentDetail.Save(existingDetail);
                    Batch.ImportedSuccessfully++;
                    return;
                }

                exception.ProjectId = existingDetail.ProjectId;
                exception.ShipperId = existingDetail.ShipperId;
                exception.Reason = string.Format("Tracking Number has already been imported for shipment. Current Value: [{0}]",
                                  existingDetail.FedExTrackingNumber);
            }
            else
            {
                exception.Reason = string.Format("Shipment does not exist for Shipment Number [{0}]",
                                                 exception.Reference);
            }
            
            DataProvider.Current.TrackingNumberException.Add(exception);
            Batch.ImportedWithException++;

        }

	    private TrackingNumberException NewTrackingNumberException()
		{
			return new TrackingNumberException
       		{
       			Id = Guid.NewGuid(),
       			BatchId = Batch.Id
       		};
		}

		List<string[]> ReadLines()
		{
			Batch.NumbersInBatch = 0;
			var rows = new List<string[]>();

			try
			{
				var content = File.ReadAllText(FilePath);
				var lines = content.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

				for (var index = 1; index < lines.Length; index++) //Skip first line (index = 0)
				{
					var match = Matcher.Match(lines[index]);

					if (!match.Success)
					{
						ImportErrors.Add("Line {0}. Unexpected file format.", index);
						continue;
					}

					var row = match.Groups["value"].Captures.Cast<Capture>().Select(c => c.Value).ToArray();

					if (row.Length < FieldCount)
					{
						ImportErrors.Add("Line {0}. Expected at least {1} fields in row but was {2}", index, FieldCount, row.Length);
						continue;
					}

					// skip row with all empty values
					if (Array.TrueForAll(row, field => field == String.Empty))
						continue;
					
					rows.Add(row);
				}
			}
			catch (Exception ex)
			{
				ImportErrors.Add(ex.Message);
			}
			finally
			{
				Batch.NumbersInBatch = rows.Count;
			}

			return rows;
		}

		private void ReportProgress(string message, int total, int value)
		{
			var handler = ImportProgress;

			if (handler != null)
				handler(this, new ImportExportProgressEventArgs(message, total, value));
		}

		private readonly Regex Matcher = new Regex(Pattern, RegexOptions.Compiled);

		private const string Pattern = "^(\"(?<value>[^\"]*)\"|(?<value>[^,\"]*))?(,(\"(?<value>[^\"]*)\"|(?<value>[^,\"]*)))*$";


	}
}
