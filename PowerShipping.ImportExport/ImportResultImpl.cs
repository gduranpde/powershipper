namespace PowerShipping.ImportExport
{
	class ImportResultImpl : ImportResult
	{
		public int TotalRecords { get; set; }
		public int ImportedSuccessfully { get; set; }
		public int ImportedWithException { get; set; }

		public int TotalImported
		{
			get { return ImportedSuccessfully + ImportedWithException; }
		}

		public string[] ImportErrors { get; set; }
	}
}