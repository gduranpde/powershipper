﻿using System;
using System.Collections.Generic;
using System.IO;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.ImportExport
{
    public class FedExLabelExporter
    {
        private readonly Guid ShipmentId;
        private readonly string TargetFolder;

        public FedExLabelExporter(Guid shipmentId, string targetFolder)
        {
            ShipmentId = shipmentId;
            TargetFolder = targetFolder;
        }

        public ExportResult Export()
        {
            var result = new ExportResultImpl();

            //bool isExist = IsExistFile(TargetFolder, ShipmentId.ToString());

            Shipment shipment = GetShipmentById(ShipmentId);
            string filePath = Path.Combine(TargetFolder, GenerateFileName(ShipmentId.ToString(), shipment.PurchaseOrderNumber, shipment.ShipperCode));

            try
            {
                List<FedExLabel> labels = GetLabelsByShipment(ShipmentId);

                //if (!isExist)
                //{
                    using (var file = new StreamWriter(filePath, false))
                    {
                        file.WriteLine(ColumnHeaders());
                        labels.ForEach(label => file.WriteLine(Formatted(label)));
                    }
                    result.OutputFilePath = filePath;
                //}
                //else
                //{
                //    result.OutputFilePath = GetFile(TargetFolder, ShipmentId.ToString());
                //}
                
                result.LabelsExported = labels.Count;
            }
            catch (Exception ex)
            {
                result.ExportErrors = new[] {ex.Message};
            }


            return result;
        }

        private static string ColumnHeaders()
        {
            var values = new List<string>
                             {
                                 "RECIPIENT ID",
                                 "CONTACT",
                                 "COMPANY",
                                 "ADDRESS 1",
                                 "ADDRESS 2",
                                 "CITY",
                                 "STATE/PROVINCE",
                                 "COUNTRY CODE",
                                 "ZIP",
                                 "PHONE",
                                 "SERVICE",
                                 "RESIDENTIAL",
                                 "WEIGHT 1",
                                 "WEIGHT 2",
                                 "WEIGHT 3",
                                 "WEIGHT 4",
                                 "WEIGHT 5",
                                 "'REFERENCE'",
                                 "REFERENCE 2",
                                 "REFERENCE 3",
                                 "REFERENCE 4",
                                 "REFERENCE 5",
                                 "'INVOICE #'",
                                 "'PURCHASE ORDER #'",
                                 "'DEPARTMENT NOTES'"
                             };

            return String.Join(",", values.ToArray());
        }

        private static string Formatted(FedExLabel label)
        {
            if (label.RecipientId.Length > 6)
                label.RecipientId = label.RecipientId.Substring(label.RecipientId.Length - 6);
            if (label.Reference.Length > 22)
                label.Reference = label.Reference.Substring(0, 22);

            var values = new List<string>
                             {
                                 FormattedValue(label.RecipientId),
                                 FormattedValue(label.Contact, 35),
                                 FormattedValue(label.Company, 35),
                                 FormattedValue(label.Address1, 35),
                                 FormattedValue(label.Address2, 35),
                                 FormattedValue(label.City, 35),
                                 FormattedValue(label.State, 2),
                                 FormattedValue(label.CountryCode, 2),
                                 FormattedValue(label.Zip, 10),
                                 FormattedValue(label.Phone, 12),
                                 FormattedValue(label.Service, 35),
                                 FormattedValue(label.Residential, 1),
                                 FormattedValue(label.Weight1),
                                 FormattedValue(label.Weight2),
                                 FormattedValue(label.Weight3),
                                 FormattedValue(label.Weight4),
                                 FormattedValue(label.Weight5),
                                 FormattedValue(label.Reference, 35),
                                 FormattedValue(label.Reference2, 35),
                                 FormattedValue(label.Reference3, 35),
                                 FormattedValue(label.Reference4, 35),
                                 FormattedValue(label.Reference5, 35),
                                 FormattedValue(label.InvoiceNumber, 35),
                                 FormattedValue(label.PurchaseOrderNumber, 35),
                                 FormattedValue(label.DepartmentNotes, 25)
                             };

            return String.Join(",", values.ToArray());
        }

        private static string FormattedValue(string fieldValue)
        {
            if (fieldValue != null)
                return String.Format("\"{0}\"", fieldValue.Replace("\n", " ").Replace("\"", " "));

            return String.Format("\"{0}\"", string.Empty);
        }

        private static string FormattedValue(string fieldValue, int maxLength)
        {
            if (fieldValue != null)
                return String.Format("\"{0}\"",
                                     fieldValue.Replace("\n", " ").Replace("\"", " ").Substring(0,
                                                                                                Math.Min(maxLength,
                                                                                                         fieldValue.
                                                                                                             Length)));

            return String.Format("\"{0}\"", string.Empty);
        }

        private static string GenerateFileName(string id, string poNum, string shipper)
        {
            //return String.Format("FedExLabels_{0}_{1}_{2}.csv", poNum, DateTime.Now.ToString("yyyyMMdd"), id);
            return String.Format("{1}_Labels_PO{0}.csv", poNum, shipper);
        }

        private static List<FedExLabel> GetLabelsByShipment(Guid shipmentId)
        {
            return DataProvider.Current.ShipmentDetail.GetFedExLabels(shipmentId);
        }

        private static Shipment GetShipmentById(Guid shipmentId)
        {
            return DataProvider.Current.Shipment.GetOne(shipmentId);
        }

        private static bool IsExistFile(string dir, string id)
        {
            var di = new DirectoryInfo(dir);
            foreach (FileInfo fi in di.GetFiles())
            {
                if (fi.Name.Contains(id))
                {
                    return true;
                }
            }
            return false;
        }

        private static  string GetFile(string dir, string id)
        {
            var di = new DirectoryInfo(dir);
            foreach (FileInfo fi in di.GetFiles())
            {
                if (fi.Name.Contains(id))
                {
                    return fi.FullName;
                }
            }
            return "";
        }
    }
}