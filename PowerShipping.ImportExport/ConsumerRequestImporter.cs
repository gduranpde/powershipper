﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.ImportExport
{
    public class ConsumerRequestImporter
    {
        private const int FieldCount = 18;

        private readonly string FilePath;
        private readonly string ImportedBy;
        private readonly Guid ProjectId;
        private readonly ConsumerRequestBatch Batch;
    
        public readonly ErrorCollection ImportErrors = new ErrorCollection();

        public event EventHandler<ImportExportProgressEventArgs> ImportProgress;

        public ConsumerRequestImporter(string filePath, string importedBy, Guid projectId)
        {
            FilePath = filePath;
            ImportedBy = importedBy;
            ProjectId = projectId;

            string batchLabel = CreateBatchLabel();

            Batch = new ConsumerRequestBatch
            {
                Id = Guid.NewGuid(),
                ImportedBy = ImportedBy,
                ImportedDate = DateTime.Now,
                FileName = Path.GetFileName(FilePath),
                RequestsInBatch = 0,
                ImportedSuccessfully = 0,
                ImportedWithException = 0,
                BatchLabel = batchLabel,
                ProjectId = ProjectId
            };
        }

        public ConsumerRequestBatch Import()
        {
            ReportProgress("Loading file...", 100, 50);
            var lines = ReadLines();
            ReportProgress("File loaded.", 100, 100);

            if (ImportErrors.IsEmpty)
            {
                ReportProgress("Importing Consumer Requests...", Batch.RequestsInBatch, 0);

                try
                {
                    With.Transaction(() =>
                    {
                        DataProvider.Current.ConsumerRequestBatch.Add(Batch);

                        lines.ForEach(l =>
                        {
                            ImportLine(l);

                            ReportProgress("Importing Consumer Requests...",
                                Batch.RequestsInBatch, Batch.ImportedSuccessfully + Batch.ImportedWithException);
                        });

                        DataProvider.Current.ConsumerRequestBatch.Save(Batch);
                    });

                    ReportProgress("Consumer Requests imported successfully.",
                            Batch.RequestsInBatch, Batch.ImportedSuccessfully + Batch.ImportedWithException);
                }
                catch (Exception ex)
                {
                    ImportErrors.Add(ex.Message);
                    ReportProgress("Consumer Requests import failed.",
                        Batch.RequestsInBatch, Batch.RequestsInBatch);
                }
            }

            return Batch;
        }

        private List<string[]> ReadLines()
        {
            Batch.RequestsInBatch = 0;
            var rows = new List<string[]>();

            try
            {
                var content = File.ReadAllText(FilePath);

                var lines = content.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                for (var index = 1; index < lines.Length; index++) //Skip first line (index = 0)
                {
                    var match = Matcher.Match(lines[index]);

                    if (!match.Success)
                    {
                        ImportErrors.Add("Line {0}. Unexpected file format.", index);
                       // labelErrors.ForeColor = Color.Red;
                       // labelErrors.Text = " Bad Record encountered at row {0} of Import file. File could not be processed" + index;
                        continue;
                    }

                    var row = match.Groups["value"].Captures.Cast<Capture>().Select(c => c.Value).ToArray();

                    if (row.Length < FieldCount)
                    {
                        ImportErrors.Add("Line {0}. Expected at least {1} values in row but was {2}", index, FieldCount, row.Length);
                        continue;
                    }

                    // skip row with all empty values
                    if (Array.TrueForAll(row, field => field == String.Empty))
                        continue;

                    rows.Add(row);
                }
            }
            catch (Exception ex)
            {
                ImportErrors.Add(ex.Message);
            }
            finally
            {
                Batch.RequestsInBatch = rows.Count;
            }

            return rows;
        }

        private string CreateBatchLabel()
        {
            string batchLabel;
            string prevBatchLabel = DataProvider.Current.ConsumerRequestBatch.GetPreviousBatchLabel(Convert.ToDateTime(DateTime.Now.ToShortDateString()));
            if (!string.IsNullOrEmpty(prevBatchLabel))
            {
                int number = Convert.ToInt32(prevBatchLabel.Split('-')[3]);
                number++;
                batchLabel = DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Year + "-" + number;
            }
            else
            {
                batchLabel = DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Year + "-1";
            }
            return batchLabel;
        }

        private void ImportLine(string[] columns)
        {
            var exception = NewConsumerRequestException();
            var mapping = EntityMapping.Map(columns, exception);

            if (mapping.Success)
            {
                var request = NewConsumerRequest();

                exception.States = DataProvider.Current.State.GetByProject(ProjectId);

                var validation = EntityValidation.MapAndValidate(exception, request);

                if (validation.Success)
                {
                    request.ProjectId = ProjectId;
                    DataProvider.Current.ConsumerRequest.Save(request);
                    Batch.ImportedSuccessfully++;
                    return;
                }
                exception.ProjectId = ProjectId;
                DataProvider.Current.ConsumerRequestException.Add(exception);
                Batch.ImportedWithException++;
            }
        }

        private ConsumerRequestException NewConsumerRequestException()
        {
            return new ConsumerRequestException
            {
                Id = Guid.NewGuid(),
                BatchId = Batch.Id,
                ProjectId = ProjectId
            };
        }

        private ConsumerRequest NewConsumerRequest()
        {
            return new ConsumerRequest
                       {
                           Id = Guid.NewGuid(),
                           BatchId = Batch.Id,
                           ChangedBy = Batch.ImportedBy,
                           ProjectId = ProjectId,
                           CRImportedDate = Batch.ImportedDate
                       };
        }

        private void ReportProgress(string message, int total, int value)
        {
            var handler = ImportProgress;

            if (handler != null)
                handler(this, new ImportExportProgressEventArgs(message, total, value));
        }

        private readonly Regex Matcher = new Regex(Pattern, RegexOptions.Compiled);

        private const string Pattern = "^(\"(?<value>[^\"]*)\"|(?<value>[^,\"]*))?(,(\"(?<value>[^\"]*)\"|(?<value>[^,\"]*)))*$";

    }
}