namespace PowerShipping.ImportExport
{
	class ExportResultImpl : ExportResult
	{
		public string OutputFilePath { get; set; }
		public string[] ExportErrors { get; set; }
		public int LabelsExported { get; set; }
	}
}