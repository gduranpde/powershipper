﻿using System;
using System.IO;
using PowerShipping.Entities;

namespace PowerShipping.ImportExport
{
	public class DataExchange
	{
		public static DataExchange Current = new DataExchange();

        public ConsumerRequestBatch ImportConsumerRequestsFromCsv(string filePath, string importedBy, Guid projectId)
		{
			return new ConsumerRequestImporter(filePath, importedBy, projectId).Import();
		}

		public OptOutBatch ImportOptOutsFromCsv(string path, string importer)
		{
			var batch = NewOptOutBatch(path, importer);
			//import data here
			return batch;
		}

		public ExportResult ExportFedExLabels(Guid shipmentId, string targetFolder)
		{
			return new FedExLabelExporter(shipmentId, targetFolder).Export();
		}

		static OptOutBatch NewOptOutBatch(string path, string importer)
		{
			return new OptOutBatch
			{
				Id = Guid.NewGuid(),
				ImportedBy = importer,
				ImportedDate = DateTime.Now,
				FileName = Path.GetFileName(path),
				OptOutsInBatch = 0,
				ImportedSuccessfully = 0,
				ImportedWithException = 0
			};
		}
	}
}
