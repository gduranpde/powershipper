﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.ImportExport
{
    public class ReshipImporter
    {
        private const int FieldCount = 3;
        private int sortOrder = 0;

        private readonly string FilePath;
        private readonly string ImportedBy;
        private readonly Guid ProjectId;
        private readonly ReshipImportBatch Batch;

        DataTable dtSubgroup = new DataTable();

        public readonly ErrorCollection ImportErrors = new ErrorCollection();

        public event EventHandler<ImportExportProgressEventArgs> ImportProgress;

        public ReshipImporter(string filePath, string importedBy)//, Guid projectId
        {
            FilePath = filePath;
            ImportedBy = importedBy;

            Batch = new ReshipImportBatch
            {
                ReshipImportBatchID = Guid.NewGuid(),
                ImportedBy = ImportedBy,
                ImportedTime = DateTime.Now,
                SourceFileName = Path.GetFileName(FilePath),
                NumberOfRecords = 0,
                ImportedSuccessfully = 0,
                NumberWithExceptions = 0
            };
        }

        public ReshipImportBatch Import(DateTime ReshipDate, Guid ShipperID, string path)
        {
            ReportProgress("Loading file...", 100, 50);
            var lines = ReadLines();

            ReportProgress("File loaded.", 100, 100);

            if (ImportErrors.IsEmpty)
            {
                ReportProgress("Importing Consumer Requests...", Batch.NumberOfRecords, 0);

                try
                {
                    With.Transaction(() =>
                    {
                        Batch.ShipperID = ShipperID;
                        Batch.SourceFilePath = path;
                        Batch.NumberOfRecords = lines.Count;

                        DataProvider.Current.ReshipImportBatch.Add(Batch);

                        List<string[]> arrSubGroups = GetImportBatchSubGroupsSave(lines, ReshipDate, ShipperID);

                        arrSubGroups.ForEach(l =>
                            {
                                sortOrder = ++sortOrder;
                                ImportLine(l);

                                ReportProgress("Importing Consumer Requests...",
                                    Batch.NumberOfRecords, Batch.ImportedSuccessfully + Batch.NumberWithExceptions);
                            });

                        DataProvider.Current.ReshipImportBatch.Save(Batch);
                    });

                    ReportProgress("Consumer Requests imported successfully.",
                            Batch.NumberOfRecords, Batch.ImportedSuccessfully + Batch.NumberWithExceptions);
                }
                catch (Exception ex)
                {
                    ImportErrors.Add(ex.Message);
                    ReportProgress("Consumer Requests import failed.",
                        Batch.NumberOfRecords, Batch.NumberOfRecords);
                }
            }

            return Batch;
        }

        private List<string[]> ReadLines()
        {
            Batch.NumberOfRecords = 0;
            var rows = new List<string[]>();

            try
            {
                var content = File.ReadAllText(FilePath);

                var lines = content.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                for (var index = 1; index < lines.Length; index++) //Skip first line (index = 0)
                {
                    var match = Matcher.Match(lines[index]);

                    if (!match.Success)
                    {
                        ImportErrors.Add("Line {0}. Unexpected file format.", index);
                        continue;
                    }

                    var row = match.Groups["value"].Captures.Cast<Capture>().Select(c => c.Value).ToArray();

                    if (row.Length < FieldCount)
                    {
                        ImportErrors.Add("Line {0}. Expected at least {1} values in row but was {2}", index, FieldCount, row.Length);
                        continue;
                    }

                    // skip row with all empty values
                    if (Array.TrueForAll(row, field => field == String.Empty))
                        continue;

                    rows.Add(row);
                }
            }
            catch (Exception ex)
            {
                ImportErrors.Add(ex.Message);
            }
            finally
            {
                Batch.NumberOfRecords = rows.Count;
            }

            return rows;
        }

        private void ImportLine(string[] columns)
        {
            string[] a = columns;

            string ShipmentNum = Convert.ToString(columns[1]);

            var exception = NewReshipImportException();
            var mapping = EntityMapping.Map(columns, exception);

            if (mapping.Success)
            {
                var request = NewShipmentdetail();

                // exception.States = DataProvider.Current.State.GetByProject(ProjectId);

                var validation = EntityValidation.MapAndValidate(exception, request);

                if (validation.Success)
                {
                    request.ShipperId = Batch.ShipperID;
                    request.SortOrder = sortOrder;
                    request.FedExBatchId = Batch.ReshipImportBatchID;

                    DataProvider.Current.ShipmentDetail.Add(request);

                    //----- Update Consumer Request -------//

                    Guid crID = new Guid(request.ConsumerRequestId.ToString());
                    ConsumerRequest CR = DataProvider.Current.ConsumerRequest.GetOne(crID);

                    CR.IsReship = true;
                    CR.Status = ConsumerRequestStatus.PendingShipment;
                    DataProvider.Current.ConsumerRequest.Save(CR);

                    //------- Update ShipmentDetail.ReshipStatus ---------//

                    ShipmentDetail SD = DataProvider.Current.ShipmentDetail.GetByShipmentNumber(ShipmentNum);

                    SD.ReshipStatus = ShipmentReshipStatus.Reshipped;
                    DataProvider.Current.ShipmentDetail.Save(SD);

                    //---------------------------------------------------//

                    Batch.ImportedSuccessfully++;
                    return;
                }
                //exception.ProjectId = ProjectId;
                DataProvider.Current.ReshipImportException.Add(exception);
                Batch.NumberWithExceptions++;
            }
        }

        private readonly Regex Matcher = new Regex(Pattern, RegexOptions.Compiled);

        private const string Pattern = "^(\"(?<value>[^\"]*)\"|(?<value>[^,\"]*))?(,(\"(?<value>[^\"]*)\"|(?<value>[^,\"]*)))*$";

        private List<string[]> GetImportBatchSubGroupsSave(List<string[]> Records, DateTime ReshipDate, Guid shipperID)
        {
            dtSubgroup.Columns.Add("AccountNumber", typeof(string));
            dtSubgroup.Columns.Add("ShipmentNumber", typeof(string));
            dtSubgroup.Columns.Add("TrakkingNumber", typeof(string));
            dtSubgroup.Columns.Add("KitType", typeof(string));
            dtSubgroup.Columns.Add("Project", typeof(string));
            dtSubgroup.Columns.Add("ShipmentID", typeof(string));

            for (int i = 0; i < Records.Count; i++)
            {
                DataRow dr = dtSubgroup.NewRow();

                string[] r1 = Records[i];

                dr[0] = r1[0];
                dr[1] = r1[1];
                dr[2] = r1[2];

                DataTable dtTemp = DataProvider.Current.ReshipImportBatch.GetKitType_ProjectByShipmentNumber(Convert.ToInt32(r1[1]));

                if (dtTemp.Rows.Count > 0)
                {
                    dr[3] = dtTemp.Rows[0][0];
                    dr[4] = dtTemp.Rows[0][1];
                }

                dtSubgroup.Rows.Add(dr);
            }

            DataView view = new DataView(dtSubgroup);

            DataTable distValues = view.ToTable(true, "KitType", "Project");
            distValues.Columns.Add("ShipmentID", typeof(string));

            DataTable dtShipmentNumber = DataProvider.Current.ReshipImportBatch.GetReshipImportMaxShipmentNumber();

            int MaxShipmentNumber = 0;
            if (dtShipmentNumber.Rows.Count > 0)
            {
                MaxShipmentNumber = Convert.ToInt32(dtShipmentNumber.Rows[0][0]);
            }

            for (int i = 0; i < distValues.Rows.Count; i++)
            {
                Shipment objShipment = new Shipment();
                objShipment.Id = Guid.NewGuid();
                distValues.Rows[i]["ShipmentID"] = objShipment.Id;

                objShipment.KitTypeId = new Guid(distValues.Rows[i]["KitType"].ToString());
                objShipment.PurchaseOrderNumber = "Reship-" + MaxShipmentNumber + "-" + (i + 1);
                objShipment.DateLabelsNeeded = ReshipDate;
                objShipment.DateCustomerWillShip = ReshipDate;
                objShipment.CreatedBy = Batch.ImportedBy;
                objShipment.CreatedTime = DateTime.Now;
                objShipment.ProjectId = new Guid(distValues.Rows[i]["Project"].ToString());
                objShipment.ShipperId = shipperID;

                DataProvider.Current.Shipment.Add(objShipment);
            }

            for (int i = 0; i < dtSubgroup.Rows.Count; i++) // Assign Shipment ID to each record
            {
                string kitType1 = dtSubgroup.Rows[i]["kitType"].ToString();
                string Project1 = dtSubgroup.Rows[i]["Project"].ToString();

                DataRow[] dr = distValues.Select("KitType='" + kitType1 + "' AND Project='" + Project1 + "'");

                dtSubgroup.Rows[i]["ShipmentID"] = dr[0].ItemArray[2];
            }

            //------------------------------------//

            List<string[]> strDetailIDList = new List<string[]>(); // Converting to List of String Array

            foreach (DataRow row in dtSubgroup.Rows)
            {
                var stringArray = row.ItemArray.Cast<string>().ToArray();

                strDetailIDList.Add(stringArray);
            }

            return strDetailIDList;
        }

        private ShipmentDetail NewShipmentdetail()
        {
            return new ShipmentDetail
                {
                    Id = Guid.NewGuid(),
                    HasException = false,
                    IsReship = true,
                    ShipDate = null,
                    ReshipStatus = ShipmentReshipStatus.NA,
                    ShippingStatus = ShipmentShippingStatus.Pending,
                    ShippingStatusDate = DateTime.Now,
                    FedExStatusDate = null,
                    FedExStatusCode = null,
                    FedExStatusHistory = null,
                    Notes = null,
                    FedExLastUpdateStatus = null,
                    ReasonId = null
                };
        }

        private ReshipImportException NewReshipImportException()
        {
            return new ReshipImportException
            {
                ReshipExceptionImportID = Guid.NewGuid(),
                ReshipImportBatchID = Batch.ReshipImportBatchID
            };
        }

        private void ReportProgress(string message, int total, int value)
        {
            var handler = ImportProgress;

            if (handler != null)
                handler(this, new ImportExportProgressEventArgs(message, total, value));
        }
    }
}