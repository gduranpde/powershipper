namespace PowerShipping.ImportExport
{
	public interface ExportResult
	{
		string OutputFilePath { get; }
		string[] ExportErrors { get; }
		int LabelsExported { get; }
	}
}