namespace PowerShipping.ImportExport
{
	public class ValidationResult
	{
		public bool Success
		{
			get { return Errors.Count == 0; }
		}

		public readonly ErrorCollection Errors = new ErrorCollection();
	}
}