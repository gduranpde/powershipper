﻿using System;
using System.Collections.Generic;
using System.Linq;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.ImportExport
{
    public static class EntityValidation
    {
        public static ValidationResult Validate(OptOutException exception)
        {
            return Validate(exception, true);
        }

        public static ValidationResult Validate(TrackingNumberException exception)
        {
            return Validate(exception, true);
        }

        public static ValidationResult Validate(ConsumerRequestException exception)
        {
            return Validate(exception, true);
        }

        public static ValidationResult Validate(OptOutException exception, bool overrideReason)
        {
            return MapAndValidate(exception, new OptOut(), overrideReason);
        }

        public static ValidationResult Validate(TrackingNumberException exception, bool overrideReason)
        {
            return MapAndValidate(exception, new ShipmentDetail(), overrideReason);
        }

        public static ValidationResult Validate(ConsumerRequestException exception, bool overrideReason)
        {
            return MapAndValidate(exception, new ConsumerRequest(), overrideReason);
        }

        public static ValidationResult MapAndValidate(OptOutException exception, OptOut optout)
        {
            return MapAndValidate(exception, optout, true);
        }

        public static ValidationResult MapAndValidate(TrackingNumberException exception, ShipmentDetail detail)
        {
            return MapAndValidate(exception, detail, true);
        }

        public static ValidationResult MapAndValidate(ConsumerRequestException exception, ConsumerRequest request)
        {
            return MapAndValidate(exception, request, true);
        }

		public static ValidationResult MapAndValidate(CustomerException exception, Customer customer)
		{
			return MapAndValidate(exception, customer, true);
		}

        public static ValidationResult MapAndValidate(ReshipImportException exception, ShipmentDetail request)
        {
            return MapAndValidate(exception, request, true);
        }

        public static ValidationResult MapAndValidate(OptOutException exception, OptOut optout, bool overrideReason)
        {
            var result = new ValidationResult();

            MapAndValidate(exception, optout, result.Errors);

            if (overrideReason)
                exception.Reason = result.Errors.ToString();

            return result;
        }

        public static ValidationResult MapAndValidate(TrackingNumberException exception, ShipmentDetail detail, bool overrideReason)
        {
            var result = new ValidationResult();

            MapAndValidate(exception, detail, result.Errors);

            if (overrideReason)
                exception.Reason = result.Errors.ToString();

            return result;
        }

        public static ValidationResult MapAndValidate(ConsumerRequestException exception, ConsumerRequest request, bool overrideReason)
        {
            var result = new ValidationResult();

            MapAndValidate(exception, request, result.Errors);

            if (overrideReason)
                exception.Reason = result.Errors.ToString();

            return result;
        }

		public static ValidationResult MapAndValidate(CustomerException exception, Customer customer, bool overrideReason)
		{
			var result = new ValidationResult();

			MapAndValidate(exception, customer, result.Errors);

			if (overrideReason)
				exception.Reason = result.Errors.ToString();

			return result;
		}

        public static ValidationResult MapAndValidate(ReshipImportException exception, ShipmentDetail request, bool overrideReason)
        {
            var result = new ValidationResult();

            MapAndValidate(exception, request, result.Errors);

            if (overrideReason)
                exception.ReasonForException = result.Errors.ToString();

            return result;
        }

        public static ValidationResult MapAndValidate(ConsumerRequestException exception, ConsumerRequest request, bool overrideReason, bool skipRules)
        {
            var result = new ValidationResult();

            MapAndValidate(exception, request, result.Errors, skipRules);

            if (overrideReason)
                exception.Reason = result.Errors.ToString();

            return result;
        }

        private static bool MapAndValidate(OptOutException exception, OptOut optout, ErrorCollection errors)
        {
            var mapping = EntityMapping.Map(exception, optout);

            if (!mapping.Success)
            {
                mapping.Errors.ForEach(errors.Add);
                return false;
            }

            var validation = ValidateInDatabase(optout);

            if (!validation.Success)
            {
                validation.Errors.ForEach(errors.Add);
                return false;
            }

            return true;
        }

        private static bool MapAndValidate(TrackingNumberException exception, ShipmentDetail detail, ErrorCollection errors)
        {
            var mapping = EntityMapping.Map(exception, detail);

            if (!mapping.Success)
            {
                mapping.Errors.ForEach(errors.Add);
                return false;
            }

            var validation = ValidateInDatabase(exception);

            if (!validation.Success)
            {
                validation.Errors.ForEach(errors.Add);
                return false;
            }

            return true;
        }

        //file import
        private static bool MapAndValidate(ConsumerRequestException exception, ConsumerRequest request, ErrorCollection errors)
        {
            var mapping = EntityMapping.Map(exception, request);

            if (!mapping.Success)
            {
                mapping.Errors.ForEach(errors.Add);
                return false;
            }

            var validation = ValidateInDatabase(request);

            if (!validation.Success)
            {
                validation.Errors.ForEach(errors.Add);
                return false;
            }

            return true;
        }

		private static bool MapAndValidate(CustomerException exception, Customer customer, ErrorCollection errors)
		{
			var mapping = EntityMapping.Map(exception, customer);

			if (!mapping.Success)
			{
				mapping.Errors.ForEach(errors.Add);
				return false;
			}

			return true;
		}

        private static bool MapAndValidate(ReshipImportException exception, ShipmentDetail request, ErrorCollection errors)
        {
            var mapping = EntityMapping.Map(exception, request);

            if (!mapping.Success)
            {
                mapping.Errors.ForEach(errors.Add);
                return false;
            }

            var validation = ValidateInDatabase(request);

            if (!validation.Success)
            {
                validation.Errors.ForEach(errors.Add);
                return false;
            }

            return true;
        }

        private static bool MapAndValidate(ConsumerRequestException exception, ConsumerRequest request, ErrorCollection errors, bool skipRules)
        {
            exception.SkipRules = skipRules;
            var mapping = EntityMapping.Map(exception, request);

            if (!mapping.Success)
            {
                mapping.Errors.ForEach(errors.Add);
                return false;
            }

            return true;
        }

        public static ValidationResult Validate(OptOut optout)
        {
            var result = new ValidationResult();

            var accountNumberAsString = ReplaceNullAndTrim(optout.AccountNumber);
            if (String.IsNullOrEmpty(accountNumberAsString))
                result.Errors.Add("Account Number should not be empty.");

            var accountNameAsUpperString = ReplaceNullAndTrim(optout.AccountName).ToUpper();
            if (String.IsNullOrEmpty(accountNameAsUpperString))
                result.Errors.Add("Account Name should not be empty.");

            var addressAsUpperString = ReplaceNullAndTrim(optout.Address).ToUpper();
            if (String.IsNullOrEmpty(addressAsUpperString))
                result.Errors.Add("Address should not be empty.");

            var cityAsUpperString = ReplaceNullAndTrim(optout.City).ToUpper();
            if (String.IsNullOrEmpty(cityAsUpperString))
                result.Errors.Add("City should not be empty.");

            var stateAsUpperString = ReplaceNullAndTrim(optout.State).ToUpper();
            if (String.IsNullOrEmpty(stateAsUpperString))
                result.Errors.Add("State should not be empty.");
            //else if (stateAsUpperString != "PA")
            //    result.Errors.Add("State is not 'PA'. Current value: [{0}]", stateAsUpperString);

            var zipCodeAsString = ReplaceNullAndTrim(optout.ZipCode);
            var zipCodeDigits = zipCodeAsString.Where(Char.IsDigit).ToArray();
            if (zipCodeAsString.Length < 5 || zipCodeDigits.Length < 5)
                result.Errors.Add("Zip Code should contain at least 5 digits.");

            var phone1AsString = ReplaceNullAndTrim(optout.Phone1);
            var phone1Digits = phone1AsString.Where(Char.IsDigit).ToArray();
            if (phone1AsString.Length == 0 && phone1Digits.Length == 0 || phone1AsString.Length == 10 && phone1Digits.Length == 10) { }
            else
                result.Errors.Add("Phone1 should either be empty or contain 10 digits");

            var phone2AsString = ReplaceNullAndTrim(optout.Phone2);
            var phone2Digits = phone2AsString.Where(Char.IsDigit).ToArray();
            if (phone2AsString.Length == 0 && phone2Digits.Length == 0 || phone2AsString.Length == 10 && phone2Digits.Length == 10) { }
            else
                result.Errors.Add("Phone2 should either be empty or contain 10 digits");

            var existingOptOut = DataProvider.Current.OptOut.GetByAccountNumber(optout.AccountNumber, optout.ProjectId);
            if (existingOptOut != null && existingOptOut.Id != optout.Id)
                result.Errors.Add("Opt Out with Account Number [{0}] already exists", optout.AccountNumber);

            return result;
        }

        public static ValidationResult Validate(ConsumerRequest request)
        {
            var result = new ValidationResult();

            var accountNumberAsString = ReplaceNullAndTrim(request.AccountNumber);
            if (String.IsNullOrEmpty(accountNumberAsString))
                result.Errors.Add("Account Number should not be empty.");

            var accountNameAsUpperString = ReplaceNullAndTrim(request.AccountName).ToUpper();
            if (String.IsNullOrEmpty(accountNameAsUpperString))
                result.Errors.Add("Account Name should not be empty.");

            var address1AsUpperString = ReplaceNullAndTrim(request.Address1).ToUpper();
            if (String.IsNullOrEmpty(address1AsUpperString))
                result.Errors.Add("Address1 should not be empty.");
            else if (address1AsUpperString.Length > 35)
                result.Errors.Add("Address1 should not be more than 35 symbols.");

            var cityAsUpperString = ReplaceNullAndTrim(request.City).ToUpper();
            if (String.IsNullOrEmpty(cityAsUpperString))
                result.Errors.Add("City should not be empty.");

            var stateAsUpperString = ReplaceNullAndTrim(request.State).ToUpper();
            if (String.IsNullOrEmpty(stateAsUpperString))
                result.Errors.Add("State should not be empty.");
            else if (request.OutOfState == false)
            {
                //if (!request.OutOfState)
                //    if (stateAsUpperString != "PA")
                //        result.Errors.Add("State is not 'PA'. Current value: [{0}]", stateAsUpperString);
                bool contains = false;
                foreach (State s in request.States)
                {
                    if (s.Code == stateAsUpperString)
                    {
                        contains = true;
                        break;
                    }
                }
                if (!contains)
                    result.Errors.Add("State is not in available States for this Project'. Current value: [{0}]", stateAsUpperString);
            }

            var zipCodeAsString = ReplaceNullAndTrim(request.ZipCode);
            var zipCodeDigits = zipCodeAsString.Where(Char.IsDigit).ToArray();
            if (zipCodeAsString.Length < 5 || zipCodeDigits.Length < 5)
                result.Errors.Add("Zip Code should contain at least 5 digits.");

            var phone1AsString = ReplaceNullAndTrim(request.Phone1);
            var phone1Digits = phone1AsString.Where(Char.IsDigit).ToArray();
            if (phone1AsString.Length == 0 && phone1Digits.Length == 0 || phone1AsString.Length == 10 && phone1Digits.Length == 10) { }
            else
                result.Errors.Add("Phone1 should either be empty or contain 10 digits");

            var phone2AsString = ReplaceNullAndTrim(request.Phone2);
            var phone2Digits = phone2AsString.Where(Char.IsDigit).ToArray();
            if (phone2AsString.Length == 0 && phone2Digits.Length == 0 || phone2AsString.Length == 10 && phone2Digits.Length == 10) { }
            else
                result.Errors.Add("Phone2 should either be empty or contain 10 digits");

            Project pNew = DataProvider.Current.Project.GetOne(request.ProjectId);

            var existingRequest = DataProvider.Current.ConsumerRequest.GetByAccountNumber(request.AccountNumber, request.ProjectId);
            if ((pNew.IsAllowDupsInProject == false) && (existingRequest != null && existingRequest.Id != request.Id))
                result.Errors.Add("Consumer Request with Account Number [{0}] already exists", request.AccountNumber);

            return result; // TEST1
        }
        //cr
        public static ValidationResult Validate(ConsumerRequest request, bool lightValidation)
        {
            var result = new ValidationResult();

            if (!lightValidation)
            {
                var accountNumberAsString = ReplaceNullAndTrim(request.AccountNumber);
                if (String.IsNullOrEmpty(accountNumberAsString))
                    result.Errors.Add("Account Number should not be empty.");

                var accountNameAsUpperString = ReplaceNullAndTrim(request.AccountName).ToUpper();
                if (String.IsNullOrEmpty(accountNameAsUpperString))
                    result.Errors.Add("Account Name should not be empty.");

                var address1AsUpperString = ReplaceNullAndTrim(request.Address1).ToUpper();
                if (String.IsNullOrEmpty(address1AsUpperString))
                    result.Errors.Add("Address1 should not be empty.");
                else if (address1AsUpperString.Length > 35)
                    result.Errors.Add("Address1 should not be more than 35 symbols.");

                var cityAsUpperString = ReplaceNullAndTrim(request.City).ToUpper();
                if (String.IsNullOrEmpty(cityAsUpperString))
                    result.Errors.Add("City should not be empty.");

                var stateAsUpperString = ReplaceNullAndTrim(request.State).ToUpper();
                if (String.IsNullOrEmpty(stateAsUpperString))
                    result.Errors.Add("State should not be empty.");
                else if (request.OutOfState == false)
                {
                    //if (!request.OutOfState)
                    //    if (stateAsUpperString != "PA")
                    //        result.Errors.Add("State is not 'PA'. Current value: [{0}]", stateAsUpperString);
                    bool contains = false;
                    foreach (State s in request.States)
                    {
                        if (s.Code == stateAsUpperString)
                        {
                            contains = true;
                            break;
                        }
                    }
                    if (!contains)
                        result.Errors.Add("State is not in available States for this Project'. Current value: [{0}]", stateAsUpperString);
                }

                var zipCodeAsString = ReplaceNullAndTrim(request.ZipCode);
                var zipCodeDigits = zipCodeAsString.Where(Char.IsDigit).ToArray();
                if (zipCodeAsString.Length < 5 || zipCodeDigits.Length < 5)
                    result.Errors.Add("Zip Code should contain at least 5 digits.");
            }

            var phone1AsString = ReplaceNullAndTrim(request.Phone1);
            var phone1Digits = phone1AsString.Where(Char.IsDigit).ToArray();
            if (phone1AsString.Length == 0 && phone1Digits.Length == 0 || phone1AsString.Length == 10 && phone1Digits.Length == 10) { }
            else
                result.Errors.Add("Phone1 should either be empty or contain 10 digits");

            var phone2AsString = ReplaceNullAndTrim(request.Phone2);
            var phone2Digits = phone2AsString.Where(Char.IsDigit).ToArray();
            if (phone2AsString.Length == 0 && phone2Digits.Length == 0 || phone2AsString.Length == 10 && phone2Digits.Length == 10) { }
            else
                result.Errors.Add("Phone2 should either be empty or contain 10 digits");

            //Project pNew = DataProvider.Current.Project.GetOne(request.ProjectId);
            //List<Guid> exceptionsIds = DataProvider.Current.ProjectDuplicateExceptions.GetAllExceptionsByProjectId(pNew.Id);
            //bool wasFound = false;
            //foreach (Guid id in exceptionsIds)
            //{
            //    List<string> exceptionAccNo = DataProvider.Current.ConsumerRequest.GetAccountNumberById(id);
            //    if (exceptionAccNo.Contains(request.AccountNumber))
            //    {
            //        wasFound = true;
            //        break;
            //    }
            //}

            //List<ConsumerRequest> allConsumerRequests = DataProvider.Current.ConsumerRequest.GetAllByAccountNumber(request.AccountNumber);
            //int countAllConsumerRequests = allConsumerRequests.Count();
            //var existingRequest = DataProvider.Current.ConsumerRequest.GetByAccountNumber(request.AccountNumber, request.ProjectId);
            //if (!(wasFound && pNew.IsAllowDupsInProject == false))
            //{
            //    if ((countAllConsumerRequests > 0) || ((pNew.IsAllowDupsInProject == false) && (existingRequest != null && existingRequest.Id != request.Id)))
            //        result.Errors.Add("Consumer Request with Account Number [{0}] already exists", request.AccountNumber);
            //}
            //return result;

            Project currentProject = DataProvider.Current.Project.GetOne(request.ProjectId);
            if (currentProject.IsAllowDuplicates)
            {
                return result;
            }
            else
            {
                if (currentProject.DuplicateExceptions == false)
                {
                    //check if account number exists in ConsumerRequest table
                    var count = DataProvider.Current.ConsumerRequest.CheckExistsAccountNumberInAllProjects(request.AccountNumber);//CheckExistsAccountNumberInProject(request.ProjectId, request.AccountNumber);

                    if (count > 0)
                    {
                        result.Errors.Add("Project doesn't allow duplicates");
                        return result;
                    }                   
                }
                else
                {
                    // check if the account number is in one of the projects of the exceptions list
                    //take exception projects
                    List<Guid> exceptionsIds = DataProvider.Current.ProjectDuplicateExceptions.GetAllExceptionsByProjectId(currentProject.Id);

                    //check if the the account number is in one of the projects
                    foreach (var projectId in exceptionsIds)
                    {
                        var count = DataProvider.Current.ConsumerRequest.CheckExistsAccountNumberInProject(projectId, request.AccountNumber);
                        var countAll = DataProvider.Current.ConsumerRequest.CheckExistsAccountNumberInAllProjects(request.AccountNumber);

                        // ok , account exists in exceptions projects list
                        if (countAll == count)
                        {
                            if (count == 1)
                            {
                                return result;
                            }
                            else if (count > 1)
                            {
                                result.Errors.Add("Consumer Request with Account Number [{0}] already exists in the exception project", request.AccountNumber);
                            }
                        }
                        else
                        {
                            result.Errors.Add("Consumer Request with Account Number [{0}] already exists in other projects that doesn't allow duplicates", request.AccountNumber);
                        }
                    }
                }

                return result;
            }
        }

        public static ValidationResult Validate(ConsumerRequestException request, bool overiderReason, bool lightVaidation)
        {
            var result = new ValidationResult();

            var phone1AsString = ReplaceNullAndTrim(request.Phone1);
            var phone1Digits = phone1AsString.Where(Char.IsDigit).ToArray();
            if (phone1AsString.Length == 0 && phone1Digits.Length == 0 || phone1AsString.Length == 10 && phone1Digits.Length == 10) { }
            else
                result.Errors.Add("Phone1 should either be empty or contain 10 digits");

            var phone2AsString = ReplaceNullAndTrim(request.Phone2);
            var phone2Digits = phone2AsString.Where(Char.IsDigit).ToArray();
            if (phone2AsString.Length == 0 && phone2Digits.Length == 0 || phone2AsString.Length == 10 && phone2Digits.Length == 10) { }
            else
                result.Errors.Add("Phone2 should either be empty or contain 10 digits");

            //Project pNew = DataProvider.Current.Project.GetOne(request.ProjectId);

            //var existingRequest = DataProvider.Current.ConsumerRequest.GetByAccountNumber(request.AccountNumber, request.ProjectId);
            //if ((pNew.IsAllowDupsInProject == false) && (existingRequest != null && existingRequest.Id != request.Id))
            //    result.Errors.Add("Consumer Request with Account Number [{0}] already exists", request.AccountNumber);

            Project currentProject = DataProvider.Current.Project.GetOne(request.ProjectId);
            if (currentProject.IsAllowDuplicates)
            {
                return result;
            }
            else
            {
                if (currentProject.DuplicateExceptions == false)
                {
                    //check if account number exists in ConsumerRequest table
                    var count = DataProvider.Current.ConsumerRequest.CheckExistsAccountNumberInAllProjects(request.AccountNumber);//CheckExistsAccountNumberInProject(request.ProjectId, request.AccountNumber);

                    if (count > 0)
                    {
                        result.Errors.Add("Project doesn't allow duplicates");
                        return result;
                    }
                }
                else
                {
                    // check if the account number is in one of the projects of the exceptions list
                    //take exception projects
                    List<Guid> exceptionsIds = DataProvider.Current.ProjectDuplicateExceptions.GetAllExceptionsByProjectId(currentProject.Id);

                    //check if the the account number is in one of the projects
                    foreach (var projectId in exceptionsIds)
                    {
                        var count = DataProvider.Current.ConsumerRequest.CheckExistsAccountNumberInProject(projectId, request.AccountNumber);
                        var countAll = DataProvider.Current.ConsumerRequest.CheckExistsAccountNumberInAllProjects(request.AccountNumber);

                        // ok , account exists in exceptions projects list
                        if (countAll == count)
                        {
                            if (count == 1)
                            {
                                return result;
                            }
                            else if (count > 1)
                            {
                                result.Errors.Add("Consumer Request with Account Number [{0}] already exists in the exception project", request.AccountNumber);
                            }
                        }
                        else
                        {
                            result.Errors.Add("Consumer Request with Account Number [{0}] already exists in other projects that doesn't allow duplicates", request.AccountNumber);
                        }
                    }
                }
            }
            return result; //TEST2
        }

        private static ValidationResult ValidateInDatabase(OptOut optout)
        {
            var result = new ValidationResult();

            var existingShipments = DataProvider.Current.ShipmentDetail.GetByAccountNumber(optout.AccountNumber);
            if (existingShipments.Count > 0)
            {
                var pendingShipment = existingShipments.Find(sh => sh.ShippingStatus == ShipmentShippingStatus.Pending);

                if (pendingShipment != null)
                    result.Errors.Add("Pending Shipment exists for Account Number [{0}]", optout.AccountNumber);
                else
                    result.Errors.Add("Shipment exists for Account Number [{0}]", optout.AccountNumber);
            }

            var existingOptOut = DataProvider.Current.OptOut.GetByAccountNumber(optout.AccountNumber, optout.ProjectId);
            if (existingOptOut != null && existingOptOut.Id != optout.Id)
                result.Errors.Add("Opt Out with Account Number [{0}] already exists", optout.AccountNumber);

            return result;
        }
       
        private static ValidationResult ValidateInDatabase(TrackingNumberException exception)
        {
            var result = new ValidationResult();

            var detail = DataProvider.Current.ShipmentDetail.GetByShipmentNumber(exception.Reference);
            if (detail == null)
                result.Errors.Add("Shipment does not exist for Shipment Number [{0}]", exception.Reference);
            else if (!String.IsNullOrEmpty(detail.FedExTrackingNumber))
                result.Errors.Add("Tracking Number has already been imported for shipment. Current Value: [{0}]", detail.FedExTrackingNumber);

            return result;
        }

        public static ValidationResult ValidateConsReqFromConsReqEx(ConsumerRequest request)
        {
            ValidationResult result = new ValidationResult();

            // get the project of the 
            Project currentProject = DataProvider.Current.Project.GetOne(request.ProjectId);
            if (currentProject.IsAllowDuplicates)
            {
                return result;
            }
            else
            {
                // check if the account number is in one of the projects of the exceptions list
                //take exception projects
                List<Guid> exceptionsIds = DataProvider.Current.ProjectDuplicateExceptions.GetAllExceptionsByProjectId(currentProject.Id);

                //check if the the account number is in one of the projects
                foreach (var projectId in exceptionsIds)
                {
                    var count = DataProvider.Current.ConsumerRequest.CheckExistsAccountNumberInProject(projectId, request.AccountNumber);
                    // ok , account exists in exceptions projects list
                    if (count != 0)
                    {
                        return result;
                    }
                    //else
                    //{
                    //    result.Errors.Add("Consumer Request with Account Number [{0}] already exists and the project doesn't allow duplicates", request.AccountNumber);
                    //}
                }

                result.Errors.Add("Consumer Request with Account Number [{0}] already exists and the project doesn't allow duplicates", request.AccountNumber);
                return result;
            }
        }
        //file
	    private static ValidationResult ValidateInDatabase(ConsumerRequest request)
	    {
		    ValidationResult result = new ValidationResult();

		    Project currentProject = DataProvider.Current.Project.GetOne(request.ProjectId);

            //todo check in the operating company is a match
            var companies = DataProvider.Current.OperatingCompany.GetByProjectServiceState(currentProject.Id);
            if (!companies.Select(c=>c.OperatingCompanyCode.ToLower()).Contains(request.OperatingCompany.ToLower()))
                result.Errors.Add(string.Format("{0} does not match list of valid values for Project.", request.OperatingCompany));

		    if (currentProject.IsAllowDuplicates)
		    {
			    return result;
		    }
		    else
		    {
			    var countAll = DataProvider.Current.ConsumerRequest.CheckExistsAccountNumberInAllProjects(request.AccountNumber);
			    var countInCurrentProject = DataProvider.Current.ConsumerRequest.CheckExistsAccountNumberInProject(currentProject.Id, request.AccountNumber);

			    if (currentProject.DuplicateExceptions == false)
			    {
				    if (currentProject.IsAllowDupsInProject)
				    {
					    //check if account number exists in ConsumerRequest table for other Projects and if it does, then it's an exception
					    if (countAll > countInCurrentProject)
					    {
							result.Errors.Add("Consumer Request with Account Number [{0}] already exists in other projects where duplicates are not allowed", request.AccountNumber);
						    return result;
					    }
				    }
				    else
				    {
					    //check if account number exists in ConsumerRequest table and if it does, then it's an exception
					    if (countAll > 0)
					    {
							result.Errors.Add("Consumer Request with Account Number [{0}] already exists in other projects where duplicates are not allowed", request.AccountNumber);
						    return result;
					    }
				    }
			    }
			    else
			    {
				    //check if the account number is in one of the projects of the exceptions list; if it is then -> ok else exception
				    //take exception projects
				    List<Guid> exceptionsIds = DataProvider.Current.ProjectDuplicateExceptions.GetAllExceptionsByProjectId(currentProject.Id);
				    var numberOfAppearances = 0;
				    foreach (var projectId in exceptionsIds)
				    {
					    numberOfAppearances += DataProvider.Current.ConsumerRequest.CheckExistsAccountNumberInProject(projectId, request.AccountNumber);
				    }

					// count also the duplicates in the current project
					if (currentProject.IsAllowDupsInProject && (!exceptionsIds.Contains(currentProject.Id)))
				    {
					    numberOfAppearances += countInCurrentProject;
				    }

				    // if the total number of appearances in exceptions projects ( + current project) is the same with countAll 
					// -> then the account appers only in the exceptions ( + current project) and is OK, 
				    // otherwise -> the account appears also in other projects than the exception list                    
				    if (countAll != numberOfAppearances)
				    {
						result.Errors.Add("Consumer Request with Account Number [{0}] already exists in other projects where duplicates are not allowed", request.AccountNumber);
					    return result;
				    }
			    }
		    }

		    return result;
	    }

	    private static ValidationResult ValidateInDatabase(ConsumerRequestException exception)
        {
            var result = new ValidationResult();

            var existingOptOut = DataProvider.Current.OptOut.GetByAccountNumber(exception.AccountNumber, exception.ProjectId);
            if (existingOptOut != null)
                result.Errors.Add("Opt Out exists for Account Number [{0}]", exception.AccountNumber);

            return result;
        }

        private static string ReplaceNullAndTrim(string original)
        {
            if (String.IsNullOrEmpty(original))
                return String.Empty;

            return original.Trim();
        }

        private static ValidationResult ValidateInDatabase(ShipmentDetail request)
        {
            ValidationResult result = new ValidationResult();

            // Project pNew = DataProvider.Current.Project.GetOne(request.ProjectId);

            ConsumerRequest CR = DataProvider.Current.ConsumerRequest.GetByAccountNumber(request.AccountNumber, request.ProjectId);

            if (CR != null)
            {
                request.ConsumerRequestId = CR.Id;

                if (request.ProjectId != CR.ProjectId)
                {
                    result.Errors.Add("Project mismatch with Consumer Request for the Account Number [{0}]", request.AccountNumber);
                }

                request.ShipmentDetailAddress1 = CR.Address1;
                request.ShipmentDetailAddress2 = CR.Address2;
                request.ShipmentDetailCity = CR.City;
                request.ShipmentDetailState = CR.State;
                request.ShipmentDetailZip = CR.ZipCode;
            }
            else
            {
                result.Errors.Add("Invalid Account Number [{0}]", request.AccountNumber);
            }

            return result;
        }
    }
}