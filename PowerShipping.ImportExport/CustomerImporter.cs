﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using PowerShipping.Data;
using PowerShipping.Entities;

namespace PowerShipping.ImportExport
{
	public class CustomerImporter
	{
		private const int FieldCount = 26;

		private readonly string _filePath;
		private readonly Guid _projectId;

		private readonly CustomerImportHistory _history;
		private readonly List<CustomerDataImportException> _importExceptions =
			new List<CustomerDataImportException>();
		private readonly HashSet<string> _accountNumbersToImport = new HashSet<string>();

		public readonly ErrorCollection ImportErrors = new ErrorCollection();
		public event EventHandler<ImportExportProgressEventArgs> ImportProgress;

		public CustomerImporter(string filePath, string userId, Guid projectId, string uploadType, int exceptionCount)
		{
			_filePath = filePath;
			_projectId = projectId;

			_history = new CustomerImportHistory
			{
				Id = Guid.NewGuid(),
				CountRecords = 0,
				FileName = Path.GetFileName(_filePath),
				ImportDate = DateTime.Now,
				ProjectId = _projectId,
				UserId = new Guid(userId),
				UploadType = uploadType,
				ExceptionCount = exceptionCount
			};
		}

		public CustomerImportHistory Import(bool replaceExistingRecords)
        {
			ReportProgress("Loading file...", 100, 50);
			var lines = ReadLines();
			ReportProgress("File loaded.", 100, 100);

			if (ImportErrors.IsEmpty)
			{
				ReportProgress("Importing Customers ...", _history.CountRecords, 0);

				try
				{
					if (replaceExistingRecords)
					{
						DataProvider.Current.Customer.DeleteAllByProjectId(_projectId);
					}
					else
					{
						GetExistingCustomerAccounts();
					}

					var idx = 1;
					string tempFilePath = _filePath + "temp.cvs";
					using (var w = new StreamWriter(tempFilePath))
					{
						var bulkImportFile = CreateBulkImportFile();
						var maxRowsToInsert = 0;

						lines.ForEach(l =>
						{
							if (ImportLine(l, ++idx))
							{
								//add the line to the Bulk file
								bulkImportFile.Append(string.Format("{0}{1}", string.Join(",", l), Environment.NewLine));
								maxRowsToInsert++;

								if (maxRowsToInsert > 2000)
								{
									w.Write(bulkImportFile);
									w.Flush();

									maxRowsToInsert = 0;
									bulkImportFile = new StringBuilder();
								}
							}

							ReportProgress("Importing Customers...",
								_history.CountRecords, idx);
						});

						if (maxRowsToInsert > 0)
						{
							w.Write(bulkImportFile);
							w.Flush();
						}
					}
					
					With.Transaction(() =>
					{
						if (replaceExistingRecords)
						{
							DataProvider.Current.CustomerImportHistory.ImportBulk(tempFilePath, _projectId);
						}
						else
						{
							DataProvider.Current.CustomerImportHistory.AppendImportBulk(tempFilePath, _projectId);
						}

						DataProvider.Current.CustomerImportHistory.Add(_history);

						if (_importExceptions.Any())
						{
							_importExceptions.ForEach(el => DataProvider.Current.CustomerDataImportException.Add(el));
						}
					});

					//delete the temporary file
					File.Delete(tempFilePath);
					
					ReportProgress("Customers imported successfully.", _history.CountRecords, 0);

				}
				catch (Exception ex)
				{
					ImportErrors.Add(ex.Message);
					ReportProgress("Customer import failed.", _history.CountRecords, 0);
				}
			}

			return _history;
		}

		private void GetExistingCustomerAccounts()
		{
			string query = string.Format("Select * from Customer where ProjectId ='{0}'", _projectId);
			var accountNrs = DataProvider.Current.Customer.GetCustom(query).Select(el => el.AccountNumber).ToList();
			foreach (var accountNr in accountNrs)
			{
				_accountNumbersToImport.Add(accountNr);
			}
		}

		private StringBuilder CreateBulkImportFile()
		{
			var cvsBulkImport = new StringBuilder();
			var values = new string[26];

			#region Make the Header

			values[0] = "AccountNumber";
			values[1] = "CompanyName";
			values[2] = "ContactName";
			values[3] = "Email1";
			values[4] = "Email2";
			values[5] = "Phone1";
			values[6] = "Phone2";
			values[7] = "MailingAddress1";
			values[8] = "MailingAddress2";
			values[9] = "MailingCity";
			values[10] = "MailingState";
			values[11] = "MailingZip";
			values[12] = "ServiceAddress1";
			values[13] = "ServiceAddress2";
			values[14] = "ServiceCity";
			values[15] = "ServiceState";
			values[16] = "ServiceZip";
			values[17] = "OperatingCompany";
			values[18] = "WaterHeaterType";
			values[19] = "PremiseID";
			values[20] = "OkToContact";
			values[21] = "OptOut";
			values[22] = "InvitationCode";
			values[23] = "FacilityType";
			values[24] = "RateCode";
			values[25] = "IncomeQualified";

			#endregion

			cvsBulkImport.Append(string.Format("{0}{1}", string.Join(",", values), Environment.NewLine));

			return cvsBulkImport;
		}

		List<string[]> ReadLines()
		{
			_history.CountRecords = 0;
			var rows = new List<string[]>();

			try
			{
				using (var sr = new StreamReader(_filePath))
				{
					string line;
					int index = 0;
					//Read and display lines from the file until the end of the file is reached.                
					while ((line = sr.ReadLine()) != null)
					{
						index++;
						if (index == 1)
						{
							continue;
						}

						var match = Matcher.Match(line);
						if (!match.Success)
						{
							_importExceptions.Add(new CustomerDataImportException
							{
								CustomerDataImportExceptionId = Guid.NewGuid(),
								CustomerDataImportId = _history.Id,
								RecordNumber = index.ToString(),
								RecordData = line,
								Reason = string.Format("Line {0}. Unexpected file format.", index)
							});
							continue;
						}

						var row = match.Groups["value"].Captures.Cast<Capture>().Select(c => c.Value.TrimEnd()).ToArray();

						if (row.Length < FieldCount)
						{
							_importExceptions.Add(new CustomerDataImportException
							{
								CustomerDataImportExceptionId = Guid.NewGuid(),
								CustomerDataImportId = _history.Id,
								RecordNumber = index.ToString(),
								RecordData = line,
								Reason = string.Format("Line {0}. Expected at least {1} values in row but was {2}", index, FieldCount, row.Length)
							});
							continue;
						}

						// skip row with all empty values
						if (Array.TrueForAll(row, field => field == String.Empty))
							continue;

						rows.Add(row);
					}
				}
			}
			catch (Exception ex)
			{
				ImportErrors.Add(ex.Message);
			}
			finally
			{
				_history.CountRecords = rows.Count;
			}

			return rows;
		}

		private bool ImportLine(string[] columns, int rowNr)
		{
			var exception = NewCustomerException();
			var mapping = EntityMapping.Map(columns, exception);
			var importException = NewCustomerDataImportException(rowNr, columns);

			if (mapping.Success)
			{
				var customer = NewCustomer();

				var validation = EntityValidation.MapAndValidate(exception, customer);
				if (_accountNumbersToImport.Contains(customer.AccountNumber))
				{
					importException.Reason = string.Format("Customer exists for Account Number [{0}].", customer.AccountNumber);
				}
				else
				{
					_accountNumbersToImport.Add(customer.AccountNumber);

					if (validation.Success)
					{
						customer.ProjectId = _projectId;
						return true;
					}

					importException.Reason = validation.Errors.ToString(";");
				}
			}
			else
			{
				importException.Reason = mapping.Errors.ToString(";");
			}

			_importExceptions.Add(importException);

			return false;
		}

		private CustomerException NewCustomerException()
		{
			return new CustomerException
			{
				Id = Guid.NewGuid(),
				ProjectId = _projectId
			};
		}

		private Customer NewCustomer()
		{
			return new Customer
			{
				Id = Guid.NewGuid(),
				ProjectId = _projectId
			};
		}

		private CustomerDataImportException NewCustomerDataImportException(int rowNr, string[] columns )
		{
			return new CustomerDataImportException
			{
				CustomerDataImportExceptionId = Guid.NewGuid(),
				CustomerDataImportId = _history.Id,
				RecordNumber = rowNr.ToString(),
				RecordData = String.Join(", ", columns)
			};
		}

		private void ReportProgress(string message, int total, int value)
		{
			var handler = ImportProgress;

			if (handler != null)
				handler(this, new ImportExportProgressEventArgs(message, total, value));
		}

		private readonly Regex Matcher = new Regex(Pattern, RegexOptions.Compiled);

		private const string Pattern = "^(\"(?<value>[^\"]*)\"|(?<value>[^,\"]*))?(,(\"(?<value>[^\"]*)\"|(?<value>[^,\"]*)))*$";

	}
}
