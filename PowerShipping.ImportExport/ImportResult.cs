namespace PowerShipping.ImportExport
{
	public interface ImportResult
	{
		int TotalRecords { get; }
		int ImportedSuccessfully { get; }
		int ImportedWithException { get; }
		
		string[] ImportErrors { get; }
	}
}