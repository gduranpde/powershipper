using System;

namespace PowerShipping.ImportExport
{
	public class ImportExportProgressEventArgs : EventArgs
	{
		public readonly int Total;
		public readonly int Value;
		public readonly string Message;
		
		public ImportExportProgressEventArgs(string message, int total, int value)
		{
			Total = total;
			Value = value;
			Message = message;
		}
	}
}